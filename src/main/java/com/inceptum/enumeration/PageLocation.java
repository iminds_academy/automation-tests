package com.inceptum.enumeration;

import java.util.HashMap;

/**
 * This represents a page and it's URL location relative to the host.
 */
public class PageLocation {

    /* ----- FIELDS ----- */
    private String basePath;
    private HashMap<String, String> locations = new HashMap<String, String>() {{
        put("HOME", "");
    }};

    /* ----- CONSTRUCTORS ----- */

    /**
     * Instantiates a new page location.
     *
     * @param basePath  the basePath url;
     * @param name the name
     */
    public PageLocation(String basePath) {
        this.basePath = basePath;
    }

    /* ----- GETTERS / SETTERS ----- */

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl(String page) {
        return locations.get(page);
    }

    /**
     * Gets the base path.
     *
     * @return the name
     */
    public String getBasePath() {
        return basePath;
    }

    /**
     * Returns the full url
     *
     * @return the name
     */
    public String getFullUrl(String page) {
        return new StringBuilder().append(getBasePath()).append('/')
                    .append(getUrl(page)).toString();
    }
}
