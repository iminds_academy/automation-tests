package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class AddContentPage extends BasePage {


    /* ----- FIELDS ----- */

    @FindBy(how = How.XPATH, using = "//a/h2[text()='Info page']")
    private WebElement linkInfoPage;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Live session']")
    private WebElement linkLiveSession;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Embed content']")
    private WebElement linkEmbedContent;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Media page']")
    private WebElement linkMediaPage;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Task']")
    private WebElement linkTask;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Theory']")
    private WebElement linkTheory;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Image']")
    private WebElement linkImage;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Presentation']")
    private WebElement linkPresentation;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Video']")
    private WebElement linkVideo;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Multiple choice question']")
    private WebElement linkMultipleChoiceQuestion;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Quiz']")
    private WebElement linkQuiz;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Class']")
    private WebElement linkClass;
    @FindBy(how = How.XPATH, using = "//a/h2[text()='Group']")
    private WebElement linkGroup;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Course materials")
    private WebElement linkCourseMaterials;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Others")
    private WebElement linkOthers;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Peer Assignment")
    private WebElement linkPeerAssignment;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Advanced course components")
    private WebElement linkAdvancedCourseComponents;


    /*---------CONSTRUCTORS--------*/

    public AddContentPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /*---------METHODS----------*/

    public  ViewInfoPage clickLinkInfoPage() throws Exception {
        waitUntilElementIsClickable(linkInfoPage);
        clickItem(linkInfoPage, "The link 'Info page' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewInfoPage(webDriver, locations);
    }
    public ViewLiveSessionPage clickLinkLiveSession() throws Exception {
        waitUntilElementIsClickable(linkLiveSession);
        clickItem(linkLiveSession, "The link 'Live session' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewLiveSessionPage(webDriver, locations);
    }
    public ViewMediaPage clickLinkMediaPage() throws Exception {
        waitUntilElementIsClickable(linkMediaPage);
        clickItem(linkMediaPage, "The link 'Media page' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewMediaPage(webDriver, locations);
    }
    public ViewTaskPage clickLinkTask() throws Exception {
        waitUntilElementIsClickable(linkTask);
        clickItem(linkTask, "The link 'Task' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewTaskPage(webDriver, locations);
    }
    public ViewTheoryPage clickLinkTheory() throws Exception {
        waitUntilElementIsClickable(linkTheory);
        clickItem(linkTheory, "The link 'Theory' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewTheoryPage(webDriver, locations);
    }
    public ViewImagePage clickLinkImage() throws Exception {
        waitUntilElementIsClickable(linkCourseMaterials);
        clickItem(linkCourseMaterials, "The link 'Course materials' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkImage);
        clickItem(linkImage, "The link 'Image' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewImagePage(webDriver, locations);
    }
    public ViewPresentationPage clickLinkPresentation() throws Exception {
        waitUntilElementIsClickable(linkCourseMaterials);
        clickItem(linkCourseMaterials, "The link 'Course materials' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkPresentation);
        clickItem(linkPresentation, "The link 'Presentation' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewPresentationPage(webDriver, locations);
    }
    public ViewEmbedContentPage clickLinkEmbedContentPage() throws Exception {
        waitUntilElementIsClickable(linkAdvancedCourseComponents);
        clickItem(linkAdvancedCourseComponents, "The link 'Advanced course components' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkEmbedContent);
        clickItem(linkEmbedContent, "The link 'Embed content' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewEmbedContentPage(webDriver, locations);
    }
    public ViewVideoPage clickLinkVideo() throws Exception {
        waitUntilElementIsClickable(linkCourseMaterials);
        clickItem(linkCourseMaterials, "The link 'Course materials' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkVideo);
        clickItem(linkVideo, "The link 'Video' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new ViewVideoPage(webDriver, locations);
    }
    public CreateMultipleChoiceQuestionPage clickLinkMultipleChoiceQuestion() throws Exception {
        waitUntilElementIsClickable(linkOthers);
        clickItem(linkOthers, "The link 'Others' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkMultipleChoiceQuestion);
        clickItem(linkMultipleChoiceQuestion, "The link 'Multiple choice question' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new CreateMultipleChoiceQuestionPage(webDriver, locations);
    }
    public CreateQuizPage clickLinkQuiz() throws Exception {
        waitUntilElementIsClickable(linkOthers);
        clickItem(linkOthers, "The link 'Interactivity' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkQuiz);
        clickItem(linkQuiz, "The link 'Quiz' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new CreateQuizPage(webDriver, locations);
    }
    public CreateClassPage clickLinkClass() throws Exception {
        waitUntilElementIsClickable(linkOthers);
        clickItem(linkOthers, "The link 'Others' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkClass);
        scrollToElement(linkClass);
        clickItem(linkClass, "The link 'Class' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new CreateClassPage(webDriver, locations);
    }
    public CreateGroupPage clickLinkGroup() throws Exception {
        waitUntilElementIsClickable(linkOthers);
        clickItem(linkOthers, "The link 'Others' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkGroup);
        clickItem(linkGroup, "The link 'Group' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new CreateGroupPage(webDriver, locations);
    }
    public CreatePeerAssignmentPage clickLinkPeerAssignment() throws Exception {
        waitUntilElementIsClickable(linkOthers);
        clickItem(linkOthers, "The link 'Others' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkPeerAssignment);
        clickItem(linkPeerAssignment, "The link 'Peer Assignment' on the AddContent page could not be clicked.");
        waitForPageToLoad();
        return new CreatePeerAssignmentPage(webDriver, locations);
    }


}
