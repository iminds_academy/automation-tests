package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;


public class AddFeedbackBlockPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;

    @FindBy(how = How.ID, using = "edit-feedback-feedback-1-personal-name")
    private WebElement fieldNameOfPerson;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-1-personal-title")
    private WebElement fieldHisTitle;
    @FindBy(how = How.CSS, using = "input[id='edit-feedback-feedback-1-personal-avatar-upload']")
    private WebElement fieldAvatar;
    @FindBy(how = How.CSS, using = "[id*='feedback-1'] iframe")
    private WebElement iframeFeedback;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-1-personal-avatar-upload-button")
    private WebElement buttonUpload;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-1-personal-avatar-remove-button")
    private WebElement buttonRemove;

    @FindBy(how = How.ID, using = "edit-feedback-feedback-2-personal-name")
    private WebElement fieldNameOfPerson2;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-2-personal-title")
    private WebElement fieldHisTitle2;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-2-personal-avatar-upload")
    private WebElement fieldAvatar2;
    @FindBy(how = How.CSS, using = "[id*='feedback-2'] iframe")
    private WebElement iframeFeedback2;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-2-personal-avatar-upload-button")
    private WebElement buttonUpload2;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-2-personal-avatar-remove-button")
    private WebElement buttonRemove2;

    @FindBy(how = How.ID, using = "edit-add-button")
    private WebElement buttonAddFeedback;
    @FindBy(how = How.ID, using = "edit-feedback-feedback-1-personal-delete-button-button") // the 1st feedback
    private WebElement buttonDeleteFeedback;
    @FindBy(how = How.ID, using = "edit-actions-submit")
    private WebElement buttonSave;


    /* ----- CONSTRUCTORS ----- */

    public AddFeedbackBlockPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public void fillFieldNameOfPerson(String name) throws Exception {
        waitUntilElementIsVisible(fieldNameOfPerson);
        fieldNameOfPerson.sendKeys(name);
    }
    public void fillFieldHisTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldHisTitle);
        fieldHisTitle.sendKeys(title);
    }
    public WebElement getFieldAvatar() {
        return fieldAvatar;
    }
    public WebElement getIframeFeedback() {
        return iframeFeedback;
    }
    public WebElement getButtonUpload() {
        return buttonUpload;
    }
    public WebElement getButtonRemove() {
        return buttonRemove;
    }
    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The Remove button on the AddFeedbackBlock page could not be clicked.");
        waitUntilElementIsVisible(buttonUpload);
    }

    public void fillFieldNameOfPerson2(String name) throws Exception {
        waitUntilElementIsVisible(fieldNameOfPerson2);
        fieldNameOfPerson2.sendKeys(name);
    }
    public void fillFieldHisTitle2(String title) throws Exception {
        waitUntilElementIsVisible(fieldHisTitle2);
        fieldHisTitle2.sendKeys(title);
    }
    public WebElement getFieldAvatar2() {
        return fieldAvatar2;
    }
    public WebElement getIframeFeedback2() {
        return iframeFeedback2;
    }
    public WebElement getButtonUpload2() {
        return buttonUpload2;
    }
    public WebElement getButtonRemove2() {
        return buttonRemove2;
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the AddFeedbackBlock page could not be clicked.");
        waitForPageToLoad();
    }
    public void clickButtonAddFeedback() throws Exception {
        waitUntilElementIsClickable(buttonAddFeedback);
        clickItem(buttonAddFeedback, "The 'Add feedback' button on the AddFeedbackBlock page could not be clicked.");
    }
    public void clickButtonDeleteFeedback() throws Exception {
        waitUntilElementIsClickable(buttonDeleteFeedback);
        clickItem(buttonDeleteFeedback, "The 'Delete feedback' button on the AddFeedbackBlock page could not be clicked.");
        waitForPageToLoad();
    }

}
