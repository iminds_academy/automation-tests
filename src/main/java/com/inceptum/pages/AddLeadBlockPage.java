package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;


public class AddLeadBlockPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-text")
    private WebElement fieldText;
    @FindBy(how = How.CSS, using = "input[id='edit-image-upload']")
    private WebElement fieldUpload;
    @FindBy(how = How.ID, using = "edit-buttons-buttons-1-title")
    private WebElement fieldTitleOfButton;
    @FindBy(how = How.ID, using = "edit-buttons-buttons-2-title")
    private WebElement fieldTitleOfButton2;
    @FindBy(how = How.ID, using = "edit-buttons-buttons-1-url")
    private WebElement fieldUrlOfButton;
    @FindBy(how = How.ID, using = "edit-buttons-buttons-2-url")
    private WebElement fieldUrlOfButton2;
    @FindBy(how = How.ID, using = "edit-image-upload-button")
    private WebElement buttonUpload;
    @FindBy(how = How.ID, using = "edit-image-remove-button")
    private WebElement buttonRemove;
    @FindBy(how = How.ID, using = "edit-add-button")
    private WebElement buttonAddButton;
    @FindBy(how = How.ID, using = "edit-buttons-buttons-1-delete-button-button")
    private WebElement buttonDeleteButton;
    @FindBy(how = How.ID, using = "edit-actions-submit")
    private WebElement buttonSave;


    /* ----- CONSTRUCTORS ----- */

    public AddLeadBlockPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public WebElement getFieldText() {
        return fieldText;
    }
    public void fillFieldText(String text) throws Exception {
        waitUntilElementIsVisible(fieldText);
        fieldText.sendKeys(text);
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the AddLeadBlock page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getFieldUpload() {
        return fieldUpload;
    }
    public WebElement getButtonUpload() {
        return buttonUpload;
    }
    public WebElement getButtonRemove() {
        return buttonRemove;
    }
    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The 'Remove' button on the AddLeadBlock page could not be clicked.");
        waitUntilElementIsVisible(buttonUpload);
    }
    public void clickButtonAddButton() throws Exception {
        waitUntilElementIsClickable(buttonAddButton);
        clickItem(buttonAddButton, "The 'Add button' button on the AddLeadBlock page could not be clicked.");
    }
    public void fillFieldTitleOfButton(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleOfButton);
        fieldTitleOfButton.sendKeys(title);
    }
    public void fillFieldUrlOfButton(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlOfButton);
        fieldUrlOfButton.sendKeys(url);
    }
    public void fillFieldTitleOfButton2(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleOfButton2);
        fieldTitleOfButton2.sendKeys(title);
    }
    public void fillFieldUrlOfButton2(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlOfButton2);
        fieldUrlOfButton2.sendKeys(url);
    }
    public void clickButtonDeleteButton() throws Exception {
        waitUntilElementIsClickable(buttonDeleteButton);
        clickItem(buttonDeleteButton, "The 'Delete button' button on the AddLeadBlock page could not be clicked.");
        waitForPageToLoad();
    }


}
