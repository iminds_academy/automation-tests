package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class AddLiveSessionTypePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-name")
    private WebElement fieldName;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSave;


    /*---------CONSTRUCTORS--------*/
    public AddLiveSessionTypePage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldName (String name) throws Exception {
        waitUntilElementIsVisible(fieldName);
        fieldName.sendKeys(name);
    }
    public WebElement getFieldName() {
        waitUntilElementIsVisible(fieldName);
        return fieldName;
    }
    public EditLiveSessionPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The button Save on the AddLiveSessionType page could not be clicked.");
        waitForPageToLoad();
        return new EditLiveSessionPage(webDriver, locations);
    }

}
