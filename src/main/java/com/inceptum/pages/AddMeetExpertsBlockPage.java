package com.inceptum.pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;


public class AddMeetExpertsBlockPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-experts-elements-1-form-personal-user")
    private WebElement fieldUser;
    @FindBy(how = How.CSS, using = "[id*='edit-experts-elements-1-form-personal-name-name']")
    private WebElement fieldName;
    @FindBy(how = How.CSS, using = "[id*='experts-elements-1-form-description'] iframe")
    private WebElement iframeDescription;
    @FindBy(how = How.CSS, using = "[id*='edit-experts-elements-1-form-personal-name-override']")
    private WebElement checkboxOverrideName;

    @FindBy(how = How.ID, using = "edit-experts-elements-2-form-personal-user")
    private WebElement fieldUser2;
    @FindBy(how = How.CSS, using = "[id*='edit-experts-elements-2-form-personal-name-name']")
    private WebElement fieldName2;
    @FindBy(how = How.CSS, using = "[id*='experts-elements-2-form-description'] iframe")
    private WebElement iframeDescription2;
    @FindBy(how = How.CSS, using = "[id*='edit-experts-elements-2-form-personal-name-override']")
    private WebElement checkboxOverrideName2;

    @FindBy(how = How.CSS, using = "label+[id*='edit-experts-elements-1-form-personal-picture-picture']")
    private WebElement blockProfilePicture;
    @FindBy(how = How.ID, using = "edit-experts-elements-1-form-personal-picture-override--2")
    private WebElement checkboxOverridePicture;
    @FindBy(how = How.CSS, using = "[id*=edit-experts-elements-1-form-personal-picture-picture-upload][type='file']")
    private WebElement fieldProfilePicture;
    @FindBy(how = How.CSS, using = "[id*='edit-experts-elements-1-form-personal-picture-picture-upload-button']")
    private WebElement buttonUpload;
    @FindBy(how = How.ID, using = "edit-experts-elements-1-form-personal-picture-picture-remove-button")
    private WebElement buttonRemove;
    @FindBy(how = How.CSS, using = "[id*='edit-experts-elements-1-form-personal-profile-internal']")
    private WebElement radiobuttonInternalProfile;
    @FindBy(how = How.ID, using = "edit-add-button")
    private WebElement buttonAddExpert;
    @FindBy(how = How.ID, using = "edit-experts-elements-1-form-personal-delete-button")
    private WebElement buttonDeleteExpert;
    @FindBy(how = How.ID, using = "edit-actions-submit")
    private WebElement buttonSave;


    /* ----- CONSTRUCTORS ----- */

    public AddMeetExpertsBlockPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public WebElement getFieldUser() {
        waitUntilElementIsVisible(fieldUser);
        return fieldUser;
    }
    public WebElement getFieldName() {
        return fieldName;
    }
    public WebElement getIframeDescription() {
        return iframeDescription;
    }
    public void clickButtonAddExpert() throws Exception {
        waitUntilElementIsClickable(buttonAddExpert);
        clickItem(buttonAddExpert, "The 'Add expert' button on the AddMeetExpertBlock page could not be clicked.");
    }
    public WebElement getCheckboxOverrideName() {
        return checkboxOverrideName;
    }
    public void selectCheckboxOverrideName() throws Exception {
        waitUntilElementIsClickable(checkboxOverrideName);
        clickItem(checkboxOverrideName, "The checkbox Override name on the AddMeetExpertBlock page could not be clicked.");
    }
    public WebElement getFieldUser2() {
        waitUntilElementIsVisible(fieldUser2);
        return fieldUser2;
    }
    public WebElement getFieldName2() {
        return fieldName2;
    }
    public WebElement getIframeDescription2() {
        return iframeDescription2;
    }
    public WebElement getCheckboxOverrideName2() {
        return checkboxOverrideName2;
    }
    public WebElement getBlockProfilePicture() {
        return blockProfilePicture;
    }
    public void selectCheckboxOverridePicture() throws Exception {
        waitUntilElementIsClickable(checkboxOverridePicture);
        clickItem(checkboxOverridePicture, "The checkbox Override profile picture on the AddMeetExpertBlock page could not be clicked.");
    }
    public WebElement getFieldProfilePicture() {
        return fieldProfilePicture;
    }
    public WebElement getButtonUpload() {
        return buttonUpload;
    }
    public WebElement getButtonRemove() {
        return buttonRemove;
    }
    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The Remove button on the AddMeetExpertsBlock page could not be clicked.");
        waitUntilElementIsVisible(buttonUpload);
    }
    public void selectRadiobuttonInternalProfile() throws Exception {
        waitUntilElementIsClickable(radiobuttonInternalProfile);
        radiobuttonInternalProfile.sendKeys(Keys.SPACE);
    }
    public void clickButtonDeleteExpert() throws Exception {
        waitUntilElementIsClickable(buttonDeleteExpert);
        clickItem(buttonDeleteExpert, "The button 'Delete expert' on the AddMeetExpertsBlock page could not be clicked.");
        waitForPageToLoad();
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the AddMeetExpertsBlock page could not be clicked.");
        waitForPageToLoad();
    }

}
