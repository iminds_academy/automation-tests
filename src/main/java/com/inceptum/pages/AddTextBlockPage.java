package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;


public class AddTextBlockPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "[id*='edit-text'] iframe")
    private WebElement frameText;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSave;


    /* ----- CONSTRUCTORS ----- */

    public AddTextBlockPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public WebElement getFrameText() {
        return frameText;
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the AddTextBlock page could not be clicked.");
        waitForPageToLoad();
    }



}
