package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class AddUserPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-field-profile-full-name-und-0-value")
    private WebElement fieldFullName;
    @FindBy(how = How.ID, using = "edit-mail")
    private WebElement fieldEmail;
    @FindBy(how = How.ID, using = "edit-pass-pass1")
    private WebElement fieldPassword;
    @FindBy(how = How.ID, using = "edit-pass-pass2")
    private WebElement fieldConfirmPassword;
    @FindBy(how = How.ID, using = "edit-status-1")
    private WebElement radiobuttonActive;
    @FindBy(how = How.ID, using = "edit-status-0")
    private WebElement radiobuttonBlocked;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonCreateNewAccount;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement messageError;


    /*---------CONSTRUCTORS--------*/
    public AddUserPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void addUser(String fullName, String email, String password) throws Exception {
        // Enter data to the fields
        fillInFields(fullName, email, password);

        // Check 'Active' is selected
        Assert.assertTrue(radiobuttonActive.isSelected());
        clickButtonCreateNewAccount();
        assertTextOfMessage(message, ".*Created a new user account for " + fullName + ". No e-mail has been sent.");
    }
    public void addUserWithRole(String role, String fullName, String email, String password) throws Exception {
        // Enter data to the fields
        fillInFields(fullName, email, password);
        // Check 'Active' is selected
        Assert.assertTrue(radiobuttonActive.isSelected());
        // Select needed role for the user
        selectRole(role);

        // Save data and check info message
        clickButtonCreateNewAccount();
        assertTextOfMessage(message, ".*Created a new user account for " + fullName + ". No e-mail has been sent.");
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getMessageError() {
        return messageError;
    }
    public void clickButtonCreateNewAccount() throws Exception {
        waitUntilElementIsClickable(buttonCreateNewAccount);
        clickItem(buttonCreateNewAccount, "The button CreateNewAccount on the AddUser page could not be clicked.");
        waitForPageToLoad();
    }
    public void fillInFields(String fullName, String email, String password) throws Exception {
        waitForPageToLoad();
        waitUntilElementIsVisible(fieldFullName);
        fieldFullName.sendKeys(fullName);
        waitUntilElementIsVisible(fieldEmail);
        fieldEmail.sendKeys(email);
        waitUntilElementIsVisible(fieldPassword);
        fieldPassword.sendKeys(password);
        waitUntilElementIsVisible(fieldConfirmPassword);
        fieldConfirmPassword.sendKeys(password);
    }
    public WebElement getRadiobuttonActive(){
        return radiobuttonActive;
    }
    public void selectRadiobuttonBlocked() throws Exception {
        waitUntilElementIsClickable(radiobuttonBlocked);
        clickItem(radiobuttonBlocked, "The radiobutton 'Blocked' on the AddUser page could not be selected.");
        Assert.assertTrue(radiobuttonBlocked.isSelected());
    }
    public WebElement getRadiobuttonBlocked() {
        return radiobuttonBlocked;
    }
    public WebElement getFieldFullName() {
        waitUntilElementIsVisible(fieldFullName);
        return fieldFullName;
    }
    public WebElement getFieldEmail() {
        waitUntilElementIsVisible(fieldEmail);
        return fieldEmail;
    }
    public WebElement getFieldPassword() {
        waitUntilElementIsVisible(fieldPassword);
        return fieldPassword;
    }
    public WebElement getFieldConfirmPassword() {
        waitUntilElementIsVisible(fieldConfirmPassword);
        return fieldConfirmPassword;
    }


}
