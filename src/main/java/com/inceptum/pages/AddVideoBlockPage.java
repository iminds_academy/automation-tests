package com.inceptum.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;


public class AddVideoBlockPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-label")
    private WebElement fieldLabel;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select media")
    private WebElement linkSelectMedia;
    @FindBy(how = How.CSS, using = "[id='edit-field-single-video-und-0'] a.button.remove")
    private WebElement linkRemoveMedia;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSave;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;



    /* ----- CONSTRUCTORS ----- */

    public AddVideoBlockPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public SelectVideoPage clickLinkSelectMedia() throws Exception {
        waitUntilElementIsClickable(linkSelectMedia);
        clickItem(linkSelectMedia, "The 'Select media' link on the AddVideoBlock page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public WebElement getLinkRemoveMedia() {
        return linkRemoveMedia;
    }
    public void clickLinkRemoveMedia() throws Exception {
        waitUntilElementIsClickable(linkRemoveMedia);
        clickItem(linkRemoveMedia, "The 'Remove media' link on the AddVideoBlock page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("[id='edit-field-single-video-und-0'] a.button.remove")));
    }
    public WebElement getFieldLabel() {
        waitUntilElementIsVisible(fieldLabel);
        return fieldLabel;
    }
    public void fillFieldLabel(String label) throws Exception {
        waitUntilElementIsVisible(fieldLabel);
        fieldLabel.sendKeys(label);
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the AddVideoBlock page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }

}
