package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;


public class AddWhatYouNeedToKnowBlockPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-label")
    private WebElement fieldLabel;
    @FindBy(how = How.ID, using = "edit-field-wyntk-more-und-0-title")
    private WebElement fieldTitleMoreDetails;
    @FindBy(how = How.ID, using = "edit-field-wyntk-more-und-0-url")
    private WebElement fieldUrlMoreDetails;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-0-field-wyntk-section-title-und-0-value")
    private WebElement fieldTitleInfoSection;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-0-field-wyntk-section-icon-und")
    private WebElement fieldIcon;
    @FindBy(how = How.CSS, using = "#cke_edit-field-wyntk-info-und-0-field-wyntk-section-description-und-0-value iframe")
    private WebElement iframeDescription;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-0-field-wyntk-section-more-und-0-title")
    private WebElement fieldTitleInfoMoreDetails;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-0-field-wyntk-section-more-und-0-url")
    private WebElement fieldUrlInfoMoreDetails;


    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-1-field-wyntk-section-title-und-0-value")
    private WebElement fieldTitleInfoSection2;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-1-field-wyntk-section-icon-und")
    private WebElement fieldIcon2;
    @FindBy(how = How.CSS, using = "#cke_edit-field-wyntk-info-und-1-field-wyntk-section-description-und-0-value iframe")
    private WebElement iframeDescription2;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-1-field-wyntk-section-more-und-0-title")
    private WebElement fieldTitleInfoMoreDetails2;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-1-field-wyntk-section-more-und-0-url")
    private WebElement fieldUrlInfoMoreDetails2;

    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-add-more")
    private WebElement buttonAddAnotherItem;
    @FindBy(how = How.ID, using = "edit-field-wyntk-info-und-0-remove-button")
    private WebElement buttonRemove;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSave;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;


    /* ----- CONSTRUCTORS ----- */

    public AddWhatYouNeedToKnowBlockPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public WebElement getFieldLabel() {
        return fieldLabel;
    }
    public void fillFieldLabel(String label) throws Exception {
        waitUntilElementIsVisible(fieldLabel);
        fieldLabel.sendKeys(label);
    }
    public void fillFieldTitleMoreDetails(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleMoreDetails);
        fieldTitleMoreDetails.sendKeys(title);
    }
    public void fillFieldUrlMoreDetails(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlMoreDetails);
        fieldUrlMoreDetails.sendKeys(url);
    }
    public void fillFieldTitleInfoSection(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleInfoSection);
        fieldTitleInfoSection.sendKeys(title);
    }
    public void fillFieldTitleInfoSection2(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleInfoSection2);
        fieldTitleInfoSection2.sendKeys(title);
    }
    public WebElement getFieldIcon() {
        return fieldIcon;
    }
    public WebElement getFieldIcon2() {
        return fieldIcon2;
    }
    public WebElement getIframeDescription() {
        return iframeDescription;
    }
    public WebElement getIframeDescription2() {
        return iframeDescription2;
    }
    public void fillFieldTitleInfoMoreDetails(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleInfoMoreDetails);
        fieldTitleInfoMoreDetails.sendKeys(title);
    }
    public void fillFieldTitleInfoMoreDetails2(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleInfoMoreDetails2);
        fieldTitleInfoMoreDetails2.sendKeys(title);
    }
    public void fillFieldUrlInfoMoreDetails(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlInfoMoreDetails);
        fieldUrlInfoMoreDetails.sendKeys(url);
    }
    public void fillFieldUrlInfoMoreDetails2(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlInfoMoreDetails2);
        fieldUrlInfoMoreDetails2.sendKeys(url);
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the AddWhatYouNeedToDo page could not be clicked.");
        waitForPageToLoad();
    }
    public void clickButtonAddAnotherItem() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem);
        clickItem(buttonAddAnotherItem, "The button 'Add another item' on the AddWhatYouNeedToDo page could not be clicked.");
    }
    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The Remove button on the AddWhatYouNeedToDo page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }



}
