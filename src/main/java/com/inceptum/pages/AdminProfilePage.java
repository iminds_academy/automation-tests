package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 11.11.2014.
 */
public class AdminProfilePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-field-profile-full-name-und-0-value")
    private WebElement fieldFullName;
    @FindBy(how = How.ID, using = "edit-mail")
    private WebElement fieldEmailAddress;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "My results")
    private WebElement tabMyResults;


    /*---------CONSTRUCTORS--------*/

    public AdminProfilePage (WebDriver webDriver, PageLocation locations) throws Exception {
            super(webDriver, locations);
        }


    /*---------METHODS----------*/

    public String getName() {
        waitUntilElementIsVisible(fieldFullName);
        String name = fieldFullName.getAttribute("value");
        return name;
    }
    public String getEmailAddress() {
        waitUntilElementIsVisible(fieldEmailAddress);
        String emailAddress = fieldEmailAddress.getAttribute("value");
        return emailAddress;
    }
    public MyResultsPage switchToTabMyResults() throws Exception {
        waitUntilElementIsClickable(tabMyResults);
        clickItem(tabMyResults, "The tab 'My results' on the AdminProfile page could not be clicked.");
        return new MyResultsPage(webDriver, locations);
    }
    public WebElement getTabMyResults() {
        return tabMyResults;
    }

    }
