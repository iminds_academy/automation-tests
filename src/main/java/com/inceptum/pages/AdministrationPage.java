package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class AdministrationPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "div.quickedit-init-processed a.site-logo")
    private WebElement buttonHome;
    @FindBy(how = How.CSS, using = "a.escape-admin-processed")
    private WebElement buttonBackToSite;
    @FindBy(how = How.ID, using = "navbar-item--2")
    private WebElement buttonMenu;
    @FindBy(how = How.ID, using = "navbar-item--3")
    private WebElement buttonShortcuts;
    @FindBy(how = How.ID, using = "navbar-item--4")
    private WebElement buttonMyAccount;
    @FindBy(how = How.ID, using = "navbar-link-admin-content")
    private WebElement buttonContent;
    @FindBy(how = How.ID, using = "navbar-link-admin-structure")
    private WebElement buttonStructure;
    @FindBy(how = How.ID, using = "navbar-link-admin-people")
    private WebElement buttonPeople;
    @FindBy(how = How.ID, using = "navbar-link-admin-quiz")
    private WebElement buttonQuiz;
    @FindBy(how = How.LINK_TEXT, using = "Add content")
    private WebElement buttonAddContent;
    @FindBy(how = How.LINK_TEXT, using = "Find content")
    private WebElement buttonFindContent;
    @FindBy(how = How.LINK_TEXT, using = "View profile")
    private WebElement buttonViewProfile;
    @FindBy(how = How.LINK_TEXT, using = "Log out")
    private WebElement buttonLogOut;
    @FindBy(how = How.ID, using = "navbar-link-admin-config")
    private WebElement buttonConfiguration;


    /*---------CONSTRUCTORS--------*/

    public AdministrationPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public HomePage clickButtonHome() throws Exception {
        waitUntilElementIsClickable(buttonHome);
        clickItem(buttonHome, "The button 'Home' could not be clicked.");
        waitForPageToLoad();
        return new HomePage(webDriver, locations);
    }
    public void clickButtonBackToSite() throws Exception {
        waitUntilElementIsClickable(buttonBackToSite);
        clickItem(buttonBackToSite, "The button 'Back to site' could not be clicked.");
        waitForPageToLoad();
    }
    public void clickButtonMenu() throws Exception {
        waitUntilElementIsClickable(buttonMenu);
        clickItem(buttonMenu, "The button 'Menu' could not be clicked.");
    }
    public void clickButtonShortcuts() throws Exception {
        waitUntilElementIsClickable(buttonShortcuts);
        clickItem(buttonShortcuts, "The button 'Shortcuts' could not be clicked.");
    }
    public void clickButtonMyAccount() throws Exception {
        clickItem(buttonMyAccount, "The button 'My account' could not be clicked.");
    }
    public ContentPage clickButtonContent() throws Exception {
        waitUntilElementIsClickable(buttonContent);
        clickItem(buttonContent, "The button 'Content' could not be clicked.");
        waitForPageToLoad();
        return new ContentPage(webDriver, locations);
    }
    public ContentPage clickButtonFindContent() throws Exception {
        waitUntilElementIsClickable(buttonFindContent);
        clickItem(buttonFindContent, "The button 'Find content' could not be clicked.");
        return new ContentPage(webDriver, locations);
    }
    public StructurePage clickButtonStructure() throws Exception {
        waitUntilElementIsClickable(buttonStructure);
        clickItem(buttonStructure, "The button 'Structure' could not be clicked.");
        waitForPageToLoad();
        return new StructurePage(webDriver, locations);
    }
    public PeoplePage clickButtonPeople() throws Exception {
        clickItem(buttonPeople, "The button 'People' could not be clicked.");
        return new PeoplePage(webDriver, locations);
    }
    public QuizPage clickButtonQuiz() throws Exception {
        clickItem(buttonQuiz, "The button 'Quiz' could not be clicked.");
        return new QuizPage(webDriver, locations);
    }
    public AddContentPage clickButtonAddContent() throws Exception {
        clickItem(buttonAddContent, "The button 'Add content' could not be clicked.");
        return new AddContentPage(webDriver, locations);
    }
    public void clickButtonViewProfile() throws Exception {
        clickItem(buttonViewProfile, "The button 'View profile' could not be clicked.");
    }
    public WebElement getButtonMyAccount() {
        return buttonMyAccount;
    }
    public void clickButtonLogOut() throws Exception {
        clickItem(buttonLogOut, "The button 'Log out' could not be clicked.");
    }

}
