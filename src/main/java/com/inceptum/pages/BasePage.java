package com.inceptum.pages;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.jayway.restassured.response.Response;
import static com.jayway.restassured.RestAssured.given;

import com.inceptum.enumeration.PageLocation;


/**
* This is an abstract class representing a page and its user interface, while
* making use of the Page Object Pattern.
* http://code.google.com/p/selenium/wiki/PageObjects
*/
public class BasePage {

    protected String location;

    /* ----- FIELDS ----- */
    protected WebDriver webDriver;
    protected PageLocation locations;

    /* ----- WEBELEMENTS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.LINK_TEXT, using = "Edit page")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement tabAdvancedEdit;
    @FindBy(how = How.ID, using = "block-iminds-academy-class-iminds-academy-class-menu")
    private WebElement menuClass;
    @FindBy(how = How.ID, using = "modal-content")
    private WebElement popupModalContent;
    @FindBy(how = How.CSS, using = ".field__items .title")
    private WebElement fieldMaterial;
    @FindBy(how = How.CSS, using = "#quickedit-field-form")
    private WebElement popupQuickEdit;
    @FindBy(how = How.CSS, using = "iframe[class='media-youtube-player']")
    private WebElement iframeYoutubeVideo;
    @FindBy(how = How.CSS, using = "iframe[class='media-vimeo-player']")
    private WebElement iframeVimeoVideo;
    @FindBy(how = How.ID, using = "edit-masquerade-user-field")
    private WebElement fieldViewSiteAs;
    @FindBy(how = How.CSS, using = "[id*='edit-submit'][value*='Go']")
    private WebElement buttonGo;
    @FindBy(how = How.CSS, using = "[id*='masquerade-block'] a[href*='masquerade']")
    private WebElement linkSwitchBack;
    @FindBy(how = How.ID, using = "edit-roles")
    private WebElement fildsetRoles;
    @FindBy(how = How.CSS, using = "#edit-submit[value='Save']")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Groups audience")
    private WebElement linkGroupsAudience;
    @FindBy(how = How.ID, using = "edit-og-group-ref-und-0-default")
    private WebElement fieldYourGroups;
    @FindBy(how = How.CSS, using = ".button a[href*='class']")
    private WebElement buttonClassMenu;
    @FindBy(how = How.ID, using = "edit-copyto")
    private WebElement fieldCopyPage;
    @FindBy(how = How.ID, using = "edit-moveto")
    private WebElement fieldMovePage;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement messageInfo;
    @FindBy(how = How.XPATH, using = "//option[@selected=\"selected\" and text()=\"Draft\"]")
    private WebElement textDraft;
    @FindBy(how = How.TAG_NAME, using = "body")
    private WebElement bodyOfPage;


    /* ----- CONSTRUCTORS ------ */

    /**
     * Constructor injecting the WebDriver interface.
     *
     * @param webDriver The {@link WebDriver}
     */

    public BasePage(WebDriver webDriver, PageLocation locations) throws Exception {
        this.webDriver = webDriver;
        this.locations = locations;
        PageFactory.initElements(webDriver, this);

        // Check if we are on the right page
        if(location != null) {
            if(webDriver.getCurrentUrl() != locations.getFullUrl(location)) {
                throw new Exception("Expected to be on url: "+locations.getFullUrl(location)+", but we are on page: "+webDriver.getCurrentUrl());
            }
        }
    }


    /* ----- METHODS ------ */


    /**
     * Gets the {@link WebDriver} instance.
     *
     * @return The {@link WebDriver}
     */
    public WebDriver getWebDriver() {
        return webDriver;
    }
    public void clickItem(WebElement item, String errorMessage) throws Exception {
        try {
            item.click();
        }
        catch (Exception e) {
            System.err.println(errorMessage);
            throw e;
        }
    }
    public void waitForPageToLoad() throws Exception {
        ExpectedCondition < Boolean > pageLoad = new
                ExpectedCondition < Boolean > () {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        try {
            wait.until(pageLoad);
        } catch (Throwable pageLoadWaitError) {
            Assert.assertFalse("Timeout during page load", true);
        }
    }
    public void waitUntilActionWithThrobberDone() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".throbber")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".throbber")));
    }
    public void waitUntilElementIsVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch(org.openqa.selenium.TimeoutException e) {
            wait.until(ExpectedConditions.visibilityOf(element));
        }
        
    }
    public void waitUntilElementIsClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch(org.openqa.selenium.TimeoutException e) {
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getLinkEdit() {
        waitUntilElementIsVisible(linkEdit);
        return linkEdit;
    }
    public WebElement getTextDraft() {
        waitUntilElementIsVisible(textDraft);
        return textDraft;
    }
    public WebElement getTabAdvancedEdit() {
        return tabAdvancedEdit;
    }
    public String getItemNodeId(WebElement field) {
        waitUntilElementIsVisible(field);
        String value = field.getAttribute("value");
        String itemNodeId = StringUtils.substringBetween(value, "(", ")");
        return itemNodeId;
    }
    public MediaPopupPage clickLinkMaterial() throws Exception {
        Thread.sleep(1000); // for generating 'viewed' statement for the material after statement for its course component
        waitUntilElementIsClickable(fieldMaterial);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, fieldMaterial);
        return new MediaPopupPage(webDriver, locations);
    }
    public void assertTextOfMessage(WebElement message, String text) throws Exception {
        waitUntilElementIsVisible(message);
        String messageText = message.getText();
        Assert.assertTrue("Detected incorrect message: " + messageText, messageText.matches("(?s)" + text));
    }
    public WebElement getMenuClass() {
        return menuClass;
    }
    public void scrollToElement(WebElement element) throws Exception { // use the method to focus on element
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
    }
    public void waitCondition(ExpectedCondition<Boolean> condition) {
        WebDriverWait wait = new WebDriverWait(webDriver, 15);
        wait.until(condition);
    }
    public WebElement getPopupModalContent() {
        return popupModalContent;
    }
    public void waitUntilLoading() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Loading')]")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//div[contains(text(), 'Loading')]")));
    }
    public WebElement getFieldMaterial() {
        return fieldMaterial;
    }
    public WebElement getPopupQuickEdit() {
        return popupQuickEdit;
    }
    public WebElement getIframeYoutubeVideo() {
        return iframeYoutubeVideo;
    }
    public WebElement getIframeVimeoVideo() {
        return iframeVimeoVideo;
    }
    public WebElement getFieldViewSiteAs() {
        return fieldViewSiteAs;
    }
    public void autoSelectItemFromList(WebElement field, String itemName) throws Exception {
        field.sendKeys(itemName);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#autocomplete")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#autocomplete ul li:first-child div")));
        WebElement autocomplete = webDriver.findElement(By.cssSelector("#autocomplete ul li:first-child div"));
        autocomplete.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#autocomplete")));
        Thread.sleep(500); // test fails without the timeout
    }
    public void clickButtonGoMasquerading() throws Exception {
        waitUntilElementIsClickable(buttonGo); // submit masquerading
        clickItem(buttonGo, "Button Go for masquerading as user could not be clicked.");
    }
    public void clickLinkSwitchBack() throws Exception {
        waitUntilElementIsClickable(linkSwitchBack); // submit masquerading
        clickItem(linkSwitchBack, "Link 'Switch back' from masquerading as user could not be clicked.");
    }
    public void selectRole(String role) {
        WebElement checkbox = fildsetRoles.findElement(By.xpath("./div/label[contains(text(), '" + role + "')]/../input"));
        checkbox.click();
    }
    public WebElement getButtonSaveTop() {
        return buttonSaveTop;
    }
    public void checkProperClassInGroupAudience(String classExpected) throws Exception {
        waitForPageToLoad();
        waitUntilElementIsVisible(buttonSaveTop);
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' could not be clicked.");
        String classActual;
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.isSelected()) {
                classActual = option.getText();
                Assert.assertTrue(classActual.equals(classExpected));
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public void clickButtonClassMenu() throws Exception {
        waitUntilElementIsClickable(buttonClassMenu);
        clickItem(buttonClassMenu, "Class link could not be clicked.");
        OverviewPage overviewPage = new OverviewPage(webDriver, locations);
        waitUntilElementIsVisible(overviewPage.getFieldClass());
    }
    public WebElement getButtonClassMenu() {
        waitUntilElementIsVisible(buttonClassMenu);
        return buttonClassMenu;
    }
    public WebElement getFieldCopyPage() {
        return fieldCopyPage;
    }
    public WebElement getFieldMovePage() {
        return fieldMovePage;
    }
    public WebElement getMessageInfo() {
        return messageInfo;
    }
    protected void checkAlert() {
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = webDriver.switchTo().alert();
            alert.accept();
            Thread.sleep(1000);
        } catch (Exception e) {
            //exception handling
        }
    }

    public String getUrlNode(){
        String value="";
        String classValue=bodyOfPage.getAttribute("class");
        String[] parts=classValue.split(" ");
        for(String element:parts){
            if (element.contains("page-node-")&&10<element.length()&&element.length()<14){ value=element;}
        }
        String[] number=value.split("-");
        return number[2];
    }

    public void checkHttpResponseCode(String url) {
        Response response =
                given().get(url)
                        .then().extract().response();

        System.out.println(response.getStatusCode());
    }





}
