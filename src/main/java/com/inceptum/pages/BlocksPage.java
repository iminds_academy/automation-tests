package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class BlocksPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "blocks")
    private WebElement tableBlocks;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveBlocks;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;


    /*---------CONSTRUCTORS--------*/

    public BlocksPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTableBlocks() {
        return tableBlocks;
    }
    public void clickButtonSaveBlocks() throws Exception {
        waitUntilElementIsClickable(buttonSaveBlocks);
        clickItem(buttonSaveBlocks, "The button 'Save blocks' on Blocks page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }

}
