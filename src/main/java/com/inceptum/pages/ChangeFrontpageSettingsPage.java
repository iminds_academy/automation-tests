package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;


public class ChangeFrontpageSettingsPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-blocks-blocks-iaf-block-text")
    private WebElement buttonAddTextBlock;
    @FindBy(how = How.ID, using = "edit-blocks-containers-iaf-block-two-columns")
    private WebElement buttonAddTwoColumns;
    @FindBy(how = How.ID, using = "edit-blocks-blocks-iaf-block-lead")
    private WebElement buttonAddLeadBlock;
    @FindBy(how = How.ID, using = "edit-blocks-blocks-iaf-block-feedback")
    private WebElement buttonAddFeedbackBlock;
    @FindBy(how = How.ID, using = "edit-blocks-blocks-iaf-block-experts")
    private WebElement buttonAddMeetExpertsBlock;
    @FindBy(how = How.ID, using = "edit-blocks-blocks-video")
    private WebElement buttonAddVideoBlock;
    @FindBy(how = How.ID, using = "edit-blocks-blocks-wyntk")
    private WebElement buttonAddWhatYouNeedToKnowBlock;
    @FindBy(how = How.CSS, using = "#edit-submit[value='Submit']")
    private WebElement buttonSubmit;
    @FindBy(how = How.ID, using = "frontpage-table")
    private WebElement table;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".tabledrag-changed-warning.messages.warning")
    private WebElement messageTabledrag;


    /* ----- CONSTRUCTORS ----- */

    public ChangeFrontpageSettingsPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public WebElement getButtonAddTextBlock() {
        return buttonAddTextBlock;
    }
    public WebElement getButtonAddTwoColumns() {
        return buttonAddTwoColumns;
    }
    public WebElement getButtonAddLeadBlock() {
        return buttonAddLeadBlock;
    }
    public WebElement getButtonAddFeedbackBlock() {
        return buttonAddFeedbackBlock;
    }
    public WebElement getButtonAddMeetExpertsBlock() {
        return buttonAddMeetExpertsBlock;
    }
    public WebElement getButtonAddVideoBlock() {
        return buttonAddVideoBlock;
    }
    public WebElement getButtonAddWhatYouNeedToKnowBlock() {
        return buttonAddWhatYouNeedToKnowBlock;
    }
    public void clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The Submit button on the 'Change frontpage settings' page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getTable() {
        return table;
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getMessageTabledrag() {
        return messageTabledrag;
    }

}
