package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CloneOfGroupPage extends CreateGroupPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "#edit-submit[value='Save']")
    private WebElement buttonSave;


    /*---------CONSTRUCTORS--------*/
    public CloneOfGroupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public ViewGroupPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The button Save on the 'Clone of Group' page could not be clicked.");
        waitForPageToLoad();
        return new ViewGroupPage(webDriver, locations);
    }


}
