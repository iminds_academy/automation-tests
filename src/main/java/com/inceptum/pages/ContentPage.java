package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class ContentPage extends BasePage {


    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Add content")
    private WebElement linkAddContent;
    @FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[1]/td[2]/a")
    private WebElement titleOf1stElement;
    @FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[1]/td[1]/div/input")
    private WebElement checkbox1stElement;
    @FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[1]/*/ul/li[1]/a")
    private WebElement iconEditFor1stElement;
    @FindBy(how = How.XPATH, using = "//table[2]/tbody/tr[1]/*/ul/li[2]/a")
    private WebElement iconDeleteFor1stElement;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement messageUpdateOptions;
    @FindBy(how = How.ID, using = "edit-operation")
    private WebElement fieldUpdateOptions;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonUpdate;
    @FindBy(how = How.XPATH, using = "//*[@id=\"tab-bar\"]/ul/li[1]/a")
    private WebElement linkContent;
    @FindBy(how = How.ID, using = "edit-type")
    private WebElement fieldType;
    @FindBy(how = How.CSS, using = "#edit-submit[value='Filter']")
    private WebElement buttonFilter;
    @FindBy(how = How.ID, using = "edit-reset")
    private WebElement buttonReset;
    @FindBy(how = How.CSS, using = ".sticky-table input[title='Select all rows in this table']")
    private WebElement checkboxSelectAll;


    /*---------CONSTRUCTORS--------*/

    public ContentPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /*---------METHODS----------*/

    public  AddContentPage clickLinkAddContent() throws Exception {
        waitUntilElementIsVisible(linkAddContent);
        clickItem(linkAddContent, "The link 'Add content' on the Content page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("modalContent")));
        return new AddContentPage(webDriver, locations);
    }
    public WebElement getLinkAddContent() {
        return linkAddContent;
    }
    public WebElement getIconEditFor1stElement() {
        return iconEditFor1stElement;
    }
    public void clickIconEditFor1stElement() throws Exception {
        waitUntilElementIsClickable(iconEditFor1stElement);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconEditFor1stElement.sendKeys(Keys.RETURN);
        } else clickItem(iconEditFor1stElement, "The icon 'Edit' for the 1st element on the Content page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getCheckbox1stElement() {
        return checkbox1stElement;
    }
    public WebElement getIconDeleteFor1stElement() {
        return iconDeleteFor1stElement;
    }
    public DeleteItemConfirmationPage clickIconDeleteFor1stElement() throws Exception {
        waitUntilElementIsClickable(iconDeleteFor1stElement);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE
            iconDeleteFor1stElement.sendKeys(Keys.RETURN);
        } else clickItem(iconDeleteFor1stElement, "The icon 'Delete' for the 1st element on the Content page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public WebElement getTitleOf1stElement() {
        return titleOf1stElement;
    }
    public void clickLinkTitleOf1stElement() throws Exception {
        waitUntilElementIsClickable(titleOf1stElement);
        clickItem(titleOf1stElement, "The link 'Title' for the 1st element on the Content page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getMessageUpdateOptions() {
        return messageUpdateOptions;
    }
    public WebElement getFieldUpdateOptions() {
        return fieldUpdateOptions;
    }
    public DeleteItemConfirmationPage clickButtonUpdate() throws Exception {
        waitUntilElementIsClickable(buttonUpdate);
        // Execute the script to click on the button
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, buttonUpdate);
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public WebElement getLinkContent() throws Exception {
        waitUntilElementIsClickable(linkContent);
        return linkContent;
    }
    public WebElement getFieldType() {
        return fieldType;
    }
    public  void clickButtonFilter() throws Exception {
        waitUntilElementIsVisible(buttonFilter);
        clickItem(buttonFilter, "The button 'Filter' on the Content page could not be clicked.");
        waitUntilElementIsVisible(buttonReset);
    }
    public WebElement getButtonReset() {
        return buttonReset;
    }
    public  void clickButtonReset() throws Exception {
        waitUntilElementIsVisible(buttonReset);
        clickItem(buttonReset, "The button Reset on the Content page could not be clicked.");
        waitForPageToLoad();
        waitUntilElementIsVisible(buttonFilter);
    }
    public  void clickCheckboxSelectAll() throws Exception {
        waitUntilElementIsVisible(checkboxSelectAll);
        clickItem(checkboxSelectAll, "The checkbox SelectAll on the Content page could not be clicked.");
    }

}
