package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreateClassPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldName;
    @FindBy(how = How.ID, using = "edit-field-class-location-und-0-value")
    private WebElement fieldLocation;
    @FindBy(how = How.ID, using = "edit-field-class-capacity-und-0-value")
    private WebElement fieldCapacity;
    @FindBy(how = How.ID, using = "edit-field-registration-approval-und-0")
    private WebElement radiobuttonRegistrationDirectAccess;
    @FindBy(how = How.ID, using = "edit-field-registration-approval-und-1")
    private WebElement radiobuttonRegistrationNeedApproval;
    @FindBy(how = How.ID, using = "edit-field-class-registration-state-und-open")
    private WebElement radiobuttonOpenForRegistration;
    @FindBy(how = How.ID, using = "edit-field-class-registration-state-und-closed")
    private WebElement radiobuttonClosedForRegistration;
    @FindBy(how = How.ID, using = "edit-field-class-registration-state-und-automatic")
    private WebElement radiobuttonAutomaticRegistration;
    @FindBy(how = How.ID, using = "edit-field-class-registration-state-und-private")
    private WebElement radiobuttonPrivateRegistration;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-field-class-date-und-0-value2-datepicker-popup-0")
    private WebElement fieldEndDate;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'End Date')]")
    private WebElement labelEndDate;
    @FindBy(how = How.ID, using = "edit-field-class-date-und-0-value-datepicker-popup-0")
    private WebElement fieldStartDate;
    @FindBy(how = How.ID, using = "edit-field-registration-deadline-und-0-value-datepicker-popup-0")
    private WebElement fieldDeadlineStartDate;
    @FindBy(how = How.ID, using = "edit-field-registration-contact-und-0-email")
    private WebElement fieldRegistrationContact;
    @FindBy(how = How.XPATH, using = "//*[@id='edit-field-class-registration-state']/..")
    private WebElement fieldsetRegistrationSettings;
    @FindBy(how = How.CSS, using = "#edit-registration-settings a")
    private WebElement linkRegistrationSettings;
    @FindBy(how = How.ID, using = "edit-field-registration-approval-und")
    private WebElement checkboxUserNeedApproval;


    /*---------CONSTRUCTORS--------*/
    public CreateClassPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldName(String name) throws Exception {
        waitUntilElementIsVisible(fieldName);
        fieldName.sendKeys(name);
    }
    public void clickLabelEndDate() throws Exception {
        waitUntilElementIsClickable(labelEndDate);
        clickItem(labelEndDate, "The label 'End Date' on the CreateClassPage could not be clicked.");
    }
    public void fillFieldLocation(String location) throws Exception {
        waitUntilElementIsVisible(fieldLocation);
        fieldLocation.sendKeys(location);
    }
    public void fillFieldCapacity(String capacity) {
        waitUntilElementIsVisible(fieldCapacity);
        fieldCapacity.clear();
        fieldCapacity.sendKeys(capacity);
    }
    public void selectOptionOpenForRegistration() {
        waitUntilElementIsClickable(radiobuttonOpenForRegistration);
        radiobuttonOpenForRegistration.click();
        Assert.assertTrue(radiobuttonOpenForRegistration.isSelected());
    }
    public void selectOptionClosedForRegistration() {
        waitUntilElementIsClickable(radiobuttonClosedForRegistration);
        radiobuttonClosedForRegistration.sendKeys(Keys.SPACE);
        Assert.assertTrue(radiobuttonClosedForRegistration.isSelected());
    }
    public void selectOptionAutomaticRegistration() {
        waitUntilElementIsClickable(radiobuttonAutomaticRegistration);
        radiobuttonAutomaticRegistration.click();
        Assert.assertTrue(radiobuttonAutomaticRegistration.isSelected());
    }
    public void selectOptionPrivateRegistration() {
        waitUntilElementIsClickable(radiobuttonPrivateRegistration);
        radiobuttonPrivateRegistration.click();
        Assert.assertTrue(radiobuttonPrivateRegistration.isSelected());
    }
    public void userNeedsApprovalBeforeJoiningClass(boolean approval) throws Exception { // default value == false (doesn't need approval)
        if (approval==true) { // if approval is needed
            if (!(checkboxUserNeedApproval.isSelected())) { // and proper checkbox is not selected
                // Select the checkbox
                clickItem(checkboxUserNeedApproval, "The checkbox 'User need to get approval..' could not be clicked.");
                Assert.assertTrue("The checkbox 'User need to get approval..' is not selected.",
                        checkboxUserNeedApproval.isSelected());
            }
        } else { // if approval is not needed
            if (checkboxUserNeedApproval.isSelected()) { // and proper checkbox is selected
                // Click the checkbox to uncheck it
                clickItem(checkboxUserNeedApproval, "The checkbox 'User need to get approval..' could not be clicked.");
                Assert.assertTrue("The checkbox 'User need to get approval..' is selected.",
                        (!(checkboxUserNeedApproval.isSelected())));
            }
        }
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateClassPage could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getFieldEndDate() {
        return fieldEndDate;
    }
    public WebElement getFieldStartDate() {
        return fieldStartDate;
    }
    public WebElement getFieldDeadlineStartDate() {
        return fieldDeadlineStartDate;
    }
    public void fillFieldRegistrationContact(String email) throws Exception {
        waitUntilElementIsVisible(fieldRegistrationContact);
        fieldRegistrationContact.sendKeys(email);
    }
    public WebElement getFieldsetRegistrationSettings() {
        return fieldsetRegistrationSettings;
    }
    public void clickLinkRegistrationSettings() throws Exception {
        waitUntilElementIsClickable(linkRegistrationSettings);
        clickItem(linkRegistrationSettings, "The link RegistrationSettings on the CreateClassPage could not be clicked.");
        waitForPageToLoad();
    }

}
