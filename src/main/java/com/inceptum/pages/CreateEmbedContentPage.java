package com.inceptum.pages;

import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateEmbedContentPage extends BasePage{
    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHours;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMin;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Educational content")
    private WebElement tabEducationalContent;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_1_contents\"]/iframe")
    private WebElement frameVideoSummary;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_2_contents\"]/iframe")
    private WebElement frameContextualInformation;
    @FindBy(how = How.ID, using = "edit-field-iframe-und-0-url")
    private WebElement fieldIFrameURL;
    @FindBy(how = How.ID, using = "edit-field-iframe-und-0-title")
    private WebElement fieldIframeTitle;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;




    /*---------CONSTRUCTORS--------*/

    public CreateEmbedContentPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String EmbedContentTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(EmbedContentTitle);
    }
    public WebElement getFieldLengthHours() {
        return fieldLengthHours;
    }
    public WebElement getFieldLengthMin() {
        return fieldLengthMin;
    }
    public void switchToTabEducationalContent() throws Exception {
        clickItem(tabEducationalContent, "The EducationalContentTab on the CreateTask page could not be clicked.");
    }
    public WebElement getFrameVideoSummary() {
        return frameVideoSummary;
    }
    public WebElement getFrameContextualInformation() {
        return frameContextualInformation;
    }

    public void fillFieldIframeURL (String iFrameURLText) {
        waitUntilElementIsVisible(fieldIFrameURL);
        fieldIFrameURL.clear();
        fieldIFrameURL.sendKeys(iFrameURLText);
    }
    public void fillFieldIframeTitle (String iFrameTitleText) {
        waitUntilElementIsVisible(fieldIframeTitle);
        fieldIframeTitle.clear();
        fieldIframeTitle.sendKeys(iFrameTitleText);
    }
    public ViewEmbedContentPage clickButtonSaveBottom() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateTask page could not be clicked.");
        waitForPageToLoad();
        return new ViewEmbedContentPage(webDriver, locations);
    }




}