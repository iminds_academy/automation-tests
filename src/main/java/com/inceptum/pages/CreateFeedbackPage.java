package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreateFeedbackPage extends HomePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-feedback-session-name")
    private WebElement fieldFeedbackSessionName;
    @FindBy(how = How.ID, using = "edit-class-select")
    private WebElement fieldClass;
    @FindBy(how = How.ID, using = "edit-template-select")
    private WebElement fieldFeedbackContents;
    @FindBy(how = How.ID, using = "edit-groups-0-group-select")
    private WebElement fieldGroups;
    @FindBy(how = How.ID, using = "edit-groups-1-group-select")
    private WebElement fieldGroups2;
    @FindBy(how = How.ID, using = "edit-juries-0-feedback-jury-members")
    private WebElement fieldJuryMembers;
    @FindBy(how = How.ID, using = "edit-juries-1-feedback-jury-members")
    private WebElement fieldJuryMembers2;
    @FindBy(how = How.ID, using = "edit-feedback-submit")
    private WebElement buttonSave;
    @FindBy(how = How.CSS, using = ".messages.messages--error")
    private WebElement messageError;


    /* ----- CONSTRUCTORS ----- */

    public CreateFeedbackPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public WebElement getFieldFeedbackSessionName() {
        return fieldFeedbackSessionName;
    }
    public void fillFieldFeedbackSessionName(String name) throws Exception {
        waitUntilElementIsVisible(fieldFeedbackSessionName);
        fieldFeedbackSessionName.sendKeys(name);
    }
    public WebElement getFieldClass() {
        return fieldClass;
    }
    public WebElement getFieldFeedbackContents() {
        return fieldFeedbackContents;
    }
    public WebElement getFieldGroups() {
        return fieldGroups;
    }
    public void clearFieldGroups() {
        waitUntilElementIsVisible(fieldGroups);
        fieldGroups.clear();
    }
    public WebElement getFieldGroups2() {
        return fieldGroups2;
    }
    public WebElement getFieldJuryMembers() {
        return fieldJuryMembers;
    }
    public void clearFieldJuryMembers() {
        waitUntilElementIsVisible(fieldJuryMembers);
        fieldJuryMembers.clear();
    }
    public WebElement getFieldJuryMembers2() {
        return fieldJuryMembers2;
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateFeedback page could not be clicked.");
        waitForPageToLoad();
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, locations);
        waitUntilElementIsVisible(overviewFeedbackPage.getButtonNewFeedbackSession());
    }
    public WebElement getButtonSave() {
        return buttonSave;
    }
    public WebElement getMessageError() {
        return messageError;
    }

}
