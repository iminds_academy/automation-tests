package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreateGroupPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;



    /*---------CONSTRUCTORS--------*/
    public CreateGroupPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }

    public ViewGroupPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateGroup page could not be clicked.");
        waitForPageToLoad();
        return new ViewGroupPage(webDriver, locations);
    }
}
