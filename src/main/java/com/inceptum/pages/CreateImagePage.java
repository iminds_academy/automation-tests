package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateImagePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "[id*='image'] #edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select media")
    private WebElement linkSelectMedia;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-image-und-1')]/a[1]")
    private WebElement linkSelectMediaFor2ndImage;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-image-und-2')]/a[1]")
    private WebElement linkSelectMediaFor3dImage;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Remove media")
    private WebElement linkRemoveMedia;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-image-und-1')]/a[2]")
    private WebElement linkRemoveMediaFor2ndImage;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-image-und-2')]/a[2]")
    private WebElement linkRemoveMediaFor3dImage;
    @FindBy(how = How.ID, using = "edit-field-image-und-add-more")
    private WebElement buttonAddAnotherItem;
    @FindBy(how = How.ID, using = "edit-field-image-und-add-more--2")
    private WebElement buttonAddAnotherItem2;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;
    @FindBy(how = How.XPATH, using = "//*[@id=\"console\"]/div/ul/li[1]")
    private WebElement errorMessageTitle;
    @FindBy(how = How.XPATH, using = "//*[@id=\"console\"]/div/ul/li[2]")
    private WebElement errorMessageImage;
    @FindBy(how = How.XPATH, using = "//*[@id=\"field-image-values\"]/tbody/tr[1]/td[1]/a/div")
    private WebElement firstImageItem;
    @FindBy(how = How.XPATH, using = "//*[@id=\"field-image-values\"]/tbody/tr[2]/td[1]/a/div")
    private WebElement secondImageItem;
    @FindBy(how = How.XPATH, using = "//*[@id=\"field-image-add-more-wrapper\"]/div/div[2]")
    private WebElement messageReordering;



    /*---------CONSTRUCTORS--------*/
    public CreateImagePage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/
    public void fillFieldTitle(String imageTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(imageTitle);
    }
    public WebElement getFieldTitle() throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public ViewImagePage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            buttonSave.sendKeys(Keys.RETURN);
        } else clickItem(buttonSave, "The Save button on the CreateImage page could not be clicked.");
        waitForPageToLoad();
        return new ViewImagePage(webDriver, locations);
    }
    public WebElement getButtonSave() {
        return buttonSave;
    }
    public ViewImagePage clickButtonSaveTop() throws Exception {
        waitUntilElementIsClickable(buttonSaveTop);
        clickItem(buttonSaveTop, "The Save button at the top of the CreateImage page could not be clicked.");
        waitForPageToLoad();
        return new ViewImagePage(webDriver, locations);
    }
    public SelectImagePage clickLinkSelectMedia() throws Exception {
        clickItem(linkSelectMedia, "The 'Select media' link on the CreateImage page could not be clicked.");
        return new SelectImagePage(webDriver, locations);
    }
    public WebElement getLinkSelectMediaFor2ndImage() {
        return linkSelectMediaFor2ndImage;
    }
    public SelectImagePage clickLinkSelectMediaFor2ndImage() throws Exception {
        waitUntilElementIsClickable(linkSelectMediaFor2ndImage);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            linkSelectMediaFor2ndImage.sendKeys(Keys.RETURN);
        } else clickItem(linkSelectMediaFor2ndImage, "The 'Select media' link for the 2nd image on the CreateImage page could not be clicked.");
        return new SelectImagePage(webDriver, locations);
    }
    public WebElement getLinkSelectMediaFor3dImage() {
        return linkSelectMediaFor3dImage;
    }
    public SelectImagePage clickLinkSelectMediaFor3dImage() throws Exception {
        waitUntilElementIsClickable(linkSelectMediaFor3dImage);
        clickItem(linkSelectMediaFor3dImage, "The 'Select media' link for the 3d image on the CreateImage page could not be clicked.");
        return new SelectImagePage(webDriver, locations);
    }
    public void clickButtonAddAnotherItem() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            buttonAddAnotherItem.sendKeys(Keys.RETURN);
        } else clickItem(buttonAddAnotherItem, "The 'Add another item' button on the CreateImage page could not be clicked.");
    }
    public void clickButtonAddAnotherItem2() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem2);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            buttonAddAnotherItem2.sendKeys(Keys.RETURN);
        } else clickItem(buttonAddAnotherItem2, "The 'Add another item' button for the 3d image on the CreateImage page could not be clicked.");
    }
    public WebElement getLinkRemoveMedia() {
        return linkRemoveMedia;
    }
    public void clickLinkRemoveMedia() throws Exception {
        waitUntilElementIsClickable(linkRemoveMedia);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            linkRemoveMedia.sendKeys(Keys.RETURN);
        } else clickItem(linkRemoveMedia, "The 'Remove media' link on the CreateImage page could not be clicked.");
    }
    public WebElement getLinkRemoveMediaFor2ndImage() {
        return linkRemoveMediaFor2ndImage;
    }
    public void clickLinkRemoveMediaFor2ndImage() throws Exception {
        waitUntilElementIsClickable(linkRemoveMediaFor2ndImage);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            linkRemoveMediaFor2ndImage.sendKeys(Keys.RETURN);
        } else clickItem(linkRemoveMediaFor2ndImage, "The 'Remove media' link for the 2nd image on the CreateImage page could not be clicked.");
    }
    public WebElement getLinkRemoveMediaFor3dImage() {
        return linkRemoveMediaFor3dImage;
    }
    public WebElement getMessage() {
        return message;
    }

    public void assertErrorMessage(String messageTitle, String messageImage) {
        waitUntilElementIsVisible(message);
        Assert.assertEquals("messages error", message.getAttribute("class"));
        // Check correct text of error message
        String errorImageMessageTitle = errorMessageTitle.getText();
        String errorImageMessageImage = errorMessageImage.getText();
        Assert.assertTrue("Detected incorrect message: " + errorImageMessageTitle, errorImageMessageTitle.matches(messageTitle));
        Assert.assertTrue("Detected incorrect message: " + errorImageMessageImage, errorImageMessageImage.matches("(?s)" + messageImage));
    }
    public WebElement getFirstImageItem() {
        return firstImageItem;
    }
    public WebElement getSecondImageItem() {
        return secondImageItem;
    }
    public WebElement getMessageReordering() {
        return messageReordering;
    }

}
