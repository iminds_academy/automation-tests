package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class CreateInfoPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-summary-und-0-value")
    private WebElement fieldSummary;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-und")
    private WebElement fieldChapter;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-field-info-sections-und-0-field-info-section-subtitle-und-0-value")
    private WebElement fieldSubtitle;
    @FindBy(how = How.ID, using = "edit-field-info-sections-und-1-field-info-section-subtitle-und-0-value")
    private WebElement fieldSubtitle2;
    @FindBy(how = How.CSS, using = "[id*='0-field-info-section-description'] iframe")
    private WebElement frameDescription;
    @FindBy(how = How.CSS, using = "[id*='1-field-info-section-description'] iframe")
    private WebElement frameDescription2;
    @FindBy(how = How.ID, using = "edit-field-info-sections-und-0-remove-button")
    private WebElement buttonRemove;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement errorMessage;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Educational content")
    private WebElement tabEducationalContent;


    /*---------CONSTRUCTORS--------*/

    public CreateInfoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String infoPageTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(infoPageTitle);
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public void fillFieldSummary (String summaryText) {
        waitUntilElementIsVisible(fieldSummary);
        fieldSummary.clear();
        fieldSummary.sendKeys(summaryText);
    }
    public WebElement getFieldSummary() {
        waitUntilElementIsVisible(fieldSummary);
        return fieldSummary;
    }
    public WebElement getFieldChapter() {
        return fieldChapter;
    }
    public ViewInfoPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateInfoPage could not be clicked.");
        waitForPageToLoad();
        return new ViewInfoPage(webDriver, locations);
    }
    public void fillFieldSubtitle (String subtitleText) {
        waitUntilElementIsVisible(fieldSubtitle);
        fieldSubtitle.clear();
        fieldSubtitle.sendKeys(subtitleText);
    }
    public WebElement getFieldSubtitle2() {
        return fieldSubtitle2;
    }
    public void fillFieldSubtitle2 (String subtitleText) {
        waitUntilElementIsVisible(fieldSubtitle2);
        fieldSubtitle2.clear();
        fieldSubtitle2.sendKeys(subtitleText);
    }
    public WebElement getFrameDescription() {
        return frameDescription;
    }
    public WebElement getFrameDescription2() {
        return frameDescription2;
    }
    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The Remove button on the CreateInfoPage could not be clicked.");
        // Wait until the 1st section is removed
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-field-info-sections-und-1-field-info-section-subtitle-und-0-value")));
    }
    public WebElement getErrorMessage() {
        return errorMessage;
    }
    public void switchToTabEducationalContent() throws Exception {
        waitUntilElementIsClickable(tabEducationalContent);
        clickItem(tabEducationalContent, "The tab 'Educational content' on the CreateInfo page could not be clicked.");
    }


}
