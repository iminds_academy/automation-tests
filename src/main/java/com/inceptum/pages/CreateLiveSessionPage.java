package com.inceptum.pages;

import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateLiveSessionPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-summary-und-0-value")
    private WebElement fieldSummary;
    @FindBy(how = How.ID, using = "edit-field-live-session-type-und")
    private WebElement fieldLiveSessionType;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-und")
    private WebElement fieldChapter;
    @FindBy(how = How.ID, using = "edit-field-live-session-type-add-entityconnect-field-live-session-type-all-")
    private WebElement iconAddLiveSessionType;
    @FindBy(how = How.ID, using = "edit-field-live-session-type-edit-entityconnect-field-live-session-type-all-")
    private WebElement iconEditLiveSessionType;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-add-entityconnect-field-material-chapter-all-")
    private WebElement iconAddChapter;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-edit-entityconnect-field-material-chapter-all-")
    private WebElement iconEditChapter;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-add-entityconnect-field-presentations-course-0-")
    private WebElement iconAddPresentation;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-edit-entityconnect-field-presentations-course-0-")
    private WebElement iconEditPresentation;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-add-entityconnect-field-videos-course-0-")
    private WebElement iconAddVideo;
    @FindBy(how = How.CSS, using = "[id*='live-session'] #edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Educational content")
    private WebElement tabEducationalContent;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Course material")
    private WebElement tabCourseMaterial;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Additional material")
    private WebElement tabAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-task-link-und-0-title")
    private WebElement fieldTaskLinkTitle;
    @FindBy(how = How.ID, using = "edit-field-task-link-und-0-url")
    private WebElement fieldTaskLinkURL;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_1_contents\"]/iframe")
    private WebElement frameWhatDoYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_2_contents\"]/iframe")
    private WebElement framePlanning;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-target-id")
    private WebElement fieldVideosCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-target-id")
    private WebElement fieldImagesCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-target-id")
    private WebElement fieldPresentationsCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-title-course-und-0-value")
    private WebElement fieldDownloadsTitle;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-title-additional-und-0-value")
    private WebElement fieldDownloadsTitleAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload")
    private WebElement fieldDownloadsFile;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload")
    private WebElement fieldDownloadsFileAdditional;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-add-entityconnect-field-images-course-0-")
    private WebElement iconAddImage;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-remove-button")
    private WebElement buttonRemoveFile; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-remove-button")
    private WebElement buttonRemoveFileAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload-button")
    private WebElement buttonUpload; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload-button")
    private WebElement buttonUploadAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-title")
    private WebElement fieldLinksTitle;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-0-title")
    private WebElement fieldLinksTitleAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-url")
    private WebElement fieldLinksUrl;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-0-url")
    private WebElement fieldLinksUrlAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-attributes-target")
    private WebElement checkboxOpenInNewWindow;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-show-todate")
    private WebElement checkboxUseStartAndEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-time-checker-show-time")
    private WebElement checkboxShowTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-datepicker-popup-0")
    private WebElement fieldStartDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-datepicker-popup-0")
    private WebElement fieldEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-timeEntry-popup-1")
    private WebElement fieldStartTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-timeEntry-popup-1")
    private WebElement fieldEndTime;



    /*---------CONSTRUCTORS--------*/

    public CreateLiveSessionPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String liveSessionTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(liveSessionTitle);
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public void fillFieldSummary (String summaryText) {
        waitUntilElementIsVisible(fieldSummary);
        fieldSummary.clear();
        fieldSummary.sendKeys(summaryText);
    }
    public WebElement getFieldLiveSessionType() {
        return fieldLiveSessionType;
    }
    public WebElement getFieldChapter() {
        return fieldChapter;
    }
    public AddLiveSessionTypePage clickIconAddLiveSessionType() throws Exception {
        waitUntilElementIsClickable(iconAddLiveSessionType);
        clickItem(iconAddLiveSessionType, "The AddLiveSessionType icon on the CreateLiveSession page could not be clicked.");
        waitForPageToLoad();
        return new AddLiveSessionTypePage(webDriver, locations);
    }
    public EditLiveSessionTypePage clickIconEditLiveSessionType() throws Exception {
        waitUntilElementIsClickable(iconEditLiveSessionType);
        clickItem(iconEditLiveSessionType, "The EditLiveSessionType icon on the CreateLiveSession page could not be clicked.");
        waitForPageToLoad();
        return new EditLiveSessionTypePage(webDriver, locations);
    }
    public AddChapterPage clickIconAddChapter() throws Exception {
        clickItem(iconAddChapter, "The AddChapter icon on the CreateLiveSession page could not be clicked.");
        return new AddChapterPage(webDriver, locations);
    }
    public EditChapterPage clickIconEditChapter() throws Exception {
        waitUntilElementIsClickable(iconEditChapter);
        clickItem(iconEditChapter,"The EditChapter icon on the CreateLiveSession page could not be clicked.");
        waitForPageToLoad();
        return new EditChapterPage(webDriver, locations);
    }
    public EditPresentationPage clickIconEditPresentation() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            iconEditPresentation.sendKeys(Keys.RETURN);
        } else clickItem(iconEditPresentation, "The icon 'Edit Presentation' on the EditLiveSession page could not be clicked.");
        return new EditPresentationPage(webDriver, locations);
    }
    public ViewLiveSessionPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateLiveSession page could not be clicked.");
        waitForPageToLoad();
        return new ViewLiveSessionPage(webDriver, locations);
    }
    public ViewLiveSessionPage clickButtonSaveTop() throws Exception {
        waitUntilElementIsClickable(buttonSaveTop);
        clickItem(buttonSaveTop, "The Save button at the top of the CreateLiveSession page could not be clicked.");
        waitForPageToLoad();
        return new ViewLiveSessionPage(webDriver, locations);
    }
    public WebElement getButtonSaveTop() {
        return buttonSaveTop;
    }
    public void switchToTabEducationalContent() throws Exception {
        waitUntilElementIsClickable(tabEducationalContent);
        clickItem(tabEducationalContent, "The EducationalContentTab on the CreateLiveSession page could not be clicked.");
    }
    public void switchToTabCourseMaterial() throws Exception {
        waitUntilElementIsClickable(tabCourseMaterial);
        clickItem(tabCourseMaterial, "The CourseMaterialTab on the CreateLiveSession page could not be clicked.");
    }
    public void switchToTabAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(tabAdditionalMaterial);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, tabAdditionalMaterial);
        waitForPageToLoad();
    }
    public void fillFieldTaskLinkTitle(String taskLinkTitle) {
        waitUntilElementIsVisible(fieldTaskLinkTitle);
        fieldTaskLinkTitle.sendKeys(taskLinkTitle);
    }
    public void fillFieldTaskLinkURL(String taskLinkURL) {
        waitUntilElementIsVisible(fieldTaskLinkURL);
        fieldTaskLinkURL.sendKeys(taskLinkURL);
    }
    public WebElement getFieldTaskLinkTitle() {
        return fieldTaskLinkTitle;
    }
    public WebElement getFieldTaskLinkURL() {
        return fieldTaskLinkURL;
    }
    public WebElement getFrameWhatDoYouNeedToDo() {
        return frameWhatDoYouNeedToDo;
    }
    public WebElement getFramePlanning() {
        return framePlanning;
    }
    public WebElement getFieldVideosCourseMaterial() {
        return fieldVideosCourseMaterial;
    }
    public WebElement getFieldImagesCourseMaterial() throws Exception {
        waitUntilElementIsVisible(fieldImagesCourseMaterial);
        return fieldImagesCourseMaterial;
    }
    public WebElement getFieldPresentationsCourseMaterial() {
        return fieldPresentationsCourseMaterial;
    }
    public void fillFieldDownloadsTitle(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitle);
        fieldDownloadsTitle.sendKeys(title);
    }
    public void fillFieldDownloadsTitleAdditional(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitleAdditional);
        fieldDownloadsTitleAdditional.sendKeys(title);
    }
    public void clickButtonUpload() throws Exception {
        waitUntilElementIsClickable(buttonUpload);
        clickItem(buttonUpload, "The button Upload on the CreateLiveSession page could not be clicked.");
    }
    public void clickButtonUploadAdditional() throws Exception {
        waitUntilElementIsClickable(buttonUploadAdditional);
        clickItem(buttonUploadAdditional, "The button Upload on the CreateLiveSession page could not be clicked.");
    }
    public void uploadFileToDownloads(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFile.sendKeys(path);
        clickButtonUpload();
        waitUntilElementIsVisible(buttonRemoveFile);
    }
    public void uploadFileToDownloadsAdditional(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFileAdditional.sendKeys(path);
        clickButtonUploadAdditional();
        waitUntilElementIsVisible(buttonRemoveFileAdditional);
    }
    public void fillFieldLinksTitle(String title) {
        waitUntilElementIsVisible(fieldLinksTitle);
        fieldLinksTitle.sendKeys(title);
    }
    public void fillFieldLinksTitleAdditional(String title) {
        waitUntilElementIsVisible(fieldLinksTitleAdditional);
        fieldLinksTitleAdditional.sendKeys(title);
    }
    public void fillFieldLinksUrl(String url) {
        waitUntilElementIsVisible(fieldLinksUrl);
        fieldLinksUrl.sendKeys(url);
    }
    public void fillFieldLinksUrlAdditional(String url) {
        waitUntilElementIsVisible(fieldLinksUrlAdditional);
        fieldLinksUrlAdditional.sendKeys(url);
    }
    public void selectCheckboxOpenInNewWindow() throws Exception {
        waitUntilElementIsClickable(checkboxOpenInNewWindow);
        clickItem(checkboxOpenInNewWindow, "The checkbox 'Open URL in a New Window' on the CreateLiveSession page could not be clicked.");
    }
    public CreateImagePage clickIconAddImage() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImage);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddImage.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImage, "The icon 'Add Image' on the EditLiveSession page could not be clicked.");
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return createImagePage;
    }
    public CreatePresentationPage clickIconAddPresentation() throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddPresentation);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddPresentation.sendKeys(Keys.RETURN);
        } else clickItem(iconAddPresentation, "The icon 'Add Presentation' on the EditLiveSession page could not be clicked.");
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return createPresentationPage;
    }
    public CreateVideoPage clickIconAddVideo() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideo);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddVideo.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideo, "The icon 'Add Video' on the EditLiveSession page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return createVideoPage;
    }
    public WebElement getIconAddImage() {
        return iconAddImage;
    }
    public void selectCheckboxUseStartAndEndDate() throws Exception {
        waitUntilElementIsClickable(checkboxUseStartAndEndDate);
        clickItem(checkboxUseStartAndEndDate, "The checkbox 'Use start and end date' on the CreateLiveSession page could not be clicked.");
        waitUntilElementIsVisible(fieldEndDate);
    }
    public WebElement getFieldEndDate() {
        return fieldEndDate;
    }
    public WebElement getFieldStartDate() {
        return fieldStartDate;
    }
    public void selectCheckboxShowTime() throws Exception {
        waitUntilElementIsClickable(checkboxShowTime);
        clickItem(checkboxShowTime, "The checkbox 'Show time' on the CreateLiveSession page could not be clicked.");
        waitUntilElementIsVisible(fieldStartTime);
    }
    public WebElement getFieldStartTime() {
        return fieldStartTime;
    }
    public WebElement getFieldEndTime() {
        return fieldEndTime;
    }


}
