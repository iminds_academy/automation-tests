package com.inceptum.pages;


import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateMediaPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-summary-und-0-value")
    private WebElement fieldSummary;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-und")
    private WebElement fieldChapter;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHours;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMin;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "General")
    private WebElement tabGeneral;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Educational content")
    private WebElement tabEducationalContent;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Course material")
    private WebElement tabCourseMaterial;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Additional material")
    private WebElement tabAdditionalMaterial;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select media")
    private WebElement linkSelectMedia;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-video-und-2')]/a[1]")
    private WebElement linkSelectMediaFor3dVideo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_1_contents\"]/iframe")
    private WebElement frameVideoSummary;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_2_contents\"]/iframe")
    private WebElement frameContextualInformation;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-video-und-0] a.button.remove")
    private WebElement linkRemoveMedia;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-video-und-2] a.button.remove")
    private WebElement linkRemoveMediaFor3dVideo;
    @FindBy(how = How.ID, using = "edit-field-presentations-additional-und-0-add-entityconnect-field-presentations-additional-0-")
    private WebElement iconAddPresentation;
    @FindBy(how = How.ID, using = "edit-field-presentations-additional-und-0-target-id")
    private WebElement fieldPresentationsAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-0-target-id")
    private WebElement fieldVideosAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-target-id")
    private WebElement fieldVideosCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-1-target-id")
    private WebElement fieldVideoAdditionalMaterial2;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-target-id")
    private WebElement fieldPresentationsCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-1-target-id")
    private WebElement fieldPresentationsCourseMaterial2;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-target-id")
    private WebElement fieldImagesCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-1-target-id")
    private WebElement fieldImagesCourseMaterial2;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-title-course-und-0-value")
    private WebElement fieldDownloadsTitle;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-title-course-und-0-value")
    private WebElement fieldDownloadsTitle2;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-title-additional-und-0-value")
    private WebElement fieldDownloadsTitleAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload")
    private WebElement fieldDownloadsFile;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-upload")
    private WebElement fieldDownloadsFile2;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload")
    private WebElement fieldDownloadsFileAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-remove-button")
    private WebElement buttonRemoveFile; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-remove-button")
    private WebElement buttonRemoveFile2; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-remove-button")
    private WebElement buttonRemoveFileAdditional; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload-button")
    private WebElement buttonUpload; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-upload-button")
    private WebElement buttonUpload2; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload-button")
    private WebElement buttonUploadAdditional; // Downloads
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-title")
    private WebElement fieldLinksTitle;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-0-title")
    private WebElement fieldLinksTitleAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-1-title")
    private WebElement fieldLinksTitle2;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-url")
    private WebElement fieldLinksUrl;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-0-url")
    private WebElement fieldLinksUrlAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-1-url")
    private WebElement fieldLinksUrl2;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-add-more")
    private WebElement buttonAddAnotherPresentationCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-add-more")
    private WebElement buttonAddAnotherImageCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-add-more")
    private WebElement buttonAddAnotherVideoAdditionalMaterial;
    @FindBy(how = How.CSS, using = "[id*='edit-field-videos-additional-und-1-add']")
    private WebElement iconAddVideoAdditionalMaterial2;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-add-entityconnect-field-videos-course-0-")
    private WebElement iconAddVideo;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-add-more")
    private WebElement buttonAddAnotherFileCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-add-more")
    private WebElement buttonAddAnotherLinkAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-links-additional-und-0-attributes-target")
    private WebElement checkboxOpenInNewWindow;


    /*---------CONSTRUCTORS--------*/
    public CreateMediaPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String mediaPageTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(mediaPageTitle);
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public WebElement getFieldSummary() {
        return fieldSummary;
    }
    public void fillFieldSummary (String summaryText) {
        waitUntilElementIsVisible(fieldSummary);
        fieldSummary.clear();
        fieldSummary.sendKeys(summaryText);
    }
    public WebElement getFieldChapter() {
        return fieldChapter;
    }
    public ViewMediaPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateMediaPage could not be clicked.");
        waitForPageToLoad();
        return new ViewMediaPage(webDriver, locations);
    }
    public WebElement getFieldLengthHours() {
        return fieldLengthHours;
    }
    public WebElement getFieldLengthMin() {
        return fieldLengthMin;
    }
    public void switchToTabGeneral() throws Exception {
        waitUntilElementIsClickable(tabGeneral);
        clickItem(tabGeneral, "The GeneralTab on the CreateMediaPage could not be clicked.");
    }
    public void switchToTabEducationalContent() throws Exception {
        waitUntilElementIsClickable(tabEducationalContent);
        clickItem(tabEducationalContent, "The EducationalContentTab on the CreateMediaPage could not be clicked.");
    }
    public void switchToTabCourseMaterial() throws Exception {
        waitUntilElementIsClickable(tabCourseMaterial);
        clickItem(tabCourseMaterial, "The CourseMaterialTab on the CreateMedia page could not be clicked.");
    }
    public void switchToTabAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(tabAdditionalMaterial);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, tabAdditionalMaterial);
        waitForPageToLoad();
    }
    public SelectVideoPage clickLinkSelectMedia() throws Exception {
        waitUntilElementIsClickable(linkSelectMedia);
        clickItem(linkSelectMedia, "The 'Select media' link on the CreateMediaPage could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public WebElement getFrameVideoSummary() {
        return frameVideoSummary;
    }
    public WebElement getFrameContextualInformation() {
        return frameContextualInformation;
    }
    public WebElement getLinkRemoveMedia() {
        return linkRemoveMedia;
    }
    public void clickLinkRemoveMedia() throws Exception {
        waitUntilElementIsClickable(linkRemoveMedia);
        clickItem(linkRemoveMedia, "The 'Remove media' link on the EditMediaPage could not be clicked.");
    }
    public CreatePresentationPage clickIconAddPresentation() throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddPresentation);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddPresentation.sendKeys(Keys.RETURN);
        } else clickItem(iconAddPresentation, "The icon 'Add Presentation' on the EditMedia page could not be clicked.");
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return new CreatePresentationPage(webDriver, locations);
    }
    public CreateVideoPage clickIconAddVideo() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideo);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddVideo.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideo, "The icon 'Add Video' on the EditMedia page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return new CreateVideoPage(webDriver, locations);
    }
    public WebElement getIconAddVideo() {
        return iconAddVideo;
    }
    public WebElement getFieldPresentationsAdditionalMaterial() {
        return fieldPresentationsAdditionalMaterial;
    }
    public WebElement getFieldPresentationsCourseMaterial() {
        return fieldPresentationsCourseMaterial;
    }
    public WebElement getFieldPresentationsCourseMaterial2() {
        return fieldPresentationsCourseMaterial2;
    }
    public WebElement getFieldImagesCourseMaterial() {
        return fieldImagesCourseMaterial;
    }
    public WebElement getFieldImagesCourseMaterial2() {
        return fieldImagesCourseMaterial2;
    }
    public WebElement getFieldVideoAdditionalMaterial2() {
        return fieldVideoAdditionalMaterial2;
    }
    public void fillFieldDownloadsTitle(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitle);
        fieldDownloadsTitle.sendKeys(title);
    }
    public void fillFieldDownloadsTitleAdditional(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitleAdditional);
        fieldDownloadsTitleAdditional.sendKeys(title);
    }
    public void clearFieldDownloadsTitle() {
        waitUntilElementIsVisible(fieldDownloadsTitle);
        fieldDownloadsTitle.clear();
    }
    public WebElement getFieldDownloadsTitle2() {
        return fieldDownloadsTitle2;
    }
    public void fillFieldDownloadsTitle2(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitle2);
        fieldDownloadsTitle2.sendKeys(title);
    }
    public void clickButtonUpload() throws Exception {
        waitUntilElementIsClickable(buttonUpload);
        clickItem(buttonUpload, "The button Upload on the CreateMedia page could not be clicked.");
    }
    public void clickButtonUploadAdditional() throws Exception {
        waitUntilElementIsClickable(buttonUploadAdditional);
        clickItem(buttonUploadAdditional, "The button Upload on the CreateMedia page could not be clicked.");
    }
    public void clickButtonUpload2() throws Exception {
        waitUntilElementIsClickable(buttonUpload2);
        clickItem(buttonUpload2, "The button Upload for the 2nd file on the CreateMedia page could not be clicked.");
    }
    public void uploadFileToDownloads(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFile.sendKeys(path);
        clickButtonUpload();
        waitUntilElementIsVisible(buttonRemoveFile);
    }
    public void uploadFileToDownloadsAdditional(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFileAdditional.sendKeys(path);
        clickButtonUploadAdditional();
        waitUntilElementIsVisible(buttonRemoveFileAdditional);
    }
    public void uploadFileToDownloads2(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFile2.sendKeys(path);
        clickButtonUpload2();
        waitUntilElementIsVisible(buttonRemoveFile2);
    }
    public WebElement getButtonUpload() {
        return buttonUpload;
    }
    public void clickButtonRemoveFile() throws Exception {
        waitUntilElementIsClickable(buttonRemoveFile);
        clickItem(buttonRemoveFile,
                "The button 'Remove' for the downloaded file on the CreateMediaPage could not be clicked.");
        waitUntilElementIsVisible(getButtonUpload());
    }
    public WebElement getFieldLinksTitleAdditional() {
        return fieldLinksTitleAdditional;
    }
    public void fillFieldLinksTitleAdditional(String title) {
        waitUntilElementIsVisible(fieldLinksTitleAdditional);
        fieldLinksTitleAdditional.sendKeys(title);
    }
    public void fillFieldLinksTitle(String title) {
        waitUntilElementIsVisible(fieldLinksTitle);
        fieldLinksTitle.sendKeys(title);
    }
    public WebElement getFieldLinksUrlAdditional() {
        return fieldLinksUrlAdditional;
    }
    public void fillFieldLinksUrlAdditional(String url) {
        waitUntilElementIsVisible(fieldLinksUrlAdditional);
        fieldLinksUrlAdditional.sendKeys(url);
    }
    public void fillFieldLinksUrl(String url) {
        waitUntilElementIsVisible(fieldLinksUrl);
        fieldLinksUrl.sendKeys(url);
    }
    public WebElement getFieldLinksTitle2() {
        return fieldLinksTitle2;
    }
    public void fillFieldLinksTitle2(String title) {
        waitUntilElementIsVisible(fieldLinksTitle2);
        fieldLinksTitle2.sendKeys(title);
    }
    public void fillFieldLinksUrl2(String url) {
        waitUntilElementIsVisible(fieldLinksUrl2);
        fieldLinksUrl2.sendKeys(url);
    }
    public WebElement getLinkSelectMediaFor3dVideo() {
        return linkSelectMediaFor3dVideo;
    }
    public SelectVideoPage clickLinkSelectMediaFor3dVideo() throws Exception {
        waitUntilElementIsClickable(linkSelectMediaFor3dVideo);
        clickItem(linkSelectMediaFor3dVideo, "The 'Select media' link for the 3d video on the CreateMedia page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public WebElement getLinkRemoveMediaFor3dVideo() {
        return linkRemoveMediaFor3dVideo;
    }
    public void clickButtonAddAnotherPresentationCourseMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherPresentationCourseMaterial);
        clickItem(buttonAddAnotherPresentationCourseMaterial,
                "The button 'Add another item' for presentations on the CreateMediaPage could not be clicked.");
        waitUntilElementIsVisible(getFieldPresentationsCourseMaterial2());
    }
    public void clickButtonAddAnotherImageCourseMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherImageCourseMaterial);
        clickItem(buttonAddAnotherImageCourseMaterial,
                "The button 'Add another item' for images on the CreateMediaPage could not be clicked.");
        waitUntilElementIsVisible(getFieldImagesCourseMaterial2());
    }
    public WebElement getFieldVideosAdditionalMaterial() {
        return fieldVideosAdditionalMaterial;
    }
    public WebElement getFieldVideosCourseMaterial() {
        return fieldVideosCourseMaterial;
    }
    public void clickButtonAddAnotherVideoAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherVideoAdditionalMaterial);
        clickItem(buttonAddAnotherVideoAdditionalMaterial,
                "The button 'Add another item' for videos on the CreateMediaPage could not be clicked.");
        waitUntilElementIsVisible(getIconAddVideoAdditionalMaterial2());
    }
    public WebElement getIconAddVideoAdditionalMaterial2() {
        return iconAddVideoAdditionalMaterial2;
    }
    public CreateVideoPage clickIconAddVideoAdditionalMaterial2() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideoAdditionalMaterial2);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IExplorer: problem with clicking an element that is not in focus
            iconAddVideoAdditionalMaterial2.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideoAdditionalMaterial2, "The AddVideo icon on the CreateMedia page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return new CreateVideoPage(webDriver, locations);
    }
    public void clickButtonAddAnotherFileCourseMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherFileCourseMaterial);
        clickItem(buttonAddAnotherFileCourseMaterial,
                "The button 'Add another item' for downloads on the CreateMediaPage could not be clicked.");
        waitUntilElementIsVisible(getFieldDownloadsTitle2());
    }
    public void clickButtonAddAnotherLinkAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherLinkAdditionalMaterial);
        clickItem(buttonAddAnotherLinkAdditionalMaterial,
                "The button 'Add another item' for links on the CreateMediaPage could not be clicked.");
        waitUntilElementIsVisible(getFieldLinksTitle2());
    }
    public  void clickCheckboxOpenInNewWindow() throws Exception {
        waitUntilElementIsClickable(checkboxOpenInNewWindow);
        clickItem(checkboxOpenInNewWindow, "The checkbox 'Open URL in a New Window' for the link " +
                "on the CreateMediaPage could not be clicked.");
    }

}
