package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateMultipleChoiceQuestionPage extends BasePage {

    /* ----- FIELDS ----- */


    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_2_contents\"]/iframe")
    private WebElement frameQuestion;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_3_contents\"]/iframe")
    private WebElement frameAlternative1;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_6_contents\"]/iframe")
    private WebElement frameAlternative2;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_9_contents\"]/iframe")
    private WebElement frameAlternative3;
    @FindBy(how = How.CSS, using = "iframe[title$=\"edit-alternatives-3-answer-value\"]") // [id='cke_954']+iframe
    private WebElement frameAlternative4;
    @FindBy(how = How.ID, using = "edit-choice-boolean")
    private WebElement checkboxSimpleScoring;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-alternatives-0-correct")
    private WebElement checkboxAlternative1;
    @FindBy(how = How.ID, using = "edit-alternatives-1-correct")
    private WebElement checkboxAlternative2;
    @FindBy(how = How.ID, using = "edit-alternatives-2-correct")
    private WebElement checkboxAlternative3;
    @FindBy(how = How.ID, using = "edit-alternatives-3-correct")
    private WebElement checkboxAlternative4;
    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-alternatives-multichoice-add-alternative")
    private WebElement buttonAddMoreAlternatives;
    @FindBy(how = How.CSS, using = "#edit-add-directly a")
    private WebElement linkAddToQuiz;
    @FindBy(how = How.ID, using = "edit-add-directly-new")
    private WebElement fieldTitleForNewQuiz;



    /*---------CONSTRUCTORS--------*/
    public CreateMultipleChoiceQuestionPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFrameQuestion() {
        return frameQuestion;
    }
    public WebElement getFrameAlternative1() {
        return frameAlternative1;
    }
    public WebElement getFrameAlternative2() {
        return frameAlternative2;
    }
    public WebElement getFrameAlternative3() {
        return frameAlternative3;
    }
    public WebElement getFrameAlternative4() {
        return frameAlternative4;
    }
    public WebElement getCheckboxSimpleScoring() {
        waitUntilElementIsVisible(checkboxSimpleScoring);
        return checkboxSimpleScoring;
    }
    public ViewMultipleChoiceQuestionPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        buttonSave.sendKeys(Keys.RETURN);
        waitForPageToLoad();
        return new ViewMultipleChoiceQuestionPage(webDriver, locations);
    }
    public WebElement getButtonSave() {
        return buttonSave;
    }
    public WebElement getCheckboxAlternative1() {
        return checkboxAlternative1;
    }
    public WebElement getCheckboxAlternative2() {
        return checkboxAlternative2;
    }
    public WebElement getCheckboxAlternative3() {
        return checkboxAlternative3;
    }
    public void clickCheckboxAlternative1() throws Exception {
        waitUntilElementIsClickable(checkboxAlternative1);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with selecting a checkbox
            checkboxAlternative1.sendKeys(Keys.SPACE);
        } else clickItem(checkboxAlternative1, "The checkbox Alternative1 on the CreateMultipleChoiceQuestion page could not be selected.");
    }
    public void checkTitleFieldIsNotEmpty() {
        waitUntilElementIsVisible(fieldTitle);
        String value = fieldTitle.getAttribute("value");
        Assert.assertFalse(value.isEmpty());
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public void fillFieldTitle(String title) {
        fieldTitle.sendKeys(title);
    }
    public void clickButtonAddMoreAlternatives() throws Exception {
        waitUntilElementIsClickable(buttonAddMoreAlternatives);
        clickItem(buttonAddMoreAlternatives, "The button AddMoreAlternatives on the EditMultipleChoiceQuestion page could not be clicked.");
        waitForPageToLoad();
    }
    public void waitUntilCheckboxAlternative4Appears() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            Thread.sleep(1000);
        } else {
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.id("edit-alternatives-3-correct")));
        }
    }
    public void clickCheckboxAlternative4() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with selecting a checkbox
            checkboxAlternative4.sendKeys(Keys.SPACE);
        } else clickItem(checkboxAlternative4, "The checkbox Alternative4 on the EditMultipleChoiceQuestion page could not be selected.");
    }
    public void clickLinkAddToQuiz() throws Exception {
        waitUntilElementIsClickable(linkAddToQuiz);
        clickItem(linkAddToQuiz, "The link 'Add to Quiz' on the CreateMultipleChoiceQuestion page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#edit-add-directly .fieldset-wrapper")));
    }
    public WebElement getFieldTitleForNewQuiz() {
        waitUntilElementIsVisible(fieldTitleForNewQuiz);
        return fieldTitleForNewQuiz;
    }

}
