package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreateNewAccountPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "input.text-full")
    private WebElement fieldFullName;
    @FindBy(how = How.CSS, using = "input[name='mail']")
    private WebElement fieldEmail;
    @FindBy(how = How.CSS, using = "input.password-field")
    private WebElement fieldPassword;
    @FindBy(how = How.CSS, using = "input.password-confirm")
    private WebElement fieldConfirmPassword;
    @FindBy(how = How.CSS, using = "input[value='Create new account']")
    private WebElement buttonCreateNewAccount;
    @FindBy(how = How.CSS, using = ".messages.messages--error")
    private WebElement messageError;
    @FindBy(how = How.LINK_TEXT, using = "Log in")
    private WebElement linkLogin;


    /*---------CONSTRUCTORS--------*/

    public CreateNewAccountPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldFullName(String fullName) {
        waitUntilElementIsVisible(fieldFullName);
        fieldFullName.sendKeys(fullName);
    }
    public void fillFieldEmail(String email) {
        waitUntilElementIsVisible(fieldEmail);
        fieldEmail.sendKeys(email);
    }
    public void fillFieldPassword(String password) {
        waitUntilElementIsVisible(fieldPassword);
        fieldPassword.sendKeys(password);
    }
    public void fillFieldConfirmPassword(String password) {
        waitUntilElementIsVisible(fieldConfirmPassword);
        fieldConfirmPassword.sendKeys(password);
    }
    public void clickButtonCreateNewAccount() throws Exception {
        waitUntilElementIsClickable(buttonCreateNewAccount);
        clickItem(buttonCreateNewAccount, "The button 'Create new account' on the CreateNewAccount page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessageError() {
        return messageError;
    }
    public LoginFormPage switchToLoginPage() throws Exception {
        waitUntilElementIsClickable(linkLogin);
        clickItem(linkLogin, "'Log in' page could not be opened.");
        return new LoginFormPage(webDriver, locations);
    }
}
