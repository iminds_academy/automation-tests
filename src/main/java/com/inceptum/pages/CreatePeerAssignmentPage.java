package com.inceptum.pages;


import java.io.File;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreatePeerAssignmentPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-summary-und-0-value")
    private WebElement fieldSummary;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-und")
    private WebElement fieldChapter;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHours;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMinutes;
    @FindBy(how = How.ID, using = "edit-field-number-reviews-und")
    private WebElement fieldNumberOfReviews;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Deadlines")
    private WebElement tabDeadlines;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Assignment context")
    private WebElement tabAssignmentContext;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Assignment resources")
    private WebElement tabTaskResources; // 'Assignment resources'
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Additional resources")
    private WebElement tabAdditionalResources;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Assignment cases")
    private WebElement tabAssignmentCases;
    @FindBy(how = How.XPATH, using = "//*[@class='horizontal-tabs clearfix']/ul/li/a/*[contains(text(), 'Evaluation criteria')]")
    private WebElement tabEvaluationCriteria;
    @FindBy(how = How.ID, using = "edit-field-draft-date-und-0-value-datepicker-popup-0")
    private WebElement fieldDraftSubmissionDeadlineDate;
    @FindBy(how = How.ID, using = "edit-field-draft-date-und-0-value-timeEntry-popup-1")
    private WebElement fieldDraftSubmissionDeadlineTime;
    @FindBy(how = How.ID, using = "edit-field-review-date-und-0-value-datepicker-popup-0")
    private WebElement fieldReviewSubmissionDeadlineDate;
    @FindBy(how = How.ID, using = "edit-field-review-date-und-0-value-timeEntry-popup-1")
    private WebElement fieldReviewSubmissionDeadlineTime;
    @FindBy(how = How.ID, using = "edit-field-final-date-und-0-value-datepicker-popup-0")
    private WebElement fieldFinalSubmissionDeadlineDate;
    @FindBy(how = How.ID, using = "edit-field-final-date-und-0-value-timeEntry-popup-1")
    private WebElement fieldFinalSubmissionDeadlineTime;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Upload button settings")
    private WebElement linkUploadButtonSettings;
    @FindBy(how = How.ID, using = "edit-field-iminds-academy-storage-und-0-iminds-academy-storage-account")
    private WebElement fieldSelectStorageAccount;
    @FindBy(how = How.ID, using = "edit-field-downloads-template-und-0-field-download-title-template-und-0-value")
    private WebElement fieldTitleTemplateSolution;
    @FindBy(how = How.ID, using = "edit-field-downloads-template-und-0-field-download-file-template-und-0-upload")
    private WebElement fieldUploadTemplate;
    @FindBy(how = How.ID, using = "edit-field-downloads-template-und-0-field-download-file-template-und-0-upload-button")
    private WebElement buttonUploadTemplate;
    @FindBy(how = How.ID, using = "edit-field-downloads-template-und-0-field-download-file-template-und-0-remove-button")
    private WebElement buttonRemoveTemplate;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_3_contents\"]/iframe")
    private WebElement iframeAssignmentCase;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_4_contents\"]/iframe")
    private WebElement iframeAssignmentCase2;
    @FindBy(how = How.CSS, using = "#cke_edit-field-cases-und-2-value iframe")
    private WebElement iframeAssignmentCase3;
    @FindBy(how = How.CSS, using = "#cke_1_contents iframe")
    private WebElement iframeWhatDoYouNeedToDo;
    @FindBy(how = How.ID, using = "edit-field-cases-und-add-more")
    private WebElement buttonAddAnotherButtonCase;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-field-multimedia-section-und-add-more")
    private WebElement buttonAddAnotherItemMainContent;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-0-field-criterion-name-und-0-value")
    private WebElement fieldNameEvaluationCriteria;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-0-field-criterion-description-und-0-value")
    private WebElement fieldDescriptionEvaluationCriteria;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-1-field-criterion-name-und-0-value")
    private WebElement fieldCriteriaName2;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-1-field-criterion-description-und-0-value")
    private WebElement fieldCriteriaDescription2;
    @FindBy(how = How.ID, using = "edit-field-multimedia-section-und-0-field-mmsection-title-und-0-value")
    private WebElement fieldTitleMainContent;
    @FindBy(how = How.ID, using = "edit-field-multimedia-section-und-1-field-mmsection-title-und-0-value")
    private WebElement fieldTitleMainContent2;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement errorMessage;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-target-id")
    private WebElement fieldPresentationsTaskResources;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-1-target-id")
    private WebElement fieldPresentationsTaskResources2;
    @FindBy(how = How.ID, using = "edit-field-presentations-peertask-und-0-target-id")
    private WebElement fieldPresentationsAdditionalResources;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-edit-entityconnect-field-presentations-course-0-")
    private WebElement iconEditPresentation;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-target-id")
    private WebElement fieldVideosTaskResources;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-1-target-id")
    private WebElement fieldVideosTaskResources2;
    @FindBy(how = How.ID, using = "edit-field-videos-peertask-und-0-target-id")
    private WebElement fieldVideosAdditionalResources;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-edit-entityconnect-field-videos-course-0-")
    private WebElement iconEditVideo;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-target-id") // ??? correct css ???
    private WebElement fieldImagesTaskResources;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-1-target-id")  // ??? correct css ???
    private WebElement fieldImagesTaskResources2;
    @FindBy(how = How.ID, using = "edit-field-images-peertask-und-0-target-id")
    private WebElement fieldImagesAdditionalResourses;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-edit-entityconnect-field-images-course-0-")
    private WebElement iconEditImage;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-title-course-und-0-value")
    private WebElement fieldTitleDownloads;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-title-course-und-0-value")
    private WebElement fieldTitleDownloads2;
    @FindBy(how = How.ID, using = "edit-field-downloads-peertask-und-0-field-download-title-peertask-und-0-value")
    private WebElement fieldTitleDownloadsAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload")
    private WebElement fieldDownloadsFile;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-upload")
    private WebElement fieldDownloadsFile2;
    @FindBy(how = How.ID, using = "edit-field-downloads-peertask-und-0-field-download-file-peertask-und-0-upload")
    private WebElement fieldDownloadsFileAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload-button")
    private WebElement buttonUploadFileToDownloads;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-upload-button")
    private WebElement buttonUploadFileToDownloads2;
    @FindBy(how = How.ID, using = "edit-field-downloads-peertask-und-0-field-download-file-peertask-und-0-upload-button")
    private WebElement buttonUploadFileToDownloadsAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-remove-button")
    private WebElement buttonRemoveFileFromDownloads;
    @FindBy(how = How.ID, using = "edit-field-downloads-peertask-und-0-field-download-file-peertask-und-0-remove-button")
    private WebElement buttonRemoveFileFromDownloadsAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-remove-button")
    private WebElement buttonRemoveFieldsetDownloads;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-remove-button")
    private WebElement buttonRemoveFieldsetDownloads2;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-title")
    private WebElement fieldTitleLink;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-1-title")
    private WebElement fieldTitleLink2;
    @FindBy(how = How.ID, using = "edit-field-links-peertask-und-0-title")
    private WebElement fieldTitleLinkAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-url")
    private WebElement fieldUrlLink;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-1-url")
    private WebElement fieldUrlLink2;
    @FindBy(how = How.ID, using = "edit-field-links-peertask-und-0-url")
    private WebElement fieldUrlLinkAdditional;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-add-more")
    private WebElement buttonAddAnotherItemPresentation;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-add-more")
    private WebElement buttonAddAnotherItemVideo;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-add-more")
    private WebElement buttonAddAnotherItemImage;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-add-more")
    private WebElement buttonAddAnotherItemFileDownloads;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-add-more")
    private WebElement buttonAddAnotherItemLink;
    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-attributes-target")
    private WebElement checkboxOpenInNewWindow;
    @FindBy(how = How.ID, using = "edit-field-links-peertask-und-0-attributes-target")
    private WebElement checkboxOpenInNewWindowAdditional;


    /*---------CONSTRUCTORS--------*/
    public CreatePeerAssignmentPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public void fillFieldTitle(String assignmentTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(assignmentTitle);
    }
    public void fillFieldSummary(String assignmentSummary) throws Exception {
        waitUntilElementIsVisible(fieldSummary);
        fieldSummary.sendKeys(assignmentSummary);
    }
    public WebElement getFieldChapter() {
        return fieldChapter;
    }
    public WebElement getFieldLengthHours() {
        waitUntilElementIsVisible(fieldLengthHours);
        return fieldLengthHours;
    }
    public WebElement getFieldLengthMinutes() {
        waitUntilElementIsVisible(fieldLengthMinutes);
        return fieldLengthMinutes;
    }
    public WebElement getFieldNumberOfReviews() {
        waitUntilElementIsVisible(fieldNumberOfReviews);
        return fieldNumberOfReviews;
    }
    public void switchToTabDeadlines() throws Exception {
        waitUntilElementIsClickable(tabDeadlines);
        clickItem(tabDeadlines, "The tab Deadlines on the CreatePeerAssignment page could not be clicked.");
    }
    public void switchToTabAssignmentContext() throws Exception {
        waitUntilElementIsClickable(tabAssignmentContext);
        clickItem(tabAssignmentContext, "The tab 'Assignment context' on the CreatePeerAssignment page could not be clicked.");
    }
    public void switchToTabTaskResources() throws Exception {
        waitUntilElementIsClickable(tabTaskResources);
        clickItem(tabTaskResources, "The tab 'Task resources' on the CreatePeerAssignment page could not be clicked.");
    }
    public void switchToTabAdditionalResources() throws Exception {
        waitUntilElementIsClickable(tabAdditionalResources);
        clickItem(tabAdditionalResources, "The tab 'Additional resources' on the CreatePeerAssignment page could not be clicked.");
    }
    public void switchToTabAssignmentCases() throws Exception {
        waitUntilElementIsClickable(tabAssignmentCases);
        clickItem(tabAssignmentCases, "The tab 'Assignment cases' on the CreatePeerAssignment page could not be clicked.");
    }
    public void switchToTabEvaluationCriteria() throws Exception {
        waitUntilElementIsClickable(tabEvaluationCriteria);
        clickItem(tabEvaluationCriteria, "The tab 'Evaluation criteria' on the CreatePeerAssignment page could not be clicked.");
    }
    public WebElement getFieldDraftSubmissionDeadlineDate() {
        waitUntilElementIsVisible(fieldDraftSubmissionDeadlineDate);
        return fieldDraftSubmissionDeadlineDate;
    }
    public WebElement getFieldDraftSubmissionDeadlineTime() {
        waitUntilElementIsVisible(fieldDraftSubmissionDeadlineTime);
        return fieldDraftSubmissionDeadlineTime;
    }
    public WebElement getFieldReviewSubmissionDeadlineDate() {
        waitUntilElementIsVisible(fieldReviewSubmissionDeadlineDate);
        return fieldReviewSubmissionDeadlineDate;
    }
    public WebElement getFieldReviewSubmissionDeadlineTime() {
        waitUntilElementIsVisible(fieldReviewSubmissionDeadlineTime);
        return fieldReviewSubmissionDeadlineTime;
    }
    public WebElement getFieldFinalSubmissionDeadlineDate() {
        waitUntilElementIsVisible(fieldFinalSubmissionDeadlineDate);
        return fieldFinalSubmissionDeadlineDate;
    }
    public WebElement getFieldFinalSubmissionDeadlineTime() {
        waitUntilElementIsVisible(fieldFinalSubmissionDeadlineTime);
        return fieldFinalSubmissionDeadlineTime;
    }

    public void fillFieldDeadlineTimePlus(WebElement field, int addHours) throws Exception { // this method gets current time and adds certain hours to it
        // Get current time
        Calendar currentTime = Calendar.getInstance();
        int hoursInt = currentTime.get(Calendar.HOUR_OF_DAY);
        int hoursPlusInt = hoursInt + addHours; // Get time (currentHours + additionalHours)
        String hoursPlus = Integer.toString(hoursPlusInt);
        int minutesInt = currentTime.get(Calendar.MINUTE);
        String minutes = Integer.toString(minutesInt);
        waitUntilElementIsClickable(field);
        field.click();
        // Exception for hour = 23
        if (hoursPlus.equals("24")) {
            field.sendKeys("23");
            field.sendKeys(Keys.ARROW_RIGHT);
            field.sendKeys("59");
        } else {
            field.sendKeys(hoursPlus);
            field.sendKeys(Keys.ARROW_RIGHT);
            field.sendKeys(minutes);
        }
    }
    public void selectStorageAccount(String accountName) throws Exception {
        scrollToElement(fieldTitle);
        waitUntilElementIsClickable(linkUploadButtonSettings);
        linkUploadButtonSettings.click();
        waitUntilElementIsVisible(fieldSelectStorageAccount);
        List<WebElement> options = fieldSelectStorageAccount.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals(accountName)) {
                option.click();
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public void fillFieldTitleTemplateSolution(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleTemplateSolution);
        fieldTitleTemplateSolution.sendKeys(title);
    }
    public void clickButtonUploadTemplate() throws Exception {
        waitUntilElementIsClickable(buttonUploadTemplate);
        clickItem(buttonUploadTemplate, "The button 'Upload' for Template on the CreatePeerAssignment page could not be clicked.");
    }
    public void selectTemplate(String templatePath) throws Exception {
        String path = new File(templatePath).getAbsolutePath();
        waitUntilElementIsClickable(fieldUploadTemplate);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") ||
                PropertyLoader.loadProperty("browser.platform").equals("any")) { // Exception for Linux OS/Mavericks: enter a correct file path
            String templatePathLinux = path.replace("D:", "");
            fieldUploadTemplate.sendKeys(templatePathLinux);
        } else fieldUploadTemplate.sendKeys(path);
        clickButtonUploadTemplate();
        waitUntilElementIsVisible(buttonRemoveTemplate);
    }
    public WebElement getIframeAssignmentCase() {
        return iframeAssignmentCase;
    }
    public WebElement getIframeAssignmentCase2() {
        return iframeAssignmentCase2;
    }
    public WebElement getIframeAssignmentCase3() {
        return iframeAssignmentCase3;
    }
    public WebElement getIframeWhatDoYouNeedToDo() {
        return iframeWhatDoYouNeedToDo;
    }
    public void clickButtonAddAnotherButtonCase() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherButtonCase);
        clickItem(buttonAddAnotherButtonCase, "The button 'Add another item' on CreatePeerAssignment page (Assignment resources) could not be clicked.");
        waitUntilElementIsVisible(iframeAssignmentCase3);
    }
    public ViewPeerAssignmentPage clickButtonSaveTop() throws Exception {
        waitUntilElementIsClickable(getButtonSaveTop());
        WebElement pageTitle = webDriver.findElement(By.cssSelector("h1"));
        scrollToElement(pageTitle);
        clickItem(getButtonSaveTop(), "The button 'Save' at the top of CreatePeerAssignment page could not be clicked.");
        waitForPageToLoad();
        return new ViewPeerAssignmentPage(webDriver, locations);
    }
    public ViewPeerAssignmentPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreatePeerAssignment page could not be clicked.");
        waitForPageToLoad();
        return new ViewPeerAssignmentPage(webDriver, locations);
    }
    public void clickButtonAddAnotherItemMainContent() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemMainContent);
        clickItem(buttonAddAnotherItemMainContent, "The button 'Add another item' on the CreatePeerAssignment page could not be clicked.");
    }
    public void fillFieldNameEvaluationCriteria(String name) throws Exception {
        waitUntilElementIsVisible(fieldNameEvaluationCriteria);
        fieldNameEvaluationCriteria.clear();
        fieldNameEvaluationCriteria.sendKeys(name);
    }
    public void fillFieldDescriptionEvaluationCriteria(String description) throws Exception {
        waitUntilElementIsVisible(fieldDescriptionEvaluationCriteria);
        fieldDescriptionEvaluationCriteria.sendKeys(description);
    }
    public void addEvaluationCriteria(String name, String description) throws Exception {
        fillFieldNameEvaluationCriteria(name);
        fillFieldDescriptionEvaluationCriteria(description);
    }
    public WebElement getFieldCriteriaName2() {
        return fieldCriteriaName2;
    }
    public void fillFieldCriteriaName2(String title) throws Exception {
        waitUntilElementIsVisible(fieldCriteriaName2);
        fieldCriteriaName2.sendKeys(title);
    }
    public void fillFieldCriteriaDescription2(String title) throws Exception {
        waitUntilElementIsVisible(fieldCriteriaDescription2);
        fieldCriteriaDescription2.sendKeys(title);
    }
    public void addEvaluationCriteria2(String name, String description) throws Exception {
        fillFieldCriteriaName2(name);
        fillFieldCriteriaDescription2(description);
    }
    public void fillFieldTitleMainContent(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleMainContent);
        fieldTitleMainContent.sendKeys(title);
    }
    public void fillFieldTitleMainContent2(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleMainContent2);
        fieldTitleMainContent2.sendKeys(title);
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getErrorMessage() {
        return errorMessage;
    }
    public WebElement getFieldPresentationsTaskResources() {
        return fieldPresentationsTaskResources;
    }
    public WebElement getFieldPresentationsTaskResources2() {
        return fieldPresentationsTaskResources2;
    }
    public WebElement getFieldPresentationsAdditionalResources() {
        return fieldPresentationsAdditionalResources;
    }
    public EditPresentationPage clickIconEditPresentation() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            iconEditPresentation.sendKeys(Keys.RETURN);
        } else clickItem(iconEditPresentation, "The icon 'Edit Presentation' on the EditPeerAssignment page could not be clicked.");
        return new EditPresentationPage(webDriver, locations);
    }
    public EditVideoPage clickIconEditVideo() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            iconEditVideo.sendKeys(Keys.RETURN);
        } else clickItem(iconEditVideo, "The icon 'Edit Video' on the EditPeerAssignment page could not be clicked.");
        return new EditVideoPage(webDriver, locations);
    }
    public EditImagePage clickIconEditImage() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            iconEditImage.sendKeys(Keys.RETURN);
        } else clickItem(iconEditImage, "The icon 'Edit Image' on the EditPeerAssignment page could not be clicked.");
        return new EditImagePage(webDriver, locations);
    }
    public WebElement getFieldVideosTaskResources() {
        return fieldVideosTaskResources;
    }
    public WebElement getFieldVideosTaskResources2() {
        return fieldVideosTaskResources2;
    }
    public WebElement getFieldVideosAdditionalResources() throws Exception {
        waitUntilElementIsVisible(fieldVideosAdditionalResources);
        return fieldVideosAdditionalResources;
    }
    public WebElement getFieldImagesTaskResources() {
        return fieldImagesTaskResources;
    }
    public WebElement getFieldImagesAdditionalResources() throws Exception {
        waitUntilElementIsVisible(fieldImagesAdditionalResourses);
        return fieldImagesAdditionalResourses;
    }
    public WebElement getFieldImagesTaskResources2() {
        return fieldImagesTaskResources2;
    }
    public void fillFieldTitleDownloads(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleDownloads);
        fieldTitleDownloads.sendKeys(title);
    }
    public void fillFieldTitleDownloadsAdditional(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleDownloadsAdditional);
        fieldTitleDownloadsAdditional.sendKeys(title);
    }
    public WebElement getFieldDownloadsFile() {
        return fieldDownloadsFile;
    }
    public WebElement getFieldDownloadsFileAdditional() {
        return fieldDownloadsFileAdditional;
    }
    public void clickButtonUploadFileToDownloads() throws Exception {
        waitUntilElementIsClickable(buttonUploadFileToDownloads);
        clickItem(buttonUploadFileToDownloads, "The button 'Upload' on the CreatePeerAssignment page could not be clicked.");
    }
    public void clickButtonUploadFileToDownloadsAdditional() throws Exception {
        waitUntilElementIsClickable(buttonUploadFileToDownloadsAdditional);
        clickItem(buttonUploadFileToDownloadsAdditional, "The button 'Upload' on the CreatePeerAssignment page could not be clicked.");
    }
    public WebElement getButtonRemoveFileFromDownloads() {
        return buttonRemoveFileFromDownloads;
    }
    public WebElement getButtonRemoveFileFromDownloadsAdditional() {
        return buttonRemoveFileFromDownloadsAdditional;
    }
    public void clickButtonRemoveFieldsetDownloads() throws Exception {
        waitUntilElementIsClickable(buttonRemoveFieldsetDownloads);
        clickItem(buttonRemoveFieldsetDownloads, "The button 'Remove' for 'Downloads' fieldset on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(buttonUploadFileToDownloads);
    }
    public WebElement getButtonRemoveFieldsetDownloads2() {
        return buttonRemoveFieldsetDownloads2;
    }
    public void clickButtonRemoveFieldsetDownloads2() throws Exception {
        waitUntilElementIsClickable(buttonRemoveFieldsetDownloads2);
        clickItem(buttonRemoveFieldsetDownloads2, "The button 'Remove' for 'Downloads' fieldset (another item) on the CreatePeerAssignment page could not be clicked.");
        // Wait until the 2nd fieldset is removed
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-field-downloads-course-und-1-field-download-title-course-und-0-value")));
    }
    public void fillFieldTitleLink(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleLink);
        fieldTitleLink.sendKeys(title);
    }
    public void fillFieldTitleLinkAdditional(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitleLinkAdditional);
        fieldTitleLinkAdditional.sendKeys(title);
    }
    public void fillFieldUrlLink(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlLink);
        fieldUrlLink.sendKeys(url);
    }
    public void fillFieldUrlLinkAdditional(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrlLinkAdditional);
        fieldUrlLinkAdditional.sendKeys(url);
    }
    public void clearFieldTitleLink() throws Exception {
        waitUntilElementIsVisible(fieldTitleLink);
        fieldTitleLink.clear();
    }
    public void clearFieldTitleLink2() throws Exception {
        waitUntilElementIsVisible(fieldTitleLink2);
        fieldTitleLink2.clear();
    }
    public void clearFieldUrlLink() throws Exception {
        waitUntilElementIsVisible(fieldUrlLink);
        fieldUrlLink.clear();
    }
    public void clearFieldUrlLink2() throws Exception {
        waitUntilElementIsVisible(fieldUrlLink2);
        fieldUrlLink2.clear();
    }
    public void clickButtonAddAnotherItemPresentation() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemPresentation);
        buttonAddAnotherItemPresentation.sendKeys(Keys.RETURN);
        waitUntilElementIsVisible(fieldPresentationsTaskResources2);
    }
    public void clickButtonAddAnotherItemVideo() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemVideo);
        clickItem(buttonAddAnotherItemVideo, "The button 'Add another item' for video on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(fieldVideosTaskResources2);
    }
    public void clickButtonAddAnotherItemImage() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemImage);
        clickItem(buttonAddAnotherItemImage, "The button 'Add another item' for image on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(fieldImagesTaskResources2);
    }
    public void clickButtonAddAnotherItemFileDownloads() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemFileDownloads);
        clickItem(buttonAddAnotherItemFileDownloads, "The button 'Add another item' for Downloads on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(fieldTitleDownloads2);
    }
    public void fillFieldTitleDownloads2(String title) throws Exception {
        fieldTitleDownloads2.sendKeys(title);
    }
    public void clickButtonAddAnotherItemLink() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemLink);
        clickItem(buttonAddAnotherItemLink, "The button 'Add another item' for link on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(fieldTitleLink2);
    }
    public void fillFieldTitleLink2(String title) throws Exception {
        fieldTitleLink2.sendKeys(title);
    }
    public void fillFieldUrlLink2(String url) throws Exception {
        fieldUrlLink2.sendKeys(url);
    }
    public WebElement getFieldDownloadsFile2() {
        return fieldDownloadsFile2;
    }
    public void clickButtonUploadFileToDownloads2() throws Exception {
        waitUntilElementIsClickable(buttonUploadFileToDownloads2);
        clickItem(buttonUploadFileToDownloads2, "The button 'Upload' for new item on the CreatePeerAssignment page could not be clicked.");
    }
    public DeleteItemConfirmationPage clickButtonDeleteTop() throws Exception {
        waitUntilElementIsClickable(buttonDeleteTop);
        clickItem(buttonDeleteTop, "The Delete button on the CreatePeerAssignment page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public void selectCheckboxOpenInNewWindow() throws Exception {
        waitUntilElementIsClickable(checkboxOpenInNewWindow);
        clickItem(checkboxOpenInNewWindow, "The checkbox 'Open URL in a New Window' on the PeerAssignment page could not be clicked.");
    }
    public void selectCheckboxOpenInNewWindowAdditional() throws Exception {
        waitUntilElementIsClickable(checkboxOpenInNewWindowAdditional);
        clickItem(checkboxOpenInNewWindowAdditional, "The checkbox 'Open URL in a New Window' on the PeerAssignment page could not be clicked.");
    }


}
