package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreatePresentationPage extends BasePage {

        /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "[id*='presentation'] #edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select media")
    private WebElement linkSelectMedia;
    @FindBy(how = How.XPATH, using = "//*[@id=\"edit-field-slideshare-presentation-und-0\"]/a[2]")
    private WebElement linkRemoveMedia;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;
    @FindBy(how = How.XPATH, using = "//*[@id=\"console\"]/div/ul/li[1]")
    private WebElement errorMessageTitle;
    @FindBy(how = How.XPATH, using = "//*[@id=\"console\"]/div/ul/li[2]")
    private WebElement errorMessagePresentation;
    @FindBy(how = How.XPATH, using = "//*[@id=\"edit-field-slideshare-presentation-und-0-preview\"]/div/div")
    private WebElement thumbnailPresentation;


    /*---------CONSTRUCTORS--------*/
    public CreatePresentationPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/
    public void fillFieldTitle(String presentationTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(presentationTitle);
    }
    public WebElement getFieldTitle() throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public ViewPresentationPage clickButtonSave() throws Exception {
        clickItem(buttonSave, "The Save button on the CreatePresentation page could not be clicked.");
        waitForPageToLoad();
        return new ViewPresentationPage(webDriver, locations);
    }
    public ViewPresentationPage clickButtonSaveTop() throws Exception {
        clickItem(buttonSaveTop, "The Save button at the top of the CreatePresentation page could not be clicked.");
        waitForPageToLoad();
        return new ViewPresentationPage(webDriver, locations);
    }
    public SelectPresentationPage clickLinkSelectMedia() throws Exception {
        waitUntilElementIsClickable(linkSelectMedia);
        clickItem(linkSelectMedia, "The 'Select media' link on the CreatePresentation page could not be clicked.");
        return new SelectPresentationPage(webDriver, locations);
    }
    public WebElement getLinkRemoveMedia() {
        return linkRemoveMedia;
    }
    public void clickLinkRemoveMedia() throws Exception {
        waitUntilElementIsClickable(linkRemoveMedia);
        clickItem(linkRemoveMedia, "The link 'Remove Media' on the EditPresentation page could not be clicked.");
    }
    public void checkLinkIsNotDisplayed() throws Exception {
        waitForPageToLoad();
        Assert.assertEquals("display: none;", linkRemoveMedia.getAttribute("style"));
    }
    public WebElement getMessage() {
        return message;
    }
    public void assertErrorMessage(String messageTitle, String messagePresentation) {
        Assert.assertEquals("messages error", message.getAttribute("class"));
        // Check correct text of error message
        String errorPresentationMessageTitle = errorMessageTitle.getText();
        String errorPresentationMessagePresentation = errorMessagePresentation.getText();
        Assert.assertTrue("Detected incorrect message: " + errorPresentationMessageTitle, errorPresentationMessageTitle.matches(messageTitle));
        Assert.assertTrue("Detected incorrect message: " + errorPresentationMessagePresentation, errorPresentationMessagePresentation.matches("(?s)" + messagePresentation));
    }
    public WebElement getThumbnailPresentation() {
        return thumbnailPresentation;
    }

}
