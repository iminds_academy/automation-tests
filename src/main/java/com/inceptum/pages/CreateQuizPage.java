package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateQuizPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-randomization-0")
    private WebElement radiobuttonNoRandomization;



    /*---------CONSTRUCTORS--------*/
    public CreateQuizPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public void fillFieldTitle(String title) {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public WebElement getButtonSave() {
        return buttonSave;
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            buttonSave.sendKeys(Keys.RETURN);
        } else clickItem(buttonSave, "The Save button on the CreateQuiz page could not be clicked.");
        waitForPageToLoad();
    }
    public void checkNoRandomizationIsSelected() throws Exception {
        waitUntilElementIsClickable(radiobuttonNoRandomization);
        if (!(radiobuttonNoRandomization.isSelected())) {
            clickItem(radiobuttonNoRandomization, "The radiobutton 'No randomization' on the CreateQuiz page could not be clicked.");
        }
        Assert.assertTrue("Radiobutton 'No randomization' on the CreateQuiz page is not selected.", radiobuttonNoRandomization.isSelected());
    }

}
