package com.inceptum.pages;


import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateTaskPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-summary-und-0-value")
    private WebElement fieldSummary;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-und")
    private WebElement fieldChapter;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-add-entityconnect-field-material-chapter-all-")
    private WebElement iconAddChapter;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-edit-entityconnect-field-material-chapter-all-")
    private WebElement iconEditChapter;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Educational content")
    private WebElement tabEducationalContent;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Task resources")
    private WebElement tabTaskResources;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Additional resources")
    private WebElement tabAdditionalResources;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHours;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMin;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_1_contents\"]/iframe")
    private WebElement frameWhatDoYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_2_contents\"]/iframe")
    private WebElement frameWhatIsThisFor;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_3_contents\"]/iframe")
    private WebElement frameLearningObjectives;
    @FindBy(how = How.CSS, using = "[name='add_entityconnect__field_videos_course_0_']") // ID"edit-field-videos-course-und-0-add-entityconnect-field-videos-course-0-"
    private WebElement iconAddVideo;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-0-add-entityconnect-field-videos-additional-0-")
    private WebElement iconAddVideoAdditional;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-target-id")
    private WebElement fieldVideosTaskResourses;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-0-target-id")
    private WebElement fieldVideosAdditionalResourses;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-target-id")
    private WebElement fieldPresentationsTaskResourses;
    @FindBy(how = How.ID, using = "edit-field-presentations-additional-und-0-target-id")
    private WebElement fieldPresentationsAdditionalResourses;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-target-id")
    private WebElement fieldImagesTaskResources;
    @FindBy(how = How.ID, using = "edit-field-images-additional-und-0-target-id")
    private WebElement fieldImagesAdditionalResources;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-add-entityconnect-field-images-course-0-")
    private WebElement iconAddImage;
    @FindBy(how = How.ID, using = "edit-field-images-additional-und-0-add-entityconnect-field-images-additional-0-")
    private WebElement iconAddImageAdditional;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-title-additional-und-0-value")
    private WebElement fieldDownloadsTitle;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload")
    private WebElement fieldDownloadsFile;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-remove-button")
    private WebElement buttonRemoveFile; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload-button")
    private WebElement buttonUpload; // Downloads
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-add-entityconnect-field-presentations-course-0-")
    private WebElement iconAddPresentation;
    @FindBy(how = How.ID, using = "edit-field-presentations-additional-und-0-add-entityconnect-field-presentations-additional-0-")
    private WebElement iconAddPresentationAdditional;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-title")
    private WebElement fieldLinksTitle;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-url")
    private WebElement fieldLinksUrl;



    /*---------CONSTRUCTORS--------*/
    public CreateTaskPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/
    public void fillFieldTitle(String taskTitle) {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(taskTitle);
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public void fillFieldSummary (String summaryText) {
        waitUntilElementIsVisible(fieldSummary);
        fieldSummary.clear();
        fieldSummary.sendKeys(summaryText);
    }
    public WebElement getFieldChapter() {
        return fieldChapter;
    }
    public AddChapterPage clickIconAddChapter() throws Exception {
        waitUntilElementIsClickable(iconAddChapter);
        clickItem(iconAddChapter, "The AddChapter icon on the CreateTask page could not be clicked.");
        waitForPageToLoad();
        return new AddChapterPage(webDriver, locations);
    }
    public EditChapterPage clickIconEditChapter() throws Exception {
        clickItem(iconEditChapter, "The EditChapter icon on the CreateTask page could not be clicked.");
        return new EditChapterPage(webDriver, locations);
    }
    public ViewTaskPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateTask page could not be clicked.");
        waitForPageToLoad();
        return new ViewTaskPage(webDriver, locations);
    }
    public ViewTaskPage clickButtonSaveTop() throws Exception {
        waitUntilElementIsClickable(buttonSaveTop);
        clickItem(buttonSaveTop, "The Save button at the top of the CreateTask page could not be clicked.");
        waitForPageToLoad();
        return new ViewTaskPage(webDriver, locations);
    }
    public void switchToTabEducationalContent() throws Exception {
        clickItem(tabEducationalContent, "The EducationalContentTab on the CreateTask page could not be clicked.");
    }
    public void switchToTabTaskResources() throws Exception {
        waitUntilElementIsVisible(tabTaskResources);
        clickItem(tabTaskResources, "The TaskResourcesTab on the CreateTask page could not be clicked.");
    }
    public void switchToTabAdditionalResources() throws Exception {
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, tabAdditionalResources);
    }
    public WebElement getFieldLengthHours() {
        return fieldLengthHours;
    }
    public WebElement getFieldLengthMin() {
        return fieldLengthMin;
    }
    public WebElement getFrameWhatDoYouNeedToDo() {
        return frameWhatDoYouNeedToDo;
    }
    public WebElement getFrameWhatIsThisFor() {
        return frameWhatIsThisFor;
    }
    public WebElement getFrameLearningObjectives() {
        return frameLearningObjectives;
    }
    public WebElement getIconAddVideo() {
        return iconAddVideo;
    }
    public WebElement getIconAddVideoAdditional() {
        return iconAddVideoAdditional;
    }
    public CreateVideoPage clickIconAddVideo() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideo);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IExplorer: problem with clicking an element that is not in focus
            iconAddVideo.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideo, "The AddVideo icon on the CreateTask page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return new CreateVideoPage(webDriver, locations);
    }
    public CreateVideoPage clickIconAddVideoAdditional() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideoAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IExplorer: problem with clicking an element that is not in focus
            iconAddVideoAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideoAdditional, "The AddVideo icon on the CreateTask page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return new CreateVideoPage(webDriver, locations);
    }
    public WebElement getFieldVideosTaskResorses() {
        waitUntilElementIsVisible(fieldVideosTaskResourses);
        return fieldVideosTaskResourses;
    }
    public WebElement getFieldVideosAdditionalResourses() {
        waitUntilElementIsVisible(fieldVideosAdditionalResourses);
        return fieldVideosAdditionalResourses;
    }
    public WebElement getFieldPresentationsTaskResorses() {
        waitUntilElementIsVisible(fieldPresentationsTaskResourses);
        return fieldPresentationsTaskResourses;
    }
    public WebElement getFieldImagesAdditionalResources() throws Exception {
        waitUntilElementIsVisible(fieldImagesAdditionalResources);
        return fieldImagesAdditionalResources;
    }
    public WebElement getFieldImagesTaskResources() throws Exception {
        waitUntilElementIsVisible(fieldImagesTaskResources);
        return fieldImagesTaskResources;
    }
    public WebElement getFieldPresentationsAdditionalResourses() {
        waitUntilElementIsVisible(fieldPresentationsAdditionalResourses);
        return fieldPresentationsAdditionalResourses;
    }
    public String getChapterID(String chapterName) {
        waitUntilElementIsVisible(fieldChapter);
        List<WebElement> options = fieldChapter.findElements(By.tagName("option"));
        String chapterId ="";
        for (WebElement option : options) {
            if (option.getText().equals(chapterName)){
                chapterId = option.getAttribute("value");
                option.click();
                break;
            }
        }
        return chapterId;
    }
    public CreateImagePage clickIconAddImageAdditional() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImageAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddImageAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImageAdditional, "The icon 'Add Image' on the EditTask page could not be clicked.");
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return new CreateImagePage(webDriver, locations);
    }
    public WebElement getIconAddImageAdditional() {
        return iconAddImageAdditional;
    }
    public void fillFieldDownloadsTitle(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitle);
        fieldDownloadsTitle.sendKeys(title);
    }
    public void clickButtonUpload() throws Exception {
        waitUntilElementIsClickable(buttonUpload);
        clickItem(buttonUpload, "The button Upload on the CreateTask page could not be clicked.");
    }
    public void uploadFileToDownloads(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFile.sendKeys(path);
        clickButtonUpload();
        waitUntilElementIsVisible(buttonRemoveFile);
    }
    public CreatePresentationPage clickIconAddPresentation() throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddPresentation);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddPresentation.sendKeys(Keys.RETURN);
        } else clickItem(iconAddPresentation, "The AddPresentation icon on the CreateTask page could not be clicked.");
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return new CreatePresentationPage(webDriver, locations);
    }
    public CreatePresentationPage clickIconAddPresentationAdditional() throws Exception {
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, iconAddPresentationAdditional);
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return new CreatePresentationPage(webDriver, locations);
    }
    public CreateImagePage clickIconAddImage() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImage);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddImage.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImage, "The AddImage icon on the CreateTask page could not be clicked.");
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return new CreateImagePage(webDriver, locations);
    }
    public WebElement getIconAddPresentation() {
        return iconAddPresentation;
    }
    public WebElement getIconAddPresentationAdditional() {
        return iconAddPresentationAdditional;
    }
    public void fillFieldLinksTitle(String title) {
        waitUntilElementIsVisible(fieldLinksTitle);
        fieldLinksTitle.sendKeys(title);
    }
    public void fillFieldLinksUrl(String url) {
        waitUntilElementIsVisible(fieldLinksUrl);
        fieldLinksUrl.sendKeys(url);
    }

}

