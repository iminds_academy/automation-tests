package com.inceptum.pages;


import java.io.File;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CreateTheoryPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-summary-und-0-value")
    private WebElement fieldSummary;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-und")
    private WebElement fieldChapter;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-add-entityconnect-field-material-chapter-all-")
    private WebElement iconAddChapter;
    @FindBy(how = How.ID, using = "edit-field-material-chapter-edit-entityconnect-field-material-chapter-all-")
    private WebElement iconEditChapter;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Educational content")
    private WebElement tabEducationalContent;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Course material")
    private WebElement tabCourseMaterial;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Additional material")
    private WebElement tabAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHours;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMin;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_1_contents\"]/iframe")
    private WebElement frameWhatDoYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_2_contents\"]/iframe")
    private WebElement frameWhatIsThisFor;
    @FindBy(how = How.XPATH, using = "//*[@id=\"cke_3_contents\"]/iframe")
    private WebElement frameLearningObjectives;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-0-target-id")
    private WebElement fieldVideosAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-target-id")
    private WebElement fieldVideosCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-edit-entityconnect-field-videos-course-0-")
    private WebElement iconEditVideo;
    @FindBy(how = How.ID, using = "edit-field-images-additional-und-0-edit-entityconnect-field-images-additional-0-")
    private WebElement iconEditImage;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-add-entityconnect-field-images-course-0-")
    private WebElement iconAddImage;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-add-entityconnect-field-presentations-course-0-")
    private WebElement iconAddPresentation;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-add-entityconnect-field-videos-course-0-")
    private WebElement iconAddVideo;
    @FindBy(how = How.ID, using = "edit-field-presentations-additional-und-0-add-entityconnect-field-presentations-additional-0-")
    private WebElement iconAddPresentationAdditional;
    @FindBy(how = How.ID, using = "edit-field-videos-additional-und-0-add-entityconnect-field-videos-additional-0-")
    private WebElement iconAddVideoAdditional;
    @FindBy(how = How.ID, using = "edit-field-images-additional-und-0-add-entityconnect-field-images-additional-0-")
    private WebElement iconAddImageAdditional;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-target-id")
    private WebElement fieldImagesCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-images-additional-und-0-target-id")
    private WebElement fieldImagesAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-presentations-additional-und-0-target-id")
    private WebElement fieldPresentationsAdditionalMaterial;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-target-id")
    private WebElement fieldPresentationsCourseMaterial;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-title-additional-und-0-value")
    private WebElement fieldDownloadsTitle;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload")
    private WebElement fieldDownloadsFile;
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-remove-button")
    private WebElement buttonRemoveFile; // Downloads
    @FindBy(how = How.ID, using = "edit-field-downloads-additional-und-0-field-download-file-additional-und-0-upload-button")
    private WebElement buttonUpload; // Downloads


    /*---------CONSTRUCTORS--------*/
    public CreateTheoryPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/
    public void fillFieldTitle(String theoryTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(theoryTitle);
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public void fillFieldSummary (String summaryText) {
        waitUntilElementIsVisible(fieldSummary);
        fieldSummary.clear();
        fieldSummary.sendKeys(summaryText);
    }
    public WebElement getFieldChapter() {
        return fieldChapter;
    }
    public AddChapterPage clickIconAddChapter() throws Exception {
        clickItem(iconAddChapter, "The AddChapter icon on the CreateTheory page could not be clicked.");
        return new AddChapterPage(webDriver, locations);
    }
    public EditChapterPage clickIconEditChapter() throws Exception {
        clickItem(iconEditChapter, "The EditChapter icon on the CreateTheory page could not be clicked.");
        return new EditChapterPage(webDriver, locations);
    }
    public ViewTheoryPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateTheory page could not be clicked.");
        waitForPageToLoad();
        return new ViewTheoryPage(webDriver, locations);
    }
    public ViewTheoryPage clickButtonSaveTop() throws Exception {
        waitUntilElementIsClickable(buttonSaveTop);
        clickItem(buttonSaveTop, "The Save button at the top of the CreateTheory page could not be clicked.");
        waitForPageToLoad();
        return new ViewTheoryPage(webDriver, locations);
    }
    public void switchToTabEducationalContent() throws Exception {
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            tabEducationalContent.sendKeys(Keys.RETURN);
        } else {
            clickItem(tabEducationalContent, "The EducationalContentTab on the CreateTheory page could not be clicked.");
        }
    }
    public void switchToTabCourseMaterial() throws Exception {
        waitUntilElementIsVisible(tabCourseMaterial);
        clickItem(tabCourseMaterial, "The CourseMaterialTab on the CreateTheory page could not be clicked.");
    }
    public void switchToTabAdditionalMaterial() throws Exception {
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, tabAdditionalMaterial);
    }
    public WebElement getFieldLengthHours() {
        return fieldLengthHours;
    }
    public WebElement getFieldLengthMin() {
        return fieldLengthMin;
    }
    public WebElement getFrameWhatDoYouNeedToDo() {
        return frameWhatDoYouNeedToDo;
    }
    public WebElement getFrameWhatIsThisFor() {
        return frameWhatIsThisFor;
    }
    public WebElement getFrameLearningObjectives() {
        return frameLearningObjectives;
    }
    public WebElement getFieldVideosAdditionalMaterial() {
        return fieldVideosAdditionalMaterial;
    }
    public WebElement getFieldVideosCourseMaterial() {
        return fieldVideosCourseMaterial;
    }
    public EditVideoPage clickIconEditVideo() throws Exception {
        waitUntilElementIsClickable(iconEditVideo);
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            iconEditVideo.sendKeys(Keys.RETURN);
        } else clickItem(iconEditVideo, "The icon 'Edit Video' on the EditTheory page could not be clicked.");
        return new EditVideoPage(webDriver, locations);
    }
    public EditImagePage clickIconEditImage() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            iconEditImage.sendKeys(Keys.RETURN);
        } else clickItem(iconEditImage, "The icon 'Edit Image' on the EditTheory page could not be clicked.");
        return new EditImagePage(webDriver, locations);
    }
    public WebElement getIconAddPresentationAdditional() {
        return iconAddPresentationAdditional;
    }
    public WebElement getFieldImagesCourseMaterial() {
        return fieldImagesCourseMaterial;
    }
    public WebElement getFieldImagesAdditionalMaterial() {
        return fieldImagesAdditionalMaterial;
    }
    public WebElement getFieldPresentationsAdditionalMaterial() {
        return fieldPresentationsAdditionalMaterial;
    }
    public WebElement getFieldPresentationsCourseMaterial() {
        return fieldPresentationsCourseMaterial;
    }
    public void fillFieldDownloadsTitle(String title) {
        waitUntilElementIsVisible(fieldDownloadsTitle);
        fieldDownloadsTitle.sendKeys(title);
    }
    public void clickButtonUpload() throws Exception {
        waitUntilElementIsClickable(buttonUpload);
        clickItem(buttonUpload, "The button Upload on the CreateTheory page could not be clicked.");
    }
    public void uploadFileToDownloads(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        fieldDownloadsFile.sendKeys(path);
        clickButtonUpload();
        waitUntilElementIsVisible(buttonRemoveFile);
    }
    public CreateImagePage clickIconAddImage() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImage);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            iconAddImage.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImage, "The icon 'Add Image' on Edit page could not be clicked.");
        waitForPageToLoad();
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return new CreateImagePage(webDriver, locations);
    }
    public CreateVideoPage clickIconAddVideoAdditional() throws Exception {
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, iconAddVideoAdditional);
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitForPageToLoad();
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return new CreateVideoPage(webDriver, locations);
    }
    public WebElement getIconAddVideoAdditional() {
        return iconAddVideoAdditional;
    }
    public CreatePresentationPage clickIconAddPresentationAdditional() throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddPresentationAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddPresentationAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddPresentationAdditional, "The AddPresentation icon on Edit page could not be clicked.");
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return new CreatePresentationPage(webDriver, locations);
    }
    public CreateImagePage clickIconAddImageAdditional() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImageAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddImageAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImageAdditional, "The AddImage icon on Edit page could not be clicked.");
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return new CreateImagePage(webDriver, locations);
    }
    public WebElement getIconAddImage() {
        return iconAddImage;
    }
    public WebElement getIconAddPresentation() {
        return iconAddPresentation;
    }
    public WebElement getIconAddVideo() {
        return iconAddVideo;
    }

}
