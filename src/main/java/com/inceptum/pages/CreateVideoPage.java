package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreateVideoPage extends BasePage {

        /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "[id*='video'] #edit-title")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "[id*='video'] #edit-submit--2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select media")
    private WebElement linkSelectMedia;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-video-und-1')]/a[1]")
    private WebElement linkSelectMediaFor2ndVideo;
    @FindBy(how = How.XPATH, using = "//*[contains(@id, 'edit-field-video-und-2')]/a[1]")
    private WebElement linkSelectMediaFor3dVideo;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-video-und-0] a.button.remove")
    private WebElement linkRemoveMedia;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-video-und-1] a.button.remove")
    private WebElement linkRemoveMediaFor2ndVideo;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-video-und-2] a.button.remove")
    private WebElement linkRemoveMediaFor3dVideo;
    @FindBy(how = How.ID, using = "edit-field-video-und-add-more")
    private WebElement buttonAddAnotherItem;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.error li:nth-of-type(1)")
    private WebElement errorMessageTitle;
    @FindBy(how = How.CSS, using = ".messages.error li:nth-of-type(2)")
    private WebElement errorMessageVideo;
    @FindBy(how = How.CSS, using = ".tabledrag-changed-warning.messages.warning")
    private WebElement messageReordering;
    @FindBy(how = How.CSS, using = "#edit-field-video-und-0-preview>div>div")
    private WebElement thumbnailVideo;
    @FindBy(how = How.XPATH, using = "//*[@id=\"field-video-values\"]/tbody/tr[1]/td[1]/a")
    private WebElement firstVideoItem;
    @FindBy(how = How.XPATH, using = "//*[@id=\"field-video-values\"]/tbody/tr[2]/td[1]/a")
    private WebElement secondVideoItem;


    /*---------CONSTRUCTORS--------*/
    public CreateVideoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/
    public void fillFieldTitle(String videoTitle) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(videoTitle);
    }
    public WebElement getFieldTitle() throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public ViewVideoPage clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The Save button on the CreateVideo page could not be clicked.");
        waitForPageToLoad();
        return new ViewVideoPage(webDriver, locations);
    }
    public WebElement getButtonSave() {
        return buttonSave;
    }
    public ViewVideoPage clickButtonSaveTop() throws Exception {
        waitUntilElementIsClickable(buttonSaveTop);
        clickItem(buttonSaveTop, "The Save button at the top of the CreateVideo page could not be clicked.");
        waitForPageToLoad();
        return new ViewVideoPage(webDriver, locations);
    }
    public SelectVideoPage clickLinkSelectMedia() throws Exception {
        waitUntilElementIsClickable(linkSelectMedia);
        clickItem(linkSelectMedia, "The 'Select media' link on the CreateVideo page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public WebElement getLinkSelectMediaFor2ndVideo() {
        return linkSelectMediaFor2ndVideo;
    }
    public WebElement getLinkSelectMediaFor3dVideo() {
        return linkSelectMediaFor3dVideo;
    }
    public SelectVideoPage clickLinkSelectMediaFor2ndVideo() throws Exception {
        waitUntilElementIsClickable(linkSelectMediaFor2ndVideo);
        clickItem(linkSelectMediaFor2ndVideo, "The 'Select media' link for the 2nd video on the CreateVideo page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public SelectVideoPage clickLinkSelectMediaFor3dVideo() throws Exception {
        waitUntilElementIsClickable(linkSelectMediaFor3dVideo);
        clickItem(linkSelectMediaFor3dVideo, "The 'Select media' link for the 3d video on the CreateVideo page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public WebElement getLinkRemoveMedia() {
        return linkRemoveMedia;
    }
    public void clickLinkRemoveMedia() throws Exception {
        clickItem(linkRemoveMedia, "The link 'Remove Media' on the EditVideo page could not be clicked.");
    }
    public void checkLinkIsNotDisplayed() {
        Assert.assertEquals("display: none;", linkRemoveMedia.getAttribute("style"));
    }
    public WebElement getLinkRemoveMediaFor2ndVideo() {
        return linkRemoveMediaFor2ndVideo;
    }
    public WebElement getLinkRemoveMediaFor3dVideo() {
        return linkRemoveMediaFor3dVideo;
    }
    public void clickButtonAddAnotherItem() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem);
        clickItem(buttonAddAnotherItem, "The 'Add another item' button on the CreateVideo page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public void assertErrorMessage(String messageTitle, String messageVideo) throws Exception {
        waitUntilElementIsVisible(message);
        Assert.assertEquals("messages error", message.getAttribute("class"));
        // Check correct text of error message
        String errorVideoMessageTitle = errorMessageTitle.getText();
        String errorVideoMessageVideo = errorMessageVideo.getText();
        Assert.assertTrue("Detected incorrect message: " + errorVideoMessageTitle, errorVideoMessageTitle.matches(messageTitle));
        Assert.assertTrue("Detected incorrect message: " + errorVideoMessageVideo, errorVideoMessageVideo.matches("(?s)" + messageVideo));
    }
    public WebElement getMessageReordering() {
        return messageReordering;
    }
    public WebElement getThumbnailVideo() {
        return thumbnailVideo;
    }
    public WebElement getFirstVideoItem() {
        return firstVideoItem;
    }
    public WebElement getSecondVideoItem() {
        return secondVideoItem;
    }

}
