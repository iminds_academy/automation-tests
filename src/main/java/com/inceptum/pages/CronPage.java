package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CronPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-run")
    private WebElement buttonRunCron;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;



    /*---------CONSTRUCTORS--------*/
    public CronPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickButtonRunCron() throws Exception {
        waitUntilElementIsClickable(buttonRunCron);
        clickItem(buttonRunCron, "The button 'Run cron' on Cron page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }

}
