package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class DeleteItemConfirmationPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".confirmation #edit-submit")
    private WebElement buttonDelete;
    @FindBy(how = How.CSS, using = "#edit-submit[value*='Confirm']")
    private WebElement buttonConfirm;
    @FindBy(how = How.ID, using = "edit-cancel")
    private WebElement linkCancel;
    @FindBy(how = How.CSS, using = "#branding h1")
    private WebElement message;
    @FindBy(how = How.ID, using = "edit-mollom-feedback-")
    private WebElement checkboxDoNotReport;
    @FindBy(how = How.ID, using = "edit-verb-0")
    private WebElement checkboxDeleteClassMakeContentPublic;


    /*---------CONSTRUCTORS--------*/
    public DeleteItemConfirmationPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public  void clickButtonDelete() throws Exception {
        waitUntilElementIsClickable(buttonDelete);
        clickItem(buttonDelete, "The button 'Delete' on the Confirmation page could not be clicked.");
        waitForPageToLoad();
        Thread.sleep(500);
    }
    public WebElement getButtonDelete() {
        return buttonDelete;
    }
    public  void clickLinkCancel() throws Exception {
        waitUntilElementIsClickable(linkCancel);
        clickItem(linkCancel, "The link 'Cancel' on the Confirmation page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }
    public void clickCheckboxDoNotReport() throws Exception {
        waitUntilElementIsClickable(checkboxDoNotReport);
        clickItem(checkboxDoNotReport, "The checkbox 'Do not report' on the Confirmation page could not be clicked.");
    }
    public  void clickButtonConfirm() throws Exception {
        waitUntilElementIsClickable(buttonConfirm);
        clickItem(buttonConfirm, "The button 'Confirm' on the Confirmation page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".messages.status")));
    }
    public void clickCheckboxDeleteClassMakeContentPublic() throws Exception {
        waitUntilElementIsClickable(checkboxDeleteClassMakeContentPublic);
        clickItem(checkboxDeleteClassMakeContentPublic, "The checkbox 'Delete the class and make the content public' on " +
                "the Confirmation page could not be clicked.");
    }

}
