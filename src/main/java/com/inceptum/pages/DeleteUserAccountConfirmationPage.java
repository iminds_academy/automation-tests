package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class DeleteUserAccountConfirmationPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonCancelAccounts; // buttonCancelAccount
    @FindBy(how = How.CSS, using = "#branding h1")
    private WebElement message;
    @FindBy(how = How.ID, using = "edit-mollom-feedback-")
    private WebElement checkboxDoNotReport;
    @FindBy(how = How.CSS, using = "[value='user_cancel_delete']")
    private WebElement checkboxDeleteAccountAndItsContent;
    @FindBy(how = How.CSS, using = "[value='user_cancel_block']")
    private WebElement checkboxDisableAccountAndKeepContent;
    @FindBy(how = How.ID, using = "edit-state")
    private WebElement fieldState;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonConfirm;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonNext;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;



    /*---------CONSTRUCTORS--------*/
    public DeleteUserAccountConfirmationPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickButtonCancelAccounts() throws Exception {
        waitUntilElementIsClickable(buttonCancelAccounts);
        clickItem(buttonCancelAccounts, "The button 'Cancel accounts' on the Confirmation page could not be clicked.");
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains("People"));
    }
    public WhoIsWhoPage clickButtonCancelAccount() throws Exception { // WhoIsWho page
        waitUntilElementIsClickable(buttonCancelAccounts);
        clickItem(buttonCancelAccounts, "The button 'Cancel account' on the Confirmation page could not be clicked.");
        return new WhoIsWhoPage(webDriver, locations);
    }
    public WebElement getMessage() {
        return message;
    }
    public void selectCheckboxDoNotReport() throws Exception {
        waitUntilElementIsClickable(checkboxDoNotReport);
        clickItem(checkboxDoNotReport, "The checkbox 'Do not report' on the Confirmation page could not be clicked.");
    }
    public void selectCheckboxDeleteAccountAndItsContent() throws Exception {
        waitUntilElementIsClickable(checkboxDeleteAccountAndItsContent);
        clickItem(checkboxDeleteAccountAndItsContent, "The checkbox 'Delete account and its content' on the Confirmation page could not be clicked.");
    }
    public void selectCheckboxDisableAccountAndKeepContent() throws Exception {
        waitUntilElementIsClickable(checkboxDisableAccountAndKeepContent);
        clickItem(checkboxDisableAccountAndKeepContent,
                "The option 'Disable the account and keep its content.' on the Confirmation page could not be selected.");
    }
    public WebElement getFieldState() {
        return fieldState;
    }
    public  WhoIsWhoPage clickButtonConfirm() throws Exception {
        waitUntilElementIsClickable(buttonConfirm);
        clickItem(buttonConfirm, "The button 'Confirm' on the Confirmation page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".messages.messages--status")));
        return new WhoIsWhoPage(webDriver, locations);
    }
    public  void clickButtonNext() throws Exception {
        waitUntilElementIsClickable(buttonNext);
        clickItem(buttonNext, "The button 'Next' on the Confirmation page could not be clicked.");
        waitForPageToLoad();
    }
    public  void clickLinkCancel() throws Exception {
        waitUntilElementIsClickable(linkCancel);
        clickItem(linkCancel, "The link 'Cancel' on the Confirmation page could not be clicked.");
        waitForPageToLoad();
    }



}
