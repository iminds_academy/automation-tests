package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class DownloadsPopupInlineEditingPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-add-more")
    private WebElement buttonAddAnotherItem;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-title-course-und-0-value")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-title-course-und-0-value")
    private WebElement fieldTitle2;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload")
    private WebElement fieldUploadFile;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-upload")
    private WebElement fieldUploadFile2;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-upload-button")
    private WebElement buttonUpload;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-upload-button")
    private WebElement buttonUpload2;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-field-download-file-course-und-0-remove-button")
    private WebElement buttonRemove;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-1-field-download-file-course-und-0-remove-button")
    private WebElement buttonRemove2;
    @FindBy(how = How.ID, using = "edit-field-downloads-course-und-0-remove-button")
    private WebElement buttonRemoveFieldSet;


    /*---------CONSTRUCTORS--------*/

    public DownloadsPopupInlineEditingPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickButtonAddAnotherItem() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem);
        clickItem(buttonAddAnotherItem, "The button AddAnotherItem on the Downloads popup could not be clicked.");
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(title);
    }
    public void fillFieldTitle2(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle2);
        fieldTitle2.sendKeys(title);
    }
    public WebElement getFieldUploadFile() {
        waitUntilElementIsVisible(fieldUploadFile);
        return fieldUploadFile;
    }
    public WebElement getFieldUploadFile2() {
        waitUntilElementIsVisible(fieldUploadFile2);
        return fieldUploadFile2;
    }
    public void clickButtonUpload() throws Exception {
        waitUntilElementIsClickable(buttonUpload);
        clickItem(buttonUpload, "The button Upload on the Downloads popup could not be clicked.");
    }
    public void clickButtonUpload2() throws Exception {
        waitUntilElementIsClickable(buttonUpload2);
        clickItem(buttonUpload2, "The button Upload for the 2nd fieldset on the Downloads popup could not be clicked.");
    }
    public WebElement getButtonRemove() {
        return buttonRemove;
    }
    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The button Remove on the Downloads popup could not be clicked.");
        waitUntilElementIsVisible(buttonUpload);
    }
    public WebElement getButtonRemove2() {
        return buttonRemove2;
    }
    public void clickButtonRemoveFieldSet() throws Exception {
        waitUntilElementIsClickable(buttonRemoveFieldSet);
        clickItem(buttonRemoveFieldSet, "The button Remove for fieldset on the Downloads popup could not be clicked.");
        waitUntilElementIsVisible(buttonUpload);
    }


}
