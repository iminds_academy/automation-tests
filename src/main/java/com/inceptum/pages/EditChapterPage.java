package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditChapterPage extends AddChapterPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "form[action*='chapter'] #edit-delete")
    private WebElement buttonDelete;


    /*---------CONSTRUCTORS--------*/
    public EditChapterPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public DeleteItemConfirmationPage clickButtonDelete() throws Exception {
        waitUntilElementIsClickable(buttonDelete);
        clickItem(buttonDelete, "The button Delete on the EditChapter page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public WebElement getButtonDelete() {
        return buttonDelete;
    }

}
