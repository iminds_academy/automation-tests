package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditClassPage extends CreateClassPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;


    /*---------CONSTRUCTORS--------*/
    public EditClassPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public DeleteItemConfirmationPage clickButtonDeleteTop() throws Exception {
        waitUntilElementIsClickable(buttonDeleteTop);
        clickItem(buttonDeleteTop, "The button DeleteTop on the EditClassPage could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }


}
