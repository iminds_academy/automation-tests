package com.inceptum.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditEmbedContentPage extends CreateEmbedContentPage{
     /* ----- FIELDS ----- */
     @FindBy(how = How.ID, using = "edit-delete")
     private WebElement buttonDeleteBottom;






    /*---------CONSTRUCTORS--------*/

    public EditEmbedContentPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public DeleteItemConfirmationPage clickButtonDeleteBottom() throws Exception {
        waitUntilElementIsClickable(buttonDeleteBottom);
        clickItem(buttonDeleteBottom, "The Delete button at the top of the Embed Content page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }


}
