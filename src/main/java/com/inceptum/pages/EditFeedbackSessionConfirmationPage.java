package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditFeedbackSessionConfirmationPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonConfirm;            // doesn't exist anymore?

    /*---------CONSTRUCTORS--------*/
    public EditFeedbackSessionConfirmationPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

//    public  void clickButtonConfirm() throws Exception {
//        waitUntilElementIsClickable(buttonConfirm);
//        clickItem(buttonConfirm, "The button 'Confirm' on the Confirmation page could not be clicked.");
//        Thread.sleep(500);
//    }

}
