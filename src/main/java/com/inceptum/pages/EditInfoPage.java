package com.inceptum.pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 24.11.2014.
 */
public class EditInfoPage extends CreateInfoPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Groups audience")
    private WebElement linkGroupsAudience;
    @FindBy(how = How.ID, using = "edit-og-group-ref-und-0-default")
    private WebElement fieldYourGroups;
    @FindBy(how = How.ID, using = "edit-field-info-sections-und-add-more")
    private WebElement buttonAddAnotherItem;
    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;


    /*---------CONSTRUCTORS--------*/

    public EditInfoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickLinkGroupsAudience() throws Exception {
        waitUntilElementIsVisible(buttonDeleteTop);
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' on the EditInfoPage could not be clicked.");
    }
    public String getClassName() throws Exception {
        clickLinkGroupsAudience();
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String className = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                className = option.getText();
                break;
            }
        }
        return className;
    }
    public String getClassNode() {
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String classNode = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                classNode = option.getAttribute("value");
                break;
            }
        }
        return classNode;
    }
    public String getChapterName() throws Exception {
        waitUntilElementIsVisible(getFieldChapter());
        List<WebElement> options = getFieldChapter().findElements(By.tagName("option"));
        String chapterName = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                chapterName = option.getText();
                break;
            }
        }
        return chapterName;
    }
    public String getChapterID() {
        waitUntilElementIsVisible(getFieldChapter());
        List<WebElement> options = getFieldChapter().findElements(By.tagName("option"));
        String chapterID = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                chapterID = option.getAttribute("value");
                break;
            }
        }
        return chapterID;
    }
    public void clickButtonAddAnotherItem() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem);
        clickItem(buttonAddAnotherItem, "The button 'Add another item' on the EditInfoPage could not be clicked.");
        waitUntilElementIsVisible(getFieldSubtitle2());
    }
    public DeleteItemConfirmationPage clickButtonDeleteTop() throws Exception {
        waitUntilElementIsClickable(buttonDeleteTop);
        clickItem(buttonDeleteTop, "The top Delete button on the EditInfoPage could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }


}
