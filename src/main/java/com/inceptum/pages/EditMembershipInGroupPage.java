package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditMembershipInGroupPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonUpdateMembership;
    @FindBy(how = How.ID, using = "edit-roles")
    private WebElement fildsetRoles;
    @FindBy(how = How.ID, using = "edit-state")
    private WebElement fieldStatus;
    @FindBy(how = How.LINK_TEXT, using = "Remove")
    private WebElement linkRemove;


    /* ----- CONSTRUCTORS ----- */

    public EditMembershipInGroupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public void clickButtonUpdateMembership() throws Exception {
        waitUntilElementIsClickable(buttonUpdateMembership);
        clickItem(buttonUpdateMembership, "The button 'Update membership' on the EditMembershipInGroup page could not be clicked.");
    }
    public void selectRole(String role) {
        WebElement checkbox = fildsetRoles.findElement(By.xpath("./div/label[contains(text(), '" + role + "')]/../input"));
        checkbox.click();
    }
    public WebElement getFieldStatus() {
        return fieldStatus;
    }
    public RemoveMembershipInGroupPage clickLinkRemove() throws Exception {
        waitUntilElementIsClickable(linkRemove);
        clickItem(linkRemove, "The link 'Remove' on the EditMembershipInGroup page could not be clicked.");
        return new RemoveMembershipInGroupPage(webDriver, locations);
    }


}
