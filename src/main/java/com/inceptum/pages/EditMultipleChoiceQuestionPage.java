package com.inceptum.pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class EditMultipleChoiceQuestionPage extends CreateMultipleChoiceQuestionPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-delete--2")
    private WebElement buttonDelete;
    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;


    /*---------CONSTRUCTORS--------*/
    public EditMultipleChoiceQuestionPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public DeleteItemConfirmationPage clickButtonDelete() throws Exception {
        clickItem(buttonDelete, "The Delete button on the EditMCQ page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public DeleteItemConfirmationPage clickButtonDeleteTop() throws Exception {
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with clicking an element that is not in focus
            buttonDeleteTop.sendKeys(Keys.RETURN);
        } else clickItem(buttonDeleteTop, "The Delete button at the top of the EditMCQ page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }

}
