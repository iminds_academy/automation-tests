package com.inceptum.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class EditPeerAssignmentPage extends CreatePeerAssignmentPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Select media")
    private WebElement linkSelectMedia;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-multimedia-section-und-1] a.button.launcher")
    private WebElement linkSelectMedia2;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-multimedia] a.button.remove")
    private WebElement linkRemoveMedia;
    @FindBy(how = How.CSS, using = "div[id^=edit-field-multimedia-section-und-1] a.button.remove")
    private WebElement linkRemoveMedia2;
    @FindBy(how = How.CSS, using = "#cke_edit-field-multimedia-section-und-1-field-mmsection-description-und-0-value iframe")
    private WebElement frameDescriptionAnotherItemMainContent;
    @FindBy(how = How.ID, using = "edit-field-multimedia-section-und-1-remove-button")
    private WebElement buttonRemoveAnotherItemMainContent;
    @FindBy(how = How.ID, using = "edit-field-presentations-course-und-0-add-entityconnect-field-presentations-course-0-")
    private WebElement iconAddPresentation;
    @FindBy(how = How.ID, using = "edit-field-presentations-peertask-und-0-add-entityconnect-field-presentations-peertask-0-")
    private WebElement iconAddPresentationAdditional;
    @FindBy(how = How.ID, using = "edit-field-videos-course-und-0-add-entityconnect-field-videos-course-0-")
    private WebElement iconAddVideo;
    @FindBy(how = How.ID, using = "edit-field-videos-peertask-und-0-add-entityconnect-field-videos-peertask-0-")
    private WebElement iconAddVideoAdditional;
    @FindBy(how = How.ID, using = "edit-field-images-course-und-0-add-entityconnect-field-images-course-0-")
    private WebElement iconAddImage;
    @FindBy(how = How.ID, using = "edit-field-images-peertask-und-0-add-entityconnect-field-images-peertask-0-")
    private WebElement iconAddImageAdditional;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-0-field-performance-levels-und-0-field-level-name-und-0-value")
    private WebElement fieldLevelName;
    @FindBy(how = How.CSS, using = "[id*='0-field-level-rationale'] iframe")
    private WebElement frameRationale;
    @FindBy(how = How.CSS, using = "[id*='0-field-level-remediation'] iframe")
    private WebElement frameSuggestedRemediation;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-0-field-performance-levels-und-add-more")
    private WebElement buttonAddAnotherItemLevel;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-0-field-performance-levels-und-5-field-level-name-und-0-value")
    private WebElement fieldLevelName2;
    @FindBy(how = How.CSS, using = "[id*='5-field-level-rationale'] iframe")
    private WebElement frameRationale2;
    @FindBy(how = How.CSS, using = "[id*='5-field-level-remediation'] iframe")
    private WebElement frameSuggestedRemediation2;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-add-more")
    private WebElement buttonAddAnotherItemCriteria;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-0-field-performance-levels-und-5-remove-button")
    private WebElement buttonRemoveLevel2;
    @FindBy(how = How.ID, using = "edit-field-evaluation-criteria-und-1-remove-button")
    private WebElement buttonRemoveCriteria2;
    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Groups audience")
    private WebElement linkGroupsAudience;
    @FindBy(how = How.ID, using = "edit-og-group-ref-und-0-default")
    private WebElement fieldYourGroups;


    /*---------CONSTRUCTORS--------*/

    public EditPeerAssignmentPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public SelectVideoPage clickLinkSelectMedia() throws Exception {
        waitUntilElementIsClickable(linkSelectMedia);
        clickItem(linkSelectMedia, "The 'Select media' link on the EditPeerAssignment page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public SelectVideoPage clickLinkSelectMedia2() throws Exception {
        waitUntilElementIsClickable(linkSelectMedia2);
        clickItem(linkSelectMedia2, "The 'Select media' link for another item on the EditPeerAssignment page could not be clicked.");
        return new SelectVideoPage(webDriver, locations);
    }
    public WebElement getLinkRemoveMedia() {
        return linkRemoveMedia;
    }
    public WebElement getLinkRemoveMedia2() {
        waitUntilElementIsVisible(linkRemoveMedia2);
        return linkRemoveMedia2;
    }
    public void clickLinkRemoveMedia2() throws Exception {
        waitUntilElementIsClickable(linkRemoveMedia2);
        clickItem(linkRemoveMedia2, "The 'Remove media' link for another item on the EditPeerAssignment page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div[id^=edit-field-multimedia-section-und-1] a.button.remove")));
    }
    public WebElement getFrameDescriptionAnotherItemMainContent() {
        return frameDescriptionAnotherItemMainContent;
    }
    public void clickButtonRemoveAnotherItemMainContent() throws Exception {
        waitUntilElementIsClickable(buttonRemoveAnotherItemMainContent);
        clickItem(buttonRemoveAnotherItemMainContent, "The button 'Remove' for another item main content on the EditPeerAssignment page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-field-multimedia-section-und-1-remove-button")));
    }
    public CreatePresentationPage clickIconAddPresentation() throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddPresentation);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddPresentation.sendKeys(Keys.RETURN);
        } else clickItem(iconAddPresentation, "The AddPresentation icon on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return createPresentationPage;
    }
    public CreatePresentationPage clickIconAddPresentationAdditional() throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddPresentationAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddPresentationAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddPresentationAdditional, "The AddPresentation icon on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(createPresentationPage.getFieldTitle());
        return createPresentationPage;
    }
    public CreateVideoPage clickIconAddVideo() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideo);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddVideo.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideo, "The AddVideo icon on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return createVideoPage;
    }
    public CreateVideoPage clickIconAddVideoAdditional() throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        waitUntilElementIsClickable(iconAddVideoAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddVideoAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddVideoAdditional, "The AddVideo icon on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(createVideoPage.getFieldTitle());
        return createVideoPage;
    }
    public CreateImagePage clickIconAddImage() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImage);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddImage.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImage, "The AddImage icon on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return createImagePage;
    }
    public CreateImagePage clickIconAddImageAdditional() throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        waitUntilElementIsClickable(iconAddImageAdditional);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox: problem with clicking an element that is not in focus
            iconAddImageAdditional.sendKeys(Keys.RETURN);
        } else clickItem(iconAddImageAdditional, "The AddImage icon on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(createImagePage.getFieldTitle());
        return createImagePage;
    }
    public void fillFieldLevelName(String title) throws Exception {
        waitUntilElementIsVisible(fieldLevelName);
        fieldLevelName.clear();
        fieldLevelName.sendKeys(title);
    }
    public WebElement getFrameRationale() {
        return frameRationale;
    }
    public WebElement getFrameSuggestedRemediation() {
        return frameSuggestedRemediation;
    }
    public WebElement getFrameRationale2() {
        return frameRationale2;
    }
    public WebElement getFrameSuggestedRemediation2() {
        return frameSuggestedRemediation2;
    }
    public void clickButtonAddAnotherItemLevel() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemLevel);
        clickItem(buttonAddAnotherItemLevel, "The button 'Add another item' for new performance level on the CreatePeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(fieldLevelName2);
    }
    public void fillFieldLevelName2(String title) throws Exception {
        waitUntilElementIsVisible(fieldLevelName2);
        fieldLevelName2.sendKeys(title);
    }
    public void clickButtonAddAnotherItemCriteria() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItemCriteria);
        clickItem(buttonAddAnotherItemCriteria, "The button 'Add another item' for new evaluation criteria on the CreatePeerAssignment page could not be clicked.");
        CreatePeerAssignmentPage createPeerAssignmentPage = new CreatePeerAssignmentPage(webDriver, locations);
        waitUntilElementIsVisible(createPeerAssignmentPage.getFieldCriteriaName2());
    }
    public void clickButtonRemoveLevel2() throws Exception {
        waitUntilElementIsClickable(buttonRemoveLevel2);
        clickItem(buttonRemoveLevel2, "The button 'Remove' for the 2nd level of the 1st criteria on the CreatePeerAssignment page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        String idFieldNameLevel2 = "edit-field-evaluation-criteria-und-0-field-performance-levels-und-5-field-level-name-und-0-value";
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(idFieldNameLevel2)));
    }
    public void clickButtonRemoveCriteria2() throws Exception {
        waitUntilElementIsClickable(buttonRemoveCriteria2);
        clickItem(buttonRemoveCriteria2, "The button 'Remove' for the 2nd criteria on the CreatePeerAssignment page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        String idFieldNameCriteria2 = "edit-field-evaluation-criteria-und-1-field-criterion-name-und-0-value";
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id(idFieldNameCriteria2)));
    }
    public void clickLinkGroupsAudience() throws Exception {
        waitUntilElementIsVisible(buttonDeleteTop);
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' on the EditPeerAssignment page could not be clicked.");
    }
    public String getClassNode() {
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String classNode = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                classNode = option.getAttribute("value");
                break;
            }
        }
        return classNode;
    }
    public String getChapterName() throws Exception {
        waitUntilElementIsVisible(getFieldChapter());
        List<WebElement> options = getFieldChapter().findElements(By.tagName("option"));
        String chapterName = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                chapterName = option.getText();
                break;
            }
        }
        return chapterName;
    }
    public String getChapterID() {
        waitUntilElementIsVisible(getFieldChapter());
        List<WebElement> options = getFieldChapter().findElements(By.tagName("option"));
        String chapterID = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                chapterID = option.getAttribute("value");
                break;
            }
        }
        return chapterID;
    }



}
