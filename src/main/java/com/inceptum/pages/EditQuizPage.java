package com.inceptum.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditQuizPage extends CreateQuizPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Groups audience")
    private WebElement linkGroupsAudience;
    @FindBy(how = How.ID, using = "edit-og-group-ref-und-0-default")
    private WebElement fieldYourGroups;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Pass/fail options")
    private WebElement linkPassFailOptions;
    @FindBy(how = How.ID, using = "edit-pass-rate")
    private WebElement fieldPassRateForQuiz;


    /*---------CONSTRUCTORS--------*/

    public EditQuizPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickLinkGroupsAudience() throws Exception {
        waitUntilElementIsVisible(buttonDeleteTop);
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' on the EditQuiz page could not be clicked.");
    }
    public String getClassName() throws Exception {
        clickLinkGroupsAudience();
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String className = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                className = option.getText();
                break;
            }
        }
        return className;
    }
    public String getClassNode() {
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String classNode = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                classNode = option.getAttribute("value");
                break;
            }
        }
        return classNode;
    }

    public void checkPassRateIsEqual(int passRate) throws Exception {
        // Click the link 'Pass/fail options'
        waitUntilElementIsClickable(linkPassFailOptions);
        clickItem(linkPassFailOptions, "The link 'Pass/fail options' on the EditQuiz page could not be clicked.");
        waitUntilElementIsVisible(fieldPassRateForQuiz);
        String passRateActual = fieldPassRateForQuiz.getAttribute("value"); // get actual pass rate
        String passRateExpected = Integer.toString(passRate); // convert expected pass rate int value into string
        if (!(passRateActual.equals(passRateExpected))) { // enter expected value in the field if actual pass rate different
            fieldPassRateForQuiz.clear();
            fieldPassRateForQuiz.sendKeys(passRateExpected);
        }
        scrollToElement(getButtonSave());
        clickButtonSave(); // save changes
    }



}
