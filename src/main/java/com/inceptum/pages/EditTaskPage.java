package com.inceptum.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditTaskPage extends CreateTaskPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-delete--2")
    private WebElement buttonDelete;
    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement messageCreation;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Groups audience")
    private WebElement linkGroupsAudience;
    @FindBy(how = How.ID, using = "edit-og-group-ref-und-0-default")
    private WebElement fieldYourGroups;

    /*---------CONSTRUCTOR--------*/

    public EditTaskPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public DeleteItemConfirmationPage clickButtonDelete () throws Exception {
        clickItem(buttonDelete, "The Delete button on the EditTask page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public DeleteItemConfirmationPage clickButtonDeleteTop() throws Exception {
        waitUntilElementIsClickable(buttonDeleteTop);
        clickItem(buttonDeleteTop, "The Delete button at the top of the EditTask page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public WebElement getMessageCreation() {
        return messageCreation;
    }
    public WebElement getMessage() {
        return message;
    }
    public void clickLinkGroupsAudience() throws Exception {
        waitUntilElementIsVisible(buttonDeleteTop);
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' on the EditTask page could not be clicked.");
    }
    public String getClassName() throws Exception {
        clickLinkGroupsAudience();
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String className = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                className = option.getText();
                break;
            }
        }
        return className;
    }
    public String getClassNode() {
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String classNode = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                classNode = option.getAttribute("value");
                break;
            }
        }
        return classNode;
    }
    public String getChapterName() throws Exception {
        waitUntilElementIsVisible(getFieldChapter());
        List<WebElement> options = getFieldChapter().findElements(By.tagName("option"));
        String chapterName = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                chapterName = option.getText();
                break;
            }
        }
        return chapterName;
    }
    public String getChapterID() {
        waitUntilElementIsVisible(getFieldChapter());
        List<WebElement> options = getFieldChapter().findElements(By.tagName("option"));
        String chapterID = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                chapterID = option.getAttribute("value");
                break;
            }
        }
        return chapterID;
    }

}
