package com.inceptum.pages;


import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class EditUserAccountPage extends AddUserPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-cancel")
    private WebElement buttonCancelAccount;
    @FindBy(how = How.XPATH, using = "//a/strong[text()='Profile settings']")
    private WebElement tabProfileSettings;


    /*---------CONSTRUCTORS--------*/
    public EditUserAccountPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The button Save on the AddUser page could not be clicked.");
        waitForPageToLoad();
    }
    public DeleteUserAccountConfirmationPage clickButtonCancelAccount(List<String> user) throws Exception {
        waitUntilElementIsClickable(buttonCancelAccount);
        clickItem(buttonCancelAccount, "The button 'Cancel account' on the EditUserAccount page could not be clicked.");
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains("Are you sure you want to cancel the account " + user.get(0)));
        return new DeleteUserAccountConfirmationPage(webDriver, locations);
    }
    public void clearAndFillInFields(String fullName, String email, String password) {
        getFieldFullName().clear();
        getFieldFullName().sendKeys(fullName);

        getFieldEmail().clear();
        getFieldEmail().sendKeys(email);

        getFieldPassword().sendKeys(password);
        getFieldConfirmPassword().sendKeys(password);
    }
    public ProfileSettingsPage switchToTabProfileSettings() throws Exception {
        waitUntilElementIsClickable(tabProfileSettings);
        clickItem(tabProfileSettings, "The tab 'Profile settings' on the EditUserAccount page could not be clicked.");
        return new ProfileSettingsPage(webDriver, locations);
    }


}
