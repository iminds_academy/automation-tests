package com.inceptum.pages;


import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class EditVideoPage extends CreateVideoPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-delete--2")
    private WebElement buttonDelete;
    @FindBy(how = How.ID, using = "edit-delete")
    private WebElement buttonDeleteTop;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Groups audience")
    private WebElement linkGroupsAudience;
    @FindBy(how = How.ID, using = "edit-og-group-ref-und-0-default")
    private WebElement fieldYourGroups;

    /*---------CONSTRUCTOR--------*/

    public EditVideoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getButtonDelete() {
        return buttonDelete;
    }
    public DeleteItemConfirmationPage clickButtonDelete() throws Exception {
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE: problem with clicking an element that is not in focus
            buttonDelete.sendKeys(Keys.RETURN);
        } else clickItem(buttonDelete, "The Delete button on the EditVideo page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public DeleteItemConfirmationPage clickButtonDeleteTop() throws Exception {
        clickItem(buttonDeleteTop, "The Delete button at the top of the EditVideo page could not be clicked.");
        waitForPageToLoad();
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public void clickLinkGroupsAudience() throws Exception {
        waitUntilElementIsVisible(buttonDeleteTop);
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' on the EditVideo page could not be clicked.");
    }
    public String getClassName() throws Exception {
        clickLinkGroupsAudience();
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String className = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                className = option.getText();
                break;
            }
        }
        return className;
    }
    public String getClassNode() {
        waitUntilElementIsVisible(fieldYourGroups);
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        String classNode = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                classNode = option.getAttribute("value");
                break;
            }
        }
        return classNode;
    }

}
