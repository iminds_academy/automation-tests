package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class EditYourProfilePage extends AddUserPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-mail")
    private WebElement fieldEmailAddress;
    @FindBy(how = How.ID, using = "edit-field-profile-full-name-und-0-value")
    private WebElement fieldFullName;


    /*---------CONSTRUCTORS--------*/
    public EditYourProfilePage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void checkNewEmailAddressIsDisplayed(String email) throws Exception {
        waitUntilElementIsVisible(fieldEmailAddress);
        String actualEmail = fieldEmailAddress.getAttribute("value");
        Assert.assertTrue("E-mail address is wrong.", actualEmail.equals(email));
    }
    public WebElement getFieldFullName() {
        waitUntilElementIsVisible(fieldFullName);
        return fieldFullName;
    }

}
