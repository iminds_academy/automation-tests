package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class FeedbackEvaluationPage extends HomePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-field-navigation")
    private WebElement fieldGroup;
    @FindBy(how = How.ID, using = "edit-field-nabc-need-comment-und-0-value")
    private WebElement fieldNeed;
    @FindBy(how = How.ID, using = "edit-field-nabc-approach-comment-und-0-value")
    private WebElement fieldApproach;
    @FindBy(how = How.ID, using = "edit-field-nabc-benefit-comment-und-0-value")
    private WebElement fieldBenefit;
    @FindBy(how = How.ID, using = "edit-field-nabc-competition-comment-und-0-value")
    private WebElement fieldCompetition;
    @FindBy(how = How.ID, using = "edit-field-team-communicate-comment-und-0-value")
    private WebElement fieldCommunicate;
    @FindBy(how = How.ID, using = "edit-field-team-achieved-comment-und-0-value")
    private WebElement fieldAchieved;
    @FindBy(how = How.ID, using = "edit-field-team-complementary-comment-und-0-value")
    private WebElement fieldComplementary;
    @FindBy(how = How.ID, using = "edit-field-presentation-quality-comme-und-0-value")
    private WebElement fieldQuality;
    @FindBy(how = How.ID, using = "edit-field-business-potential-comment-und-0-value")
    private WebElement fieldExitEvent;
    @FindBy(how = How.ID, using = "edit-field-business-potential-idea-co-und-0-value")
    private WebElement fieldIdea;
    @FindBy(how = How.ID, using = "edit-field-business-potential-feas-co-und-0-value")
    private WebElement fieldFeasibility;
    @FindBy(how = How.ID, using = "edit-field-presentation-overall-und-0-value")
    private WebElement fieldPresentation;
    @FindBy(how = How.ID, using = "edit-field-presentation-q-a-und-0-value")
    private WebElement fieldQA;

    @FindBy(how = How.CSS, using = "#edit-field-nabc-need-evaluation .star-button[value='0']")
    private WebElement iconOneStarNeed;
    @FindBy(how = How.CSS, using = "#edit-field-presentation-quality-evalu .star-button[value='0']")
    private WebElement iconOneStarQuality;
    @FindBy(how = How.CSS, using = "#edit-field-business-potential-evaluat .star-button[value='0']")
    private WebElement iconOneStarExitEvent;
    @FindBy(how = How.CSS, using = "#edit-field-nabc-approach-evaluation .star-button[value='1']")
    private WebElement iconTwoStarsApproach;
    @FindBy(how = How.CSS, using = "#edit-field-team-complementary-evaluat .star-button[value='1']")
    private WebElement iconTwoStarsComplementary;
    @FindBy(how = How.CSS, using = "#edit-field-nabc-benefit-evaluation .star-button[value='2']")
    private WebElement iconThreeStarsBenefit;
    @FindBy(how = How.CSS, using = "#edit-field-team-achieved-evaluation .star-button[value='2']")
    private WebElement iconThreeStarsAchieved;
    @FindBy(how = How.CSS, using = "#edit-field-nabc-competition-evaluatio .star-button[value='3']")
    private WebElement iconFourStarsCompetition;
    @FindBy(how = How.CSS, using = "#edit-field-team-communicate-evaluatio .star-button[value='3']")
    private WebElement iconFourStarsCommunicate;
    @FindBy(how = How.CSS, using = "#edit-field-business-potential-idea-ev .star-button[value='3']")
    private WebElement iconFourStarsIdea;
    @FindBy(how = How.CSS, using = "#edit-field-business-potential-feas-ev .star-button[value='3']")
    private WebElement iconFourStarsFeasibility;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "NABC")
    private WebElement tabNABC;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Team")
    private WebElement tabTeam;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Presentation")
    private WebElement tabPresentation;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Business potential")
    private WebElement tabBusinessPotential;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Overall advice")
    private WebElement tabOverallAdvice;
    @FindBy(how = How.ID, using = "edit-save2")
    private WebElement buttonSave;
    @FindBy(how = How.ID, using = "edit-submit2")
    private WebElement buttonSubmit;


    /* ----- CONSTRUCTORS ----- */

    public FeedbackEvaluationPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public WebElement getFieldGroup() {
        return fieldGroup;
    }
    public void fillFieldNeed(String commentNeed) {
        waitUntilElementIsVisible(fieldNeed);
        fieldNeed.sendKeys(commentNeed);
    }
    public void fillFieldApproach(String commentApproach) {
        waitUntilElementIsVisible(fieldApproach);
        fieldApproach.sendKeys(commentApproach);
    }
    public void fillFieldBenefit(String commentBenefit) {
        waitUntilElementIsVisible(fieldBenefit);
        fieldBenefit.sendKeys(commentBenefit);
    }
    public void fillFieldCompetition(String commentCompetition) {
        waitUntilElementIsVisible(fieldCompetition);
        fieldCompetition.sendKeys(commentCompetition);
    }
    public void fillFieldCommunicate(String commentCommunicate) {
        waitUntilElementIsVisible(fieldCommunicate);
        fieldCommunicate.sendKeys(commentCommunicate);
    }
    public void fillFieldAchieved(String commentAchieved) {
        waitUntilElementIsVisible(fieldAchieved);
        fieldAchieved.sendKeys(commentAchieved);
    }
    public void fillFieldComplementary(String commentComplementary) {
        waitUntilElementIsVisible(fieldComplementary);
        fieldComplementary.sendKeys(commentComplementary);
    }
    public void fillFieldQuality(String commentQuality) {
        waitUntilElementIsVisible(fieldQuality);
        fieldQuality.sendKeys(commentQuality);
    }
    public void fillFieldExitEvent(String commentExitEvent) {
        waitUntilElementIsVisible(fieldExitEvent);
        fieldExitEvent.sendKeys(commentExitEvent);
    }
    public void fillFieldIdea(String commentIdea) {
        waitUntilElementIsVisible(fieldIdea);
        fieldIdea.sendKeys(commentIdea);
    }
    public void fillFieldFeasibility(String commentFeasibility) {
        waitUntilElementIsVisible(fieldFeasibility);
        fieldFeasibility.sendKeys(commentFeasibility);
    }
    public void fillFieldPresentation(String commentPresentation) {
        waitUntilElementIsVisible(fieldPresentation);
        fieldPresentation.sendKeys(commentPresentation);
    }
    public void fillFieldQA(String commentQA) {
        waitUntilElementIsVisible(fieldQA);
        fieldQA.sendKeys(commentQA);
    }

    public void clickIconOneStarNeed() throws Exception {
        waitUntilElementIsClickable(iconOneStarNeed);
        clickItem(iconOneStarNeed, "The 'one star' icon for NeedField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconOneStarExitEvent() throws Exception {
        waitUntilElementIsClickable(iconOneStarExitEvent);
        clickItem(iconOneStarExitEvent, "The 'one star' icon for ExitEventField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconOneStarQuality() throws Exception {
        waitUntilElementIsClickable(iconOneStarQuality);
        clickItem(iconOneStarQuality, "The 'one star' icon for QualityField on the FeedbackEvaluation page could not be clicked.");
    }

    public void clickIconTwoStarsApproach() throws Exception {
        waitUntilElementIsClickable(iconTwoStarsApproach);
        clickItem(iconTwoStarsApproach, "The 'two stars' icon for ApproachField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconTwoStarsComplementary() throws Exception {
        waitUntilElementIsClickable(iconTwoStarsComplementary);
        clickItem(iconTwoStarsComplementary, "The 'two stars' icon for ComplementaryField on the FeedbackEvaluation page could not be clicked.");
    }

    public void clickIconThreeStarsBenefit() throws Exception {
        waitUntilElementIsClickable(iconThreeStarsBenefit);
        clickItem(iconThreeStarsBenefit, "The 'three stars' icon for BenefitField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconThreeStarsAchieved() throws Exception {
        waitUntilElementIsClickable(iconThreeStarsAchieved);
        clickItem(iconThreeStarsAchieved, "The 'three stars' icon for AchievedField on the FeedbackEvaluation page could not be clicked.");
    }

    public void clickIconFourStarsCompetition() throws Exception {
        waitUntilElementIsClickable(iconFourStarsCompetition);
        clickItem(iconFourStarsCompetition, "The 'four stars' icon for CompetitionField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconFourStarsCommunicate() throws Exception {
        waitUntilElementIsClickable(iconFourStarsCommunicate);
        clickItem(iconFourStarsCommunicate, "The 'four stars' icon for CommunicateField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconFourStarsFeasibility() throws Exception {
        waitUntilElementIsClickable(iconFourStarsFeasibility);
        clickItem(iconFourStarsFeasibility, "The 'four stars' icon for FeasibilityField on the FeedbackEvaluation page could not be clicked.");
    }
    public void clickIconFourStarsIdea() throws Exception {
        waitUntilElementIsClickable(iconFourStarsIdea);
        clickItem(iconFourStarsIdea, "The 'four stars' icon for IdeaField on the FeedbackEvaluation page could not be clicked.");
    }

    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The button Save on the FeedbackEvaluation page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ctools-ajaxing"))); // saving
    }
    public void clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The button Submit on the FeedbackEvaluation page could not be clicked.");
    }

    public WebElement getTabNABC() {
        return tabNABC;
    }
    public void switchToTabTeam() throws Exception {
        waitUntilElementIsClickable(tabTeam);
        clickItem(tabTeam, "The tab Team on the FeedbackEvaluation page could not be clicked.");
    }
    public void switchToTabPresentation() throws Exception {
        waitUntilElementIsClickable(tabPresentation);
        clickItem(tabPresentation, "The tab Presentation on the FeedbackEvaluation page could not be clicked.");
    }
    public void switchToTabBusinessPotential() throws Exception {
        waitUntilElementIsClickable(tabBusinessPotential);
        clickItem(tabBusinessPotential, "The tab 'Business potential' on the FeedbackEvaluation page could not be clicked.");
    }
    public void switchToTabOverallAdvice() throws Exception {
        waitUntilElementIsClickable(tabOverallAdvice);
        clickItem(tabOverallAdvice, "The tab 'Overall Advice' on the FeedbackEvaluation page could not be clicked.");
    }


}
