package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class GroupTabGroupPage extends CreateClassPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Add people")
    private WebElement linkAddPeople;
    @FindBy(how = How.LINK_TEXT, using = "People")
    private WebElement linkPeople;


    /*---------CONSTRUCTORS--------*/
    public GroupTabGroupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public PeopleInGroupForGroupPage clickLinkAddPeople() throws Exception {
        waitUntilElementIsClickable(linkAddPeople);
        clickItem(linkAddPeople, "The link 'Add people' on the Group page (for Group) could not be clicked.");
        return new PeopleInGroupForGroupPage(webDriver, locations);
    }
    public PeopleInGroupForGroupPage clickLinkPeople() throws Exception {
        waitUntilElementIsClickable(linkPeople);
        clickItem(linkPeople, "The link 'People' on the Group page (for Group) could not be clicked.");
        return new PeopleInGroupForGroupPage(webDriver, locations);
    }


}
