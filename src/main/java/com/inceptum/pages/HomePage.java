package com.inceptum.pages;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;


public class HomePage extends BasePage {

    /* ----- FIELDS ----- */
    protected String location = "HOME";

    @FindBy(how = How.LINK_TEXT, using = "Sign in")
    private WebElement buttonSignIn;
    @FindBy(how = How.LINK_TEXT, using = "Peer assignments")
    private WebElement buttonPeerAssignments;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.LINK_TEXT, using = "Sign out")
    private WebElement linkSignOut;
    @FindBy(how = How.LINK_TEXT, using = "Profile")
    private WebElement linkProfile;
    @FindBy(how = How.CSS, using = "a.feedback")
    private WebElement linkFeedback;
    @FindBy(how = How.CSS, using = ".subfuncs a[href*='whoIsWho']")
    private WebElement linkWhoIsWho;
    @FindBy(how = How.CSS, using = "a[href='/WhoIsWho']")
    private WebElement linkUsers;
    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "See course overview")
    private WebElement buttonSeeCourseOverview;
    @FindBy(how = How.CSS, using = "#block-iminds-academy-class-iminds-academy-class-menu .arrow")
    private WebElement buttonMore; // arrow of dropdown
    @FindBy(how = How.CSS, using = "#block-iminds-academy-class-iminds-academy-class-menu .popup")
    private WebElement popupClassMenu;
    @FindBy(how = How.XPATH, using = "//a[contains(@href,'users')]")
    private WebElement userName;
    @FindBy(how = How.XPATH, using = "//a[@class='left_part_iminds_academy_class']")
    private WebElement tabOfClasses; //


    /* ----- CONSTRUCTORS ----- */

    public HomePage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    /* ----- METHODS ----- */

    public static HomePage openThisPage(WebDriver webDriver, PageLocation locations) throws Exception {
        webDriver.get(locations.getFullUrl("HOME"));
        return new HomePage(webDriver, locations);
    }

    public LoginFormPage clickButtonSignIn() throws Exception {
        waitUntilElementIsClickable(buttonSignIn);
        clickItem(buttonSignIn, "The button 'Sign in' on the HomePage could not be clicked.");
        LoginFormPage login = new LoginFormPage(webDriver, locations);
        login.waitForPopup();
        return login;
    }
    public WebElement getButtonSignIn() {
        return buttonSignIn;
    }
    public WebElement getButtonPeerAssignments() {
        return buttonPeerAssignments;
    }
    public OverviewAssignmentsPage clickButtonPeerAssignments() throws Exception {
        waitUntilElementIsClickable(buttonPeerAssignments);
        clickItem(buttonPeerAssignments, "The button 'Peer assignments' on the HomePage could not be clicked.");
        waitForPageToLoad();
        return new OverviewAssignmentsPage(webDriver, locations);
    }
    public WebElement getMessage() {
        return message;
    }
    public void clickLinkSignOut() throws Exception {
        waitUntilElementIsClickable(linkSignOut);
        clickItem(linkSignOut, "The link 'Sign out' on the HomePage could not be clicked.");
        waitForPageToLoad();
    }
    public void clickLinkProfile(List<String> user) throws Exception {
        WebElement linkUserAccount = webDriver.findElement(By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']"));
        linkUserAccount.click();
        waitUntilElementIsClickable(linkProfile);
        clickItem(linkProfile, "The link 'Profile' on the HomePage could not be clicked.");
    }
    public OverviewFeedbackPage clickLinkFeedback() throws Exception {
        // Open 'Class' dropdown menu
        waitUntilElementIsClickable(buttonMore);
        clickItem(buttonMore, "Button (arrow) 'More' could not be clicked on the Home page");
        // Wait until menu is opened
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return popupClassMenu.isDisplayed();
            }
        });
        // Click feedback link
        waitUntilElementIsClickable(linkFeedback);
        clickItem(linkFeedback, "The link 'Feedback' on the HomePage could not be clicked.");
        waitForPageToLoad();
        return new OverviewFeedbackPage(webDriver, locations);
    }
    public WhoIsWhoPage clickLinkWhoIsWho() throws Exception {
        // Open 'Class' dropdown menu
        waitUntilElementIsClickable(buttonMore);
        clickItem(buttonMore, "Button (arrow) 'More' could not be clicked on the Home page");
        // Wait until menu is opened
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return popupClassMenu.isDisplayed();
            }
        });
        // Click 'Who is who' link
        waitUntilElementIsClickable(linkWhoIsWho);
        clickItem(linkWhoIsWho, "The link 'Who is Who' on the HomePage could not be clicked.");
        waitForPageToLoad();
        return new WhoIsWhoPage(webDriver, locations);
    }
    public void checkLinkWhoIsWhoIsNotDisplayed() throws Exception {
        // Open 'Class' dropdown menu
        waitUntilElementIsClickable(tabOfClasses);
        clickItem(tabOfClasses, "Button (arrow) 'More' could not be clicked on the Home page");
        // Wait until menu is opened
      /*  waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return popupClassMenu.isDisplayed();
            }
        });*/
        List<WebElement> links = webDriver.findElements(By.cssSelector(".subfuncs a[href*='whoIsWho']"));
        int size = links.size();
        Assert.assertTrue("The link 'Who's who' is displayed in class menu list.", size == 0);
    }
    public WhoIsWhoPage clickLinkUsers(List<String> user) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']"))); // user's link
        webDriver.findElement(By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']")).click();
        // Wait until menu is opened
        final WebElement dropdown = webDriver.findElement(By.cssSelector("#block-system-user-menu .menu>li.expanded"));
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return dropdown.getAttribute("class").contains("open");
            }
        });
        waitUntilElementIsClickable(linkUsers);
        clickItem(linkUsers, "The link 'Users' on the HomePage could not be clicked.");
        waitForPageToLoad();
        return new WhoIsWhoPage(webDriver, locations);
    }
    public void checkLinkUsersIsNotDisplayedForUser(List<String> user) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']"))); // user's link
        webDriver.findElement(By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']")).click();
        // Wait until menu is opened
        final WebElement dropdown = webDriver.findElement(By.cssSelector("#block-system-user-menu .menu>li.expanded"));
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                return dropdown.getAttribute("class").contains("open");
            }
        });
        List<WebElement> links = webDriver.findElements(By.cssSelector("a[href='/WhoIsWho']"));
        int size = links.size();
        Assert.assertTrue("The link 'Who's who' is displayed in class menu list.", size == 0);
    }
    public ChangeFrontpageSettingsPage clickLinkEdit() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the HomePage could not be clicked.");
        return new ChangeFrontpageSettingsPage(webDriver, locations);
    }
    public OverviewPage clickButtonSeeCourseOverview() throws Exception {
        waitUntilElementIsClickable(buttonSeeCourseOverview);
        clickItem(buttonSeeCourseOverview, "The button 'See course overview' on the HomePage could not be clicked.");
        waitForPageToLoad();
        return new OverviewPage(webDriver, locations);
    }
    public OverviewPage selectClassFromDropdown(String className) throws Exception {
        // Check if needed Class has been already selected on Home page
        if (getButtonClassMenu().getText().equals(className)) {
            clickButtonClassMenu();
        } else {
            // Click dropdown arrow 'More' to show options
            waitUntilElementIsClickable(buttonMore);
            clickItem(buttonMore, "Button (arrow) 'More' could not be clicked on the Home page");
            // Wait until popup is opened
            waitCondition(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    return popupClassMenu.isDisplayed();
                }
            });
            // Click option with the ClassName
            WebElement optionClass = popupClassMenu.findElement(By.xpath("//*[@class='classes']/a[text()='" + className + "']"));
            optionClass.click();
        }
        OverviewPage overviewPage = new OverviewPage(webDriver, locations);
        waitUntilElementIsVisible(overviewPage.getNavPanel());
        return new OverviewPage(webDriver, locations);
    }

    public WebElement getUserName() {
        return userName;
    }
}
