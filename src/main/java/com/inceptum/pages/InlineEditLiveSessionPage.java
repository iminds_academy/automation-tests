package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class InlineEditLiveSessionPage extends ViewLiveSessionPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "h2 div")
    private WebElement fieldTitleInline;
    @FindBy(how = How.ID, using = "edit-field-live-session-type-und")
    private WebElement fieldLiveSessionTypePopup;
    @FindBy(how = How.CSS, using = ".field--name-field-what-you-need-todo .cke_inner")
    private WebElement ckeditorFieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = ".field--name-field-planning .cke_inner")
    private WebElement ckeditorFieldPlanning;
    @FindBy(how = How.CSS, using = ".action-save")
    private WebElement buttonSaveInline;
    @FindBy(how = How.CSS, using = "#quickedit_modal .action-save.quickedit-button")
    private WebElement buttonSaveChanges;
    @FindBy(how = How.CSS, using = ".action-cancel")
    private WebElement buttonCloseInline;
    @FindBy(how = How.CSS, using = "#quickedit_modal .action-cancel.quickedit-button")
    private WebElement buttonDiscardChanges;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-what-you-need-todo'] .field__item")
    private WebElement fieldWhatYouNeedToDoInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-planning'] .field__item")
    private WebElement fieldPlanningInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-date-start-end'] .field__item")
    private WebElement fieldDateInline;
    @FindBy(how = How.CSS, using = "#edit-field-date-start-end")
    private WebElement popupDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-show-todate")
    private WebElement checkboxUseStartAndEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-time-checker-show-time")
    private WebElement checkboxShowTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-datepicker-popup-0")
    private WebElement fieldStartDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-datepicker-popup-0")
    private WebElement fieldEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-timeEntry-popup-1")
    private WebElement fieldStartTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-timeEntry-popup-1")
    private WebElement fieldEndTime;
    @FindBy(how = How.CSS, using = ".add_course_material_button")
    private WebElement buttonAddCourseMaterial;
    @FindBy(how = How.CSS, using = "h2 .messages--error")
    private WebElement errorMessageFieldTitle;
    @FindBy(how = How.CSS, using = ".field--name-field-what-you-need-todo .messages--error")
    private WebElement errorMessageFieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = ".field--name-field-planning .messages--error")
    private WebElement errorMessageFieldPlanning;
    @FindBy(how = How.CSS, using = ".field--name-field-what-you-need-todo .cke_button__sourcedialog")
    private WebElement buttonSourceFieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = ".field--name-field-planning .cke_button__sourcedialog")
    private WebElement buttonSourceFieldPlanning;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[6]/table//textarea")
    private WebElement textareaSourceWhatYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[7]/table//textarea")
    private WebElement textareaSourcePlanning;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[6]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkWhatYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[7]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkPlanning;


    /*---------CONSTRUCTORS--------*/

    public InlineEditLiveSessionPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitleInline (String title) {
        // Click Title field and wait until it becomes editable
        waitUntilElementIsClickable(fieldTitleInline);
        fieldTitleInline.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2 div[contenteditable='true']")));
        // Clear the field and enter a new title
        fieldTitleInline.clear();
        fieldTitleInline.sendKeys(title);
    }
    public void clearFieldTitleInline () {
        // Click Title field and wait until it becomes editable
        waitUntilElementIsClickable(fieldTitleInline);
        fieldTitleInline.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2 div[contenteditable='true']")));
        // Clear the field
        fieldTitleInline.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        fieldTitleInline.sendKeys(Keys.DELETE);
    }
    public WebElement getFieldLiveSessionTypePopup(){
        return fieldLiveSessionTypePopup;
    }
    public void clickFieldLiveSessionType() {
        waitUntilElementIsClickable(getFieldLiveSessionType());
        getFieldLiveSessionType().click();
        waitUntilElementIsClickable(fieldLiveSessionTypePopup);
    }
    public void fillFieldWhatYouNeedToDoInline (String text) throws Exception {
        // Click 'What you need to do' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldWhatYouNeedToDoInline);
        fieldWhatYouNeedToDoInline.click();
        waitUntilElementIsVisible(ckeditorFieldWhatYouNeedToDo);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-what-you-need-todo'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-what-you-need-todo').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldWhatYouNeedToDoWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldWhatYouNeedToDo);
        buttonSourceFieldWhatYouNeedToDo.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourceWhatYouNeedToDo);
        textareaSourceWhatYouNeedToDo.sendKeys(text);
        buttonOkWhatYouNeedToDo.click();
    }
    public void clearFieldWhatYouNeedToDoInline() throws Exception {
        // Click 'What you need to do' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldWhatYouNeedToDoInline);
        fieldWhatYouNeedToDoInline.click();
        waitUntilElementIsVisible(ckeditorFieldWhatYouNeedToDo);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-what-you-need-todo'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldWhatYouNeedToDo.click();
        waitUntilElementIsVisible(textareaSourceWhatYouNeedToDo); // popup is opened
        textareaSourceWhatYouNeedToDo.clear(); // clear textfield
        buttonOkWhatYouNeedToDo.click(); // submit changes on the popup
        waitUntilElementIsVisible(ckeditorFieldWhatYouNeedToDo);
        Thread.sleep(1000); // test fails without the timeout
    }
    public void fillFieldPlanningInline (String text) {
        // Click 'Planning' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldPlanningInline);
        fieldPlanningInline.click();
        waitUntilElementIsVisible(ckeditorFieldPlanning);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-planning'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-planning').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldPlanningWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldPlanning);
        buttonSourceFieldPlanning.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourcePlanning);
        textareaSourcePlanning.sendKeys(text);
        buttonOkPlanning.click();
    }
    public void clearFieldPlanningInline() {
        // Click 'Planning' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldPlanningInline);
        fieldPlanningInline.click();
        waitUntilElementIsVisible(getErrorMessageFieldWhatYouNeedToDo());
        waitUntilElementIsVisible(ckeditorFieldPlanning);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-planning'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldPlanning.click();
        waitUntilElementIsVisible(textareaSourcePlanning);
        textareaSourcePlanning.clear();
        buttonOkPlanning.click();
    }
    public WebElement getButtonSaveInline() {
        return buttonSaveInline;
    }
    public ViewLiveSessionPage clickButtonSaveInline() throws Exception {
        Thread.sleep(1000);
        scrollToElement(buttonSaveInline);
        waitUntilElementIsClickable(buttonSaveInline);
        buttonSaveInline.sendKeys(Keys.ENTER);
        waitForPageToLoad();
        return new ViewLiveSessionPage(webDriver, locations);
    }
    public void clickButtonCloseInline() throws Exception {
        scrollToElement(buttonCloseInline);
        waitUntilElementIsClickable(buttonCloseInline);
        buttonCloseInline.sendKeys(Keys.ENTER);
    }
    public void clickFieldDateInline() throws Exception {
        waitUntilElementIsClickable(fieldDateInline);
        Thread.sleep(1000);
        // Click the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-date-start-end').item(0).click();");
        waitUntilLoading();
        waitUntilElementIsVisible(popupDate);
    }
    public void selectCheckboxUseStartAndEndDate() throws Exception {
        Thread.sleep(1000);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("edit-field-date-start-end-und-0-show-todate")));
        // Select the checkbox
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-show-todate').click();");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".end-date-wrapper")));
    }
    public void selectCheckboxShowTime() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("edit-field-date-start-end-und-0-time-checker-show-time")));
        // Select the checkbox
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-time-checker-show-time').click();");
        waitUntilElementIsVisible(fieldStartTime);
    }
    public WebElement getFieldEndDate() {
        return fieldEndDate;
    }
    public String getStartDate() {
        waitUntilElementIsVisible(fieldStartDate);
        return fieldStartDate.getAttribute("value");
    }
    public String getEndDate() {
        waitUntilElementIsVisible(fieldEndDate);
        return fieldEndDate.getAttribute("value");
    }
    public WebElement getFieldStartTime() {
        return fieldStartTime;
    }
    public WebElement getFieldEndTime() {
        return fieldEndTime;
    }
    public void clickButtonAddCourseMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddCourseMaterial);
        clickItem(buttonAddCourseMaterial, "The button 'Add course material' on the LiveSession page could not be clicked.");
        waitUntilElementIsVisible(getPopupModalContent());
    }
    public ViewLiveSessionPage clickButtonSaveChanges() throws Exception {
        waitUntilElementIsClickable(buttonSaveChanges);
        clickItem(buttonSaveChanges, "The button 'Save' on the 'You have unsaved changes' popup could not be clicked.");
        waitForPageToLoad();
        return new ViewLiveSessionPage(webDriver, locations);
    }
    public void clickButtonDiscardChanges() throws Exception {
        waitUntilElementIsClickable(buttonDiscardChanges);
        clickItem(buttonDiscardChanges, "The button 'Discard changes' on the 'You have unsaved changes' popup could not be clicked.");
    }
    public WebElement getErrorMessageFieldTitle() {
        waitUntilElementIsVisible(errorMessageFieldTitle);
        return errorMessageFieldTitle;
    }
    public WebElement getErrorMessageFieldWhatYouNeedToDo() {
        waitUntilElementIsVisible(errorMessageFieldWhatYouNeedToDo);
        return errorMessageFieldWhatYouNeedToDo;
    }
    public WebElement getErrorMessageFieldPlanning() {
        waitUntilElementIsVisible(errorMessageFieldPlanning);
        return errorMessageFieldPlanning;
    }

}
