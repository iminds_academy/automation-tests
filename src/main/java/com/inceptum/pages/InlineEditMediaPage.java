package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class InlineEditMediaPage extends ViewMediaPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "h2 div")
    private WebElement fieldTitleInline;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHoursPopup;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMinutesPopup;
    @FindBy(how = How.CSS, using = ".field--name-field-video-summary .cke_inner")
    private WebElement ckeditorFieldVideoSummary;
    @FindBy(how = How.CSS, using = ".field--name-field-context-info .cke_inner")
    private WebElement ckeditorFieldContextualInformation;
    @FindBy(how = How.CSS, using = ".action-save")
    private WebElement buttonSaveInline;
    @FindBy(how = How.CSS, using = "#quickedit_modal .action-save.quickedit-button")
    private WebElement buttonSaveChanges;
    @FindBy(how = How.CSS, using = ".action-cancel")
    private WebElement buttonCloseInline;
    @FindBy(how = How.CSS, using = "#quickedit_modal .action-cancel.quickedit-button")
    private WebElement buttonDiscardChanges;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-video-summary'] .field__item")
    private WebElement fieldVideoSummaryInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-context-info'] .field__item")
    private WebElement fieldContextualInformationInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-date-start-end'] .field__item")
    private WebElement fieldDateInline;
    @FindBy(how = How.CSS, using = "#edit-field-date-start-end")
    private WebElement popupDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-datepicker-popup-0")
    private WebElement fieldStartDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-datepicker-popup-0")
    private WebElement fieldEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-timeEntry-popup-1")
    private WebElement fieldStartTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-timeEntry-popup-1")
    private WebElement fieldEndTime;
    @FindBy(how = How.CSS, using = ".add_course_material_button")
    private WebElement buttonAddCourseMaterial;
    @FindBy(how = How.CSS, using = "h2 .messages--error")
    private WebElement errorMessageFieldTitle;
    @FindBy(how = How.CSS, using = ".field--name-field-video-summary .messages--error")
    private WebElement errorMessageFieldVideoSummary;
    @FindBy(how = How.CSS, using = ".field--name-field-context-info .messages--error")
    private WebElement errorMessageFieldContextualInformation;
    @FindBy(how = How.CSS, using = ".field--name-field-video-summary .cke_button__sourcedialog")
    private WebElement buttonSourceFieldVideoSummary;
    @FindBy(how = How.CSS, using = ".field--name-field-context-info .cke_button__sourcedialog")
    private WebElement buttonSourceFieldContextualInformation;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[6]/table//textarea")
    private WebElement textareaSourceVideoSummary;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[7]/table//textarea")
    private WebElement textareaSourceContextualInformation;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[6]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkVideoSummary;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[7]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkContextualInformation;


    /*---------CONSTRUCTORS--------*/

    public InlineEditMediaPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitleInline (String title) {
        // Click Title field and wait until it becomes editable
        waitUntilElementIsClickable(fieldTitleInline);
        fieldTitleInline.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2 div[contenteditable='true']")));
        // Clear the field and enter a new title
        fieldTitleInline.clear();
        fieldTitleInline.sendKeys(title);
    }
    public void clearFieldTitleInline () {
        // Click Title field and wait until it becomes editable
        waitUntilElementIsClickable(fieldTitleInline);
        fieldTitleInline.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2 div[contenteditable='true']")));
        // Clear the field
        fieldTitleInline.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        fieldTitleInline.sendKeys(Keys.DELETE);
    }

    public void clickFieldLength() {
        waitUntilElementIsClickable(getFieldLength());
        getFieldLength().click();
        waitUntilElementIsClickable(fieldLengthHoursPopup);
    }
    public WebElement getFieldLengthHoursPopup() {
        return fieldLengthHoursPopup;
    }
    public WebElement getFieldLengthMinutesPopup() {
        return fieldLengthMinutesPopup;
    }
    public void fillFieldVideoSummaryInline (String text) throws Exception {
        // Click 'Video summary' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldVideoSummaryInline);
        fieldVideoSummaryInline.click();
        waitUntilElementIsVisible(ckeditorFieldVideoSummary);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-video-summary'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-video-summary').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldVideoSummaryWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldVideoSummary);
        buttonSourceFieldVideoSummary.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourceVideoSummary);
        textareaSourceVideoSummary.sendKeys(text);
        buttonOkVideoSummary.click();
    }
    public void clearFieldVideoSummaryInline() throws Exception {
        // Click 'Video summary' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldVideoSummaryInline);
        fieldVideoSummaryInline.click();
        waitUntilElementIsVisible(ckeditorFieldVideoSummary);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-video-summary'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldVideoSummary.click();
        waitUntilElementIsVisible(textareaSourceVideoSummary); // popup is opened
        textareaSourceVideoSummary.clear(); // clear textfield
        buttonOkVideoSummary.click(); // submit changes on the popup
        waitUntilElementIsVisible(ckeditorFieldVideoSummary);
        Thread.sleep(1000); // test fails without the timeout
    }
    public void fillFieldContextualInformationInline (String text) throws Exception {
        clickLinkInfo(); // open 'Contextual information' popup
        // Click 'Contextual information' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldContextualInformationInline);
        fieldContextualInformationInline.click();
        waitUntilElementIsVisible(ckeditorFieldContextualInformation);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-context-info'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-context-info').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldContextualInformationWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldContextualInformation);
        buttonSourceFieldContextualInformation.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourceContextualInformation);
        textareaSourceContextualInformation.sendKeys(text);
        buttonOkContextualInformation.click();
    }
    public void clearFieldContextualInformationInline() throws Exception {
        clickLinkInfo(); // open 'Contextual information' popup
        // Click 'Contextual information' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldContextualInformationInline);
        fieldContextualInformationInline.click();
        waitUntilElementIsVisible(getErrorMessageFieldVideoSummary());
        waitUntilElementIsVisible(ckeditorFieldContextualInformation);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-context-info'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldContextualInformation.click();
        waitUntilElementIsVisible(textareaSourceContextualInformation);
        textareaSourceContextualInformation.clear();
        buttonOkContextualInformation.click();
    }
    public WebElement getButtonSaveInline() {
        return buttonSaveInline;
    }
    public ViewMediaPage clickButtonSaveInline() throws Exception {
        Thread.sleep(1000);
        scrollToElement(buttonSaveInline);
        waitUntilElementIsClickable(buttonSaveInline);
        buttonSaveInline.sendKeys(Keys.ENTER);
        waitForPageToLoad();
        return new ViewMediaPage(webDriver, locations);
    }
    public void clickButtonCloseInline() throws Exception {
        scrollToElement(buttonCloseInline);
        waitUntilElementIsClickable(buttonCloseInline);
        buttonCloseInline.sendKeys(Keys.ENTER);
    }
    public void clickFieldDateInline() throws Exception {
        waitUntilElementIsClickable(fieldDateInline);
        Thread.sleep(1000);
        // Click the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-date-start-end').item(0).click();");
        waitUntilLoading();
        waitUntilElementIsVisible(popupDate);
    }
    public void selectCheckboxUseStartAndEndDate() throws Exception {
        Thread.sleep(1000);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("edit-field-date-start-end-und-0-show-todate")));
        // Select the checkbox
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-show-todate').click();");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".end-date-wrapper")));
    }
    public void selectCheckboxShowTime() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("edit-field-date-start-end-und-0-time-checker-show-time")));
        // Select the checkbox
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-time-checker-show-time').click();");
        waitUntilElementIsVisible(fieldStartTime);
    }
    public WebElement getFieldEndDate() {
        return fieldEndDate;
    }
    public String getStartDate() {
        waitUntilElementIsVisible(fieldStartDate);
        return fieldStartDate.getAttribute("value");
    }
    public String getEndDate() {
        waitUntilElementIsVisible(fieldEndDate);
        return fieldEndDate.getAttribute("value");
    }
    public WebElement getFieldStartTime() {
        return fieldStartTime;
    }
    public WebElement getFieldEndTime() {
        return fieldEndTime;
    }
    public void clickButtonAddCourseMaterial() throws Exception {
        clickLinkLinks(); // open Links popup
        waitUntilElementIsClickable(buttonAddCourseMaterial);
        clickItem(buttonAddCourseMaterial, "The button 'Add course material' on the MediaPage page could not be clicked.");
        waitUntilElementIsVisible(getPopupModalContent());
    }
    public ViewMediaPage clickButtonSaveChanges() throws Exception {
        waitUntilElementIsClickable(buttonSaveChanges);
        clickItem(buttonSaveChanges, "The button 'Save' on the 'You have unsaved changes' popup could not be clicked.");
        waitForPageToLoad();
        return new ViewMediaPage(webDriver, locations);
    }
    public void clickButtonDiscardChanges() throws Exception {
        waitUntilElementIsClickable(buttonDiscardChanges);
        clickItem(buttonDiscardChanges, "The button 'Discard changes' on the 'You have unsaved changes' popup could not be clicked.");
    }
    public WebElement getErrorMessageFieldTitle() {
        waitUntilElementIsVisible(errorMessageFieldTitle);
        return errorMessageFieldTitle;
    }
    public WebElement getErrorMessageFieldVideoSummary() {
        waitUntilElementIsVisible(errorMessageFieldVideoSummary);
        return errorMessageFieldVideoSummary;
    }
    public WebElement getErrorMessageFieldContextualInformation() {
        waitUntilElementIsVisible(errorMessageFieldContextualInformation);
        return errorMessageFieldContextualInformation;
    }

}
