package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class InlineEditTheoryPage extends ViewTheoryPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "h2 div")
    private WebElement fieldTitleInline;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-3600")
    private WebElement fieldLengthHoursPopup;
    @FindBy(how = How.ID, using = "edit-field-length-und-0-value-60")
    private WebElement fieldLengthMinutesPopup;
    @FindBy(how = How.CSS, using = ".field--name-field-what-you-need-todo .cke_inner")
    private WebElement ckeditorFieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = ".field--name-field-whats-this-for .cke_inner")
    private WebElement ckeditorFieldWhatsThisFor;
    @FindBy(how = How.CSS, using = ".field--name-field-goal .cke_inner")
    private WebElement ckeditorFieldLearningObjectives;
    @FindBy(how = How.CSS, using = ".action-save")
    private WebElement buttonSaveInline;
    @FindBy(how = How.CSS, using = "#quickedit_modal .action-save.quickedit-button")
    private WebElement buttonSaveChanges;
    @FindBy(how = How.CSS, using = ".action-cancel")
    private WebElement buttonCloseInline;
    @FindBy(how = How.CSS, using = "#quickedit_modal .action-cancel.quickedit-button")
    private WebElement buttonDiscardChanges;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-what-you-need-todo'] .field__item")
    private WebElement fieldWhatYouNeedToDoInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-whats-this-for'] .field__item")
    private WebElement fieldWhatsThisForInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-goal'] .field__item")
    private WebElement fieldLearningObjectivesInline;
    @FindBy(how = How.CSS, using = ".quickedit-active [class*='field-date-start-end'] .field__item")
    private WebElement fieldDateInline;
    @FindBy(how = How.CSS, using = "#edit-field-date-start-end")
    private WebElement popupDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-show-todate")
    private WebElement checkboxUseStartAndEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-time-checker-show-time")
    private WebElement checkboxShowTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-datepicker-popup-0")
    private WebElement fieldStartDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-datepicker-popup-0")
    private WebElement fieldEndDate;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value-timeEntry-popup-1")
    private WebElement fieldStartTime;
    @FindBy(how = How.ID, using = "edit-field-date-start-end-und-0-value2-timeEntry-popup-1")
    private WebElement fieldEndTime;
    @FindBy(how = How.CSS, using = ".add_course_material_button")
    private WebElement buttonAddCourseMaterial;
    @FindBy(how = How.CSS, using = "h2 .messages--error")
    private WebElement errorMessageFieldTitle;
    @FindBy(how = How.CSS, using = ".field--name-field-what-you-need-todo .messages--error")
    private WebElement errorMessageFieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = ".field--name-field-whats-this-for .messages--error")
    private WebElement errorMessageFieldWhatsThisFor;
    @FindBy(how = How.CSS, using = ".field--name-field-goal .messages--error")
    private WebElement errorMessageFieldLearningObjectives;
    @FindBy(how = How.CSS, using = ".field--name-field-what-you-need-todo .cke_button__sourcedialog")
    private WebElement buttonSourceFieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = ".field--name-field-whats-this-for .cke_button__sourcedialog")
    private WebElement buttonSourceFieldWhatsThisFor;
    @FindBy(how = How.CSS, using = ".field--name-field-goal .cke_button__sourcedialog")
    private WebElement buttonSourceFieldLearningObjectives;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[6]/table//textarea")
    private WebElement textareaSourceWhatYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[7]/table//textarea")
    private WebElement textareaSourceWhatsThisFor;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[8]/table//textarea")
    private WebElement textareaSourceLearningObjectives;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[6]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkWhatYouNeedToDo;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[7]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkWhatsThisFor;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[8]/table//a[@class='cke_dialog_ui_button cke_dialog_ui_button_ok']")
    private WebElement buttonOkLearningObjectives;


    /*---------CONSTRUCTORS--------*/

    public InlineEditTheoryPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitleInline (String title) {
        // Click Title field and wait until it becomes editable
        waitUntilElementIsClickable(fieldTitleInline);
        fieldTitleInline.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2 div[contenteditable='true']")));
        // Clear the field and enter a new title
        fieldTitleInline.clear();
        fieldTitleInline.sendKeys(title);
    }
    public void clearFieldTitleInline () {
        // Click Title field and wait until it becomes editable
        waitUntilElementIsClickable(fieldTitleInline);
        fieldTitleInline.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("h2 div[contenteditable='true']")));
        // Clear the field
        fieldTitleInline.sendKeys(Keys.chord(Keys.CONTROL, "a"));
        fieldTitleInline.sendKeys(Keys.DELETE);
    }

    public void clickFieldLength() {
        waitUntilElementIsClickable(getFieldLength());
        getFieldLength().click();
        waitUntilElementIsClickable(fieldLengthHoursPopup);
    }
    public WebElement getFieldLengthHoursPopup() {
        return fieldLengthHoursPopup;
    }
    public WebElement getFieldLengthMinutesPopup() {
        return fieldLengthMinutesPopup;
    }
    public void fillFieldWhatYouNeedToDoInline (String text) throws Exception {
        // Click 'What you need to do' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldWhatYouNeedToDoInline);
        fieldWhatYouNeedToDoInline.click();
        waitUntilElementIsVisible(ckeditorFieldWhatYouNeedToDo);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-what-you-need-todo'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-what-you-need-todo').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldWhatYouNeedToDoWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldWhatYouNeedToDo);
        buttonSourceFieldWhatYouNeedToDo.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourceWhatYouNeedToDo);
        textareaSourceWhatYouNeedToDo.sendKeys(text);
        buttonOkWhatYouNeedToDo.click();
    }
    public void clearFieldWhatYouNeedToDoInline() throws Exception {
        // Click 'What you need to do' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldWhatYouNeedToDoInline);
        fieldWhatYouNeedToDoInline.click();
        waitUntilElementIsVisible(ckeditorFieldWhatYouNeedToDo);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-what-you-need-todo'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldWhatYouNeedToDo.click();
        waitUntilElementIsVisible(textareaSourceWhatYouNeedToDo); // popup is opened
        textareaSourceWhatYouNeedToDo.clear(); // clear textfield
        buttonOkWhatYouNeedToDo.click(); // submit changes on the popup
        waitUntilElementIsVisible(ckeditorFieldWhatYouNeedToDo);
        Thread.sleep(1000); // test fails without the timeout
    }
    public void fillFieldWhatsThisForInline (String text) {
        // Click 'What's this for' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldWhatsThisForInline);
        fieldWhatsThisForInline.click();
        waitUntilElementIsVisible(ckeditorFieldWhatsThisFor);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-whats-this-for'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-whats-this-for').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldLearningObjectivesInline (String text) {
        // Click 'Learning objectives' field and wait until it becomes editable
        waitUntilElementIsClickable(fieldLearningObjectivesInline);
        fieldLearningObjectivesInline.click();
        waitUntilElementIsVisible(ckeditorFieldLearningObjectives);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-goal'] div[contenteditable='true']")));
        // Enter text into the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-goal').item(0)" +
                        ".getElementsByTagName('p').item(0).innerHTML='" + text + "';");
    }
    public void fillFieldWhatsThisForWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldWhatsThisFor);
        buttonSourceFieldWhatsThisFor.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourceWhatsThisFor);
        textareaSourceWhatsThisFor.sendKeys(text);
        buttonOkWhatsThisFor.click();
    }
    public void fillFieldLearningObjectivesWithSource (String text) throws Exception {
        // Click the button Source
        waitUntilElementIsClickable(buttonSourceFieldLearningObjectives);
        buttonSourceFieldLearningObjectives.click();
        // Enter text and submit changes
        waitUntilElementIsVisible(textareaSourceLearningObjectives);
        textareaSourceLearningObjectives.sendKeys(text);
        buttonOkLearningObjectives.click();
    }
    public void clearFieldWhatsThisForInline() {
        // Click "What's this for" field and wait until it becomes editable
        waitUntilElementIsClickable(fieldWhatsThisForInline);
        fieldWhatsThisForInline.click();
        waitUntilElementIsVisible(getErrorMessageFieldWhatYouNeedToDo());
        waitUntilElementIsVisible(ckeditorFieldWhatsThisFor);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-whats-this-for'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldWhatsThisFor.click();
        waitUntilElementIsVisible(textareaSourceWhatsThisFor);
        textareaSourceWhatsThisFor.clear();
        buttonOkWhatsThisFor.click();
    }
    public void clearFieldLearningObjectivesInline() throws Exception {
        // Click "Learning objectives" field and wait until it becomes editable
        Thread.sleep(1000); // test fails without the timeout
        waitUntilElementIsClickable(fieldLearningObjectivesInline);
        fieldLearningObjectivesInline.click();
        waitUntilElementIsVisible(getErrorMessageFieldWhatsThisFor());
        waitUntilElementIsVisible(ckeditorFieldLearningObjectives);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By
                .cssSelector("[class*='field-goal'] div[contenteditable='true']")));
        // Clear the field with 'Source' button (CKEditor)
        buttonSourceFieldLearningObjectives.click();
        waitUntilElementIsVisible(textareaSourceLearningObjectives);
        textareaSourceLearningObjectives.clear();
        buttonOkLearningObjectives.click();
    }
    public WebElement getButtonSaveInline() {
        return buttonSaveInline;
    }
    public ViewTheoryPage clickButtonSaveInline() throws Exception {
        Thread.sleep(1000);
        scrollToElement(buttonSaveInline);
        waitUntilElementIsClickable(buttonSaveInline);
        buttonSaveInline.sendKeys(Keys.ENTER);
        waitForPageToLoad();
        return new ViewTheoryPage(webDriver, locations);
    }
    public void clickButtonCloseInline() throws Exception {
        scrollToElement(buttonCloseInline);
        waitUntilElementIsClickable(buttonCloseInline);
        buttonCloseInline.sendKeys(Keys.ENTER);
    }
    public void clickFieldDateInline() throws Exception {
        waitUntilElementIsClickable(fieldDateInline);
        Thread.sleep(1000);
        // Click the field
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementsByClassName('field--name-field-date-start-end').item(0).click();");
        waitUntilLoading();
        waitUntilElementIsVisible(popupDate);
    }
    public void selectCheckboxUseStartAndEndDate() throws Exception {
        Thread.sleep(1000);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("edit-field-date-start-end-und-0-show-todate")));
        // Select the checkbox
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-show-todate').click();");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".end-date-wrapper")));
    }
    public void selectCheckboxShowTime() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.id("edit-field-date-start-end-und-0-time-checker-show-time")));
        // Select the checkbox
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-time-checker-show-time').click();");
        waitUntilElementIsVisible(fieldStartTime);
    }
    public WebElement getFieldEndDate() {
        return fieldEndDate;
    }
    public String getStartDate() {
        waitUntilElementIsVisible(fieldStartDate);
        return fieldStartDate.getAttribute("value");
    }
    public String getEndDate() {
        waitUntilElementIsVisible(fieldEndDate);
        return fieldEndDate.getAttribute("value");
    }
    public WebElement getFieldStartTime() {
        return fieldStartTime;
    }
    public WebElement getFieldEndTime() {
        return fieldEndTime;
    }
    public void clickButtonAddCourseMaterial() throws Exception {
        waitUntilElementIsClickable(buttonAddCourseMaterial);
        clickItem(buttonAddCourseMaterial, "The button 'Add course material' on the Theory page could not be clicked.");
        waitUntilElementIsVisible(getPopupModalContent());
    }
    public ViewTheoryPage clickButtonSaveChanges() throws Exception {
        waitUntilElementIsClickable(buttonSaveChanges);
        clickItem(buttonSaveChanges, "The button 'Save' on the 'You have unsaved changes' popup could not be clicked.");
        waitForPageToLoad();
        return new ViewTheoryPage(webDriver, locations);
    }
    public void clickButtonDiscardChanges() throws Exception {
        waitUntilElementIsClickable(buttonDiscardChanges);
        clickItem(buttonDiscardChanges, "The button 'Discard changes' on the 'You have unsaved changes' popup could not be clicked.");
    }
    public WebElement getErrorMessageFieldTitle() {
        waitUntilElementIsVisible(errorMessageFieldTitle);
        return errorMessageFieldTitle;
    }
    public WebElement getErrorMessageFieldWhatYouNeedToDo() {
        waitUntilElementIsVisible(errorMessageFieldWhatYouNeedToDo);
        return errorMessageFieldWhatYouNeedToDo;
    }
    public WebElement getErrorMessageFieldWhatsThisFor() {
        waitUntilElementIsVisible(errorMessageFieldWhatsThisFor);
        return errorMessageFieldWhatsThisFor;
    }
    public WebElement getErrorMessageFieldLearningObjectives() {
        waitUntilElementIsVisible(errorMessageFieldLearningObjectives);
        return errorMessageFieldLearningObjectives;
    }

}
