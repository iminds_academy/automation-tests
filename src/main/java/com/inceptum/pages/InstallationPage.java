//package com.inceptum.pages;
//
//import com.inceptum.enumeration.PageLocation;
//import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.FindBy;
//import org.openqa.selenium.support.How;
//
///**
// * Created by Olga on 22.08.2014.
// */
//public class InstallationPage extends BasePage {
//
//    /* ----- FIELDS ----- */
//
//    @FindBy(how = How.XPATH, using = "//li[text()='Choose profile']")
//    private WebElement fieldChooseProfile;
//    @FindBy(how = How.XPATH, using = "//li[text()='Choose language']")
//    private WebElement fieldChooseLanguage;
//    @FindBy(how = How.XPATH, using = "//li[text()='Verify requirements']")
//    private WebElement fieldVerifyRequirements;
//    @FindBy(how = How.XPATH, using = "//li[text()='Set up database']")
//    private WebElement fieldSetUpDatabase;
//    @FindBy(how = How.XPATH, using = "//li[text()='Install profile']")
//    private WebElement fieldInstallProfile;
//    @FindBy(how = How.XPATH, using = "//li[text()='Configure site']")
//    private WebElement fieldConfigureSite;
//    @FindBy(how = How.XPATH, using = "//li[text()='Finished']")
//    private WebElement fieldFinished;
//    @FindBy(how = How.ID, using = "edit-profile--4")
//    private WebElement checkboxImindsAcademy;
//    @FindBy(how = How.ID, using = "edit-submit")
//    private WebElement buttonSaveAndContinue;
//    @FindBy(how = How.ID, using = "edit-locale--2")
//    private WebElement checkboxEnglish;
//    @FindBy(how = How.ID, using = "edit-locale--2")
//    private WebElement tableReport;
//    @FindBy(how = How.XPATH, using = "//a[text()='proceed with the installation']")
//    private WebElement linkInstallation;
//    @FindBy(how = How.ID, using = "edit-mysql-database")
//    private WebElement fieldDatabaseName;
//    @FindBy(how = How.ID, using = "edit-mysql-username")
//    private WebElement fieldDatabaseUsername;
//    @FindBy(how = How.ID, using = "edit-mysql-password")
//    private WebElement fieldDatabasePassword;
//    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Advanced options")
//    private WebElement fieldsetAdvancedOptions;
//    @FindBy(how = How.ID, using = "edit-mysql-host")
//    private WebElement fieldDatabaseHost;
//
//
//
//    /*---------CONSTRUCTORS--------*/
//    public InstallationPage(WebDriver webDriver, PageLocation locations) throws Exception {
//        super(webDriver, locations);
//    }
//
//
//    /*---------METHODS----------*/
//
//    public WebElement getFieldChooseProfile() {
//        return fieldChooseProfile;
//    }
//    public WebElement getFieldChooseLanguage() {
//        return fieldChooseLanguage;
//    }
//    public WebElement getFieldVerifyRequirements() {
//        return fieldVerifyRequirements;
//    }
//    public WebElement getFieldSetUpDatabase() {
//        return fieldSetUpDatabase;
//    }
//    public WebElement getFieldInstallProfile() {
//        return fieldInstallProfile;
//    }
//    public WebElement getFieldConfigureSite() {
//        return fieldConfigureSite;
//    }
//    public WebElement getFieldFinished() {
//        return fieldFinished;
//    }
//    public void selectCheckboxImindsAcademy() throws Exception {
//        waitUntilElementIsDisplayed(checkboxImindsAcademy);
//        clickItem(checkboxImindsAcademy, "The checkbox ImindsAcademy on the Installation page could not be selected.");
//    }
//    public void clickButtonSaveAndContinue() throws Exception {
//        waitUntilElementIsDisplayed(buttonSaveAndContinue);
//        clickItem(buttonSaveAndContinue, "The button SaveAndContinue on the Installation page could not be clicked.");
//    }
//    public WebElement getCheckboxEnglish() {
//        return checkboxEnglish;
//    }
//    public WebElement getTableReport() {
//        return tableReport;
//    }
//    public void clickLinkInstallation() throws Exception {
//        waitUntilElementIsDisplayed(linkInstallation);
//        clickItem(linkInstallation, "The link 'proceed with the installation' on the Installation page could not be clicked.");
//    }
//    public void fillFieldDatabaseName (String databaseName) {
//        waitUntilElementIsDisplayed(fieldDatabaseName);
//        fieldDatabaseName.sendKeys(databaseName);
//    }
//    public void fillFieldDatabaseUsername (String databaseUsername) {
//        fieldDatabaseUsername.sendKeys(databaseUsername);
//    }
//    public void fillFieldDatabasePassword (String databasePassword) {
//        fieldDatabasePassword.sendKeys(databasePassword);
//    }
//    public void fillFieldDatabaseHost (String databaseHost) {
//        waitUntilElementIsDisplayed(fieldsetAdvancedOptions);
//        fieldsetAdvancedOptions.click();
//        waitUntilElementIsDisplayed(fieldDatabaseHost);
//        fieldDatabaseHost.clear();
//        fieldDatabaseHost.sendKeys(databaseHost);
//    }
//    public WebElement getFieldDatabaseHost() {
//        return fieldDatabaseHost;
//    }
//
//
//}
