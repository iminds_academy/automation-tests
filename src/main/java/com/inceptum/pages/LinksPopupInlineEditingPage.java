package com.inceptum.pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class LinksPopupInlineEditingPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-title")
    private WebElement fieldTitle;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-url")
    private WebElement fieldUrl;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-add-more")
    private WebElement buttonAddAnotherItem;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-2-title")
    private WebElement fieldTitle3;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-2-url")
    private WebElement fieldUrl3;
    @FindBy(how = How.ID, using = "edit-field-links-course-und-0-attributes-target")
    private WebElement checkboxOpenInNewWindow;


    /*---------CONSTRUCTORS--------*/

    public LinksPopupInlineEditingPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle);
        fieldTitle.clear();
        fieldTitle.sendKeys(title);
    }
    public void fillFieldUrl(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrl);
        fieldUrl.clear();
        fieldUrl.sendKeys(url);
    }
    public void clickButtonAddAnotherItem() throws Exception {
        waitUntilElementIsClickable(buttonAddAnotherItem);
        clickItem(buttonAddAnotherItem, "The button AddAnotherItem on the Links popup could not be clicked.");
    }
    public void fillFieldTitle3(String title) throws Exception {
        waitUntilElementIsVisible(fieldTitle3);
        fieldTitle3.sendKeys(title);
    }
    public void fillFieldUrl3(String url) throws Exception {
        waitUntilElementIsVisible(fieldUrl3);
        fieldUrl3.sendKeys(url);
    }
    public void selectCheckboxOpenInNewWindow() throws Exception {
        waitUntilElementIsClickable(checkboxOpenInNewWindow);
        checkboxOpenInNewWindow.sendKeys(Keys.SPACE);
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public WebElement getFieldUrl() {
        return fieldUrl;
    }



}
