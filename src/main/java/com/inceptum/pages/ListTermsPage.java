package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ListTermsPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;



    /*---------CONSTRUCTORS--------*/
    public ListTermsPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getMessage() {
        return message;
    }
    public EditTermPage clickLinkEdit(String termName) throws Exception{
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText(termName)));
        WebElement term = webDriver.findElement(By.linkText(termName));
        String idTermView = term.getAttribute("id");
        String idTermEdit = idTermView.replace("view", "edit");
        webDriver.findElement(By.id(idTermEdit)).click();
        waitForPageToLoad();
        return new EditTermPage(webDriver, locations);
    }

}
