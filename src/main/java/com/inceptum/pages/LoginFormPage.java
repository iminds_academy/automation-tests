package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.JavascriptExecutor;

import com.inceptum.enumeration.PageLocation;

public class LoginFormPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "input[name='name']")
    private WebElement fieldEmail;
    @FindBy(how = How.CSS, using = "input[name='pass']")
    private WebElement fieldPassword;
    @FindBy(how = How.CSS, using = "input[value='Log in']")
    private WebElement buttonLogIn;
    @FindBy(how = How.LINK_TEXT, using = "Create new account")
    private WebElement linkCreateNewAccount;
    @FindBy(how = How.LINK_TEXT, using = "Request new password")
    private WebElement linkRequestNewPassword;
    @FindBy(how = How.ID, using = "navbar-item--4")
    private WebElement buttonMyAccountAdmin;
    @FindBy(how = How.CSS, using = "#modal-content .messages.messages--error")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.messages--error")
    private WebElement errorMessage;
    @FindBy(how = How.ID, using = "modal-content")
    private WebElement formLogin;


    /*---------CONSTRUCTORS--------*/

    public LoginFormPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void waitForPopup() {
        ((JavascriptExecutor) webDriver).executeScript("jQuery.fx.off = true;");
        waitUntilElementIsVisible(fieldEmail);
    }
    public WebElement getButtonMyAccountAdmin() {
        return buttonMyAccountAdmin;
    }
    public WebElement getFieldEmail() {
        return fieldEmail;
    }
    public void fillFieldEmail(String email) {
        waitUntilElementIsClickable(fieldEmail);
        fieldEmail.sendKeys(email);
    }
    public void fillFieldPassword(String password) {
        fieldPassword.sendKeys(password);
    }
    public BasePage clickButtonLogIn() throws Exception {
        waitUntilElementIsClickable(buttonLogIn);
        clickItem(buttonLogIn, "The button 'Log in' on the LoginForm popup could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until 'Log in' button will be invisible
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//input[@id='edit-submit'][@value='Log in']")));
        waitForPageToLoad();
        return new BasePage(webDriver, locations);
    }
    public WebElement getButtonLogIn() {
        return buttonLogIn;
    }
    public CreateNewAccountPage switchToCreateNewAccountPage() throws Exception {
        waitUntilElementIsClickable(linkCreateNewAccount);
        clickItem(linkCreateNewAccount, "'Create new account' page could not be opened.");
        waitForPageToLoad();
        return new CreateNewAccountPage(webDriver, locations);
    }
    public RequestNewPasswordPage switchToRequestNewPasswordPage() throws Exception {
        waitUntilElementIsClickable(linkRequestNewPassword);
        clickItem(linkRequestNewPassword, "'Request new password' page could not be opened.");
        return new RequestNewPasswordPage(webDriver, locations);
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getErrorMessage() {
        return errorMessage;
    }
    public WebElement getFormLogin() {
        return formLogin;
    }
}
