package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class ManageQuestionsQuizPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Manage questions")
    private WebElement tabManageQuestions;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.CSS, using = "#mq-fieldset a.fieldset-title")
    private WebElement fieldQuestionsInThisQuiz;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSubmit;
    @FindBy(how = How.CSS, using = "#edit-additional-questions .fieldset-legend a")
    private WebElement linkCreateNewQuestion;
    @FindBy(how = How.CSS, using = ".add-questions a[href*='multichoice']")
    private WebElement linkMultipleChoiceQuestion;


    /*---------CONSTRUCTORS--------*/

    public ManageQuestionsQuizPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTabManageQuestions() {
        waitUntilElementIsVisible(tabManageQuestions);
        return tabManageQuestions;
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getFieldQuestionsInThisQuiz() {
        waitUntilElementIsVisible(fieldQuestionsInThisQuiz);
        return fieldQuestionsInThisQuiz;
    }
    public ViewQuizPage clickTabView() throws Exception {
        waitUntilElementIsClickable(tabView);
        clickItem(tabView, "The tab View page could not be clicked from ManageQuestionsQuiz page.");
        waitForPageToLoad();
        return new ViewQuizPage(webDriver, locations);
    }
    public void addQuestionToQuiz(String question) throws Exception {
        waitForPageToLoad();
        // Exception for Linux/Chrome (remove '?' sign from the question)
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            String[] parts = question.split("\\?"); // Separate needed node URL
            question = parts[0]; // without '?'
        }

        // Select checkbox near needed question to add it to the quiz
        WebElement questionLink = webDriver.findElement(By.xpath("//div[@class='fieldset-wrapper']/table/tbody/tr/td[2]/a[contains(text(), '" + question + "')]"));
        WebElement checkbox = questionLink.findElement(By.xpath("../../td[1]/div/input"));
        waitUntilElementIsClickable(checkbox);

        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for IE: problem with selecting a checkbox
            checkbox.sendKeys(Keys.SPACE);
        } else clickItem(checkbox, "The checkbox for correct answer on the ManageQuestionsQuiz page could not be selected.");

        // Check that the question is in 'added questions' list
        List<WebElement> equalQuestions = webDriver.findElements(By.xpath("//*[@id='question-list']/tbody/tr/td[1]/a[2][contains(text(), '" + question + "')]"));
        for (WebElement addedQuestion : equalQuestions) {
            if (!(addedQuestion.findElement(By.xpath("../..")).getAttribute("class").matches(".*hidden-question.*"))) {
                return;
            }
        }
        Assert.assertTrue("The question is not shown in the list of added questions.", false);
    }
    public void clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The Submit button on the ManageQuestionsQuiz page could not be clicked.");
        waitForPageToLoad();
    }
    public void clickLinkCreateNewQuestion() throws Exception {
        waitUntilElementIsClickable(linkCreateNewQuestion);
        clickItem(linkCreateNewQuestion, "The link 'Create new question' on the ManageQuestionsQuiz page could not be clicked.");
        // Wait until fieldset is displayed
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#edit-additional-questions .fieldset-wrapper")));
    }
    public CreateMultipleChoiceQuestionPage clickLinkMCQuestion() throws Exception {
        waitUntilElementIsClickable(linkMultipleChoiceQuestion);
        clickItem(linkMultipleChoiceQuestion, "The link 'Multiple choice question' on the ManageQuestionsQuiz page could not be clicked.");
        waitForPageToLoad();
        return new CreateMultipleChoiceQuestionPage(webDriver, locations);
    }



}
