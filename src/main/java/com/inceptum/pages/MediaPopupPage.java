package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class MediaPopupPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "modal-content")
    private WebElement popup;
    @FindBy(how = How.CSS, using = ".parent+h2")
    private WebElement fieldPopupName;
    @FindBy(how = How.CSS, using = ".media-youtube-player")
    private WebElement iframeYoutubePlayer;
    @FindBy(how = How.CSS, using = ".media-vimeo-player")
    private WebElement iframeVimeoPlayer;
    @FindBy(how = How.CSS, using = ".ytp-play-button")
    private WebElement buttonPlayPauseYoutube;
    @FindBy(how = How.CSS, using = ".play-icon")
    private WebElement buttonPlayVimeo;
//    @FindBy(how = How.CSS, using = ".ytp-button-pause") // doesn't exist now
//    private WebElement buttonPauseYoutube;
    @FindBy(how = How.CSS, using = ".pause-icon")
    private WebElement buttonPauseVimeo;
    @FindBy(how = How.CSS, using = ".ytp-progress-bar")
    private WebElement progressBarYoutube;
    @FindBy(how = How.CSS, using = "[class*='scrubber-button']")
    private WebElement sliderYoutube;
    @FindBy(how = How.ID, using = "player")
    private WebElement videoYoutube;
    @FindBy(how = How.ID, using = "player")
    private WebElement videoVimeo;
    @FindBy(how = How.CSS, using = ".progress .loaded")
    private WebElement progressBarVimeo;


    /*---------CONSTRUCTORS--------*/
    public MediaPopupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void assertPopupIsOpened() {
        waitUntilElementIsVisible(popup);
    }
    public String getPopupName() {
        waitUntilElementIsVisible(fieldPopupName);
        String popupName = fieldPopupName.getText();
        return popupName;
    }
    public WebElement getIframeYoutubePlayer() {
        return iframeYoutubePlayer;
    }
    public WebElement getIframeVimeoPlayer() {
        return iframeVimeoPlayer;
    }
    public WebElement getButtonPlayPauseYoutube() {
        return buttonPlayPauseYoutube;
    }
//    public WebElement getButtonPauseYoutube() {
//        return buttonPauseYoutube;
//    }
    public WebElement getProgressBarYoutube() {
        return progressBarYoutube;
    }
    public WebElement getSliderYoutube() {
        return sliderYoutube;
    }
    public WebElement getVideoYoutube() {
        return videoYoutube;
    }
    public WebElement getVideoVimeo() {
        return videoVimeo;
    }
    public WebElement getButtonPlayVimeo() {
        return buttonPlayVimeo;
    }
    public WebElement getButtonPauseVimeo() {
        return buttonPauseVimeo;
    }
    public WebElement getProgressBarVimeo() {
        return progressBarVimeo;
    }

}
