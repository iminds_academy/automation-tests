package com.inceptum.pages;

import com.inceptum.enumeration.PageLocation;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Sasha on 8/31/2016.
 */
public class ModalClassWindowPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-cancel")
    private WebElement buttonCancel;

    /*---------CONSTRUCTORS--------*/
    public ModalClassWindowPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }

    public WebElement getButtonCancel() {
        return buttonCancel;
    }

    public HomePage clickButtonCancel() throws Exception {
        waitUntilElementIsClickable(getButtonCancel());
        clickItem(getButtonCancel(), "The button 'Cancel' on the HomePage could not be clicked.");
        return new HomePage(webDriver, locations);
    }
}
