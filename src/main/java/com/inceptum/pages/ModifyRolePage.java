package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class ModifyRolePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonNext;
    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;
    @FindBy(how = How.ID, using = "edit-add-roles")
    private WebElement fieldAddRoles;
    @FindBy(how = How.ID, using = "edit-remove-roles")
    private WebElement fieldRemoveRoles;


    /* ----- CONSTRUCTORS ----- */

    public ModifyRolePage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public void clickButtonNext() throws Exception {
        waitUntilElementIsClickable(buttonNext);
        clickItem(buttonNext, "The Next button on the ModifyRole page could not be clicked.");
    }
    public WebElement getFieldAddRoles() {
        return fieldAddRoles;
    }
    public WebElement getFieldRemoveRoles() {
        return fieldRemoveRoles;
    }
    public void clickLinkCancel() throws Exception {
        waitUntilElementIsClickable(linkCancel);
        clickItem(linkCancel, "The Cancel link on the ModifyRole page could not be clicked.");
    }

}
