package com.inceptum.pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class ModulesPage extends BasePage {

    /* ----- FIELDS ----- */

//    @FindBy(how = How.ID, using = "edit-modules-iminds-academy-imindsx-googleserviceaccount-oauth-enable")
//    private WebElement checkboxImindsXGoogleServiceAccountQAuth;
    @FindBy(how = How.XPATH, using = "//label[contains(@for, 'imindsx-googleserviceaccount-oauth')]/../.." +
            "/td[@class='checkbox']/div[1]")
    private WebElement switchImindsXGoogleServiceAccountQAuth;
    @FindBy(how = How.XPATH, using = "//label[contains(@for, 'tincanapi-iminds-peas')]/../../td[@class='checkbox']/div[1]")
    private WebElement switchTinCanApiPeas;
    @FindBy(how = How.XPATH, using = "//label[contains(@for, 'iminds-peas-peertask')]/../../td[@class='checkbox']/div[1]")
    private WebElement switchPeerAssignmentCore;
    @FindBy(how = How.XPATH, using = "//label[contains(@for, 'iminds-peas-peas-navigation')]/../../td[@class='checkbox']/div[1]")
    private WebElement switchPeerAssignmentNavigation;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveConfiguration;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;


    /*---------CONSTRUCTORS--------*/
    public ModulesPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getSwitchImindsXGoogleServiceAccountQAuth() {
        return switchImindsXGoogleServiceAccountQAuth;
    }
    public WebElement getSwitchTinCanApiPeas() {
        return switchTinCanApiPeas;
    }
    public WebElement getSwitchPeerAssignmentCore() {
        return switchPeerAssignmentCore;
    }
    public WebElement getSwitchPeerAssignmentNavigation() {
        return switchPeerAssignmentNavigation;
    }
    public void clickButtonSaveConfiguration() throws Exception {
        waitUntilElementIsClickable(buttonSaveConfiguration);
        buttonSaveConfiguration.sendKeys(Keys.ENTER);
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }

}
