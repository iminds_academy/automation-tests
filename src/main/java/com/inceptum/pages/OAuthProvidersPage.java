package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class OAuthProvidersPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Add provider")
    private WebElement linkAddProvider;
    @FindBy(how = How.ID, using = "edit-oauth-type")
    private WebElement fieldSelectOAuthType;
    @FindBy(how = How.CSS, using = "input[value='Next']")
    private WebElement buttonNext;
    @FindBy(how = How.ID, using = "edit-account-name")
    private WebElement fieldAccountName;
    @FindBy(how = How.ID, using = "edit-account-email")
    private WebElement fieldAccountEmail;
    @FindBy(how = How.CSS, using = "input[id='edit-private-key-upload']")
    private WebElement fieldPrivateKey;
    @FindBy(how = How.ID, using = "edit-private-key-upload-button")
    private WebElement buttonUpload;
    @FindBy(how = How.ID, using = "edit-private-key-remove-button")
    private WebElement buttonRemove;
    @FindBy(how = How.ID, using = "edit-storage-gdrive-convert")
    private WebElement checkboxConvertUploadedFiles;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveConfiguration;


    /*---------CONSTRUCTORS--------*/
    public OAuthProvidersPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickLinkAddProvider() throws Exception {
        waitUntilElementIsClickable(linkAddProvider);
        clickItem(linkAddProvider, "The link 'Add provider' on OAuthProviders page could not be clicked.");
    }
    public WebElement getLinkAddProvider() {
        return linkAddProvider;
    }
    public WebElement getFieldSelectOAuthType() {
        return fieldSelectOAuthType;
    }
    public void clickButtonNext() throws Exception {
        waitUntilElementIsClickable(buttonNext);
        clickItem(buttonNext, "The button Next on OAuthProviders page could not be clicked.");
    }
    public void fillFieldAccountName (String accountName) {
        waitUntilElementIsVisible(fieldAccountName);
        fieldAccountName.sendKeys(accountName);
    }
    public void fillFieldAccountEmail (String accountEmail) {
        waitUntilElementIsVisible(fieldAccountEmail);
        fieldAccountEmail.sendKeys(accountEmail);
    }
    public WebElement getFieldPrivateKey() {
        return fieldPrivateKey;
    }
    public void clickButtonUpload() throws Exception {
        waitUntilElementIsClickable(buttonUpload);
        clickItem(buttonUpload, "The button Upload on OAuthProviders page could not be clicked.");
    }
    public WebElement getButtonRemove() {
        return buttonRemove;
    }
    public void clickButtonSaveConfiguration() throws Exception {
        waitUntilElementIsClickable(buttonSaveConfiguration);
        clickItem(buttonSaveConfiguration, "The button 'Save configuration' on OAuthProviders page could not be clicked.");
        waitForPageToLoad();
    }
    public void selectCheckboxConvertUploadedFiles() {
        waitUntilElementIsClickable(checkboxConvertUploadedFiles);
        if (!(checkboxConvertUploadedFiles.isSelected())) {
            checkboxConvertUploadedFiles.click();
        }
        Assert.assertTrue("The checkbox is not selected.", checkboxConvertUploadedFiles.isSelected());
    }

}
