package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class OverviewAssignmentsPage extends HomePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".l-content-inner h2 select")
    private WebElement fieldClass;
    @FindBy(how = How.XPATH, using = "//div[@id='nav-panel-actions']/ul/li/a[text()='Add peer assignment']")
    private WebElement buttonAddPeerAssignment;
    @FindBy(how = How.CSS, using = ".l-content .messages.messages--status")
    private WebElement infoMessage;
    @FindBy(how = How.LINK_TEXT, using = "Group")
    private WebElement linkGroup;


    /* ----- CONSTRUCTORS ----- */

    public OverviewAssignmentsPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public WebElement getFieldClass() {
        waitUntilElementIsVisible(fieldClass);
        return fieldClass;
    }
    public void selectClass(String className) {
        List<WebElement> options = fieldClass.findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                option.click();
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public CreatePeerAssignmentPage clickButtonAddPeerAssignment() throws Exception {
        waitUntilElementIsClickable(buttonAddPeerAssignment);
        clickItem(buttonAddPeerAssignment, "The button 'Add peer assignment' on the OverviewAssignment page could not be clicked.");
        return new CreatePeerAssignmentPage(webDriver, locations);
    }
    public WebElement getInfoMessage() {
        waitUntilElementIsVisible(infoMessage);
        return infoMessage;
    }
    public GroupTabClassPage clickLinkGroup() throws Exception {
        waitUntilElementIsClickable(linkGroup);
        clickItem(linkGroup, "The link 'Group' on the OverviewAssignment page could not be clicked.");
        return new GroupTabClassPage(webDriver, locations);
    }


}
