package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class OverviewFeedbackPage extends HomePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "New feedback session")
    private WebElement buttonNewFeedbackSession;
    @FindBy(how = How.CSS, using = ".class-select select")
    private WebElement fieldSelectClass;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;


    /* ----- CONSTRUCTORS ----- */

    public OverviewFeedbackPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public CreateFeedbackPage clickButtonNewFeedbackSession() throws Exception {
        waitUntilElementIsClickable(buttonNewFeedbackSession);
        clickItem(buttonNewFeedbackSession, "The button 'New feedback session' on the OverviewFeedback page could not be clicked.");
        waitForPageToLoad();
        return new CreateFeedbackPage(webDriver, locations);
    }
    public WebElement getButtonNewFeedbackSession() {
        return buttonNewFeedbackSession;
    }
    public WebElement getMessage() {
        return message;
    }
    public void selectClassOnFeedbackOverviewPage(String className) throws Exception {
        waitUntilElementIsVisible(fieldSelectClass);
        List<WebElement> options = fieldSelectClass.findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                if (!(option.isSelected())) {
                    option.click();
                    waitForPageToLoad();
                }
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }

}
