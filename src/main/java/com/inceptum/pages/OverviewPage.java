package com.inceptum.pages;


import java.util.ArrayList;
import java.util.List;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class OverviewPage extends HomePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "nav-panel-content")
    private WebElement navPanel;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Re-order content")
    private WebElement buttonReOrderContent;
    @FindBy(how = How.CSS, using = ".messages.messages--warning.sortable")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".l-content .messages.messages--status")
    private WebElement infoMessage;
    @FindBy(how = How.CSS, using = ".messages--error.element-hidden.types_error")
    private WebElement errorMessage;
    @FindBy(how = How.XPATH, using = "//ul[@class='menu editor_menu_sortable sortable']/li/a[text()='Save']")
    private WebElement buttonSave;
    @FindBy(how = How.CSS, using = ".l-content-inner h2 select")
    private WebElement fieldClass;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Group")
    private WebElement linkGroup;
    @FindBy(how = How.CSS, using = "#nav-panel-actions .node_add") // at the top of page
    private WebElement buttonAddContent;
    @FindBy(how = How.CSS, using = "#nav-panel-content .node_add") // at the end of page
    private WebElement buttonAddContent2;
    @FindBy(how = How.CSS, using = "a.node-add-peertask")
    private WebElement linkPeerAssignment;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-live-session']")
    private WebElement linkLiveSession;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-info']")
    private WebElement linkInfoPage;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-rich-media-page']")
    private WebElement linkMediaPage;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-task']")
    private WebElement linkTask;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-theory']")
    private WebElement linkTheory;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-image']")
    private WebElement linkImage;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-presentation']")
    private WebElement linkPresentation;
    @FindBy(how = How.CSS, using = "#modal-content a[class*='add-video']")
    private WebElement linkVideo;
    @FindBy(how = How.CSS, using = "#modal-content .material-components a.fieldset-title")
    private WebElement linkCourseMaterials;
    @FindBy(how = How.CSS, using = "#modal-content .advanced-course-components a.fieldset-title")
    private WebElement linkAdvancedCourseComponents;
    @FindBy(how = How.CSS, using = "a[href*='/clone/confirm']")
    private WebElement linkClone;
    @FindBy(how = How.CSS, using = ".block__content .button a")
    private WebElement linkClass;
    @FindBy(how = How.TAG_NAME, using = "body")
    private WebElement bodyOfPage;
    @FindBy(how = How.CLASS_NAME, using = "arrow")
    private WebElement arrow;
    @FindBy(how = How.XPATH, using ="//a[@href='/class/example-class']")
    private WebElement exampleClass;



    /* ----- CONSTRUCTORS ----- */

    public OverviewPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public void assertChapterIsShown(String chapterTitle) {
        waitUntilElementIsVisible(navPanel);
        List<WebElement> options = navPanel.findElements(By.className("tag"));
        for (WebElement option : options) {
            if (option.getText().equals(chapterTitle)) {
                System.out.println("Option: " + option.getText());
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public void clickButtonReOrderContent() throws Exception {
        waitUntilElementIsClickable(buttonReOrderContent);
        clickItem(buttonReOrderContent, "The 'Re-order content' button on the Overview page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getButtonReOrderContent() {
        return buttonReOrderContent;
    }
    public WebElement getMessage() {
        waitUntilElementIsVisible(message);
        return message;
    }
    public WebElement getInfoMessage() {
        waitUntilElementIsVisible(infoMessage);
        return infoMessage;
    }
    public WebElement getErrorMessage() {
        waitUntilElementIsVisible(errorMessage);
        return errorMessage;
    }
    public WebElement fillLastChapterField(String title) {
        waitUntilElementIsVisible(navPanel);
        List<WebElement> elements = navPanel.findElements(By.tagName("input"));
        WebElement lastElement = elements.get(elements.size() - 1);
        lastElement.sendKeys(title);
        WebElement workshop = lastElement.findElement(By.xpath("./../.."));
        return workshop;
    }
    public void renameCreatedChapterField(String newName) {
        waitUntilElementIsVisible(navPanel);
        List<WebElement> elements = navPanel.findElements(By.tagName("input"));
        WebElement createdElement = elements.get(elements.size() - 2);
        createdElement.clear();
        createdElement.sendKeys(newName);
    }
    public WebElement clearChapterField(String chapterID) {
        waitUntilElementIsVisible(navPanel);
        // Get all elements with class 'workshop' from navigation panel
        List<WebElement> elements = new ArrayList<WebElement>();
        elements.addAll(navPanel.findElements(By.cssSelector(".workshop")));
        elements.addAll(navPanel.findElements(By.cssSelector(".workshop.first")));
        elements.addAll(navPanel.findElements(By.cssSelector(".workshop.last")));
        WebElement tag = null;
        String tagID = "";
        // Clear proper chapter field
        for (WebElement option : elements) {
            String className = option.findElement(By.xpath("./div[2]")).getAttribute("class");
            if (className.equals("tag")) { // find elements with class 'tag'
                tag = option.findElement(By.xpath("./div[2]"));
                tagID = tag.getAttribute("tagid");
                if (tagID != null && tagID.equals(chapterID)) { // find the tag which tagId is equal to chapterId
                    WebElement fieldChapter = tag.findElement(By.xpath("./input"));
                    fieldChapter.clear();
                    break;
                }
            }
        }
        return tag;
    }

    public void clickButtonSave() throws Exception {
        // Click button Save
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The 'Save' button on the Overview page could not be clicked.");
        // Alert can appear, so use the method below
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 4);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = webDriver.switchTo().alert();
            alert.accept();  // close alert window
            waitUntilElementIsClickable(buttonSave); // click Save button one more time
            clickItem(buttonSave, "The 'Save' button on the Overview page could not be clicked.");
        } catch (Exception e) {
            //exception handling
        }
        waitForPageToLoad();
    }
    public WebElement getButtonSave() {
        return buttonSave;
    }
    public WebElement getFieldClass() {
        waitUntilElementIsClickable(fieldClass);
        return fieldClass;
    }
    public WebElement getNavPanel() {
        waitUntilElementIsVisible(navPanel);
        return navPanel;
    }
    public void selectClassByName(String className) {
        waitUntilElementIsClickable(fieldClass);
        List<WebElement> options = fieldClass.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                if (!option.isSelected()) {
                    option.click();
                    return;  // option is clicked
                }
                else { return; } // option has been already selected from list
            }
        }
        Assert.assertTrue("No such option in the list.", false); // no such option
    }
    public String getClassNode() {
        waitUntilElementIsVisible(fieldClass);
        List<WebElement> options = fieldClass.findElements(By.tagName("option"));
        String classNode = "";
        for (WebElement option : options) {
            if (option.isSelected()) {
                classNode = option.getAttribute("value");
                break;
            }
        }
        return classNode;
    }
    public GroupTabClassPage clickLinkGroup() throws Exception {
        waitUntilElementIsClickable(linkGroup);
        clickItem(linkGroup, "The link 'Group' on the Overview page could not be clicked.");
        return new GroupTabClassPage(webDriver, locations);
    }
    public void clickButtonAddContent() throws Exception {
        waitUntilElementIsClickable(buttonAddContent);
        clickItem(buttonAddContent, "The button 'Add content' on the Overview page could not be clicked.");
        waitUntilElementIsVisible(getPopupModalContent());
    }
    public void clickButtonAddContent2() throws Exception {
        waitUntilElementIsClickable(buttonAddContent2);
        scrollToElement(buttonAddContent2);
        clickItem(buttonAddContent2, "The button 'Add content' at the end of Overview page could not be clicked.");
        waitUntilElementIsVisible(getPopupModalContent());
    }
    public CreatePeerAssignmentPage clickLinkPeerAssignmentOnPopup() throws Exception {
        waitUntilElementIsClickable(linkAdvancedCourseComponents);
        clickItem(linkAdvancedCourseComponents, "The link 'Advanced course components' on the AddContent page could not be clicked.");
        waitUntilElementIsClickable(linkPeerAssignment);
        scrollToElement(linkPeerAssignment);
        waitUntilElementIsClickable(linkPeerAssignment);
        clickItem(linkPeerAssignment, "The link 'Peer Assignment' on the AddContent popup on Overview page could not be clicked");
        CreatePeerAssignmentPage createPeerAssignmentPage = new CreatePeerAssignmentPage(webDriver, locations);
        waitUntilElementIsVisible(createPeerAssignmentPage.getFieldTitle());
        return new CreatePeerAssignmentPage(webDriver, locations);
    }
    public EditClassPage clickLinkClone() throws Exception {
        waitUntilElementIsClickable(linkClone);
        clickItem(linkClone, "The link 'Clone' on the Overview page could not be clicked.");
        waitForPageToLoad();
        return new EditClassPage(webDriver, locations);
    }
    public String getTextOfSelectedClass() throws Exception {
        BasePage basePage = new BasePage(webDriver, locations);
        waitUntilElementIsVisible(basePage.getMenuClass());
        waitUntilElementIsVisible(linkClass);
        return linkClass.getText().trim();
    }
    public ViewLiveSessionPage clickLinkLiveSession() throws Exception {
        waitUntilElementIsClickable(linkLiveSession);
        clickItem(linkLiveSession, "The link 'Live session' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewLiveSessionPage(webDriver, locations);
    }
    public ViewInfoPage clickLinkInfoPage() throws Exception {
        waitUntilElementIsClickable(linkInfoPage);
        clickItem(linkInfoPage, "The link 'Info page' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewInfoPage(webDriver, locations);
    }
    public ViewMediaPage clickLinkMediaPage() throws Exception {
        waitUntilElementIsClickable(linkMediaPage);
        clickItem(linkMediaPage, "The link 'Media page' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewMediaPage(webDriver, locations);
    }
    public ViewTaskPage clickLinkTask() throws Exception {
        waitUntilElementIsClickable(linkTask);
        scrollToElement(linkTask);
        clickItem(linkTask, "The link 'Task' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewTaskPage(webDriver, locations);
    }
    public ViewTheoryPage clickLinkTheory() throws Exception {
        waitUntilElementIsClickable(linkTheory);
        scrollToElement(linkTheory);
        clickItem(linkTheory, "The link 'Theory' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewTheoryPage(webDriver, locations);
    }
    public ViewImagePage clickLinkImage() throws Exception {
        clickLinkCourseMaterials();
        waitUntilElementIsClickable(linkImage);
        scrollToElement(linkImage);
        clickItem(linkImage, "The link 'Image' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewImagePage(webDriver, locations);
    }
    public ViewPresentationPage clickLinkPresentation() throws Exception {
        clickLinkCourseMaterials();
        waitUntilElementIsClickable(linkPresentation);
        scrollToElement(linkPresentation);
        clickItem(linkPresentation, "The link 'Presentation' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewPresentationPage(webDriver, locations);
    }
    public ViewVideoPage clickLinkVideo() throws Exception {
        clickLinkCourseMaterials();
        waitUntilElementIsClickable(linkVideo);
        scrollToElement(linkVideo);
        clickItem(linkVideo, "The link 'Video' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
        return new ViewVideoPage(webDriver, locations);
    }
    public void clickLinkCourseMaterials() throws Exception {
        waitUntilElementIsClickable(linkCourseMaterials);
        scrollToElement(linkCourseMaterials);
        clickItem(linkCourseMaterials, "The link 'Course materials' on the AddContent popup on Overview page could not be clicked");
        waitForPageToLoad();
    }


    public String getUrlNodeForClass(){
        String value="";
        String classValue=bodyOfPage.getAttribute("class");
        String[] parts=classValue.split(" ");
        for(String element:parts){
            if (element.contains("page-overview-nojs-")&&19<element.length()&&element.length()<23){ value=element;}
        }
        String[] number=value.split("-");
        return number[3];
    }

    public void selectClassOnNavBar(){
        arrow.click();
        waitUntilElementIsVisible(exampleClass);
        exampleClass.click();

    }


}
