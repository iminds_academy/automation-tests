package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class PeopleInGroupForClassPage extends CreateClassPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-name")
    private WebElement fieldUsername;
    @FindBy(how = How.ID, using = "edit-operation")
    private WebElement fieldOperations;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonAddUsers;
    @FindBy(how = How.CSS, using = "input[value*='Execute']")
    private WebElement buttonExecute;
    @FindBy(how = How.CSS, using = ".messages.messages--status")  //.messages.status
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement messageError;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Group")
    private WebElement linkGroup;


    /*---------CONSTRUCTORS--------*/
    public PeopleInGroupForClassPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldUsername() {
        waitUntilElementIsVisible(fieldUsername);
        return fieldUsername;
    }
    public void clickButtonAddUsers() throws Exception {
        waitUntilElementIsClickable(buttonAddUsers);
        clickItem(buttonAddUsers, "The button 'Add users' on the PeopleInGroup page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getMessageError() {
        return messageError;
    }
    public GroupTabClassPage clickLinkGroup() throws Exception {
        waitUntilElementIsClickable(linkGroup);
        clickItem(linkGroup, "The link 'Group' on the PeopleInGroup page could not be clicked.");
        return new GroupTabClassPage(webDriver, locations);
    }
    public DeleteItemConfirmationPage clickButtonExecute() throws Exception {
        waitUntilElementIsClickable(buttonExecute);
        clickItem(buttonExecute, "The button 'Execute' on the PeopleInGroup page could not be clicked.");
        return new DeleteItemConfirmationPage(webDriver, locations);
    }
    public WebElement getFieldOperations() {
        return fieldOperations;
    }

}
