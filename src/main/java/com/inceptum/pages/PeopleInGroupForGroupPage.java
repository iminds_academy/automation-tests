package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class PeopleInGroupForGroupPage extends CreateClassPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-name")
    private WebElement fieldUsername;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonAddUsers;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement messageError;
    @FindBy(how = How.LINK_TEXT, using = "Group")
    private WebElement linkGroup;


    /*---------CONSTRUCTORS--------*/
    public PeopleInGroupForGroupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldUsername() {
        waitUntilElementIsVisible(fieldUsername);
        return fieldUsername;
    }
    public void clickButtonAddUsers() throws Exception {
        waitUntilElementIsVisible(buttonAddUsers);
        waitUntilElementIsClickable(buttonAddUsers);
        clickItem(buttonAddUsers, "The button 'Add users' on the PeopleInGroup page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public GroupTabGroupPage clickLinkGroup() throws Exception {
        waitUntilElementIsClickable(linkGroup);
        clickItem(linkGroup, "The link 'Group' on the PeopleInGroup page could not be clicked.");
        waitForPageToLoad();
        return new GroupTabGroupPage(webDriver, locations);
    }

}
