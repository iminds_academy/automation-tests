package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class PeoplePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-operation")
    private WebElement fieldUpdateOptions;
    @FindBy(how = How.CSS, using = ".form-submit[value='Update']")
    private WebElement buttonUpdate;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;


    /*---------CONSTRUCTORS--------*/
    public PeoplePage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldUpdateOptions() {
        return fieldUpdateOptions;
    }
    public DeleteUserAccountConfirmationPage clickButtonUpdate() throws Exception {
        waitUntilElementIsClickable(buttonUpdate);
        clickItem(buttonUpdate, "The button 'Update' on the People page could not be clicked.");
        waitForPageToLoad();
        return new DeleteUserAccountConfirmationPage(webDriver, locations);
    }
    public WebElement getButtonUpdate() {
        return buttonUpdate;
    }
    public WebElement getMessage() {
        return message;
    }

}
