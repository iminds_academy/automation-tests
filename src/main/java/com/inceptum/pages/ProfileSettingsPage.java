package com.inceptum.pages;


import java.io.File;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class ProfileSettingsPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-picture-upload")
    private WebElement fieldUploadPicture;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSave;



    /*---------CONSTRUCTORS--------*/
    public ProfileSettingsPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void uploadImage(String imagePath) throws Exception {
        String imageAbsolutePath = new File(imagePath).getAbsolutePath();
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") ||
                PropertyLoader.loadProperty("browser.platform").equals("any")) { // Exception for Linux OS/Mavericks: enter a correct file path
            String imagePathLinux = imageAbsolutePath.replace("D:", "");
            fieldUploadPicture.sendKeys(imagePathLinux);
        } else fieldUploadPicture.sendKeys(imageAbsolutePath);
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The button 'Save' on the ProfileSettings page could not be clicked.");
        waitForPageToLoad();
    }


}
