package com.inceptum.pages;

import java.util.Calendar;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 11.08.2014.
 */
public class RecentLogMessagesPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Clear log messages")
    private WebElement fieldClearLogMessages;
    @FindBy(how = How.ID, using = "edit-clear")
    private WebElement buttonClearLogMessages;
    @FindBy(how = How.XPATH, using = "//*[@id=\"admin-dblog\"]/tbody/tr/td[1]")
    private WebElement message;
    @FindBy(how = How.XPATH, using = "//th[text()='Message']/../td")
    private WebElement logMessage;
    @FindBy(how = How.ID, using = "admin-dblog")
    private WebElement logTable;


    /*---------CONSTRUCTORS--------*/

    public RecentLogMessagesPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickButtonClearLogMessages() throws Exception {
        waitUntilElementIsClickable(fieldClearLogMessages);
        clickItem(fieldClearLogMessages, "The fieldset 'ClearLogMessages' on the RecentLogMessages page could not be opened.");
        waitUntilElementIsClickable(buttonClearLogMessages);
        clickItem(buttonClearLogMessages, "The button 'ClearLogMessages' on the RecentLogMessages page could not be clicked.");
    }
    public void checkEmptyListOfLogMessages() throws Exception {
        // Wait until "Clear log messages" button disappears
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-clear")));
        // Check if all log messages are deleted
        if (!(message.getText().matches("No log messages available."))) {
            // Get current time in seconds
            Calendar calendar = Calendar.getInstance();
            int secondsCurrentTime = calendar.get(Calendar.SECOND);
            while (message.getText().matches("No log messages available.")) {
                clickButtonClearLogMessages();
                // Break after 10 sec in any case
                if (calendar.get(Calendar.SECOND) >= secondsCurrentTime + 10) {
                    break;
                }
            }
        }
        Assert.assertTrue("Not all log messages were deleted.", message.getText().matches("No log messages available."));
    }
    public String getTextOfLogMessage() {
        waitUntilElementIsVisible(logMessage);
        String text = logMessage.getText();
        return text;
    }
    public WebElement getLogTable() {
        waitUntilElementIsVisible(logTable);
        return logTable;
    }

}
