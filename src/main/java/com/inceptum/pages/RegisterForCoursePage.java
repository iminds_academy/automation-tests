package com.inceptum.pages;


import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class RegisterForCoursePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "[id*='edit-submit'][value='Confirm']")
    private WebElement buttonConfirmRegistration;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Register for a course:')]")
    private WebElement labelRegisterForACourse;



    /*---------CONSTRUCTORS--------*/

    public RegisterForCoursePage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public HomePage clickButtonConfirmRegistration() throws Exception {
        waitUntilElementIsClickable(buttonConfirmRegistration);
        buttonConfirmRegistration.sendKeys(Keys.RETURN);
        waitForPageToLoad();
        return new HomePage(webDriver, locations);
    }
    public WebElement getLabelRegisterForACourse() {
        return labelRegisterForACourse;
    }

}
