package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class RemoveMembershipInGroupPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonRemove;
    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement linkCancel;


    /* ----- CONSTRUCTORS ----- */

    public RemoveMembershipInGroupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public void clickButtonRemove() throws Exception {
        waitUntilElementIsClickable(buttonRemove);
        clickItem(buttonRemove, "The button 'Remove' on the RemoveMembershipInGroup page could not be clicked.");
    }
    public void clickLinkCancel() throws Exception {
        waitUntilElementIsClickable(linkCancel);
        clickItem(linkCancel, "The link 'Cancel' on the RemoveMembershipInGroup page could not be clicked.");
    }



}
