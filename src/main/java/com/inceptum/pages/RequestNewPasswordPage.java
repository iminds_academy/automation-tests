package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class RequestNewPasswordPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "input[name='name']")
    private WebElement fieldUsernameOrEmailAddress;
    @FindBy(how = How.CSS, using = "input[value='E-mail new password']")
    private WebElement buttonEmailNewPassword;
    @FindBy(how = How.CSS, using = ".messages.messages--error")
    private WebElement messageError;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;



    /*---------CONSTRUCTORS--------*/

    public RequestNewPasswordPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldUsernameOrEmailAddress(String usernameOrEmailAddress) {
        waitUntilElementIsVisible(fieldUsernameOrEmailAddress);
        fieldUsernameOrEmailAddress.sendKeys(usernameOrEmailAddress);
    }
    public void clickButtonEmailNewPassword() throws Exception {
        waitUntilElementIsClickable(buttonEmailNewPassword);
        clickItem(buttonEmailNewPassword, "The button 'E-mail new password' on the RequestNewPasswordForm popup could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessageError() {
        return messageError;
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getFieldUsernameOrEmailAddress() {
        waitUntilElementIsVisible(fieldUsernameOrEmailAddress);
        return fieldUsernameOrEmailAddress;
    }
}
