package com.inceptum.pages;


import java.io.File;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class SelectImagePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-upload")
    private WebElement fieldUploadANewFile;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSubmit;
    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement buttonCancel;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Library")
    private WebElement linkLibrary;
    @FindBy(how = How.LINK_TEXT, using = "Submit")
    private WebElement buttonSubmitLibrary;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;



    /*---------CONSTRUCTORS--------*/
    public SelectImagePage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldUploadANewFile() {
        return fieldUploadANewFile;
    }
    public CreateImagePage clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The 'Submit' button on the UploadImage popup could not be clicked.");
        return new CreateImagePage(webDriver, locations);
    }
    public CreateImagePage clickButtonSubmitLibrary() throws Exception {
        clickItem(buttonSubmitLibrary, "The 'Submit' button on the AddImageLibrary popup could not be clicked.");
        return new CreateImagePage(webDriver, locations);
    }
    public void clickLinkLibrary() throws Exception {
        waitUntilElementIsClickable(linkLibrary);
        clickItem(linkLibrary, "The 'Library' link on the UploadImage popup could not be clicked.");
        waitForPageToLoad();
    }

    public CreateImagePage selectImage(String imgPath) throws Exception {
        CreateImagePage createImagePage = new CreateImagePage(webDriver, locations);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        waitUntilElementIsVisible(fieldUploadANewFile);
        String imagePath = new File(imgPath).getAbsolutePath();
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") ||
                PropertyLoader.loadProperty("browser.platform").equals("any")) { // Exception for Linux OS/Mavericks: enter a correct file path
            String imagePathLinux = imagePath.replace("D:", "");
            //String imageNewPath = imagePathLinux.replaceAll("\\\\", "/");
            fieldUploadANewFile.sendKeys(imagePathLinux);
        } else fieldUploadANewFile.sendKeys(imagePath);
        clickButtonSubmit();
        webDriver.switchTo().defaultContent();
        waitUntilElementIsVisible(createImagePage.getLinkRemoveMedia());
        return createImagePage;
    }
    public void selectImageInFrame(String imgPath) throws Exception {
        String imagePath = new File(imgPath).getAbsolutePath();
        fieldUploadANewFile.sendKeys(imagePath);
        clickButtonSubmit();
    }
    public WebElement getButtonCancel() {
        return buttonCancel;
    }
    public CreateImagePage selectImageFromLibrary() throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        clickLinkLibrary();
        // Select an image
        WebElement imageItemList = webDriver.findElement(By.id("media-browser-library-list"));
        waitUntilElementIsClickable(imageItemList);
        List<WebElement> imageItems = imageItemList.findElements(By.tagName("img"));
        imageItems.get(0).click();
        clickButtonSubmitLibrary();
        webDriver.switchTo().defaultContent();
        return new CreateImagePage(webDriver, locations);
    }
    public WebElement getMessage() {
        return message;
    }

}
