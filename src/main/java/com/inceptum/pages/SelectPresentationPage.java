package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class SelectPresentationPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-embed-code")
    private WebElement fieldUrlOrEmbedCode;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSubmit;
    @FindBy(how = How.CSS, using = "#media-tab-library a[class*='button-no']")
    private WebElement buttonCancel;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Library")
    private WebElement linkLibrary;
    @FindBy(how = How.CSS, using = "#media-tab-library a[class*='button-yes']")
    private WebElement buttonSubmitLibrary;
    @FindBy(how = How.LINK_TEXT, using = "Slideshare")
    private WebElement linkSlideshare;
    @FindBy(how = How.CSS, using = ".messages.error") //*[@id=\"media-browser-page\"]/div[1]")
    private WebElement message;


    /*---------CONSTRUCTORS--------*/
    public SelectPresentationPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldUrlOrEmbedCode(String urlOrEmbedCode) throws Exception {
        waitUntilElementIsVisible(fieldUrlOrEmbedCode);
        fieldUrlOrEmbedCode.sendKeys(urlOrEmbedCode);
    }
    public WebElement getFieldUrlOrEmbedCode() throws Exception {
        waitUntilElementIsVisible(fieldUrlOrEmbedCode);
        return fieldUrlOrEmbedCode;
    }
    public CreatePresentationPage clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The 'Submit' button on the AddPresentation popup could not be clicked.");
        return new CreatePresentationPage(webDriver, locations);
    }
    public WebElement getButtonCancel() {
        return buttonCancel;
    }
    public void clickLinkLibrary() throws Exception {
        waitUntilElementIsClickable(linkLibrary);
        clickItem(linkLibrary, "The 'Library' link on the AddPresentation popup could not be clicked.");
        waitForPageToLoad();
    }
    public CreatePresentationPage clickButtonSubmitLibrary() throws Exception {
        waitUntilElementIsClickable(buttonSubmitLibrary);
        clickItem(buttonSubmitLibrary, "The 'Submit' button on the AddPresentationLibrary popup could not be clicked.");
        return new CreatePresentationPage(webDriver, locations);
    }
    public CreatePresentationPage selectSlidesharePresentation(String urlOrEmbedCode) throws Exception {
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, locations);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        waitUntilElementIsVisible(linkSlideshare);
        Assert.assertTrue(linkSlideshare.isDisplayed());
        // Check the link is valid
        Assert.assertEquals("http://www.slideshare.net/", linkSlideshare.getAttribute("href"));
        fillFieldUrlOrEmbedCode(urlOrEmbedCode);
        clickButtonSubmit();
        conditionPresentationInLibrary();
        waitUntilElementIsVisible(createPresentationPage.getLinkRemoveMedia());
        return createPresentationPage;
    }
    // Condition for the case when a presentation is already exists in a Library:
    public void conditionPresentationInLibrary() throws Exception {
        Thread.sleep(1000);
        List<WebElement> messages = webDriver.findElements(By.cssSelector(".messages.error"));
        if (messages.size()>0) {
            String textErrorMessage = message.getText();
            if (textErrorMessage.contains("You have entered a URL for a slideshare presentation that is already in your library.")) {
                clickLinkLibrary();
                // Waiting till thumbnails appear
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.presenceOfElementLocated(By.className("media-item")));
                // Select the 1st thumbnail from list
                webDriver.findElement(By.className("media-item")).click();
                clickButtonSubmitLibrary();
                webDriver.switchTo().defaultContent();
            }
        } else webDriver.switchTo().defaultContent();
    }
    public WebElement getMessage() {
        return message;
    }
    public void waitingTillThumbnailsAppear() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("media-item")));
    }
    public void select1stThumbnailFromList() {
        webDriver.findElement(By.className("media-item")).click();
    }

}
