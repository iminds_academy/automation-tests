package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class SelectVideoPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-embed-code")
    private WebElement fieldUrlOrEmbedCode;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonSubmit;
    @FindBy(how = How.LINK_TEXT, using = "Cancel")
    private WebElement buttonCancel;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Library")
    private WebElement linkLibrary;
    @FindBy(how = How.LINK_TEXT, using = "Submit")
    private WebElement buttonSubmitLibrary;
    @FindBy(how = How.LINK_TEXT, using = "Zentrick")
    private WebElement linkZentrick;
    @FindBy(how = How.LINK_TEXT, using = "Youtube")
    private WebElement linkYoutube;
    @FindBy(how = How.LINK_TEXT, using = "Vimeo")
    private WebElement linkVimeo;
    @FindBy(how = How.CSS, using = ".messages.error")
    private WebElement message;


    /*---------CONSTRUCTORS--------*/
    public SelectVideoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldUrlOrEmbedCode(String urlOrEmbedCode) throws Exception {
        waitUntilElementIsVisible(fieldUrlOrEmbedCode);
        fieldUrlOrEmbedCode.sendKeys(urlOrEmbedCode);
    }
    public WebElement getFieldUrlOrEmbedCode() throws Exception {
        waitUntilElementIsVisible(fieldUrlOrEmbedCode);
        return fieldUrlOrEmbedCode;
    }
    public CreateVideoPage clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The 'Submit' button on the AddVideo popup could not be clicked.");
        return new CreateVideoPage(webDriver, locations);
    }
    public WebElement getButtonCancel() {
        return buttonCancel;
    }
    public void clickLinkLibrary() throws Exception {
        waitUntilElementIsClickable(linkLibrary);
        clickItem(linkLibrary, "The 'Library' link on the AddVideo popup could not be clicked.");
    }
    public CreateVideoPage clickButtonSubmitLibrary() throws Exception {
        waitUntilElementIsClickable(buttonSubmitLibrary);
        clickItem(buttonSubmitLibrary, "The 'Submit' button on the AddVideoLibrary popup could not be clicked.");
        return new CreateVideoPage(webDriver, locations);
    }
    public CreateVideoPage selectVideo(String urlOrEmbedCode) throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        wait.until(ExpectedConditions.visibilityOf(linkZentrick));
        Assert.assertTrue(linkZentrick.isDisplayed());
        // Check the link for Zentrick is valid
        Assert.assertEquals("http://studio.zentrick.com/", linkZentrick.getAttribute("href"));
        Assert.assertTrue(linkYoutube.isDisplayed());
        // Check the link for Youtube is valid
        Assert.assertEquals("http://www.youtube.com/", linkYoutube.getAttribute("href"));
        Assert.assertTrue(linkVimeo.isDisplayed());
        // Check the link for Vimeo is valid
        Assert.assertEquals("https://vimeo.com/", linkVimeo.getAttribute("href"));
        fillFieldUrlOrEmbedCode(urlOrEmbedCode);
        clickButtonSubmit();
        webDriver.switchTo().defaultContent();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("mediaBrowser")));
        return createVideoPage;
    }
    public CreateVideoPage selectVideoFromLibrary(String videoThumbnail) throws Exception {
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, locations);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        clickLinkLibrary();
        // Select created video
        WebElement videoItemList = webDriver.findElement(By.id("media-browser-library-list"));
        waitUntilElementIsClickable(videoItemList);
        List<WebElement> videoItems = videoItemList.findElements(By.tagName("img"));

        boolean found = false;
        for (WebElement option : videoItems) {
            if (option.getAttribute("src").equals(videoThumbnail)) {
                option.click();
                found = true;
                break;
            }
        }
        Assert.assertTrue(found);
        clickButtonSubmitLibrary();
        webDriver.switchTo().defaultContent();
        wait.until(ExpectedConditions.visibilityOf(createVideoPage.getFieldTitle()));
        return createVideoPage;
    }
    public WebElement getMessage() {
        return message;
    }

}
