package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class StructurePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Taxonomy")
    private WebElement linkTaxonomy;



    /*---------CONSTRUCTORS--------*/
    public StructurePage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public TaxonomyPage clickLinkTaxonomy() throws Exception {
        waitUntilElementIsClickable(linkTaxonomy);
        clickItem(linkTaxonomy, "The link 'Taxonomy' on Structure page could not be clicked.");
        waitForPageToLoad();
        return new TaxonomyPage(webDriver, locations);
    }

}
