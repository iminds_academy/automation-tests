package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class TakeQuizPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Take")
    private WebElement tabTake;
    @FindBy(how = How.ID, using = "quiz-question-number")
    private WebElement fieldQuestionNumber;
    @FindBy(how = How.CSS, using = "#edit-submit[value='Next']")
    private WebElement buttonNext;
    @FindBy(how = How.CSS, using = "#edit-submit[value='Finish']")
    private WebElement buttonFinish;


    /*---------CONSTRUCTORS--------*/

    public TakeQuizPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTabTake() {
        return tabTake;
    }

    public void clickButtonNext() throws Exception {
        waitUntilElementIsClickable(buttonNext);
        clickItem(buttonNext, "The button 'Next' on TakeQuiz page could not be clicked.");
        waitForPageToLoad();
    }
    public void clickButtonFinish() throws Exception {
        waitUntilElementIsClickable(buttonFinish);
        clickItem(buttonFinish, "The button 'Finish' on TakeQuiz page could not be clicked.");
    }

    public void checkQuestionAndItsOrderNumber(String question, int number) {
        // Exception for Linux/Chrome (remove '?' sign from the question)
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            String[] parts = question.split("\\?"); // Separate needed node URL
            question = parts[0]; // without '?'
        }

        // Check that the question order number of Quiz is correct
        String numberExpected = Integer.toString(number);
        waitUntilElementIsVisible(fieldQuestionNumber);
        String numberActual = fieldQuestionNumber.getText();
        Assert.assertEquals("Current number of question is incorrect: " + numberActual + "instead of " + numberExpected + ".",
                numberExpected, numberActual);
        // Check that question name is shown on the page correctly
        WebElement questionField = webDriver.findElement(By.xpath("//p[text()='" + question + "']"));
        Assert.assertTrue(questionField.isDisplayed());
    }

    public void selectVariant(int i) {
        String number = Integer.toString(i);
        String css = "#edit-tries-answer tr:nth-of-type(" + number + ") input";
        WebElement variant = webDriver.findElement(By.cssSelector(css));
        waitUntilElementIsClickable(variant);
        variant.click();
    }

}
