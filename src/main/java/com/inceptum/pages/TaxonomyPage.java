package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class TaxonomyPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-1-add")
    private WebElement linkAddTermsForLiveSessionTypes;
    @FindBy(how = How.ID, using = "edit-2-add")
    private WebElement linkAddTermsForChapter;
    @FindBy(how = How.ID, using = "edit-1-list")
    private WebElement linkListTermsForLiveSessionTypes;
    @FindBy(how = How.ID, using = "edit-2-list")
    private WebElement linkListTermsForChapter;



    /*---------CONSTRUCTORS--------*/
    public TaxonomyPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public AddTermPage clickLinkAddTermsForLiveSessionTypes() throws Exception {
        waitUntilElementIsClickable(linkAddTermsForLiveSessionTypes);
        clickItem(linkAddTermsForLiveSessionTypes, "The link 'add terms' for LiveSessionTypes on Taxonomy page could not be clicked.");
        waitForPageToLoad();
        return new AddTermPage(webDriver, locations);
    }
    public AddTermPage clickLinkAddTermsForChapter() throws Exception {
        waitUntilElementIsClickable(linkAddTermsForChapter);
        clickItem(linkAddTermsForChapter, "The link 'add terms' for Chapter on Taxonomy page could not be clicked.");
        waitForPageToLoad();
        return new AddTermPage(webDriver, locations);
    }
    public ListTermsPage clickLinkListTermsForLiveSessionTypes() throws Exception {
        waitUntilElementIsClickable(linkListTermsForLiveSessionTypes);
        clickItem(linkListTermsForLiveSessionTypes, "The link 'list terms' for LiveSessionTypes on Taxonomy page could not be clicked.");
        waitForPageToLoad();
        return new ListTermsPage(webDriver, locations);
    }
    public ListTermsPage clickLinkListTermsForChapter() throws Exception {
        waitForPageToLoad();
        waitUntilElementIsClickable(linkListTermsForChapter);
        clickItem(linkListTermsForChapter, "The link 'list terms' for Chapter on Taxonomy page could not be clicked.");
        waitForPageToLoad();
        return new ListTermsPage(webDriver, locations);
    }

}
