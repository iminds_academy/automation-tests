package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 10.11.2014.
 */
public class TinCanApiPage extends BasePage {


    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "edit-tincanapi-watchdog")
    private WebElement checkboxLogServerResponse;
    @FindBy(how = How.ID, using = "edit-tincanapi-anonymous")
    private WebElement checkboxTrackAnonymousUsers;
    @FindBy(how = How.ID, using = "edit-tincanapi-simplify-id")
    private WebElement checkboxSimplifyStatementsIDs;
    @FindBy(how = How.ID, using = "edit-tincanapi-content-types-class")
    private WebElement checkboxClass;
    @FindBy(how = How.ID, using = "edit-tincanapi-content-types-group")
    private WebElement checkboxGroupContentTypes;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-group")
    private WebElement checkboxGroupIMinds;
    @FindBy(how = How.ID, using = "edit-tincanapi-content-types-image")
    private WebElement checkboxImage;
    @FindBy(how = How.ID, using = "edit-tincanapi-content-types-presentation")
    private WebElement checkboxPresentation;
    @FindBy(how = How.ID, using = "edit-tincanapi-content-types-video")
    private WebElement checkboxVideo;
    @FindBy(how = How.ID, using = "edit-tincanapi-view-modes-full")
    private WebElement checkboxFullContent;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-external-content")
    private WebElement checkboxEmbedContent;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-info")
    private WebElement checkboxInfoPage;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-live-session")
    private WebElement checkboxLiveSession;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-peertask")
    private WebElement checkboxPeerAssignment;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-rich-media-page")
    private WebElement checkboxMediaPage;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-task")
    private WebElement checkboxTaskPage;
    @FindBy(how = How.ID, using = "edit-tincanapi-iminds-course-components-theory")
    private WebElement checkboxTheoryPage;

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Content Types")
    private WebElement linkContentTypes;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "iMinds")
    private WebElement linkIminds;

    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveConfiguration;
    @FindBy(how = How.CSS, using = ".messages.status")
    private WebElement message;
    @FindBy(how = How.ID, using = "edit-tincanapi-endpoint")
    private WebElement fieldEndpoint;
    @FindBy(how = How.ID, using = "edit-tincanapi-auth-user")
    private WebElement fieldUser;
    @FindBy(how = How.ID, using = "edit-tincanapi-auth-password")
    private WebElement fieldPassword;


    /*---------CONSTRUCTORS--------*/

    public TinCanApiPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getCheckboxLogServerResponse() {
        waitUntilElementIsClickable(checkboxLogServerResponse);
        return checkboxLogServerResponse;
    }
    public WebElement getCheckboxTrackAnonymousUsers() {
        waitUntilElementIsClickable(checkboxTrackAnonymousUsers);
        return checkboxTrackAnonymousUsers;
    }
    public WebElement getCheckboxSimplifyStatementsIDs() {
        waitUntilElementIsClickable(checkboxSimplifyStatementsIDs);
        return checkboxSimplifyStatementsIDs;
    }
    public WebElement getCheckboxClass() {
        waitUntilElementIsClickable(checkboxClass);
        return checkboxClass;
    }
    public WebElement getCheckboxGroupContentTypes() {
        waitUntilElementIsClickable(checkboxGroupContentTypes);
        return checkboxGroupContentTypes;
    }
    public WebElement getCheckboxGroupIMinds() {
        waitUntilElementIsClickable(checkboxGroupIMinds);
        return checkboxGroupIMinds;
    }
    public WebElement getCheckboxImage() {
        waitUntilElementIsClickable(checkboxImage);
        return checkboxImage;
    }
    public WebElement getCheckboxPresentation() {
        waitUntilElementIsClickable(checkboxPresentation);
        return checkboxPresentation;
    }
    public WebElement getCheckboxVideo() {
        waitUntilElementIsClickable(checkboxVideo);
        return checkboxVideo;
    }
    public WebElement getCheckboxFullContent() {
        waitUntilElementIsClickable(checkboxFullContent);
        return checkboxFullContent;
    }
    public WebElement getCheckboxEmbedContent() {
        waitUntilElementIsClickable(checkboxEmbedContent);
        return checkboxEmbedContent;
    }
    public WebElement getCheckboxInfoPage() {
        waitUntilElementIsClickable(checkboxInfoPage);
        return checkboxInfoPage;
    }
    public WebElement getCheckboxLiveSession() {
        waitUntilElementIsClickable(checkboxLiveSession);
        return checkboxLiveSession;
    }
    public WebElement getCheckboxPeerAssignment() {
        waitUntilElementIsClickable(checkboxPeerAssignment);
        return checkboxPeerAssignment;
    }
    public WebElement getCheckboxMediaPage() {
        waitUntilElementIsClickable(checkboxMediaPage);
        return checkboxMediaPage;
    }
    public WebElement getCheckboxTaskPage() {
        waitUntilElementIsClickable(checkboxTaskPage);
        return checkboxTaskPage;
    }
    public WebElement getCheckboxTheoryPage() {
        waitUntilElementIsClickable(checkboxTheoryPage);
        return checkboxTheoryPage;
    }
    public void clickLinkContentTypes() throws Exception {
        waitUntilElementIsClickable(linkContentTypes);
        clickItem(linkContentTypes, "The link 'Content Types' on Tin Can API page could not be clicked.");
    }
    public void clickLinkIminds() throws Exception {
        waitUntilElementIsClickable(linkIminds);
        clickItem(linkIminds, "The link 'iMinds' on Tin Can API page could not be clicked.");
    }
    public void clickButtonSaveConfiguration() throws Exception {
        waitUntilElementIsClickable(buttonSaveConfiguration);
        clickItem(buttonSaveConfiguration, "The button 'Save configuration' on Tin Can API page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getFieldEndpoint() {
        waitUntilElementIsVisible(fieldEndpoint);
        return fieldEndpoint;
    }
    public WebElement getFieldUser() {
        waitUntilElementIsVisible(fieldUser);
        return fieldUser;
    }
    public WebElement getFieldPassword() {
        waitUntilElementIsVisible(fieldPassword);
        return fieldPassword;
    }
    public void fillFieldEndpoint(String endpoint) {
        getFieldEndpoint().clear();
        fieldEndpoint.sendKeys(endpoint);
    }
    public void fillFieldUser(String username) {
        getFieldUser().clear();
        fieldUser.sendKeys(username);
    }
    public void fillFieldPassword(String password) {
        getFieldPassword().clear();
        fieldPassword.sendKeys(password);
    }

}
