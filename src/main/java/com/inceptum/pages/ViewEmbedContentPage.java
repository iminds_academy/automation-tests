package com.inceptum.pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;



    public class ViewEmbedContentPage extends BasePage {

        /* ----- FIELDS ----- */
        @FindBy(how = How.CSS, using = ".messages.messages--status")
        private WebElement message;
        @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .row.row1 h2 div")
        private WebElement fieldTitle;




    /*---------CONSTRUCTORS--------*/

        public ViewEmbedContentPage (WebDriver webDriver, PageLocation locations) throws Exception {
            super(webDriver, locations);
        }


    /*---------METHODS----------*/
    public WebElement getMessage() {
        return message;
    }

        public WebElement getFieldTitle() {
            waitUntilElementIsVisible(fieldTitle);
            return fieldTitle;
        }



}
