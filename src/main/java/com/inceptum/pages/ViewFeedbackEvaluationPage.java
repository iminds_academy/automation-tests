package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class ViewFeedbackEvaluationPage extends FeedbackEvaluationPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".form-type-select select")
    private WebElement fieldJury;
    @FindBy(how = How.CSS, using = ".group-nabc-need-criteria")
    private WebElement rowNeed;
    @FindBy(how = How.CSS, using = ".group-nabc-approach-criteria")
    private WebElement rowApproach;
    @FindBy(how = How.CSS, using = ".group-nabc-benefit-criteria")
    private WebElement rowBenefit;
    @FindBy(how = How.CSS, using = ".group-nabc-competition-criteria")
    private WebElement rowCompetition;
    @FindBy(how = How.CSS, using = ".group-team-communicate-criteria")
    private WebElement rowCommunicate;
    @FindBy(how = How.CSS, using = ".group-team-achieved-criteria")
    private WebElement rowAchieved;
    @FindBy(how = How.CSS, using = ".group-team-complementar-criteria")
    private WebElement rowComplementary;
    @FindBy(how = How.CSS, using = ".group-present-quality-criteria")
    private WebElement rowQuality;
    @FindBy(how = How.CSS, using = ".group-business-event-criteria")
    private WebElement rowExitEvent;
    @FindBy(how = How.CSS, using = ".group-business-idea-criteria")
    private WebElement rowIdea;
    @FindBy(how = How.CSS, using = ".group-business-feasibil-criteria")
    private WebElement rowFeasibility ;

    @FindBy(how = How.XPATH, using = "//div[@class='field__label'][contains(text(), 'Presentation')]/../div[2]/div")
    private WebElement fieldPresentationComment ;
    @FindBy(how = How.XPATH, using = "//div[@class='field__label'][contains(text(), 'Q&A')]/../div[2]/div")
    private WebElement fieldQAComment ;



    /* ----- CONSTRUCTORS ----- */

    public ViewFeedbackEvaluationPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */

    public WebElement getFieldJury() {
        return fieldJury;
    }
    public WebElement getRowNeed() {
        return rowNeed;
    }
    public WebElement getRowApproach() {
        return rowApproach;
    }
    public WebElement getRowBenefit() {
        return rowBenefit;
    }
    public WebElement getRowCompetition() {
        return rowCompetition;
    }
    public WebElement getRowCommunicate() {
        return rowCommunicate;
    }
    public WebElement getRowAchieved() {
        return rowAchieved;
    }
    public WebElement getRowComplementary() {
        return rowComplementary;
    }
    public WebElement getRowQuality() {
        return rowQuality;
    }
    public WebElement getRowExitEvent() {
        return rowExitEvent;
    }
    public WebElement getRowIdea() {
        return rowIdea;
    }
    public WebElement getRowFeasibility() {
        return rowFeasibility;
    }
    public WebElement getFieldPresentationComment() {
        return fieldPresentationComment;
    }
    public WebElement getFieldQAComment() {
        return fieldQAComment;
    }

}
