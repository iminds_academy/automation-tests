package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 24.11.2014.
 */
public class ViewGroupPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.CSS, using = ".field--name-group-group .group.manager")
    private WebElement fieldGroup;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Group")
    private WebElement linkGroup;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "Clone content")
    private WebElement linkCloneContent;


    /*---------CONSTRUCTORS--------*/

    public ViewGroupPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getFieldGroup() {
        return fieldGroup;
    }
    public GroupTabGroupPage clickLinkGroup() throws Exception {
        waitUntilElementIsClickable(linkGroup);
        clickItem(linkGroup, "The link 'Group' on the ViewGroup page could not be clicked.");
        waitForPageToLoad();
        return new GroupTabGroupPage(webDriver, locations);
    }
    public CloneOfGroupPage clickLinkCloneContent() throws Exception {
        waitUntilElementIsClickable(linkCloneContent);
        clickItem(linkCloneContent, "The link 'Clone content' on the ViewGroup page could not be clicked.");
        waitForPageToLoad();
        return new CloneOfGroupPage(webDriver, locations);
    }


}
