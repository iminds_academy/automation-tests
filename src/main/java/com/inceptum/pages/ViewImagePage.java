package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ViewImagePage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.CSS, using = "h2>div")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "article .node__content .image .field--name-field-image .field__items")
    private WebElement images;


    /*---------CONSTRUCTORS--------*/

    public ViewImagePage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public EditImagePage openEditImagePage() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the ViewImage page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Advanced edit")));
        clickItem(linkAdvancedEdit, "The link 'Advanced edit' on the ViewImage page could not be clicked.");
        return new EditImagePage(webDriver, locations);
    }
    public WebElement getMessage() {
        return message;
    }

    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public void checkOneItemIsDisplayed() {
        List<WebElement> imageItems = images.findElements(By.tagName("img"));
        Assert.assertEquals(1, imageItems.size());
    }
    public String getImageTitle() {
        String imageTitle = webDriver.findElement(By.cssSelector(".l-content-inner h2")).getText();
        return imageTitle;
    }

}
