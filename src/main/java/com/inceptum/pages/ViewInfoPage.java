package com.inceptum.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 24.11.2014.
 */
public class ViewInfoPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .row.row1 h2 div")
    private WebElement fieldTitle;


    /*---------CONSTRUCTORS--------*/

    public ViewInfoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }


}
