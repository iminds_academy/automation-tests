package com.inceptum.pages;


import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ViewLiveSessionPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .row.row1 h2 div")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "[class*='field-live-session-type']")
    private WebElement fieldLiveSessionType;
    @FindBy(how = How.CSS, using = "[class*='field-date-start-end'] .field__item")
    private WebElement fieldDate;
    @FindBy(how = How.CSS, using = "[class*='field-what-you-need-todo'] .field__item")
    private WebElement fieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = "[class*='field-planning'] .field__item")
    private WebElement fieldPlanning;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-course")
    private WebElement linkDownloadedCourseMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-links-course a")
    private WebElement linkExternalCourseMaterial;
    @FindBy(how = How.LINK_TEXT, using = "Revisions")
    private WebElement linkRevisions;
    @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .field--name-field-live-session-type .field-item")
    private WebElement fieldType;
    @FindBy(how = How.CSS, using = "div[class*='field-task-link'] a")
    private WebElement linkTask;


    /*---------CONSTRUCTORS--------*/

    public ViewLiveSessionPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void openEditLiveSessionPage() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the ViewLiveSession page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Advanced edit")));
        clickItem(linkAdvancedEdit, "The link 'Advanced edit' on the ViewLiveSession page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public WebElement getFieldDate() {
        waitUntilElementIsVisible(fieldDate);
        return fieldDate;
    }
    public WebElement getFieldLiveSessionType() {
        return fieldLiveSessionType;
    }
    public WebElement getFieldWhatYouNeedToDo() {
        return fieldWhatYouNeedToDo;
    }
    public WebElement getFieldPlanning() {
        return fieldPlanning;
    }
    public WebElement getLinkRevisions() {
        waitUntilElementIsClickable(linkRevisions);
        return linkRevisions;
    }
    public WebElement getFieldType() {
        waitUntilElementIsVisible(fieldType);
        return fieldType;
    }
    public WebElement getLinkTask() {
        waitUntilElementIsVisible(linkTask);
        return linkTask;
    }
    public void assertTaskLinkIsShown() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("field--name-field-task-link")));
        int i = webDriver.findElements(By.className("field--name-field-task-link")).size();
        Assert.assertTrue(i==1);
    }
    public void assertTaskLinkIsNotShown() {
        int i = webDriver.findElements(By.className("field--name-field-task-link")).size();
        Assert.assertTrue(i==0);
    }
    public String getDownloadsIdCourseMaterial() {
        waitUntilElementIsVisible(linkDownloadedCourseMaterial);
        String downloadsId = linkDownloadedCourseMaterial.getAttribute("href");
        return downloadsId;
    }
    public WebElement getLinkExternalCourseMaterial() {
        return linkExternalCourseMaterial;
    }
    public WebElement getLinkDownloadedCourseMaterial()  {
        waitUntilElementIsVisible(linkDownloadedCourseMaterial);
        return linkDownloadedCourseMaterial;
    }
    public void clickLinkDownloadedCourseMaterial() throws Exception {
        waitUntilElementIsClickable(linkDownloadedCourseMaterial);
        clickItem(linkDownloadedCourseMaterial, "The link of downloaded course material could not be clicked.");
    }


}
