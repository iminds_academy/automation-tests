package com.inceptum.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 24.11.2014.
 */
public class ViewMediaPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .row.row1 h2 div")
    private WebElement fieldTitle;
    @FindBy(how = How.LINK_TEXT, using = "Info")
    private WebElement linkInfo;
    @FindBy(how = How.LINK_TEXT, using = "Links")
    private WebElement linkLinks;
    @FindBy(how = How.CSS, using = ".field__items .title")
    private WebElement fieldMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-course")
    private WebElement linkDownloadedCourseMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-links-additional a")
    private WebElement linkExternalAdditionalMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-links-course a")
    private WebElement linkExternalCourseMaterial;
    @FindBy(how = How.ID, using = "modal-content")
    private WebElement presentationPopup;
    @FindBy(how = How.CSS, using = "[class*='field--name-field-length']")
    private WebElement fieldLength;
    @FindBy(how = How.CSS, using = "[class*='field-date-start-end'] .field__item")
    private WebElement fieldDate;
    @FindBy(how = How.CSS, using = "[class*='field-video-summary'] .field__item")
    private WebElement fieldVideoSummary;
    @FindBy(how = How.CSS, using = "[class*='field-context-info'] .field__item")
    private WebElement fieldContextualInformation;
    @FindBy(how = How.CSS, using = ".parent+h2")
    private WebElement fieldPresentationPopupName;
    @FindBy(how = How.CSS, using = ".field__items iframe")
    private WebElement iframePopup;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "close")
    private WebElement linkClose;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[1]/div[1]//*[@class='prefix']")
    private WebElement fieldDatePrefix;


    /*---------CONSTRUCTORS--------*/

    public ViewMediaPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public WebElement getFieldDate() {
        return fieldDate;
    }
    public WebElement getLinkInfo() {
        return linkInfo;
    }
    public void clickLinkInfo() throws Exception {
        waitUntilElementIsClickable(linkInfo);
        clickItem(linkInfo, "The link 'Info' could not be clicked.");
    }
    public WebElement getLinkLinks() {
        return linkLinks;
    }
    public void clickLinkLinks() throws Exception {
        waitUntilElementIsClickable(linkLinks);
        clickItem(linkLinks, "The link 'Links' on ViewMedia page could not be clicked.");
        waitUntilElementIsVisible(getLinkClose());
    }
    public void clickLinkAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(linkLinks);
        clickItem(linkLinks, "The 'Links' link on ViewMedia page could not be clicked.");
        waitUntilElementIsClickable(fieldMaterial);
        clickItem(fieldMaterial, "The course material link on ViewMedia page could not be clicked.");
    }
    public void assertPresentationIsOpened() {
        waitUntilElementIsVisible(presentationPopup);
    }
    public String getPresentationPopupName() {
        waitUntilElementIsVisible(fieldPresentationPopupName);
        String presentationPopupName = fieldPresentationPopupName.getText();
        return presentationPopupName;
    }
    public WebElement getIframePopup() {
        return iframePopup;
    }
    public String getDownloadsIdCourseMaterial() throws Exception {
        waitUntilElementIsClickable(linkLinks);
        clickItem(linkLinks, "The 'Links' link on ViewMedia page could not be clicked.");
        waitUntilElementIsVisible(linkDownloadedCourseMaterial);
        String downloadsId = linkDownloadedCourseMaterial.getAttribute("href");
        return downloadsId;
    }
    public WebElement getLinkDownloadedCourseMaterial()  {
        return linkDownloadedCourseMaterial;
    }
    public void clickLinkDownloadedCourseMaterial() throws Exception {
        waitUntilElementIsClickable(linkDownloadedCourseMaterial);
        clickItem(linkDownloadedCourseMaterial, "The link of downloaded course material could not be clicked.");
    }
    public void visitLinkExternalAdditionalMaterial() throws Exception { // opens the link at the same browser tab
        waitUntilElementIsClickable(linkLinks);
        clickItem(linkLinks, "The 'Links' link on ViewMedia page could not be clicked.");
        waitUntilElementIsClickable(linkExternalAdditionalMaterial);
        clickItem(linkExternalAdditionalMaterial, "The external link (additional material) could not be clicked.");
    }
    public WebElement getLinkExternalCourseMaterial() {
        waitUntilElementIsVisible(linkExternalCourseMaterial);
        return linkExternalCourseMaterial;
    }
    public WebElement getLinkClose() {
        return linkClose;
    }
    public void clickLinkClose() throws Exception {
        waitUntilElementIsClickable(linkClose);
        clickItem(linkClose, "The link 'close' could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.partialLinkText("close")));
    }
    public void clickLinkAdditionalMaterialByName(String title) throws Exception {
        String xpathLink = "//*[@id='mobileMenu']/div[1]/div[1]//div[contains(@class, 'type-link-field')]" +
                "//a[text()='" + title + "']";
        WebElement link = webDriver.findElement(By.xpath(xpathLink));
        waitUntilElementIsClickable(link);
        clickItem(link, "The link from Additional material could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getFieldLength() {
        return fieldLength;
    }
    public WebElement getFieldVideoSummary() {
        return fieldVideoSummary;
    }
    public WebElement getFieldContextualInformation() {
        waitUntilElementIsVisible(fieldContextualInformation);
        return fieldContextualInformation;
    }
    public String getPrefix() {
        return fieldDatePrefix.getText();
    }

}
