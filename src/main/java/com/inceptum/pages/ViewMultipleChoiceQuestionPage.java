package com.inceptum.pages;


import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ViewMultipleChoiceQuestionPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.XPATH, using = "//table[2]/tbody/*/td[1]/span")
    private WebElement answerList;
    @FindBy(how = How.CSS, using = ".field__items p")
    private WebElement questionField;


    /*---------CONSTRUCTORS--------*/

    public ViewMultipleChoiceQuestionPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void openEditImagePage() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the ViewMultipleChoiceQuestion page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Advanced edit")));
        clickItem(linkAdvancedEdit, "The link 'Advanced edit' on the ViewMultipleChoiceQuestion page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public void checkNumberOfCorrectAnswers(int number) {
        waitUntilElementIsVisible(answerList);
        List<WebElement> correctAnswers = answerList.findElements(By.xpath("//*[@class='multichoice-icon correct']"));
        int numberOfCorrectAnswers = correctAnswers.size();
        Assert.assertTrue(numberOfCorrectAnswers == number);
    }
    public String getQuestionTitle() {
        waitUntilElementIsVisible(questionField);
        String questionTitleTrimmed = questionField.getText();
        String questionTitle = "Multiple choice question: " + questionTitleTrimmed;
        return questionTitle;
    }


}
