package com.inceptum.pages;


import java.io.File;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class ViewPeerAssignmentPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.LINK_TEXT, using = "Start your assignment")
    private WebElement buttonStartYourAssignment;
    @FindBy(how = How.CSS, using = ".peertask-workflow-item.item.active.current span.above")
    private WebElement tabActive;
    @FindBy(how = How.CSS, using = ".commentbox")
    private WebElement comment;
    @FindBy(how = How.XPATH, using = "//span[@class='above'][text()='1']")
    private WebElement tabStep1;
    @FindBy(how = How.XPATH, using = "//span[@class='above'][text()='2']")
    private WebElement tabStep2;
    @FindBy(how = How.XPATH, using = "//span[@class='above'][text()='3']")
    private WebElement tabStep3;
    @FindBy(how = How.XPATH, using = "//*[@class='peas-contents']/div[1]")
    private WebElement fieldYourPersonalAssignment;
    @FindBy(how = How.LINK_TEXT, using = "Fill out the solution template") // ?????????? doesn't exist now
    private WebElement buttonFillOutTheSolutionTemplate;
    @FindBy(how = How.CSS, using = "[class*='draft'] .form-draft-submission a")
    private WebElement buttonCreateDraftSubmission;
    @FindBy(how = How.CSS, using = "[class*='final'] .form-draft-submission a")
    private WebElement buttonCreateFinalSubmission;
    @FindBy(how = How.ID, using = "edit-file")
    private WebElement fieldUploadDraft;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSubmitDraft;
    @FindBy(how = How.XPATH, using = "//a[text()='View submission']")
    private WebElement buttonViewSubmission;
    @FindBy(how = How.XPATH, using = "//*[@class='title'][contains(., 'Your draft solution')]/../div[2][@class='subtitle']")
    private WebElement fieldDraftComment;
    @FindBy(how = How.XPATH, using = "//*[@class='title'][contains(., 'draft solution')]/../div[2][@class='subtitle']")
    private WebElement fieldReviewComment;
    @FindBy(how = How.XPATH, using = "//*[@class='title'][contains(., 'Your final solution')]/../div[2][@class='subtitle']")
    private WebElement fieldFinalComment;
    @FindBy(how = How.CSS, using = ".form-draft-submission .title")
    private WebElement fieldCheckSubmittedDraft;
    @FindBy(how = How.XPATH, using = "//*[@id='peertask-peer-review-form']/div/div[@class='criterion']")
    private WebElement fieldEvaluationCriteria;
    @FindBy(how = How.XPATH, using = "//h2[text()='Evaluation criteria']/../div/a")
    private WebElement linkEvaluationCriteria;
    @FindBy(how = How.CSS, using = "#peertask-peer-review-form textarea")
    private WebElement fieldComments;
    @FindBy(how = How.CSS, using = ".review-feedback")
    private WebElement fieldUsefulnessOfComment;
    @FindBy(how = How.XPATH, using = "//*[@class='entity entity-field-collection-item field-collection-item-field-reviews']/div[3]/div/div")
    private WebElement fieldPeersComment;
    @FindBy(how = How.ID, using = "edit-submit")
    private WebElement buttonSaveYourComments;
    @FindBy(how = How.CSS, using = "#edit-submit[value='Update your comments']")
    private WebElement buttonUpdateYourComments;
    @FindBy(how = How.XPATH, using = "//label[contains(., 'yes')]")
    private WebElement radiobuttonYes;
    @FindBy(how = How.XPATH, using = "//label[contains(., 'no')]")
    private WebElement radiobuttonNo;
    @FindBy(how = How.XPATH, using = "//a[text()='Edit your solution']")
    private WebElement buttonEditYourSolution;
    @FindBy(how = How.CSS, using = ".node--presentation .title")
    private WebElement fieldPresentationResource;
    @FindBy(how = How.CSS, using = ".node--video .title")
    private WebElement fieldVideoResource;
    @FindBy(how = How.CSS, using = ".node--image .title")
    private WebElement fieldImageResource;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-course .field-item")
    private WebElement fieldUploadedFileResource;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-peertask .field-item")
    private WebElement fieldUploadedFileAdditional;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-course")
    private WebElement fieldFileDownloads;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-peertask")
    private WebElement fieldFileDownloadsAdditional;
    @FindBy(how = How.CSS, using = ".field--name-field-links-course .field__item a")
    private WebElement fieldLinkResource;
    @FindBy(how = How.CSS, using = ".field--name-field-links-peertask .field__item a")
    private WebElement fieldLinkAdditional;
    @FindBy(how = How.CSS, using = "#modal-content")
    private WebElement popupEvaluationCriteria;
    @FindBy(how = How.CSS, using = ".modal-header a.close")
    private WebElement linkClosePopup;
    @FindBy(how = How.CSS, using = ".field--name-field-cases p")
    private WebElement fieldAssignmentCase;
    @FindBy(how = How.CSS, using = ".submitted div")
    private WebElement fieldPeerToReview;
    @FindBy(how = How.CSS, using = "a.peertask-review-tab")
    private WebElement tabPeerToReview;
    @FindBy(how = How.CSS, using = ".form-draft-submission a")
    private WebElement linkViewSubmission;
    @FindBy(how = How.CSS, using = "#edit-input-usefulness [for*='1']")
    private WebElement usefulYes;
    @FindBy(how = How.CSS, using = "#edit-input-usefulness [for*='0']")
    private WebElement usefulNo;
    @FindBy(how = How.ID, using = "edit-status")
    private WebElement fieldStatus;


    /*---------CONSTRUCTORS--------*/

    public ViewPeerAssignmentPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getButtonStartYourAssignment() {
        return buttonStartYourAssignment;
    }
    public void clickButtonStartYourAssignment() throws Exception {
        waitUntilElementIsClickable(buttonStartYourAssignment);
        clickItem(buttonStartYourAssignment, "The button 'Start your assignment' on the ViewPeerAssignment page could not be clicked.");
    }
    public void checkActiveTab(String step) {
        waitUntilElementIsVisible(tabActive);
        Assert.assertTrue("Tab " + step + " is not active.", tabActive.getText().equals(step));
    }
    public WebElement getComment() {
        waitUntilElementIsVisible(comment);
        return comment;
    }
    public WebElement getTabStep1() {
        return tabStep1;
    }
    public void switchToStep1() throws Exception {
        waitUntilElementIsClickable(tabStep1);
        clickItem(tabStep1, "The tab 'Step1' on the ViewPeerAssignment page could not be clicked.");
        waitForPageToLoad();
    }
    public void switchToStep2() throws Exception {
        waitUntilElementIsClickable(tabStep2);
        clickItem(tabStep2, "The tab 'Step2' on the ViewPeerAssignment page could not be clicked.");
        waitForPageToLoad();
    }
    public void switchToStep3() throws Exception {
        waitUntilElementIsClickable(tabStep3);
        clickItem(tabStep3, "The tab 'Step3' on the ViewPeerAssignment page could not be clicked.");
        waitForPageToLoad();
    }
    public void checkAssignmentCase(String caseExpected) {
        waitUntilElementIsVisible(fieldYourPersonalAssignment);
        String caseActual = fieldYourPersonalAssignment.getText();
        Assert.assertTrue("Assignment case " + caseActual + " is not as expected: " + caseExpected + ".",
                caseActual.equals(caseExpected));
    }
    public void clickButtonFillOutTheSolutionTemplate() throws Exception {
        waitUntilElementIsClickable(buttonFillOutTheSolutionTemplate);
        clickItem(buttonFillOutTheSolutionTemplate, "The button 'Fill out the solution template' on the ViewPeerAssignment page could not be clicked.");
    }
    public void clickButtonCreateDraftSubmission() throws Exception {
        waitUntilElementIsClickable(buttonCreateDraftSubmission);
        clickItem(buttonCreateDraftSubmission, "The button 'Create draft submission' on the ViewPeerAssignment page could not be clicked.");
    }
    public void clickButtonCreateFinalSubmission() throws Exception {
        waitUntilElementIsClickable(buttonCreateFinalSubmission);
        clickItem(buttonCreateFinalSubmission, "The button 'Create final submission' on the ViewPeerAssignment page could not be clicked.");
    }
    public void clickButtonSubmitDraft() throws Exception {
        waitUntilElementIsClickable(buttonSubmitDraft);
        clickItem(buttonSubmitDraft, "The button 'Submit' on the ViewPeerAssignment page could not be clicked.");
    }
    public void uploadYourDraft(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        waitUntilElementIsClickable(fieldUploadDraft);
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") ||
                PropertyLoader.loadProperty("browser.platform").equals("any")) { // Exception for Linux OS/Mavericks: enter a correct file path
            String filePathLinux = path.replace("D:", "");
            fieldUploadDraft.sendKeys(filePathLinux);
        } else fieldUploadDraft.sendKeys(path);
        clickButtonSubmitDraft();
        waitUntilElementIsVisible(buttonViewSubmission);
    }
    public void downloadTemplate(String nameTemplate) {
        String linkXpath = "//a/div/div[text()='" + nameTemplate + "']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(linkXpath)));
        webDriver.findElement(By.xpath(linkXpath)).click();
    }
    public WebElement getFieldDraftComment() {
        waitUntilElementIsVisible(fieldDraftComment);
        return fieldDraftComment;
    }
    public WebElement getFieldReviewComment() {
        waitUntilElementIsVisible(fieldReviewComment);
        return fieldReviewComment;
    }
    public WebElement getFieldFinalComment() {
        waitUntilElementIsVisible(fieldFinalComment);
        return fieldFinalComment;
    }
    public String getUsernameForReviewing() {
        waitUntilElementIsVisible(fieldCheckSubmittedDraft);
        String text = fieldCheckSubmittedDraft.getText();
        String[] parts = text.split(" - draft"); // Separate username
        String username = parts[0];
        String usernameTrimmed = username.trim();
        System.out.println("Current user is reviewing the work of user: " + usernameTrimmed);
        return usernameTrimmed;
    }
    public void clickButtonViewSubmission() throws Exception {
        waitUntilElementIsClickable(buttonViewSubmission);
        clickItem(buttonViewSubmission, "The button 'View submission' on the ViewPeerAssignment page could not be clicked.");
    }
    public WebElement getButtonViewSubmission() {
        return buttonViewSubmission;
    }
    public void checkEvaluationCriteria(String nameExpected, String descriptionExpected) {
        waitUntilElementIsVisible(fieldEvaluationCriteria);
        String nameActual = fieldEvaluationCriteria.findElement(By.xpath("./span[1]")).getText();
        String descriptionActual = fieldEvaluationCriteria.findElement(By.xpath("./span[2]")).getText();
        Assert.assertTrue("Name of Evaluation criteria is shown incorrectly: " + nameActual, nameActual.equals(nameExpected));
        Assert.assertTrue("Description of Evaluation criteria is shown incorrectly: " + descriptionActual,
                descriptionActual.equals(descriptionExpected));
    }
    public void clickLinkEvaluationCiteria() throws Exception {
        waitUntilElementIsClickable(linkEvaluationCriteria);
        clickItem(linkEvaluationCriteria, "The link 'Evaluation criteria' on the ViewPeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(popupEvaluationCriteria);
    }
    public void fillFieldComments(String comment) throws Exception {
        waitUntilElementIsVisible(fieldComments);
        fieldComments.sendKeys(comment);
    }
    public void submitComment() throws Exception{
        waitUntilElementIsClickable(buttonSaveYourComments);
        clickItem(buttonSaveYourComments, "The button 'Save your comments' on the ViewPeerAssignment page could not be clicked.");
        waitUntilElementIsVisible(buttonUpdateYourComments);
        assertTextOfMessage(message, ".*Your comments have been stored");
    }
    public void editComment(String commentFeedbackUpdated) throws Exception {
        webDriver.navigate().refresh(); // Refresh the page to remove info message from previous step
        Thread.sleep(500);
        fieldComments.clear();
        fieldComments.sendKeys(commentFeedbackUpdated);
        waitUntilElementIsClickable(buttonUpdateYourComments);
        clickItem(buttonUpdateYourComments, "The button 'Update your comments' on the ViewPeerAssignment page could not be clicked.");
        assertTextOfMessage(message, ".*Your comments have been stored");
    }
    public void checkCommentsOnStep3() {
        waitUntilElementIsVisible(comment);
        String commentReadActual = webDriver.findElement(By.xpath("//*[@class='peas-contents']/div[1]")).getText();
        String commentFinalVersionActual = webDriver.findElement(By.xpath("//*[@class='peas-contents']/div[2]")).getText();
        Assert.assertTrue("'Read the comments of your peers' message is incorrect: " + commentReadActual,
                commentReadActual.matches("(?s).*Review comments will be published after the review deadline.*"));
        Assert.assertTrue("'Submit your final version' message is incorrect: " + commentFinalVersionActual,
                commentFinalVersionActual.matches("(?s).*You can upload the final version of your assignment after the review deadline.*"));
    }
    public WebElement getFieldUsefulnessOfComment() {
        waitUntilElementIsVisible(fieldUsefulnessOfComment);
        return fieldUsefulnessOfComment;
    }
    public WebElement getFieldPeersComment() {
        waitUntilElementIsVisible(fieldPeersComment);
        return fieldPeersComment;
    }
    public void selectUsefulYes() throws Exception {
        waitUntilElementIsVisible(radiobuttonYes);
        clickItem(radiobuttonYes, "The radiobutton 'Yes' on the ViewPeerAssignment page could not be clicked.");
        waitUntilActionWithThrobberDone();
    }
    public void selectUsefulNo() throws Exception {
        waitUntilElementIsClickable(radiobuttonNo);
        clickItem(radiobuttonNo, "The radiobutton 'No' on the ViewPeerAssignment page could not be clicked.");
        waitUntilActionWithThrobberDone();
    }
    public void clickButtonEditYourSolution() throws Exception {
        waitUntilElementIsClickable(buttonEditYourSolution);
        clickItem(buttonEditYourSolution, "The button 'Edit your solution' on the ViewPeerAssignment page could not be clicked.");
    }
    public WebElement getFieldPresentationResource() {
        return fieldPresentationResource;
    }
    public WebElement getFieldVideoResource() {
        return fieldVideoResource;
    }
    public WebElement getFieldImageResource() {
        return fieldImageResource;
    }
    public WebElement getFieldUploadedFileResource() {
        return fieldUploadedFileResource;
    }
    public WebElement getFieldLinkResource() {
        return fieldLinkResource;
    }
    public WebElement getFieldLinkAdditional() {
        return fieldLinkAdditional;
    }
    public void clickLinkClosePopup() throws Exception {
        waitUntilElementIsClickable(linkClosePopup);
        clickItem(linkClosePopup, "The link 'Close' (popup) on the ViewPeerAssignment page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modal-content"))); // popup is closed
        waitUntilElementIsClickable(linkEvaluationCriteria);
    }
    public String getCaseText() {
        waitUntilElementIsVisible(fieldAssignmentCase);
        String caseText = fieldAssignmentCase.getText();
        return caseText;
    }
    public String getDownloadsIdTaskResources() {
        waitUntilElementIsVisible(fieldFileDownloads);
        String downloadsId = fieldFileDownloads.getAttribute("href");
        return downloadsId;
    }
    public String getDownloadsIdAdditionalResources() {
        waitUntilElementIsVisible(fieldFileDownloadsAdditional);
        String downloadsId = fieldFileDownloadsAdditional.getAttribute("href");
        return downloadsId;
    }
    public void clickFileDownloads() throws Exception {
        Thread.sleep(1000); // for correct generating of statements
        waitUntilElementIsClickable(fieldUploadedFileResource);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, fieldUploadedFileResource);
    }
    public void clickFileDownloadsAdditional() throws Exception {
        Thread.sleep(1000); // for correct generating of statements
        waitUntilElementIsClickable(fieldUploadedFileAdditional);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, fieldUploadedFileAdditional);
    }
    public WebElement getFieldPeerToReview() {
        waitUntilElementIsVisible(fieldPeerToReview);
        return fieldPeerToReview;
    }
    public String getReviewId() {
        waitUntilElementIsVisible(tabPeerToReview);
        String reviewId = tabPeerToReview.getAttribute("href");
        return reviewId;
    }
    public String getPeerSubmissionId() { // !!!!!!!!!!!! (the person id instead of the object id)
        waitUntilElementIsVisible(tabPeerToReview);
        String reviewId = tabPeerToReview.getAttribute("href");
        String reviewId2 = reviewId.replace("/review", "");
        String peerSubmissionId = reviewId2.concat("/draft-solution");
        return peerSubmissionId;
    }
    public void clickLinkViewSubmission() throws Exception {
        waitUntilElementIsClickable(linkViewSubmission);
        clickItem(linkViewSubmission, "The link 'View submission' on the ViewPeerAssignment page could not be clicked.");
    }
    public void clickCommentUsefulYes() throws Exception {
        waitUntilElementIsClickable(usefulYes);
        clickItem(usefulYes, "The link 'Yes' on the ViewPeerAssignment page could not be clicked.");
        waitUntilActionWithThrobberDone();
    }
    public void clickCommentUsefulNo() throws Exception {
        waitUntilElementIsClickable(usefulNo);
        clickItem(usefulNo, "The link 'No' on the ViewPeerAssignment page could not be clicked.");
        waitUntilActionWithThrobberDone();
    }
    public WebElement getFieldStatus() {
        waitUntilElementIsVisible(fieldStatus);
        return fieldStatus;
    }



}
