package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ViewPresentationPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.CSS, using = "h2>div")
    private WebElement fieldTitle;


    /*---------CONSTRUCTORS--------*/

    public ViewPresentationPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void openEditPresentationPage() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the ViewPresentation page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Advanced edit")));
        clickItem(linkAdvancedEdit, "The link 'Advanced edit' on the ViewPresentation page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public String getPresentationTitle() {
        String presentationTitle = webDriver.findElement(By.cssSelector(".l-content-inner h2")).getText();
        return presentationTitle;
    }

}
