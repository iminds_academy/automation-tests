package com.inceptum.pages;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class ViewQuizPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.ID, using = "edit-button")
    private WebElement buttonStartQuiz;


    /*---------CONSTRUCTORS--------*/

    public ViewQuizPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTabView() {
        return tabView;
    }
    public TakeQuizPage clickButtonStartQuiz() throws Exception {
        waitUntilElementIsClickable(buttonStartQuiz);
        clickItem(buttonStartQuiz, "The button 'Start quiz' on ViewQuiz page could not be clicked.");
        waitForPageToLoad();
        return new TakeQuizPage(webDriver, locations);
    }


}
