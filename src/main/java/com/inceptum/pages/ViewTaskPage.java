package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ViewTaskPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .row.row1 h2 div")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "[class*='field-date-start-end'] .field__item")
    private WebElement fieldDate;
    @FindBy(how = How.CSS, using = "[class*='field--name-field-length']")
    private WebElement fieldLength;
    @FindBy(how = How.CSS, using = "[class*='field-what-you-need-todo'] .field__item")
    private WebElement fieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = "[class*='field-whats-this-for'] .field__item")
    private WebElement fieldWhatThisFor;
    @FindBy(how = How.CSS, using = "[class*='field-goal'] .field__item")
    private WebElement fieldLearningObjectives;
    @FindBy(how = How.LINK_TEXT, using = "Revisions")
    private WebElement linkRevisions;
    @FindBy(how = How.CSS, using = ".title>div[data-quickedit-field-id]")
    private WebElement fieldResource;
    @FindBy(how = How.CSS, using = ".field__items .title")
    private WebElement fieldMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-course")
    private WebElement linkDownloadedTaskResources;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-additional")
    private WebElement linkDownloadedAdditionalMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-links-course a")
    private WebElement linkExternalTaskResources;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[1]/div[1]//*[@class='prefix']")
    private WebElement fieldDatePrefix;


    /*---------CONSTRUCTORS--------*/

    public ViewTaskPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void openEditTaskPage() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the ViewTask page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Advanced edit")));
        clickItem(linkAdvancedEdit, "The link 'Advanced edit' on the ViewTask page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getFieldTitle() {
        waitUntilElementIsVisible(fieldTitle);
        return fieldTitle;
    }
    public WebElement getLinkRevisions() {
        return linkRevisions;
    }
    public WebElement getFieldResource() {
        return fieldResource;
    }
    public MediaPopupPage clickLinkAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(fieldMaterial);
        clickItem(fieldMaterial, "The link of additional material could not be clicked.");
        return new MediaPopupPage(webDriver, locations);
    }
    public String getDownloadsIdAdditionalMaterial() {
        waitUntilElementIsVisible(linkDownloadedAdditionalMaterial);
        String downloadsId = linkDownloadedAdditionalMaterial.getAttribute("href");
        return downloadsId;
    }
    public WebElement getLinkDownloadedAdditionalMaterial()  {
        return linkDownloadedAdditionalMaterial;
    }
    public void clickLinkDownloadedAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(linkDownloadedAdditionalMaterial);
        clickItem(linkDownloadedAdditionalMaterial, "The link of downloaded additional material could not be clicked.");
    }
    public MediaPopupPage clickLinkMaterial() throws Exception {
        waitUntilElementIsClickable(fieldMaterial);
        clickItem(fieldMaterial, "The link of task resource could not be clicked.");
        return new MediaPopupPage(webDriver, locations);
    }
    public WebElement getLinkExternalTaskResources() {
        return linkExternalTaskResources;
    }
    public WebElement getLinkDownloadedTaskResources()  {
        return linkDownloadedTaskResources;
    }
    public void clickLinkDownloadedTaskResources() throws Exception {
        waitUntilElementIsClickable(linkDownloadedTaskResources);
        clickItem(linkDownloadedTaskResources, "The link of downloaded task resource could not be clicked.");
    }
    public WebElement getFieldLength() {
        return fieldLength;
    }
    public WebElement getFieldWhatYouNeedToDo() {
        return fieldWhatYouNeedToDo;
    }
    public WebElement getFieldWhatThisFor() {
        return fieldWhatThisFor;
    }
    public WebElement getFieldLearningObjectives() {
        return fieldLearningObjectives;
    }
    public String getPrefix() {
        return fieldDatePrefix.getText();
    }
    public WebElement getFieldDate() {
        return fieldDate;
    }

}
