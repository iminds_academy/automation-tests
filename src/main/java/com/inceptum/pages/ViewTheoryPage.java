package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ViewTheoryPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Edit")
    private WebElement linkEdit;
    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.PARTIAL_LINK_TEXT, using = "View")
    private WebElement tabView;
    @FindBy(how = How.CSS, using = ".l-page-container:first-of-type .row.row1 h2 div")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "[class*='field-date-start-end'] .field__item")
    private WebElement fieldDate;
    @FindBy(how = How.LINK_TEXT, using = "Revisions")
    private WebElement linkRevisions;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-additional")
    private WebElement linkDownloadedAdditionalMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-download-title-course")
    private WebElement linkDownloadedCourseMaterial;
    @FindBy(how = How.CSS, using = ".field--name-field-links-course a")
    private WebElement linkExternalCourseMaterial;
    @FindBy(how = How.ID, using = "modal-content")
    private WebElement popup;
    @FindBy(how = How.CSS, using = ".parent+h2")
    private WebElement fieldPresentationPopupName;
    @FindBy(how = How.CSS, using = "[class*='field--name-field-length']")
    private WebElement fieldLength;
    @FindBy(how = How.CSS, using = "[class*='field-what-you-need-todo'] .field__item")
    private WebElement fieldWhatYouNeedToDo;
    @FindBy(how = How.CSS, using = "[class*='field-whats-this-for'] .field__item")
    private WebElement fieldWhatThisFor;
    @FindBy(how = How.CSS, using = "[class*='field-goal'] .field__item")
    private WebElement fieldLearningObjectives;
    @FindBy(how = How.XPATH, using = "//*[@id='mobileMenu']/div[1]/div[1]//*[@class='prefix']")
    private WebElement fieldDatePrefix;


    /*---------CONSTRUCTORS--------*/

    public ViewTheoryPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void openEditTheoryPage() throws Exception {
        waitUntilElementIsClickable(linkEdit);
        clickItem(linkEdit, "The link 'Edit' on the ViewTheory page could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Advanced edit")));
        clickItem(linkAdvancedEdit, "The link 'Advanced edit' on the ViewTheory page could not be clicked.");
    }
    public WebElement getMessage() {
        return message;
    }
    public WebElement getTabView() {
        return tabView;
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public WebElement getFieldDate() {
        return fieldDate;
    }
    public WebElement getLinkRevisions() {
        return linkRevisions;
    }
    public MediaPopupPage clickLinkMaterial() throws Exception {
        waitUntilElementIsClickable(getFieldMaterial());
        clickItem(getFieldMaterial(), "The link of course material could not be clicked.");
        return new MediaPopupPage(webDriver, locations);
    }
    public void assertPopupIsOpened() {
        waitUntilElementIsVisible(popup);
    }
    public String getPresentationPopupName() {
        waitUntilElementIsVisible(fieldPresentationPopupName);
        String presentationPopupName = fieldPresentationPopupName.getText();
        return presentationPopupName;
    }
    public String getDownloadsIdAdditionalMaterial() {
        waitUntilElementIsVisible(linkDownloadedAdditionalMaterial);
        String downloadsId = linkDownloadedAdditionalMaterial.getAttribute("href");
        return downloadsId;
    }
    public WebElement getLinkDownloadedAdditionalMaterial()  {
        return linkDownloadedAdditionalMaterial;
    }
    public void clickLinkDownloadedAdditionalMaterial() throws Exception {
        waitUntilElementIsClickable(linkDownloadedAdditionalMaterial);
        clickItem(linkDownloadedAdditionalMaterial, "The link of downloaded additional material could not be clicked.");
    }
    public WebElement getFieldLength() {
        return fieldLength;
    }
    public WebElement getFieldWhatYouNeedToDo() {
        return fieldWhatYouNeedToDo;
    }
    public WebElement getFieldWhatThisFor() {
        return fieldWhatThisFor;
    }
    public WebElement getFieldLearningObjectives() {
        return fieldLearningObjectives;
    }
    public String getPrefix() {
        return fieldDatePrefix.getText();
    }
    public WebElement getLinkExternalCourseMaterial() {
        return linkExternalCourseMaterial;
    }
    public WebElement getLinkDownloadedCourseMaterial()  {
        return linkDownloadedCourseMaterial;
    }
    public void clickLinkDownloadedCourseMaterial() throws Exception {
        waitUntilElementIsClickable(linkDownloadedCourseMaterial);
        clickItem(linkDownloadedCourseMaterial, "The link of downloaded course material could not be clicked.");
    }

}
