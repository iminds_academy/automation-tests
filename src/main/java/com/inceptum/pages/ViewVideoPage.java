package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class ViewVideoPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.LINK_TEXT, using = "Advanced edit")
    private WebElement linkAdvancedEdit;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    @FindBy(how = How.CSS, using = "h2>div")
    private WebElement fieldTitle;


    /*---------CONSTRUCTORS--------*/

    public ViewVideoPage (WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getMessage() {
        return message;
    }
    public WebElement getFieldTitle() {
        return fieldTitle;
    }
    public String getVideoTitle() {
        String videoTitle = webDriver.findElement(By.cssSelector(".l-content-inner h2")).getText();
        return videoTitle;
    }

}
