package com.inceptum.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

import java.util.ArrayList;
import java.util.List;

public class WhoIsWhoPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Administrator')]")
    private WebElement tabAdministrator;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Admins')]")
    private WebElement tabAdmins;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Class manager')]")
    private WebElement tabClassManager;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Master editor')]")
    private WebElement tabMasterEditor;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Juries')]")
    private WebElement tabJuries;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Teachers')]")
    private WebElement tabTeachers;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Authenticated user')]")
    private WebElement tabAuthenticatedUser;
    @FindBy(how = How.XPATH, using = "//label[contains(text(), 'Students')]")
    private WebElement tabStudents;
    @FindBy(how = How.CSS, using = "h2 select")
    private WebElement fieldClass;
    @FindBy(how = How.ID, using = "edit-status")
    private WebElement fieldStatus;
    @FindBy(how = How.ID, using = "edit-operation")
    private WebElement fieldOperation;
    @FindBy(how = How.ID, using = "edit-submit--2")
    private WebElement buttonExecute;
    @FindBy(how = How.XPATH, using = "//*[@class='create-user']/a[contains(text(), 'Add user')]")
    private WebElement buttonAddUser;
    @FindBy(how = How.XPATH, using = "//*[@class='create-user']/a[contains(text(), 'Create user')]")
    private WebElement buttonCreateUser;
    @FindBy(how = How.CSS, using = ".messages.messages--status")
    private WebElement message;
    private final static String xPathOFThrobber="//div[@class='ajax-progress ajax-progress-throbber']/div[@class='throbber']";

    /* ----- CONSTRUCTORS ----- */

    public WhoIsWhoPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /* ----- METHODS ----- */
    // Select "All users" on "Who's who page"
    public void switchToTabAdministrator() throws Exception {
        waitUntilElementIsClickable(tabAdministrator);
        clickItem(tabAdministrator, "The tab 'Administrator' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabAdminisInCertainClass() throws Exception {
        waitUntilElementIsClickable(tabAdmins);
        clickItem(tabAdmins, "The tab 'Administrator' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabAuthenticatedUser() throws Exception {
        waitUntilElementIsClickable(tabAuthenticatedUser);
        clickItem(tabAuthenticatedUser, "The tab 'Authenticated user' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabClassManager() throws Exception {
        waitUntilElementIsClickable(tabClassManager);
        clickItem(tabClassManager, "The tab 'Class manager' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabMasterEditor() throws Exception {
        waitUntilElementIsClickable(tabMasterEditor);
        clickItem(tabMasterEditor, "The tab 'Master editor' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void waitUntilTableIsRefreshed() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".throbber")));
        //wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".throbber")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPathOFThrobber)));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xPathOFThrobber)));

    }
    public void switchToTabJuries() throws Exception { // grid view
        waitUntilElementIsClickable(tabJuries);
        clickItem(tabJuries, "The tab 'Juries' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabTeachers() throws Exception { // grid view
        waitUntilElementIsClickable(tabTeachers);
        clickItem(tabTeachers, "The tab 'Teachers' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabTeachersListView() throws Exception { // list view
        waitUntilElementIsClickable(tabTeachers);
        clickItem(tabTeachers, "The tab 'Teachers' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void switchToTabStudentsListView() throws Exception { // list view
        waitUntilElementIsClickable(tabStudents);
        clickItem(tabStudents, "The tab 'Students' on WhoIsWho page could not be clicked.");
        waitUntilTableIsRefreshed();
    }
    public void waitUntilFirstTabTableOpens() { // list view
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("form[action*='tab=1']")));
    }

    public WebElement getFieldClass() {
        return fieldClass;
    }
    public WebElement getTabStudents() {
        return tabStudents;
    }
    public WebElement getTabAuthenticatedUser() {
        return tabAuthenticatedUser;
    }
    public WebElement getFieldStatus() {
        return fieldStatus;
    }
    public WebElement getFieldOperation() {
        return fieldOperation;
    }
    public void clickButtonExecute() throws Exception {
        waitUntilElementIsClickable(buttonExecute);
        clickItem(buttonExecute, "The Execute button on the WhoIsWho page could not be clicked.");
        waitForPageToLoad();
    }
    public WebElement getMessage() {
        return message;
    }
    public PeopleInGroupForClassPage clickButtonAddUser() throws Exception {
        waitUntilElementIsClickable(buttonAddUser);
        clickItem(buttonAddUser, "The 'Add user' button on the WhoIsWho page could not be clicked.");
        waitForPageToLoad();
        return new PeopleInGroupForClassPage(webDriver, locations);
    }
    public AddUserPage clickButtonCreateUser() throws Exception {
        waitUntilElementIsClickable(buttonCreateUser);
        clickItem(buttonCreateUser, "The 'Create user' button on the WhoIsWho page could not be clicked.");
        waitForPageToLoad();
        return new AddUserPage(webDriver, locations);
    }

    public ArrayList<String> getTabsName(){
        ArrayList<String> listOfActualValue = new ArrayList<String>();
        List<WebElement> listForValue;
        listForValue = webDriver.findElements(By.xpath("//label[contains(@for, 'edit-role-')]"));
        for (WebElement element : listForValue) {
            String elementValue = element.getText();
            listOfActualValue.add(parseFileName(elementValue));
        }



    return  listOfActualValue;
    }
public String parseFileName(String value){
    String[] elementSplit=value.split(" ");
    String tabName=elementSplit[0];
    return tabName;
}



}
