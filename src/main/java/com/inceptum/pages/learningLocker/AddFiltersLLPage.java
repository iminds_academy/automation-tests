package com.inceptum.pages.learningLocker;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class AddFiltersLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "a[data-tab='user']")
    private WebElement tabByUser;
    @FindBy(how = How.CSS, using = "a[data-tab='date']")
    private WebElement tabByDate;
    @FindBy(how = How.CSS, using = "a[data-tab='page']")
    private WebElement tabByPage;
    @FindBy(how = How.CSS, using = "#page_search_form .items input")
    private WebElement fieldSelectPage;
    @FindBy(how = How.CSS, using = "#user_search_form .items input")
    private WebElement fieldSelectUser;
    @FindBy(how = How.ID, using = "applyFiltersButton")
    private WebElement buttonApplyFilters;
    @FindBy(how = How.CSS, using = ".radio-group [name='anonymous'][value='0']")
    private WebElement radiobuttonShowEverything;
    @FindBy(how = How.CSS, using = ".radio-group [name='anonymous'][value='1']")
    private WebElement radiobuttonShowAnonymousData;
    @FindBy(how = How.CSS, using = ".radio-group [name='anonymous'][value='2']")
    private WebElement radiobuttonDoNotShowAnonymous;
    @FindBy(how = How.CSS, using = "#daterange_search_form input[name='start']")
    private WebElement fieldStartDate;
    @FindBy(how = How.CSS, using = "#daterange_search_form input[name='end']")
    private WebElement fieldEndDate;
    @FindBy(how = How.CSS, using = "a.date-clear-link")
    private WebElement linkClearDates;
    @FindBy(how = How.CSS, using = "#page_search_form a.clear-link")
    private WebElement linkClearAllSelectedPages;
    @FindBy(how = How.CSS, using = "#user_search_form a.clear-link")
    private WebElement linkClearAllSelectedUsers;
    @FindBy(how = How.CSS, using = ".actions a.g-modal-close")
    private WebElement linkCancel;
    @FindBy(how = How.ID, using = "saveFilterButton")
    private WebElement buttonSave;


    /*---------CONSTRUCTORS--------*/

    public AddFiltersLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTabByUser() {
        waitUntilElementIsVisible(tabByUser);
        return tabByUser;
    }
    public WebElement getTabByDate() {
        waitUntilElementIsVisible(tabByDate);
        return tabByDate;
    }
    public WebElement getTabByPage() {
        waitUntilElementIsVisible(tabByPage);
        return tabByPage;
    }
    public WebElement getFieldSelectPage() {
        waitUntilElementIsVisible(fieldSelectPage);
        return fieldSelectPage;
    }
    public WebElement getFieldSelectUser() {
        waitUntilElementIsVisible(fieldSelectUser);
        return fieldSelectUser;
    }
    public void clickButtonApplyFilters() throws Exception {
        waitUntilElementIsClickable(buttonApplyFilters);
        clickItem(buttonApplyFilters, "The button 'Apply filters' could not be clicked from 'Add filters' popup.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".filtering-modal")));
        waitForPageToLoad();
    }
    public WebElement getRadiobuttonShowEverything() {
        return radiobuttonShowEverything;
    }
    public void selectRadiobuttonShowAnonymousData() throws Exception {
        waitUntilElementIsClickable(radiobuttonShowAnonymousData);
        radiobuttonShowAnonymousData.sendKeys(Keys.SPACE);
    }
    public void selectRadiobuttonDoNotShowAnonymous() throws Exception {
        waitUntilElementIsClickable(radiobuttonDoNotShowAnonymous);
        radiobuttonDoNotShowAnonymous.sendKeys(Keys.SPACE);
    }
    public WebElement getFieldStartDate() {
        waitUntilElementIsVisible(fieldStartDate);
        return fieldStartDate;
    }
    public WebElement getFieldEndDate() {
        waitUntilElementIsVisible(fieldEndDate);
        return fieldEndDate;
    }
    public void clickLinkClearDates() throws Exception {
        waitUntilElementIsClickable(linkClearDates);
        clickItem(linkClearDates, "The link 'Clear dates' could not be clicked from 'Add filters' popup.");
        Thread.sleep(500);
    }
    public void clickLinkClearAllSelectedPages() throws Exception {
        // Click 'Clear all selected pages' link
        waitUntilElementIsClickable(linkClearAllSelectedPages);
        clickItem(linkClearAllSelectedPages, "The link 'Clear all selected pages' could not be clicked from 'Add filters' popup.");
        // Wait until no item is shown in selected pages list
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@class='item']/*[@class='name']")));
    }
    public void clickLinkClearAllSelectedUsers() throws Exception {
        // Click 'Clear all selected users' link
        waitUntilElementIsClickable(linkClearAllSelectedUsers);
        clickItem(linkClearAllSelectedUsers, "The link 'Clear all selected users' could not be clicked from 'Add filters' popup.");
        // Wait until no item is shown in selected users list
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@class='item']/*[@class='name']")));
    }
    public void clickLinkCancel() throws Exception {
        waitUntilElementIsClickable(linkCancel);
        clickItem(linkCancel, "The link 'Cancel' could not be clicked from 'Add filters' popup.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".filtering-modal")));
        waitForPageToLoad();
    }
    public void clickButtonSave() throws Exception {
        waitUntilElementIsClickable(buttonSave);
        clickItem(buttonSave, "The button 'Save' could not be clicked from 'Add filters' popup.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".filtering-modal")));
        waitForPageToLoad();
    }













}
