package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.BasePage;

public class AdminDashboardLLPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "a.lrs-chooser")
    private WebElement fieldLRS;


    /*---------CONSTRUCTORS--------*/

    public AdminDashboardLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldLRS() {
        waitUntilElementIsVisible(fieldLRS);
        return fieldLRS;
    }




}
