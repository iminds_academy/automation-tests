package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class AllUsersLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".paginator>div.stats")
    private WebElement paginationStatsField;
    @FindBy(how = How.CSS, using = "table.vau-table")
    private WebElement tableUsers;



    /*---------CONSTRUCTORS--------*/

    public AllUsersLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getPaginationStatsField() {
        waitUntilElementIsVisible(paginationStatsField);
        return paginationStatsField;
    }
    public WebElement getTableUsers() {
        waitUntilElementIsVisible(tableUsers);
        return tableUsers;
    }



}
