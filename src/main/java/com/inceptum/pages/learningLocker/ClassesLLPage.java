package com.inceptum.pages.learningLocker;


import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class ClassesLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".cleanup-button[href*='/reset']")
    private WebElement linkCleanLRS;



    /*---------CONSTRUCTORS--------*/

    public ClassesLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickLinkCleanLRS() throws Exception {
        waitUntilElementIsClickable(linkCleanLRS);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, linkCleanLRS);
        checkAlert();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".alert-success")));
    }




}
