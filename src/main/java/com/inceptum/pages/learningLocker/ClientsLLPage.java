package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.learningLocker.AdminDashboardLLPage;

public class ClientsLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.XPATH, using = "//td[2]/a")
    private WebElement fieldUsername;
    @FindBy(how = How.XPATH, using = "//td[3]/a")
    private WebElement fieldPassword;
    @FindBy(how = How.XPATH, using = "//b[contains(text(), 'Endpoint')]/../span")
    private WebElement fieldEndpoint;


    /*---------CONSTRUCTORS--------*/

    public ClientsLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getFieldUsername() {
        waitUntilElementIsVisible(fieldUsername);
        return fieldUsername;
    }
    public WebElement getFieldPassword() {
        waitUntilElementIsVisible(fieldPassword);
        return fieldPassword;
    }
    public WebElement getFieldEndpoint() {
        waitUntilElementIsVisible(fieldEndpoint);
        return fieldEndpoint;
    }



}
