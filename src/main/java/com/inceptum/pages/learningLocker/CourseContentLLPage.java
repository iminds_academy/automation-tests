package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CourseContentLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "list_pages")
    private WebElement tableCourseContent;
    @FindBy(how = How.CSS, using = "#list_active_users~a")
    private WebElement linkViewAllUsers;



    /*---------CONSTRUCTORS--------*/

    public CourseContentLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTableCourseContent() {
        waitUntilElementIsVisible(tableCourseContent);
        return tableCourseContent;
    }
    public AllUsersLLPage clickLinkViewAllUsers() throws Exception {
        waitUntilElementIsClickable(linkViewAllUsers);
        clickItem(linkViewAllUsers, "The link 'View all users' could not be clicked from CourseContent page.");
        waitForPageToLoad();
        return new AllUsersLLPage(webDriver, locations);
    }




}
