package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CourseDetailedLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "list_active_users")
    private WebElement tableMostActiveUsers;
    @FindBy(how = How.ID, using = "list_course_material")
    private WebElement tableCourseMaterial;
    @FindBy(how = How.ID, using = "list_additional_material")
    private WebElement tableAdditionalMaterial;



    /*---------CONSTRUCTORS--------*/

    public CourseDetailedLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTableMostActiveUsers() {
        waitUntilElementIsVisible(tableMostActiveUsers);
        return tableMostActiveUsers;
    }
    public WebElement getTableCourseMaterial() {
        waitUntilElementIsVisible(tableCourseMaterial);
        return tableCourseMaterial;
    }
    public WebElement getTableAdditionalMaterial() {
        waitUntilElementIsVisible(tableAdditionalMaterial);
        return tableAdditionalMaterial;
    }




}
