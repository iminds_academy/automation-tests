package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class CreateLrsPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "input#title")
    private WebElement fieldTitle;
    @FindBy(how = How.CSS, using = "input#description")
    private WebElement fieldDescription;
    @FindBy(how = How.CSS, using = ".form-group [type='submit']")
    private WebElement buttonSubmit;


    /*---------CONSTRUCTORS--------*/

    public CreateLrsPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldTitle(String title) {
        waitUntilElementIsClickable(fieldTitle);
        fieldTitle.sendKeys(title);
    }
    public void fillFieldDescription(String description) {
        waitUntilElementIsClickable(fieldDescription);
        fieldDescription.sendKeys(description);
    }
    public void clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The button 'Submit' on the CreateLRS page could not be clicked.");
        waitForPageToLoad();
    }



}
