package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.BasePage;

public class LoginFormLLPage extends BasePage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "input#email")
    private WebElement fieldEmail;
    @FindBy(how = How.CSS, using = "input#password")
    private WebElement fieldPassword;
    @FindBy(how = How.CSS, using = ".form-group [type='submit']")
    private WebElement buttonSubmit;


    /*---------CONSTRUCTORS--------*/

    public LoginFormLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void fillFieldEmail(String email) {
        waitUntilElementIsClickable(fieldEmail);
        fieldEmail.sendKeys(email);
    }
    public void fillFieldPassword(String password) {
        waitUntilElementIsClickable(fieldPassword);
        fieldPassword.sendKeys(password);
    }
    public AdminDashboardLLPage clickButtonSubmit() throws Exception {
        waitUntilElementIsClickable(buttonSubmit);
        clickItem(buttonSubmit, "The button 'Submit' on the LoginFormLL page could not be clicked.");
        waitForPageToLoad();
        return new AdminDashboardLLPage(webDriver, locations);
    }




}
