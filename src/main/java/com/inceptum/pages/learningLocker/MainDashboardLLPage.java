package com.inceptum.pages.learningLocker;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;

public class MainDashboardLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = ".nav-sidebar a[href$='dashboard']")
    private WebElement linkMainDashboard;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href*='client/manage']")
    private WebElement linkManageClients;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href*='classes/manage']")
    private WebElement linkManageClasses;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href$='dashboard/course']")
    private WebElement linkCourseContent;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href*='dashboard/course/movies']")
    private WebElement linkMovies;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href*='dashboard/course/quizzes']")
    private WebElement linkQuizzes;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href*='dashboard/user']")
    private WebElement linkUsers;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href*='dashboard/filters']")
    private WebElement linkFilters;
    @FindBy(how = How.CSS, using = ".nav-sidebar a[href$='dashboard/statements']")
    private WebElement linkStatements;
    @FindBy(how = How.CSS, using = ".navbar2 #classDropdown")
    private WebElement fieldClass;
    @FindBy(how = How.ID, using = "list_most_viewed_pages")
    private WebElement tableMostViewedPages;
    @FindBy(how = How.CSS, using = "#list_active_users~a")
    private WebElement linkViewAllUsers;
    @FindBy(how = How.XPATH, using = "//a[contains(@class, 'global-filter-button')]/i[@class='icon icon-plus']/..")
    private WebElement buttonAddFiltering;
    @FindBy(how = How.CSS, using = "#currentFilterReset+a[class*='global-filter-button']")
    private WebElement buttonChangeFiltering;
    @FindBy(how = How.CSS, using = ".current-filter-display")
    private WebElement fieldCurrentFilter;
    @FindBy(how = How.ID, using = "currentFilterReset")
    private WebElement iconResetFilter;


    /*---------CONSTRUCTORS--------*/

    public MainDashboardLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public MainDashboardLLPage clickLinkMainDashboard() throws Exception {
        waitUntilElementIsClickable(linkMainDashboard);
        clickItem(linkMainDashboard, "The link 'Main dashboard' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new MainDashboardLLPage(webDriver, locations);
    }
    public ClientsLLPage clickLinkManageClients() throws Exception {
        waitUntilElementIsClickable(linkManageClients);
        clickItem(linkManageClients, "The link 'Manage clients' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new ClientsLLPage(webDriver, locations);
    }
    public ClassesLLPage clickLinkManageClasses() throws Exception {
        waitUntilElementIsClickable(linkManageClasses);
        clickItem(linkManageClasses, "The link 'Manage classes' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new ClassesLLPage(webDriver, locations);
    }
    public CourseContentLLPage clickLinkCourseContent() throws Exception {
        waitUntilElementIsClickable(linkCourseContent);
        clickItem(linkCourseContent, "The link 'Course content' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new CourseContentLLPage(webDriver, locations);
    }
    public UsersLLPage clickLinkUsers() throws Exception {
        waitUntilElementIsClickable(linkUsers);
        clickItem(linkUsers, "The link 'Users' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new UsersLLPage(webDriver, locations);
    }
    public WebElement getLinkFilters() {
        return linkFilters;
    }
    public FiltersLLPage clickLinkFilters() throws Exception {
        waitUntilElementIsClickable(linkFilters);
        clickItem(linkFilters, "The link 'Filters' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new FiltersLLPage(webDriver, locations);
    }
    public MoviesLLPage clickLinkMovies() throws Exception {
        waitUntilElementIsClickable(linkMovies);
        clickItem(linkMovies, "The link 'Movies' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new MoviesLLPage(webDriver, locations);
    }
    public QuizzesLLPage clickLinkQuizzes() throws Exception {
        waitUntilElementIsClickable(linkQuizzes);
        clickItem(linkQuizzes, "The link 'Quizzes' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new QuizzesLLPage(webDriver, locations);
    }
    public StatementsLLPage clickLinkStatements() throws Exception {
        waitUntilElementIsClickable(linkStatements);
        clickItem(linkStatements, "The link 'Statements' on sidebar menu could not be clicked.");
        waitForPageToLoad();
        return new StatementsLLPage(webDriver, locations);
    }
    public WebElement getFieldClass() {
        waitUntilElementIsVisible(fieldClass);
        return fieldClass;
    }
    public WebElement getTableMostViewedPages() {
        waitUntilElementIsVisible(tableMostViewedPages);
        return tableMostViewedPages;
    }
    public AllUsersLLPage clickLinkViewAllUsers() throws Exception {
        waitUntilElementIsClickable(linkViewAllUsers);
        clickItem(linkViewAllUsers, "The link 'View all users' could not be clicked from MainDashboard page.");
        waitForPageToLoad();
        return new AllUsersLLPage(webDriver, locations);
    }
    public WebElement getButtonAddFiltering() {
        return buttonAddFiltering;
    }
    public AddFiltersLLPage clickButtonAddFiltering() throws Exception {
        waitUntilElementIsClickable(buttonAddFiltering);
        clickItem(buttonAddFiltering, "The button 'Add filtering' could not be clicked from MainDashboard page.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".filtering-modal")));
        return new AddFiltersLLPage(webDriver, locations);
    }
    public AddFiltersLLPage clickButtonChangeFiltering() throws Exception {
        waitUntilElementIsClickable(buttonChangeFiltering);
        clickItem(buttonChangeFiltering, "The button 'Change filtering' could not be clicked from MainDashboard page.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".filtering-modal")));
        return new AddFiltersLLPage(webDriver, locations);
    }
    public WebElement getFieldCurrentFilter() {
        waitUntilElementIsVisible(fieldCurrentFilter);
        return fieldCurrentFilter;
    }
    public void clickIconResetFilter() throws Exception {
        waitUntilElementIsClickable(iconResetFilter);
        clickItem(iconResetFilter, "The icon 'reset filter' could not be clicked from MainDashboard page.");
    }









}
