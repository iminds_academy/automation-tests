package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class MovieDetailedLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "list_top_users")
    private WebElement tableTopUsers;



    /*---------CONSTRUCTORS--------*/

    public MovieDetailedLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTableTopUsers() {
        waitUntilElementIsVisible(tableTopUsers);
        return tableTopUsers;
    }




}
