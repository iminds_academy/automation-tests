package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class MoviesLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "a[href$='statementsParentsFix']")
    private WebElement iconCogwheel;
    @FindBy(how = How.CSS, using = ".table.movies-list")
    private WebElement tableMovies;
    @FindBy(how = How.ID, using = "list_watched_users")
    private WebElement tableTopUsersWatched;


    /*---------CONSTRUCTORS--------*/

    public MoviesLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickIconCogwheel() throws Exception {
        waitUntilElementIsClickable(iconCogwheel);
        clickItem(iconCogwheel, "The icon 'cogwheel' on the Movies page could not be clicked.");
        checkAlert(); // close alert window
        waitForPageToLoad();
    }
    public WebElement getTableMovies() {
        return tableMovies;
    }
    public WebElement getTableTopUsersWatched() {
        waitUntilElementIsVisible(tableTopUsersWatched);
        return tableTopUsersWatched;
    }



}
