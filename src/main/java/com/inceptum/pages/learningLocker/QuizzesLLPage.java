package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class QuizzesLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "#list_pages .table")
    private WebElement tableQuizzes;
    @FindBy(how = How.CSS, using = "#list_score_per_user")
    private WebElement popupListScorePerUser;
    @FindBy(how = How.ID, using = "list_best_users")
    private WebElement listBestUsers;
    @FindBy(how = How.ID, using = "list_worst_users")
    private WebElement listWorstUsers;


    /*---------CONSTRUCTORS--------*/

    public QuizzesLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTableQuizzes() {
        waitUntilElementIsVisible(tableQuizzes);
        return tableQuizzes;
    }
    public WebElement getPopupListScorePerUser() {
        waitUntilElementIsVisible(popupListScorePerUser);
        return popupListScorePerUser;
    }
    public WebElement getListBestUsers() {
        waitUntilElementIsVisible(listBestUsers);
        return listBestUsers;
    }
    public WebElement getListWorstUsers() {
        waitUntilElementIsVisible(listWorstUsers);
        return listWorstUsers;
    }



}
