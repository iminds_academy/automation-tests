package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class UserDetailedLLPage extends MainDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.ID, using = "list_pages")
    private WebElement tableDetailedCourseProgress;
    @FindBy(how = How.ID, using = "list_quizzes")
    private WebElement listQuizzes;


    /*---------CONSTRUCTORS--------*/

    public UserDetailedLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public WebElement getTableDetailedCourseProgress() {
        waitUntilElementIsVisible(tableDetailedCourseProgress);
        return tableDetailedCourseProgress;
    }
    public WebElement getListQuizzes() {
        waitUntilElementIsVisible(listQuizzes);
        return listQuizzes;
    }



}
