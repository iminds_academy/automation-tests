package com.inceptum.pages.learningLocker;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.inceptum.enumeration.PageLocation;

public class UsersLLPage extends AdminDashboardLLPage {

    /* ----- FIELDS ----- */

    @FindBy(how = How.CSS, using = "a[href*='flush_cache']")
    private WebElement iconClearCache;
    @FindBy(how = How.CSS, using = "#list_active_users~a")
    private WebElement linkViewAllUsers;


    /*---------CONSTRUCTORS--------*/

    public UsersLLPage(WebDriver webDriver, PageLocation locations) throws Exception {
        super(webDriver, locations);
    }


    /*---------METHODS----------*/

    public void clickIconClearCache() throws Exception {
        waitUntilElementIsClickable(iconClearCache);
        clickItem(iconClearCache, "The icon 'clear cache' on the Users page could not be clicked.");
        waitForPageToLoad();
    }
    public AllUsersLLPage clickLinkViewAllUsers() throws Exception {
        waitUntilElementIsClickable(linkViewAllUsers);
        clickItem(linkViewAllUsers, "The link 'View all users' could not be clicked from Users page.");
        waitForPageToLoad();
        return new AllUsersLLPage(webDriver, locations);
    }



}
