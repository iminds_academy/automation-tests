package com.inceptum.util;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * This handles basic Debug logging.
 */

public class Debug {

    /* ----- CONSTANTS ----- */
    public static final int ALWAYS = 0;
    public static final int VERBOSE = 1;
    public static final int TIMING = 2;
    public static final int INFO = 3;
    public static final int IMPORTANT = 4;
    public static final int WARNING = 5;
    public static final int ERROR = 6;
    public static final int FATAL = 7;
    /* ----- FIELDS ----- */
    protected static Logger rootLogger = Logger.getRootLogger();
    protected static boolean levelOnCache[] = new boolean[8];

    /* ----- CONSTRUCTORS ----- */

    /* ----- METHODS ----- */

    /**
     * @param logLevel
     * @param msg
     * @param module
     */
    public static void log(Level logLevel, String msg, String module) {
        Logger logger = getLogger(module);
        logger.setLevel(logLevel);
        logger.log(logLevel, msg);
    }

    public static void logTrace(String msg, String module) {
        log(Level.TRACE, msg, module);
    }

    public static void log(String msg, String module) {
        log(Level.DEBUG, msg, module);
    }

    public static void logInfo(String msg, String module) {
        log(Level.INFO, msg, module);
    }

    public static void logWarning(String msg, String module) {
        log(Level.WARN, msg, module);
    }

    public static void logError(String msg, String module) {
        log(Level.ERROR, msg, module);
    }

    public static void logFatal(String msg, String module) {
        log(Level.FATAL, msg, module);
    }


    /* ----- GETTERS / SETTERS ----- */

    /**
     * Given the module name for the class we're logging in, this method will
     * return the appropriate {@link Logger} to use. In cases where the class
     * name can't be obtained the root logger will be used.
     *
     * @param module The name of the class module
     * @return The appropriate {@link Logger} instance
     */
    public static Logger getLogger(String module) {
        if (StringUtils.isNotBlank(module)) {
            return Logger.getLogger(module);
        } else {
            return rootLogger;
        }
    }


    /* ----- STATIC METHODS ----- */

    /* ----- INNER CLASSES ----- */

}
