package com.inceptum.webdriver;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.DefaultCredentialsProvider;
import com.gargoylesoftware.htmlunit.WebClient;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

/**
 * Extends {@link HtmlUnitDriver} purely to support BASIC authentication.
 */

public class AuthenticatedHtmlUnitDriver extends HtmlUnitDriver {

    /* ----- CONSTANTS ----- */

    /* ----- FIELDS ----- */
    private final String user;
    private final String pass;


    /* ----- CONSTRUCTORS ----- */

    /**
     * Instantiates a new authenticated html unit driver.
     *
     * @param username the username
     * @param password the password
     */
    public AuthenticatedHtmlUnitDriver(String username, String password) {
        this.user = username;
        this.pass = password;
    }

    /* (non-Javadoc)
      * @see org.openqa.selenium.htmlunit.HtmlUnitDriver#newWebClient(com.gargoylesoftware.htmlunit.BrowserVersion)
      */
    @Override
    protected WebClient newWebClient(BrowserVersion browserVersion) {
        WebClient client = super.newWebClient(browserVersion);
        DefaultCredentialsProvider provider = new DefaultCredentialsProvider();
        provider.addCredentials(this.user, this.pass);
        client.setCredentialsProvider(provider);
        return client;
    }
}
