package com.inceptum.webdriver;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.lang.StringUtils;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;

import com.inceptum.vo.BrowserVo;
import com.opera.core.systems.OperaDriver;

//import org.openqa.selenium.android.AndroidDriver;
//import org.openqa.selenium.ie.InternetExplorerDriver;
//import org.openqa.selenium.iphone.IPhoneDriver;

/**
 * This is the factory to instantiate a WebDriver for use. It will smartly
 * return an instance of a local driver or an instance of a remote web driver
 * via the GridHub2 specified which in itself will smartly return a web driver
 * or Selenium RC instance of the browser.
 */
public class WebDriverFactory {

    /* ----- CONSTANTS ----- */

    // Browsers
    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    public static final String OPERA = "opera";
    public static final String SAFARI = "safari";
    public static final String INTERNET_EXPLORER = "ie";
    public static final String HTML_UNIT = "htmlunit";
    public static final String IPHONE = "iphone";
    // Platforms
    public static final String WINDOWS = "windows";
    public static final String ANDROID = "android";
    public static final String XP = "xp";
    public static final String VISTA = "vista";
    public static final String WIN_8 = "win8";
    public static final String WINDOWS8_1 = "win8.1";
    public static final String MAVERICKS = "mavericks";
    public static final String MAC = "mac";
    public static final String LINUX = "linux";


    /* ----- FIELDS ----- */

    /* ----- CONSTRUCTORS ----- */


    /* ----- METHODS ----- */

    /**
     * Returns a {@link org.openqa.selenium.remote.RemoteWebDriver} instance from the provided URL of the
     * Grid Hub and the Browser instance.
     *
     * @param gridHubUrl grid hub URI
     * @param browser    Browser object containing info around the browser to hit
     * @param username   username for BASIC authentication on the page to test
     * @param password   password for BASIC authentication on the page to test
     * @return A {@link org.openqa.selenium.remote.RemoteWebDriver}
     */
    public static WebDriver getInstance(String gridHubUrl, BrowserVo browser,
                                        String username, String password,
                                        String currentTest) {

        String browserName = browser.getName();
        String browserVersion = browser.getVersion();
        String browserPlatform = browser.getPlatform();

        // In case there is no Hub
        if (StringUtils.isBlank(gridHubUrl)) {
            return getInstance(browserName, username, password);
        }

        // In case there is no browser
        if (StringUtils.isBlank(browserName)) {
            browserName = FIREFOX;
        }

        // In case there is no platform
        if (StringUtils.isBlank(browserPlatform)) {
            browserPlatform = VISTA;
        }

        // In case there is no version
        if (StringUtils.isBlank(browserVersion)) {
            if(browserName.equals(FIREFOX)) {
                browserVersion = "34";
            } else if(browserName.equals(INTERNET_EXPLORER)) {
                browserVersion = "11";
            } else if(browserName.equals(OPERA)) {
                browserVersion = "11";
            } else if(browserName.equals(SAFARI)) {
                browserVersion = "7";
            }
        }


        WebDriver webDriver = null;
        DesiredCapabilities capability = new DesiredCapabilities();
        capability.setJavascriptEnabled(true);

        if (CHROME.equals(browserName)) {
            capability = DesiredCapabilities.chrome();
        } else if (FIREFOX.equals(browserName)) {

            capability = DesiredCapabilities.firefox();

            FirefoxProfile ffProfile = new FirefoxProfile();
            ffProfile.setEnableNativeEvents(true);

            // Authenication Hack for Firefox
            if (username != null && password != null) {
                ffProfile.setPreference("network.http.phishy-userpass-length",
                        255);
                capability.setCapability(FirefoxDriver.PROFILE, ffProfile);
            }

            capability.setCapability(CapabilityType.TAKES_SCREENSHOT, true);
        } else if (INTERNET_EXPLORER.equals(browserName)) {
            capability = DesiredCapabilities.internetExplorer();

            capability.setCapability("requireWindowFocus", true);
            capability.setCapability("enablePersistentHover", false); // from https://code.google.com/p/selenium/issues/detail?id=2067

            /*capability = DesiredCapabilities.internetExplorer();
            capability.setCapability(
                    InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
                    true);*/
        } else if (OPERA.equals(browserName)) {
            capability = DesiredCapabilities.opera();
        } else if (ANDROID.equals(browserName)) {
            capability = DesiredCapabilities.android();
        } else if (IPHONE.equals(browserName)) {
            capability = DesiredCapabilities.iphone();
        } else if (SAFARI.equals(browserName)) {
            capability = DesiredCapabilities.safari();
        } else {

            capability = DesiredCapabilities.htmlUnit();
            // HTMLunit Check
            if (username != null && password != null) {
                webDriver = (HtmlUnitDriver) new AuthenticatedHtmlUnitDriver(username, password);
            } else {
                webDriver = new HtmlUnitDriver(true);
            }

            return webDriver;
        }

        capability = setVersionAndPlatform(capability, browserVersion,
                browserPlatform);
        capability.setCapability("name", currentTest);
        capability.setCapability("command-timeout", 60);

        // Create Remote WebDriver
        try {
            RemoteWebDriver remoteWebDriver = new RemoteWebDriver(new URL(gridHubUrl), capability);
            remoteWebDriver.setFileDetector(new LocalFileDetector());
            webDriver = remoteWebDriver;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return webDriver;
    }

    /**
     * This is a factory method to return a WebDriver instance for the provided
     * browser type.
     *
     * @param browser  String representing the local browser to hit
     * @param username username for BASIC authentication on the page to test
     * @param password password for BASIC authentication on the page to test
     * @return WebDriver A {@link WebDriver} instance
     */
    public static WebDriver getInstance(String browser, String username,
                                        String password) {

        WebDriver webDriver = null;

        if (CHROME.equals(browser)) {
            setChromeDriver();

            webDriver = new ChromeDriver();
        } else if (FIREFOX.equals(browser)) {

            FirefoxProfile ffProfile = new FirefoxProfile();
            ffProfile.setEnableNativeEvents(true);

            // Authenication Hack for Firefox
            if (username != null && password != null) {
                ffProfile.setPreference("network.http.phishy-userpass-length",
                        255);
            }

            webDriver = new FirefoxDriver(ffProfile);

        } else if (INTERNET_EXPLORER.equals(browser)) {
            webDriver = new InternetExplorerDriver();

        } else if (OPERA.equals(browser)) {
            webDriver = new OperaDriver();

        } else if (IPHONE.equals(browser)) {
            // try {
            //     webDriver = new IPhoneDriver();
            // } catch (Exception e) {
            //     e.printStackTrace();
            // }

        } else if (SAFARI.equals(browser)) {
            webDriver = new SafariDriver();

        } else if (ANDROID.equals(browser)) {
            // webDriver = new AndroidDriver();

        } else {

            // HTMLunit Check
            if (username != null && password != null) {
                webDriver =
                        (HtmlUnitDriver) new AuthenticatedHtmlUnitDriver(
                                username, password);
            } else {
                webDriver = new HtmlUnitDriver(true);
            }
        }

        return webDriver;
    }

    /**
     * Helper method to set version and platform for a specific browser.
     *
     * @param capability DesiredCapabilities object coming from the selected
     *                   browser
     * @param version    browser version
     * @param platform   browser platform
     * @return DesiredCapabilities
     */
    private static DesiredCapabilities setVersionAndPlatform(
            DesiredCapabilities capability, String version, String platform) {
        if (MAC.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.MAC);
        } else if (LINUX.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.LINUX);
        } else if (XP.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.XP);
        } else if (VISTA.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.VISTA);
        } else if (WINDOWS.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.WINDOWS);
        } else if (ANDROID.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.ANDROID);
        } else if(WIN_8.equalsIgnoreCase(platform)) {
            capability.setPlatform(Platform.WIN8);
        } else if(WINDOWS8_1.equalsIgnoreCase(platform)) {
            capability.setCapability("platform", "win8.1");
        } else if(MAVERICKS.equalsIgnoreCase(platform)) {
            capability.setCapability("platform", "OS X 10.9");
        } else {
            capability.setPlatform(Platform.ANY);
        }

        if (version != null) {
            capability.setVersion(version);
        }
        return capability;
    }

    /**
     * Helper method to set ChromeDriver location into the right ststem property
     */
    private static void setChromeDriver() {
        String os = System.getProperty("os.name").toLowerCase().substring(0, 3);
        String chromeBinary = "src/main/resources/drivers/chrome/chromedriver"
                + (os.equals("win") ? ".exe" : "");
        System.setProperty("webdriver.chrome.driver", chromeBinary);
    }
}
