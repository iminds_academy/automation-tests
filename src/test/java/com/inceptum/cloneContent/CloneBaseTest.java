package com.inceptum.cloneContent;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TestBase;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.BasePage;
import com.inceptum.pages.CreateImagePage;
import com.inceptum.pages.CreatePresentationPage;
import com.inceptum.pages.CreateVideoPage;
import com.inceptum.pages.EditClassPage;
import com.inceptum.pages.GroupTabGroupPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.PeopleInGroupForClassPage;
import com.inceptum.pages.SelectImagePage;
import com.inceptum.pages.SelectPresentationPage;
import com.inceptum.pages.SelectVideoPage;
import com.inceptum.pages.ViewGroupPage;
import com.inceptum.pages.ViewPeerAssignmentPage;
import com.inceptum.pages.ViewTheoryPage;
import com.inceptum.pages.ViewVideoPage;

/**
 * Created by Olga on 24.09.2015.
 */
public class CloneBaseTest extends TestBase {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final String statusPublished = "Published";


    /* ----- Tests ----- */

    @Before
    public void beforeTest() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage.openThisPage(webDriver, pageLocation);
        // Login as admin and clear Recent log messages
        loginAs(admin);
        clearLogMessages();
        // Delete Class1
        deleteClass(className);
        // Delete Class2
        deleteClassFromOverviewPage(className2);
        // Delete clones of Class1 and Class2
        deleteClassFromOverviewPage("Clone of " + className);
        deleteClassFromOverviewPage("Clone of " + className2);
        waitForPageToLoad();
    }

    @After
    public void afterTest() throws Exception {
        HomePage homePage = openHomePage();
        String xpathLinkUserAccount = "//nav[@id='block-system-user-menu']/ul/li/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLinkUserAccount)));
        WebElement linkUserAccount = webDriver.findElement(By.xpath(xpathLinkUserAccount));
        if (!(linkUserAccount.getText().equals(admin.get(0)))) {
            if (linkUserAccount.getText().equals("Sign in")) {
                login(admin);
            } else {
                linkUserAccount.click();
                homePage.clickLinkSignOut();
                login(admin);
            }
        }
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> errors = new ArrayList<String>();
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available
                break;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {  // Find php messages starting with "Notice:"/"Warning:"/"Strict warning:"
                    errors.add(message.getAttribute("href"));
                }
            }
        }

        // Get all php messages
        for (String url : errors) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            System.out.println(fullMessage);
        }

        if (errors.size() > 0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }


    public String cloneClass(String className) throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        EditClassPage editClassPage = overviewPage.clickLinkClone();
        editClassPage.clickButtonSave();
        String classSelected = overviewPage.getTextOfSelectedClass();
        Assert.assertTrue("Wrong Class is selected in class menu field.", classSelected.equals("Clone of " + className));
        return webDriver.getCurrentUrl();
    }
    public void checkNumberOfEntitiesInMessage(String className, int number) throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        String numberString = Integer.toString(number);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class Clone of " + className + " has been created.\n" +
                "Finished " + numberString + " entities created successfully.");
    }
    public void checkNumberOfEntitiesInMessageForGroup(String groupName, int number) throws Exception {
        ViewGroupPage viewGroupPage = new ViewGroupPage(webDriver, pageLocation);
        String numberString = Integer.toString(number);
        assertTextOfMessage(viewGroupPage.getMessage(), ".*Group Clone of " + groupName + " has been created.\n" +
                "Finished " + numberString + " entities created successfully.");
    }
    public String addTheoryToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent();
        // Click the link 'Theory' on a popup
        ViewTheoryPage viewTheoryPage = overviewPage.clickLinkTheory();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory theory temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTheoryPage.getLinkEdit().isDisplayed());
        return viewTheoryPage.getFieldTitle().getText();
    }
    public String addVideoToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent();
        // Click the link 'Video' on a popup
        ViewVideoPage viewVideoPage = overviewPage.clickLinkVideo();
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        return viewVideoPage.getFieldTitle().getText();
    }
    public void checkClassFromOverviewPage(String titleOfNode, String className) throws Exception {
        waitForPageToLoad();
        // Find element (Course component) by its title on Class overview page
        WebElement node = webDriver.findElement(By.xpath("//a/h2/div[text()='" + titleOfNode + "']"));
        // Get its nodeId
        String nodeId = node.findElement(By.xpath("./../../..")).getAttribute("nodeid");
        // Get url of node Edit page
        String urlEditNodePage = websiteUrl + "/node/" + nodeId + "/edit";
        // Open edit page
        webDriver.get(urlEditNodePage);
        // Check Class in 'Groups audience'
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.checkProperClassInGroupAudience(className);
    }
    public void checkItemIsShownOnOverviewClassPage(String title) throws Exception {
        waitForPageToLoad();
        List<WebElement> items = webDriver.findElements(By.xpath("//a/h2/div[text()='" + title + "']"));
        Assert.assertTrue("The item isn't shown OR there are >1 items with such title on the page.", items.size() == 1);
    }
    public void checkNumberOfNodesOnOverviewPage(int nodes) throws Exception {
        waitForPageToLoad();
        List<WebElement> items = webDriver.findElements(By.cssSelector("div a.node"));
        int nodesActual = items.size();
        Assert.assertTrue("Another number of nodes is shown on Class overview page.", nodesActual == nodes);
    }
    public void checkNoItemOnOverviewClassPage() throws Exception {
        waitForPageToLoad();
        List<WebElement> items = webDriver.findElements(By.xpath("//a/h2/div"));
        Assert.assertTrue("The list of nodes on Class overview page is not empty.", items.size() == 0);
    }
    public void checkItemIsShownInContentTable(String title, int times) throws Exception {
        waitForPageToLoad();
        List<WebElement> items = webDriver.findElements(By.xpath("//td[2]/a[text()='" + title + "']"));
        Assert.assertTrue("The item isn't shown on the page as many times as expected.", items.size() == times);
    }
    public void checkNumberOfMembersInClass(String urlClassOverviewPage, int number) throws Exception {
        openPeopleInGroupFromClassOverview(urlClassOverviewPage);
        List<WebElement> rows = webDriver.findElements(By.cssSelector("[id*='views-form-og-members'] tbody tr"));
        String numberActual = Integer.toString(rows.size());
        String numberExpected = Integer.toString(number);
        Assert.assertTrue("Number of users is not equal to: '" + numberExpected + "'.", numberActual.equals(numberExpected));
    }
    public PeopleInGroupForClassPage openPeopleInGroupFromClassOverview(String urlClassOverviewPage) throws Exception {
        String urlPeopleInClassGroupPage = urlClassOverviewPage.replace("overview/nojs", "group/node").concat("/admin/people");
        webDriver.get(urlPeopleInClassGroupPage);
        waitForPageToLoad();
        return new PeopleInGroupForClassPage(webDriver, pageLocation);
    }
    public void checkUserIsDisplayedInPeopleInGroupTable(List<String> user) throws Exception {
        waitForPageToLoad();
        List<WebElement> users = webDriver.findElements(By.xpath("//td[2]/a[@class='username'][text()='" + user.get(0) + "']"));
        Assert.assertTrue("0 or more than 1 users are shown in the table with such name.", users.size() == 1);
    }
    public void checkDraftPA() throws Exception {
        waitForPageToLoad();
        List<WebElement> drafts = webDriver.findElements(By.cssSelector("a.node--peertask.node-unpublished"));
        Assert.assertTrue("No draft PA has been found on the Class overview page (or number is >1).", drafts.size() == 1);
    }
    public void selectStatusOfPA(String status) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        selectItemFromListByOptionName(viewPeerAssignmentPage.getFieldStatus(), status);
        waitForPageToLoad();
    }
    public void checkViewMaterialPageTitleIs(String title) throws Exception {
        waitForPageToLoad();
        String titleActual = webDriver.findElement(By.cssSelector("h2 div")).getText();
        Assert.assertTrue("Title actual is not equal to title expected.", titleActual.equals(title));
    }
    public void copyPageToClass(String className) throws Exception { // from View component/material page
        BasePage basePage = new BasePage(webDriver, pageLocation);
        selectItemFromListByOptionName(basePage.getFieldCopyPage(), className);
        waitForPageToLoad();
        assertClassIsSelectedInClassMenu(className);
    }
    public void movePageToClass(String className) throws Exception { // from View component/material page
        BasePage basePage = new BasePage(webDriver, pageLocation);
        selectItemFromListByOptionName(basePage.getFieldMovePage(), className);
        waitForPageToLoad();
        assertClassIsSelectedInClassMenu(className);
    }
    public void addImageFromEditPageWithIcon(WebElement iconAddImage, WebElement fieldImages) throws Exception {
        clickItem(iconAddImage, "The icon 'add' could not be clicked on Edit page.");
        CreateImagePage createImagePage = new CreateImagePage(webDriver, pageLocation);
        createImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        BasePage basePage = new BasePage(webDriver, pageLocation);
        assertTextOfMessage(basePage.getMessageInfo(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = fieldImages.getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(imageTitle + ".*"));
    }
    public void addPresentationFromEditPageWithIcon(WebElement iconAddPresentation, WebElement fieldPresentations) throws Exception {
        clickItem(iconAddPresentation, "The icon 'add' could not be clicked on Edit page.");
        CreatePresentationPage createPresentationPage = new CreatePresentationPage(webDriver, pageLocation);
        createPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        BasePage basePage = new BasePage(webDriver, pageLocation);
        assertTextOfMessage(basePage.getMessageInfo(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = fieldPresentations.getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation, titleOfCreatedPresentation.matches(presentationTitle + ".*"));
    }
    public void addVideoFromEditPageWithIcon(WebElement iconAddVideo, WebElement fieldVideos, String urlVideo) throws Exception {
        clickItem(iconAddVideo, "The icon 'add' could not be clicked on Edit page.");
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, pageLocation);
        createVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(urlVideo); // urlYouTube
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        BasePage basePage = new BasePage(webDriver, pageLocation);
        assertTextOfMessage(basePage.getMessageInfo(), ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEO'S field
        String titleOfCreatedVideo = fieldVideos.getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEO'S field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(videoTitle + ".*"));
    }
    public void checkListOfMaterialsIsDisplayedOnViewPage(List<String> titleMaterialsExpected) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".field__items .title")));
        List<WebElement> materials = webDriver.findElements(By.cssSelector(".field__items .title"));
        List<String> titleMaterials = new ArrayList<String>();
        for (WebElement material : materials) {
            String titleMaterial = material.getText();
            titleMaterials.add(titleMaterial);
        }
        if (titleMaterials.size() == 0) {
            Assert.assertTrue("List of materials is empty.", false);
        }
        Assert.assertTrue("The materials are displayed on View page.", titleMaterials.equals(titleMaterialsExpected));
    }
    public void checkListOfFilesIsDisplayedOnViewPage(List<String> titleFilesExpected) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".field__items .content a")));
        List<WebElement> files = webDriver.findElements(By.cssSelector(".field__items .content a"));
        List<String> titleFiles = new ArrayList<String>();
        for (WebElement file : files) {
            String titleMaterial = file.getText();
            titleFiles.add(titleMaterial);
        }
        if (titleFiles.size() == 0) {
            Assert.assertTrue("List of materials is empty.", false);
        }
        Assert.assertTrue("The materials are displayed on View page.", titleFiles.equals(titleFilesExpected));
    }
    public void checkListOfLinksIsDisplayedOnViewPage(List<String> titleLinksExpected) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("[class*='field-links'] .field__items a")));
        List<WebElement> links = webDriver.findElements(By.cssSelector("[class*='field-links'] .field__items a"));
        List<String> titleLinks = new ArrayList<String>();
        for (WebElement link : links) {
            String titleLink = link.getText();
            titleLinks.add(titleLink);
        }
        if (titleLinks.size() == 0) {
            Assert.assertTrue("List of materials is empty.", false);
        }
        Assert.assertTrue("The materials are displayed on View page.", titleLinks.equals(titleLinksExpected));
    }
    public void checkUserIsInGroup(List<String> user) throws Exception {
        GroupTabGroupPage groupTabGroupPage = new GroupTabGroupPage(webDriver, pageLocation);
        groupTabGroupPage.clickLinkPeople();
        waitUntilPageIsOpenedWithTitle("People");
        String userXpath = "//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']"; // xpath for the username in 'People in group' table
        Assert.assertTrue("The user name is not displayed in 'People in Group' table.", webDriver.findElement(By.xpath(userXpath)).isDisplayed());
    }
    public void checkUserIsNotShownInGroupTable(List<String> user) throws Exception {
        GroupTabGroupPage groupTabGroupPage = new GroupTabGroupPage(webDriver, pageLocation);
        groupTabGroupPage.clickLinkPeople();
        waitUntilPageIsOpenedWithTitle("People");
        List<WebElement> users = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']"));
        Assert.assertTrue("The user name is displayed in 'People in Group' table.", users.size() == 0);
    }





}
