package com.inceptum.cloneContent;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.inceptum.pages.BasePage;
import com.inceptum.pages.CloneOfGroupPage;
import com.inceptum.pages.EditImagePage;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.EditPeerAssignmentPage;
import com.inceptum.pages.EditPresentationPage;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.EditVideoPage;
import com.inceptum.pages.GroupTabClassPage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.PeopleInGroupForClassPage;
import com.inceptum.pages.PeopleInGroupForGroupPage;
import com.inceptum.pages.ViewGroupPage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.ViewPeerAssignmentPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;

/**
 * Created by Olga on 24.09.2015.
 */
public class CloneContentTest extends CloneBaseTest {

    /* ----- CONSTANTS ----- */

//    protected final List<String> administrator = Arrays.asList("administrator", "administrator");



    /* ----- Tests ----- */

    @Test
    public void testCloneEmptyClass() throws Exception {
        // Add a new Class
        addClass(className);
        // Clone the Class from its overview page
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 0);
        // Check that original Class still exists
        checkClassExistsInCourseContentsList(className);
    }

    @Test
    public void testAddContentToClonedClass() throws Exception {
        // Add a new Class
        addClass(className);
        // Clone the Class from its overview page
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 0);
        // Add Live session, check the Class is displayed in 'Groups audience'
        addLiveSessionToClass();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.checkProperClassInGroupAudience("Clone of " + className);
    }

    @Test
    public void testCloneClassWithContentCourseComponents() throws Exception {
        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add 5 different Course components to the Class
        String titleInfoPage = addInfoPageToClass(); // Info page
        webDriver.get(classOverviewPageUrl);
        String titleLiveSession = addLiveSessionToClass(); // Live session
        webDriver.get(classOverviewPageUrl);
        String titleMediaPage = addMediaPageToClass(); // Media page
        webDriver.get(classOverviewPageUrl);
        String titleTask = addTaskToClass(); // Task
        webDriver.get(classOverviewPageUrl);
        String titleTheory = addTheoryToClass(); // Theory
        webDriver.get(classOverviewPageUrl);

        // Clone the Class from its overview page
        String cloneOfClassOverviewPageUrl = cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 5);

        // Check that the new Class (clone) is displayed on Edit Course component pages
        checkClassFromOverviewPage(titleInfoPage, cloneOfClassName); // Info page
        webDriver.get(cloneOfClassOverviewPageUrl);
        checkClassFromOverviewPage(titleLiveSession, cloneOfClassName); // Live session
        webDriver.get(cloneOfClassOverviewPageUrl);
        checkClassFromOverviewPage(titleMediaPage, cloneOfClassName); // Media page
        webDriver.get(cloneOfClassOverviewPageUrl);
        checkClassFromOverviewPage(titleTask, cloneOfClassName); // Task
        webDriver.get(cloneOfClassOverviewPageUrl);
        checkClassFromOverviewPage(titleTheory, cloneOfClassName); // Theory
        // Check that the Course components are still displayed on original Class page
        webDriver.get(classOverviewPageUrl);
        checkItemIsShownOnOverviewClassPage(titleInfoPage);
        checkItemIsShownOnOverviewClassPage(titleLiveSession);
        checkItemIsShownOnOverviewClassPage(titleMediaPage);
        checkItemIsShownOnOverviewClassPage(titleTask);
        checkItemIsShownOnOverviewClassPage(titleTheory);
        // Check that each of the Course component is displayed twice in Content table
        openContentPage();
        checkItemIsShownInContentTable(titleInfoPage, 2);
        checkItemIsShownInContentTable(titleLiveSession, 2);
        checkItemIsShownInContentTable(titleMediaPage, 2);
        checkItemIsShownInContentTable(titleTask, 2);
        checkItemIsShownInContentTable(titleTheory, 2);
    }

    @Test
    public void testCloneClassWithMembers() throws Exception {
        // Add a new user
        addAuthenticatedUser(student);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add the user to the Class
        addUserToClass(student, classOverviewPageUrl);

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        String cloneOfClassOverviewPageUrl = cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 0);

        // Check that the user is not displayed in group table of cloneClass
        checkNumberOfMembersInClass(cloneOfClassOverviewPageUrl, 1); // only 1 member is shown
        checkUserIsDisplayedInPeopleInGroupTable(admin); // it's admin
        // Check that the user is still shown in group table of original Class
        checkNumberOfMembersInClass(classOverviewPageUrl, 2);
        checkUserIsDisplayedInPeopleInGroupTable(student);
    }

    @Test
    public void testCloneClassWithGroup() throws Exception {
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a group to the Class
        createGroup(groupName, className);

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 0);

        // Check only one group with such title is displayed in Content table
        openContentPage();
        checkItemIsShownInContentTable(groupName, 1);
        // Check that original Class is displayed in 'Groups audience' of the Group
        clickEditLinkInContentTable(groupName);
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.checkProperClassInGroupAudience(className);
    }

    @Test
    public void testCloneClassWithContentCourseMaterials() throws Exception {
        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add 3 different Course materials to the Class
        String titleImage = addImageToClass(); // Image
        webDriver.get(classOverviewPageUrl);
        String titlePresentation = addPresentationToClass(); // Presentation
        webDriver.get(classOverviewPageUrl);
        String titleVideo = addVideoToClass(); // Video
        webDriver.get(classOverviewPageUrl);

        // Clone the Class from its overview page
        cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 3);

        // Check that each of the Course material is displayed twice in Content table
        openContentPage();
        checkItemIsShownInContentTable(titleImage, 2);
        checkItemIsShownInContentTable(titlePresentation, 2);
        checkItemIsShownInContentTable(titleVideo, 2);
        // Click the 1st video 'edit' link, check 'Clone of class' is shown in 'Groups audience'
        clickEditLinkInContentTable(titleVideo);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.checkProperClassInGroupAudience(cloneOfClassName);
    }

    @Test
    public void testCloneClassWithPA() throws Exception {
        checkModulesAreEnabledForPA();

        // Add a new user
        addAuthenticatedUser(student);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a PA to the Class
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath);

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        String cloneOfClassOverviewPageUrl = cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);
        checkDraftPA();

        // Add the user to the clone of Class
        addUserToClass(student, cloneOfClassOverviewPageUrl);
        // Open overview page of cloneClass as user
        webDriver.get(cloneOfClassOverviewPageUrl);
        masqueradeAsUser(student);
        checkNoItemOnOverviewClassPage(); // empty node list is displayed for the user
        switchBackFromMasqueradingAs(student, admin);
        // Open View PA page as the user
        openViewNodePageFromClassOverview(titlePA);
        masqueradeAsUser(student);
        String message = webDriver.findElement(By.cssSelector(".l-content-inner p")).getText();
        Assert.assertTrue("The message isn't equal to expected message.",
                message.equals("You are not authorized to access this page."));
        switchBackFromMasqueradingAs(student, admin);
        // Change PA status to 'Published'
        selectStatusOfPA(statusPublished);
        // Open View PA page as the user again
        masqueradeAsUser(student);
        checkViewMaterialPageTitleIs(titlePA);
        switchBackFromMasqueradingAs(student, admin);
        // Check clone of Class is shown in 'Groups audience' of the PA
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.checkProperClassInGroupAudience(cloneOfClassName);
    }

    @Test // PILOT-1580 (PA can't be deleted from Content table) !!!!!!!!!!
    public void testCloneClassWithPeerSubmission() throws Exception {
        checkModulesAreEnabledForPA();
        deleteContentWithTitleFromTable(titlePA);
        String titleSubmission = "Submission (" + titlePA + ")";
        deleteContentWithTitleFromTable(titleSubmission);

        // Add a new user
        addAuthenticatedUser(student);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add the user to the Class
        addUserToClass(student, classOverviewPageUrl);
        // Add a PA to the Class
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath);
        // Create draft submission by the user
        masqueradeAsUser(student);
        checkViewMaterialPageTitleIs(titlePA);
        createDraftSubmission(student);
        switchBackFromMasqueradingAs(student, admin);

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);
        checkDraftPA(); // draft status is selected

        // Check that the submission hasn't been cloned
        openContentPage();
        checkItemIsShownInContentTable(titlePA, 2); // two PA
        checkItemIsShownInContentTable(titleSubmission, 1); // one submission
    }

    @Test // PILOT-1577 (Wrong number of entities) !!!!!!!!!!!!!
    public void testCloneClassWithLiveSessionContainsImage() throws Exception {
        deleteContentWithTitleFromTable(imageTitle);

        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add a LiveSession to the Class
        String titleLiveSession = addLiveSessionToClass();
        // Add an Image with 'add' icon to the LS
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        addImageFromEditPageWithIcon(editLiveSessionPage.getIconAddImage(), editLiveSessionPage.getFieldImagesCourseMaterial());
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        // The Image is shown on a View LS page
        Assert.assertEquals(imageTitle, viewLiveSessionPage.getFieldMaterial().getText());

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 2); // PILOT-1577 !!!!!!!!

        // Check that LS and Image titles repeated twice in the table
        openContentPage();
        checkItemIsShownInContentTable(titleLiveSession, 2);
        checkItemIsShownInContentTable(imageTitle, 2);
        // Open Edit Image page to check Class
        clickEditLinkInContentTable(imageTitle);
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.checkProperClassInGroupAudience(cloneOfClassName);
    }

    @Test // PILOT-1577 (Wrong number of entities) !!!!!!!!!!!!!
    public void testCloneClassWithTaskContainsPresentation() throws Exception {
        deleteContentWithTitleFromTable(presentationTitle);

        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add a Task to the Class
        String titleTask = addTaskToClass();
        // Add a Presentation with 'add' icon to the Task
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        addPresentationFromEditPageWithIcon(editTaskPage.getIconAddPresentation(),
                editTaskPage.getFieldPresentationsTaskResorses());
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        // Added Presentation is shown on a View Task page
        Assert.assertEquals(presentationTitle, viewTaskPage.getFieldMaterial().getText());

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 2); // PILOT-1577 !!!!!!!!

        // Check that Task and Presentation titles repeated twice in the table
        openContentPage();
        checkItemIsShownInContentTable(titleTask, 2);
        checkItemIsShownInContentTable(presentationTitle, 2);
        // Open Edit Presentation page to check Class
        clickEditLinkInContentTable(presentationTitle);
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.checkProperClassInGroupAudience(cloneOfClassName);
    }

    @Test  // PILOT-1577 !!!!!!!!
    public void testCloneClassWithTheoryContainsVideo() throws Exception {
        deleteContentWithTitleFromTable(videoTitle);

        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add a Theory to the Class
        String titleTheory = addTheoryToClass();
        // Add a Video (YouTube) with 'add' icon to the Theory
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabAdditionalMaterial();
        addVideoFromEditPageWithIcon(editTheoryPage.getIconAddVideoAdditional(), editTheoryPage.getFieldVideosAdditionalMaterial(), urlYouTube);
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");
        // Added Video to the Theory is on a View page
        Assert.assertEquals(videoTitle, viewTheoryPage.getFieldMaterial().getText());

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 2); // PILOT-1577 !!!!!!!!

        // Check that Theory and Video titles repeated twice in the table
        openContentPage();
        checkItemIsShownInContentTable(titleTheory, 2);
        checkItemIsShownInContentTable(videoTitle, 2);
        // Open Edit Video page to check Class
        clickEditLinkInContentTable(videoTitle);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.checkProperClassInGroupAudience(cloneOfClassName);
    }

    @Test  // PILOT-1577 !!!!!!!!
    public void testCloneClassWithMediaPageContainsVideo() throws Exception {
        deleteContentWithTitleFromTable(videoTitle);

        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add a MediaPage to the Class
        String titleMediaPage = addMediaPageToClass();
        // Add a Video (Vimeo) with 'add' icon to the MediaPage
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial();
        addVideoFromEditPageWithIcon(editMediaPage.getIconAddVideo(), editMediaPage.getFieldVideosCourseMaterial(), urlVimeo);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        assertMessage(viewMediaPage.getMessage(), "messages messages--status", ".*Media page .+ has been updated.");
        // Added Video is shown on a View Media page
        viewMediaPage.clickLinkLinks(); // open Links popup
        Assert.assertEquals(videoTitle, viewMediaPage.getFieldMaterial().getText());

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 2); // PILOT-1577 !!!!!!!!

        // Check that Media page and Video titles repeated twice in the table
        openContentPage();
        checkItemIsShownInContentTable(titleMediaPage, 2);
        checkItemIsShownInContentTable(videoTitle, 2);
        // Open Edit Video page to check Class
        clickEditLinkInContentTable(videoTitle);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.checkProperClassInGroupAudience(cloneOfClassName);
    }

    @Test
    public void testCloneClassWithDownloadableFile() throws Exception {
        // Add a new user
        addAuthenticatedUser(student);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a LiveSession to the Class
        String titleLiveSession = addLiveSessionToClass();
        // Add a downloadable file to the LS
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        editLiveSessionPage.fillFieldDownloadsTitle(fileTitle);
        editLiveSessionPage.uploadFileToDownloads(docxFilePath);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        Assert.assertEquals(fileTitle, viewLiveSessionPage.getLinkDownloadedCourseMaterial().getText());

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        String cloneOfClassOverviewPageUrl = cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);
        // Add the user to the cloneClass
        addUserToClass(student, cloneOfClassOverviewPageUrl);

        // Check that the user can download the file
        webDriver.get(cloneOfClassOverviewPageUrl);
        waitForPageToLoad();
        masqueradeAsUser(student);
        openViewNodePageFromClassOverview(titleLiveSession);
        viewLiveSessionPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
        switchBackFromMasqueradingAs(student, admin);
    }

    @Test
    public void testCloneClassWithLink() throws Exception {
        // Add a new user
        addAuthenticatedUser(student);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a Task to the Class
        String titleTask = addTaskToClass();
        // Add a link to the Task
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        editTaskPage.fillFieldLinksTitle(linkTitle);
        editTaskPage.fillFieldLinksUrl(linkUrl);
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        Assert.assertEquals(linkTitle, viewTaskPage.getLinkExternalTaskResources().getText());

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        String cloneOfClassOverviewPageUrl = cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);
        // Add the user to the cloneClass
        addUserToClass(student, cloneOfClassOverviewPageUrl);

        // Check that the user can visit link
        webDriver.get(cloneOfClassOverviewPageUrl);
        waitForPageToLoad();
        masqueradeAsUser(student);
        openViewNodePageFromClassOverview(titleTask);
        WebElement link = getLinkByName(linkTitle);
        viewMaterialInAnotherBrowserTab(link);
        switchBackFromMasqueradingAs(student, admin);
    }

    @Test
    public void testCloneClassWithTimeInformation() throws Exception {
        // Add a new Class
        String classOverviewPageUrl = addClass(className); // get url of its overview page
        // Add a LiveSession to the Class
        String titleLiveSession = addLiveSessionToClass();
        // Add Date/Time start/end values
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.selectCheckboxUseStartAndEndDate();
        editLiveSessionPage.selectCheckboxShowTime();
        String date = selectCurrentDate(editLiveSessionPage.getFieldStartDate());
        String startTime = enterStartTime(editLiveSessionPage.getFieldStartTime(), 0);
        selectCurrentDate(editLiveSessionPage.getFieldEndDate());
        String endTime = enterEndTime(editLiveSessionPage.getFieldEndTime(), 1);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        // Check that Date/Time information is displayed correctly on View page
        String dateTimeInfo = viewLiveSessionPage.getFieldDate().getText();
        Assert.assertTrue("Date/Time information doesn't contain full data or incorrect.", dateTimeInfo.contains(date)
                && dateTimeInfo.contains(startTime) && dateTimeInfo.contains(endTime));

        // Clone the Class
        webDriver.get(classOverviewPageUrl);
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);

        // Open cloned LS View page, check Date/Time info is displayed correctly
        openViewNodePageFromClassOverview(titleLiveSession);
        String dateTimeInfoActual = viewLiveSessionPage.getFieldDate().getText();
        Assert.assertEquals("Actual Date/Time information isn't equal to expected.", dateTimeInfo, dateTimeInfoActual);
    }

    @Test
    public void testCloningByAdministrator() throws Exception {
        // Add a user with 'administrator' site role
        addUserWithSiteRole(administrator, roleAdministrator);
        // Add a new Class
        addClass(className);

        // Masquerade as the user, check that he can clone the Class
        masqueradeAsUser(administrator);
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.clickButtonClassMenu(); // class menu (top of the page)
        assertClassIsSelected(className);
        cloneClass(className);
        String cloneOfClassName = "Clone of " + className; // get the new Class name
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 0);
        switchBackFromMasqueradingAs(administrator, admin);

        // Check original Class and clone of Class are displayed in the table
        openContentPage();
        checkItemIsShownInContentTable(className, 1);
        checkItemIsShownInContentTable(cloneOfClassName, 1);
    }

    @Test
    public void testCloningByMasterEditor() throws Exception {
        // Add a user with 'master editor' site role
        addUserWithSiteRole(masterEditor, roleMasterEditor);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a Theory to the Class
        String titleTheory = addTheoryToClass();

        // Masquerade as the user, check that he can clone the Class
        webDriver.get(classOverviewPageUrl);
        waitForPageToLoad();
        masqueradeAsUser(masterEditor);
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.clickButtonClassMenu(); // class menu (top of the page)
        assertClassIsSelected(className);
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);
        switchBackFromMasqueradingAs(masterEditor, admin);

        // Check that the Theory title repeated twice in Content table
        openContentPage();
        checkItemIsShownInContentTable(titleTheory, 2);
    }

    @Test  // PILOT-1577 !!!!!!!! It is not an issue. It is normal behavior (Resolution)
    public void testCloningByClassManager() throws Exception {
        deleteContentWithTitleFromTable(videoTitle);

        // Add a user with 'class manager' site role
        addUserWithSiteRole(classManager, roleClassManager);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a MediaPage to the Class
        String titleMediaPage = addMediaPageToClass();
        // Add a Video (YouTube) with 'add' icon to the MediaPage
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial();
        addVideoFromEditPageWithIcon(editMediaPage.getIconAddVideo(), editMediaPage.getFieldVideosCourseMaterial(), urlYouTube);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        assertMessage(viewMediaPage.getMessage(), "messages messages--status", ".*Media page .+ has been updated.");
        // The Video link is displayed on View Media page
        viewMediaPage.clickLinkLinks(); // open Links popup
        Assert.assertEquals(videoTitle, viewMediaPage.getFieldMaterial().getText());

        // Masquerade as the user, check that he can clone the Class
        webDriver.get(classOverviewPageUrl);
        waitForPageToLoad();
        masqueradeAsUser(classManager);
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.clickButtonClassMenu(); // class menu (top of the page)
        assertClassIsSelected(className);
        cloneClass(className);
        // Check number of entities
        checkNumberOfEntitiesInMessage(className, 1);  // PILOT-1577 !!!!!!!! It is not an issue. It is normal behavior (Resolution)
        switchBackFromMasqueradingAs(classManager, admin);

        // Check that the MediaPage title and Video title repeated twice in Content table
        openContentPage();
        checkItemIsShownInContentTable(titleMediaPage, 2);
        checkItemIsShownInContentTable(videoTitle, 2);
    }

    @Test
    public void testCloningContentByClassEditor() throws Exception {
        // Add a new user
        addAuthenticatedUser(contentEditor);
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add an Image to the Class
        String titleImage = addImageToClass();
        // Create Task, add the Image to 'Task resources'
        webDriver.get(classOverviewPageUrl);
        String titleTask = addTaskToClass();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        autoSelectItemFromList(editTaskPage.getFieldImagesTaskResources(), titleImage);
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        Assert.assertEquals(titleImage, viewTaskPage.getFieldMaterial().getText());
        // Add the user to Class group, select 'content editor' role
        webDriver.get(classOverviewPageUrl);
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        GroupTabClassPage groupTabClassPage = overviewPage.clickLinkGroup();
        PeopleInGroupForClassPage peopleInGroupForClassPage = groupTabClassPage.clickLinkAddPeople();
        // Add the user to the Class with selected role
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), contentEditor.get(0));
        selectRole(roleContentEditor);
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(peopleInGroupForClassPage.getMessage(),
                ".*" + contentEditor.get(0) + ".*has been added to the group " + className + ".");

        // Masquerade as contentEditor, open View Task page
        webDriver.get(classOverviewPageUrl);
        masqueradeAsUser(contentEditor);
        assertClassIsSelectedInClassMenu(className); // the Class is selected in class menu
        openViewNodePageFromClassOverview(titleTask);
        // Check that only the Class and Example class are available for contentEditor
        assertItemIsInTheList(viewTaskPage.getFieldCopyPage(), className);
        assertItemIsInTheList(viewTaskPage.getFieldCopyPage(), exampleClass);
        int options = countOptionsInList(viewTaskPage.getFieldCopyPage());
        Assert.assertTrue("Another number of options is shown in 'Copy page' list.", options == 3); // the number includes 'Copy page' link
        // Click 'Copy page', select Example class
        copyPageToClass(exampleClass);

        // Open Content table as admin
        switchBackFromMasqueradingAs(contentEditor, admin);
        openContentPage();
        checkItemIsShownInContentTable(titleTask, 1); // the titleTask is displayed in the table
        String titleCloneOfTask = "Clone of " + titleTask;
        checkItemIsShownInContentTable(titleCloneOfTask, 1); // title of Clone of the Task is displayed in the table
        checkItemIsShownInContentTable(titleImage, 2); // titleImage is displayed twice

        // Check that Example class is selected in CloneOfClass 'Groups audience'
        clickEditLinkInContentTable(titleCloneOfTask);
        editTaskPage.checkProperClassInGroupAudience(exampleClass);
        // Check that the original Class is selected in Task 'Groups audience'
        openContentPage();
        clickEditLinkInContentTable(titleTask);
        editTaskPage.checkProperClassInGroupAudience(className);
        // Check that Example class is selected in the cloned Image 'Groups audience'
        openContentPage();
        clickEditLinkInContentTable(titleImage);
        editTaskPage.checkProperClassInGroupAudience(exampleClass);
        // Check that the original Class is selected in Image 'Groups audience'
        openContentPage();
        clickEditLinkInContentTableFor2ndLink(titleImage);
        editTaskPage.checkProperClassInGroupAudience(className);
    }

    @Test
    public void testCopyCourseComponentsToAnotherClass() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);

        // Add 5 different Course components to the Class
        String titleInfoPage = addInfoPageToClass(); // Info page
        webDriver.get(classOverviewPageUrl);
        String titleLiveSession = addLiveSessionToClass(); // Live session
        webDriver.get(classOverviewPageUrl);
        String titleMediaPage = addMediaPageToClass(); // Media page
        webDriver.get(classOverviewPageUrl);
        String titleTask = addTaskToClass(); // Task
        webDriver.get(classOverviewPageUrl);
        String titleTheory = addTheoryToClass(); // Theory
        webDriver.get(classOverviewPageUrl);
        // Check that all 5 nodes are displayed on the Class overview page
        checkNumberOfNodesOnOverviewPage(5);

        // Copy each Course component to Class2
        openViewNodePageFromClassOverview(titleInfoPage);
        copyPageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleLiveSession);
        copyPageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleMediaPage);
        copyPageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleTask);
        copyPageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleTheory);
        copyPageToClass(className2);

        // Check that 5 Course components are still displayed on the Class overview page
        webDriver.get(classOverviewPageUrl);
        checkNumberOfNodesOnOverviewPage(5);
        // Click Theory node to check class in 'Groups audience'
        openViewNodePageFromClassOverview(titleTheory);
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.checkProperClassInGroupAudience(className);
        // Open Class2 overview page, check that 5 nodes are displayed there
        webDriver.get(classOverviewPageUrl2);
        checkNumberOfNodesOnOverviewPage(5);
        // Open Clone of Theory Edit page, check that Class2 is selected in 'Groups audience'
        openViewNodePageFromClassOverview("Clone of " + titleTheory);
        openEditPage();
        editTheoryPage.checkProperClassInGroupAudience(className2);
    }

    @Test
    public void testCopyCourseMaterialsToAnotherClass() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);

        // Add 3 different materials to the Class
        String titleVideo = addVideoToClass(); // Video
        webDriver.get(classOverviewPageUrl);
        String titlePresentation = addPresentationToClass(); // Presentation
        webDriver.get(classOverviewPageUrl);
        String titleImage = addImageToClass(); // Image
        webDriver.get(classOverviewPageUrl);

        // Copy each material to Class2
        openContentPage();
        openViewMaterialPageFromContentTable(titleVideo);
        copyPageToClass(className2);
        openContentPage();
        openViewMaterialPageFromContentTable(titlePresentation);
        copyPageToClass(className2);
        openContentPage();
        openViewMaterialPageFromContentTable(titleImage);
        copyPageToClass(className2);

        // Check that clones of each material are displayed in Content table
        openContentPage();
        String titleVideoClone = "Clone of " + titleVideo;
        String titlePresentationClone = "Clone of " + titlePresentation;
        String titleImageClone = "Clone of " + titleImage;
        checkItemIsShownInContentTable(titleVideoClone, 1);
        checkItemIsShownInContentTable(titlePresentationClone, 1);
        checkItemIsShownInContentTable(titleImageClone, 1);

        // Add Live Session to Class2
        webDriver.get(classOverviewPageUrl2);
        addLiveSessionToClass();
        // Add clones of materials to proper fields of 'Course material' tab (thus the materials exist in Class2)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.checkProperClassInGroupAudience(className2);
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldPresentationsCourseMaterial(), titlePresentationClone);
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), titleVideoClone);
        autoSelectItemFromList(editLiveSessionPage.getFieldImagesCourseMaterial(), titleImageClone);
    }

    @Test
    public void testCopyCourseComponentWithCourseMaterials() throws Exception {
        // Add two new Classes
        addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add Theory to Class1
        String titleTheory = addTheoryToClass();
        // Add Presentation, Video, Image to the Theory with 'add' icons (Course materials)
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabCourseMaterial();
        addPresentationFromEditPageWithIcon(editTheoryPage.getIconAddPresentation(),
                editTheoryPage.getFieldPresentationsCourseMaterial()); // Presentation
        addVideoFromEditPageWithIcon(editTheoryPage.getIconAddVideo(), editTheoryPage.getFieldVideosCourseMaterial(), urlYouTube); // Video
        addImageFromEditPageWithIcon(editTheoryPage.getIconAddImage(), editTheoryPage.getFieldImagesCourseMaterial()); // Image
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory .+ has been updated.");
        // Check that three materials are displayed on View Theory page
        List<String> titleMaterialsExpected = Arrays.asList(presentationTitle, videoTitle, imageTitle);
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);

        // Copy the Theory to Class2
        copyPageToClass(className2);
        // Check title of cloned Theory
        String titleCloneTheory = "Clone of " + titleTheory;
        checkViewMaterialPageTitleIs(titleCloneTheory);
        // Check that all materials are displayed on its View page
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);
        // Check that Class2 is displayed in 'Group audience' on Edit material page
        clickMaterialLink(presentationTitle);
        openEditPageModalContent();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.checkProperClassInGroupAudience(className2);
        // Check that Class1 is still shown for material in original Theory
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleTheory);
        clickMaterialLink(presentationTitle);
        openEditPageModalContent();
        editPresentationPage.checkProperClassInGroupAudience(className);
    }

    @Test
    public void testCopyCourseComponentWithAdditionalMaterials() throws Exception {
        // Add two new Classes
        addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add Task to Class1
        String titleTask = addTaskToClass();
        // Add Presentation, Video, Image to the Task with 'add' icons (Additional resources)
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabAdditionalResources();
        addPresentationFromEditPageWithIcon(editTaskPage.getIconAddPresentationAdditional(),
                editTaskPage.getFieldPresentationsAdditionalResourses()); // Presentation
        addVideoFromEditPageWithIcon(editTaskPage.getIconAddVideoAdditional(),
                editTaskPage.getFieldVideosAdditionalResourses(), urlYouTube); // Video
        addImageFromEditPageWithIcon(editTaskPage.getIconAddImageAdditional(),
                editTaskPage.getFieldImagesAdditionalResources()); // Image
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task .+ has been updated.");
        // Check that three materials are displayed on View Task page
        List<String> titleMaterialsExpected = Arrays.asList(presentationTitle, videoTitle, imageTitle);
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);

        // Copy the Task to Class2
        copyPageToClass(className2);
        // Check title of cloned Task
        String titleCloneTask = "Clone of " + titleTask;
        checkViewMaterialPageTitleIs(titleCloneTask);
        // Check that all materials are displayed on its View page
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);
        // Check that Class2 is displayed in 'Group audience' on Edit material page
        clickMaterialLink(presentationTitle);
        openEditPageModalContent();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.checkProperClassInGroupAudience(className2);
        // Check that Class1 is still shown for material in original Task
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleTask);
        clickMaterialLink(presentationTitle);
        openEditPageModalContent();
        editPresentationPage.checkProperClassInGroupAudience(className);
    }

    @Test
    public void testCopyCourseComponentWithDownloadableFile() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add a new user, add him to both Classes
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl2);
        addUserToClass(student, classOverviewPageUrl);
        // Add Media page to Class1
        webDriver.get(classOverviewPageUrl);
        String titleMediaPage = addMediaPageToClass();
        // Add downloadable files to Course and Additional materials
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial(); // file in Course materials
        editMediaPage.fillFieldDownloadsTitle(fileTitle);
        editMediaPage.uploadFileToDownloads(docxFilePath);
        editMediaPage.switchToTabAdditionalMaterial(); // file in Additional materials
        editMediaPage.fillFieldDownloadsTitleAdditional(fileTitle2);
        editMediaPage.uploadFileToDownloadsAdditional(xlsxFilePath);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        // Check that two files are displayed on Media page Links overlay
        viewMediaPage.clickLinkLinks(); // open Links popup
        List<String> titleFilesExpected = Arrays.asList(fileTitle, fileTitle2);
        checkListOfFilesIsDisplayedOnViewPage(titleFilesExpected);

        // Copy the Media page to Class2
        copyPageToClass(className2);
        // Check title of cloned page
        String titleCloneMediaPage = "Clone of " + titleMediaPage;
        checkViewMaterialPageTitleIs(titleCloneMediaPage);

        // Masquerade as the user, check that files are downloadable
        masqueradeAsUser(student);
        viewMediaPage.clickLinkLinks();
        clickLinkDownloadedCourseMaterialByName(fileTitle);
        waitUntilFileDownloaded();
        clickLinkDownloadedCourseMaterialByName(fileTitle2);
        waitUntilFileDownloaded();
        // Open original Media page, check that the files are still displayed on Links overlay
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleMediaPage);
        viewMediaPage.clickLinkLinks();
        checkListOfFilesIsDisplayedOnViewPage(titleFilesExpected);
        // Switch back from masquerading
        switchBackFromMasqueradingAs(student, admin);
    }

    @Test
    public void testCopyCourseComponentWithLinks() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add a new user, add him to both Classes
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl2);
        addUserToClass(student, classOverviewPageUrl);
        // Add LiveSession to Class1
        webDriver.get(classOverviewPageUrl);
        String titleLiveSession = addLiveSessionToClass();
        // Add links to Course and Additional materials
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial(); // link in Course materials
        editLiveSessionPage.fillFieldLinksTitle(linkTitle);
        editLiveSessionPage.fillFieldLinksUrl(linkUrl);
        editLiveSessionPage.switchToTabAdditionalMaterial(); // link in Additional materials
        editLiveSessionPage.fillFieldLinksTitleAdditional(linkTitle2);
        editLiveSessionPage.fillFieldLinksUrlAdditional(linkUrl2);
        editLiveSessionPage.clickButtonSave();
        // Check that two links are displayed on View LS page
        List<String> titleLinksExpected = Arrays.asList(linkTitle, linkTitle2);
        checkListOfLinksIsDisplayedOnViewPage(titleLinksExpected);

        // Copy the LiveSession to Class2
        copyPageToClass(className2);
        // Check title of cloned page
        String titleCloneLiveSession = "Clone of " + titleLiveSession;
        checkViewMaterialPageTitleIs(titleCloneLiveSession);

        // Masquerade as the user, check that links can be visited
        masqueradeAsUser(student);
        WebElement link = getLinkByName(linkTitle);
        viewMaterialInAnotherBrowserTab(link);
        WebElement link2 = getLinkByName(linkTitle2);
        viewMaterialInAnotherBrowserTab(link2);
        // Open original LiveSession, check that the links are still displayed on LS View page
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleLiveSession);
        checkListOfLinksIsDisplayedOnViewPage(titleLinksExpected);
        // Switch back from masquerading
        switchBackFromMasqueradingAs(student, admin);
    }

    @Test
    public void testCopyGroup() throws Exception {
        // Add a new Class
        String classOverviewPageUrl = addClass(className);
        // Add a new user, add him to the Class
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl);
        // Add a group to the Class
        webDriver.get(classOverviewPageUrl);
        createGroup(groupName, className);
        String urlViewGroupPage = webDriver.getCurrentUrl(); // get url of View Group page
        // Add the user to the Group
        addUserToGroup(student, groupName);
        // Check the user name is displayed on Group overview page
        PeopleInGroupForGroupPage peopleInGroupForGroupPage = new PeopleInGroupForGroupPage(webDriver, pageLocation);
        peopleInGroupForGroupPage.clickLinkGroup();
        checkUserIsInGroup(student);

        // Clone the Group
        webDriver.get(urlViewGroupPage);
        ViewGroupPage viewGroupPage = new ViewGroupPage(webDriver, pageLocation);
        CloneOfGroupPage cloneOfGroupPage = viewGroupPage.clickLinkCloneContent();
        viewGroupPage = cloneOfGroupPage.clickButtonSave();
        // Check number of entities
        checkNumberOfEntitiesInMessageForGroup(groupName, 0);

        // Check that the user is not displayed in 'People in Group' table for the cloneGroup
        viewGroupPage.clickLinkGroup();
        checkUserIsNotShownInGroupTable(student);
    }

    @Test // PILOT-1580 (PA can't be deleted) !!!!!!!!!!!!!
    public void testCopyPA() throws Exception {
        checkModulesAreEnabledForPA();
        deleteContentWithTitleFromTable(titlePA);
        String titleClonePA = "Clone of " + titlePA;
        deleteContentWithTitleFromTable(titleClonePA);
        String titleSubmission = "Submission (" + titleClonePA + ")";
        deleteContentWithTitleFromTable(titleSubmission);

        // Add two new Classes
        String classOverviewPageUrl = addClass(className);
        String classOverviewPageUrl2 = addClass(className2);
        // Add a new user, add him to both Classes
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl);
        addUserToClass(student, classOverviewPageUrl2);
        // Add a PA to Class1
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath);
        // Check that PA has Published status
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        String statusActual = viewPeerAssignmentPage.getFieldStatus().findElement(By.cssSelector("[selected]")).getText();
        Assert.assertTrue("Status of PA is not Published.", statusActual.equals(statusPublished));

        // Copy the PA to Class2
        copyPageToClass(className2);
        // Check title of cloned page, Publish status
        checkViewMaterialPageTitleIs(titleClonePA);
        statusActual = viewPeerAssignmentPage.getFieldStatus().findElement(By.cssSelector("[selected]")).getText();
        Assert.assertTrue("Status of PA is not Published.", statusActual.equals(statusPublished));
        // Masquerade as the user, create draft submission
        masqueradeAsUser(student);
        createDraftSubmission(student);
        switchBackFromMasqueradingAs(student, admin);
        // Check that original PA, Copy of the PA, Submission of the copy are displayed in Content table
        openContentPage();
        checkItemIsShownInContentTable(titlePA, 1); // original PA
        checkItemIsShownInContentTable(titleClonePA, 1); // clone of PA
        checkItemIsShownInContentTable(titleSubmission, 1); // submission
    }

    @Test
    public void testMoveCourseComponentsToAnotherClass() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);

        // Add 5 different Course components to the Class1
        String titleInfoPage = addInfoPageToClass(); // Info page
        webDriver.get(classOverviewPageUrl);
        String titleLiveSession = addLiveSessionToClass(); // Live session
        webDriver.get(classOverviewPageUrl);
        String titleMediaPage = addMediaPageToClass(); // Media page
        webDriver.get(classOverviewPageUrl);
        String titleTask = addTaskToClass(); // Task
        webDriver.get(classOverviewPageUrl);
        String titleTheory = addTheoryToClass(); // Theory
        webDriver.get(classOverviewPageUrl);
        // Check that all 5 nodes are displayed on the Class overview page
        checkNumberOfNodesOnOverviewPage(5);

        // Move each Course component to Class2
        openViewNodePageFromClassOverview(titleInfoPage);
        movePageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleLiveSession);
        movePageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleMediaPage);
        movePageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleTask);
        movePageToClass(className2);
        webDriver.get(classOverviewPageUrl);
        openViewNodePageFromClassOverview(titleTheory);
        movePageToClass(className2);

        // Check that no Course component is displayed on the Class1 overview page
        webDriver.get(classOverviewPageUrl);
        checkNoItemOnOverviewClassPage();
        // Check that 5 nodes are displayed on the Class2 overview page
        webDriver.get(classOverviewPageUrl2);
        checkNumberOfNodesOnOverviewPage(5);
        // Click Task node to check class in 'Groups audience'
        openViewNodePageFromClassOverview(titleTask);
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.checkProperClassInGroupAudience(className2);
    }

    @Test
    public void testMoveCourseMaterialsToAnotherClass() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);

        // Add 3 different materials to the Class
        String titleVideo = addVideoToClass(); // Video
        webDriver.get(classOverviewPageUrl);
        String titlePresentation = addPresentationToClass(); // Presentation
        webDriver.get(classOverviewPageUrl);
        String titleImage = addImageToClass(); // Image
        webDriver.get(classOverviewPageUrl);

        // Move each material to Class2
        openContentPage();
        openViewMaterialPageFromContentTable(titleVideo);
        movePageToClass(className2);
        openContentPage();
        openViewMaterialPageFromContentTable(titlePresentation);
        movePageToClass(className2);
        openContentPage();
        openViewMaterialPageFromContentTable(titleImage);
        movePageToClass(className2);

        // Check that the materials are displayed in Content table
        openContentPage();
        checkItemIsShownInContentTable(titleVideo, 1);
        checkItemIsShownInContentTable(titlePresentation, 1);
        checkItemIsShownInContentTable(titleImage, 1);

        // Add Live Session to Class2
        webDriver.get(classOverviewPageUrl2);
        addLiveSessionToClass();
        // Add clones of materials to proper fields of 'Course material' tab (thus the materials exist in Class2)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.checkProperClassInGroupAudience(className2);
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldPresentationsCourseMaterial(), titlePresentation);
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), titleVideo);
        autoSelectItemFromList(editLiveSessionPage.getFieldImagesCourseMaterial(), titleImage);
    }

    @Test
    public void testMoveCourseComponentWithCourseMaterials() throws Exception {
        // Add two new Classes
        addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add Theory to Class1
        String titleTheory = addTheoryToClass();
        // Add Presentation, Video, Image to the Theory with 'add' icons (Course materials)
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabCourseMaterial();
        addPresentationFromEditPageWithIcon(editTheoryPage.getIconAddPresentation(),
                editTheoryPage.getFieldPresentationsCourseMaterial()); // Presentation
        addVideoFromEditPageWithIcon(editTheoryPage.getIconAddVideo(), editTheoryPage.getFieldVideosCourseMaterial(), urlYouTube); // Video
        addImageFromEditPageWithIcon(editTheoryPage.getIconAddImage(), editTheoryPage.getFieldImagesCourseMaterial()); // Image
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory .+ has been updated.");
        // Check that three materials are displayed on View Theory page
        List<String> titleMaterialsExpected = Arrays.asList(presentationTitle, videoTitle, imageTitle);
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);

        // Move the Theory to Class2
        movePageToClass(className2);
        // Check title of moved page
        checkViewMaterialPageTitleIs(titleTheory);
        // Check that all materials are displayed on its View page
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);
        // Check that Class2 is displayed in 'Group audience' on Edit material page
        clickMaterialLink(imageTitle);
        openEditPageModalContent();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.checkProperClassInGroupAudience(className2);
        // Check that no node is displayed on Class1 overview page
        webDriver.get(classOverviewPageUrl);
        checkNoItemOnOverviewClassPage();
    }

    @Test
    public void testMoveCourseComponentWithAdditionalMaterials() throws Exception {
        // Add two new Classes
        addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add Task to Class1
        String titleTask = addTaskToClass();
        // Add Presentation, Video, Image to the Task with 'add' icons (Additional resources)
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabAdditionalResources();
        addPresentationFromEditPageWithIcon(editTaskPage.getIconAddPresentationAdditional(),
                editTaskPage.getFieldPresentationsAdditionalResourses()); // Presentation
        addVideoFromEditPageWithIcon(editTaskPage.getIconAddVideoAdditional(),
                editTaskPage.getFieldVideosAdditionalResourses(), urlYouTube); // Video
        addImageFromEditPageWithIcon(editTaskPage.getIconAddImageAdditional(),
                editTaskPage.getFieldImagesAdditionalResources()); // Image
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task .+ has been updated.");
        // Check that three materials are displayed on View Task page
        List<String> titleMaterialsExpected = Arrays.asList(presentationTitle, videoTitle, imageTitle);
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);

        // Move the Task to Class2
        movePageToClass(className2);
        // Check title of moved page
        checkViewMaterialPageTitleIs(titleTask);
        // Check that all materials are displayed on its View page
        checkListOfMaterialsIsDisplayedOnViewPage(titleMaterialsExpected);
        // Check that Class2 is displayed in 'Group audience' on Edit material page
        clickMaterialLink(videoTitle);
        openEditPageModalContent();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.checkProperClassInGroupAudience(className2);
        // Check that no node is displayed on Class1 overview page
        webDriver.get(classOverviewPageUrl);
        checkNoItemOnOverviewClassPage();
    }

    @Test
    public void testMoveCourseComponentWithDownloadableFile() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add a new user, add him to both Classes
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl2);
        addUserToClass(student, classOverviewPageUrl);
        // Add Media page to Class1
        webDriver.get(classOverviewPageUrl);
        String titleMediaPage = addMediaPageToClass();
        // Add downloadable files to Course and Additional materials
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial(); // file in Course materials
        editMediaPage.fillFieldDownloadsTitle(fileTitle);
        editMediaPage.uploadFileToDownloads(docxFilePath);
        editMediaPage.switchToTabAdditionalMaterial(); // file in Additional materials
        editMediaPage.fillFieldDownloadsTitleAdditional(fileTitle2);
        editMediaPage.uploadFileToDownloadsAdditional(xlsxFilePath);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        // Check that two files are displayed on Media page Links overlay
        viewMediaPage.clickLinkLinks(); // open Links popup
        List<String> titleFilesExpected = Arrays.asList(fileTitle, fileTitle2);
        checkListOfFilesIsDisplayedOnViewPage(titleFilesExpected);

        // Move the Media page to Class2
        movePageToClass(className2);
        // Check title of moved page
        checkViewMaterialPageTitleIs(titleMediaPage);

        // Masquerade as the user, check that files are downloadable
        masqueradeAsUser(student);
        viewMediaPage.clickLinkLinks();
        clickLinkDownloadedCourseMaterialByName(fileTitle);
        waitUntilFileDownloaded();
        clickLinkDownloadedCourseMaterialByName(fileTitle2);
        waitUntilFileDownloaded();
        // Open Class1 overview page, check that there are no nodes
        webDriver.get(classOverviewPageUrl);
        checkNoItemOnOverviewClassPage();
        // Switch back from masquerading
        switchBackFromMasqueradingAs(student, admin);
    }

    @Test
    public void testMoveCourseComponentWithLinks() throws Exception {
        // Add two new Classes
        String classOverviewPageUrl2 = addClass(className2);
        String classOverviewPageUrl = addClass(className);
        // Add a new user, add him to both Classes
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl2);
        addUserToClass(student, classOverviewPageUrl);
        // Add LiveSession to Class1
        webDriver.get(classOverviewPageUrl);
        String titleLiveSession = addLiveSessionToClass();
        // Add links to Course and Additional materials
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial(); // link in Course materials
        editLiveSessionPage.fillFieldLinksTitle(linkTitle);
        editLiveSessionPage.fillFieldLinksUrl(linkUrl);
        editLiveSessionPage.switchToTabAdditionalMaterial(); // link in Additional materials
        editLiveSessionPage.fillFieldLinksTitleAdditional(linkTitle2);
        editLiveSessionPage.fillFieldLinksUrlAdditional(linkUrl2);
        editLiveSessionPage.clickButtonSave();
        // Check that two links are displayed on View LS page
        List<String> titleLinksExpected = Arrays.asList(linkTitle, linkTitle2);
        checkListOfLinksIsDisplayedOnViewPage(titleLinksExpected);

        // Move the LiveSession to Class2
        movePageToClass(className2);
        // Check title of moved page
        checkViewMaterialPageTitleIs(titleLiveSession);

        // Masquerade as the user, check that links can be visited
        masqueradeAsUser(student);
        WebElement link = getLinkByName(linkTitle);
        viewMaterialInAnotherBrowserTab(link);
        WebElement link2 = getLinkByName(linkTitle2);
        viewMaterialInAnotherBrowserTab(link2);
        // Open Class1 overview page, check that there are no nodes
        webDriver.get(classOverviewPageUrl);
        checkNoItemOnOverviewClassPage();
        // Switch back from masquerading
        switchBackFromMasqueradingAs(student, admin);
    }

    @Test  // PILOT-1580 (PA can't be deleted) !!!!!!!!!!!!!
    public void testMovePA() throws Exception {
        checkModulesAreEnabledForPA();
        deleteContentWithTitleFromTable(titlePA);
        String titleSubmission = "Submission (" + titlePA + ")";
        deleteContentWithTitleFromTable(titleSubmission);

        // Add two new Classes
        String classOverviewPageUrl = addClass(className);
        String classOverviewPageUrl2 = addClass(className2);
        // Add a new user, add him to both Classes
        addAuthenticatedUser(student);
        addUserToClass(student, classOverviewPageUrl);
        addUserToClass(student, classOverviewPageUrl2);
        // Add a PA to Class1
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath);
        // Check that PA has Published status
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        String statusActual = viewPeerAssignmentPage.getFieldStatus().findElement(By.cssSelector("[selected]")).getText();
        Assert.assertTrue("Status of PA is not Published.", statusActual.equals(statusPublished));

        // Move the PA to Class2
        movePageToClass(className2);
        // Check title of cloned page, Publish status
        checkViewMaterialPageTitleIs(titlePA);
        statusActual = viewPeerAssignmentPage.getFieldStatus().findElement(By.cssSelector("[selected]")).getText();
        Assert.assertTrue("Status of PA is not Published.", statusActual.equals(statusPublished));
        // Masquerade as the user, create draft submission
        masqueradeAsUser(student);
        createDraftSubmission(student);
        // Check that there are no nodes on Class1 overview page
        webDriver.get(classOverviewPageUrl);
        checkNoItemOnOverviewClassPage();
        switchBackFromMasqueradingAs(student, admin);
        // Check that the PA, Submission are displayed in Content table
        openContentPage();
        checkItemIsShownInContentTable(titlePA, 1); // PA
        checkItemIsShownInContentTable(titleSubmission, 1); // submission
    }

    @Test
    public void testVisibilityOfClassInCopyMovePageList() throws Exception {
        // Add a new Class
        addClass(className);
        // Add LiveSession to the Class
        addLiveSessionToClass();
        // Check that the Class name is displayed in 'Copy page' list
        BasePage basePage = new BasePage(webDriver, pageLocation);
        assertItemIsInTheList(basePage.getFieldCopyPage(), className);
        // Check that the Class name is not displayed in 'Move page' list
        assertOptionIsAbsentInTheList(basePage.getFieldMovePage(), className);
    }












}
