package com.inceptum.common;

import java.io.File;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;

import com.inceptum.whoIsWho.WhoIsWhoBaseTest;
import org.apache.log4j.Logger;
import org.junit.*;
import org.junit.rules.MethodRule;
import org.junit.rules.TestWatchman;
import org.junit.runners.model.FrameworkMethod;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.common.base.Predicate;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.AddContentPage;
import com.inceptum.pages.AddUserPage;
import com.inceptum.pages.BasePage;
import com.inceptum.pages.ContentPage;
import com.inceptum.pages.CreateClassPage;
import com.inceptum.pages.CreateGroupPage;
import com.inceptum.pages.CreateNewAccountPage;
import com.inceptum.pages.CreatePeerAssignmentPage;
import com.inceptum.pages.CronPage;
import com.inceptum.pages.DeleteItemConfirmationPage;
import com.inceptum.pages.DeleteUserAccountConfirmationPage;
import com.inceptum.pages.EditClassPage;
import com.inceptum.pages.EditUserAccountPage;
import com.inceptum.pages.GroupTabClassPage;
import com.inceptum.pages.GroupTabGroupPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.ModulesPage;
import com.inceptum.pages.OAuthProvidersPage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.PeopleInGroupForClassPage;
import com.inceptum.pages.PeopleInGroupForGroupPage;
import com.inceptum.pages.PeoplePage;
import com.inceptum.pages.RecentLogMessagesPage;
import com.inceptum.pages.RegisterForCoursePage;
import com.inceptum.pages.ViewGroupPage;
import com.inceptum.pages.ViewImagePage;
import com.inceptum.pages.ViewInfoPage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.ViewPeerAssignmentPage;
import com.inceptum.pages.ViewPresentationPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;
import com.inceptum.pages.WhoIsWhoPage;
import com.inceptum.util.Debug;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * This is the base class for all the various test classes.
 */
public abstract class TestBase {


    /* ----- CONSTANTS ----- */
    private static final String MODULE = TestBase.class.getName();

    /* ----- FIELDS ----- */
    protected static WebDriver webDriver;
    protected static String gridHubUrl;
    protected static String websiteUrl;
    protected static BrowserVo browser;
    protected static PageLocation pageLocation;

    protected static final List<String> admin = Arrays.asList("admin", "admin");
    protected static final String className = "Class1";
    protected final String className2 = "Class2";
    protected final String location = "Some location.";
    protected final String capacity = "100";
    protected static final String exampleClass = "Example class";
    protected final List<String> juryMember = Arrays.asList("juryMember", "juryMember");
    protected final List<String> juryMember2 = Arrays.asList("juryMember2", "juryMember2");
    protected final String roleJuryMember = "jury member";
    protected final List<String> contentEditor = Arrays.asList("contentEditor", "contentEditor");
    protected final String roleContentEditor = "content editor";
    protected final List<String> teacherMember = Arrays.asList("teacherMember", "teacherMember");
    protected final String roleTeacherMember = "teacher member";
    protected final List<String> masterEditor = Arrays.asList("masterEditor", "masterEditor");
    protected final String roleMasterEditor = "master editor";
    protected final List<String> classManager = Arrays.asList("classManager", "classManager");
    protected final String roleClassManager = "class manager";
    protected final List<String> administratorMember = Arrays.asList("administratorMember", "administratorMember");
    protected final List<String> administrator = Arrays.asList("administrator", "administrator");
    protected final String roleAdministratorMember = "administrator member";
    protected final String roleAdministrator = "administrator";
    protected final String roleClassEditor = "class editor";
    protected final String roleClassJury = "class jury";
    protected static final List<String> anonymous = Arrays.asList("Anonymous", "");
    protected static final List<String> student = Arrays.asList("student", "student");
    protected final List<String> student2 = Arrays.asList("student2", "student2");
    protected final List<String> expert = Arrays.asList("expert", "expert");
    protected final String groupName = "Group1";
    protected final String groupName2 = "Group2";
    protected final String linkRegistered = "Registered";
    protected final String linkRegister = "Register";
    protected final String linkViewClass = "View";
    protected final String linkCompleteClass = "Complete";
    protected final String linkRegistrationPending = "Registration pending";
    protected final String image3Path = "src/test/resources/files/image3.jpg";
    protected final String image4Path = "src/test/resources/files/image4.png";
    protected final String urlVimeo = "http://vimeo.com/43714499";
    protected final String youtube = "youtube";
    protected final String vimeo = "vimeo";
    protected final String slideshare = "slideshare";
    protected static final String linkTitle = "Gismeteo";
    protected static final String linkTitle2 = "Wiki";
    protected static final String linkUrl = "http://www.gismeteo.com/";
    protected static final String linkUrl2 = "https://www.wikipedia.org";
    protected final String coaching = "Coaching";
    protected static final String googleServiceAccount = "Google Service Account";
    protected static final String accountName = "GDrive";
    protected static final String accountEmail = "380609569033-e1p3ji8i4qg797j8cutsv19cmo07pir8@developer.gserviceaccount.com";
    protected static final String privateKey = "src/test/resources/files/key(2).p12";
    protected final String titlePA = "PeerAssignment1";
    protected final String summaryPA = "SummaryForPeerAssignment1";
    protected final String titleTemplateSolution = "TemplateSolution1";
    protected final String assignmentCase1 = "AssignmentCase1.";
    protected static final String imageTitle = "Image1";
    protected static final String image1Path = "src/test/resources/files/image1.jpeg";
    protected static final String presentationTitle = "Presentation1";
    protected final String urlSlideshare = "http://www.slideshare.net/tutorialsruby/csstutorialboxmodel"; // !!! TRIMMED URL
    protected static final String videoTitle = "Video1";
    protected final String urlYouTube = "http://www.youtube.com/watch?v=Wz2klMXDqF4";
    protected static final String fileTitle = "File1";
    protected static final String fileTitle2 = "File2";
    protected static final String docxFilePath = "src/test/resources/files/hello.docx";
    protected static final String xlsxFilePath = "src/test/resources/files/numbers.xlsx";
    protected final String whatDoYouNeedToDoText = "What do you need to do text.";
    protected final String videoSummary = "Video Summary."; //Contextualinformation
    protected final String contextualInformation = "Contextual Information.";
    protected static final List<String> WhoIsWhoFiveTabs = Arrays.asList("Students","Admins","Editors","Juries", "Teachers");
    protected static final List<String> WhoIsWhoThreeTabs = Arrays.asList("Students","Juries", "Teachers");


    protected boolean GDrive;
    protected boolean approval; // false

    public static String currentTest;


    /* ----- METHODS ----- */

    @Rule
    public MethodRule watchman = new TestWatchman() {
       public void starting(FrameworkMethod method) {
          currentTest = method.getName();
       }
    };

    /*@BeforeClass
    public static void beforeBaseClass() throws Exception {
        websiteUrl = System.getProperty("site.url");

        // Check that site URL is specified
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu).");
        }

        // Check that site URL doesn't get response code = 404, 500, 503
        int responseCode = getResponseCode(websiteUrl);
        if (responseCode == 404 || responseCode == 500 || responseCode == 503)
            Assert.fail("BeforeClass test failed: response code is " + responseCode + ".");

        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");

        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password);
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //webDriver.manage().timeouts().pageLoadTimeout(100, TimeUnit.SECONDS);  // switched off: 'safari' doesn't support pageLoadTimeout
        webDriver.manage().window().maximize();
//
//        PageLocation pageLocation = new PageLocation(websiteUrl);
//        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
//        Thread.sleep(1000);
//        if (webDriver.getCurrentUrl().equals(websiteUrl.concat("/install.php"))) {
//            installProfile();
//        }

    }*/

    /**
     * Initialize all the necessary properties and setup the WebDriver.
     */

    @Before
    public void init() throws Exception {
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");

        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

//        PageLocation pageLocation = new PageLocation(websiteUrl);
//        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
    }

    /**
     * Does all the teardown work by quitting the webdriver.
     */
    @After
    public void tearDown() {
        quitWebDriver();
    }

    protected static void quitWebDriver() {
        // Just try to quit the webDriver.
        // This can fail because e.g. TestingBot fails when trying
        // to close a Firefox Browser that has played Flash.
        final WebDriver driver = webDriver;
        new Thread(
            new Runnable() {
                public void run() {
                    try {
                        if (driver != null) {
                            driver.quit();
                        }
                    } catch(Exception e) {

                    }
                }
            }
        ).start();        
    }


    /**
     * This will load the page in WebDriver of the PageLocation that is
     * supplied.
     *
     * @param pageLocation The {@link com.inceptum.enumeration.PageLocation}
     */

    protected static void loadPage(PageLocation pageLocation) {
        if (pageLocation != null) {
            webDriver.get(new StringBuilder().append(websiteUrl).append('/')
                    .append(pageLocation.getUrl("HOME")).toString());
        } else {
            Debug.logError("Unable to load page", MODULE);
        }
    }

    /**
     * This moves the mouse over the element specified.
     *
     * @param element The {@link org.openqa.selenium.WebElement} to mouse over.
     */
    public static void mouseOverElement(WebElement element) {
        Actions action = new Actions(webDriver);
        action.moveToElement(element).build().perform();
        action.moveByOffset(1, 1).build().perform();
        Debug.logInfo("Moving Over", MODULE);
    }

    /**
     * This moves the mouse to given coordinates and perform click.
     *
     * @param x coordinates
     * @param y coordinates
     */
    public static void mouseMoveAndClickByCoordinates(int x, int y) {
        Actions action = new Actions(webDriver);
        action.moveByOffset(x, y).click().build().perform();
    }

    /**
     * Causes the {@link org.openqa.selenium.WebDriver} to wait one full second.
     */
    public static void waitOneSecond() throws Exception {
        Thread.sleep(1000); //implicitWait(1000);
    }

    /**
     * This causes the {@link org.openqa.selenium.WebDriver} to implicitly wait for the duration
     * specified in milliseconds.
     *
     * @param duration The duration to wait in milliseconds.
     */
    public static void implicitWait(int duration) {
        //webDriver.manage().timeouts().implicitlyWait(duration, TimeUnit.MILLISECONDS);
    }

    /**
     * This causes WebDriver to wait for an element to be displayed.
     *
     * @param element The {@link org.openqa.selenium.WebElement} to wait for
     */
    public static void waitForElementDisplayed(WebElement element) {
        new WebDriverWait(webDriver, 10).until(checkElementDisplayed(element));
    }

    /**
     * Provides the predicate for {@link #waitForElementDisplayed(org.openqa.selenium.WebElement)}
     * to wait for an element to become displayed.
     *
     * @param element The {@link org.openqa.selenium.WebElement} to wait for
     * @return The {@link com.google.common.base.Predicate} for the visibility check
     */
    private static Predicate<WebDriver> checkElementDisplayed(
            final WebElement element) {
        return new Predicate<WebDriver>() {

            @Override
            public boolean apply(WebDriver webDriver) {
                return isElementDisplayed(element);
            }

        };
    }

    /**
     * This is used by the {@link #checkElementDisplayed(org.openqa.selenium.WebElement)} method to
     * poll the element for whether or not it is displayed.
     *
     * @param element The {@link org.openqa.selenium.WebElement}
     * @return true or false
     */
    private static boolean isElementDisplayed(WebElement element) {
        return element.isDisplayed();
    }

    /**
     * Get the error message for link assertion
     *
     * @param expectedUrl The expected url
     * @return String with error message and current and expected urls
     */
    protected String linkErrorMessage(String expectedUrl) {
        return "User wasn't navigated to appropriate page." + " Actual: " + webDriver.getCurrentUrl() + " Expected: " + expectedUrl;
    }

    /**
     * Asserts if expected url is current url
     *
     * @param expectedUrl The expected url
     */
    protected void assertExpectedUrlIsCurrentUrl(String expectedUrl) {
        Assert.assertTrue(linkErrorMessage(expectedUrl), webDriver.getCurrentUrl().equals(expectedUrl));
    }
    protected String getPageTitle() {
        String pageTitle = webDriver.findElement(By.cssSelector("h1")).getText();
        return pageTitle;
    }
    protected void waitUntilPageIsOpenedWithTitle(String title) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h1[contains(text(), '" + title + "')]")));
        String titleActual = getPageTitle();
        Assert.assertTrue(titleActual.contains(title));
    }
    protected void clickElementByLinkText(String linkText) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText(linkText)));
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("firefox") ||
                PropertyLoader.loadProperty("browser.name").equals("ie")) { // Exception for Linux/Firefox and IE
            webDriver.findElement(By.linkText(linkText)).sendKeys(Keys.RETURN);
        } else {
            webDriver.findElement(By.linkText(linkText)).click();
        }
        waitForPageToLoad();
    }
    protected EditUserAccountPage clickEditLinkInPeopleTable(List<String> user) throws Exception {
        String xpathEditLink = "//a[text()='" + user.get(0) + "']/../../td/a[text()='edit']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathEditLink)));
        WebElement linkEdit = webDriver.findElement(By.xpath(xpathEditLink));
        clickItem(linkEdit, "The link edit for current user could not be clicked on People page.");
        return new EditUserAccountPage(webDriver, pageLocation);
    }
    protected void clickEditLinkInContentTable(String title) throws Exception {
        String xpathEditLink = "//a[text()='" + title + "']/../../td/ul/li/a[text()='edit']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathEditLink)));
        WebElement linkEdit = webDriver.findElement(By.xpath(xpathEditLink));
        clickItem(linkEdit, "The link edit for current content could not be clicked on Content page.");
    }
    protected void clickEditLinkInContentTableFor2ndLink(String title) throws Exception {
        String xpathEditLink = "//a[text()='" + title + "']/../../td/ul/li/a[text()='edit']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathEditLink)));
        List<WebElement> links = webDriver.findElements(By.xpath(xpathEditLink));
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, links.get(1));
        waitForPageToLoad();
    }
    protected void selectItemFromList(WebElement field, int number) throws Exception {
        List<WebElement> options = field.findElements(By.tagName("option"));
        options.get(number).click();
        waitForPageToLoad();
    }
    protected String getNameOfOption(WebElement field, int numberOfOption) {
        List<WebElement> options = field.findElements(By.tagName("option"));
        return options.get(numberOfOption).getText();
    }
    public String getNameOfSelectedOption(WebElement field) throws Exception {
        waitForPageToLoad();
        List<WebElement> options = field.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if(option.isSelected()) {
                return option.getText();
            }
        }
        Assert.assertTrue("No option has been found in the list or no option is selected", false);
        return "";
    }
    protected void selectItemFromListByClassName(String className, int number) throws Exception {
        ContentPage contentPage = new ContentPage(webDriver, pageLocation);
        waitUntilElementIsClickable(contentPage.getLinkAddContent());
        List<WebElement> options = webDriver.findElements(By.className(className));
        if (PropertyLoader.loadProperty("browser.name").equals("ie")) { // Problem with selecting a checkbox that is not in focus (IE)
            options.get(number).sendKeys(Keys.SPACE);
        } else options.get(number).click();
    }
    protected static void selectItemFromListByOptionName(WebElement field, String optionName) {
        waitUntilElementIsVisible(field);
        List<WebElement> options = field.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals(optionName)) {
                option.click();
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    protected void selectItemFromListByOptionValue(WebElement field, String optionValue) {
        waitUntilElementIsVisible(field);
        List<WebElement> options = field.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getAttribute("value").equals(optionValue)) {
                option.click();
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    protected void assertItemIsInTheList(WebElement field, String optionName) throws Exception {
        waitForPageToLoad();
        waitUntilElementIsVisible(field);
        List<WebElement> options = field.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals(optionName)) {
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    protected void assertOptionIsAbsentInTheList(WebElement field, String optionName) {
        waitUntilElementIsVisible(field);
        List<WebElement> options = field.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals(optionName)) {
                Assert.assertTrue("The option is present in the list.", false);
            }
        }
    }
    protected void fillFrame(WebElement iframe, String text) {
        waitUntilElementIsVisible(iframe);
        webDriver.switchTo().frame(iframe);
        // Workaround for https://code.google.com/p/selenium/issues/detail?id=2331
        if (browser.getName().equals("firefox") || browser.getName().equals("ie") || browser.getName().equals("safari")) {
            ((JavascriptExecutor) webDriver).executeScript("document.body.innerHTML = '<p>" + text + "</p>'");
        }
        else {
            WebElement frame = webDriver.findElement(By.xpath("/html/body"));
            frame.clear();
            frame.sendKeys(text);
        }
        webDriver.switchTo().defaultContent();
    }
    protected void clearFrame(WebElement iframe) {
        waitUntilElementIsVisible(iframe);
        webDriver.switchTo().frame(iframe);
        WebElement frame = webDriver.findElement(By.xpath("/html/body/p"));
        frame.clear();
        webDriver.switchTo().defaultContent();
    }
    protected void checkAlert() {
        try {
            WebDriverWait wait = new WebDriverWait(webDriver, 4);
            wait.until(ExpectedConditions.alertIsPresent());
            Alert alert = webDriver.switchTo().alert();
            alert.accept();
        } catch (Exception e) {
            //exception handling
        }
    }
    protected void assertElementActiveStatus(WebElement element) throws Exception {
        waitUntilElementIsVisible(element);
        try {
            Assert.assertEquals("active", element.getAttribute("class"));
        } catch (Exception e) {
            System.err.println("Element is not active.");
        }
    }
    protected static void assertMessage(WebElement message, String status, String text) throws Exception {
        waitUntilElementIsVisible(message);
        Assert.assertEquals(status, message.getAttribute("class"));
        String messageText = message.getText();
        Assert.assertTrue("Detected incorrect message: " + messageText, messageText.matches("(?s)" + text));
    }
    public static void assertTextOfMessage(WebElement message, String text) throws Exception {
        waitUntilElementIsVisible(message);
        String messageText = message.getText();
        Assert.assertTrue("Detected incorrect message: " + messageText, messageText.matches("(?s)" + text));
    }
    public void assertTitleIsChangedOnViewPage(WebElement fieldTitle, String titleExpected) {
        waitUntilElementIsVisible(fieldTitle);
        String title = fieldTitle.getText();
        Assert.assertTrue("Detected incorrect Title: " + title, title.matches(titleExpected));
    }
    public void assertTitleIsChangedInContentTable(WebElement fieldTitle, String titleExpected) {
        waitUntilElementIsVisible(fieldTitle);
        String title = fieldTitle.getText();
        Assert.assertTrue("Detected incorrect Title: " + title, title.matches(titleExpected));
    }
    public void autoSelectItemFromList(WebElement field, String itemName) throws Exception {
        closeCookiesPopup();
        field.sendKeys(itemName);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#autocomplete")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#autocomplete ul li:first-child div")));
        WebElement autocomplete = webDriver.findElement(By.cssSelector("#autocomplete ul li:first-child div"));
        autocomplete.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#autocomplete")));
        Thread.sleep(500); // test fails without the timeout
    }
    public String autoSelectItemFromListAndGetNode(WebElement field, String itemName) throws Exception {
        closeCookiesPopup();
        field.sendKeys(itemName);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#autocomplete")));
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#autocomplete ul li:first-child div")));
        WebElement autocomplete = webDriver.findElement(By.cssSelector("#autocomplete ul li:first-child div"));
        autocomplete.click();
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#autocomplete")));
        Thread.sleep(500); // test fails without the timeout
        // Get node of the material
        String titleOfMaterial = field.getAttribute("value");
        String node = titleOfMaterial.split("\\(")[1].split("\\)")[0]; // split the node
        return node;
    }
    public void checkThumbnailIsShown(WebElement thumbnail) {
        Assert.assertEquals("media-thumbnail", thumbnail.getAttribute("class"));
    }
    public void checkElementIsAbsent(String cssPath) throws Exception {
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getLinkEdit());
        int elements = webDriver.findElements(By.cssSelector(cssPath)).size();
        Assert.assertTrue(elements == 0);
    }
    public static void waitUntilElementIsVisible(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.visibilityOf(element));
    }
    public static void waitUntilElementIsClickable(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void assertFieldIsNotEmpty(WebElement field, String textIfEmpty) {
        String value = field.getAttribute("value");
        if (value.isEmpty()) {
            field.sendKeys(textIfEmpty);
        }
    }
    public static void assertCheckboxIsSelected(WebElement checkbox) {
        waitUntilElementIsVisible(checkbox);
        if (!(checkbox.isSelected())) {
            checkbox.click();
        }
    }
    public void assertCheckboxIsNotSelected(WebElement checkbox) {
        waitUntilElementIsVisible(checkbox);
        if (checkbox.isSelected()) {
            checkbox.click(); // click to remove selection
        }
    }
    public RecentLogMessagesPage openRecentLogMessagesPage() throws Exception {
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        return new RecentLogMessagesPage(webDriver, pageLocation);
    }
    protected void clearLogMessages() throws Exception {
        openRecentLogMessagesPage();
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);
        recentLogMessagesPage.clickButtonClearLogMessages();
        recentLogMessagesPage.checkEmptyListOfLogMessages();
        waitForPageToLoad();
    }
    public static HomePage openHomePage() throws Exception {
        webDriver.get(websiteUrl);
        ExpectedCondition < Boolean > pageLoad = new
                ExpectedCondition < Boolean > () {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        try {
            wait.until(pageLoad);
        } catch (Throwable pageLoadWaitError) {
            Assert.assertFalse("Timeout during page load", true);
        }
        return new HomePage(webDriver, pageLocation);
    }
    public ContentPage openContentPage() throws Exception {
        webDriver.get(websiteUrl.concat("/admin/content"));
        waitForPageToLoad();
        waitUntilPageIsOpenedWithTitle("Content");
        return new ContentPage(webDriver, pageLocation);
    }
    public PeoplePage openPeoplePage() throws Exception {
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        waitUntilPageIsOpenedWithTitle("People");
        return new PeoplePage(webDriver, pageLocation);
    }
    public void conditionForTaskLink(String taskUrl) {  // Condition for Linux/Chrome if task URL contains characters '5', '6'
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            if (taskUrl.contains("5") || taskUrl.contains("6")) {
                Assert.fail("Test failed: taskLinkURL has character '5' or '6' (Linux/Chrome) - 5=backspace, 6=tab.");
            } // If don't make this fail - taskLink will be created but it'll be not valid.
        }
    }
    public void clickItem(WebElement item, String errorMessage) throws Exception {
        try {
            item.click();
        }
        catch (Exception e) {
            System.err.println(errorMessage);
            throw e;
        }
    }
    public void clickMaterialLink(String title) throws Exception { // for Course and Additional materials
        String xpath = "//a/div/div[@class='title']/div[text()='" + title + "']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));
        waitUntilElementIsClickable(webDriver.findElement(By.xpath(xpath)));
        clickItem(webDriver.findElement(By.xpath(xpath)), "The link '" + title +"' on the View page could not be clicked.");
    }

    public static void waitForPageToLoad() throws Exception {
        ExpectedCondition < Boolean > pageLoad = new
                ExpectedCondition < Boolean > () {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
                    }
                };

        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        try {
            wait.until(pageLoad);
        } catch (Throwable pageLoadWaitError) {
            Assert.assertFalse("Timeout during page load", true);
        }
    }
    public void viewMaterialInAnotherBrowserTab(WebElement linkMaterial) throws Exception {
        // Store the current window handle
        waitForPageToLoad();
        Thread.sleep(1000); // additional time for correct order of received statements
        String winHandleBefore = webDriver.getWindowHandle();
        waitUntilElementIsClickable(linkMaterial);
        clickItem(linkMaterial, "The linkMaterial could not be clicked.");
        // Switch to new opened window
        switchFromFirstPageToSecond(winHandleBefore);
        // Close new window
        webDriver.close();
        // Switch back to the 1st window
        webDriver.switchTo().window(winHandleBefore);
    }
    public void switchFromFirstPageToSecond(String winHandleBefore) throws Exception {
        // Wait until the second tab is opened
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                Set<String> handles = webDriver.getWindowHandles();
                int tabs = handles.size();
                return tabs == 2;
            }
        });
        try {
            Set<String> handles = webDriver.getWindowHandles();
            for (String handle : handles) {
                if (!(handle.equals(winHandleBefore))) {
                    webDriver.switchTo().window(handle);
                    break;
                }
            }
        } catch (Exception e) {
            System.err.println("Couldn't get to the second page.");
        }
    }

    public void loginAs(List<String> userCredentials) throws Exception { // use this method only for PEAS tool (has defined className)
        // Open Login popup window and enter user's credentials
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(userCredentials.get(0));
        loginFormPage.fillFieldPassword(userCredentials.get(1));
        // Click 'Log in' button
        BasePage basePage = loginFormPage.clickButtonLogIn();
        String radiobuttonXpath= "//div[text()='" + className + "']/../../../../input[@type='radio']";

        // Check if 'Register for a course' page appeared after 'Log in' button has been clicked
        if (webDriver.findElements(By.id("modal-content")).size() == 1) { // the case, if current user
                                                                          // hasn't been registered for any course yet
            // Select a Course to register
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(radiobuttonXpath)));
            webDriver.findElement(By.xpath(radiobuttonXpath)).click();
            RegisterForCoursePage registerForCoursePage = new RegisterForCoursePage(webDriver, pageLocation);
            homePage = registerForCoursePage.clickButtonConfirmRegistration();
        }
        // Wait until user link will appear on Home page
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + userCredentials.get(0) + "']")));
        String xpathButtonRegister = "//h2[text()='" + className + "']/../../div[2]/a[text()='Register']";

        // Check if the user is not registered for the Course yet
        if (webDriver.findElements(By.xpath(xpathButtonRegister)).size() == 1) {
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathButtonRegister)));
            webDriver.findElement(By.xpath(xpathButtonRegister)).click();
            RegisterForCoursePage registerForCoursePage = new RegisterForCoursePage(webDriver, pageLocation);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath(radiobuttonXpath)));
            WebElement radiobutton = webDriver.findElement(By.xpath(radiobuttonXpath));
            radiobutton.click();
            Assert.assertTrue(radiobutton.isSelected());
            homePage = registerForCoursePage.clickButtonConfirmRegistration();
        }
        Thread.sleep(1000); // This timeout is needed for the next step (clicking "Peer assignments" button)
        // If the timeout will be short, clicking on the button may lead you to "You are not authorized to..." page
    }
    public void login(List<String> userCredentials) throws Exception {
        // Open Login popup window and enter user's credentials
        HomePage homePage = openHomePage();
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(userCredentials.get(0));
        loginFormPage.fillFieldPassword(userCredentials.get(1));
        // Click 'Log in' button
        loginFormPage.clickButtonLogIn();

        // Check if 'Register for a course' page appeared after 'Log in' button has been clicked
        checkIfRegistrationPopupIsOpenedAndCancel();
        waitUntilProfileLinkIsDisplayed(userCredentials);
    }
    public void submitLoginForm(List<String> username) throws Exception {
        HomePage homePage = openHomePage();
        homePage.clickButtonSignIn();
        fillFieldsOnLoginFormAndSubmit(username);
    }
    public void fillFieldsOnLoginFormAndSubmit(List<String> username) throws Exception {
        waitForPageToLoad();
        CreateNewAccountPage createNewAccountPage = new CreateNewAccountPage(webDriver, pageLocation);
        final String xpathLoginActive = "//a[contains(@class, 'login')]/ancestor::li[@class='active']";
        List<WebElement> loginFieldsActive = webDriver.findElements(By.xpath(xpathLoginActive));
        if (loginFieldsActive.size()==0) {
            createNewAccountPage.switchToLoginPage();
            // Wait until 'Log in' tab is active
            waitCondition(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    List<WebElement> loginFieldsActive = webDriver.findElements(By.xpath(xpathLoginActive));
                    return loginFieldsActive.size() == 1;
                }
            });
        }
        LoginFormPage loginFormPage = new LoginFormPage(webDriver, pageLocation);
        loginFormPage.fillFieldEmail(username.get(0));
        loginFormPage.fillFieldPassword(username.get(1));
        loginFormPage.clickButtonLogIn();
    }
    public void waitUntilLoginPopupIsClosed() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modal-content")));
    }
    public void checkIfRegistrationPopupIsOpenedAndCancel() throws Exception {
        if (webDriver.findElements(By.id("modal-content")).size() == 1) { // the case, if current user
                                                                          // hasn't been registered for any course yet
            // Cancel registration for a course (click link Cancel)
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("input[value='Cancel']")));
            webDriver.findElement(By.cssSelector("input[value='Cancel']")).click();
        }
        waitForPageToLoad();
    }
    public void waitUntilProfileLinkIsDisplayed(List<String> user) {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']")));
    }
    public void logout(List<String> user) throws Exception {
        webDriver.get(websiteUrl);
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(
                By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']")));
        webDriver.findElement(By.xpath("//nav[@id='block-system-user-menu']/ul/li/a[text()='" + user.get(0) + "']")).click();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        homePage.clickLinkSignOut();
        waitUntilElementIsVisible(homePage.getButtonSignIn());
    }
    public String addClass(String className) throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateClassPage createClassPage = addContentPage.clickLinkClass();
        createClassPage.fillFieldName(className);
        createClassPage.fillFieldLocation(location);
        checkRegistrationSettingsIsOpen();
        createClassPage.selectOptionOpenForRegistration();
        createClassPage.userNeedsApprovalBeforeJoiningClass(approval); // don't need approval (direct access)
        createClassPage.clickButtonSave();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been created.");
        return webDriver.getCurrentUrl();
    }
    public void addClassAutomaticState(String className, int daysStart, int daysEnd, int capacity) throws Exception {
        // Open CreateClass page and fill in the fields
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateClassPage createClassPage = addContentPage.clickLinkClass();
        createClassPage.fillFieldName(className);
        selectDateDiffFormat(createClassPage.getFieldStartDate(), daysStart); // daysStart - number of days from actualStartDate
        createClassPage.clickLabelEndDate(); // click the label to open datepicker
        selectDateDiffFormat(createClassPage.getFieldEndDate(), daysEnd); // daysEnd - number of days from actualEndDate
        createClassPage.fillFieldLocation(location);
        checkRegistrationSettingsIsOpen();
        String capacityNumber = Integer.toString(capacity);
        createClassPage.fillFieldCapacity(capacityNumber);
        selectDateDiffFormat(createClassPage.getFieldDeadlineStartDate(), daysStart); // set deadline date
        createClassPage.userNeedsApprovalBeforeJoiningClass(approval);
        createClassPage.clickButtonSave();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been created.");
    }
    public void addClassOpenForRegistrationState(String className, int daysStart, int daysEnd) throws Exception {
        // Open CreateClass page and fill in the fields
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateClassPage createClassPage = addContentPage.clickLinkClass();
        createClassPage.fillFieldName(className);
        selectDateDiffFormat(createClassPage.getFieldStartDate(), daysStart); // daysStart - number of days from actualStartDate
        createClassPage.clickLabelEndDate();
        selectDateDiffFormat(createClassPage.getFieldEndDate(), daysEnd); // daysEnd - number of days from actualEndDate
        createClassPage.fillFieldLocation(location);
        checkRegistrationSettingsIsOpen();
        createClassPage.selectOptionOpenForRegistration();
        createClassPage.userNeedsApprovalBeforeJoiningClass(approval);
        createClassPage.clickButtonSave();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been created.");
    }
    public void addClassOpenForRegistrationNeedApproval(String className, int daysStart, int daysEnd) throws Exception {
        // Open CreateClass page and fill in the fields
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateClassPage createClassPage = addContentPage.clickLinkClass();
        createClassPage.fillFieldName(className);
        selectDateDiffFormat(createClassPage.getFieldStartDate(), daysStart); // daysStart - number of days from actualStartDate
        createClassPage.clickLabelEndDate();
        selectDateDiffFormat(createClassPage.getFieldEndDate(), daysEnd); // daysEnd - number of days from actualEndDate
        createClassPage.fillFieldLocation(location);
        checkRegistrationSettingsIsOpen();
        createClassPage.selectOptionOpenForRegistration();
        createClassPage.userNeedsApprovalBeforeJoiningClass(approval = true); // select "User need to get approval before joining class"
        String emailAddress = getEmail(admin); // get email and fill in the 'Registration contact' field
        createClassPage.fillFieldRegistrationContact(emailAddress);
        createClassPage.clickButtonSave();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been created.");
    }
    public void addClassPrivateRegistrationState(String className, int daysStart, int daysEnd) throws Exception {
        // Open CreateClass page and fill in the fields
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateClassPage createClassPage = addContentPage.clickLinkClass();
        createClassPage.fillFieldName(className);
        selectDateDiffFormat(createClassPage.getFieldStartDate(), daysStart); // daysStart - number of days from actualStartDate
        createClassPage.clickLabelEndDate();
        selectDateDiffFormat(createClassPage.getFieldEndDate(), daysEnd); // daysEnd - number of days from actualEndDate
        createClassPage.fillFieldLocation(location);
        checkRegistrationSettingsIsOpen();
        createClassPage.selectOptionPrivateRegistration();
        createClassPage.clickButtonSave();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been created.");
    }
    public void deleteClass(String className) throws Exception {
        HomePage homePage = openHomePage();
        clickButtonGoToFullProgram();
        List<WebElement> options = webDriver.findElements(By.tagName("option"));
        Iterator<WebElement> iterator = options.iterator();
        while (iterator.hasNext()) {
            WebElement option = iterator.next();
            if (option.getText().equals(className)) {
                option.click();
                waitForPageToLoad();
                openEditPage();
                EditClassPage editClassPage = new EditClassPage(webDriver, pageLocation);
                DeleteItemConfirmationPage deleteItemConfirmationPage = editClassPage.clickButtonDeleteTop();

                deleteItemConfirmationPage.clickCheckboxDeleteClassMakeContentPublic();

                deleteItemConfirmationPage.clickButtonDelete();
                waitUntilElementIsVisible(homePage.getMenuClass());
                clickButtonGoToFullProgram();
                options = webDriver.findElements(By.tagName("option"));
                iterator = options.iterator();
            }
        }
    }
    public void deleteClassFromOverviewPage(String className) throws Exception {
        waitForPageToLoad();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        List<WebElement> options = webDriver.findElements(By.tagName("option"));
        Iterator<WebElement> iterator = options.iterator();
        while (iterator.hasNext()) {
            WebElement option = iterator.next();
            if (option.getText().equals(className)) {
                option.click();
                waitForPageToLoad();
                openEditPage();
                EditClassPage editClassPage = new EditClassPage(webDriver, pageLocation);
                DeleteItemConfirmationPage deleteItemConfirmationPage = editClassPage.clickButtonDeleteTop();

                deleteItemConfirmationPage.clickCheckboxDeleteClassMakeContentPublic();

                deleteItemConfirmationPage.clickButtonDelete();
                waitUntilElementIsVisible(overviewPage.getMenuClass());
                clickButtonGoToFullProgram();
                options = webDriver.findElements(By.tagName("option"));
                iterator = options.iterator();
            }
        }
    }
    public void checkClassLinkIsShown(String className, String linkName) throws Exception { // Home page should be opened
        String xpathLink = "//h2[text()='" + className + "']/../../div/a[text()='" + linkName + "']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLink)));
        WebElement link = webDriver.findElement(By.xpath(xpathLink));
        Assert.assertTrue("The link " + linkName + " is not displayed.", link.isDisplayed());
    }

    public void checkClassLinkIsNotPresented(String className, String linkName) throws Exception { // Home page should be opened
        String xpathLink = "//h2[text()='" + className + "']/../../div/a[text()='" + linkName + "']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpathLink)));
    }
    public void clickClassLink(String className, String linkName) throws Exception {
        String xpathLink = "//h2[text()='" + className + "']/../../div/a[text()='" + linkName + "']";
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLink)));
        WebElement link = webDriver.findElement(By.xpath(xpathLink));
        closeCookiesPopup();
        link.click();
        waitForPageToLoad();
    }
    public void checkClassIsNotShownOnFrontpage(String className) throws Exception {
        openHomePage();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Upcoming classes']")));
        int classNumber = webDriver.findElements(By.xpath("//h2[text()='" + className + "']")).size();
        Assert.assertTrue("The class " + className + " is shown on the Home page.", classNumber == 0);
    }
    public void checkClassIsShownOnFrontpage(String className) throws Exception {
        openHomePage();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Upcoming classes']")));
        int classNumber = webDriver.findElements(By.xpath("//h2[text()='" + className + "']")).size();
        Assert.assertTrue("The class " + className + " is shown on the Home page.", classNumber == 1);
    }
    public void assertClassIsSelected(String className) throws Exception { // in 'Course contents'
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        waitUntilElementIsVisible(overviewPage.getFieldClass());
        String selectedClass = webDriver.findElement(By.xpath("//a[@class='left_part_iminds_academy_class']")).getText();
        Assert.assertTrue("The class " + className + " is not selected on the Overview page.", selectedClass.equals(className));
    }
    public void assertClassIsSelectedInClassMenu(String className) throws Exception {
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getButtonClassMenu());
        String classActual = basePage.getButtonClassMenu().getText();
        Assert.assertTrue("Actual class is not equal to expected class.", classActual.equals(className));
    }
    public ViewGroupPage createGroup(String groupName, String className) throws Exception {
        deleteContentTypeFromTable("Group");
        // Open CreateGroup page
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateGroupPage createGroupPage = addContentPage.clickLinkGroup();
        // Enter group's name and select the class
        createGroupPage.fillFieldTitle(groupName);
        selectProperClassInGroupAudience(className);
        // Save data and check messages
        ViewGroupPage viewGroupPage = createGroupPage.clickButtonSave();
        assertTextOfMessage(viewGroupPage.getMessage(), ".*Group " + groupName + " has been created.");
        assertTextOfMessage(viewGroupPage.getFieldGroup(), "You are the group manager");
        return new ViewGroupPage(webDriver, pageLocation);
    }
    public void deleteContentTypeFromTable(String type) throws Exception {
        // Open Content table and delete all content with defined type
        webDriver.get(websiteUrl.concat("/admin/content"));
        waitForPageToLoad();
        ContentPage contentPage = new ContentPage(webDriver, pageLocation);
        List<WebElement> options = contentPage.getFieldType().findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(type)) { // select the type
                option.click();
                contentPage.clickButtonFilter(); // filter content
                // If content with the type is found, delete it
                if(!(webDriver.findElement(By.xpath("//table[2]/tbody/tr[1]/td[1]")).getText().equals("No content available."))) {
                    contentPage.clickCheckboxSelectAll();
                    List<WebElement> optionsUpdate = contentPage.getFieldUpdateOptions().findElements((By.tagName("option")));
                    for (WebElement optionDelete : optionsUpdate) {
                        if (optionDelete.getAttribute("value").equals("delete")) { // delete option in 'Update options'
                            optionDelete.click();
                            DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
                            deleteItemConfirmationPage.clickCheckboxDoNotReport();
                            deleteItemConfirmationPage.clickButtonDelete();
                            waitUntilPageIsOpenedWithTitle("Content");
                            contentPage.clickButtonReset();
                            return;
                        }
                    }
                } else {
                    contentPage.clickButtonReset();
                    return;
                } // return if no content with the type is found
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public void deleteContentWithTitleFromTable(String title) throws Exception {
        // Open Content table
        webDriver.get(websiteUrl.concat("/admin/content"));
        waitForPageToLoad();
        // Find nodes with defined title
        List<WebElement> items = webDriver.findElements(By.xpath("//table[2]/tbody/tr/td[2]/a[text()='" + title + "']"));
        if (items.size() == 0) {
            return;
        }
        for (WebElement item : items) { // select checkbox of the node if it has current title
                item.findElement(By.xpath("./../../td[1]/div/input")).click();
        }
        // Delete nodes with proper 'Update' option
        ContentPage contentPage = new ContentPage(webDriver, pageLocation);
        List<WebElement> optionsUpdate = contentPage.getFieldUpdateOptions().findElements((By.tagName("option")));
        for (WebElement optionDelete : optionsUpdate) {
            if (optionDelete.getAttribute("value").equals("delete")) { // delete option in 'Update options'
                optionDelete.click();
                DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
                deleteItemConfirmationPage.clickCheckboxDoNotReport();
                deleteItemConfirmationPage.clickButtonDelete();
                waitUntilPageIsOpenedWithTitle("Content");
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false); // Show the message if 'Delete' option hasn't been found
    }
    public void selectProperClassInGroupAudience(String classExpected) throws Exception {
        // Click link 'Groups audience'
        WebElement linkGroupsAudience = webDriver.findElement(By.partialLinkText("Groups audience"));
        waitUntilElementIsClickable(linkGroupsAudience);
        clickItem(linkGroupsAudience, "The link 'Groups audience' on the Create page could not be clicked.");
        // Find the name of needed Class in the field and select it
        WebElement fieldYourGroups = webDriver.findElement(By.id("edit-og-group-ref-und-0-default"));
        List<WebElement> options = fieldYourGroups.findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals(classExpected)) {
                option.click();
                Thread.sleep(500); // the interval is needed to select the option
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }

    public String getEmail(List<String> user) {
        String trimmedName = user.get(0).replace(" ", ""); // remove space from full name
        String nameLowerCase = trimmedName.toLowerCase(); // change the name to lower case
        String email = nameLowerCase.concat("@tut.by");
        return email;
    }
    public String getMboxValue(List<String> user) {
        String actorMbox = "mailto:" + getEmail(user);
        return actorMbox;
    }
    public String getReviewName(List<String> user, String peerTaskName) {
        String reviewName = peerTaskName + " - Review " + user.get(0);
        return reviewName;
    }
    public String getPeerSubmissionName(List<String> user, String peerTaskName) {
        String peerSubmissionName = peerTaskName + " - Draft submission of " + user.get(0);
        return peerSubmissionName;
    }
    public String selectCurrentDate(WebElement field) throws Exception {
        waitUntilElementIsClickable(field);
        field.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ui-datepicker-div"))); // datepicker is shown
        WebElement currentDate = webDriver.findElement(By.cssSelector("a.ui-state-default.ui-state-highlight"));
        waitUntilElementIsClickable(currentDate);
        Thread.sleep(500); // doesn't work without this timeout!
        currentDate.click();
        // Wait until calendar is hidden
        (new WebDriverWait(webDriver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return (!d.findElement(By.id("ui-datepicker-div")).isDisplayed());
            }
        });
        return field.getAttribute("value");
    }

    public void selectDate(WebElement field, int days) throws Exception {
        waitUntilElementIsClickable(field);
        field.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ui-datepicker-div"))); // datepicker is shown

        // Select other date
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, days); // int day - is quantity of days
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String otherDate = format.format(currentDate.getTime());
        field.clear();
        field.sendKeys(otherDate);
    }
    public void selectDateDiffFormat(WebElement field, int days) {
        waitUntilElementIsClickable(field);
        field.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("ui-datepicker-div"))); // datepicker is shown

        // Select other date
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, days); // int day - is quantity of days
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String otherDate = format.format(currentDate.getTime());
        field.clear();

        field.sendKeys(otherDate);
    }

    public String enterStartTime(WebElement field, int addHours) throws Exception { // inline editing
        Thread.sleep(1000);
        // Get current time
        Calendar currentTime = Calendar.getInstance();
        // Hours
        int hoursInt = currentTime.get(Calendar.HOUR_OF_DAY);
        int hoursPlusInt = hoursInt + addHours; // Get time (currentHours + additionalHours)
        String hoursPlus = Integer.toString(hoursPlusInt);
        // Edit 'hours' if it has 1 character (time should have 2 characters)
        int hoursCounter = hoursPlus.length();
        if (hoursCounter == 1) {
            hoursPlus = "0" + hoursPlus;
        }
        int minutesInt = currentTime.get(Calendar.MINUTE);
        String minutes = Integer.toString(minutesInt);
        // Edit 'minutes' if it has 1 character (time should have 2 characters)
        int minutesCounter = minutes.length();
        if (minutesCounter == 1) {
            minutes = "0" + minutes;
        }
        // Enter time
        String time;
        scrollToElement(field);
        // Exception for hour = 23
        if (hoursPlus.equals("24")) {
            field.sendKeys("23");
            field.sendKeys(Keys.ARROW_RIGHT);
            field.sendKeys("59");
            time = "23:59";
        } else {
            waitUntilElementIsVisible(field);
            time = hoursPlus + ":" + minutes;
            ((JavascriptExecutor) webDriver)
                    .executeScript("document.getElementById('edit-field-date-start-end-und-0-value-timeEntry-popup-1')" +
                            ".value = '" + time + "';");
        }
        return time;
    }
    public String enterEndTime(WebElement field, int addHours) throws Exception { // inline editing
        Thread.sleep(1000);
        // Get current time
        Calendar currentTime = Calendar.getInstance();
        // Hours
        int hoursInt = currentTime.get(Calendar.HOUR_OF_DAY);
        int hoursPlusInt = hoursInt + addHours; // Get time (currentHours + additionalHours)
        String hoursPlus = Integer.toString(hoursPlusInt);
        // Edit 'hours' if it has 1 character (time should have 2 characters)
        int hoursCounter = hoursPlus.length();
        if (hoursCounter == 1) {
            hoursPlus = "0" + hoursPlus;
        }
        int minutesInt = currentTime.get(Calendar.MINUTE);
        String minutes = Integer.toString(minutesInt);
        // Edit 'minutes' if it has 1 character (time should have 2 characters)
        int minutesCounter = minutes.length();
        if (minutesCounter == 1) {
            minutes = "0" + minutes;
        }
        // Enter time
        String time;
        scrollToElement(field);
        // Exception for hour = 23
        if (hoursPlus.equals("24")) {
            field.sendKeys("23");
            field.sendKeys(Keys.ARROW_RIGHT);
            field.sendKeys("59");
            time = "23:59";
        } else {
            waitUntilElementIsVisible(field);
            time = hoursPlus + ":" + minutes;
            ((JavascriptExecutor) webDriver)
                    .executeScript("document.getElementById('edit-field-date-start-end-und-0-value2-timeEntry-popup-1')" +
                            ".value = '" + time + "';");
        }
        return time;
    }
    public String checkClassIsAddedOnOverview(String className) throws Exception {
        webDriver.get(websiteUrl);
        waitForPageToLoad();
        clickButtonGoToFullProgram();
        // Wait until class field is visible on Overview page
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".class-select")));
        // Check whether a Class with defined name already exist
        List<WebElement> options = webDriver.findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                option.click();
                System.out.println("The class " + className + " has already been added.");
                // Make the Class 'Open for registration'
                openEditPage();
                EditClassPage editClassPage = new EditClassPage(webDriver, pageLocation);
                checkRegistrationSettingsIsOpen();
                editClassPage.selectOptionOpenForRegistration();
                editClassPage.userNeedsApprovalBeforeJoiningClass(approval); // leave the field 'User need approval' unchecked
                editClassPage.clickButtonSave();
                OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
                assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been updated.");
                return webDriver.getCurrentUrl();
            }
        }
        // Add a Class if there is no such Class with defined name
        String classUrl = addClass(className);
        return classUrl;
    }
    public void checkClassExistsInCourseContentsList(String className) {
        // Wait until class field is visible on Overview page
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".class-select")));
        List<WebElement> options = webDriver.findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public void checkUserIsCreated(String user) throws Exception {
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        waitUntilPageIsOpenedWithTitle("People");
        // Check whether the user has been already created
        List<WebElement> options = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a")); // find all users
        for (WebElement option : options) {
            if (option.getText().equals(user)) {
                return;
            }
        }
        // Create new user
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = user.concat("@mail.ru");
        addUserPage.addUser(user, email, user);
        assertTextOfMessage(addUserPage.getMessage(), ".*Created a new user account for " + user
                + ". No e-mail has been sent.");
    }
    public void addRole(String role, List<String> user) throws Exception {
        // Open People page and select the user account (its checkbox)
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        for (WebElement element : elements) {
            if (element.getText().equals(user.get(0))) {
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                // Find option = role in 'Update options' -> 'Add a role to the selected' users and submit
                PeoplePage peoplePage = new PeoplePage(webDriver, pageLocation);
                List<WebElement> options = peoplePage.getFieldUpdateOptions().findElements(By.tagName("option"));
                for (WebElement option : options) {
                    if (option.getText().equals(role)) {
                        if (option.findElement(By.xpath("./..")).getAttribute("label").equals("Add a role to the selected users")) {
                            option.click();
                            wait.until(ExpectedConditions.elementToBeSelected(option));
                            peoplePage.clickButtonUpdate();
                            assertTextOfMessage(peoplePage.getMessage(), ".*The update has been performed.");
                            // Check that the selected role is displayed correctly in People table
                            roleIsDisplayedInPeopleTable(role, user);
                            return;
                        }
                    }
                }
            }
        }
    }

    public void roleIsDisplayedInPeopleTable(String role, List<String> user) throws Exception {
        String roleXpath = "//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']/../../td[4]/div";
        String actualRole = webDriver.findElement(By.xpath(roleXpath)).getText();
        Assert.assertTrue("The role is displayed incorrectly in People table: " + actualRole + ".", actualRole.equals(role));
    }

    public void roleIsAbsentInPeopleTable(List<String> user) throws Exception {
        String roleXpath = "//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']/../../td[4]/div";
        String actualRole = webDriver.findElement(By.xpath(roleXpath)).getText();
        Assert.assertTrue("The role is displayed in People table: " + actualRole + ".", actualRole.equals(""));
    }

    public void assertGroupRoleIsShown(List<String> user, String role, String className) throws Exception {
        // Open 'People in group' page with 'People' link
        GroupTabClassPage groupClassPage = new GroupTabClassPage(webDriver, pageLocation);
        groupClassPage.clickLinkPeople();
        waitUntilPageIsOpenedWithTitle("People in group " + className);
        // Check the user has correct role
        List<WebElement> options = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a")); // find all users
        for (WebElement option : options) {
            if (option.getText().equals(user.get(0))) {
                String roleActual = option.findElement(By.xpath("./../../td[4]/div/ul/li")).getText();
                Assert.assertTrue("Actual role '" + roleActual + "' doesn't match with expected role.", roleActual.equals(role));
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }

    public void checkUserIsSubscribedWithRole(String role, String className, List<String> user) throws Exception {
        // Open 'People in group' page with 'People' link
        GroupTabClassPage groupClassPage = new GroupTabClassPage(webDriver, pageLocation);
        PeopleInGroupForClassPage peopleInGroupForClassPage = groupClassPage.clickLinkPeople();
        waitUntilPageIsOpenedWithTitle("People in group " + className);
        // Check whether the user has been already subscribed
        List<WebElement> options = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a")); // find all users
        for (WebElement option : options) {
            if (option.getText().equals(user.get(0)) &&
                    option.findElement(By.xpath("./../../td[4]/div/ul/li")).getText().equals(role)) {
                return;
            }
        }
        // Add the user to group
        peopleInGroupForClassPage.clickLinkGroup();
        waitUntilPageIsOpenedWithTitle(className);
        peopleInGroupForClassPage = groupClassPage.clickLinkAddPeople();
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), user.get(0));
        selectRole(role);
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(peopleInGroupForClassPage.getMessage(), ".*" + user.get(0) + " has been added to the group " + className + ".");
    }

    public void selectRole(String role) throws Exception {
        String xpathRole = "//*[@id='edit-roles']/div/label[contains(text(), '" + role + "')]/../input";
        WebDriverWait wait =new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(xpathRole)));
        WebElement checkbox = webDriver.findElement(By.xpath(xpathRole));
        checkbox.click();
        Assert.assertTrue(checkbox.isSelected());
    }

    public void checkUserIsAlreadyRegistered(List<String> user) throws Exception {
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        Iterator<WebElement> iterator = elements.iterator();
        while (iterator.hasNext()) {
            WebElement element = iterator.next();
            if (element.getText().contains(user.get(0))) { // cancel user account if it has been already created
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                cancelSelectedUserAccount(user);
                elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
                iterator = elements.iterator();
            }
        }
    }

    public void checkClassIsSelectedOnRegisterPage(String className) throws Exception {
        waitForPageToLoad();
        String xpathRadiobutton = "//div[text()='" + className + "']/../../../../input";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathRadiobutton)));
        WebElement radiobutton = webDriver.findElement(By.xpath(xpathRadiobutton));
        Assert.assertTrue("The radiobutton is not selected on 'Register for a course page.'", radiobutton.isSelected());
    }
    public WebElement checkNoClassIsSelectedOnRegisterPage(String className) {
        // Check that needed Class is displayed on Register form
        String xpathRadiobutton = "//div[text()='" + className + "']/../../../../input";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathRadiobutton)));
        WebElement radiobuttonClass = webDriver.findElement(By.xpath(xpathRadiobutton));
        // Check that no Class is selected on Register form
        String cssRadiobuttonSelected = ".form-radio[checked='checked']";
        List<WebElement> buttons = webDriver.findElements(By.cssSelector(cssRadiobuttonSelected));
        Assert.assertTrue("Some Class is selected on Register page.", buttons.size() == 0);
        return radiobuttonClass;
    }

    public void cancelSelectedUserAccount(List<String> user) throws Exception {
        PeoplePage peoplePage = new PeoplePage(webDriver, pageLocation);
        List<WebElement> options = peoplePage.getFieldUpdateOptions().findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals("Cancel the selected user accounts")) {
                option.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(option));
                DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = peoplePage.clickButtonUpdate();
                wait.until(ExpectedConditions.titleContains("Are you sure you want to cancel these user accounts?"));
                deleteUserAccountConfirmationPage.selectCheckboxDeleteAccountAndItsContent();
                deleteUserAccountConfirmationPage.selectCheckboxDoNotReport();
                deleteUserAccountConfirmationPage.clickButtonCancelAccounts();
                assertTextOfMessage(peoplePage.getMessage(), ".*" + user.get(0) + ".*has been deleted.");
                break;
            }
        }
    }

    public void removeSelectedUseraccountFromClass() throws Exception {
        PeopleInGroupForClassPage peopleInGroupForClassPage = new PeopleInGroupForClassPage(webDriver, pageLocation);
        List<WebElement> options = peopleInGroupForClassPage.getFieldOperations().findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals("Remove from group")) {
                option.click();
                DeleteItemConfirmationPage deleteItemConfirmationPage = peopleInGroupForClassPage.clickButtonExecute();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.titleContains("Are you sure you want to perform"));
                deleteItemConfirmationPage.clickButtonConfirm();
                assertTextOfMessage(peopleInGroupForClassPage.getMessage(), ".*Performed.*on 1 item.");
                break;
            }
        }
    }

    public void addUserToClassWithRole(List<String> user, String className, String role) throws Exception {
        // Check that Class is created, get its url (Overview page)
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl();
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));

        // Open PeopleInGroup page for current class
        webDriver.get(viewClassPageUrl);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();
        PeopleInGroupForClassPage peopleInGroupForClassPage = whoIsWhoPage.clickButtonAddUser();
        // Add the user to the Class with selected role
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), user.get(0));
        selectRole(role);
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + user.get(0) + ".*has been added to the group " + className + ".");
    }

    public String addStudentToClass(List<String> user, String className) throws Exception { // student == user without role
        // Check that Class is created, get its url (Overview page)
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl();
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));

        // Open WhoIsWho page for current class
        webDriver.get(viewClassPageUrl);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();
        PeopleInGroupForClassPage peopleInGroupForClassPage = whoIsWhoPage.clickButtonAddUser();
        // Add the user to the Class
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), user.get(0));
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + user.get(0) + ".*has been added to the group " + className + ".");
        return viewClassPageUrl;
    }

    public void addUserToClass(List<String> user, String viewClassPageUrl) throws Exception {
        // Open the Class overview page
        webDriver.get(viewClassPageUrl);
        waitForPageToLoad();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        WebElement classSelected = overviewPage.getFieldClass().findElement(By.xpath("//a[@class='left_part_iminds_academy_class']"));
        String className = classSelected.getText(); // get Class name from overview page
        // Add the user to the Class
        GroupTabClassPage groupTabClassPage = overviewPage.clickLinkGroup();
        PeopleInGroupForClassPage peopleInGroupForClassPage = groupTabClassPage.clickLinkAddPeople();
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), user.get(0));
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(peopleInGroupForClassPage.getMessage(), ".*" + user.get(0) + ".*has been added to the group " + className + ".");
    }

    public void addUserToGroup(List<String> user, String groupName) throws Exception {
        ViewGroupPage viewGroupPage = new ViewGroupPage(webDriver, pageLocation);
        GroupTabGroupPage groupTabGroupPage = viewGroupPage.clickLinkGroup();
        PeopleInGroupForGroupPage peopleInGroupForGroupPage = groupTabGroupPage.clickLinkAddPeople();
        waitUntilPageIsOpenedWithTitle("People in group");
        autoSelectItemFromList(peopleInGroupForGroupPage.getFieldUsername(), user.get(0));
        peopleInGroupForGroupPage.clickButtonAddUsers();
        assertTextOfMessage(peopleInGroupForGroupPage.getMessage(), ".*" + user.get(0) + ".*has been added to the group " + groupName + ".");
    }

    public void addAuthenticatedUser(List<String> user) throws Exception {
        checkUserIsAlreadyRegistered(user);
        openAddUserPage();
        addUserFromPeopleInGroupPage(user);
    }

    public void addUserFromPeopleInGroupPage(List<String> user) throws Exception {
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));
    }

    public void addUserWithSiteRole(List<String> user, String role) throws Exception {
        checkUserIsAlreadyRegistered(user);
        openAddUserPage();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUserWithRole(role, user.get(0), email, user.get(1));
    }

    public void waitUntilFileDownloaded() throws Exception {
        Set<String> handles;
        do {
            handles = webDriver.getWindowHandles();
            Thread.sleep(100);
        }
        while (!(handles.size()==1));
        Thread.sleep(1000);
    }


    /*public void click(By by) throws Exception{
        WebElement element = webDriver.findElement(by);
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
        element.click();
    }*/
    public void openEditPage() throws Exception {
        // Click "Edit" tab
        BasePage basePage = new BasePage(webDriver, pageLocation);
        Thread.sleep(1000);
        List<WebElement> links = webDriver.findElements(By.partialLinkText("Edit page"));
        WebElement linkEdit;
        String page;
        if (links.size() == 0) {
            links = webDriver.findElements(By.partialLinkText("Edit class"));
            if (links.size() == 0) {
                linkEdit = webDriver.findElement(By.linkText("Edit"));
                waitUntilElementIsClickable(linkEdit);
                Thread.sleep(1000);
                page = webDriver.getCurrentUrl();
                linkEdit.click();
            } else {
                linkEdit = webDriver.findElement(By.partialLinkText("Edit class"));
                waitUntilElementIsClickable(linkEdit);
                Thread.sleep(1000);
                page = webDriver.getCurrentUrl();
                linkEdit.click();
            }
        } else {
            linkEdit = webDriver.findElement(By.partialLinkText("Edit page"));
            waitUntilElementIsClickable(linkEdit);
            Thread.sleep(1000);
            page = webDriver.getCurrentUrl();
            linkEdit.click();
        }

        waitForPageToLoad();
        // Get new page url (This can fail if page is busy loading)
        // Therefor keep trying till we get one.
        String newPage = null;
        while (newPage == null) {
            try {
                newPage = webDriver.getCurrentUrl();
            } catch (Exception e) {
                    Thread.sleep(500);
            }
        }

        // Check if already on the "Edit" page, if not "Advanced edit" should come into play
        if (newPage.equals(page)) {

           // Wait till "Advanced edit" is shown (Can be slow in initialisation)

            waitUntilElementIsClickable(basePage.getTabAdvancedEdit());
            // Click "Advanced edit"
            webDriver.findElement(By.partialLinkText("Advanced edit")).click();
            waitForPageToLoad();
        }
    }

    protected void openEditPageModalContent() throws Exception {
        // Click "Edit" tab
        Thread.sleep(1000);
        WebElement linkEdit = webDriver.findElement(By.cssSelector("#modalContent .editPage a"));
        waitUntilElementIsClickable(linkEdit);
        String page = webDriver.getCurrentUrl();
        linkEdit.click();

        waitForPageToLoad();
        // Get new page url (This can fail if page is busy loading)
        // Therefore keep trying till we get one.
        String newPage = null;
        while (newPage == null) {
            try {
                newPage = webDriver.getCurrentUrl();
            } catch (Exception e) {
                Thread.sleep(500);
            }
        }

        // Check if already on the "Edit" page, if not "Advanced edit" should come into play
        if (newPage.equals(page)) {
            // Wait till "Advanced edit" is shown (Can be slow in initialisation)
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#modalContent .editing a")));
            // Click "Advanced edit"
            webDriver.findElement(By.cssSelector("#modalContent .editing a")).click();
            waitForPageToLoad();
        }
    }

    public static int getResponseCode(String URLName){
        try {
            HttpURLConnection.setFollowRedirects(false);
            HttpURLConnection con =
                    (HttpURLConnection) new URL(URLName).openConnection();
            con.setRequestMethod("HEAD");
            return con.getResponseCode();
        }
        catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void selectClass(String className) throws Exception {
        waitForPageToLoad();
        clickButtonGoToFullProgram();
        List<WebElement> options = webDriver.findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                option.click();
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }

    public void openAddUserPage() throws Exception {
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        waitUntilElementIsVisible(addUserPage.getFieldFullName());
    }

    public void closeCookiesPopup() {
        // 'Cookie_notification' message can be shown on the page, it blocks clicking the link (Chrome)
        List<WebElement> notifications = webDriver.findElements(By.xpath("//div[@id='cookie_notification'][@style='display: block;']"));
        if (notifications.size() == 1) {
            WebElement notification = webDriver.findElement(By.xpath("//div[@id='cookie_notification'][@style='display: block;']"));
            if (notification.isDisplayed()) {
                webDriver.findElement(By.xpath("//div[@id='cookie_notification'][@style='display: block;']/input")).click(); // close the message
            }
        }
    }

    public static void waitCondition(ExpectedCondition<Boolean> condition) {
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(condition);
    }

    public void uploadImage(WebElement field, String imagePath, WebElement buttonUpload) throws Exception {
        String imageAbsolutePath = new File(imagePath).getAbsolutePath();
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") ||
                PropertyLoader.loadProperty("browser.platform").equals("any")) { // Exception for Linux OS/Mavericks: enter a correct file path
            String imagePathLinux = imageAbsolutePath.replace("D:", "");
            field.sendKeys(imagePathLinux);
        } else field.sendKeys(imageAbsolutePath);
        // Click Upload button and wait until Remove button appears
        buttonUpload.click();
    }
    public void checkNumberOfIframes(int number) {
        List<WebElement> iframes = webDriver.findElements(By.cssSelector("iframe"));
        int numberOfIframes = iframes.size();
        Assert.assertTrue("The actual number of iframes on the page is not correct: " + numberOfIframes,
                numberOfIframes == number);
    }
    public void checkNumberOfImages(int number) {
        List<WebElement> images = webDriver.findElements(By.cssSelector(".field__items img"));
        int numberOfImages = images.size();
        Assert.assertTrue("The actual number of images on the page is not correct: " + numberOfImages,
                numberOfImages == number);
    }
    public OverviewPage clickButtonGoToFullProgram() throws Exception {
        // Open class menu
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getMenuClass());
        WebElement arrow = basePage.getMenuClass().findElement(By.xpath("//*[@class='arrow'][text()='More']"));
        waitUntilElementIsClickable(arrow);
        clickItem(arrow, "Class menu could not be opened.");
        // Click the link 'Go to full program'
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(text(), 'Go to full program')]")));
        WebElement linkGoToFullProgram = webDriver.findElement(By.xpath("//a[contains(text(), 'Go to full program')]"));
        waitUntilElementIsClickable(linkGoToFullProgram);
        clickItem(linkGoToFullProgram, "The link 'Go to full program' on the HomePage could not be clicked.");
        waitForPageToLoad();
        return new OverviewPage(webDriver, pageLocation);
    }
    public String getLinkUrlFrom1stElement() {
        String xpath1stElementTitle = "//table[2]/tbody/tr[1]/td[2]/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath1stElementTitle)));
        String href = webDriver.findElement(By.xpath(xpath1stElementTitle)).getAttribute("href");
        return href;
    }
    public static void scrollToElement(WebElement element) throws Exception { // use the method to focus on element
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", element);
        Thread.sleep(500);
    }

    public String getChapterOrder(String classNode, String title, String nodeId) throws Exception {
        webDriver.get(websiteUrl.concat("/overview/nojs/") + classNode); // Redirecting to Overview page with current class node
        waitForPageToLoad();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        waitUntilElementIsVisible(overviewPage.getNavPanel());
        String chapterOrder = "";
        List<WebElement> options = overviewPage.getNavPanel().findElements(By.xpath("//h2/div[text()='" + title + "']/../../.."));
        for (WebElement option : options) {
            if (option.getAttribute("nodeid").equals(nodeId)) {
                chapterOrder = option.getAttribute("nodeweight");
                break;
            }
        }
        return chapterOrder;
    }

    public void masqueradeAsUser(List<String> user) throws Exception {
        // Enter username into 'View site as' field
        BasePage basePage = new BasePage(webDriver, pageLocation);
        scrollToElement(basePage.getFieldViewSiteAs());
        waitUntilElementIsClickable(basePage.getFieldViewSiteAs());
        autoSelectItemFromList(basePage.getFieldViewSiteAs(), user.get(0)); // select needed user
        basePage.clickButtonGoMasquerading();
        // Wait until user's profile link is displayed
        waitUntilProfileLinkIsDisplayed(user);
        // Wait until the message is displayed
        String text = ".*You are now masquerading as " + user.get(0) + ".";
        WebElement message = webDriver.findElement(By.cssSelector(".messages.messages--status"));
        assertTextOfMessage(message, text);
        waitForPageToLoad();
    }

    public void switchBackFromMasqueradingAs(List<String> user, List<String> userDefault) throws Exception {
        // Click link 'Switch back'
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.clickLinkSwitchBack();
        // Wait until default user profile link is displayed
        waitUntilProfileLinkIsDisplayed(userDefault);
        // Wait until the message is displayed
        String text = ".*You are no longer masquerading as " + user.get(0) +
                " and are now logged in as " + userDefault.get(0) + ".";
        WebElement message = webDriver.findElement(By.cssSelector(".messages.messages--status"));
        assertTextOfMessage(message, text);
    }
    public void waitUntilActionWithThrobberDone() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".throbber")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".throbber")));
    }
    public void checkModulesAreEnabledForPA() throws Exception {
        // Enable 'iMindsX Google Service Account OAuth' module
        webDriver.get(websiteUrl.concat("/admin/modules"));
        enableModuleImindsXGoogleServiceAccountQAuth();
        enableModulesPeerAssignmentCoreAndNavigation();

        // Add a provider if it doesn't exist yet
        webDriver.get(websiteUrl.concat("/admin/config/services/oauth_provider"));
        OAuthProvidersPage oAuthProvidersPage = new OAuthProvidersPage(webDriver, pageLocation);
        waitUntilElementIsClickable(oAuthProvidersPage.getLinkAddProvider());
        List<WebElement> providers = webDriver.findElements(By.xpath("//tr/td[text()='" + accountName + "']"));
        if (providers.size() == 0) {
            addProvider();
        }
    }
    public static void addProvider() throws Exception {
        // Click 'Add provider' link
        OAuthProvidersPage oAuthProvidersPage = new OAuthProvidersPage(webDriver, pageLocation);
        oAuthProvidersPage.clickLinkAddProvider();
        // Fill in the fields for a new provider
        selectItemFromListByOptionName(oAuthProvidersPage.getFieldSelectOAuthType(), googleServiceAccount);
        oAuthProvidersPage.clickButtonNext();
        oAuthProvidersPage.fillFieldAccountName(accountName);
        oAuthProvidersPage.fillFieldAccountEmail(accountEmail);
        uploadFile(privateKey);
        oAuthProvidersPage.selectCheckboxConvertUploadedFiles();
        oAuthProvidersPage.clickButtonSaveConfiguration();
        // Wait until changes are saved, check the provider is in a list of added providers
        waitUntilElementIsClickable(oAuthProvidersPage.getLinkAddProvider());
        List<WebElement> providers = webDriver.findElements(By.xpath("//tr/td[text()='" + accountName + "']"));
        Assert.assertTrue("The provider hasn't been added.", providers.size() == 1);
    }
    public static void uploadFile(String filePath) throws Exception {
        String absolutePath = new File(filePath).getAbsolutePath();
        OAuthProvidersPage oAuthProvidersPage = new OAuthProvidersPage(webDriver, pageLocation);
        WebElement fieldPrivateKey = oAuthProvidersPage.getFieldPrivateKey();
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") ||
                PropertyLoader.loadProperty("browser.platform").equals("any")) { // Exception for Linux OS/Mavericks: enter a correct file path
            String filePathLinux = absolutePath.replace("D:", "");
            oAuthProvidersPage.getFieldPrivateKey().sendKeys(filePathLinux);
        } else fieldPrivateKey.sendKeys(absolutePath);
        oAuthProvidersPage.clickButtonUpload();
        waitUntilElementIsVisible(oAuthProvidersPage.getButtonRemove());
    }

    public static void enableModuleImindsXGoogleServiceAccountQAuth() throws Exception {
        ModulesPage modulesPage = new ModulesPage(webDriver, pageLocation);
        // If switch has OFF state switch it ON
        waitUntilElementIsVisible(modulesPage.getSwitchImindsXGoogleServiceAccountQAuth());
        if (modulesPage.getSwitchImindsXGoogleServiceAccountQAuth().getAttribute("class").contains("off")) {
            modulesPage.getSwitchImindsXGoogleServiceAccountQAuth().click();
            final WebElement checkbox = modulesPage.getSwitchImindsXGoogleServiceAccountQAuth();
            waitCondition(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    return checkbox.findElement(By.xpath("./../..")).getAttribute("class").contains("enabling");
                }
            });
            // Submit changes
            modulesPage.clickButtonSaveConfiguration();
            assertTextOfMessage(modulesPage.getMessage(), ".*The configuration options have been saved.");
        }
    }
    public static void enableModulesPeerAssignmentCoreAndNavigation() throws Exception {
        ModulesPage modulesPage = new ModulesPage(webDriver, pageLocation);
        // If switch has OFF state switch it ON
        waitUntilElementIsVisible(modulesPage.getSwitchPeerAssignmentCore());
        if (modulesPage.getSwitchPeerAssignmentCore().getAttribute("class").contains("off")) {
            modulesPage.getSwitchPeerAssignmentCore().click();
            final WebElement checkboxCore = modulesPage.getSwitchPeerAssignmentCore();
            waitCondition(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    return checkboxCore.findElement(By.xpath("./../..")).getAttribute("class").contains("enabling");
                }
            });
            // Submit changes
            modulesPage.clickButtonSaveConfiguration();
            assertTextOfMessage(modulesPage.getMessage(), ".*The configuration options have been saved.");
        }
        waitUntilElementIsVisible(modulesPage.getSwitchPeerAssignmentNavigation());
        if (modulesPage.getSwitchPeerAssignmentNavigation().getAttribute("class").contains("off")) {
            modulesPage.getSwitchPeerAssignmentNavigation().click();
            final WebElement checkboxNavigation = modulesPage.getSwitchPeerAssignmentNavigation();
            waitCondition(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    return checkboxNavigation.findElement(By.xpath("./../..")).getAttribute("class").contains("enabling");
                }
            });
            // Submit changes
            modulesPage.clickButtonSaveConfiguration();
            assertTextOfMessage(modulesPage.getMessage(), ".*The configuration options have been saved.");
        }
    }
    public void createPeerAssignmentFromClassOverview(boolean GDrive, String templatePath) throws Exception {
        // Open 'Create peer assignment' page from the Class overview page
        webDriver.get(websiteUrl);
        waitForPageToLoad();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewPage overviewPage = homePage.selectClassFromDropdown(className);
        overviewPage.clickButtonAddContent();
        CreatePeerAssignmentPage createPeerAssignmentPage = overviewPage.clickLinkPeerAssignmentOnPopup();
        // Check that proper Class is in 'Your groups' field
        createPeerAssignmentPage.checkProperClassInGroupAudience(className);

        // Enter data into "General settings" page
        createPeerAssignmentPage.fillFieldTitle(titlePA);
        createPeerAssignmentPage.fillFieldSummary(summaryPA);
        // Select 'Coaching' option in the Chapter field
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldChapter(), coaching);
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldLengthHours(), "3");
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldLengthMinutes(), "45");
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldNumberOfReviews(), "1");

        // Select Deadlines
        createPeerAssignmentPage.switchToTabDeadlines();
        // Draft submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldDraftSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldDraftSubmissionDeadlineTime(), 1);
        // Review submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldReviewSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldReviewSubmissionDeadlineTime(), 1);
        // Final submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldFinalSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldFinalSubmissionDeadlineTime(), 1);

        // Fill in 'What do you need to do' iframe in Assignment context
        createPeerAssignmentPage.switchToTabAssignmentContext();
        fillFrame(createPeerAssignmentPage.getIframeWhatDoYouNeedToDo(), whatDoYouNeedToDoText);

        // Select Template on "Assignment resources"
        createPeerAssignmentPage.switchToTabTaskResources();
        if (GDrive) { // condition, that checks if we'll use storage account or not
            createPeerAssignmentPage.selectStorageAccount(accountName);
        }
        createPeerAssignmentPage.fillFieldTitleTemplateSolution(titleTemplateSolution);

        createPeerAssignmentPage.selectTemplate(templatePath);

        // Enter data into 'Assignment cases'
        createPeerAssignmentPage.switchToTabAssignmentCases();
        fillFrame(createPeerAssignmentPage.getIframeAssignmentCase(), assignmentCase1);

        ViewPeerAssignmentPage viewPeerAssignmentPage = createPeerAssignmentPage.clickButtonSaveTop();

        // Check that Peer Assignment is created
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPeerAssignmentPage.getLinkEdit().isDisplayed());
    }
    public void createDraftSubmission(List<String> user) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.clickButtonCreateDraftSubmission();
        // Switch to google window, close tab
        String defaultWindow = webDriver.getWindowHandle();
        switchFromFirstPageToSecond(defaultWindow);
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.titleContains("DRAFT-" + user.get(0))); // Google doc is opened
        webDriver.close();
        // Change focus on the default tab
        webDriver.switchTo().window(defaultWindow);
    }
    public WebElement getLinkByName(String name) { // link on View Course component page
        String xpathLink = "//*[@id='mobileMenu']/div[1]/div[1]//div[contains(@class, 'type-link-field')]" +
                "//a[text()='" + name + "']";
        WebElement link = webDriver.findElement(By.xpath(xpathLink));
        waitUntilElementIsClickable(link);
        return link;
    }
    public int countOptionsInList(WebElement field) {
        List<WebElement> options = field.findElements(By.tagName("option"));
        return options.size();
    }
    public void runCron() throws Exception {
        webDriver.get(websiteUrl.concat("/admin/config/system/cron"));
        waitUntilPageIsOpenedWithTitle("Cron");
        CronPage cronPage = new CronPage(webDriver, pageLocation);
        cronPage.clickButtonRunCron();
        WebDriverWait wait = new WebDriverWait(webDriver, 60);
        wait.until(ExpectedConditions.visibilityOf(cronPage.getMessage()));
        assertTextOfMessage(cronPage.getMessage(), ".*Cron run successfully.");
    }
    public String addImageToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent();
        // Click the link 'Image' on a popup
        ViewImagePage viewImagePage = overviewPage.clickLinkImage();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        return viewImagePage.getFieldTitle().getText();
    }
    public String addMediaPageToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent2();
        // Click the link 'Media page' on a popup
        ViewMediaPage viewMediaPage = overviewPage.clickLinkMediaPage();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page rich_media_page temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMediaPage.getLinkEdit().isDisplayed());
        return viewMediaPage.getFieldTitle().getText();
    }
    public String addLiveSessionToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent2();
        // Click the link 'Live session' on a popup
        ViewLiveSessionPage viewLiveSessionPage = overviewPage.clickLinkLiveSession();
        assertTextOfMessage(viewLiveSessionPage.getMessage(), ".*Live session live_session temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewLiveSessionPage.getLinkEdit().isDisplayed());
        return viewLiveSessionPage.getFieldTitle().getText();
    }
    public String addInfoPageToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent2();
        // Click the link 'Info page' on a popup
        ViewInfoPage viewInfoPage = overviewPage.clickLinkInfoPage();
        assertTextOfMessage(viewInfoPage.getMessage(), ".*Info page info temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewInfoPage.getLinkEdit().isDisplayed());
        return viewInfoPage.getFieldTitle().getText();
    }
    public String addPresentationToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent();
        // Click the link 'Presentation' on a popup
        ViewPresentationPage viewPresentationPage = overviewPage.clickLinkPresentation();
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation presentation temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
        return viewPresentationPage.getFieldTitle().getText();
    }
    public String addTaskToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent();
        // Click the link 'Task' on a popup
        ViewTaskPage viewTaskPage = overviewPage.clickLinkTask();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task task temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTaskPage.getLinkEdit().isDisplayed());
        return viewTaskPage.getFieldTitle().getText();
    }
    public String addTheoryToClass() throws Exception {
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.clickButtonAddContent();
        // Click the link 'Theory' on a popup
        ViewTheoryPage viewTheoryPage = overviewPage.clickLinkTheory();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory theory temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTheoryPage.getLinkEdit().isDisplayed());
        return viewTheoryPage.getFieldTitle().getText();
    }
    public void clickLinkDownloadedCourseMaterialByName(String title) throws Exception {
        String xpathLink = "//*[@id='mobileMenu']/div[1]/div[1]//a[contains(@class, 'field-download-title')]" +
                "//div[text()='" + title + "']";
        WebElement link = webDriver.findElement(By.xpath(xpathLink));
        waitUntilElementIsClickable(link);
        clickItem(link, "The link of downloaded course material could not be clicked.");
    }
    public void openViewNodePageFromClassOverview(String title) throws Exception {
        waitForPageToLoad();
        WebElement linkNode = webDriver.findElement(By.xpath("//a/h2/div[text()='" + title + "']"));
        clickItem(linkNode, "The link of node could not be clicked on the Class overview page.");
        waitForPageToLoad();
    }
    public void waitUntilYoutubeVideoIsCompleted() throws Exception { // for intervals < 20 sec
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        mouseOverElement(mediaPopupPage.getVideoYoutube());
        // Wait until 'title' attribute appears ('replay' button)
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ytp-play-button[title]")));
    }
    public void clickYoutubeLargePlayButton() throws Exception {
        // Wait until large play button is displayed
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(".ytp-large-play-button")));
        WebElement playButton = webDriver.findElement(By.cssSelector(".ytp-large-play-button"));
        // Click the button
        clickItem(playButton, "The large 'play' button could not be clicked on Youtube media player.");
        // Wait until the button becomes invisible
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector(".ytp-large-play-button")));
    }
    public void waitUntilVimeoVideoIsCompleted() throws Exception { // only for video intervals <= 30 sec
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        WebDriverWait wait = new WebDriverWait(webDriver, 30);
        wait.until(ExpectedConditions.visibilityOf(mediaPopupPage.getButtonPlayVimeo()));
    }
//    public void waitUntilVimeoVideoPlaysSec(final int sec) {   // obsolete (see method below)
//        waitCondition(new ExpectedCondition<Boolean>() {
//            public Boolean apply(WebDriver webDriver) {
//                // If a progress bar is not displayed, move mouse over player container
//                WebElement container = webDriver.findElement(By.id("player"));
//                if (!(webDriver.findElement(By.cssSelector(".progress")).isDisplayed())) {
//                    mouseOverElement(container);
//                    WebDriverWait wait = new WebDriverWait(webDriver, 10);
//                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".progress")));
//                }
//                WebElement progressBar = webDriver.findElement(By.cssSelector(".progress"));
//                float secondsExpected = (float)sec;
//                // Wait until video is playing on the defined second
//                String secondsPlayed = progressBar.findElement(By.cssSelector(".played")).getAttribute("aria-valuenow");
//                float secondsActual = Float.parseFloat(secondsPlayed);
//                return (secondsExpected <= secondsActual) && (secondsActual <= secondsExpected + 0.7);
//            }
//        });
//    }
    public void waitUntilVimeoVideoPlaysSec(final int sec) {
        // Get 'played' progress bar
        final WebElement progressBarPlayed = webDriver.findElement(By.cssSelector(".progress .played"));
        waitCondition(new ExpectedCondition<Boolean>() { // wait until defined value of seconds is shown
            public Boolean apply(WebDriver webDriver) {
                String textPlayed = progressBarPlayed.getAttribute("aria-valuetext");
                String secondsPlayed = textPlayed.split(" second")[0];
                final int seconds = Integer.parseInt(secondsPlayed);
                return seconds == sec;
            }
        });
    }
    public void clickButtonPauseVimeo() throws Exception {
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        mouseOverElement(mediaPopupPage.getVideoVimeo());
        waitUntilElementIsClickable(mediaPopupPage.getButtonPauseVimeo());
        clickItem(mediaPopupPage.getButtonPauseVimeo(), "Pause button on VimeoMedia player could not be clicked.");
        waitUntilElementIsVisible(mediaPopupPage.getButtonPlayVimeo());
        Thread.sleep(2000); // before skipping
    }
    public void openViewMaterialPageFromContentTable(String title) throws Exception {
        waitForPageToLoad();
        WebElement linkMaterial = webDriver.findElement(By.xpath("//tr/td[2]/a[text()='" + title + "']"));
        clickItem(linkMaterial, "The link of material could not be clicked on the Content page.");
        waitForPageToLoad();
    }
    public String getNodeFromViewComponentPage() throws Exception {
        waitForPageToLoad();
        String nodeText = webDriver.findElement(By.cssSelector(".l-page.quickedit-init-processed .node"))
                .getAttribute("data-quickedit-entity-id");
        String node = nodeText.split("node/")[1];
        return node;
    }
    public String getNodeFromViewCoursePage() throws Exception {
        waitForPageToLoad();
        String nodeText = webDriver.findElement(By.cssSelector(".l-page .node"))
                .getAttribute("data-quickedit-entity-id");
        String node = nodeText.split("node/")[1];
        return node;
    }
    public void checkRegistrationSettingsIsOpen() throws Exception { // CreateClass page is opened
        waitForPageToLoad();
        CreateClassPage createClassPage = new CreateClassPage(webDriver, pageLocation);
        if (!(createClassPage.getFieldsetRegistrationSettings().isDisplayed())) {
            createClassPage.clickLinkRegistrationSettings();
            waitUntilElementIsVisible(createClassPage.getFieldsetRegistrationSettings());
        }
        Assert.assertTrue("Registration settings fieldset is not displayed.",
                createClassPage.getFieldsetRegistrationSettings().isDisplayed());
    }


//    protected static void assertElementActiveStatusStatic(WebElement element) throws Exception {
//        try {
//            Assert.assertEquals("active", element.getAttribute("class"));
//        } catch (Exception e) {
//            System.err.println("Element is not active.");
//        }
//    }

    /*protected static void installProfile() throws Exception {
        // Select "Iminds Academy" checkbox on Installation page, submit
        InstallationPage installationPage = new InstallationPage(webDriver, pageLocation);
        assertElementActiveStatusStatic(installationPage.getFieldChooseProfile());
        installationPage.selectCheckboxImindsAcademy();
        installationPage.clickButtonSaveAndContinue();

        // Submit "English" language
        waitForElementDisplayed(installationPage.getCheckboxEnglish());
        assertElementActiveStatusStatic(installationPage.getFieldChooseLanguage());
        installationPage.clickButtonSaveAndContinue();

        // Click installation link
        waitForElementDisplayed(installationPage.getTableReport());
        assertElementActiveStatusStatic(installationPage.getFieldVerifyRequirements());
        installationPage.clickLinkInstallation();

        // Fill in the fields for Database configuration, submit
        installationPage.fillFieldDatabaseName();

    }*/


}


