package com.inceptum.common;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.joda.time.Period;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.AdminProfilePage;
import com.inceptum.pages.BasePage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.RecentLogMessagesPage;
import com.inceptum.pages.TinCanApiPage;

/**
 * Created by Olga on 01.06.2015.
 */
public class TinCanBaseTest  extends TestBase {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final String objectType = "Activity";
    protected final String verbIdViewed = "http://adlnet.gov/expapi/verbs/viewed";
    protected final String verbIdFails = "http://adlnet.gov/expapi/verbs/fails";
    protected final String verbIdPassed = "http://adlnet.gov/expapi/verbs/passed";
    protected final String verbIdRated = "http://adlnet.gov/expapi/verbs/rated";
    protected final String verbIdCompleted = "http://adlnet.gov/expapi/verbs/completed";
    protected final String verbDisplayViewed = "viewed";
    protected final String verbDisplayWatched = "watched";
    protected final String verbDisplayPlay = "play";
    protected final String verbDisplayComplete = "complete"; // for video statement
    protected final String verbDisplayPaused = "paused";
    protected final String verbDisplaySkipped = "skipped";
    protected final String verbDisplayDownloaded = "downloaded";
    protected final String verbDisplayVisited = "visited";
    protected final String verbDisplayFails = "fails";
    protected final String verbDisplayProgressed = "progressed";
    protected final String verbDisplayReviewed = "reviewed";
    protected final String verbDisplayRated = "rated";
    protected final String verbDisplayCompleted = "completed"; // for PA statement

    protected final List<String> verbIdPlay = Arrays.asList("http://activitystrea.ms/schema/1.0/play", "http://adlnet.gov/expapi/verbs/play");
    protected final List<String> verbIdWatched = Arrays.asList("http://activitystrea.ms/schema/1.0/watch", "http://adlnet.gov/expapi/verbs/watched");
    protected final List<String> verbIdSkipped = Arrays.asList("http://activitystrea.ms/schema/1.0/skipped", "http://adlnet.gov/expapi/verbs/skipped");
    protected final List<String> verbIdPaused = Arrays.asList("http://activitystrea.ms/schema/1.0/paused", "http://adlnet.gov/expapi/verbs/paused");
    protected final List<String> verbIdComplete = Arrays.asList("http://activitystrea.ms/schema/1.0/complete", "http://adlnet.gov/expapi/verbs/complete");
    protected final List<String> videoPlayType = Arrays.asList("http://orw.iminds.be/tincan/media/video", "http://activitystrea.ms/schema/1.0/video");

    protected final String verbIdDownloaded = "http://adlnet.gov/expapi/verbs/downloaded";
    protected final String verbIdReviewed = "http://adlnet.gov/expapi/verbs/reviewed";
    protected final String verbIdVisited = "http://adlnet.gov/expapi/verbs/visited";
    protected final String videoViewType = "http://orw.iminds.be/tincan/content/type/video";
    protected final String imageType = "http://orw.iminds.be/tincan/content/type/image";
    protected final String presentationType = "http://orw.iminds.be/tincan/content/type/presentation";
    protected final String youtubeURL = "http://www.youtube.com/watch?v=OY5lkNRbMV4";  //6 sec     "https://www.youtube.com/watch?v=GO_hv-YFGXE"; - 1min 25sec
    protected final String youtubeURL2 = "https://www.youtube.com/watch?v=ji3bFv5QJGg"; // 15 sec
    protected final String vimeoURL = "http://vimeo.com/107319908"; // 9 sec

    protected boolean additional = true;


    /* ----- Methods ----- */

    public void checkTinCanApiPage() throws Exception {
        // Open TinCanApi configuration page
        webDriver.get(websiteUrl.concat("/admin/config/services/tincanapi"));
        waitForPageToLoad();
        TinCanApiPage tinCanApiPage = new TinCanApiPage(webDriver, pageLocation);

        // Check the case when fields are empty on the TinCanApi page, and fill them if necessary
        assertFieldIsNotEmpty(tinCanApiPage.getFieldEndpoint(), "Endpoint");
        assertFieldIsNotEmpty(tinCanApiPage.getFieldUser(), "userName");
        assertFieldIsNotEmpty(tinCanApiPage.getFieldPassword(), "userPassword");

        // Check that Watchdog is enabled
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxLogServerResponse()); // is not enabled on production (should be selected)
                                                                                // 'Track anonymous users', 'Simplify statements IDs' are enabled on production (should be selected)
        // Check that correct content is enabled
        tinCanApiPage.clickLinkContentTypes();
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxClass());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxImage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxPresentation());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxVideo());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxFullContent());

        tinCanApiPage.clickLinkIminds();
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxEmbedContent()); // PILOT-1681 !!!
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxInfoPage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxLiveSession());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxPeerAssignment());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxMediaPage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxTaskPage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxTheoryPage());
        // Save changes
        tinCanApiPage.clickButtonSaveConfiguration();
        assertMessage(tinCanApiPage.getMessage(), "messages status", ".*The configuration options have been saved.");
    }
    public List<String> getListOfStatements() throws Exception {
        // Open "Recent log messages" page
        waitUntilPageIsOpenedWithTitle("Recent log messages");
        // Get list of tincanapi statements which start with 'request'
        String xpath = "//*/table/tbody/tr/td[4]/a[contains(text(), 'request')]";
        List<WebElement> messages = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> links = new ArrayList<String>(); // Get links of tincanapi messages
        for (WebElement message : messages) {
            links.add(message.getAttribute("href"));
        }
        ArrayList<String> statements = new ArrayList<String>(); // Get text of the statements
        for (String url : links) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String statement = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            statements.add(statement);
        }
        return statements;
    }

    public String getName() throws Exception { // tincan
        AdminProfilePage adminProfilePage = new AdminProfilePage(webDriver, pageLocation);
        webDriver.get(websiteUrl.concat("/user/1/edit"));
        waitForPageToLoad();
        String name = adminProfilePage.getName();
        return name;
    }
    public String getEmailAddress() throws Exception { // tincan
        AdminProfilePage adminProfilePage = new AdminProfilePage(webDriver, pageLocation);
        webDriver.get(websiteUrl.concat("/user/1/edit"));
        waitForPageToLoad();
        String emailAddress = adminProfilePage.getEmailAddress();
        return emailAddress;
    }
    public void checkGroupingViewValues(String logMessage, String id, String name, String objectType) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");

        // Check 'id' value
        Map<String, Object> contextActivities = (Map)context.get("contextActivities");
        Map<String, Object> contextGrouping = (Map)contextActivities.get("grouping");
        String contextID = (String)contextGrouping.get("id");
        Assert.assertTrue("ID value '" + contextID + "' of key 'context' is not equal to '" + id + "'", contextID.equals(id));

        // Check 'name' value
        Map<String, Object> contextDefinition = (Map)contextGrouping.get("definition");
        Map<String, Object> contextName = (Map)contextDefinition.get("name");
        String contextEnUS = (String)contextName.get("en-US");
        Assert.assertTrue("Name value '" + contextEnUS + "' of key 'context' is not equal to '" + name + "'", contextEnUS.equals(name));

        // Check 'objectType' value
        String objectTypeValue = (String)contextGrouping.get("objectType");
        Assert.assertTrue("ObjectType value '" + objectTypeValue + "' of key 'context' is not equal to '" + objectType + "'", objectTypeValue.equals(objectType));
    }
    public void checkActorValues(String logMessage, String name, String mbox) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'actor' value
        Map<String, Object> actor = (Map)post.get("actor");

        // Check 'name' value
        String actorName = (String)actor.get("name");
        Assert.assertTrue("Name value '" + actorName + "' of key 'actor' is not equal to '" + name + "'", actorName.equals(name));
        // Check 'mbox' value
        String actorMbox = (String)actor.get("mbox");
        Assert.assertTrue("Mbox value '" + actorMbox + "' of key 'actor' is not equal to '" + mbox + "'", actorMbox.equals(mbox));
    }
    public void checkVerbValues(String logMessage, String id, String display) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'verb' value
        Map<String, Object> verb = (Map)post.get("verb");

        // Check 'id' value
        String verbID = (String)verb.get("id");
        Assert.assertTrue("ID value '" + verbID + "' of key 'verb' is not equal to '" + id + "'", verbID.equals(id));

        // Check 'display' value
        Map<String, Object> verbDisplay = (Map)verb.get("display");
        String verbEnUS = (String)verbDisplay.get("en-US");
        Assert.assertTrue("Display value '" + verbEnUS + "' of key 'verb' is not equal to '" + display + "'", verbEnUS.equals(display));
    }
    public void checkVideoVerbValues(String logMessage, List<String> id, String display) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'verb' value
        Map<String, Object> verb = (Map)post.get("verb");

        // Check 'id' value - two values are possible
        String verbID = (String)verb.get("id");
        Assert.assertTrue("ID value '" + verbID + "' of key 'verb' is incorrect.", id.contains(verbID));

        // Check 'display' value
        Map<String, Object> verbDisplay = (Map)verb.get("display");
        String verbEnUS = (String)verbDisplay.get("en-US");
        Assert.assertTrue("Display value '" + verbEnUS + "' of key 'verb' is not equal to '" + display + "'", verbEnUS.equals(display));
    }
    public void checkObjectViewValues(String logMessage, String id, String name, String type) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'id' value
        String objectID = (String)object.get("id");
        Assert.assertTrue("ID value '" + objectID + "' of key 'object' is not equal to '" + id + "'", objectID.equals(id));

        // Check 'name' value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        Map<String, Object> objectName = (Map)objectDefinition.get("name");
        String objectEnUS = (String)objectName.get("en-US");
        Assert.assertTrue("Name value '" + objectEnUS + "' of key 'object' is not equal to '" + name + "'", objectEnUS.equals(name));

        // Check 'type' value
        String typeValue = (String)objectDefinition.get("type");
        Assert.assertTrue("Type value '" + typeValue + "' of key 'object' is not equal to '" + type + "'", typeValue.equals(type));
    }

    public void checkExtensionsValues(String logMessage, String id, String name, String order) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");

        // Check 'id' value
        String chapterID = (String)extensions.get("http://orw.iminds.be/tincan/chapterId");
        Assert.assertTrue("ChapterID value '" + chapterID + "' of key 'extensions' is not equal to '" + id + "'", chapterID.equals(id));
        // Check 'name' value
        String chapterName = (String)extensions.get("http://orw.iminds.be/tincan/chapterName");
        Assert.assertTrue("ChapterName value '" + chapterName + "' of key 'extensions' is not equal to '" + name + "'", chapterName.equals(name));
        // Check 'order' value
        Object chapterOrderValue = extensions.get("http://orw.iminds.be/tincan/chapterOrder");
        String chapterOrder = (chapterOrderValue == null) ? null : chapterOrderValue.toString();
        Assert.assertTrue("ChapterOrder value '" + chapterOrder + "' of key 'extensions' is not equal to '" + order + "'", chapterOrder.equals(order));
    }
    public void checkAuthorValue(String logMessage, String mbox) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");
        // Check 'author' value
        String authorMbox = (String)extensions.get("http://orw.iminds.be/tincan/author");
        Assert.assertTrue("Author value '" + authorMbox + "' of key 'extensions' is not equal to '" + mbox + "'", authorMbox.equals(mbox));
    }
    public void checkFinalValue(String logMessage, String true1) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");

        // Check 'true' value
        Object booleanValueObject = extensions.get("http://orw.iminds.be/tincan/final");
        String booleanValue = (booleanValueObject == null) ? null : booleanValueObject.toString();
        Assert.assertTrue("'Final' value '" + booleanValue + "' of key 'extensions' is not equal to '" + true1 + "'", booleanValue.equals(true1));
    }
    public int getDurationYoutubeVideo() throws Exception {
        // Switch to media frame
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getIframeYoutubeVideo());
        webDriver.switchTo().frame(basePage.getIframeYoutubeVideo());

        // Get video duration
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ytp-time-duration")));
        WebElement fieldDuration = webDriver.findElement(By.cssSelector(".ytp-time-duration"));
        String script = "return arguments[0].innerText"; // js script for extracting text from the hidden element
        String durationText = (String) ((JavascriptExecutor) webDriver).executeScript(script, fieldDuration);

        webDriver.switchTo().defaultContent();
        // Separate the value into minutes, seconds
        String[] parts = durationText.split(":");
        int minutes=Integer.parseInt(parts[0]);
        int seconds=Integer.parseInt(parts[1]);
        // Count seconds
        int duration = seconds + (60 * minutes);
        return duration;
    }
    public int getDurationVimeoVideo() throws Exception {
        // Switch to media frame
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getIframeVimeoVideo());
        webDriver.switchTo().frame(basePage.getIframeVimeoVideo());
        WebElement fieldDuration = webDriver.findElement(By.cssSelector("div.loaded"));

        // Get time in seconds (rounded)
        String durationText = fieldDuration.getAttribute("aria-valuemax");
        int duration=Integer.parseInt(durationText); // duration in seconds
        webDriver.switchTo().defaultContent();
        return duration;
    }
    public int getCurrentTimeYoutubeVideo() throws Exception {
        // Switch to media frame and wait until current time is shown
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        WebElement fieldCurrentTime = webDriver.findElement(By.cssSelector(".ytp-time-current"));
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.visibilityOf(fieldCurrentTime));

        // Get time and convert it to seconds
        String currentTimeText = fieldCurrentTime.getText();
        webDriver.switchTo().defaultContent();
        // Separate the value into minutes, seconds
        String[] parts = currentTimeText.split(":");
        int minutes=Integer.parseInt(parts[0]);
        int seconds=Integer.parseInt(parts[1]);
        // Count seconds
        int currentTime = seconds + (60 * minutes);
        return currentTime;
    }
    public void clickButtonPauseYoutube() throws Exception {
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        mouseOverElement(mediaPopupPage.getVideoYoutube());
        // Get value of the attribute
        final String value = webDriver.findElement(By.cssSelector(".ytp-play-button")).getAttribute("aria-label");
        // Execute the script to click on the button
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, mediaPopupPage.getButtonPlayPauseYoutube());
        // Wait until 'play' button appears (value of the attribute will be changed)
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                String valueNew = webDriver.findElement(By.cssSelector(".ytp-play-button")).getAttribute("aria-label");
                return (!(valueNew == value));
            }
        });
        Thread.sleep(2000); // before skipping
    }
    public void clickButtonPlayYoutube() throws Exception {
        Thread.sleep(2000); // after skipping
        // Wait until 'play' button is present
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ytp-play-button")));
        // Get value of the attribute
        final String value = webDriver.findElement(By.cssSelector(".ytp-play-button")).getAttribute("aria-label");
        // Execute the script to click on the button
        String script = "arguments[0].click();";
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        ((JavascriptExecutor) webDriver).executeScript(script, mediaPopupPage.getButtonPlayPauseYoutube());
        // Wait until 'pause' button appears (value of the attribute will be changed)
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                String valueNew = webDriver.findElement(By.cssSelector(".ytp-play-button")).getAttribute("aria-label");
                return (!(valueNew == value));
            }
        });
    }
    public void clickButtonPlayVimeo() throws Exception {
        Thread.sleep(2000); // after skipping
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        mouseOverElement(mediaPopupPage.getVideoVimeo());
        waitUntilElementIsClickable(mediaPopupPage.getButtonPlayVimeo());
        clickItem(mediaPopupPage.getButtonPlayVimeo(), "Play button on VimeoMedia player could not be clicked.");
        waitUntilElementIsVisible(mediaPopupPage.getButtonPauseVimeo());
    }

    // The method should be used only for generating video (main content) statements from PeerTask View page
    // (without viewing video statement)
    public void checkVideoPausedStatementsWithoutNode(List<String> statements, String url, String videoName,
                                                      String classNodeUrl, String className, String parentNode,
                                           List<Integer> durationArray, List<Integer> startPointArray,
                                           List<Integer> pausePointArray, List<Integer> endPointArray) throws Exception {
        // Get actorName, actorMbox
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        // Specify list of verbs, that should be included in the statements
        // (here a copy of the List is made, so it's possible to remove an item from the List)
        List<String> verbs = new ArrayList<String>(Arrays.asList(verbDisplayPlay, verbDisplayWatched,
                verbDisplayPaused, verbDisplayPlay, verbDisplayWatched, verbDisplayComplete, verbDisplayPaused));

        // Find a statement that includes a certain verb, verify the statement, remove the verb from the list of specified verbs
        for (int i = 0; i < 7; i++) {
            String currentStatement = statements.get(i);
            String currentVerb = getDisplayVerb(currentStatement);
            // if 'verb = play || watched', get start point (seconds)
            int startPoint = 0;
            if (currentVerb.equals(verbDisplayPlay) || currentVerb.equals(verbDisplayWatched)) {
                startPoint = getStartPoint(currentStatement);
            }
            // if 'verb = paused', get end point (seconds)
            int endPoint = 0;
            if (currentVerb.equals(verbDisplayPaused)) {
                endPoint = getEndPoint(currentStatement);
            }

            if (currentVerb.equals(verbDisplayPlay) && startPointArray.contains(startPoint)) { // play (before pause)
                checkVideoPlayStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, startPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && startPointArray.contains(startPoint)) { // watched (before pause)
                checkVideoWatchedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, startPointArray, pausePointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && pausePointArray.contains(endPoint)) { // paused
                checkVideoPausedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, pausePointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPlay) && pausePointArray.contains(startPoint)) { // play (after pause)
                checkVideoPlayStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, pausePointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && pausePointArray.contains(startPoint)) { // watched (after pause)
                checkVideoWatchedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, pausePointArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayComplete)) { // complete
                checkVideoCompleteStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && endPointArray.contains(endPoint)) { // paused (video complete)
                checkVideoPausedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            }
        }
        int size = verbs.size(); // number of wrong statements
        String number = Integer.toString(size);
        // Check that the statements include all specified verbs, so the result of this method should be 'verbs quantity = 0'
        Assert.assertTrue("Statements are not generated correctly: " + number + " statements are wrong.", size == 0);
    }
    protected String getDisplayVerb(String logMessage) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'verb' value
        Map<String, Object> verb = (Map)post.get("verb");
        Map<String, Object> verbDisplay = (Map)verb.get("display");
        String verbEnUS = (String)verbDisplay.get("en-US");
        return verbEnUS;
    }
    public int getStartPoint(String statement) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");
        // Check 'starting-point' value
        String startPointValue = (String)extensions.get("http://orw.iminds.be/tincan/starting-point"); // or "http://id.tincanapi.com/extension/starting-point"
        long startPointSeconds = Period.parse(startPointValue).toStandardDuration().getStandardSeconds(); // get actual start point
        Integer seconds = (int) (long) startPointSeconds; // convert into Integer
        return seconds;
    }
    public int getEndPoint(String statement) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");
        // Check 'starting-point' value
        String endPointValue = (String)extensions.get("http://orw.iminds.be/tincan/ending-point"); // or "http://id.tincanapi.com/extension/ending-point”
        long endPointSeconds = Period.parse(endPointValue).toStandardDuration().getStandardSeconds(); // get actual end point
        Integer seconds = (int) (long) endPointSeconds; // convert into Integer
        return seconds;
    }
    public void checkVideoViewedStatement(String statement, String actorName, String actorMbox, String videoNodeUrl,
                                          String videoName, String classNodeUrl, String className, String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(statement, videoNodeUrl, videoName, videoViewType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        checkParentValues(statement, parentId, objectType);
    }
    public void checkVideoPlayStatement(String statement, String actorName, String actorMbox, String url, String videoName,
                                        String videoNodeUrl, String classNodeUrl, String className, String parentNode,
                                        List<Integer> durationArray, List<Integer> startPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdPlay, verbDisplayPlay);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = videoNodeUrl.concat("?parent=" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeStartingPoint(statement, startPointArray); // check start point
    }
    public void checkVideoPlayStatementWithoutNode(String statement, String actorName, String actorMbox, String url, // for PA
                                                   String videoName, String classNodeUrl, String className,
                                                   String parentNode, List<Integer> durationArray,
                                                   List<Integer> startPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdPlay, verbDisplayPlay);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = websiteUrl.concat("/node/" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeStartingPoint(statement, startPointArray); // check start point
    }
    public void checkVideoWatchedStatement(String statement, String actorName, String actorMbox, String url, String videoName,
                                           String videoNodeUrl, String classNodeUrl, String className, String parentNode,
                                           List<Integer> durationArray, List<Integer> startPointArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdWatched, verbDisplayWatched);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = videoNodeUrl.concat("?parent=" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeStartingPoint(statement, startPointArray); // check start point
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoWatchedStatementWithoutNode(String statement, String actorName, String actorMbox, String url, String videoName,
                                           String classNodeUrl, String className, String parentNode,
                                           List<Integer> durationArray, List<Integer> startPointArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdWatched, verbDisplayWatched);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = websiteUrl.concat("/node/" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeStartingPoint(statement, startPointArray); // check start point
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoSkippedStatement(String statement, String actorName, String actorMbox, String url, String videoName,
                                           String videoNodeUrl, String classNodeUrl, String className, String parentNode,
                                           List<Integer> durationArray, List<Integer> startPointArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdSkipped, verbDisplaySkipped);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = videoNodeUrl.concat("?parent=" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeStartingPoint(statement, startPointArray); // check start point
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoSkippedStatementWithoutNode(String statement, String actorName, String actorMbox, String url, String videoName,
                                           String classNodeUrl, String className, String parentNode, List<Integer> durationArray,
                                           List<Integer> startPointArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdSkipped, verbDisplaySkipped);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = websiteUrl.concat("/node/" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeStartingPoint(statement, startPointArray); // check start point
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoPausedStatement(String statement, String actorName, String actorMbox, String url, String videoName,
                                          String videoNodeUrl, String classNodeUrl, String className, String parentNode,
                                          List<Integer> durationArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdPaused, verbDisplayPaused);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = videoNodeUrl.concat("?parent=" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoPausedStatementWithoutNode(String statement, String actorName, String actorMbox, String url, String videoName,
                                          String classNodeUrl, String className, String parentNode,
                                          List<Integer> durationArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdPaused, verbDisplayPaused);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = websiteUrl.concat("/node/" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoCompleteStatement(String statement, String actorName, String actorMbox, String url, String videoName,
                                            String videoNodeUrl, String classNodeUrl, String className, String parentNode,
                                            List<Integer> durationArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdComplete, verbDisplayComplete);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = videoNodeUrl.concat("?parent=" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    public void checkVideoCompleteStatementWithoutNode(String statement, String actorName, String actorMbox, String url, String videoName,
                                            String classNodeUrl, String className, String parentNode,
                                            List<Integer> durationArray, List<Integer> endPointArray) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVideoVerbValues(statement, verbIdComplete, verbDisplayComplete);
        // Key 'object'
        checkObjectVideoViewValues(statement, url, videoName, videoPlayType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'parent'
        String parentId = websiteUrl.concat("/node/" + parentNode);
        checkParentValues(statement, parentId, objectType);
        // Key 'extensions'
        checkExtensionsTimeDuration(statement, durationArray); // check duration
        checkExtensionsTimeEndingPoint(statement, endPointArray); // check end point
    }
    protected void checkParentValues(String logMessage, String parentId, String objectType) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");

        // Check 'id' value
        Map<String, Object> contextActivities = (Map)context.get("contextActivities");
        Map<String, Object> parent = (Map)contextActivities.get("parent");
        String id = (String)parent.get("id");
        Assert.assertTrue("ParentID value '" + id + "' of key 'context' is not equal to '" + parentId + "'", id.equals(parentId));

        // Check 'objectType' value
        String objectTypeValue = (String)parent.get("objectType");
        Assert.assertTrue("ObjectType value '" + objectTypeValue + "' of key 'context' is not equal to '" + objectType + "'", objectTypeValue.equals(objectType));
    }
    protected void checkObjectVideoViewValues(String logMessage, String url, String name, List<String> type) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'id' value
        String[] parts = url.split("://"); // Separate needed node URL (expected)
        url = parts[1];
        String objectID = (String)object.get("id");
        parts = objectID.split("://"); // Separate needed node URL (actual)
        objectID = parts[1];
        Assert.assertTrue("ID value '" + objectID + "' of key 'object' is not equal to '" + url + "'", objectID.equals(url));

        // Check 'name' value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        Map<String, Object> objectName = (Map)objectDefinition.get("name");
        String objectEnUS = (String)objectName.get("en-US");
        Assert.assertTrue("Name value '" + objectEnUS + "' of key 'object' is not equal to '" + name + "'", objectEnUS.equals(name));

        // Check 'type' value
        String typeValue = (String)objectDefinition.get("type");
        Assert.assertTrue("Type value '" + typeValue + "' of key 'object' is incorrect.", type.contains(typeValue));
    }

    protected void checkExtensionsTimeDuration(String statement, List<Integer> durationArray) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");

        // Check 'duration' value
        String durationValue = (String)extensions.get("http://orw.iminds.be/tincan/length"); // or "http://id.tincanapi.com/extension/duration"
        long durationSeconds = Period.parse(durationValue).toStandardDuration().getStandardSeconds(); // get actual duration
        Integer seconds = (int) (long) durationSeconds; // convert into Integer
        Assert.assertTrue("Actual duration of key 'extensions' is not equal to expected duration.",
                durationArray.contains(seconds));
    }
    protected void checkExtensionsTimeStartingPoint(String statement, List<Integer> startPointArray) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");

        // Check 'starting-point' value
        String startPointValue = (String)extensions.get("http://orw.iminds.be/tincan/starting-point"); // or "http://id.tincanapi.com/extension/starting-point"
        long startPointSeconds = Period.parse(startPointValue).toStandardDuration().getStandardSeconds(); // get actual start point
        Integer seconds = (int) (long) startPointSeconds; // convert into Integer
        Assert.assertTrue("Actual start point of key 'extensions' is not equal to expected start point.",
                startPointArray.contains(seconds));
    }
    protected void checkExtensionsTimeEndingPoint(String statement, List<Integer> endPointArray) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");

        // Check 'ending-point' value
        String endPointValue = (String)extensions.get("http://orw.iminds.be/tincan/ending-point"); // or "http://id.tincanapi.com/extension/ending-point"
        long endPointSeconds = Period.parse(endPointValue).toStandardDuration().getStandardSeconds(); // get actual end point
        Integer seconds = (int) (long) endPointSeconds; // convert into Integer
        Assert.assertTrue("Actual end point of key 'extensions' is not equal to expected end point.",
                endPointArray.contains(seconds));
    }
    protected String getVideoYoutubeName() throws Exception {
        // Switch to media frame
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getIframeYoutubeVideo());
        webDriver.switchTo().frame(basePage.getIframeYoutubeVideo());
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("a[class*='title-link']")));
        WebElement fieldName = webDriver.findElement(By.cssSelector("a[class*='title-link']"));
        String script = "return arguments[0].innerText"; // js script for extracting text from the hidden element
        // Get video name
        String videoName = (String) ((JavascriptExecutor) webDriver).executeScript(script, fieldName);
        // Switch to default content
        webDriver.switchTo().defaultContent();
        return videoName;
    }
    protected String getVideoVimeoName() throws Exception {
        // Switch to media frame
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getIframeVimeoVideo());
        webDriver.switchTo().frame(basePage.getIframeVimeoVideo());
        // Wait until name presences in HTML document
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".title h1 a")));
        WebElement fieldName = webDriver.findElement(By.cssSelector(".title h1 a"));
        String script = "return arguments[0].innerText"; // js script for extracting text from the hidden element
        // Get video name
        String videoName = (String) ((JavascriptExecutor) webDriver).executeScript(script, fieldName);
        // Switch to default content
        webDriver.switchTo().defaultContent();
        return videoName;
    }
    public void skipPartOfVimeoVideo(int widthPercentage) throws Exception {
        // Get whole video width
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        mouseOverElement(mediaPopupPage.getVideoVimeo()); // move mouse over video player to show progress bar
        waitUntilElementIsVisible(mediaPopupPage.getProgressBarVimeo());
        WebElement progressBar = mediaPopupPage.getProgressBarVimeo();
        double width = (double)progressBar.getSize().getWidth();
        double percentage = (double)widthPercentage/100; // convert percentage to decimal
        // Count end point of skipping
        double endPointDouble = width*percentage;
        int endPoint = (int)endPointDouble; // convert to int
        // Move mouse to the point and click (skip the part of video - counted from the beginning of the video!)
        new Actions(webDriver).moveToElement(progressBar, endPoint, 0).click().build().perform();
    }
    public void checkVideoSkippedStatementsWithoutNode(List<String> statements, String url,  // for PA only (without viewing video)
                                            String videoName, String classNodeUrl, String className,
                                            String parentNode, List<Integer> durationArray, List<Integer> startPointArray,
                                            List<Integer> startSkipPointArray, List<Integer> endSkipPointArray,
                                            List<Integer> endPointArray) throws Exception {
        // Get actorName, actorMbox
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        // Specify list of verbs, that should be included in the statements
        // (here a copy of the List is made, so it's possible to remove an item from the List)
        List<String> verbs = new ArrayList<String>(Arrays.asList(verbDisplayPlay, verbDisplayWatched,
                verbDisplayPaused, verbDisplaySkipped, verbDisplayPaused, verbDisplayPlay, verbDisplayPaused,
                verbDisplayWatched, verbDisplayComplete));

        // Find a statement that includes a certain verb, verify the statement, remove the verb from the list of specified verbs
        for (int i = 0; i < 9; i++) {
            String currentStatement = statements.get(i);
            String currentVerb = getDisplayVerb(currentStatement);
            // if 'verb = play || watched', get start point (seconds)
            int startPoint = 0;
            if (currentVerb.equals(verbDisplayPlay) || currentVerb.equals(verbDisplayWatched)) {
                startPoint = getStartPoint(currentStatement);
            }
            // if 'verb = paused', get end point (seconds)
            int endPoint = 0;
            if (currentVerb.equals(verbDisplayPaused)) {
                endPoint = getEndPoint(currentStatement);
            }

            if (currentVerb.equals(verbDisplayPlay) && startPointArray.contains(startPoint)) { // play (before pause)
                checkVideoPlayStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, startPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && startPointArray.contains(startPoint)) { // watched (before pause)
                checkVideoWatchedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, startPointArray, startSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && startSkipPointArray.contains(endPoint)) { // paused
                checkVideoPausedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, startSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplaySkipped)) { // skipped
                checkVideoSkippedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, startSkipPointArray, endSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && endSkipPointArray.contains(endPoint)) { // paused
                checkVideoPausedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPlay) && endSkipPointArray.contains(startPoint)) { // play (after pause)
                checkVideoPlayStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && endSkipPointArray.contains(startPoint)) { // watched (after pause)
                checkVideoWatchedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endSkipPointArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayComplete)) { // complete
                checkVideoCompleteStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && endPointArray.contains(endPoint)) { // paused (video complete)
                checkVideoPausedStatementWithoutNode(currentStatement, actorName, actorMbox, url, videoName, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            }
        }
        int size = verbs.size(); // number of wrong statements
        String number = Integer.toString(size);
        // Check that the statements include all specified verbs, so the result of this method should be 'verbs quantity = 0'
        Assert.assertTrue("Statements are not generated correctly: " + number + " statements are wrong.", size == 0);
    }
    public void waitUntilYoutubeVideoPlaysSec(final int sec) throws Exception {
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                // If a progress bar is not displayed, move mouse over player container
                WebElement container = webDriver.findElement(By.id("player"));
                if (!(webDriver.findElement(By.cssSelector(".ytp-progress-bar")).isDisplayed())) {
                    mouseOverElement(container);
                    WebDriverWait wait = new WebDriverWait(webDriver, 10);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ytp-progress-bar")));
                }
                WebElement progressBar = webDriver.findElement(By.cssSelector(".ytp-progress-bar"));
                String seconds = Integer.toString(sec);
                // Wait until video is playing on the defined second
                return progressBar.getAttribute("aria-valuenow").equals(seconds);
            }
        });
    }
    public void skipPartOfYoutubeVideo() throws Exception {
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        mouseOverElement(mediaPopupPage.getVideoYoutube()); // to show progress bar
        mouseOverElement(mediaPopupPage.getProgressBarYoutube()); // to show slider
        waitUntilElementIsVisible(mediaPopupPage.getSliderYoutube());
        int endPoint = getYoutubeSkipEndPointPX(); // get 2/3 of whole video width
        // Move a slider to 2/3 of whole video width from the beginning
        new Actions(webDriver).dragAndDropBy(mediaPopupPage.getSliderYoutube(), endPoint, 0).perform();
    }
    public int getYoutubeSkipEndPointPX() {
        // Wait until progress bar is shown
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".ytp-progress-bar-container")));
        // Get whole video width
        String style = webDriver.findElement(By.cssSelector(".ytp-chrome-bottom")).getAttribute("style");
        String width = style.split("width: ")[1].split("px")[0]; // split width
        int widthPX = Integer.parseInt(width); // convert to int (px)
        // Count 2/3 of whole video width
        int shiftFromStartPX = (widthPX/3)*2;
        // Get width of watched video
        String style2 = webDriver.findElement(By.cssSelector("[class*='scrubber-button']")).getAttribute("style");
        String widthWatched = style2.split("left: ")[1].split("px")[0]; // split width
        float widthFloat = Float.parseFloat(widthWatched);
        int widthWatchedPX = (int)widthFloat; // convert to int (px)
        // Count end point (px)
        int endPoint = shiftFromStartPX - widthWatchedPX;
        return endPoint;
    }

    public  String getOnlyTextOfMessage() throws Exception{
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);

        String logMessage = recentLogMessagesPage.getTextOfLogMessage();
        return logMessage;

    }

    public String getParticularTinCanMessage(String verb, String id) throws Exception {
        String logMessage =null;
        boolean meaning=true;
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);
        openRecentLogMessagesPage();
        waitForPageToLoad();

        ArrayList<String> messages = new ArrayList<String>();
        List<WebElement> options = recentLogMessagesPage.getLogTable().findElements(By.xpath("//td[text()='tincanapi']/.././td[4]/a"));
        for (WebElement option : options){


            if (option.getText().startsWith("{\"request\":")) {
                messages.add(option.getAttribute("href"));}}
        for (String url : messages) {
            webDriver.get(url);
                 meaning=true;
                 logMessage=getOnlyTextOfMessage();
                if(checkMessageByIdAndVerb(logMessage, verb, id)){break;}
             meaning=false;
         }
        if(!meaning){throw new NoSuchContextException("There is no tinCanApi statement with verb ["+verb+"] and id ["+id+"]");}


        return logMessage;


    }


    public boolean checkMessageByIdAndVerb(String logMessage, String idverb, String id) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'verb' value
        boolean variable;
        try {
            boolean value=false;
            Map<String, Object> object = (Map)post.get("object");

            // Check 'id' value
            String objectID = (String)object.get("id");


        Map<String, Object> verb = (Map)post.get("verb");


        // Check 'display' value
        Map<String, Object> verbDisplay = (Map)verb.get("display");
        String verbEnUS = (String)verbDisplay.get("en-US");

        if(verbEnUS.equals(idverb)&&objectID.equals(id)) {value=true;}
            return value;
        }
        catch (Exception e){return false;}


    }

    public String getTextOfTinCanLogMessage() throws Exception {
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);
        openRecentLogMessagesPage();
        clickFirstTinCanApiLink();
        String logMessage = recentLogMessagesPage.getTextOfLogMessage();
        return logMessage;
    }
    public void clickFirstTinCanApiLink() throws Exception {
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);
        List<WebElement> options = recentLogMessagesPage.getLogTable().findElements(By.xpath("//td[text()='tincanapi']/.././td[4]/a"));
        for (WebElement option : options) {
            if (option.getText().startsWith("{\"request\":")) {
                option.click();
                break;
            }
        }
    }
    public void checkViewImageStatement(boolean additional, String logMessage, String imageNodeUrl, String imageName,
                                        String classNodeUrl, String className, String parentId) throws Exception {
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, imageNodeUrl, imageName, imageType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'parent'
        checkParentValues(logMessage, parentId, objectType);
        // Check key 'extensions' if the material is additional
        if (additional) {
            checkAdditionalValue(logMessage, "1");
        }
    }
    public void checkViewVideoStatement(boolean additional, String logMessage, String videoNodeUrl, String videoName,
                                        String classNodeUrl, String className, String parentId) throws Exception {
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, videoNodeUrl, videoName, videoViewType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'parent'
        checkParentValues(logMessage, parentId, objectType);
        // Check key 'extensions' if the material is additional
        if (additional) {
            checkAdditionalValue(logMessage, "1");
        }
    }
    public void checkViewPresentationStatement(boolean additional, String logMessage, String presentationNodeUrl,
                                               String presentationName, String classNodeUrl, String className,
                                               String parentId) throws Exception {
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, presentationNodeUrl, presentationName, presentationType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'parent'
        checkParentValues(logMessage, parentId, objectType);
        // Check key 'extensions' if the material is additional
        if (additional) {
            checkAdditionalValue(logMessage, "1");
        }
    }
    public void checkViewFileDownloadsStatement(boolean additional, String logMessage, String downloadsId,
                                               String downloadsTitle, String classNodeUrl, String className,
                                               String parentId) throws Exception {
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdDownloaded, verbDisplayDownloaded);
        // Key 'object'
        checkObjectDownloadValues(logMessage, downloadsId, downloadsTitle);
        // Key 'context'
        checkContextDownloadValues(logMessage, classNodeUrl, className, parentId, objectType);
        // Check key 'extensions' if the material is additional
        if (additional) {
            checkAdditionalValue(logMessage, "1");
        }
    }
    public void checkVisitLinkStatement(boolean additional, String logMessage, String linkUrl,
                                                String linkTitle, String classNodeUrl, String className,
                                                String parentId) throws Exception {
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdVisited, verbDisplayVisited);
        // Key 'object'
        checkObjectDownloadValues(logMessage, linkUrl, linkTitle);
        // Key 'context'
        checkContextDownloadValues(logMessage, classNodeUrl, className, parentId, objectType);
        // Check key 'extensions' if the material is additional
        if (additional) {
            checkAdditionalValue(logMessage, "1");
        }
    }
    public void checkAdditionalValue(String logMessage, String true1) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");
        // Get 'extensions' value
        Map<String, Object> extensions = (Map)context.get("extensions");

        // Check 'true' value
        Object booleanValueObject = extensions.get("http://orw.iminds.be/tincan/isAdditional");
        String booleanValue = (booleanValueObject == null) ? null : booleanValueObject.toString();
        Assert.assertTrue("'Additional' value '" + booleanValue + "' of key 'extensions' is not equal to '" + true1 + "'", booleanValue.equals(true1));
    }
    protected void checkObjectDownloadValues(String logMessage, String id, String name) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'id' value
        String objectID = (String)object.get("id");
        Assert.assertTrue("ID value '" + objectID + "' of key 'object' is not equal to '" + id + "'", objectID.equals(id));

        // Check 'name' value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        Map<String, Object> objectName = (Map)objectDefinition.get("name");
        String objectEnUS = (String)objectName.get("en-US");
        Assert.assertTrue("Name value '" + objectEnUS + "' of key 'object' is not equal to '" + name + "'", objectEnUS.equals(name));
    }
    protected void checkContextDownloadValues(String logMessage, String id, String name, String parentId, String objectType) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");

        // Check 'id' value (grouping)
        Map<String, Object> contextActivities = (Map)context.get("contextActivities");
        Map<String, Object> contextGrouping = (Map)contextActivities.get("grouping");
        String contextID = (String)contextGrouping.get("id");
        Assert.assertTrue("ID value '" + contextID + "' of key 'context' is not equal to '" + id + "'", contextID.equals(id));

        // Check 'name' value (grouping)
        Map<String, Object> contextDefinition = (Map)contextGrouping.get("definition");
        Map<String, Object> contextName = (Map)contextDefinition.get("name");
        String contextEnUS = (String)contextName.get("en-US");
        Assert.assertTrue("Name value '" + contextEnUS + "' of key 'context' is not equal to '" + name + "'", contextEnUS.equals(name));

        // Check 'objectType' value (grouping)
        String objectTypeValue = (String)contextGrouping.get("objectType");
        Assert.assertTrue("ObjectType value '" + objectTypeValue + "' of key 'context' is not equal to '" + objectType + "'", objectTypeValue.equals(objectType));

        // Check 'parentId' value
        Map<String, Object> contextParent = (Map)contextActivities.get("parent");
        String contextParentID = (String)contextParent.get("id");
        Assert.assertTrue("Parent ID value '" + contextParentID + "' of key 'context' is not equal to '" + parentId + "'", contextParentID.equals(parentId));

        // Check 'parentObjectType' value
        String parentObjectTypeValue = (String)contextParent.get("objectType");
        Assert.assertTrue("Parent ObjectType value '" + parentObjectTypeValue + "' of key 'context' is not equal to '" + objectType + "'", parentObjectTypeValue.equals(objectType));
    }

    public void checkVideoSkippedStatements(List<String> statements, String videoNodeUrl, String url, String videoTitle,
                                            String videoName, String classNodeUrl, String className, String parentId,
                                            String parentNode, List<Integer> durationArray, List<Integer> startPointArray,
                                            List<Integer> startSkipPointArray, List<Integer> endSkipPointArray,
                                            List<Integer> endPointArray) throws Exception {
        // Get actorName, actorMbox
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        // Specify list of verbs, that should be included in the statements
        // (here a copy of the List is made, so it's possible to remove an item from the List)
        List<String> verbs = new ArrayList<String>(Arrays.asList(verbDisplayViewed, verbDisplayPlay, verbDisplayWatched,
                verbDisplayPaused, verbDisplaySkipped, verbDisplayPaused, verbDisplayPlay, verbDisplayPaused,
                verbDisplayWatched, verbDisplayComplete));

        // Find a statement that includes a certain verb, verify the statement, remove the verb from the list of specified verbs
        for (int i = 0; i < 10; i++) {
            String currentStatement = statements.get(i);
            String currentVerb = getDisplayVerb(currentStatement);
            // if 'verb = play || watched', get start point (seconds)
            int startPoint = 0;
            if (currentVerb.equals(verbDisplayPlay) || currentVerb.equals(verbDisplayWatched)) {
                startPoint = getStartPoint(currentStatement);
            }
            // if 'verb = paused', get end point (seconds)
            int endPoint = 0;
            if (currentVerb.equals(verbDisplayPaused)) {
                endPoint = getEndPoint(currentStatement);
            }

            if (currentVerb.equals(verbDisplayViewed)) { // viewed
                checkVideoViewedStatement(currentStatement, actorName, actorMbox, videoNodeUrl, videoTitle, classNodeUrl,
                        className, parentId);
                verbs.remove(currentVerb); // remove the verb from 'verbs' list
            } else if (currentVerb.equals(verbDisplayPlay) && startPointArray.contains(startPoint)) { // play (before pause)
                checkVideoPlayStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && startPointArray.contains(startPoint)) { // watched (before pause)
                checkVideoWatchedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startPointArray, startSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && startSkipPointArray.contains(endPoint)) { // paused
                checkVideoPausedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplaySkipped)) { // skipped
                checkVideoSkippedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startSkipPointArray, endSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && endSkipPointArray.contains(endPoint)) { // paused
                checkVideoPausedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPlay) && endSkipPointArray.contains(startPoint)) { // play (after pause)
                checkVideoPlayStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endSkipPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && endSkipPointArray.contains(startPoint)) { // watched (after pause)
                checkVideoWatchedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endSkipPointArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayComplete)) { // complete
                checkVideoCompleteStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && endPointArray.contains(endPoint)) { // paused (video complete)
                checkVideoPausedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            }
        }
        int size = verbs.size(); // number of wrong statements
        String number = Integer.toString(size);
        // Check that the statements include all specified verbs, so the result of this method should be 'verbs quantity = 0'
        Assert.assertTrue("Statements are not generated correctly: " + number + " statements are wrong.", size == 0);
    }



}
