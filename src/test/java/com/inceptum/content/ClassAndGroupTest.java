package com.inceptum.content;


import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;

import com.inceptum.pages.GroupTabGroupPage;
import com.inceptum.pages.PeopleInGroupForGroupPage;

public class ClassAndGroupTest extends CourseBaseTest {



    /*---------CONSTANTS--------*/

    protected final String roleClassJury = "class jury";
    protected final String roleClassEditor = "class editor";


    /* ----- Tests ----- */

    @Test
    public void testCreateClass() throws Exception {
        addClass(className);
    }

    @Test
    public void testCreateGroup() throws Exception {
        checkClassIsAddedOnOverview(className);
        createGroup(groupName, className);
    }

    @Test
    public void testAddJuryMemberToClass() throws Exception {
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Check that the role 'class jury' was automatically added to the user (People table)
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleClassJury, juryMember);
    }

    @Test
    public void testAddContentEditorToClass() throws Exception {
        addUserToClassWithRole(contentEditor, className, roleContentEditor);
        // Check that the role 'class editor' was automatically added to the user (People table)
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleClassEditor, contentEditor);
    }

    @Test
    public void testAddUserToGroup() throws Exception {
        // Create user account and add it to the Class
        addStudentToClass(student, className);
        // Create a Group and add it to the Class
        createGroup(groupName, className);
        // Add the user account to the Group
        addUserToGroup(student, groupName);

        // Check the user name is displayed on Group overview page
        PeopleInGroupForGroupPage peopleInGroupForGroupPage = new PeopleInGroupForGroupPage(webDriver, pageLocation);
        GroupTabGroupPage groupTabGroupPage = peopleInGroupForGroupPage.clickLinkGroup();
        groupTabGroupPage.clickLinkPeople();
        waitUntilPageIsOpenedWithTitle("People");
        String studentXpath = "//table/tbody/tr/td[2]/a[text()='" + student.get(0) + "']"; // xpath for the username in 'People in group' table
        Assert.assertTrue("The user name is not displayed in 'People in Group' table.", webDriver.findElement(By.xpath(studentXpath)).isDisplayed());
    }

    @Test
    public void testDeleteClass() throws Exception {
        addClass(className);
        deleteClass(className);
    }



}
