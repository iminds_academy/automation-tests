package com.inceptum.content;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.inceptum.pages.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Assume;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TinCanBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.util.PropertyLoader;

public class CourseBaseTest extends TinCanBaseTest {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected static final String userPassword = "admin";
    protected static final String userName = "admin";

    protected final String image2Path = "src/test/resources/files/image2.jpeg";
    protected final String image7Path = "src/test/resources/files/image7.gif";
    protected final String pdfFilePath = "src/test/resources/files/htmlText.pdf";
    protected final String pptxFilePath = "src/test/resources/files/title.pptx";
    protected final String materialCSSpath = ".l-page-container:first-of-type .row.row2 .index .field-item .title div";

    // !! Test on creation a Presentation fails with the full URL: Unable to handle the URL, PILOT-857
   // protected final String urlSlideshare = "http://www.slideshare.net/vanoosterhout/html5-and-css3-unit-a?qid=5302390a-7f81-4814-afea-29e6e2a4d7ef&v=qf1&b=&from_search=1";

    protected final String embedCodeSlideshare = "<iframe src=\"//www.slideshare.net/slideshow/embed_code/11416269\" width=\"479\" height=\"511\" frameborder=\"0\" marginwidth=\"0\" marginheight=\"0\" scrolling=\"no\" style=\"border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;\" allowfullscreen> </iframe> <div style=\"margin-bottom:5px\"> <strong> <a href=\"https://www.slideshare.net/SudheerKiran/css-tutorial-2012\" title=\"Css tutorial 2012\" target=\"_blank\">Css tutorial 2012</a> </strong> from <strong><a href=\"http://www.slideshare.net/SudheerKiran\" target=\"_blank\">Sudheer Kiran</a></strong> </div>";
    protected final String loginUrl = "/user/login?destination=overview";
    protected final String logoutUrl = "/user/logout";
    protected static final String videoTitle2 = "Video2";
    protected static final String videoTitle3 = "Video3";
    protected static final String imageTitle2 = "Image2";
    protected static final String imageTitle3 = "Image3";
    protected static final String presentationTitle2 = "Presentation2";
    protected static final String presentationTitle3 = "Presentation3";
    protected final String liveSessionTitle = "LiveSession1";
    protected final String embedContentTitle = "EmbedContent1";
    protected final String skype = "Skype";
    protected final String liveSessionSummary ="SummaryForLiveSession1.";
    protected final String planningText = "Planning text.";
    protected final String whatIsThisForText = "What is this for text.";
    protected final String learningObjectivesText = "Learning objectives text.";
    protected final String liveSessionType = "Type1";
    protected final String taskTitle = "Task1";
    protected final String taskSummary = "SummaryForTask1.";
    protected final String theoryTitle = "Theory1";
    protected final String theorySummary = "SummaryForTheory1.";
    protected final String infoPageTitle = "InfoPage1";
    protected final String infoPageSummary = "SummaryForInfoPage1.";
    protected final String infoPageSubtitle = "SubtitleForInfoPage1";
    protected final String infoPageSubtitle2 = "SubtitleForInfoPage2";
    protected final String descriptionText = "Description text.";
    protected final String descriptionText2 = "Description text2.";
    protected final String mediaPageTitle = "MediaPage1";
    protected final String mediaPageSummary = "SummaryForMediaPage1";
    protected final String videoSummaryText = "Video summary text.";
    protected final String contextualInformationText = "Contextual information text.";
    protected  String question = "WhatIsYourName?";
    protected final String quizTitle = "Quiz1";
    protected static final String quizTitle2 = "Quiz2";
    protected static final List<String> alternativesName = Arrays.asList("Mike", "Tom", "Bob");
    protected static final List<String> alternativesAge = Arrays.asList("17", "21", "30");
    protected static final List<String> alternativesCountry = Arrays.asList("Russia", "Belarus", "England");
    protected final String classroom = "Classroom";
    protected static final String question1 = "WhatIsYourName?";
    protected static final String question2 = "HowOldAreYou?";
    protected static final String question3 = "WhereAreYouFrom?";
    protected final String iFrameURL = "http://www.bbc.com/";
    protected final String iFrameTitle = "News";


    /* ----- Tests ----- */

 /*   @BeforeClass    // remove it !!!
    public static void beforeClass() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
        //HomePage homePage = new HomePage(webDriver, pageLocation);

        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(userName);
        loginFormPage.fillFieldPassword(userPassword);
        loginFormPage.getButtonLogIn().click();

        // Check that credentials are correct (if error message is shown -> test fails)
        if (webDriver.findElements(By.cssSelector("#modal-content .messages.messages--error")).size() > 0) {
            quitWebDriver();
            Assert.fail("BeforeClass test failed: incorrect login credentials.");
        }

        // Check that the administrator was logged in
        String linkModules = websiteUrl.concat("/admin/modules/"); // only administrator has an access to this page
        webDriver.get(linkModules);
        if (webDriver.findElements(By.cssSelector(".l-main p")).size() > 0) {
            String text = webDriver.findElement(By.cssSelector(".l-main p")).getText(); // "You are not authorized to access this page." text
            quitWebDriver();
            Assert.fail("BeforeClass test failed: logged in user is not an administrator. '" + text + "' message was received.");
        }
        quitWebDriver();
    }*/

    @Before
    public void beforeTest() throws Exception {

        // Ignore test-cases of ImageTest class if they run on Mavericks/Safari
        boolean isImageTest = this instanceof ImageTest;
        boolean isMavericks = PropertyLoader.loadProperty("browser.platform").equals("mavericks");
        boolean isSafari = PropertyLoader.loadProperty("browser.name").equals("safari");
        Assume.assumeFalse(isImageTest && isMavericks && isSafari);

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
        //HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(userName);
        loginFormPage.fillFieldPassword(userPassword);
        BasePage basePage = loginFormPage.clickButtonLogIn();
        clearLogMessages();
        webDriver.get(websiteUrl);
        waitForPageToLoad();
    }

    @After
    public void afterTest() throws Exception {
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> errors = new ArrayList<String>();
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available.
                break;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                // Find php messages starting with "Notice:", "Warning:" or "Strict warning"
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {
                    errors.add(message.getAttribute("href"));
                }
            }
        }

        // Get all php messages
        for (String url : errors) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            System.out.println(fullMessage);
        }

        if (errors.size() > 0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }



    public void createImageFromContentPage(String imgPath) throws Exception {
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        AddContentPage addContentPage = contentPage.clickLinkAddContent();
        ViewImagePage viewImagePage = addContentPage.clickLinkImage();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(imgPath);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        viewImagePage = editImagePage.clickButtonSave();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image " + imageTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
    }

    public void createPresentationFromContentPage() throws Exception {
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonShortcuts();
        AddContentPage addContentPage = administrationPage.clickButtonAddContent();
        ViewPresentationPage viewPresentationPage = addContentPage.clickLinkPresentation();
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation presentation temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        viewPresentationPage = editPresentationPage.clickButtonSave();
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation " + presentationTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
    }


    public void createVideoFromContentPage() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add/video"));
        waitForPageToLoad();
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlYouTube);
        waitUntilElementIsVisible(editVideoPage.getLinkRemoveMedia());
        viewVideoPage = editVideoPage.clickButtonSave();
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video " + videoTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }


    public void createVideoWithUrl(String url, String videoTitle) throws Exception {
        webDriver.get(websiteUrl.concat("/node/add/video"));
        waitForPageToLoad();
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(url);
        waitUntilElementIsVisible(editVideoPage.getLinkRemoveMedia());
        viewVideoPage = editVideoPage.clickButtonSave();
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }



    // Course components base tests


    public String createLiveSession() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewLiveSessionPage viewLiveSessionPage = addContentPage.clickLinkLiveSession();
        assertTextOfMessage(viewLiveSessionPage.getMessage(), ".*Live session live_session temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewLiveSessionPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.fillFieldTitle(liveSessionTitle);
        editLiveSessionPage.fillFieldSummary(liveSessionSummary);
        // Select 'Coaching' option in the Chapter field
        selectItemFromListByOptionName(editLiveSessionPage.getFieldChapter(), coaching);
        // Select 'Classroom' option in the 'Live session type' field
        selectItemFromListByOptionName(editLiveSessionPage.getFieldLiveSessionType(), classroom);
        editLiveSessionPage.switchToTabEducationalContent();
        fillFrame(editLiveSessionPage.getFrameWhatDoYouNeedToDo(), whatDoYouNeedToDoText);
        fillFrame(editLiveSessionPage.getFramePlanning(), planningText);
        viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertTextOfMessage(viewLiveSessionPage.getMessage(), ".*Live session " + liveSessionTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewLiveSessionPage.getLinkEdit().isDisplayed());
        return coaching;
    }

    public String createEmbedContent() throws Exception {
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonShortcuts();
        AddContentPage addContentPage = administrationPage.clickButtonAddContent();
        ViewEmbedContentPage viewEmbedContentPage = addContentPage.clickLinkEmbedContentPage();
        assertTextOfMessage(viewEmbedContentPage.getMessage(), ".*Embed content external_content temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewEmbedContentPage.getLinkEdit().isDisplayed());
        Assert.assertTrue("Content is not Draft", viewEmbedContentPage.getTextDraft().isDisplayed());
        openEditPage();
        EditEmbedContentPage editEmbedContentPage = new EditEmbedContentPage(webDriver, pageLocation);
        editEmbedContentPage.fillFieldTitle(embedContentTitle);
        // Select 0 hour
        selectItemFromList(editEmbedContentPage.getFieldLengthHours(), 3);
        // Select 30 min
        selectItemFromList(editEmbedContentPage.getFieldLengthMin(), 6);
        editEmbedContentPage.switchToTabEducationalContent();
       // editEmbedContentPage.fillFieldIframeURL(iFrameURL);
        //editEmbedContentPage.fillFieldIframeTitle(iFrameTitle);
        fillFrame(editEmbedContentPage.getFrameVideoSummary(), videoSummary);
        fillFrame(editEmbedContentPage.getFrameContextualInformation(), contextualInformation);
        viewEmbedContentPage = editEmbedContentPage.clickButtonSaveBottom();
        assertTextOfMessage(viewEmbedContentPage.getMessage(), ".*Embed content " + embedContentTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewEmbedContentPage.getLinkEdit().isDisplayed());
        return embedContentTitle;
    }



    public String createTask() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewTaskPage viewTaskPage = addContentPage.clickLinkTask();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task task temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTaskPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.fillFieldTitle(taskTitle);
        editTaskPage.fillFieldSummary(taskSummary);
        String chapter = getNameOfOption(editTaskPage.getFieldChapter(), 1); // get name of the chapter
        selectItemFromList(editTaskPage.getFieldChapter(), 1);

        // Select 0 hour
        selectItemFromList(editTaskPage.getFieldLengthHours(), 0);
        // Select 30 min
        selectItemFromList(editTaskPage.getFieldLengthMin(), 6);
        editTaskPage.switchToTabEducationalContent();
        fillFrame(editTaskPage.getFrameWhatDoYouNeedToDo(), whatDoYouNeedToDoText);
        fillFrame(editTaskPage.getFrameWhatIsThisFor(), whatIsThisForText);
        fillFrame(editTaskPage.getFrameLearningObjectives(), learningObjectivesText);
        viewTaskPage = editTaskPage.clickButtonSave();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task " + taskTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTaskPage.getLinkEdit().isDisplayed());
        return chapter;
    }



    public String createTheory() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewTheoryPage viewTheoryPage = addContentPage.clickLinkTheory();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory theory temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTheoryPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.fillFieldTitle(theoryTitle);
        editTheoryPage.fillFieldSummary(theorySummary);
        String chapter = getNameOfOption(editTheoryPage.getFieldChapter(), 3); // get name of the chapter
        selectItemFromList(editTheoryPage.getFieldChapter(), 3);

        // Select 23 hours
        selectItemFromList(editTheoryPage.getFieldLengthHours(),23);
        // Select 0 min
        selectItemFromList(editTheoryPage.getFieldLengthMin(), 0);
        editTheoryPage.switchToTabEducationalContent();
        fillFrame(editTheoryPage.getFrameWhatDoYouNeedToDo(), whatDoYouNeedToDoText);
        fillFrame(editTheoryPage.getFrameWhatIsThisFor(), whatIsThisForText);
        fillFrame(editTheoryPage.getFrameLearningObjectives(), learningObjectivesText);
        viewTheoryPage = editTheoryPage.clickButtonSave();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory " + theoryTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTheoryPage.getLinkEdit().isDisplayed());
        return chapter;
    }



    public String createInfoPage() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewInfoPage viewInfoPage = addContentPage.clickLinkInfoPage();
        assertTextOfMessage(viewInfoPage.getMessage(), ".*Info page info temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewInfoPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        editInfoPage.fillFieldTitle(infoPageTitle);
        editInfoPage.fillFieldSummary(infoPageSummary);
        String chapter = getNameOfOption(editInfoPage.getFieldChapter(), 3); // get name of the chapter
        selectItemFromList(editInfoPage.getFieldChapter(), 3);
        editInfoPage.switchToTabEducationalContent(); // switch to 'Educational content' tab
        editInfoPage.fillFieldSubtitle(infoPageSubtitle);
        fillFrame(editInfoPage.getFrameDescription(), descriptionText);
        viewInfoPage = editInfoPage.clickButtonSave();
        assertTextOfMessage(viewInfoPage.getMessage(), ".*Info page " + infoPageTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewInfoPage.getLinkEdit().isDisplayed());
        return chapter;
    }



    public String createMediaPage() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewMediaPage viewMediaPage = addContentPage.clickLinkMediaPage();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page rich_media_page temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMediaPage.getLinkEdit().isDisplayed());
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.fillFieldTitle(mediaPageTitle);
        editMediaPage.fillFieldSummary(mediaPageSummary);
        String chapter = getNameOfOption(editMediaPage.getFieldChapter(), 1); // get name of the chapter
        selectItemFromList(editMediaPage.getFieldChapter(), 1);
        // Select 1 hour
        selectItemFromList(editMediaPage.getFieldLengthHours(), 1);
        // Select 45 min
        selectItemFromList(editMediaPage.getFieldLengthMin(), 9);
        editMediaPage.switchToTabEducationalContent();
        SelectVideoPage selectVideoPage = editMediaPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlYouTube);
        waitUntilElementIsVisible(editMediaPage.getLinkRemoveMedia());
        fillFrame(editMediaPage.getFrameVideoSummary(), videoSummaryText);
        fillFrame(editMediaPage.getFrameContextualInformation(), contextualInformationText);
        viewMediaPage = editMediaPage.clickButtonSave();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page " + mediaPageTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMediaPage.getLinkEdit().isDisplayed());
        Assert.assertTrue("Info link isn't shown on ViewMedia page.", viewMediaPage.getLinkInfo().isEnabled());
        return chapter;
    }



    // Interactivity base tests



    protected void createMCQuestionFromContentPage(List<String> alternatives) throws Exception {
        // Open CreateMCQuestion page
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        AddContentPage addContentPage = contentPage.clickLinkAddContent();
        CreateMultipleChoiceQuestionPage createMultipleChoiceQuestionPage = addContentPage.clickLinkMultipleChoiceQuestion();
        // Check that "SimpleScoring" checkbox is selected by default
        Assert.assertTrue("Checkbox 'Simple scoring' is not checked.", createMultipleChoiceQuestionPage.getCheckboxSimpleScoring().isSelected());
        // Select correct answer (1st alternative)
        createMultipleChoiceQuestionPage.clickCheckboxAlternative1();

        // Exception for Linux/Chrome (remove '?' sign from the question)
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            String[] parts = question.split("\\?"); // Separate needed node URL
            question = parts[0]; // without '?'
        }

        // Fill in the frames for three alternatives
        fillFrame(createMultipleChoiceQuestionPage.getFrameQuestion(), question);
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative1(), alternatives.get(0));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative2(), alternatives.get(1));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative3(), alternatives.get(2));
        ViewMultipleChoiceQuestionPage viewMultipleChoiceQuestionPage = createMultipleChoiceQuestionPage.clickButtonSave();
        // Check that the question is created
        assertTextOfMessage(viewMultipleChoiceQuestionPage.getMessage(), ".*Multiple choice question .+ has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMultipleChoiceQuestionPage.getLinkEdit().isDisplayed());
    }

    protected void createMCQuestionWithData(String question, List<String> alternatives, int correctAnswer) throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateMultipleChoiceQuestionPage createMultipleChoiceQuestionPage = addContentPage.clickLinkMultipleChoiceQuestion();
        // Check that "SimpleScoring" checkbox is selected by default
        Assert.assertTrue("Checkbox 'Simple scoring' is not checked.", createMultipleChoiceQuestionPage.getCheckboxSimpleScoring().isSelected());
        // Select correct answer: the value int 'correctAnswer' is an order number of correct Alternative
        WebElement correctAnswer1 = createMultipleChoiceQuestionPage.getCheckboxAlternative1();
        WebElement correctAnswer2 = createMultipleChoiceQuestionPage.getCheckboxAlternative2();
        WebElement correctAnswer3 = createMultipleChoiceQuestionPage.getCheckboxAlternative3();
        if (correctAnswer == 1) { // the 1st Alternative is correct
            waitUntilElementIsClickable(correctAnswer1);
            correctAnswer1.sendKeys(Keys.SPACE);
        }
        if (correctAnswer == 2) { // the 2nd Alternative is correct
            waitUntilElementIsClickable(correctAnswer2);
            correctAnswer2.sendKeys(Keys.SPACE);
        }
        if (correctAnswer == 3) { // the 3d Alternative is correct
            waitUntilElementIsClickable(correctAnswer3);
            correctAnswer3.sendKeys(Keys.SPACE);
        }


        // Exception for Linux/Chrome
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            String[] parts = question.split("\\?"); // Separate needed node URL
            question = parts[0]; // without '?'
        }

        // Fill in the frames for three alternatives
        fillFrame(createMultipleChoiceQuestionPage.getFrameQuestion(), question);
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative1(), alternatives.get(0));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative2(), alternatives.get(1));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative3(), alternatives.get(2));
        ViewMultipleChoiceQuestionPage viewMultipleChoiceQuestionPage = createMultipleChoiceQuestionPage.clickButtonSave();
        // Check that question is created
        assertMessage(viewMultipleChoiceQuestionPage.getMessage(), "messages messages--status", ".*Multiple choice question .+ has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMultipleChoiceQuestionPage.getLinkEdit().isDisplayed());
    }
    public void createMCQuestionFromManageQuestionTab(String question, List<String> alternatives, int correctAnswer) throws Exception {
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        manageQuestionsQuizPage.clickLinkCreateNewQuestion();
        CreateMultipleChoiceQuestionPage createMultipleChoiceQuestionPage = manageQuestionsQuizPage.clickLinkMCQuestion();
        // Check that "SimpleScoring" checkbox is selected by default
        Assert.assertTrue("Checkbox 'Simple scoring' is not selected.",
                createMultipleChoiceQuestionPage.getCheckboxSimpleScoring().isSelected());
        // Select correct answer: the value int 'correctAnswer' is an order number of correct Alternative
        WebElement correctAnswer1 = createMultipleChoiceQuestionPage.getCheckboxAlternative1();
        WebElement correctAnswer2 = createMultipleChoiceQuestionPage.getCheckboxAlternative2();
        WebElement correctAnswer3 = createMultipleChoiceQuestionPage.getCheckboxAlternative3();
        if (correctAnswer == 1) { // the 1st Alternative is correct
            waitUntilElementIsClickable(correctAnswer1);
            correctAnswer1.sendKeys(Keys.SPACE);
        }
        if (correctAnswer == 2) { // the 2nd Alternative is correct
            waitUntilElementIsClickable(correctAnswer2);
            correctAnswer2.sendKeys(Keys.SPACE);
        }
        if (correctAnswer == 3) { // the 3d Alternative is correct
            waitUntilElementIsClickable(correctAnswer3);
            correctAnswer3.sendKeys(Keys.SPACE);
        }

        // Exception for Linux/Chrome
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            String[] parts = question.split("\\?"); // Separate needed node URL
            question = parts[0]; // without '?'
        }

        // Fill in the frames for three alternatives
        fillFrame(createMultipleChoiceQuestionPage.getFrameQuestion(), question);
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative1(), alternatives.get(0));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative2(), alternatives.get(1));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative3(), alternatives.get(2));
        createMultipleChoiceQuestionPage.clickButtonSave();
        // Check that question is created and added into Quiz
        assertTextOfMessage(manageQuestionsQuizPage.getMessage(), ".*Multiple choice question .+ has been created.");
        checkQuestionIsIncludedInQuiz(question);
    }

    public void checkQuestionIsIncludedInQuiz(String question) {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("question-list")));
        List<WebElement> questions = webDriver.findElements(By.xpath("//*[@id='question-list']//a[contains(text(), " +
                "'" + question + "')]"));
        if (questions.size() == 1) {
            return;
        }
        Assert.assertTrue("The question hasn't been found on the page or number of questions with such title is > 1", false);
    }

    public void createQuiz() throws Exception {
        // Open CreateQuiz page
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateQuizPage createQuizPage = addContentPage.clickLinkQuiz();
        createQuizPage.fillFieldTitle(quizTitle); // fill Title field
        createQuizPage.checkNoRandomizationIsSelected(); // check that 'No randomization' radiobutton is selected
        createQuizPage.clickButtonSave();
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        assertElementActiveStatus(manageQuestionsQuizPage.getTabManageQuestions()); // tab 'Manage questions' is active
        // Check info message
        assertTextOfMessage(manageQuestionsQuizPage.getMessage(), ".*Quiz " + quizTitle +" has been created.");
        // Check that number of questions in the Quiz = 0
        String valueQuestionsInThisQuiz = manageQuestionsQuizPage.getFieldQuestionsInThisQuiz().getText();
        Assert.assertTrue("Value of the field 'Questions in this quiz' is incorrect: " + valueQuestionsInThisQuiz,
                valueQuestionsInThisQuiz.matches("(?s).*Questions in this quiz .(0)."));

    }

    public void createMCQuestionWithQuiz(String question, List<String> alternatives, int correctAnswer, String quizTitle) throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        CreateMultipleChoiceQuestionPage createMultipleChoiceQuestionPage = addContentPage.clickLinkMultipleChoiceQuestion();
        // Check that "SimpleScoring" checkbox is selected by default
        Assert.assertTrue("Checkbox 'Simple scoring' is not checked.", createMultipleChoiceQuestionPage.getCheckboxSimpleScoring().isSelected());
        // Select correct answer: the value int 'correctAnswer' is an order number of correct Alternative
        WebElement correctAnswer1 = createMultipleChoiceQuestionPage.getCheckboxAlternative1();
        WebElement correctAnswer2 = createMultipleChoiceQuestionPage.getCheckboxAlternative2();
        WebElement correctAnswer3 = createMultipleChoiceQuestionPage.getCheckboxAlternative3();
        if (correctAnswer == 1) { // the 1st Alternative is correct
            waitUntilElementIsClickable(correctAnswer1);
            correctAnswer1.sendKeys(Keys.SPACE);
        }
        if (correctAnswer == 2) { // the 2nd Alternative is correct
            waitUntilElementIsClickable(correctAnswer2);
            correctAnswer2.sendKeys(Keys.SPACE);
        }
        if (correctAnswer == 3) { // the 3d Alternative is correct
            waitUntilElementIsClickable(correctAnswer3);
            correctAnswer3.sendKeys(Keys.SPACE);
        }

        // Exception for Linux/Chrome
        if (PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome")) {
            String[] parts = question.split("\\?"); // Separate needed node URL
            question = parts[0]; // without '?'
        }

        fillFrame(createMultipleChoiceQuestionPage.getFrameQuestion(), question); // enter a question
        // Enter a name of a new Quiz
        createMultipleChoiceQuestionPage.clickLinkAddToQuiz();
        createMultipleChoiceQuestionPage.getFieldTitleForNewQuiz().sendKeys(quizTitle);

        // Fill in the frames for three alternatives
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative1(), alternatives.get(0));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative2(), alternatives.get(1));
        fillFrame(createMultipleChoiceQuestionPage.getFrameAlternative3(), alternatives.get(2));
        ViewMultipleChoiceQuestionPage viewMultipleChoiceQuestionPage = createMultipleChoiceQuestionPage.clickButtonSave();
        // Check that question is created
        assertTextOfMessage(viewMultipleChoiceQuestionPage.getMessage(),
                ".*Quiz " + quizTitle + " has been created.\nMultiple choice question .+ has been created.");
    }

    protected void addQuestionsToQuiz(String question1, String question2, String question3) throws Exception {
        // Add the questions to the Quiz
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        manageQuestionsQuizPage.addQuestionToQuiz(question1);
        manageQuestionsQuizPage.addQuestionToQuiz(question2);
        manageQuestionsQuizPage.addQuestionToQuiz(question3);
        manageQuestionsQuizPage.clickButtonSubmit();

        // Check info message after submitting
        assertTextOfMessage(manageQuestionsQuizPage.getMessage(), ".*Questions updated successfully.");
        // Check quantity of added questions (=3)
        String valueQuestionsInThisQuiz = manageQuestionsQuizPage.getFieldQuestionsInThisQuiz().getText();
        Assert.assertTrue("Value of the field 'Questions in this quiz' is incorrect: " + valueQuestionsInThisQuiz,
                valueQuestionsInThisQuiz.matches("(?s).*Questions in this quiz .(3)."));
    }

    public void finishQuiz() throws Exception {
        TakeQuizPage takeQuizPage = new TakeQuizPage(webDriver, pageLocation);
        takeQuizPage.clickButtonFinish();
        checkAlert(); // close alert window "By proceeding you won't be able to go back and edit your answers"
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until "Question Results" page is opened
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2[text()='Question Results']")));
    }

    protected void checkInfoSectionIsDisplayed(String subtitle, String description) {
        String xpathInfoSection = ".//*[@id='mobileMenu']/div[1]/div[1]//div[text()='" + subtitle + "']/../../.." +
                "//p[text()='" + description + "']";
        List<WebElement> sections = webDriver.findElements(By.xpath(xpathInfoSection));
        Assert.assertTrue("The info section is not displayed on ViewInfoPage.", sections.size() == 1);
    }
    protected void checkInfoSectionIsNotDisplayed(String subtitle, String description) {
        String xpathInfoSection = ".//*[@id='mobileMenu']/div[1]/div[1]//div[text()='" + subtitle + "']/../../.." +
                "//p[text()='" + description + "']";
        List<WebElement> sections = webDriver.findElements(By.xpath(xpathInfoSection));
        Assert.assertTrue("The info section is displayed on ViewInfoPage.", sections.size() == 0);
    }
    protected void checkContextualInfoIsDisplayed(String info) throws Exception {
        ViewMediaPage viewMediaPage = new ViewMediaPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewMediaPage.getLinkClose()); // link 'close' is visible, popup is opened
        String xpathInfo = ".//*[@id='mobileMenu']/div[1]/div[1]//p[text()='Contextual information text.']";
        List<WebElement> infoBlocks = webDriver.findElements(By.xpath(xpathInfo));
        Assert.assertTrue("Contextual information is not displayed on Info popup.", infoBlocks.size() == 1);
    }
    public void checkVideoIsDisplayed(String player) throws Exception {
        String xpathVideo = ".//*[@id='mobileMenu']/div[1]/div[1]//iframe[@class='media-" + player + "-player']";
        WebElement video = webDriver.findElement(By.xpath(xpathVideo));
        Assert.assertTrue("The video is not displayed on the ViewMedia page.", video.isDisplayed());
    }
    public void checkVideoIsNotDisplayed(String player) throws Exception {
        String xpathVideo = ".//*[@id='mobileMenu']/div[1]/div[1]//iframe[@class='media-" + player + "-player']";
        List<WebElement> frames = webDriver.findElements(By.xpath(xpathVideo));
        Assert.assertTrue("Video is displayed on ViewMedia page.", frames.size() == 0);
    }
    public void checkMaterialCanBeOpenedFromLinksPopup(String materialTitle, String provider) {
        webDriver.findElement(By.xpath("//div[text()='" + materialTitle + "']")).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2/div[text()='" + materialTitle + "']")));
        WebElement iframe;
        if (provider.equals(slideshare)) {
            iframe = webDriver.findElement(By.cssSelector("#modal-content [id*='media-" + provider + "'] iframe"));
        } else {
            iframe = webDriver.findElement(By.cssSelector("#modal-content iframe[class='media-" + provider + "-player tracker']"));
        }
        Assert.assertTrue("Material iframe is not displayed on the popup.", iframe.isDisplayed());
    }


    public void checkImageCanBeOpenedFromLinksPopup(String imageTitle) {
        webDriver.findElement(By.xpath("//div[text()='" + imageTitle + "']")).click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2/div[text()='" + imageTitle + "']")));
        WebElement image = webDriver.findElement(By.cssSelector("#modal-content .field__items a img"));
        Assert.assertTrue("Image is not displayed on the popup.", image.isDisplayed());
    }
    public void checkAnotherMaterialCanBeOpenedFromLinksPopup(String materialTitle, String provider) throws Exception {
        WebElement otherMaterial = webDriver.findElement(By.xpath("//*[@class='other']//div[text()='" + materialTitle + "']"));
        scrollToElement(otherMaterial);
        otherMaterial.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2/div[text()='" + materialTitle + "']")));
        WebElement iframe;
        if (provider.equals(slideshare)) {
            iframe = webDriver.findElement(By.cssSelector("[id*='media-" + provider + "'] iframe"));
        } else {
            iframe = webDriver.findElement(By.cssSelector("iframe[class='media-" + provider + "-player tracker']"));
        }
        Assert.assertTrue("Material iframe is not displayed on the popup.", iframe.isDisplayed());
    }
    public void checkAnotherImageCanBeOpenedFromLinksPopup(String imageTitle) throws Exception {
        WebElement otherImage = webDriver.findElement(By.xpath("//*[@class='other']//div[text()='" + imageTitle + "']"));
        scrollToElement(otherImage);
        otherImage.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h2/div[text()='" + imageTitle + "']")));
        WebElement image = webDriver.findElement(By.cssSelector(".field__items a img"));
        Assert.assertTrue("Image is not displayed on the popup.", image.isDisplayed());
    }
    public void navigateToParentPage(String parentContentTitle) {
        WebElement parentContent = webDriver.findElement(By.xpath("//a[@class='parent']/div[text()='" + parentContentTitle + "']"));
        parentContent.click();
    }
    public void checkOneMaterialIsDisplayedOnLinksPopup(String materialTitle) {
        List<WebElement> materials = webDriver.findElements(By.xpath("//*[@id='mobileMenu']/div[1]/div[1]//div[@class='title']/div"));
        Assert.assertTrue("Not one content is shown on Links popup.", materials.size() == 1);
        WebElement linkMaterial = webDriver.findElement(By.xpath("//*[@id='mobileMenu']/div[1]/div[1]//div[@class='title']/div"));
        String titleActual = linkMaterial.getText();
        Assert.assertTrue("Wrong material title is displayed on Links popup: " + titleActual,
                titleActual.equals(materialTitle));
    }
    public void checkOneFileNameIsDisplayedOnLinksPopup(String fileTitle) {
        List<WebElement> files = webDriver.findElements(By.xpath("//*[@id='mobileMenu']/div[1]/div[1]//div[@class='content']/a/div/div"));
        Assert.assertTrue("Not one file is shown on Links popup.", files.size() == 1);
        WebElement linkFile = webDriver.findElement(By.xpath("//*[@id='mobileMenu']/div[1]/div[1]//div[@class='content']/a/div/div"));
        String titleActual = linkFile.getText();
        Assert.assertTrue("Wrong file title is displayed on Links popup: " + titleActual,
                titleActual.equals(fileTitle));
    }
    public void checkOneLinkNameIsDisplayedOnLinksPopup(String linkName) {
        List<WebElement> files = webDriver.findElements(By.xpath("//*[@id='mobileMenu']/div[1]/div[1]//div[contains(@class, 'type-link-field')]//a"));
        Assert.assertTrue("Not one link is shown on Links popup.", files.size() == 1);
        WebElement link = webDriver.findElement(By.xpath("//*[@id='mobileMenu']/div[1]/div[1]//div[contains(@class, 'type-link-field')]//a"));
        String titleActual = link.getText();
        Assert.assertTrue("Wrong link name is displayed on Links popup: " + titleActual,
                titleActual.equals(linkName));
    }
    public void checkMaterialIsShownInCourseMaterialLinksPopup(String title) {
        String xpathMaterial = "//*[@id=\"mobileMenu\"]/div[1]/div[1]//*[contains(@class, 'group-course-resources')]" +
                "//div[text()='" + title + "']";
        Assert.assertTrue("Material with title '" + title + "' is not displayed on LinksPopup/CourseMaterial on View Media page.",
                webDriver.findElement(By.xpath(xpathMaterial)).isDisplayed());
    }
    public void checkMaterialIsShownInAdditionalMaterialLinksPopup(String title) {
        String xpathMaterial = "//*[@id=\"mobileMenu\"]/div[1]/div[1]//*[contains(@class, 'group-additional-resources')]" +
                "//div[text()='" + title + "']";
        Assert.assertTrue("Material with title '" + title + "' is not displayed on LinksPopup/AdditionalMaterial on View Media page.",
                webDriver.findElement(By.xpath(xpathMaterial)).isDisplayed());
    }
    public void checkMoreLinksIsNotDisplayedOnPopup() {
        List<WebElement> links = webDriver.findElements(By.xpath("//*[@class='other']"));
        Assert.assertTrue("'More links' section is displayed on Links popup on ViewMedia page.", links.size() == 0);
    }
    public void closePopup() {
        WebElement iconClose = webDriver.findElement(By.cssSelector("#modalContent .close"));
        waitUntilElementIsClickable(iconClose);
        iconClose.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("modalContent")));
    }
    public void clickMaterialByName(String titleMaterial) throws Exception {
        String xpathMaterial = "//*[@class='title']/div[text()='" + titleMaterial + "']/ancestor::a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until material link is visible
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathMaterial)));
        WebElement linkMaterial = webDriver.findElement(By.xpath(xpathMaterial));
        clickItem(linkMaterial, "The material link on the View Course Component page could not be clicked.");
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getPopupModalContent()); // wait until popup is opened
    }
    public void reorderElementsInClass(String chapterId, WebElement target) throws Exception { // 'Re-order content' button should already be clicked
        // Find all workshops in the Class, get the one which tagID is equal to chapterID
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        List<WebElement> workshops = overviewPage.getNavPanel().findElements(By.cssSelector(".workshop"));
        for (WebElement workshop : workshops) {
            if (workshop.findElement(By.xpath("./div[2]")).getAttribute("tagid").equals(chapterId)) {
                // Find all elements in the workshop and drag-and-drop them into target
                List<WebElement> dragElements = workshop.findElements(By.xpath("./div[3]/div/div[text()='drag']"));
                for (WebElement dragElement : dragElements) {
                    (new Actions(webDriver)).dragAndDrop(dragElement, target).perform();
                }
                // Check that all elements have been moved
                dragElements = workshop.findElements(By.xpath("./div[3]/div/div[text()='drag']"));
                Assert.assertTrue("Not all elements were moved to the target workshop.", dragElements.size() == 0);
                return;
            }
        }
        Assert.assertTrue("No chapter with such tagID has been found in the list.", false);
    }
  }
