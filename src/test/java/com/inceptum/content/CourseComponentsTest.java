package com.inceptum.content;

import java.util.List;

import com.inceptum.pages.*;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;


public class CourseComponentsTest extends CourseBaseTest {


    /*---------CONSTANTS--------*/
    protected final String embedContentTitleNew = "EmbedContentTitleNew";

    protected final String liveSessionTitleNew = "NewLiveSession";
    protected final String liveSessionTypeNew = "NewType";
    protected final String taskLinkTitle = "TaskLink";
    protected final String taskTitleNew = "NewTask";
    protected final String theoryTitleNew = "NewTheory";
    protected final String chapterTitle = "Chapter1";
    protected final String chapterTitleNew = "NewChapter";
    protected final String infoPageTitleNew = "NewInfoPage";
    protected final String mediaPageTitleNew = "NewMediaPage";
    protected final String messageErrorMediaPage = ".*Title field is required.\nLength field is required.\nVideo is required.\n" +
            "Video summary field is required.\nContextual information field is required.\nSummary field is required.";


    /* ----- Tests ----- */

// Embed Content
    // Create "Embed Content" page
    @Test
    public void testCreateEmbedContent() throws Exception {
    createEmbedContent();
    }

    //"Edit Embed Content" tests. From View Page
    @Test
    public void testEditEmbedContent() throws Exception {
        createEmbedContent();
        openEditPage();
        // Change Title
        EditEmbedContentPage editEmbedContentPage =new EditEmbedContentPage(webDriver, pageLocation);
        editEmbedContentPage.fillFieldTitle(embedContentTitleNew);
        ViewEmbedContentPage viewEmbedContentPage = editEmbedContentPage.clickButtonSaveBottom();
        assertTextOfMessage(viewEmbedContentPage.getMessage(), ".*Embed content " + embedContentTitleNew + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewEmbedContentPage.getLinkEdit().isDisplayed());
    }

    //"Edit Embed Content" tests. From Content Page

    @Test
    public void testEditEmbedContentFromContentTable() throws Exception {
        createEmbedContent();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Change Title
        EditEmbedContentPage editEmbedContentPage =new EditEmbedContentPage(webDriver, pageLocation);
        editEmbedContentPage.fillFieldTitle(embedContentTitleNew);
        editEmbedContentPage.clickButtonSaveBottom();
        assertMessage(contentPage.getMessage(), "messages status", ".*Embed content .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), embedContentTitleNew);
    }

    // "Delete Embed Content" tests. From Edit Page.
    @Test
    public void testDeleteEmbedContentFromEditPage() throws Exception {
        createEmbedContent();
        openEditPage();
        EditEmbedContentPage editEmbedContentPage = new EditEmbedContentPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editEmbedContentPage.clickButtonDeleteBottom();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + embedContentTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Embed content .+ has been deleted.");
    }

    // "Delete Embed Content" tests. From Content Page.
    @Test
    public void testDeleteEmbedContentFromContentPage() throws Exception {
        createEmbedContent();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + embedContentTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Embed content .+ has been deleted.");
    }

    // "Delete Embed Content" tests. From Content Page using "Update options" 
    @Test
    public void testDeleteEmbedContentWithUpdateOptions() throws Exception {
        createEmbedContent();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
       // administrationPage.clickButtonShortcuts();
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        // Select checkbox near created Live session in Content table
        selectItemFromListByClassName("form-checkbox", 2);
        // Select Delete option from 'Update options'
        selectItemFromListByOptionName(contentPage.getFieldUpdateOptions(), "Delete selected content");
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete this item?.*");
        deleteItemConfirmationPage.clickCheckboxDoNotReport();
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessageUpdateOptions(), "messages status", ".*Deleted 1 post.");
    }




// Live session


    // "Create Live session" test

    @Test
    public void testCreateLiveSession() throws Exception {
        createLiveSession();
    }
         // "Edit Live session" tests

    @Test
    public void testEditLiveSessionFromEditTab() throws Exception {
        createLiveSession();
        openEditPage();
        // Change Title
        EditLiveSessionPage editLiveSessionPage =new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.getFieldTitle().clear();
        editLiveSessionPage.fillFieldTitle(liveSessionTitleNew);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSaveTop();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        assertTitleIsChangedOnViewPage(viewLiveSessionPage.getFieldTitle(), liveSessionTitleNew);
    }


    @Test
    public void testEditLiveSessionFromContentTable() throws Exception {
        createLiveSession();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Live session title
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.getFieldTitle().clear();
        editLiveSessionPage.fillFieldTitle(liveSessionTitleNew);
        editLiveSessionPage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Live session .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), liveSessionTitleNew);
    }



    // "Delete Live session" tests

    @Test
    public void testDeleteLiveSessionFromEditPage() throws Exception {
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editLiveSessionPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + liveSessionTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Live session .+ has been deleted.");
    }


    @Test
    public void testDeleteLiveSessionFromContentTable() throws Exception {
        createLiveSession();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + liveSessionTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Live session .+ has been deleted.");
    }


    @Test
    public void testDeleteLiveSessionWithUpdateOptions() throws Exception {
        createLiveSession();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonShortcuts();
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        // Select checkbox near created Live session in Content table
        selectItemFromListByClassName("form-checkbox", 2);
        // Select Delete option from 'Update options'
        selectItemFromListByOptionName(contentPage.getFieldUpdateOptions(), "Delete selected content");
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete this item?.*");
        deleteItemConfirmationPage.clickCheckboxDoNotReport();
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessageUpdateOptions(), "messages status", ".*Deleted 1 post.");
    }


    // "Live session type" tests


    @Test
    public void testLiveSessionTypeFromCourseComponent() throws Exception {

        // Create Live session type

        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        AddLiveSessionTypePage addLiveSessionTypePage = editLiveSessionPage.clickIconAddLiveSessionType();
        addLiveSessionTypePage.fillFieldName(liveSessionType);
        editLiveSessionPage = addLiveSessionTypePage.clickButtonSave();
        assertMessage(editLiveSessionPage.getMessageCreation(), "messages status", ".*Created new term " + liveSessionType + ".");
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        String liveSessionTypeActual = viewLiveSessionPage.getFieldType().getText();
        Assert.assertEquals(liveSessionType, liveSessionTypeActual);


        // Edit Live session type

        openEditPage();
        checkAlert();
        EditLiveSessionTypePage editLiveSessionTypePage = editLiveSessionPage.clickIconEditLiveSessionType();
        editLiveSessionTypePage.getFieldName().clear();
        editLiveSessionTypePage.getFieldName().sendKeys(liveSessionTypeNew);
        editLiveSessionPage = editLiveSessionTypePage.clickButtonSave();
        checkAlert();
        assertMessage(editLiveSessionPage.getMessage(), "messages status", ".*Updated term " + liveSessionTypeNew + ".");
        viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        liveSessionTypeActual = viewLiveSessionPage.getFieldType().getText();
        Assert.assertEquals(liveSessionTypeNew, liveSessionTypeActual);


        // Delete Live session type

        openEditPage();
        editLiveSessionTypePage = editLiveSessionPage.clickIconEditLiveSessionType();
        DeleteItemConfirmationPage deleteItemConfirmationPage = editLiveSessionTypePage.clickButtonDelete();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete the term " + liveSessionTypeNew + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertTextOfMessage(editLiveSessionPage.getMessage(), ".*Deleted term " + liveSessionTypeNew + ".");
        viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        // Check that the deleted type isn't displayed on View Live session page
        liveSessionTypeActual = viewLiveSessionPage.getFieldType().getText();
        Assert.assertFalse(liveSessionTypeActual.equals(liveSessionTypeNew));
    }


    @Test
    public void testLiveSessionTypeFromTaxonomy() throws Exception {

        // Create Live session type

        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        StructurePage structurePage = administrationPage.clickButtonStructure();
        TaxonomyPage taxonomyPage = structurePage.clickLinkTaxonomy();
        AddTermPage addTermPage = taxonomyPage.clickLinkAddTermsForLiveSessionTypes();
        addTermPage.fillFieldName(liveSessionType);
        addTermPage = addTermPage.clickButtonSave();
        assertTextOfMessage(addTermPage.getMessage(), ".*Created new term " + liveSessionType + ".");
        // Open "Create Live session" page
        webDriver.get(websiteUrl.concat("/node/add/live-session"));
        waitForPageToLoad();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        assertItemIsInTheList(editLiveSessionPage.getFieldLiveSessionType(), liveSessionType);


        // Edit Live session type

        webDriver.get(websiteUrl.concat("/admin/structure/taxonomy"));
        waitForPageToLoad();
        ListTermsPage listTermsPage = taxonomyPage.clickLinkListTermsForLiveSessionTypes();
        EditTermPage editTermPage = listTermsPage.clickLinkEdit(liveSessionType);
        editTermPage.getFieldName().clear();
        editTermPage.getFieldName().sendKeys(liveSessionTypeNew);
        editTermPage.clickButtonSave();
        assertMessage(listTermsPage.getMessage(), "messages status", ".*Updated term " + liveSessionTypeNew + ".");
        // Open "Create Live session" page
        webDriver.get(websiteUrl.concat("/node/add/live-session"));
        waitForPageToLoad();
        openEditPage();
        assertItemIsInTheList(editLiveSessionPage.getFieldLiveSessionType(), liveSessionTypeNew);


        // Delete Live session type

        webDriver.get(websiteUrl.concat("/admin/structure/taxonomy"));
        waitForPageToLoad();
        listTermsPage = taxonomyPage.clickLinkListTermsForLiveSessionTypes();
        editTermPage = listTermsPage.clickLinkEdit(liveSessionTypeNew);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editTermPage.clickButtonDelete();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete the term " + liveSessionTypeNew + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertTextOfMessage(listTermsPage.getMessage(), ".*Deleted term " + liveSessionTypeNew + ".");
    }


    // "Task link" tests


    @Test
    public void testCreateTaskLink() throws Exception {
        createTask();
        String taskUrl = webDriver.getCurrentUrl();
        conditionForTaskLink(taskUrl); // Only for Linux/Chrome
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabEducationalContent();
        editLiveSessionPage.fillFieldTaskLinkTitle(taskLinkTitle);
        editLiveSessionPage.fillFieldTaskLinkURL(taskUrl);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        String taskUrlActual = viewLiveSessionPage.getLinkTask().getAttribute("href");
        Assert.assertEquals(taskUrl, taskUrlActual);
        String taskTitleActual = viewLiveSessionPage.getLinkTask().getText();
        Assert.assertTrue(taskTitleActual.matches(taskLinkTitle));
    }


    @Test
    public void testCreateTaskLinkWithoutTitle() throws Exception{
        createTask();
        String taskUrl = webDriver.getCurrentUrl();
        conditionForTaskLink(taskUrl); // Only for Linux/Chrome
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabEducationalContent();
        editLiveSessionPage.fillFieldTaskLinkURL(taskUrl);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        String taskUrlActual = viewLiveSessionPage.getLinkTask().getAttribute("href");
        Assert.assertEquals(taskUrl, taskUrlActual);
        String taskTitleActual = viewLiveSessionPage.getLinkTask().getText();
        Assert.assertTrue(taskTitleActual.matches(taskUrl));
    }



    @Test
    public void testCreateTaskLinkWithoutUrl() throws Exception {
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabEducationalContent();
        editLiveSessionPage.fillFieldTaskLinkTitle(taskLinkTitle);
        editLiveSessionPage.clickButtonSave();
        assertMessage(editLiveSessionPage.getMessageError(), "messages error", ".*URL cannot be empty if title part is filled.*");
        // URL field has a red frame
        String classError = editLiveSessionPage.getFieldTaskLinkURL().getAttribute("class");
        Assert.assertEquals("form-text error", classError);
    }


    @Test
    public void testDeleteTaskLink() throws Exception {
        createTask();
        String taskUrl = webDriver.getCurrentUrl();
        conditionForTaskLink(taskUrl); // Only for Linux/Chrome
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabEducationalContent();
        editLiveSessionPage.fillFieldTaskLinkTitle(taskLinkTitle);
        editLiveSessionPage.fillFieldTaskLinkURL(taskUrl);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        viewLiveSessionPage.assertTaskLinkIsShown();
        String taskUrlActual = viewLiveSessionPage.getLinkTask().getAttribute("href");
        Assert.assertEquals(taskUrl, taskUrlActual);
        openEditPage();
        editLiveSessionPage.switchToTabEducationalContent();
        editLiveSessionPage.getFieldTaskLinkTitle().clear();
        editLiveSessionPage.getFieldTaskLinkURL().clear();
        // Save changes (it's needed to check that error message doesn't appear when both fields are empty)
        viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        viewLiveSessionPage.assertTaskLinkIsNotShown();
    }


// Task

    // "Create Task" test


    @Test
    public void testCreateTask() throws Exception {
            createTask();
        }


    // "Edit Task" tests

    @Test
    public void testEditTaskFromEditTab() throws Exception {
        createTask();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.getFieldTitle().clear();
        editTaskPage.getFieldTitle().sendKeys(taskTitleNew);
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSaveTop();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        assertTitleIsChangedOnViewPage(viewTaskPage.getFieldTitle(), taskTitleNew);
    }


    @Test
    public void testEditTaskFromContentTable() throws Exception {
        createTask();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Task title
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.getFieldTitle().clear();
        editTaskPage.getFieldTitle().sendKeys(taskTitleNew);
        editTaskPage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Task .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), taskTitleNew);
    }



    // "Delete Task" tests

    @Test
    public void testDeleteTaskFromEditPage() throws Exception {
        createTask();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editTaskPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + taskTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Task .+ has been deleted.");
    }

    @Test
    public void testDeleteTaskFromContentTable() throws Exception {
        createTask();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + taskTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Task .+ has been deleted.");
    }


    @Test
    public void testDeleteTaskWithUpdateOptions() throws Exception {
        createTask();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonShortcuts();
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        // Select checkbox near created Task in Content table
        selectItemFromListByClassName("form-checkbox", 2);
        // Select Delete option from 'Update options'
        selectItemFromListByOptionName(contentPage.getFieldUpdateOptions(), "Delete selected content");
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete this item?.*");
        deleteItemConfirmationPage.clickCheckboxDoNotReport();
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessageUpdateOptions(), "messages status", ".*Deleted 1 post.");
    }


// Theory


    // "Create Theory" test


    @Test
    public void testCreateTheory() throws Exception {
        createTheory();
    }



    // "Edit Theory" tests

    @Test
    public void testEditTheoryFromEditTab() throws Exception {
        createTheory();
        openEditPage();
        // Change Title
        EditTheoryPage editTheoryPage =new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.getFieldTitle().clear();
        editTheoryPage.fillFieldTitle(theoryTitleNew);
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSaveTop();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");
        assertTitleIsChangedOnViewPage(viewTheoryPage.getFieldTitle(), theoryTitleNew);
    }

    @Test
    public void testEditTheoryFromContentTable() throws Exception {
        createTheory();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Theory title
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.getFieldTitle().clear();
        editTheoryPage.fillFieldTitle(theoryTitleNew);
        editTheoryPage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Theory .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), theoryTitleNew);
    }



    // "Delete Theory" tests

    @Test
    public void testDeleteTheoryFromEditPage() throws Exception {
        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editTheoryPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + theoryTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Theory .+ has been deleted.");
    }

    @Test
    public void testDeleteTheoryFromContentTable() throws Exception {
        createTheory();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + theoryTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Theory .+ has been deleted.");
    }


    @Test
    public void testDeleteTheoryWithUpdateOptions() throws Exception {
        createTheory();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonShortcuts();
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        // Select checkbox near created Theory in Content table
        selectItemFromListByClassName("form-checkbox", 2);
        // Select Delete option from 'Update options'
        selectItemFromListByOptionName(contentPage.getFieldUpdateOptions(), "Delete selected content");
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete this item?.*");
        deleteItemConfirmationPage.clickCheckboxDoNotReport();
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessageUpdateOptions(), "messages status", ".*Deleted 1 post.");
    }



// Chapter field


    @Test
    public void testCreateChapterFromCourseComponent() throws Exception {

        // Create Chapter

        selectClass(exampleClass);
        createTask();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        AddChapterPage addChapterPage = editTaskPage.clickIconAddChapter();
        addChapterPage.fillFieldName(chapterTitle);
        addChapterPage.clickButtonSave();
        assertMessage(editTaskPage.getMessageCreation(), "messages status", ".*Created new term " + chapterTitle + ".");
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonHome();
        checkAlert();
        selectClass(exampleClass);
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        overviewPage.assertChapterIsShown(chapterTitle);


        // Edit Chapter

        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        selectItemFromListByOptionName(editLiveSessionPage.getFieldChapter(), chapterTitle);
        EditChapterPage editChapterPage = editLiveSessionPage.clickIconEditChapter();
        editChapterPage.getFieldName().clear();
        editChapterPage.fillFieldName(chapterTitleNew);
        editChapterPage.clickButtonSave();
        assertMessage(editLiveSessionPage.getMessage(), "messages status", ".*Updated term " + chapterTitleNew + ".");
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        administrationPage.clickButtonHome();
        checkAlert();
        selectClass(exampleClass);
        overviewPage.assertChapterIsShown(chapterTitleNew);


        // Delete Chapter

        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        selectItemFromListByOptionName(editTheoryPage.getFieldChapter(), chapterTitleNew);
        editChapterPage = editTheoryPage.clickIconEditChapter();
        DeleteItemConfirmationPage deleteItemConfirmationPage = editChapterPage.clickButtonDelete();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete the term " + chapterTitleNew + "?.*");
        deleteItemConfirmationPage.clickButtonDelete();
        checkAlert();
        assertTextOfMessage(editTheoryPage.getMessage(), ".*Deleted term " + chapterTitleNew + ".");
        // Chapter field has a red frame
        Assert.assertEquals("form-select required error", editTheoryPage.getFieldChapter().getAttribute("class"));
        editTheoryPage.clickButtonSave();
    }


    @Test
    public void testCreateChapterFromTaxonomy() throws Exception {

        // Create Chapter

        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        StructurePage structurePage = administrationPage.clickButtonStructure();
        TaxonomyPage taxonomyPage = structurePage.clickLinkTaxonomy();
        AddTermPage addTermPage = taxonomyPage.clickLinkAddTermsForChapter();
        addTermPage.fillFieldName(chapterTitle);
        addTermPage = addTermPage.clickButtonSave();
        assertTextOfMessage(addTermPage.getMessage(), ".*Created new term " + chapterTitle + ".");
        // Open "Create Theory" page
        webDriver.get(websiteUrl.concat("/node/add/theory"));
        waitForPageToLoad();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        assertItemIsInTheList(editTheoryPage.getFieldChapter(), chapterTitle);


        // Edit Chapter

        webDriver.get(websiteUrl.concat("/admin/structure/taxonomy"));
        waitForPageToLoad();
        ListTermsPage listTermsPage = taxonomyPage.clickLinkListTermsForChapter();
        EditTermPage editTermPage = listTermsPage.clickLinkEdit(chapterTitle);
        editTermPage.getFieldName().clear();
        editTermPage.fillFieldName(chapterTitleNew);
        editTermPage.clickButtonSave();
        assertMessage(listTermsPage.getMessage(), "messages status", ".*Updated term " + chapterTitleNew + ".");
        // Open "Create Live session" page
        webDriver.get(websiteUrl.concat("/node/add/live-session"));
        waitForPageToLoad();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        assertItemIsInTheList(editLiveSessionPage.getFieldChapter(), chapterTitleNew);


        // Delete Chapter

        webDriver.get(websiteUrl.concat("/admin/structure/taxonomy"));
        waitForPageToLoad();
        listTermsPage = taxonomyPage.clickLinkListTermsForChapter();
        editTermPage = listTermsPage.clickLinkEdit(chapterTitleNew);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editTermPage.clickButtonDelete();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete the term " + chapterTitleNew + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertTextOfMessage(listTermsPage.getMessage(), ".*Deleted term " + chapterTitleNew + ".");
    }


    @Test // PILOT-1565 !!!! // should be tested with SauceLabs (action drag&drop can't be done in some browsers)
    public void testCreateChapterViaEditDashboardButton() throws Exception {

        // Create Chapter

        OverviewPage overviewPage = clickButtonGoToFullProgram();
        overviewPage.clickButtonReOrderContent();
        assertMessage(overviewPage.getMessage(), "messages messages--warning sortable", ".*Changes made will not be saved until the button 'Save' is pressed.");
        overviewPage.fillLastChapterField(chapterTitle);
        closeCookiesPopup();
        overviewPage.clickButtonSave();
        waitUntilElementIsVisible(overviewPage.getButtonReOrderContent());
        // Open "Create Task" page
        webDriver.get(websiteUrl.concat("/node/add/task"));
        waitForPageToLoad();
        // Check that the new chapter is in the list "Chapter"
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        assertItemIsInTheList(editTaskPage.getFieldChapter(), chapterTitle);

        // Edit Chapter

        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonBackToSite();
        administrationPage.clickButtonHome();
        overviewPage = clickButtonGoToFullProgram();
        overviewPage.clickButtonReOrderContent();
        overviewPage.renameCreatedChapterField(chapterTitleNew);
        overviewPage.clickButtonSave(); // PILOT-1565 !!!!!!!!!!!!!!!!!
        waitUntilElementIsVisible(overviewPage.getButtonReOrderContent());
        // Open "Create Live session" page
        webDriver.get(websiteUrl.concat("/node/add/live-session"));
        waitForPageToLoad();
        // Check that the new chapter is in the list "Chapter"
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        assertItemIsInTheList(editLiveSessionPage.getFieldChapter(), chapterTitleNew);

        // Delete Chapter

        createTask();
        openEditPage();
        String chapterId = editTaskPage.getChapterID(chapterTitleNew);
        editTaskPage.clickButtonSave();
        administrationPage.clickButtonHome();
        overviewPage = clickButtonGoToFullProgram();

        // Clear chapter field (delete Chapter)
        overviewPage.clickButtonReOrderContent();
        WebElement tag = overviewPage.clearChapterField(chapterId);
        overviewPage.clickButtonSave();

        // Check message status
        assertTextOfMessage(overviewPage.getErrorMessage(), "(?s).*All types should be filled in");
        // Check that Chapter field has a red frame (error)
        String errorClass = tag.findElement(By.tagName("input")).getAttribute("class");
        Assert.assertEquals("error", errorClass);

        // Drag elements from deleted chapter to another one

        // Find drag elements for 'error' chapter
        WebElement currentWorkshop = tag.findElement(By.xpath("./.."));
        List<WebElement> dragElements = currentWorkshop.findElements(By.xpath("./div[3]/div/div[text()='drag']"));

        // Select a target
        WebElement target = null;
        List<WebElement> workshops = overviewPage.getNavPanel().findElements(By.cssSelector(".workshop"));
        for (WebElement workshop : workshops) {
            if (workshop.findElement(By.xpath("./div[2]")).getAttribute("tagid")!=chapterId) {
                target = workshop.findElement(By.xpath("./div[3]"));
                break;
            }
        }
        // Drag&Drop elements from 'error' chapter to another one
        for (WebElement dragElement : dragElements) {
            (new Actions(webDriver)).dragAndDrop(dragElement, target).perform();
        }

        // Confirm changes - click Save
        overviewPage.clickButtonSave();
        waitUntilElementIsVisible(overviewPage.getButtonReOrderContent());
    }

    @Test // according to PILOT-1665
    public void testCheckContentReorderingUsingSeeCourseOverviewButton() throws Exception {
        // Check if the class already exists, create new one otherwise
        checkClassIsAddedOnOverview(className);

        // Create Task in order to have at least one course component in a chapter
        createTask();
        // Open EditTask page to get its chapterID
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        String chapterId = editTaskPage.getChapterID();

        // Open the Class overview page via 'See course overview' button
        HomePage homePage = openHomePage();
        OverviewPage overviewPage = homePage.clickButtonSeeCourseOverview();
        // Check that correct Class is selected on the page
        assertClassIsSelected(className);
        // Click button 'Re-order content'
        overviewPage.clickButtonReOrderContent();
        // Check that proper message is displayed
        assertMessage(overviewPage.getMessage(), "messages messages--warning sortable", ".*Changes made will not be saved until the button 'Save' is pressed.");

        // Fill in the last chapter field (it's empty by default) and get it as target to drag-and-drop elements
        WebElement emptyWorkshop = overviewPage.fillLastChapterField(chapterTitle);
        WebElement target = emptyWorkshop.findElement(By.xpath("./div[3]"));

        // Move all elements from chapter with defined ID to the target
        reorderElementsInClass(chapterId, target);

        // Confirm changes - click Save
        closeCookiesPopup();
        overviewPage.clickButtonSave();
        waitUntilElementIsVisible(overviewPage.getButtonReOrderContent()); // PILOT-1665 !!!
    }


// Info page

    // 'Create Info page' test

    @Test
    public void testCreateInfoPage() throws Exception {
        createInfoPage();
    }


    // 'Edit Info page' tests
    @Test
    public void testEditInfoPageFromContentTable() throws Exception {
        // Create Info page
        createInfoPage();
        // Open Edit Info page from Content table
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Info page title
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        editInfoPage.getFieldTitle().clear();
        editInfoPage.fillFieldTitle(infoPageTitleNew);
        editInfoPage.clickButtonSave();

        // Check the new title is displayed in Content table
        assertMessage(contentPage.getMessage(), "messages status", ".*Info page .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), infoPageTitleNew);
    }

    @Test
    public void testAddRemoveAnotherItemFromInfoPage() throws Exception {
        // Create Info page
        createInfoPage();
        // Open Edit page, add the 2nd info section
        openEditPage();
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        editInfoPage.switchToTabEducationalContent();
        editInfoPage.clickButtonAddAnotherItem();
        editInfoPage.fillFieldSubtitle2(infoPageSubtitle2);
        fillFrame(editInfoPage.getFrameDescription2(), descriptionText2);
        ViewInfoPage viewInfoPage = editInfoPage.clickButtonSave();
        assertTextOfMessage(viewInfoPage.getMessage(), ".*Info page " + infoPageTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewInfoPage.getLinkEdit().isDisplayed());

        // Check that twi info sections are shown on the ViewInfoPage
        checkInfoSectionIsDisplayed(infoPageSubtitle, descriptionText);
        checkInfoSectionIsDisplayed(infoPageSubtitle2, descriptionText2);

        // Remove the 1st info section from Edit page
        openEditPage();
        editInfoPage.switchToTabEducationalContent();
        editInfoPage.clickButtonRemove();
        editInfoPage.clickButtonSave();

        // Check that only the 2nd info section is displayed on the ViewInfoPage
        checkInfoSectionIsNotDisplayed(infoPageSubtitle, descriptionText);
        checkInfoSectionIsDisplayed(infoPageSubtitle2, descriptionText2);
    }

    // 'Delete Info page' tests

    @Test
    public void testDeleteInfoPageFromEditPage() throws Exception {
        // Create Info page
        createInfoPage();
        // Open Edit page, delete the Info page with a proper button
        openEditPage();
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editInfoPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + infoPageTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();

        // Check info message
        assertTextOfMessage(homePage.getMessage(), ".*Info page .+ has been deleted.");
    }

    @Test
    public void testDeleteInfoPageFromContentPage() throws Exception {
        // Create Info page
        createInfoPage();
        // Delete Info page from Content table with a proper link
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + infoPageTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();

        // Check info message
        assertTextOfMessage(contentPage.getMessage(), ".*Info page .+ has been deleted.");
    }

    @Test
    public void testDeleteInfoPageWithUpdateOptions() throws Exception {
        // Create Info page
        createInfoPage();
        // Open Content page
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonShortcuts();
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        // Select checkbox near created Info page in Content table
        selectItemFromListByClassName("form-checkbox", 2);
        // Select Delete option from 'Update options'
        selectItemFromListByOptionName(contentPage.getFieldUpdateOptions(), "Delete selected content");
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickButtonUpdate();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete this item?.*");
        deleteItemConfirmationPage.clickCheckboxDoNotReport();
        deleteItemConfirmationPage.clickButtonDelete();

        // Check info message
        assertMessage(contentPage.getMessageUpdateOptions(), "messages status", ".*Deleted 1 post.");
    }

    @Test
    public void testCancelDeletingInfoPage() throws Exception {
        // Create Info page
        createInfoPage();
        // Open Content page
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        // Get href attribute of the Info page
        String linkInfoPageTitle = getLinkUrlFrom1stElement();
        // Cancel deleting the content
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + infoPageTitle + ".*");
        deleteItemConfirmationPage.clickLinkCancel();
        waitUntilPageIsOpenedWithTitle("Content");
        // Get href attribute of the 1st element in the table
        String link1stElement = getLinkUrlFrom1stElement();

        // Check that the attribute matches the one of the Info page, so the Info page hasn't been deleted
        Assert.assertTrue("The Info page is deleted (or another content is shown in the 1st row of Content table).",
                linkInfoPageTitle.equals(link1stElement));
    }

    @Test
    public void testCheckValidationOfInfoPage() throws Exception {
        // Create Info page template
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewInfoPage viewInfoPage = addContentPage.clickLinkInfoPage();
        assertTextOfMessage(viewInfoPage.getMessage(), ".*Info page info temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewInfoPage.getLinkEdit().isDisplayed());
        // Open Edit page, clear required fields
        openEditPage();
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        editInfoPage.getFieldTitle().clear();
        editInfoPage.getFieldSummary().clear();
        // Click Save and check error message
        editInfoPage.clickButtonSave();
        assertTextOfMessage(editInfoPage.getErrorMessage(), ".*Title field is required.\nSummary field is required.");

        // Fill in required fields, save the info page
        editInfoPage.fillFieldTitle(infoPageTitle);
        editInfoPage.fillFieldSummary(infoPageSummary);
        selectItemFromList(editInfoPage.getFieldChapter(), 3);
        editInfoPage.switchToTabEducationalContent();
        editInfoPage.fillFieldSubtitle(infoPageSubtitle);
        fillFrame(editInfoPage.getFrameDescription(), descriptionText);
        viewInfoPage = editInfoPage.clickButtonSave();
        // Check info message
        assertTextOfMessage(viewInfoPage.getMessage(), ".*Info page " + infoPageTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewInfoPage.getLinkEdit().isDisplayed());
    }


// Media page

    // 'Create Media page' test

    @Test
    public void testCreateMediaPage() throws Exception {
        // Create Media page
        createMediaPage();
        // Check that contextual information is shown if clicking 'Info' link
        ViewMediaPage viewMediaPage = new ViewMediaPage(webDriver, pageLocation);
        viewMediaPage.clickLinkInfo();
        checkContextualInfoIsDisplayed(contextualInformationText);
        // Close the popup
        viewMediaPage.clickLinkClose();
    }


    // 'Edit Media page' tests
    @Test
    public void testEditMediaPageFromContentTable() throws Exception {
        // Create Media page
        createMediaPage();
        // Open Edit Media page from Content table
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Media page title
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        editInfoPage.getFieldTitle().clear();
        editInfoPage.fillFieldTitle(mediaPageTitleNew);
        editInfoPage.clickButtonSave();

        // Check the new title is displayed in Content table
        assertMessage(contentPage.getMessage(), "messages status", ".*Media page .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), mediaPageTitleNew);
    }

    @Ignore // Now only one item exists on EducationalContentMediaPage from http://v3-4-20.staging.imindsx.org/.
    @Test
    public void testAddRemoveAnotherItemFromEducationalContentMediaPage() throws Exception {
        // Create Media page
        createMediaPage();
        // Add another video to Educational content
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabEducationalContent();
        editMediaPage.clickButtonAddAnotherItem();
        waitUntilElementIsVisible(editMediaPage.getLinkSelectMediaFor3dVideo());
        SelectVideoPage selectVideoPage = editMediaPage.clickLinkSelectMediaFor3dVideo();
        selectVideoPage.selectVideo(urlVimeo);
        Assert.assertTrue(editMediaPage.getLinkRemoveMediaFor3dVideo().isDisplayed());
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check that two videos (different providers) are displayed on ViewMedia page
        checkVideoIsDisplayed(youtube);
        checkVideoIsDisplayed(vimeo);

        // Remove the 1st video
        openEditPage();
        editMediaPage.switchToTabEducationalContent();
        editMediaPage.clickLinkRemoveMedia();
        editMediaPage.clickButtonSave();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page " + mediaPageTitle + " has been updated.");

        // Check that only one video is displayed on ViewMedia page
        checkVideoIsDisplayed(vimeo);
        checkVideoIsNotDisplayed(youtube);
    }

    @Test
    public void testAddRemoveAnotherPresentationFromCourseMaterialMediaPage() throws Exception {
        // Create the 1st presentation
        createPresentationFromContentPage();
        // Create the 2nd presentation
        webDriver.get(websiteUrl.concat("/node/add/presentation"));
        waitForPageToLoad();
        ViewPresentationPage viewPresentationPage = new ViewPresentationPage(webDriver, pageLocation);
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation presentation temp content has been created.");
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle2);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        viewPresentationPage = editPresentationPage.clickButtonSave();
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation " + presentationTitle2 + " has been updated.");

        // Create Media page
        createMediaPage();
        // Add the two presentations to CourseMaterial of the Media page
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editMediaPage.getFieldPresentationsCourseMaterial(), presentationTitle);
        editMediaPage.clickButtonAddAnotherPresentationCourseMaterial(); // add another presentation field
        autoSelectItemFromList(editMediaPage.getFieldPresentationsCourseMaterial2(), presentationTitle2);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check that two presentations can be opened with Links button
        viewMediaPage.clickLinkLinks();
        checkMaterialCanBeOpenedFromLinksPopup(presentationTitle, slideshare);
        checkAnotherMaterialCanBeOpenedFromLinksPopup(presentationTitle2, slideshare);
        // Navigate back to the Media page, remove the 1st presentation
        navigateToParentPage(mediaPageTitle);
        waitUntilElementIsVisible(viewMediaPage.getLinkLinks());
        openEditPage();
        editMediaPage.switchToTabCourseMaterial();
        editMediaPage.getFieldPresentationsCourseMaterial().clear();
        editMediaPage.clickButtonSave();

        // Check that only one presentation is displayed on Links popup
        viewMediaPage.clickLinkLinks();
        checkOneMaterialIsDisplayedOnLinksPopup(presentationTitle2);
    }

    @Test
    public void testAddRemoveAnotherVideoFromAdditionalMaterialMediaPage() throws Exception {
        // Create the 1st video (youtube)
        createVideoFromContentPage();

        // Create Media page
        createMediaPage();
        // Add the 1st video to Media page/Additional material
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabAdditionalMaterial();
        autoSelectItemFromList(editMediaPage.getFieldVideosAdditionalMaterial(), videoTitle);
        // Add the 2nd video with icon 'add'
        editMediaPage.clickButtonAddAnotherVideoAdditionalMaterial(); // add another video field
        CreateVideoPage createVideoPage = editMediaPage.clickIconAddVideoAdditionalMaterial2();
        createVideoPage.fillFieldTitle(videoTitle2);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(urlVimeo);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        assertTextOfMessage(editMediaPage.getMessage(), ".*Video .+ has been created.");
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check that two videos can be opened with Links button
        viewMediaPage.clickLinkLinks();
        checkMaterialCanBeOpenedFromLinksPopup(videoTitle, youtube);
        checkAnotherMaterialCanBeOpenedFromLinksPopup(videoTitle2, vimeo);
        // Navigate back to the Media page, remove the 1st video (youtube)
        navigateToParentPage(mediaPageTitle);
        waitUntilElementIsVisible(viewMediaPage.getLinkLinks());
        openEditPage();
        editMediaPage.switchToTabAdditionalMaterial();
        editMediaPage.getFieldVideosAdditionalMaterial().clear();
        editMediaPage.clickButtonSave();

        // Check that only one video (vimeo) is displayed on Links popup
        viewMediaPage.clickLinkLinks();
        checkOneMaterialIsDisplayedOnLinksPopup(videoTitle2);
    }

    @Test
    public void testAddRemoveAnotherImageFromCourseMaterialMediaPage() throws Exception {
        // Create the 1st image
        createImageFromContentPage(image1Path); // .jpeg
        openHomePage();
        // Create the 2nd image
        webDriver.get(websiteUrl.concat("/node/add/image"));
        waitForPageToLoad();
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle2);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image7Path); // .gif
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        viewImagePage = editImagePage.clickButtonSave();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image " + imageTitle2 + " has been updated.");

        // Create Media page
        createMediaPage();
        // Add the two images to CourseMaterial of the Media page
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editMediaPage.getFieldImagesCourseMaterial(), imageTitle);
        editMediaPage.clickButtonAddAnotherImageCourseMaterial(); // add another image field
        autoSelectItemFromList(editMediaPage.getFieldImagesCourseMaterial2(), imageTitle2);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check that two images can be opened with Links button
        viewMediaPage.clickLinkLinks();
        checkImageCanBeOpenedFromLinksPopup(imageTitle);
        checkAnotherImageCanBeOpenedFromLinksPopup(imageTitle2);
        // Navigate back to the Media page, remove the 1st image
        navigateToParentPage(mediaPageTitle);
        waitUntilElementIsVisible(viewMediaPage.getLinkLinks());
        openEditPage();
        editMediaPage.switchToTabCourseMaterial();
        editMediaPage.getFieldImagesCourseMaterial().clear();
        editMediaPage.clickButtonSave();

        // Check that only one image is displayed on Links popup
        viewMediaPage.clickLinkLinks();
        checkOneMaterialIsDisplayedOnLinksPopup(imageTitle2);
    }

    @Test
    public void testAddCourseAdditionalMaterialToMediaPage() throws Exception {
        // Create Presentation and Video
        createPresentationFromContentPage();
        createVideoFromContentPage();
        // Create Media page
        createMediaPage();
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        // Add the Presentation to Course material
        editMediaPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editMediaPage.getFieldPresentationsCourseMaterial(), presentationTitle);
        // Add the Video to Additional material
        editMediaPage.switchToTabAdditionalMaterial();
        autoSelectItemFromList(editMediaPage.getFieldVideosAdditionalMaterial(), videoTitle);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check that materials are displayed in proper sections on Links popup
        viewMediaPage.clickLinkLinks();
        checkMaterialIsShownInCourseMaterialLinksPopup(presentationTitle);
        checkMaterialIsShownInAdditionalMaterialLinksPopup(videoTitle);
        // Open Presentation, check 'More links' section is not shown on the page
        checkMaterialCanBeOpenedFromLinksPopup(presentationTitle, slideshare);
        checkMoreLinksIsNotDisplayedOnPopup();
        closePopup();
        // Open Video, check 'More links' section is not shown on the page
        checkMaterialCanBeOpenedFromLinksPopup(videoTitle, youtube);
        checkMoreLinksIsNotDisplayedOnPopup();
        closePopup();
        // Close Links popup
        viewMediaPage.clickLinkClose();
    }

    @Test
    public void testAddRemoveAnotherFileFromCourseMaterialMediaPage() throws Exception {
        // Create Media page
        createMediaPage();
        // Upload a file to Course material
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabCourseMaterial();
        editMediaPage.fillFieldDownloadsTitle(fileTitle);
        editMediaPage.uploadFileToDownloads(docxFilePath);
        // Upload the 2nd file with 'Add another item' button
        editMediaPage.clickButtonAddAnotherFileCourseMaterial();
        editMediaPage.fillFieldDownloadsTitle2(fileTitle2);
        editMediaPage.uploadFileToDownloads2(xlsxFilePath);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check the two files can be downloaded from Links popup
        viewMediaPage.clickLinkLinks();
        clickLinkDownloadedCourseMaterialByName(fileTitle);
        waitUntilFileDownloaded();
        clickLinkDownloadedCourseMaterialByName(fileTitle2);
        waitUntilFileDownloaded();

        // Remove the 1st file
        openEditPage();
        editMediaPage.switchToTabCourseMaterial();
        editMediaPage.clearFieldDownloadsTitle(); // clear title field for the file
        editMediaPage.clickButtonRemoveFile();
        viewMediaPage = editMediaPage.clickButtonSave();
        // Check that only the 2nd file is displayed on Links popup
        viewMediaPage.clickLinkLinks();
        checkOneFileNameIsDisplayedOnLinksPopup(fileTitle2);
    }

    @Test
    public void testAddRemoveAnotherLinkFromAdditionalMaterialMediaPage() throws Exception {
        // Create Media page
        createMediaPage();
        // Add the 1st link to Additional material
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabAdditionalMaterial();
        editMediaPage.fillFieldLinksTitleAdditional(linkTitle);
        editMediaPage.fillFieldLinksUrlAdditional(linkUrl);
        editMediaPage.clickCheckboxOpenInNewWindow(); // remove selection 'Open in a new window' by clicking the checkbox
        // Add the 2nd link to Additional material with 'Add another item' button
        editMediaPage.clickButtonAddAnotherLinkAdditionalMaterial();
        editMediaPage.fillFieldLinksTitle2(linkTitle2);
        editMediaPage.fillFieldLinksUrl2(linkUrl2);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();

        // Check that the 1st link opens the page at the same window
        viewMediaPage.clickLinkLinks();
        String urlActual = webDriver.getCurrentUrl();
        viewMediaPage.clickLinkAdditionalMaterialByName(linkTitle);
        webDriver.get(urlActual);
        waitForPageToLoad();

        // Check that the 2nd link opens the page in other tab
        WebElement link2 = getLinkByName(linkTitle2);
        viewMaterialInAnotherBrowserTab(link2);

        // Remove the 1st link
        openEditPage();
        editMediaPage.switchToTabAdditionalMaterial();
        editMediaPage.getFieldLinksTitleAdditional().clear();
        editMediaPage.getFieldLinksUrlAdditional().clear();
        editMediaPage.clickButtonSave();
        // Check that only the 2nd link is displayed on Links popup
        viewMediaPage.clickLinkLinks();
        checkOneLinkNameIsDisplayedOnLinksPopup(linkTitle2);
    }

    @Test
    public void testCheckValidationOfMediaPage() throws Exception {
        // Create a MediaPage template
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewMediaPage viewMediaPage = addContentPage.clickLinkMediaPage();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page rich_media_page temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMediaPage.getLinkEdit().isDisplayed());
        // Clear required fields, remove Video
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.getFieldTitle().clear(); // title
        editMediaPage.getFieldSummary().clear(); // summary
        selectItemFromListByOptionValue(editMediaPage.getFieldLengthHours(), "0"); // length
        editMediaPage.switchToTabEducationalContent();
        editMediaPage.clickLinkRemoveMedia(); // video
        clearFrame(editMediaPage.getFrameVideoSummary()); // video summary
        clearFrame(editMediaPage.getFrameContextualInformation()); // video info
        editMediaPage.clickButtonSave();
        // Submit changes and check error message
        assertTextOfMessage(editMediaPage.getMessageError(), messageErrorMediaPage);

        // Fill in the fields, select video
        editMediaPage.switchToTabGeneral();
        editMediaPage.fillFieldTitle(mediaPageTitle);
        editMediaPage.fillFieldSummary(mediaPageSummary);
        selectItemFromListByOptionValue(editMediaPage.getFieldLengthHours(), "23");
        editMediaPage.switchToTabEducationalContent();
        SelectVideoPage selectVideoPage = editMediaPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlYouTube);
        waitUntilElementIsVisible(editMediaPage.getLinkRemoveMedia());
        fillFrame(editMediaPage.getFrameVideoSummary(), videoSummaryText);
        fillFrame(editMediaPage.getFrameContextualInformation(), contextualInformationText);
        // Submit changes, check that the Media page is updated
        viewMediaPage = editMediaPage.clickButtonSave();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page " + mediaPageTitle + " has been updated.");
    }


    // 'Delete Media page' tests

    @Test
    public void testDeleteMediaPageFromEditPage() throws Exception {
        // Create Media page
        createMediaPage();
        // Delete Media page from its Edit page
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editMediaPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + mediaPageTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();

        // Check info message
        assertTextOfMessage(homePage.getMessage(), ".*Media page .+ has been deleted.");
    }

    @Test
    public void testDeleteMediaPageFromContentTable() throws Exception {
        // Create Media page
        createMediaPage();
        // Delete Media page from Content table with a proper link
        webDriver.get(websiteUrl.concat("/admin/content"));
        waitForPageToLoad();
        ContentPage contentPage = new ContentPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + mediaPageTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();

        // Check info message
        assertTextOfMessage(contentPage.getMessage(), ".*Media page .+ has been deleted.");
    }



}

