package com.inceptum.content;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.pages.AddContentPage;
import com.inceptum.pages.AdministrationPage;
import com.inceptum.pages.ContentPage;
import com.inceptum.pages.CreatePresentationPage;
import com.inceptum.pages.CreateVideoPage;
import com.inceptum.pages.DeleteItemConfirmationPage;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditPresentationPage;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.EditVideoPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.SelectPresentationPage;
import com.inceptum.pages.SelectVideoPage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewPresentationPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;
import com.inceptum.pages.ViewVideoPage;
import com.inceptum.util.PropertyLoader;


public class CourseMaterialsTest extends CourseBaseTest {

    /* ----- CONSTANTS ----- */

    protected final String newYoutubeUrl = "http://www.youtube.com/watch?v=9gTw2EDkaDQ";
    protected final String videoThumbnail = "https://i.ytimg.com/vi/Wz2klMXDqF4/hqdefault.jpg";
    protected final String slideshareUrlForVideo = "http://www.slideshare.net/adnaanwasey/html-fundamentals-for-web-producers";
    protected final String someOtherSite = "https://www.wikipedia.org/";
    protected final String specialSymbols = "“♣☺♂” , “”‘~!@#$%^&*()?>,./\\<][ /*<!–”\", “${code}”;–>)//;";
    protected final String videoTitleNew = "NewVideo";
    protected final String presentationTitleNew = "NewPresentation";


    /* ----- Tests ----- */

// Video content


    // "Create Video" tests

    @Test
    public void testCreateVideoFromContentPage() throws Exception {
        createVideoFromContentPage();
    }


    @Test
    public void testCreateVideoFromTask() throws Exception {
        createTask();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        editTaskPage.clickIconAddVideo();
        CreateVideoPage createVideoPage = new CreateVideoPage(webDriver, pageLocation);
        createVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(urlYouTube);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        assertMessage(editTaskPage.getMessage(), "messages status", ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEO'S field
        String titleOfCreatedVideo = editTaskPage.getFieldVideosTaskResorses().getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEO'S field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(videoTitle + ".*"));
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        // Added Video to the Task is on a View page
        Assert.assertEquals(videoTitle, viewTaskPage.getFieldResource().getText());
    }


    @Test
    public void testCreateVideoFromLibrary() throws Exception {
        // Create Video that will be displayed in Library
        createVideoFromContentPage();
        // Add a video template
        webDriver.get(websiteUrl.concat("/node/add/video"));
        waitForPageToLoad();
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        // Open Edit page, enter a title and select the video from Library
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideoFromLibrary(videoThumbnail);
        viewVideoPage = editVideoPage.clickButtonSave();
        // Check info message
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }

    @Ignore // Now only one Video item exists on Video page.
    @Test
    public void testAddAnotherItemVideo() throws Exception {
        // Add a video template
        webDriver.get(websiteUrl.concat("/node/add/video"));
        waitForPageToLoad();
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        // Enter a title, select the 1st video
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlVimeo);
        Assert.assertTrue(editVideoPage.getLinkRemoveMedia().isDisplayed());
        // Select the 2nd video
        selectVideoPage = editVideoPage.clickLinkSelectMediaFor2ndVideo();
        selectVideoPage.selectVideo(urlYouTube);
        Assert.assertTrue(editVideoPage.getLinkRemoveMediaFor2ndVideo().isDisplayed());
        // Add another item (the 3d video)
        editVideoPage.clickButtonAddAnotherItem();
        waitUntilElementIsVisible(editVideoPage.getLinkSelectMediaFor3dVideo());
        selectVideoPage = editVideoPage.clickLinkSelectMediaFor3dVideo();
        selectVideoPage.selectVideo(urlYouTube);
        Assert.assertTrue(editVideoPage.getLinkRemoveMediaFor3dVideo().isDisplayed());
        viewVideoPage = editVideoPage.clickButtonSave();

        // Check info message and the number of iframes on the page
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        checkNumberOfIframes(3);
    }


    @Test
    public void testAddVideoToTheory() throws Exception {
        createVideoFromContentPage();
        checkAlert();
        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabAdditionalMaterial();
        autoSelectItemFromList(editTheoryPage.getFieldVideosAdditionalMaterial(), videoTitle);
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");
        // Added Video is shown on a View Theory page
        Assert.assertEquals(videoTitle, viewTheoryPage.getFieldMaterial().getText());
    }


    // "Create Video with wrong data" tests

    @Test
    public void testCreateVideoWithEmptyFields() throws Exception {
        // Create video template with 'Add content' link
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        AddContentPage addContentPage = contentPage.clickLinkAddContent();
        ViewVideoPage viewVideoPage = addContentPage.clickLinkVideo();
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        // Open Edit page, remove title and media
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.getFieldTitle().clear();
        editVideoPage.clickLinkRemoveMedia();
        // Click Save
        editVideoPage.clickButtonSaveTop();
        editVideoPage.assertErrorMessage("Title field is required.", "Video is required.");
        // "Title" field has a red frame
        Assert.assertEquals("form-text required error", editVideoPage.getFieldTitle().getAttribute("class"));

        // Enter title, add a video and check that video content is created
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlVimeo);
        viewVideoPage = editVideoPage.clickButtonSave();
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }


    @Test
    public void testVideoWithSlideshareURL() throws Exception {
        // Add a video template
        webDriver.get(websiteUrl.concat("/node/add/video"));
        waitForPageToLoad();
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());

        // Open Edit page, check that the video content can't be created with unsupported provider
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        // Enter an incorrect URL for the video - slideshare URL
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        selectVideoPage.fillFieldUrlOrEmbedCode(slideshareUrlForVideo);
        selectVideoPage.clickButtonSubmit();
        // Check if error message appears
        assertMessage(selectVideoPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        // The field has a red frame
        Assert.assertEquals("media-add-from-url form-text error", selectVideoPage.getFieldUrlOrEmbedCode().getAttribute("class"));

        // Test with correct data (vimeo URL)
        selectVideoPage.getFieldUrlOrEmbedCode().clear();
        selectVideoPage.fillFieldUrlOrEmbedCode(urlVimeo);
        selectVideoPage.clickButtonSubmit();
        webDriver.switchTo().defaultContent();
        viewVideoPage = editVideoPage.clickButtonSave();
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video " + videoTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }


    @Test
    public void testVideoWithIncorrectURL() throws Exception {
        // Add a video template
        webDriver.get(websiteUrl.concat("/node/add/video"));
        waitForPageToLoad();
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        assertTextOfMessage(viewVideoPage.getMessage(), ".*Video video temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());

        // Open Edit page
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();

        // Test with empty url field
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        selectVideoPage.fillFieldUrlOrEmbedCode("");
        selectVideoPage.clickButtonSubmit();
        assertMessage(selectVideoPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        // The field has a red frame
        Assert.assertEquals("media-add-from-url form-text error", selectVideoPage.getFieldUrlOrEmbedCode().getAttribute("class"));

        // Test with other site url
        selectVideoPage.fillFieldUrlOrEmbedCode(someOtherSite);
        selectVideoPage.clickButtonSubmit();
        assertMessage(selectVideoPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        Assert.assertEquals("media-add-from-url form-text error", selectVideoPage.getFieldUrlOrEmbedCode().getAttribute("class"));
        selectVideoPage.getFieldUrlOrEmbedCode().clear();

        // Test with special symbols

        if (!(PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome"))) { // Exception for Linux/Chrome: problem with entering some special symbols
            selectVideoPage.fillFieldUrlOrEmbedCode(specialSymbols);
            selectVideoPage.clickButtonSubmit();
            assertMessage(selectVideoPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
            Assert.assertEquals("media-add-from-url form-text error", selectVideoPage.getFieldUrlOrEmbedCode().getAttribute("class"));
            selectVideoPage.getFieldUrlOrEmbedCode().clear();
        }

        // Test with 0
        selectVideoPage.fillFieldUrlOrEmbedCode("0");
        selectVideoPage.clickButtonSubmit();
        assertMessage(selectVideoPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        Assert.assertEquals("media-add-from-url form-text error", selectVideoPage.getFieldUrlOrEmbedCode().getAttribute("class"));
        selectVideoPage.getFieldUrlOrEmbedCode().clear();

        // Test with a space

        if (!(PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome"))) { // Exception for Linux/Chrome: problem with entering the space
            selectVideoPage.fillFieldUrlOrEmbedCode(" ");
            selectVideoPage.clickButtonSubmit();
            assertMessage(selectVideoPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
            Assert.assertEquals("media-add-from-url form-text error", selectVideoPage.getFieldUrlOrEmbedCode().getAttribute("class"));
            selectVideoPage.getFieldUrlOrEmbedCode().clear();
        }

        // Test with correct data (vimeo URL)
        selectVideoPage.fillFieldUrlOrEmbedCode(urlVimeo);
        selectVideoPage.clickButtonSubmit();
        webDriver.switchTo().defaultContent();
        viewVideoPage = editVideoPage.clickButtonSave();
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }


    // "Edit Video" tests

    @Test
    public void testEditVideoFromEditPage() throws Exception {
        createVideoFromContentPage();
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.getFieldTitle().clear();
        editVideoPage.fillFieldTitle(videoTitleNew);
        ViewVideoPage viewVideoPage = editVideoPage.clickButtonSaveTop();
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        assertTitleIsChangedOnViewPage(viewVideoPage.getFieldTitle(), videoTitleNew);
    }


    @Test
    public void testEditVideoFromContentTable() throws Exception {
        createVideoFromContentPage();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Video title
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.getFieldTitle().clear();
        editVideoPage.fillFieldTitle(videoTitleNew);
        editVideoPage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Video .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), videoTitleNew);
    }


    @Test
    public void testEditVideoFromCourseMaterial() throws Exception {
        createVideoFromContentPage();
        checkAlert();
        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editTheoryPage.getFieldVideosCourseMaterial(), videoTitle);
        EditVideoPage editVideoPage = editTheoryPage.clickIconEditVideo();
        waitUntilElementIsVisible(editVideoPage.getFieldTitle());
        editVideoPage.getFieldTitle().clear();
        editVideoPage.fillFieldTitle(videoTitleNew);
        editVideoPage.clickButtonSave();
        checkAlert();
        assertMessage(editTheoryPage.getMessage(), "messages status", ".*Video .+ has been updated.");
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        // Check that the video was updated also in the Content table
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), videoTitleNew);
    }


    @Test
    public void testEditVideoWithRemoveMediaOption() throws Exception {
        createVideoFromContentPage();
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        checkThumbnailIsShown(editVideoPage.getThumbnailVideo());
        editVideoPage.clickLinkRemoveMedia();
        editVideoPage.checkLinkIsNotDisplayed();
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(newYoutubeUrl);
        Assert.assertTrue(editVideoPage.getLinkRemoveMedia().isDisplayed());
        ViewVideoPage viewVideoPage = editVideoPage.clickButtonSave();
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }

    @Ignore // Now only one Video item exists on Video page.
    @Test
    public void testReorderVideoItems() throws Exception {
        createVideoFromContentPage();
        openEditPage();
        // Add new video (another item) via 'Select media' link
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMediaFor2ndVideo();
        selectVideoPage.selectVideo(newYoutubeUrl);
        // Reorder two video items
        new Actions(webDriver).dragAndDrop(editVideoPage.getFirstVideoItem(), editVideoPage.getSecondVideoItem()).perform();
        // Check that reordering has been done
        waitUntilElementIsVisible(editVideoPage.getMessageReordering());
        Assert.assertTrue(editVideoPage.getMessageReordering().isDisplayed());
        ViewVideoPage viewVideoPage = editVideoPage.clickButtonSave();
        assertMessage(viewVideoPage.getMessage(), "messages messages--status", ".*Video .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
    }



    // "Delete Video" tests

    @Test
    public void testDeleteVideoFromEditPage() throws Exception {
        createVideoFromContentPage();
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editVideoPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + videoTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Video .+ has been deleted.");
    }


    @Test
    public void testDeleteVideoFromContentTable() throws Exception {
        createVideoFromContentPage();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        checkAlert();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + videoTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Video .+ has been deleted.");
        // Check that Content page is opened
        assertElementActiveStatus(contentPage.getLinkContent());
    }


    @Test
    public void testRemoveVideoFromLiveSession() throws Exception {
        createVideoFromContentPage();
        checkAlert();
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), videoTitle);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        // Check that a video title is on ViewLiveSession page
        assertTitleIsChangedOnViewPage(viewLiveSessionPage.getFieldMaterial(), videoTitle);
        openEditPage();
        editLiveSessionPage.switchToTabCourseMaterial();
        editLiveSessionPage.getFieldVideosCourseMaterial().clear();
        editLiveSessionPage.clickButtonSave();
        // Check that video was removed (video title is absent on ViewLiveSession page)
        checkElementIsAbsent(materialCSSpath);
    }


    @Test
    public void testCancelDeletingVideo() throws Exception {
        createVideoFromContentPage();
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editVideoPage.clickButtonDelete();
        deleteItemConfirmationPage.clickLinkCancel();
        ViewVideoPage viewVideoPage =new ViewVideoPage(webDriver, pageLocation);
        Assert.assertTrue("Edit link is not shown on the page.", viewVideoPage.getLinkEdit().isDisplayed());
        // Video title is correct (the same with title of created video)
        Assert.assertTrue(viewVideoPage.getFieldTitle().getText().matches(videoTitle));
    }




// Presentation content


    // "Create Presentation" tests

    @Test
    public void testCreatePresentationFromContentPage() throws Exception {
        createPresentationFromContentPage();
    }


    @Test
    public void testCreatePresentationFromTheory() throws Exception {
        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabAdditionalMaterial();
        CreatePresentationPage createPresentationPage = editTheoryPage.clickIconAddPresentationAdditional();
        createPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        assertMessage(editTheoryPage.getMessage(), "messages status", ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editTheoryPage.getFieldPresentationsAdditionalMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation, titleOfCreatedPresentation.matches(presentationTitle + ".*"));
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");
        // Added Presentation to the Theory is on a View page
        Assert.assertEquals(presentationTitle, viewTheoryPage.getFieldMaterial().getText());
    }


    @Test
    public void testAddPresentationToTask() throws Exception {
        createPresentationFromContentPage();
        createTask();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabAdditionalResources();
        autoSelectItemFromList(editTaskPage.getFieldPresentationsAdditionalResourses(), presentationTitle);
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        // Added Presentation is shown on a View Task page
        Assert.assertEquals(presentationTitle, viewTaskPage.getFieldResource().getText());
    }


    // "Create Presentation with wrong data" tests

    @Test
    public void testCreatePresentationWithEmptyFields() throws Exception {
        // Create presentation template
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        AddContentPage addContentPage = contentPage.clickLinkAddContent();
        ViewPresentationPage viewPresentationPage = addContentPage.clickLinkPresentation();
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation presentation temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
        // Open Edit page, remove title and media
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.getFieldTitle().clear();
        editPresentationPage.clickLinkRemoveMedia();
        // Click Save
        editPresentationPage.clickButtonSaveTop();
        editPresentationPage.assertErrorMessage("Title field is required.", "Slideshare presentation is required.");
        // "Title" field has a red frame
        Assert.assertEquals("form-text required error", editPresentationPage.getFieldTitle().getAttribute("class"));

        // Enter title, select media and check that presentation is created
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        viewPresentationPage = editPresentationPage.clickButtonSave();
        assertMessage(viewPresentationPage.getMessage(), "messages messages--status", ".*Presentation .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
    }


    @Test
    public void testPresentationWithVideoURL() throws Exception {
        // Add presentation template
        webDriver.get(websiteUrl.concat("/node/add/presentation"));
        waitForPageToLoad();
        ViewPresentationPage viewPresentationPage = new ViewPresentationPage(webDriver, pageLocation);
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation presentation temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());

        // Open Edit page, check that presentation cannot be created with unsupported provider
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        // Insert incorrect URL (YouTube URL)
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        selectPresentationPage.fillFieldUrlOrEmbedCode(urlYouTube);
        selectPresentationPage.clickButtonSubmit();
        assertMessage(selectPresentationPage.getMessage(), "messages error", ".+ could not be added. Only the following types of files are allowed to be uploaded: default");
        // "URL or Embed code" field has a red frame
        Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));

        // Insert incorrect URL (Vimeo URL)
        selectPresentationPage.getFieldUrlOrEmbedCode().clear();
        selectPresentationPage.fillFieldUrlOrEmbedCode(urlVimeo);
        selectPresentationPage.clickButtonSubmit();
        assertMessage(selectPresentationPage.getMessage(), "messages error", ".+ could not be added. Only the following types of files are allowed to be uploaded: default");
        Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));

        // Test with correct URL (Slideshare URL)
        selectPresentationPage.getFieldUrlOrEmbedCode().clear();
        selectPresentationPage.fillFieldUrlOrEmbedCode(urlSlideshare);
        selectPresentationPage.clickButtonSubmit();
        selectPresentationPage.conditionPresentationInLibrary();
        viewPresentationPage = editPresentationPage.clickButtonSave();
        assertMessage(viewPresentationPage.getMessage(), "messages messages--status", ".*Presentation .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
    }


    @Test
    public void testPresentationWithIncorrectURL() throws Exception {
        // Add presentation template
        webDriver.get(websiteUrl.concat("/node/add/presentation"));
        waitForPageToLoad();
        ViewPresentationPage viewPresentationPage = new ViewPresentationPage(webDriver, pageLocation);
        assertTextOfMessage(viewPresentationPage.getMessage(), ".*Presentation presentation temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());

        // Open Edit page
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();

        // Test with empty url field
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        selectPresentationPage.fillFieldUrlOrEmbedCode("");
        selectPresentationPage.clickButtonSubmit();
        assertMessage(selectPresentationPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        // The field has a red frame
        Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));

        // Test with other site url
        selectPresentationPage.fillFieldUrlOrEmbedCode(someOtherSite);
        selectPresentationPage.clickButtonSubmit();
        assertMessage(selectPresentationPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));
        selectPresentationPage.getFieldUrlOrEmbedCode().clear();

        // Test with special symbols

        if (!(PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome"))) { // Exception for Linux/Chrome: problem with entering some special symbols
            selectPresentationPage.fillFieldUrlOrEmbedCode(specialSymbols);
            selectPresentationPage.clickButtonSubmit();
            assertMessage(selectPresentationPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
            Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));
            selectPresentationPage.getFieldUrlOrEmbedCode().clear();
        }

        // Test with 0
        selectPresentationPage.fillFieldUrlOrEmbedCode("0");
        selectPresentationPage.clickButtonSubmit();
        assertMessage(selectPresentationPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
        Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));
        selectPresentationPage.getFieldUrlOrEmbedCode().clear();

        // Test with a space

        if (!(PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome"))) { // Exception for Linux/Chrome: problem with entering the space
            selectPresentationPage.fillFieldUrlOrEmbedCode(" ");
            selectPresentationPage.clickButtonSubmit();
            assertMessage(selectPresentationPage.getMessage(), "messages error", ".+Unable to handle the provided embed string or URL.");
            Assert.assertEquals("media-add-from-url form-text error", selectPresentationPage.getFieldUrlOrEmbedCode().getAttribute("class"));
            selectPresentationPage.getFieldUrlOrEmbedCode().clear();
        }

        // Enter correct URL (Slideshare URL)
        selectPresentationPage.fillFieldUrlOrEmbedCode(urlSlideshare);
        selectPresentationPage.clickButtonSubmit();
        selectPresentationPage.conditionPresentationInLibrary();
        viewPresentationPage = editPresentationPage.clickButtonSave();
        assertMessage(viewPresentationPage.getMessage(), "messages messages--status", ".*Presentation .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
    }


    // "Edit Presentation" tests

    @Test
    public void testEditPresentationFromEditPage() throws Exception {
        createPresentationFromContentPage();
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.getFieldTitle().clear();
        editPresentationPage.fillFieldTitle(presentationTitleNew);
        ViewPresentationPage viewPresentationPage = editPresentationPage.clickButtonSaveTop();
        assertMessage(viewPresentationPage.getMessage(), "messages messages--status", ".*Presentation .+ has been updated.");
        assertTitleIsChangedOnViewPage(viewPresentationPage.getFieldTitle(), presentationTitleNew);
    }


    @Test
    public void testEditPresentationFromContentTable() throws Exception {
        createPresentationFromContentPage();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Presentation title
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.getFieldTitle().clear();
        editPresentationPage.fillFieldTitle(presentationTitleNew);
        editPresentationPage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Presentation .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), presentationTitleNew);
    }


    @Test
    public void testEditPresentationFromCourseMaterial() throws Exception {
        createPresentationFromContentPage();
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldPresentationsCourseMaterial(), presentationTitle);
        EditPresentationPage editPresentationPage = editLiveSessionPage.clickIconEditPresentation();
        editPresentationPage.getFieldTitle().clear();
        editPresentationPage.fillFieldTitle(presentationTitleNew);
        editPresentationPage.clickButtonSave();
        checkAlert();
        assertMessage(editLiveSessionPage.getMessage(), "messages status", ".*Presentation .+ has been updated.");
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        // Check that the presentation was updated also in the Content table
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), presentationTitleNew);
    }


    @Test
    public void testEditPresentationWithRemoveMediaOption() throws Exception {
        createPresentationFromContentPage();
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        checkThumbnailIsShown(editPresentationPage.getThumbnailPresentation());
        editPresentationPage.clickLinkRemoveMedia();
        editPresentationPage.checkLinkIsNotDisplayed();
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        // Select the 1st presentation from Library
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        selectPresentationPage.clickLinkLibrary();
        selectPresentationPage.waitingTillThumbnailsAppear();
        selectPresentationPage.select1stThumbnailFromList();
        selectPresentationPage.clickButtonSubmitLibrary();
        webDriver.switchTo().defaultContent();
        waitUntilElementIsClickable(editPresentationPage.getLinkRemoveMedia());
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        ViewPresentationPage viewPresentationPage = editPresentationPage.clickButtonSave();
        assertMessage(viewPresentationPage.getMessage(), "messages messages--status", ".*Presentation .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPresentationPage.getLinkEdit().isDisplayed());
    }


    // "Delete Presentation" tests

    @Test
    public void testDeletePresentationFromEditPage() throws Exception {
        createPresentationFromContentPage();
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editPresentationPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + presentationTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Presentation .+ has been deleted.");
    }


    @Test
    public void testDeletePresentationFromContentTable() throws Exception {
        createPresentationFromContentPage();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        checkAlert();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + presentationTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Presentation .+ has been deleted.");
        // Check that Content page is opened
        assertElementActiveStatus(contentPage.getLinkContent());
    }


    @Test
    public void testRemovePresentationFromTask() throws Exception {
        createPresentationFromContentPage();
        checkAlert();
        createTask();
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        autoSelectItemFromList(editTaskPage.getFieldPresentationsTaskResorses(), presentationTitle);
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        // Check that a presentation title is on ViewTask page
        assertTitleIsChangedOnViewPage(viewTaskPage.getFieldResource(), presentationTitle);
        openEditPage();
        editTaskPage.switchToTabTaskResources();
        editTaskPage.getFieldPresentationsTaskResorses().clear();
        editTaskPage.clickButtonSave();
        // Check that presentation was removed (presentation title is absent on ViewTask page)
        checkElementIsAbsent(materialCSSpath);
    }


    @Test
    public void testCancelDeletingPresentation() throws Exception {
        createPresentationFromContentPage();
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        ContentPage contentPage = administrationPage.clickButtonFindContent();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        deleteItemConfirmationPage.clickLinkCancel();
        assertElementActiveStatus(contentPage.getLinkContent());
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), presentationTitle);
    }






}


