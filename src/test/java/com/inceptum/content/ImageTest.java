package com.inceptum.content;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.pages.AddContentPage;
import com.inceptum.pages.AdministrationPage;
import com.inceptum.pages.ContentPage;
import com.inceptum.pages.CreateImagePage;
import com.inceptum.pages.DeleteItemConfirmationPage;
import com.inceptum.pages.EditImagePage;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.SelectImagePage;
import com.inceptum.pages.ViewImagePage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewTheoryPage;

/**
 * Created by Olga on 27.10.2014.
 */
public class ImageTest extends CourseBaseTest {


    /* ----- CONSTANTS ----- */

    protected final String imageTitleNew = "NewImage";



    // Image content


    // Create "Image" tests

    @Test
    public void testCreateImageFromContentPage() throws Exception {
        createImageFromContentPage(image1Path);
    }


    @Test
    public void testCreateImageFromTheory() throws Exception {
        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabCourseMaterial();
        CreateImagePage createImagePage = editTheoryPage.clickIconAddImage();
        createImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertMessage(editTheoryPage.getMessage(), "messages status", ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editTheoryPage.getFieldImagesCourseMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(imageTitle + ".*"));
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");
        // Added Image to the Theory is on a View page
        Assert.assertEquals(imageTitle, viewTheoryPage.getFieldMaterial().getText());
    }



    @Test
    public void testCreateImageFromLibrary() throws  Exception {
        // Create an image (this image will be displayed in Library)
        createImageFromContentPage(image1Path);
        // Add an image template
        webDriver.get(websiteUrl.concat("/node/add/image"));
        waitForPageToLoad();
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        // Enter a title, select image from library
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImageFromLibrary();
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        viewImagePage = editImagePage.clickButtonSave();
        // Check info message
        assertMessage(viewImagePage.getMessage(), "messages messages--status", ".*Image .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
    }

    //It is possible to create only one image on Image page from http://v3-4-20.staging.imindsx.org/
    @Ignore
    @Test
    public void testAddAnotherItemImage() throws Exception {
        // Add an image template
        webDriver.get(websiteUrl.concat("/node/add/image"));
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        waitForPageToLoad();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        // Enter a title, select the 1st image
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMediaFor2ndImage();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMediaFor2ndImage().isDisplayed());
        // Add another item (the 2nd image)
        editImagePage.clickButtonAddAnotherItem();
        waitUntilElementIsVisible(editImagePage.getLinkSelectMediaFor3dImage());
        selectImagePage = editImagePage.clickLinkSelectMediaFor3dImage();
        selectImagePage.selectImage(image2Path);
        waitUntilElementIsVisible(editImagePage.getLinkRemoveMediaFor3dImage());
        Assert.assertTrue(editImagePage.getLinkRemoveMediaFor3dImage().isDisplayed());
        viewImagePage = editImagePage.clickButtonSave();

        // Check info message, number of images on the page
        assertMessage(viewImagePage.getMessage(), "messages messages--status", ".*Image .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        checkNumberOfImages(3); // the 3d image was added by default when the content template was created
    }

    @Test
    public void testImageWithDiffFileFormat() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add/image"));
        waitForPageToLoad();
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());

        // Open Edit page
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle);

        // .jpg format
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image3Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());

        // .png format
        SelectImagePage selectImagePage1 = editImagePage.clickLinkSelectMedia();
        selectImagePage1.selectImage(image4Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());

        // .gif format
        SelectImagePage selectImagePage2 = editImagePage.clickLinkSelectMedia();
        selectImagePage2.selectImage(image7Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        waitUntilElementIsVisible(editImagePage.getButtonSave());   // This wait() is needed because "Add another item" button is clicked instead of Save.
        viewImagePage = editImagePage.clickButtonSave();
        assertMessage(viewImagePage.getMessage(), "messages messages--status", ".*Image .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());


    }



    @Test
    public void testAddImageToLiveSession() throws Exception {
        createLiveSession();
        createImageFromContentPage(image1Path);
        webDriver.get(websiteUrl.concat("/admin/content"));
        waitForPageToLoad();
        clickElementByLinkText(liveSessionTitle);
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldImagesCourseMaterial(), imageTitle);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        // Added Image is shown on a ViewLiveSession page
        Assert.assertEquals(imageTitle, viewLiveSessionPage.getFieldMaterial().getText());
    }


    // "Create Image with wrong data" tests


    @Test
    public void testCreateImageWithEmptyFields() throws Exception {
        // Create image template with 'Add content' link
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        administrationPage.clickButtonMenu();
        ContentPage contentPage = administrationPage.clickButtonContent();
        AddContentPage addContentPage = contentPage.clickLinkAddContent();
        ViewImagePage viewImagePage = addContentPage.clickLinkImage();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        // Open Edit page, remove title and media
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.getFieldTitle().clear();
        editImagePage.clickLinkRemoveMedia();
        // Click Save
        editImagePage.clickButtonSaveTop();

        // Check that error message is shown
        editImagePage.assertErrorMessage("Title field is required.", "Image is required.");
        // "Title" field has a red frame
        Assert.assertEquals("form-text required error", editImagePage.getFieldTitle().getAttribute("class"));

        // Enter title, upload an image and check that the image content is created
        editImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        viewImagePage = editImagePage.clickButtonSave();
        assertMessage(viewImagePage.getMessage(), "messages messages--status", ".*Image .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
    }



    @Test
    public void testCreateImageWithIncorrectFileFormat() throws Exception {
        // Add an image template
        webDriver.get(websiteUrl.concat("/node/add/image"));
        waitForPageToLoad();
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        // Open Edit page
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();

        // Test without any file selected
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mediaBrowser")));
        webDriver.switchTo().frame("mediaBrowser");
        selectImagePage.clickButtonSubmit();
        assertMessage(selectImagePage.getMessage(), "messages error", ".*No file appears to have been selected.");
        // "Upload a new file" field has a red frame
        Assert.assertEquals("form-file error", selectImagePage.getFieldUploadANewFile().getAttribute("class"));

        // Test with incorrect file format
        selectImagePage.selectImageInFrame(docxFilePath);
        assertMessage(selectImagePage.getMessage(), "messages error", ".*The specified file .+ could not be uploaded. Only files with the following extensions are allowed: png gif jpg jpeg.");
        // "Upload a new file" field has a red frame
        Assert.assertEquals("form-file error", selectImagePage.getFieldUploadANewFile().getAttribute("class"));

        // Test with correct data
        selectImagePage.selectImageInFrame(image1Path);
        webDriver.switchTo().defaultContent();
        waitUntilElementIsClickable(editImagePage.getLinkRemoveMedia());
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        viewImagePage = editImagePage.clickButtonSave();
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image " + imageTitle + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
    }


    // "Edit Image" tests

    @Test
    public void testEditImageFromEditPage() throws Exception {
        createImageFromContentPage(image1Path);
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        waitUntilElementIsVisible(editImagePage.getFieldTitle());
        editImagePage.getFieldTitle().clear();
        editImagePage.fillFieldTitle(imageTitleNew);
        ViewImagePage viewImagePage = editImagePage.clickButtonSaveTop();
        assertMessage(viewImagePage.getMessage(), "messages messages--status", ".*Image .+ has been updated.");
        assertTitleIsChangedOnViewPage(viewImagePage.getFieldTitle(), imageTitleNew);
    }


    @Test
    public void testEditImageFromContentTable() throws Exception {
        createImageFromContentPage(image1Path);
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Edit Image title
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        waitUntilElementIsVisible(editImagePage.getFieldTitle());
        editImagePage.getFieldTitle().clear();
        editImagePage.fillFieldTitle(imageTitleNew);
        editImagePage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Image .+ has been updated.");
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), imageTitleNew);
    }


    @Test
    public void testEditImageFromAdditionalMaterial() throws Exception {
        createImageFromContentPage(image1Path);
        checkAlert();
        createTheory();
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabAdditionalMaterial();
        autoSelectItemFromList(editTheoryPage.getFieldImagesAdditionalMaterial(), imageTitle);
        EditImagePage editImagePage = editTheoryPage.clickIconEditImage();
        waitUntilElementIsVisible(editImagePage.getFieldTitle());
        editImagePage.getFieldTitle().clear();
        editImagePage.fillFieldTitle(imageTitleNew);
        editImagePage.clickButtonSave();
        assertMessage(editTheoryPage.getMessage(), "messages status", ".*Image .+ has been updated.");
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        ContentPage contentPage = administrationPage.clickButtonContent();
        // Check that the image was updated also in the Content table
        assertTitleIsChangedInContentTable(contentPage.getTitleOf1stElement(), imageTitleNew);
    }


    @Test
    public void testEditImageWithRemoveMediaOption() throws Exception {
        // Add an image template
        webDriver.get(websiteUrl.concat("/node/add/image"));
        waitForPageToLoad();
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        assertTextOfMessage(viewImagePage.getMessage(), ".*Image image temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        // Open Edit page and add two images
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());

        editImagePage.clickButtonSave();

        openEditPage();
        //Remove Image
        editImagePage.clickLinkRemoveMedia();

        editImagePage.clickButtonSave();
        assertMessage(editImagePage.getMessage1(), "messages error", ".*Image is required.");

    }



  //Now this test is not actual, because you can add only one image on Image page(from http://v3-4-20.staging.imindsx.org/)
    @Ignore
    @Test
    public void testReorderImageItems() throws Exception {
        createImageFromContentPage(image1Path);
        openEditPage();
        // Add new image (another item) via 'Select media' link
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMediaFor2ndImage();
        selectImagePage.selectImage(image2Path);
        // Reorder two image items
        new Actions(webDriver).dragAndDrop(editImagePage.getFirstImageItem(), editImagePage.getSecondImageItem()).perform();
        // Check that reordering has been done
        Assert.assertEquals("tabledrag-changed-warning messages warning", editImagePage.getMessageReordering().getAttribute("class"));
        ViewImagePage viewImagePage = editImagePage.clickButtonSave();
        assertMessage(viewImagePage.getMessage(), "messages messages--status", ".*Image .+ has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
    }



    // "Delete Image" tests

    @Test
    public void testDeleteImageFromEditPage() throws Exception {
        createImageFromContentPage(image1Path);
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editImagePage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + imageTitle + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Image .+ has been deleted.");
    }


    @Test
    public void testDeleteImageFromContentTable() throws Exception {
        createImageFromContentPage(image1Path);
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        ContentPage contentPage = administrationPage.clickButtonContent();
        checkAlert();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        waitUntilElementIsVisible(deleteItemConfirmationPage.getButtonDelete());
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + imageTitle + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        assertMessage(contentPage.getMessage(), "messages status", ".*Image .+ has been deleted.");
        // Check that Content page is opened
        assertElementActiveStatus(contentPage.getLinkContent());
    }


    @Test
    public void testRemoveImageFromLiveSession() throws Exception {
        createImageFromContentPage(image1Path);
        checkAlert();
        createLiveSession();
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldImagesCourseMaterial(), imageTitle);
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit());
        // Check that an image title is on ViewLiveSession page
        assertTitleIsChangedOnViewPage(viewLiveSessionPage.getFieldMaterial(), imageTitle);
        openEditPage();
        editLiveSessionPage.switchToTabCourseMaterial();
        waitUntilElementIsVisible(editLiveSessionPage.getFieldImagesCourseMaterial());
        editLiveSessionPage.getFieldImagesCourseMaterial().clear();
        editLiveSessionPage.clickButtonSave();
        // Check that image was removed (image title is absent on ViewLiveSession page)
        checkElementIsAbsent(materialCSSpath);
    }


    @Test
    public void testCancelDeletingImage() throws Exception {
        createImageFromContentPage(image1Path);
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editImagePage.clickButtonDelete();
        deleteItemConfirmationPage.clickLinkCancel();
        ViewImagePage viewImagePage =new ViewImagePage(webDriver, pageLocation);
        Assert.assertTrue("Edit link is not shown on the page.", viewImagePage.getLinkEdit().isDisplayed());
        // Image title is correct (the same with title of created image)
        Assert.assertTrue(viewImagePage.getFieldTitle().getText().matches(imageTitle));
    }

}
