package com.inceptum.content;


import org.junit.Assert;
import org.junit.Test;

import com.inceptum.pages.AdministrationPage;
import com.inceptum.pages.ContentPage;
import com.inceptum.pages.DeleteItemConfirmationPage;
import com.inceptum.pages.EditMultipleChoiceQuestionPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.ViewMultipleChoiceQuestionPage;
import com.inceptum.util.PropertyLoader;


public class InteractivityTest extends CourseBaseTest {

    /*---------CONSTANTS--------*/

    protected final String titleOfQuestion = "Name";
    protected final String alternative4 = "John";
    protected final String question1 = "WhatIsYourName?";
    protected final String question2 = "HowOldAreYou?";
    protected final String question3 = "WhereAreYouFrom?";


    /* ----- Tests ----- */

// Multiple Choice Question content


    // "Create Multiple Choice Question" tests

    @Test
    public void testCreateMCQuestionFromContentPage() throws Exception {
        createMCQuestionFromContentPage(alternativesName);
    }


    @Test
    public void testEditMCQuestionFromEditPage() throws Exception {
        createMCQuestionFromContentPage(alternativesName);
        openEditPage();
        // Assert that title field is not empty (was added automatically from question)
        EditMultipleChoiceQuestionPage editMultipleChoiceQuestionPage = new EditMultipleChoiceQuestionPage(webDriver, pageLocation);
        editMultipleChoiceQuestionPage.checkTitleFieldIsNotEmpty();
        editMultipleChoiceQuestionPage.getFieldTitle().clear();
        editMultipleChoiceQuestionPage.fillFieldTitle(titleOfQuestion);
        ViewMultipleChoiceQuestionPage viewMultipleChoiceQuestionPage = editMultipleChoiceQuestionPage.clickButtonSave();
        assertTextOfMessage(viewMultipleChoiceQuestionPage.getMessage(), ".*Multiple choice question " + titleOfQuestion + " has been updated.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMultipleChoiceQuestionPage.getLinkEdit().isDisplayed());
    }


    @Test
    public void testEditMCQFromContentTable() throws Exception {
        createMCQuestionFromContentPage(alternativesName);
        AdministrationPage administrationPage = new AdministrationPage(webDriver, pageLocation);
        ContentPage contentPage = administrationPage.clickButtonContent();
        contentPage.clickIconEditFor1stElement();
        // Add the 4th Alternative
        EditMultipleChoiceQuestionPage editMultipleChoiceQuestionPage = new EditMultipleChoiceQuestionPage(webDriver, pageLocation);
        editMultipleChoiceQuestionPage.clickButtonAddMoreAlternatives();
        editMultipleChoiceQuestionPage.waitUntilCheckboxAlternative4Appears();
        editMultipleChoiceQuestionPage.clickCheckboxAlternative4();
        fillFrame(editMultipleChoiceQuestionPage.getFrameAlternative4(), alternative4);
        editMultipleChoiceQuestionPage.clickButtonSave();
        assertMessage(contentPage.getMessage(), "messages status", ".*Multiple choice question .+ has been updated.");
        contentPage.clickLinkTitleOf1stElement();
        ViewMultipleChoiceQuestionPage viewMultipleChoiceQuestionPage = new ViewMultipleChoiceQuestionPage(webDriver, pageLocation);
        viewMultipleChoiceQuestionPage.checkNumberOfCorrectAnswers(2);
    }


    @Test
    public void testDeleteMCQFromEditPage() throws Exception {
        createMCQuestionFromContentPage(alternativesName);
        openEditPage();
        EditMultipleChoiceQuestionPage editMultipleChoiceQuestionPage = new EditMultipleChoiceQuestionPage(webDriver, pageLocation);
        String title = editMultipleChoiceQuestionPage.getFieldTitle().getAttribute("value");
        DeleteItemConfirmationPage deleteItemConfirmationPage = editMultipleChoiceQuestionPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete.*");
        deleteItemConfirmationPage.clickButtonDelete();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        // Replace "?" in the title to assert correct regex (test doesn't work with "?")
        if (!(PropertyLoader.loadProperty("browser.platform").equals("linux") &
                PropertyLoader.loadProperty("browser.name").equals("chrome"))) {
            title = title.replace('?', '.');
        }
        assertTextOfMessage(homePage.getMessage(), ".*Multiple choice question " + title + ".*has been deleted.");
    }

    @Test
    public void testCreateQuiz() throws Exception { createQuiz(); }

    @Test
    public void testAddQuestionsToQuiz() throws Exception {
        // Create three questions
        createMCQuestionWithData(question1, alternativesName, 1);
        createMCQuestionWithData(question2, alternativesAge, 2);
        createMCQuestionWithData(question3, alternativesCountry, 3);
        // Create a Quiz
        createQuiz();
        // Add the questions to the Quiz
        addQuestionsToQuiz(question1,question2, question3);
    }

}
