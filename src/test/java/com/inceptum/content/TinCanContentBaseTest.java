package com.inceptum.content;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.Duration;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.AdminProfilePage;
import com.inceptum.pages.EditMultipleChoiceQuestionPage;
import com.inceptum.pages.EditQuizPage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.RecentLogMessagesPage;

/**
 * Created by Olga on 18.11.2014.
 */
public class TinCanContentBaseTest extends CourseBaseTest {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/



     /* ----- METHODS ----- */

    protected String getTextOfTinCanLogMessageSecond() throws Exception {
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);
        openRecentLogMessagesPage();
        clickSecondTinCanApiLink();
        String logMessage = recentLogMessagesPage.getTextOfLogMessage();
        return logMessage;
    }
    protected void clickSecondTinCanApiLink() throws Exception {
        RecentLogMessagesPage recentLogMessagesPage = new RecentLogMessagesPage(webDriver, pageLocation);
        List<WebElement> messages = recentLogMessagesPage.getLogTable().findElements(By.xpath("//td[text()='tincanapi']/.././td[4]/a"));
        int i = 0;
        for (WebElement message : messages) {
            if (message.getText().startsWith("{\"request\":")) {
                i++;
                if (i == 2) {
                    message.click();
                    break;
                }
            }
        }
    }
    protected void checkObjectMCQuestionViewValues(String logMessage, String id, String name, String type) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'id' value
        String objectID = (String)object.get("id");
        Assert.assertTrue("ID value '" + objectID + "' of key 'object' is not equal to '" + id + "'", objectID.equals(id));

        // Check 'name' value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        Map<String, Object> objectName = (Map)objectDefinition.get("name");
        String objectEnUS = (String)objectName.get("en-US");
        Assert.assertTrue("Name value '" + objectEnUS + "' of key 'object' is not equal to '" + name + "'", objectEnUS.equals(name));

        // Check 'type' value
        String typeValue = (String)objectDefinition.get("type");
        Assert.assertTrue("Type value '" + typeValue + "' of key 'object' is not equal to '" + type + "'", typeValue.equals(type));
    }
    protected void checkResultValue(String logMessage, String success) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'result' value
        Map<String, Object> result = (Map)post.get("result");

        // Check 'success' value
        Object successObjectValue = result.get("success");
        String successValue = (successObjectValue == null) ? null : successObjectValue.toString(); // convert bool value to string
        Assert.assertTrue("Success value '" + successValue + "' of key 'result' is not equal to '" + success + "'", successValue.equals(success));
    }
    protected void checkObjectQuizValues(String logMessage, String id, String name) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'id' value
        String objectID = (String)object.get("id");
        Assert.assertTrue("ID value '" + objectID + "' of key 'object' is not equal to '" + id + "'", objectID.equals(id));

        // Check 'name' value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        Map<String, Object> objectName = (Map)objectDefinition.get("name");
        String objectEnUS = (String)objectName.get("en-US");
        Assert.assertTrue("Name value '" + objectEnUS + "' of key 'object' is not equal to '" + name + "'", objectEnUS.equals(name));
    }
    protected void checkResultQuizValues(String logMessage, int score, String completion, String success) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'result' value
        Map<String, Object> result = (Map)post.get("result");

        // Check 'score' value
        Map<String, Object> scoreResult = (Map)result.get("score");
        Object scaledValue = scoreResult.get("scaled");
        String scaled = (scaledValue == null) ? null : scaledValue.toString();
        String scoreActual = Integer.toString(score);
        Assert.assertTrue("Score value '" + scaled + "' of key 'result' is not equal to '" + scoreActual + "'", scaled.equals(scoreActual));

        // Check 'completion' value
        Object completionObjectValue = result.get("completion");
        String completionValue = (completionObjectValue == null) ? null : completionObjectValue.toString();
        Assert.assertTrue("Completion value '" + completionValue + "' of key 'result' is not equal to '" + completion + "'", completionValue.equals(completion));

        // Check 'success' value
        Object successObjectValue = result.get("success");
        String successValue = (successObjectValue == null) ? null : successObjectValue.toString();
        Assert.assertTrue("Success value '" + successValue + "' of key 'result' is not equal to '" + success + "'", successValue.equals(success));
    }
    protected void checkResultQuizValuesPassRate(String logMessage, double score, String completion, boolean success) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'result' value
        Map<String, Object> result = (Map)post.get("result");

        // Check 'score' value
        Map<String, Object> scoreResult = (Map)result.get("score");
        Object scaledValue = scoreResult.get("scaled");
        String scaled = (scaledValue == null) ? null : scaledValue.toString();
        String scoreActual = Double.toString(score);
        Assert.assertTrue("Score value '" + scaled + "' of key 'result' is not equal to '" + scoreActual + "'", scaled.equals(scoreActual));

        // Check 'completion' value
        Object completionObjectValue = result.get("completion");
        String completionValue = (completionObjectValue == null) ? null : completionObjectValue.toString();
        Assert.assertTrue("Completion value '" + completionValue + "' of key 'result' is not equal to '" + completion + "'", completionValue.equals(completion));

        // Check 'success' value
        Object successObjectValue = result.get("success");
        String successValue = (successObjectValue == null) ? null : successObjectValue.toString();
        String successExpected = Boolean.toString(success);
        Assert.assertTrue("Success value '" + successValue + "' of key 'result' is not equal to '" + successExpected + "'", successValue.equals(successExpected));
    }
    protected void checkObjectAnswerQuestionValues(String logMessage, String id, String name, String type, String interactionType) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'id' value
        String objectID = (String)object.get("id");
        Assert.assertTrue("ID value '" + objectID + "' of key 'object' is not equal to '" + id + "'", objectID.equals(id));

        // Check 'name' value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        Map<String, Object> objectName = (Map)objectDefinition.get("name");
        String objectEnUS = (String)objectName.get("en-US");
        Assert.assertTrue("Name value '" + objectEnUS + "' of key 'object' is not equal to '" + name + "'", objectEnUS.contains(name));

        // Check 'type' value
        String objectType = (String)objectDefinition.get("type");
        Assert.assertTrue("Type value '" + objectType + "' of key 'object' is not equal to '" + type + "'", objectType.equals(type));

        // Check 'interactionType' value
        String objectInteractionType = (String)objectDefinition.get("interactionType");
        Assert.assertTrue("InteractionType value '" + objectInteractionType + "' of key 'object' is not equal to '" + interactionType + "'", objectInteractionType.equals(interactionType));
    }
    protected void checkAlternatives(String logMessage, List<String> alternatives) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'object' value
        Map<String, Object> object = (Map)post.get("object");

        // Check 'choices' (description) value
        Map<String, Object> objectDefinition = (Map)object.get("definition");
        JSONArray choices = (JSONArray)objectDefinition.get("choices"); // get list of choices

        for (int i = 0; i < choices.size(); i++) {
            Map<String, Object> choice = (Map<String, Object>) choices.get(i);
            Map<String, Object> description = (Map)choice.get("description");
            String objectEnUS = (String)description.get("en-US"); // get 'description' values
            // Check each description
            switch (i) {
                case 0:
                    Assert.assertTrue("Description value '" + objectEnUS + "' of key 'choices' is not equal to '" + alternatives.get(0) + "'", objectEnUS.contains(alternatives.get(0)));
                    break;
                case 1:
                    Assert.assertTrue("Description value '" + objectEnUS + "' of key 'choices' is not equal to '" + alternatives.get(1) + "'", objectEnUS.contains(alternatives.get(1)));
                    break;
                case 2:
                    Assert.assertTrue("Description value '" + objectEnUS + "' of key 'choices' is not equal to '" + alternatives.get(2) + "'", objectEnUS.contains(alternatives.get(2)));
                    break;
            }
        }
    }
    protected void checkExtensionsOrder(String logMessage, int order) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");

        // Check 'order' value
        Map<String, Object> extensions = (Map)context.get("extensions");
        Object orderObjectValue = extensions.get("http://orw.iminds.be/tincan/order");
        String orderValue = (orderObjectValue == null) ? null : orderObjectValue.toString();
        String orderActual = Integer.toString(order); // convert int 'order' value to string
        Assert.assertTrue("Order value '" + orderValue + "' of key 'context' is not equal to '" + orderActual + "'", orderValue.equals(orderActual));
    }
    public String getQuestionNodeUrl() throws Exception {
        openEditPage();
        EditMultipleChoiceQuestionPage editMultipleChoiceQuestionPage = new EditMultipleChoiceQuestionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editMultipleChoiceQuestionPage.getFieldTitle());
        String editQuestionUrl = webDriver.getCurrentUrl();
        String[] parts = editQuestionUrl.split("/edit"); // Separate needed node URL
        String questionNodeUrl = parts[0];
        return questionNodeUrl;
    }
    public String getQuizNodeUrl() throws Exception {
        openEditPage();
        EditQuizPage editQuizPage = new EditQuizPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editQuizPage.getFieldTitle());
        String editQuizUrl = webDriver.getCurrentUrl();
        String[] parts = editQuizUrl.split("/edit"); // Separate needed node URL
        String quizNodeUrl = parts[0];
        return quizNodeUrl;
    }
    public void checkPassRate(int passRate) throws Exception {
        openEditPage();
        EditQuizPage editQuizPage = new EditQuizPage(webDriver, pageLocation);
        editQuizPage.checkPassRateIsEqual(passRate);
    }
    public void checkContextQuizValue(String logMessage, int durationInSeconds) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'context' value
        Map<String, Object> context = (Map)post.get("context");

        // Get 'duration' value
        Map<String, Object> extensions = (Map)context.get("extensions");
        String durationValue = (String)extensions.get("http://orw.iminds.be/tincan/duration");
        // Count seconds
        Duration durationInt = DatatypeFactory.newInstance().newDuration(durationValue);
        int hours = durationInt.getHours();
        int minutes = durationInt.getMinutes();
        int seconds = durationInt.getSeconds();
        int duration = seconds + (60 * minutes) + (3600 * hours);
        // Check that duration value from statement equals actual duration value
        Assert.assertTrue("Duration value '" + duration + "' of key 'context' is not equal to '" +
                durationInSeconds + "'", duration==durationInSeconds);
    }
    public int getDuration(String quizUrl) throws Exception {
        // Open MyResults page
        AdminProfilePage adminProfilePage = new AdminProfilePage(webDriver, pageLocation);
        adminProfilePage.switchToTabMyResults();
        assertElementActiveStatus(adminProfilePage.getTabMyResults());
        // Find needed quiz by its node url
        String[] parts = quizUrl.split(websiteUrl); // separate needed node URL
        String trimmedQuizUrl = parts[1];
        WebElement linkQuiz = webDriver.findElement(By.xpath("//a[@href='" + trimmedQuizUrl + "']"));
        // Get value from 'duration' field
        String durationText = linkQuiz.findElement(By.xpath("./../../td[3]")).getText();
        parts = durationText.split("\\nDuration : ");
        String value = parts[1];
        // Separate the value into hours, minutes, seconds
        String[] values = value.split(":");
        int hours=Integer.parseInt(values[0]);
        int minutes=Integer.parseInt(values[1]);
        int seconds=Integer.parseInt(values[2]);
        // Count seconds
        int duration = seconds + (60 * minutes) + (3600 * hours);
        return duration;
    }
    public boolean quizPassed(int passRateActual, int passRateExpected) throws Exception {
        // Compare these two values
        if (passRateActual>=passRateExpected) {
            return true;
        }
        return false;
    }
    public int getPassRateActual(String quizUrl) throws Exception {
        // Open MyResults page
        AdminProfilePage adminProfilePage = new AdminProfilePage(webDriver, pageLocation);
        adminProfilePage.switchToTabMyResults();
        assertElementActiveStatus(adminProfilePage.getTabMyResults());
        // Find needed quiz by its node url
        String[] parts = quizUrl.split(websiteUrl); // separate needed node URL
        String trimmedQuizUrl = parts[1];
        WebElement linkQuiz = webDriver.findElement(By.xpath("//a[@href='" + trimmedQuizUrl + "']"));
        // Get value from 'score' field
        String scoreText = linkQuiz.findElement(By.xpath("./../../td[4]")).getText();
        parts = scoreText.split(" %");
        String scoreValue = parts[0];
        int passRateActual = Integer.parseInt(scoreValue);
        return passRateActual;
    }
    public int getCurrentTimeVimeoVideo() throws Exception {
        // Switch to media frame
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        WebElement fieldCurrentTime = webDriver.findElement(By.cssSelector("div.played"));

        // Get time in seconds
        String currentTimeText = fieldCurrentTime.getAttribute("aria-valuenow");
        webDriver.switchTo().defaultContent();
        String[] parts = currentTimeText.split("\\.");
        int currentTime=Integer.parseInt(parts[0]);
        return currentTime;
    }
    public void checkVideoCompleteStatements(List<String> statements, String videoNodeUrl, String url, String videoTitle,
                                             String videoName, String classNodeUrl, String className, String parentId,
                                             String parentNode, List<Integer> durationArray, List<Integer> startPointArray,
                                             List<Integer> endPointArray) throws Exception {
        // Get actorName, actorMbox
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        // Specify list of verbs, that should be included in the statements
        List<String> verbs = new ArrayList<String>(Arrays.asList(verbDisplayViewed, verbDisplayPlay, verbDisplayWatched, verbDisplayPaused,
                verbDisplayComplete)); // here a copy of the List is made, so it's possible to remove an item from the List

        // Find a statement that includes a certain verb, verify the statement, remove the verb from the list of specified verbs
        for (int i = 0; i < 5; i++) {
            String currentStatement = statements.get(i);
            String currentVerb = getDisplayVerb(currentStatement);

            if (currentVerb.equals(verbDisplayViewed)) { // viewed
                checkVideoViewedStatement(currentStatement, actorName, actorMbox, videoNodeUrl, videoTitle, classNodeUrl,
                        className, parentId);
                verbs.remove(currentVerb); // remove the verb from 'verbs' list
            } else if (currentVerb.equals(verbDisplayPlay)) { // play
                checkVideoPlayStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched)) { // watched
                checkVideoWatchedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startPointArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused)) { // paused
                checkVideoPausedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayComplete)) {
                checkVideoCompleteStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            }

        }
        int size = verbs.size();
        String number = Integer.toString(size);
        // Check that the statements include all specified verbs, so the result of this method should be 'verbs quantity = 0'
        Assert.assertTrue("Statements are not generated correctly: " + number + " statements are wrong.", size == 0);
    }
    public void checkVideoPausedStatements(List<String> statements, String videoNodeUrl, String url, String videoTitle,
                                           String videoName, String classNodeUrl, String className, String parentId,
                                           String parentNode, List<Integer> durationArray, List<Integer> startPointArray,
                                           List<Integer> pausePointArray, List<Integer> endPointArray) throws Exception {
        // Get actorName, actorMbox
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        // Specify list of verbs, that should be included in the statements
        // (here a copy of the List is made, so it's possible to remove an item from the List)
        List<String> verbs = new ArrayList<String>(Arrays.asList(verbDisplayViewed, verbDisplayPlay, verbDisplayWatched,
                verbDisplayPaused, verbDisplayPlay, verbDisplayWatched, verbDisplayComplete, verbDisplayPaused));

        // Find a statement that includes a certain verb, verify the statement, remove the verb from the list of specified verbs
        for (int i = 0; i < 8; i++) {
            String currentStatement = statements.get(i);
            String currentVerb = getDisplayVerb(currentStatement);
            // if 'verb = play || watched', get start point (seconds)
            int startPoint = 0;
            if (currentVerb.equals(verbDisplayPlay) || currentVerb.equals(verbDisplayWatched)) {
                startPoint = getStartPoint(currentStatement);
            }
            // if 'verb = paused', get end point (seconds)
            int endPoint = 0;
            if (currentVerb.equals(verbDisplayPaused)) {
                endPoint = getEndPoint(currentStatement);
            }

            if (currentVerb.equals(verbDisplayViewed)) { // viewed
                checkVideoViewedStatement(currentStatement, actorName, actorMbox, videoNodeUrl, videoTitle, classNodeUrl,
                        className, parentId);
                verbs.remove(currentVerb); // remove the verb from 'verbs' list
            } else if (currentVerb.equals(verbDisplayPlay) && startPointArray.contains(startPoint)) { // play (before pause)
                checkVideoPlayStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && startPointArray.contains(startPoint)) { // watched (before pause)
                checkVideoWatchedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, startPointArray, pausePointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && pausePointArray.contains(endPoint)) { // paused
                checkVideoPausedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, pausePointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPlay) && pausePointArray.contains(startPoint)) { // play (after pause)
                checkVideoPlayStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, pausePointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayWatched) && pausePointArray.contains(startPoint)) { // watched (after pause)
                checkVideoWatchedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, pausePointArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayComplete)) { // complete
                checkVideoCompleteStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            } else if (currentVerb.equals(verbDisplayPaused) && endPointArray.contains(endPoint)) { // paused (video complete)
                checkVideoPausedStatement(currentStatement, actorName, actorMbox, url, videoName, videoNodeUrl, classNodeUrl,
                        className, parentNode, durationArray, endPointArray);
                verbs.remove(currentVerb);
            }
        }
        int size = verbs.size(); // number of wrong statements
        String number = Integer.toString(size);
        // Check that the statements include all specified verbs, so the result of this method should be 'verbs quantity = 0'
        Assert.assertTrue("Statements are not generated correctly: " + number + " statements are wrong.", size == 0);
    }

    public void initTrackCounter() { // the method initializes counter of ajax calls (portions of calls);
                                     // should be used with waitForTrackCount() method
        String jscript = "window.pilotSeleniumTestTrackCount = 0;";
        ((JavascriptExecutor)webDriver).executeScript(jscript);
        jscript = "window.jQuery(document).ajaxStop(function() { window.pilotSeleniumTestTrackCount++; });";
        ((JavascriptExecutor)webDriver).executeScript(jscript);
    }

    public void waitForTrackCount(final Long expectedCount) { // the method stops ajax calling if expectedCount of portions of calls is reached;
                                                              // the method is used with initTrackCounter() method
        ExpectedCondition< Boolean > trackCountReached = new ExpectedCondition < Boolean > () {
            public Boolean apply(WebDriver driver) {
                String jscript = "return window.pilotSeleniumTestTrackCount;";
                return ((JavascriptExecutor)driver).executeScript(jscript).equals(expectedCount);
            }
        };
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait to get expected count
        try {
            wait.until(trackCountReached);
        } catch (Throwable ajaxError) {
            Assert.assertFalse("Timeout during wait for track count.", true);
        }
    }

    public void waitUntilVimeoVideoLoaded() throws Exception { // inside iframe
                                                          // (the method should be used right after video frame is open
                                                          // to load the video)
        // Click 'pause' button
        clickButtonPauseVimeo();
        // Wait until the video has 100% loaded
        final MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                String style = mediaPopupPage.getProgressBarVimeo().getAttribute("style");
                String percentage = style.split("width: ")[1].split("%")[0];
                return percentage.equals("100");
            }
        });
        // Click 'play' button
        clickButtonPlayVimeo();
    }






}
