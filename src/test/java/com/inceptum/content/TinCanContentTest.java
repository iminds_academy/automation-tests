package com.inceptum.content;

import java.util.Arrays;
import java.util.List;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;
import org.junit.Assert;
import org.junit.Test;

import com.inceptum.pages.CreateImagePage;
import com.inceptum.pages.CreatePresentationPage;
import com.inceptum.pages.EditImagePage;
import com.inceptum.pages.EditInfoPage;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.EditMultipleChoiceQuestionPage;
import com.inceptum.pages.EditPresentationPage;
import com.inceptum.pages.EditQuizPage;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.EditVideoPage;
import com.inceptum.pages.ManageQuestionsQuizPage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.SelectImagePage;
import com.inceptum.pages.SelectPresentationPage;
import com.inceptum.pages.TakeQuizPage;
import com.inceptum.pages.ViewImagePage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.ViewMultipleChoiceQuestionPage;
import com.inceptum.pages.ViewPresentationPage;
import com.inceptum.pages.ViewQuizPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;
import com.inceptum.pages.ViewVideoPage;


public class TinCanContentTest extends TinCanContentBaseTest {


    /*---------CONSTANTS--------*/

    protected final String verbIdQuizStarted = "http://adlnet.gov/expapi/verbs/started";
    protected final String verbIdQuizAnswerQuestion = "http://adlnet.gov/expapi/verbs/answered";
    protected final String verbIdQuizFinished = "http://adlnet.gov/expapi/verbs/finished";
    protected final String verbDisplayQuizStarted = "started";
    protected final String verbDisplayQuizAnswered = "answered";
    protected final String verbDisplayQuizFinished = "finished";
    protected final String downloadsTitlePdf = "File PDF";
    protected final String downloadsTitleDocx = "File DOCX";
    protected final String downloadsTitleXlsx = "File XLSX";
    protected final String downloadsTitlePptx = "File PPTX";
    protected final String youtubeTitle = "YouTubeTitle";
    protected final String youtubeTitle2 = "YouTubeTitle2";
    protected final String vimeoTitle = "VimeoTitle";
    protected final String classType = "http://orw.iminds.be/tincan/content/type/class";
    protected final String questionType = "http://orw.iminds.be/tincan/content/type/multichoice";
    protected final String quizType = "http://orw.iminds.be/tincan/content/type/quiz";
    protected final String quizInteractionType = "http://adlnet.gov/expapi/activities/cmi.interaction";
    protected final String interactionType = "choice";
    protected final String classNameSelected = "Example class";
    protected final String trueValue = "true";
    protected final String falseValue = "false";


     /* ----- Tests ----- */

// 'Viewing nodes' tests

    // Viewing Course materials

    @Test
    public void testViewImage() throws Exception {

        checkTinCanApiPage();
        createImageFromContentPage(image3Path);

        // Get the image name
        ViewImagePage viewImagePage = new ViewImagePage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewImagePage.getFieldTitle());
        String imageName = viewImagePage.getImageTitle();
        // Get the image node url
        openEditPage();
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        waitUntilElementIsVisible(editImagePage.getFieldTitle());

        String imageNodeNumber=editImagePage.getUrlNode();
        String imageNodeUrl=websiteUrl.concat("/node/") + imageNodeNumber;

        // Get Class node url and name
        String className = editImagePage.getClassName();
        String classNode = editImagePage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get text of tincan log message

        String logMessage= getParticularTinCanMessage(verbDisplayViewed, imageNodeUrl);

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, imageNodeUrl, imageName, imageType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
    }

    @Test
    public void testViewPresentation() throws Exception {

        checkTinCanApiPage();
        createPresentationFromContentPage();

        // Get the presentation name
        ViewPresentationPage viewPresentationPage = new ViewPresentationPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewPresentationPage.getFieldTitle());
        String presentationName = viewPresentationPage.getPresentationTitle();
        // Get the presentation node url
        openEditPage();
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPresentationPage.getFieldTitle());

        String presentationNodeNumber=editPresentationPage.getUrlNode();
        String presentationNodeUrl=websiteUrl.concat("/node/") + presentationNodeNumber;
        // Get Class node url and name
        String className = editPresentationPage.getClassName();
        String classNode = editPresentationPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get text of tincan log message
        String logMessage = getParticularTinCanMessage(verbDisplayViewed, presentationNodeUrl);

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, presentationNodeUrl, presentationName, presentationType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
    }

    @Test
    public void testViewVideoYoutube() throws Exception { // youtube video

        checkTinCanApiPage();
        createVideoFromContentPage();

        // Get the Video name
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewVideoPage.getFieldTitle());
        String videoName = viewVideoPage.getVideoTitle();
        // Get the Video node url
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editVideoPage.getFieldTitle());

        String videoNodeNumber=editVideoPage.getUrlNode();

        String videoNodeUrl = websiteUrl.concat("/node/") + videoNodeNumber;
        // Get Class node url and name
        String className = editVideoPage.getClassName();
        String classNode = editVideoPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get text of tincan log message
        String logMessage = getParticularTinCanMessage(verbDisplayViewed, videoNodeUrl); // The first statement has "verb": "play"

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, videoNodeUrl, videoName, videoViewType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
    }

    @Test
    public void testViewVideoVimeo() throws Exception { // vimeo video

        checkTinCanApiPage();
        createVideoWithUrl(vimeoURL, "VimeoTitle");

        // Get the Video name
        ViewVideoPage viewVideoPage = new ViewVideoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewVideoPage.getFieldTitle());
        String videoName = viewVideoPage.getVideoTitle();
        // Get the Video node url
        openEditPage();
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editVideoPage.getFieldTitle());

        String videoNodeNumber=editVideoPage.getUrlNode();

        String videoNodeUrl = websiteUrl.concat("/node/") + videoNodeNumber;

        // Get Class node url and name
        String className = editVideoPage.getClassName();
        String classNode = editVideoPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get text of tincan log message
        String logMessage = getParticularTinCanMessage(verbDisplayViewed, videoNodeUrl); // The first statement has "verb": "play"

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, videoNodeUrl, videoName, videoViewType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
    }

    @Test
    public void testViewClass() throws Exception {

        checkTinCanApiPage();
        openHomePage();

        OverviewPage overviewPage=new OverviewPage(webDriver, pageLocation);
        overviewPage.selectClassOnNavBar();
        waitForPageToLoad();


        // Get the class name
        String className = "Class: " + classNameSelected;
        // Get the class node url
        String classNode = overviewPage.getUrlNodeForClass();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get text of tincan log message
        String logMessage = getParticularTinCanMessage(verbDisplayViewed, classNodeUrl);

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, classNodeUrl, className, classType);
    }


    // Viewing Interactivity

    @Test
    public void testViewMCQuestion() throws Exception {

        checkTinCanApiPage();
        createMCQuestionFromContentPage(alternativesName);

        // Get the question name
        ViewMultipleChoiceQuestionPage viewMultipleChoiceQuestionPage = new ViewMultipleChoiceQuestionPage(webDriver, pageLocation);
        String questionName = viewMultipleChoiceQuestionPage.getQuestionTitle();
        // Get the question node url
        openEditPage();
        EditMultipleChoiceQuestionPage editMultipleChoiceQuestionPage = new EditMultipleChoiceQuestionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editMultipleChoiceQuestionPage.getFieldTitle());

        String mcQuestionNodeNumber=editMultipleChoiceQuestionPage.getUrlNode();

        String questionNodeUrl = websiteUrl.concat("/node/") + mcQuestionNodeNumber;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectMCQuestionViewValues(logMessage, questionNodeUrl, questionName, questionType);
    }

    @Test
    public void testViewQuiz() throws Exception {

        checkTinCanApiPage();
        createQuiz();

        // Get the quiz name
        String quizNameTrimmed = getPageTitle();
        String quizName = "Quiz: " + quizNameTrimmed;
        // Get the quiz node url
        openEditPage();
        EditQuizPage editQuizPage = new EditQuizPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editQuizPage.getFieldTitle());

        String quizNodeNumber=editQuizPage.getUrlNode();

        String quizNodeUrl = websiteUrl.concat("/node/") + quizNodeNumber;


        // Get Class node url and name
        String className = editQuizPage.getClassName();
        String classNode = editQuizPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        editQuizPage.clickButtonSave();
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        // View the Quiz
        ViewQuizPage viewQuizPage = manageQuestionsQuizPage.clickTabView();
        assertElementActiveStatus(viewQuizPage.getTabView());

        // Get text of tincan log message
        String logMessage = getParticularTinCanMessage(verbDisplayViewed, quizNodeUrl);

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(logMessage, quizNodeUrl, quizName, quizType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
    }


    // Viewing Course components

    @Test
    public void testViewLiveSession() throws Exception {

        checkTinCanApiPage();
        createLiveSession();

        // Get the LiveSession name
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String title = editLiveSessionPage.getFieldTitle().getAttribute("value");
        String liveSessionName = "Live session: " + title;
        // Get the LiveSession node url and node ID
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String liveSessionNodeUrl = parts[0]; // Node url
        String[] parts2 = liveSessionNodeUrl.split("node/");
        String liveSessionNodeID = parts2[1]; // Node id

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID and order
        String chapterName = editLiveSessionPage.getChapterName();
        String chapterID =editLiveSessionPage.getChapterID();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String liveSessionType = "http://orw.iminds.be/tincan/content/type/live_session";
        checkObjectViewValues(logMessage, liveSessionNodeUrl, liveSessionName, liveSessionType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'extensions'
        String chapterOrder = getChapterOrder(classNode, title, liveSessionNodeID); // Redirecting to Overview page
        checkExtensionsValues(logMessage, chapterID, chapterName, chapterOrder);
    }

    @Test
    public void testViewTask() throws Exception {

        checkTinCanApiPage();
        createTask();

        // Get the Task name
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTaskPage.getFieldTitle());
        String title = editTaskPage.getFieldTitle().getAttribute("value");
        String taskName = "Task: " + title;
        // Get the Task node url and node ID
        String editTaskUrl = webDriver.getCurrentUrl();
        String[] parts = editTaskUrl.split("/edit"); // Separate needed node URL
        String taskNodeUrl = parts[0]; // Node url
        String[] parts2 = taskNodeUrl.split("node/");
        String taskNodeID = parts2[1]; // Node id

        // Get Class node url and name
        String className = editTaskPage.getClassName();
        String classNode = editTaskPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID and order
        String chapterName = editTaskPage.getChapterName();
        String chapterID =editTaskPage.getChapterID();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String taskType = "http://orw.iminds.be/tincan/content/type/task";
        checkObjectViewValues(logMessage, taskNodeUrl, taskName, taskType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'extensions'
        String chapterOrder = getChapterOrder(classNode, title, taskNodeID); // Redirecting to Overview page
        checkExtensionsValues(logMessage, chapterID, chapterName, chapterOrder);
    }

    @Test
    public void testViewTheory() throws Exception {

        checkTinCanApiPage();
        createTheory();

        // Get the Theory name
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTheoryPage.getFieldTitle());
        String title = editTheoryPage.getFieldTitle().getAttribute("value");
        String theoryName = "Theory: " + title;
        // Get the Theory node url and node ID
        String editTheoryUrl = webDriver.getCurrentUrl();
        String[] parts = editTheoryUrl.split("/edit"); // Separate needed node URL
        String theoryNodeUrl = parts[0]; // Node url
        String[] parts2 = theoryNodeUrl.split("node/");
        String theoryNodeID = parts2[1]; // Node id

        // Get Class node url and name
        String className = editTheoryPage.getClassName();
        String classNode = editTheoryPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID and order
        String chapterName = editTheoryPage.getChapterName();
        String chapterID =editTheoryPage.getChapterID();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String theoryType = "http://orw.iminds.be/tincan/content/type/theory";
        checkObjectViewValues(logMessage, theoryNodeUrl, theoryName, theoryType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'extensions'
        String chapterOrder = getChapterOrder(classNode, title, theoryNodeID); // Redirecting to Overview page
        checkExtensionsValues(logMessage, chapterID, chapterName, chapterOrder);
    }

    @Test
    public void testViewInfoPage() throws Exception {

        checkTinCanApiPage();
        createInfoPage();

        // Get the InfoPage name
        openEditPage();
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editInfoPage.getFieldTitle());
        String title = editInfoPage.getFieldTitle().getAttribute("value");
        String infoPageName = "Info page: " + title;
        // Get the InfoPage node url and node ID
        String editInfoPageUrl = webDriver.getCurrentUrl();
        String[] parts = editInfoPageUrl.split("/edit"); // Separate needed node URL
        String infoPageNodeUrl = parts[0]; // Node url
        String[] parts2 = infoPageNodeUrl.split("node/");
        String infoPageNodeID = parts2[1]; // Node id

        // Get Class node url and name
        String className = editInfoPage.getClassName();
        String classNode = editInfoPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID and order
        String chapterName = editInfoPage.getChapterName();
        String chapterID =editInfoPage.getChapterID();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String infoPageType = "http://orw.iminds.be/tincan/content/type/info";
        checkObjectViewValues(logMessage, infoPageNodeUrl, infoPageName, infoPageType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'extensions'
        String chapterOrder = getChapterOrder(classNode, title, infoPageNodeID); // Redirecting to Overview page
        checkExtensionsValues(logMessage, chapterID, chapterName, chapterOrder);
    }

    @Test
    public void testViewMediaPage() throws Exception {

        checkTinCanApiPage();
        createMediaPage();

        // Get the MediaPage name
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editMediaPage.getFieldTitle());
        String title = editMediaPage.getFieldTitle().getAttribute("value");
        String mediaPageName = "Media page: " + title;
        // Get the MediaPage node url and node ID
        String editMediaPageUrl = webDriver.getCurrentUrl();
        String[] parts = editMediaPageUrl.split("/edit"); // Separate needed node URL
        String mediaPageNodeUrl = parts[0]; // Node url
        String[] parts2 = mediaPageNodeUrl.split("node/");
        String mediaPageNodeID = parts2[1]; // Node id

        // Get Class node url and name
        String className = editMediaPage.getClassName();
        String classNode = editMediaPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID and order
        String chapterName = editMediaPage.getChapterName();
        String chapterID =editMediaPage.getChapterID();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statements
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String mediaPageType = "http://orw.iminds.be/tincan/content/type/rich_media_page";
        checkObjectViewValues(logMessage, mediaPageNodeUrl, mediaPageName, mediaPageType);
        // Key 'context'
        checkGroupingViewValues(logMessage, classNodeUrl, className, objectType);
        // Key 'extensions'
        String chapterOrder = getChapterOrder(classNode, title, mediaPageNodeID); // Redirecting to Overview page
        checkExtensionsValues(logMessage, chapterID, chapterName, chapterOrder);
    }


    // Check 'parent' values with different combinations:

    @Test
    public void testViewImageFromLiveSession() throws Exception { // Image is added by 'add' icon on EditLiveSession page

        checkTinCanApiPage();
        createLiveSession();

        // Add Image with 'add' icon
        openEditPage();
             // Get parentId
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

            // Get Class node url and name
        String className = editLiveSessionPage.getClassName(); // from LiveSession page (not Image page),
        String classNode = editLiveSessionPage.getClassNode(); // so Class of LS and Image should be the same
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        editLiveSessionPage.switchToTabCourseMaterial();
        CreateImagePage createImagePage = editLiveSessionPage.clickIconAddImage();
        createImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertMessage(editLiveSessionPage.getMessage(), "messages status", ".*Image .+ has been created.");

        // Get the image node ID
        String imageNodeId = editLiveSessionPage.getItemNodeId(editLiveSessionPage.getFieldImagesCourseMaterial());

        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");

        // View the Image from Live Session
        MediaPopupPage mediaPopupPage = viewLiveSessionPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the image name
        String imageName = mediaPopupPage.getPopupName();

        // Get the image node url
        String imageNodeUrl = websiteUrl +"/node/" + imageNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewImageStatement(false, logMessage, imageNodeUrl, imageName, classNodeUrl, className, parentId);
    }


    @Test
    public void testViewPresentationFromTheory() throws Exception { // Presentation is added using 'autoselection' from list

        checkTinCanApiPage();
        createPresentationFromContentPage();
        createTheory();

        // Add Presentation using 'autoselect'
        openEditPage();
            // Get parentId
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTheoryPage.getFieldTitle());
        String editTheoryUrl = webDriver.getCurrentUrl();
        String[] parts = editTheoryUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

           // Get Class node url and name
        String className = editTheoryPage.getClassName(); // from Theory page (not Presentation page),
        String classNode = editTheoryPage.getClassNode(); // so Class of Theory and Presentation should be the same
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        editTheoryPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editTheoryPage.getFieldPresentationsCourseMaterial(), presentationTitle);

        // Get the presentation node ID
        String presentationNodeId = editTheoryPage.getItemNodeId(editTheoryPage.getFieldPresentationsCourseMaterial());

        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");

        // View the Presentation from Theory
        viewTheoryPage.clickLinkMaterial();
        viewTheoryPage.assertPopupIsOpened();

        // Get the Presentation name
        String presentationName = viewTheoryPage.getPresentationPopupName();

        // Get the Presentation node url
        String presentationNodeUrl = websiteUrl +"/node/" + presentationNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewPresentationStatement(false, logMessage, presentationNodeUrl, presentationName, classNodeUrl,
                className, parentId);
    }


    // Check 'Additional materials' value in statement

    @Test
    public void testViewImageFromTaskAdditionalMaterials() throws Exception { // Image is added to Additional materials of Task

        checkTinCanApiPage();
        createTask();

        // Add Image with 'add' icon
        openEditPage();
            // Get parentId
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTaskPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String[] parts = editTaskUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

            // Get Class node url and name
        String className = editTaskPage.getClassName(); // from Task page (not Image page),
        String classNode = editTaskPage.getClassNode(); // so Class of Task and Image should be the same
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        editTaskPage.switchToTabAdditionalResources();
        CreateImagePage createImagePage = editTaskPage.clickIconAddImageAdditional();
        createImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image4Path);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertMessage(editTaskPage.getMessage(), "messages status", ".*Image .+ has been created.");

        // Get the image node ID
        String imageNodeId = editTaskPage.getItemNodeId(editTaskPage.getFieldImagesAdditionalResources());

        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");

        // View the Image from Task
        MediaPopupPage mediaPopupPage = viewTaskPage.clickLinkAdditionalMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the image name
        String imageName = mediaPopupPage.getPopupName();

        // Get the image node url
        String imageNodeUrl = websiteUrl +"/node/" + imageNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewImageStatement(additional, logMessage, imageNodeUrl, imageName, classNodeUrl, className, parentId);
    }

    @Test
    public void testViewPresentationFromMediaPageAdditionalMaterials() throws Exception { // Presentation is added to Additional materials of MediaPage

        checkTinCanApiPage();
        createMediaPage();

        // Add Presentation with 'add' icon
        openEditPage();
            // Get parentId
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editMediaPage.getFieldTitle()); // check that Edit page is already opened
        String editMediaPageUrl = webDriver.getCurrentUrl();
        String[] parts = editMediaPageUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

            // Get Class node url and name
        String className = editMediaPage.getClassName(); // from Media page (not Presentation page),
        String classNode = editMediaPage.getClassNode(); // so Class of MediaPage and Presentation should be the same
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        editMediaPage.switchToTabAdditionalMaterial();
        CreatePresentationPage createPresentationPage = editMediaPage.clickIconAddPresentation();
        createPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        assertMessage(editMediaPage.getMessage(), "messages status", ".*Presentation .+ has been created.");

        // Get the presentation node ID
        String presentationNodeId = editMediaPage.getItemNodeId(editMediaPage.getFieldPresentationsAdditionalMaterial());

        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        assertMessage(viewMediaPage.getMessage(), "messages messages--status", ".*Media page .+ has been updated.");
        // Check that 'Links' link is shown and enabled on ViewMedia page
        Assert.assertTrue("'Links' link isn't enabled on ViewMedia page.", viewMediaPage.getLinkInfo().isEnabled());

        // View the Presentation from Media page
        viewMediaPage.clickLinkAdditionalMaterial();
        viewMediaPage.assertPresentationIsOpened();
        waitUntilElementIsVisible(viewMediaPage.getIframePopup());// this wait() is required here, because if the iframe is not shown
                                                                  // - the statement is not generated
        // Get the presentation name
        String presentationName = viewMediaPage.getPresentationPopupName();

        // Get the presentation node url
        String presentationNodeUrl = websiteUrl +"/node/" + presentationNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewPresentationStatement(additional, logMessage, presentationNodeUrl, presentationName, classNodeUrl,
                className, parentId);
    }


// Check statements for downloading material

    @Test
    public void testDownloadedCourseMaterialFromLiveSession() throws Exception { // Check downloading .pdf file from EditLiveSession page/CourseMaterial tab

        checkTinCanApiPage();
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Upload .pdf file
        editLiveSessionPage.switchToTabCourseMaterial();
        editLiveSessionPage.fillFieldDownloadsTitle(downloadsTitlePdf);
        editLiveSessionPage.uploadFileToDownloads(pdfFilePath);
        editLiveSessionPage.clickButtonSave();

        // Get ID of downloaded file
        ViewLiveSessionPage viewLiveSessionPage = new ViewLiveSessionPage(webDriver, pageLocation);
        String downloadsId = viewLiveSessionPage.getDownloadsIdCourseMaterial();

        // View downloaded file
        viewLiveSessionPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewFileDownloadsStatement(false, logMessage, downloadsId, downloadsTitlePdf, classNodeUrl,
                className, parentId);
    }


    @Test // PILOT-978
    public void testDownloadedAdditionalMaterialFromTheory() throws Exception { // Check downloading .docx file from EditTheory page/AdditionalMaterial tab

        checkTinCanApiPage();
        createTheory();

        // Get the Theory node url (parentId)
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTheoryPage.getFieldTitle());
        String editTheoryUrl = webDriver.getCurrentUrl();
        String[] parts = editTheoryUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

        // Get Class node url and name
        String className = editTheoryPage.getClassName();
        String classNode = editTheoryPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Upload .docx file
        editTheoryPage.switchToTabAdditionalMaterial();
        editTheoryPage.fillFieldDownloadsTitle(downloadsTitleDocx);
        editTheoryPage.uploadFileToDownloads(docxFilePath);
        editTheoryPage.clickButtonSave();

        // Get ID of downloaded file
        ViewTheoryPage viewTheoryPage = new ViewTheoryPage(webDriver, pageLocation);
        String downloadsId = viewTheoryPage.getDownloadsIdAdditionalMaterial();

        // View downloaded file (just click the link)
        viewTheoryPage.clickLinkDownloadedAdditionalMaterial();
        waitUntilFileDownloaded();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewFileDownloadsStatement(additional, logMessage, downloadsId, downloadsTitleDocx, classNodeUrl,
                className, parentId); // Fails here: no such key (PILOT-978) !!!!!!!!!!!!!!!!!!!!!!!!!
    }

    @Test // PILOT-978
    public void testDownloadedAdditionalResourceFromTask() throws Exception { // Check downloading .xlsx file from EditTask page/AdditionalResources tab

        checkTinCanApiPage();
        createTask();

        // Get the Task node url (parentId)
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTaskPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String[] parts = editTaskUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

        // Get Class node url and name
        String className = editTaskPage.getClassName();
        String classNode = editTaskPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Upload .xlsx file
        editTaskPage.switchToTabAdditionalResources();
        editTaskPage.fillFieldDownloadsTitle(downloadsTitleXlsx);
        editTaskPage.uploadFileToDownloads(xlsxFilePath);
        editTaskPage.clickButtonSave();

        // Get ID of downloaded file
        ViewTaskPage viewTaskPage = new ViewTaskPage(webDriver, pageLocation);
        String downloadsId = viewTaskPage.getDownloadsIdAdditionalMaterial();

        // View downloaded file (just click the link)
        viewTaskPage.clickLinkDownloadedAdditionalMaterial();
        waitUntilFileDownloaded();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewFileDownloadsStatement(additional, logMessage, downloadsId, downloadsTitleXlsx, classNodeUrl,
                className, parentId); // Fails here: no such key (PILOT-978) !!!!!!!!!!!!!!!!!!!!!!!!!
    }

    @Test
    public void testDownloadedCourseMaterialFromMediaPage() throws Exception { // Check downloading .pptx file from EditMedia page/CourseMaterial tab

        checkTinCanApiPage();
        createMediaPage();

        // Get the MediaPage node url (parentId)
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editMediaPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String[] parts = editTaskUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

        // Get Class node url and name
        String className = editMediaPage.getClassName();
        String classNode = editMediaPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Upload .pptx file
        editMediaPage.switchToTabCourseMaterial();
        editMediaPage.fillFieldDownloadsTitle(downloadsTitlePptx);
        editMediaPage.uploadFileToDownloads(pptxFilePath);
        editMediaPage.clickButtonSave();

        // Get ID of downloaded file
        ViewMediaPage viewMediaPage = new ViewMediaPage(webDriver, pageLocation);
        String downloadsId = viewMediaPage.getDownloadsIdCourseMaterial();

        // View downloaded file (just click the link)
        viewMediaPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewFileDownloadsStatement(false, logMessage, downloadsId, downloadsTitlePptx, classNodeUrl,
                className, parentId);
    }


// Check statements for links

    @Test
    public void testVisitLinkCourseMaterialFromLiveSession() throws Exception { // Check visiting link in new window from EditLiveSessionPage/CourseMaterial tab

        checkTinCanApiPage();
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a link to CourseMaterial tab
        editLiveSessionPage.switchToTabCourseMaterial();
        editLiveSessionPage.fillFieldLinksTitle(linkTitle);
        editLiveSessionPage.fillFieldLinksUrl(linkUrl);
        editLiveSessionPage.selectCheckboxOpenInNewWindow();
        editLiveSessionPage.clickButtonSave();

        // View link in new tab
        ViewLiveSessionPage viewLiveSessionPage = new ViewLiveSessionPage(webDriver, pageLocation);
        viewMaterialInAnotherBrowserTab(viewLiveSessionPage.getLinkExternalCourseMaterial());

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkVisitLinkStatement(false, logMessage, linkUrl, linkTitle, classNodeUrl, className, parentId);
    }

    @Test // PILOT-978
    public void testVisitLinkAdditionalMaterialFromMediaPage() throws Exception { // Check visiting link (the same browser tab) from EditMediaPage/CourseMaterial tab

        checkTinCanApiPage();
        createMediaPage();

        // Get the MediaPage node url (parentId)
        openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editMediaPage.getFieldTitle());
        String editMediaPageUrl = webDriver.getCurrentUrl();
        String[] parts = editMediaPageUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url

        // Get Class node url and name
        String className = editMediaPage.getClassName();
        String classNode = editMediaPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a link to AdditionalMaterial tab (leave checkbox 'Open URL in a New Window' unselected)
        editMediaPage.switchToTabAdditionalMaterial();
        editMediaPage.fillFieldLinksTitleAdditional(linkTitle);
        editMediaPage.fillFieldLinksUrlAdditional(linkUrl);
        editMediaPage.clickButtonSave();

        // View link at the same browser tab
        ViewMediaPage viewMediaPage = new ViewMediaPage(webDriver, pageLocation);
        viewMediaPage.visitLinkExternalAdditionalMaterial();

        waitForPageToLoad(); // wait till new page is loaded

        // Get text of tincan log message
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkVisitLinkStatement(additional, logMessage, linkUrl, linkTitle, classNodeUrl, className, parentId); // Fails here: no such key (PILOT-978) !!!!!!!!!!!!!!!!!!!!!!!!!
    }


// Check statements for Video interaction

    // Youtube media (mouseOverElement() method  doesn't work for !!! Theory !!! content type when try to click Pause button)

    @Test
    public void testWatchYoutubeVideoCompleted() throws Exception {

        checkTinCanApiPage();

        // Create youtube video and get its duration
        createVideoWithUrl(youtubeURL, youtubeTitle);

        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoYoutubeName();

        // Create Live session
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to CourseMaterial tab
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), youtubeTitle);

        // Get the video node ID
        String videoNodeId = editLiveSessionPage.getItemNodeId(editLiveSessionPage.getFieldVideosCourseMaterial());

        // Save changes and get url of View Live session page
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        String viewLiveSessionUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewLiveSession page and watch the video till the end
        webDriver.get(viewLiveSessionUrl);
        Thread.sleep(1000); // wait for generating 'view LiveSession' statement first
        MediaPopupPage mediaPopupPage = viewLiveSessionPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;
        // Wait until video is completed
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(2L); // check that two portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeYoutubeVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration.",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();

        // Check tincan statements
        checkVideoCompleteStatements(statements, videoNodeUrl, youtubeURL, videoTitle, videoName, classNodeUrl,
                className, parentId, parentNode, durationArray, startPointArray, endPointArray);
    }

    @Test
    public void testWatchYoutubeVideoPaused() throws Exception {

        checkTinCanApiPage();

        // Create youtube video and get its duration
        createVideoWithUrl(youtubeURL, youtubeTitle);

        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set pause point array
        List<Integer> pausePointArray = Arrays.asList(duration/2, (duration/2) + 1);
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoYoutubeName();

        // Create Task
        createTask();

        // Get the Task node url (parentId)
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTaskPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String[] parts = editTaskUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editTaskPage.getClassName();
        String classNode = editTaskPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to TaskResources tab
        editTaskPage.switchToTabTaskResources();
        autoSelectItemFromList(editTaskPage.getFieldVideosTaskResorses(), youtubeTitle);

        // Get the video node ID
        String videoNodeId = editTaskPage.getItemNodeId(editTaskPage.getFieldVideosTaskResorses());

        // Save changes and get url of View Task page
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        String viewTaskUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewTask -> media popup
        webDriver.get(viewTaskUrl);
        Thread.sleep(1000); // wait for generating 'view Task' statement first
        MediaPopupPage mediaPopupPage = viewTaskPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Pause in the middle of the video
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        Thread.sleep(duration*1000/2);
        clickButtonPauseYoutube();
        clickButtonPlayYoutube();
        // Wait until video is completed
        waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(4L); // check that 4 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeYoutubeVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration.",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoPausedStatements(statements, videoNodeUrl, youtubeURL, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, pausePointArray, endPointArray);
    }


    @Test // PILOT-1342 !!!!!!!!
    public void testWatchYoutubeVideoSkipped() throws Exception {

        checkTinCanApiPage();

        // Create youtube video and get its duration
        createVideoWithUrl(youtubeURL2, youtubeTitle2); // 15 sec video
        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(4, 5);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(10, 11); // = 2/3 of whole video width
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoYoutubeName();


        // Create LiveSession
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to CourseMaterial tab
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), youtubeTitle2);

        // Get the video node ID
        String videoNodeId = editLiveSessionPage.getItemNodeId(editLiveSessionPage.getFieldVideosCourseMaterial());

        // Save changes and get url of ViewLiveSession page
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        String viewLiveSessionUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewLiveSession page -> media popup
        webDriver.get(viewLiveSessionUrl);
        Thread.sleep(1000); // wait for generating 'view LiveSession' statement first
        MediaPopupPage mediaPopupPage = viewLiveSessionPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Watch the video 4 sec
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        waitUntilYoutubeVideoPlaysSec(4); // wait 4 sec
        clickButtonPauseYoutube(); // pause
        // Skip a part of video
        skipPartOfYoutubeVideo(); // skipping end point == 2/3 of whole video width
        clickButtonPlayYoutube(); // play
        // Wait until video is completed
        waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(5L); // check that 5 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeYoutubeVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration (" + duration + ").",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoSkippedStatements(statements, videoNodeUrl, youtubeURL2, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }


    @Test // PILOT-1342 !!!!!!!!
    public void testWatchYoutubeVideoSkippedWithoutPause() throws Exception {

        checkTinCanApiPage();

        // Create youtube video and get its duration
        createVideoWithUrl(youtubeURL2, youtubeTitle2);
        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(4, 5);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(10, 11); // = 2/3 of whole video width
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoYoutubeName();


        // Create LiveSession
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to CourseMaterial tab
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), youtubeTitle2);

        // Get the video node ID
        String videoNodeId = editLiveSessionPage.getItemNodeId(editLiveSessionPage.getFieldVideosCourseMaterial());

        // Save changes and get url of ViewLiveSession page
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        String viewLiveSessionUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewLiveSession page -> media popup
        webDriver.get(viewLiveSessionUrl);
        Thread.sleep(1000); // wait for generating 'view LiveSession' statement first
        MediaPopupPage mediaPopupPage = viewLiveSessionPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Watch the video 4 sec
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        waitUntilYoutubeVideoPlaysSec(4); // wait 4 sec
        // Skip a part of video without pausing
        skipPartOfYoutubeVideo(); // skipping end point == 2/3 of whole video width
        // Wait until video is completed
        waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(3L); // check that 3 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeYoutubeVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration (" + duration + ").",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoSkippedStatements(statements, videoNodeUrl, youtubeURL2, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }


    // Vimeo media

    @Test
    public void testWatchVimeoVideoCompleted() throws Exception {

        checkTinCanApiPage();

        // Create Vimeo video and get its duration
        createVideoWithUrl(vimeoURL, vimeoTitle);
        int duration = getDurationVimeoVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoVimeoName();


        // Create Live session
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to CourseMaterial tab
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), vimeoTitle);

        // Get the video node ID
        String videoNodeId = editLiveSessionPage.getItemNodeId(editLiveSessionPage.getFieldVideosCourseMaterial());

        // Save changes and get url of View Live session page
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        String viewLiveSessionUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewLiveSession page and watch the video till the end
        webDriver.get(viewLiveSessionUrl);
        Thread.sleep(1000); // wait for generating 'view LiveSession' statement first
        MediaPopupPage mediaPopupPage = viewLiveSessionPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;
        // Wait until video is completed
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        initTrackCounter();
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(2L); // check that 2 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeVimeoVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration.",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();

        // Check tincan statements
        checkVideoCompleteStatements(statements, videoNodeUrl, vimeoURL, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, endPointArray);
    }


    @Test
    public void testWatchVimeoVideoPaused() throws Exception {

        checkTinCanApiPage();

        // Create vimeo video and get its duration
        createVideoWithUrl(vimeoURL, vimeoTitle);
        int duration = getDurationVimeoVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set pause point array
        List<Integer> pausePointArray = Arrays.asList(duration/2, (duration/2) + 1);
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoVimeoName();

        // Create Task
        createTask();

        // Get the Task node url (parentId)
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTaskPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String[] parts = editTaskUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editTaskPage.getClassName();
        String classNode = editTaskPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to TaskResources tab
        editTaskPage.switchToTabTaskResources();
        autoSelectItemFromList(editTaskPage.getFieldVideosTaskResorses(), vimeoTitle);

        // Get the video node ID
        String videoNodeId = editTaskPage.getItemNodeId(editTaskPage.getFieldVideosTaskResorses());

        // Save changes and get url of View Task page
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertMessage(viewTaskPage.getMessage(), "messages messages--status", ".*Task .+ has been updated.");
        String viewTaskUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewTask page -> media popup
        webDriver.get(viewTaskUrl);
        Thread.sleep(1000); // wait for generating 'view Task' statement first
        MediaPopupPage mediaPopupPage = viewTaskPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Pause in the middle of the video
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        Thread.sleep(duration*1000/2);
        clickButtonPauseVimeo();
        clickButtonPlayVimeo();
        // Wait until video is completed
        waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(4L); // check that 4 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeVimeoVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration.",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoPausedStatements(statements, videoNodeUrl, vimeoURL, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, pausePointArray, endPointArray);
    }


    @Test // PILOT-1342 !!!!!!!!
    public void testWatchVimeoVideoSkipped() throws Exception {

        checkTinCanApiPage();

        // Create vimeo video and get its duration
        createVideoWithUrl(vimeoURL, vimeoTitle);
        int duration = getDurationVimeoVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(2, 3);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(6, 7); // these values depends on duration (70%)
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoVimeoName();


        // Create Theory
        createTheory();

        // Get the Theory node url (parentId)
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editTheoryPage.getFieldTitle());
        String editTheoryUrl = webDriver.getCurrentUrl();
        String[] parts = editTheoryUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editTheoryPage.getClassName();
        String classNode = editTheoryPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to CourseMaterial tab
        editTheoryPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editTheoryPage.getFieldVideosCourseMaterial(), vimeoTitle);

        // Get the video node ID
        String videoNodeId = editTheoryPage.getItemNodeId(editTheoryPage.getFieldVideosCourseMaterial());

        // Save changes and get url of ViewTheory page
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertMessage(viewTheoryPage.getMessage(), "messages messages--status", ".*Theory .+ has been updated.");
        String viewTheoryUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewTheory page -> media popup
        webDriver.get(viewTheoryUrl);
        Thread.sleep(1000); // wait for generating 'view Theory' statement first
        MediaPopupPage mediaPopupPage = viewTheoryPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Watch the video 2 sec
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        waitUntilVimeoVideoPlaysSec(2); // wait 2 sec
        clickButtonPauseVimeo(); // pause
        // Skip a part of video (move to the end point = 70% of video width)
        skipPartOfVimeoVideo(70); // the %value
        clickButtonPlayVimeo(); // play
        // Wait until video is completed
        waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(5L); // check that 5 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeVimeoVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration (" + duration + ").",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoSkippedStatements(statements, videoNodeUrl, vimeoURL, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray); // !!!
    }


    @Test // PILOT-1342 !!!!!!!!
    public void testWatchVimeoVideoSkippedWithoutPause() throws Exception {

        checkTinCanApiPage();

        // Create vimeo video and get its duration
        createVideoWithUrl(vimeoURL, vimeoTitle);
        int duration = getDurationVimeoVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(2, 3);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(6, 7); // these values depends on duration (70%)
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Get video name
        String videoName = getVideoVimeoName();


        // Create LiveSession
        createLiveSession();

        // Get the LiveSession node url (parentId)
        openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editLiveSessionPage.getFieldTitle());
        String editLiveSessionUrl = webDriver.getCurrentUrl();
        String[] parts = editLiveSessionUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url and name
        String className = editLiveSessionPage.getClassName();
        String classNode = editLiveSessionPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add the video to CourseMaterial tab
        editLiveSessionPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editLiveSessionPage.getFieldVideosCourseMaterial(), vimeoTitle);

        // Get the video node ID
        String videoNodeId = editLiveSessionPage.getItemNodeId(editLiveSessionPage.getFieldVideosCourseMaterial());

        // Save changes and get url of ViewLiveSession page
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertMessage(viewLiveSessionPage.getMessage(), "messages messages--status", ".*Live session .+ has been updated.");
        String viewLiveSessionUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // Open ViewLiveSession page -> media popup
        webDriver.get(viewLiveSessionUrl);
        Thread.sleep(1000); // wait for generating 'view LiveSession' statement first
        MediaPopupPage mediaPopupPage = viewLiveSessionPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the Video title
        String videoTitle = mediaPopupPage.getPopupName();

        // Get the Video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Watch the video 2 sec
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        initTrackCounter(); // initialize ajax calls counter
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        waitUntilVimeoVideoPlaysSec(3); // wait 3 sec
        // Skip a part of video (end point = 70% of video width) without pausing
        skipPartOfVimeoVideo(70);
        // Wait until video is completed
        waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        waitForTrackCount(3L); // check that 3 portions of ajax calls are received

        // Get current (end) time and check it's equal to duration value
        int currentTime = getCurrentTimeVimeoVideo();
        Assert.assertTrue("Current time (" + currentTime + ") is not equal to duration (" + duration + ").",
                durationArray.contains(currentTime));

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoSkippedStatements(statements, videoNodeUrl, vimeoURL, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }



// Check statements for Quiz interaction


    @Test
    public void testQuizInteractionCorrectAnswers() throws Exception {

        checkTinCanApiPage();

        // Create three questions and get their node url
        // The 1st question
        createMCQuestionWithData(question1, alternativesName, 1);
        String question1NodeUrl = getQuestionNodeUrl();
        // The 2nd question
        createMCQuestionWithData(question2, alternativesAge, 2);
        String question2NodeUrl = getQuestionNodeUrl();
        // The 3d question
        createMCQuestionWithData(question3, alternativesCountry, 3);
        String question3NodeUrl = getQuestionNodeUrl();

        // Create a Quiz
        createQuiz();
        // Add the questions to the Quiz
        addQuestionsToQuiz(question1,question2, question3);
        // Open View page
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        ViewQuizPage viewQuizPage = manageQuestionsQuizPage.clickTabView();
        assertElementActiveStatus(viewQuizPage.getTabView());
        String quizUrl = webDriver.getCurrentUrl(); // remember current url
        // Get quiz node url
        String quizNodeUrl = getQuizNodeUrl();

        clearLogMessages();

        // Open ViewQuiz page and start the Quiz
        webDriver.get(quizUrl);
        TakeQuizPage takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        assertElementActiveStatus(takeQuizPage.getTabTake());
        // Check that correct question is shown, select the 1st answer (right)
        takeQuizPage.checkQuestionAndItsOrderNumber(question1, 1);
        takeQuizPage.selectVariant(1);

        // Switch to the next question, select the 2nd answer (right)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question2, 2);
        takeQuizPage.selectVariant(2);

        // Switch to the next question, select the 3d answer (right)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question3, 3);
        takeQuizPage.selectVariant(3);

        // Finish the Quiz
        finishQuiz();

        openRecentLogMessagesPage();
        // Get list of tincan statements for Quiz interaction
        List<String> statements = getListOfStatements();


        // Check tincan statements

        // Check "started" statement

        String logMessage = statements.get(4);
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizStarted, verbDisplayQuizStarted);
        // Key 'object'
        checkObjectQuizValues(logMessage, quizNodeUrl, quizTitle);

        // Check "answer question" statement (the 1st question)

        logMessage = statements.get(3);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question1NodeUrl, question1, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesName);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 1); // extensions
        //Key 'result'
        checkResultValue(logMessage, trueValue);

        // Check "answer question" statement (the 2d question)

        logMessage = statements.get(2);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question2NodeUrl, question2, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesAge);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 2); // extensions
        //Key 'result'
        checkResultValue(logMessage, trueValue);

        // Check "answer question" statement (the 3d question)

        logMessage = statements.get(1);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question3NodeUrl, question3, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesCountry);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 3); // extensions
        //Key 'result'
        checkResultValue(logMessage, trueValue);


        // Check "finish quiz" statement

        logMessage = statements.get(0);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizFinished, verbDisplayQuizFinished);
        // Key 'object'
        checkObjectQuizValues(logMessage, quizNodeUrl, quizTitle);
        // Key 'result'
        checkResultQuizValues(logMessage, 1, trueValue, trueValue);
        // Key 'context'
        int duration = getDuration(quizUrl); // actual duration
        checkContextQuizValue(logMessage, duration);
    }


    @Test
    public void testQuizInteractionWrongAnswers() throws Exception {

        checkTinCanApiPage();

        // Create three questions and get their node url
        // The 1st question
        createMCQuestionWithData(question1, alternativesName, 1);
        String question1NodeUrl = getQuestionNodeUrl();
        // The 2nd question
        createMCQuestionWithData(question2, alternativesAge, 2);
        String question2NodeUrl = getQuestionNodeUrl();
        // The 3d question
        createMCQuestionWithData(question3, alternativesCountry, 3);
        String question3NodeUrl = getQuestionNodeUrl();

        // Create a Quiz
        createQuiz();
        // Add the questions to the Quiz
        addQuestionsToQuiz(question1,question2, question3);
        // Open View page
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        ViewQuizPage viewQuizPage = manageQuestionsQuizPage.clickTabView();
        assertElementActiveStatus(viewQuizPage.getTabView());
        String quizUrl = webDriver.getCurrentUrl(); // remember current url
        // Get quiz node url
        String quizNodeUrl = getQuizNodeUrl();

        clearLogMessages();

        // Open ViewQuiz page and start the Quiz
        webDriver.get(quizUrl);
        TakeQuizPage takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        assertElementActiveStatus(takeQuizPage.getTabTake());
        // Check that correct question is shown, select the 2nd answer (wrong)
        takeQuizPage.checkQuestionAndItsOrderNumber(question1, 1);
        takeQuizPage.selectVariant(2);

        // Switch to the next question, select the 3d answer (wrong)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question2, 2);
        takeQuizPage.selectVariant(3);

        // Switch to the next question, select the 1st answer (wrong)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question3, 3);
        takeQuizPage.selectVariant(1);

        // Finish the Quiz
        finishQuiz();


        openRecentLogMessagesPage();
        // Get list of tincan statements for Quiz interaction
        List<String> statements = getListOfStatements();


        // Check tincan statements

        // Check "started" statement

        String logMessage = statements.get(4);
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizStarted, verbDisplayQuizStarted);
        // Key 'object'
        checkObjectQuizValues(logMessage, quizNodeUrl, quizTitle);

        // Check "answer question" statement (the 1st question)

        logMessage = statements.get(3);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question1NodeUrl, question1, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesName);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 1); // extensions
        // Key 'result'
        checkResultValue(logMessage, falseValue);

        // Check "answer question" statement (the 2d question)

        logMessage = statements.get(2);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question2NodeUrl, question2, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesAge);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 2); // extensions
        // Key 'result'
        checkResultValue(logMessage, falseValue);

        // Check "answer question" statement (the 3d question)

        logMessage = statements.get(1);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question3NodeUrl, question3, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesCountry);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 3); // extensions
        // Key 'result'
        checkResultValue(logMessage, falseValue);


        // Check "finish quiz" statement

        logMessage = statements.get(0);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizFinished, verbDisplayQuizFinished);
        // Key 'object'
        checkObjectQuizValues(logMessage, quizNodeUrl, quizTitle);
        // Key 'result'
        checkResultQuizValues(logMessage, 0, trueValue, falseValue);
        // Key 'context'
        int duration = getDuration(quizUrl); // actual duration
        checkContextQuizValue(logMessage, duration);
    }


    @Test
    public void testQuizInteractionCheckPassRate() throws Exception {

        checkTinCanApiPage();

        // Create three questions and get their node url
        // The 1st question
        createMCQuestionWithData(question1, alternativesName, 1);
        String question1NodeUrl = getQuestionNodeUrl();
        // The 2nd question
        createMCQuestionWithData(question2, alternativesAge, 2);
        String question2NodeUrl = getQuestionNodeUrl();
        // The 3d question
        createMCQuestionWithData(question3, alternativesCountry, 3);
        String question3NodeUrl = getQuestionNodeUrl();

        // Create a Quiz
        createQuiz();
        // Check if pass rate is as expected
        int passRateExpected = 75;
        checkPassRate(passRateExpected);

        // Add the questions to the Quiz
        addQuestionsToQuiz(question1,question2, question3);
        // Open View page
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        ViewQuizPage viewQuizPage = manageQuestionsQuizPage.clickTabView();
        assertElementActiveStatus(viewQuizPage.getTabView());
        String quizUrl = webDriver.getCurrentUrl(); // remember current url
        // Get quiz node url
        String quizNodeUrl = getQuizNodeUrl();

        clearLogMessages();

        // Open ViewQuiz page and start the Quiz
        webDriver.get(quizUrl);
        TakeQuizPage takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        assertElementActiveStatus(takeQuizPage.getTabTake());
        // Check that correct question is shown, select the 2nd answer (right)
        takeQuizPage.checkQuestionAndItsOrderNumber(question1, 1);
        takeQuizPage.selectVariant(1);

        // Switch to the next question, select the 3d answer (right)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question2, 2);
        takeQuizPage.selectVariant(2);

        // Switch to the next question, select the 1st answer (wrong)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question3, 3);
        takeQuizPage.selectVariant(1);

        // Finish the Quiz
        finishQuiz();


        openRecentLogMessagesPage();
        // Get list of tincan statements for Quiz interaction
        List<String> statements = getListOfStatements();


        // Check tincan statements

        // Check "started" statement

        String logMessage = statements.get(4);
        // Key 'actor'
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizStarted, verbDisplayQuizStarted);
        // Key 'object'
        checkObjectQuizValues(logMessage, quizNodeUrl, quizTitle);

        // Check "answer question" statement (the 1st question)

        logMessage = statements.get(3);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question1NodeUrl, question1, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesName);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 1); // extensions
        // Key 'result'
        checkResultValue(logMessage, trueValue);

        // Check "answer question" statement (the 2d question)

        logMessage = statements.get(2);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question2NodeUrl, question2, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesAge);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 2); // extensions
        // Key 'result'
        checkResultValue(logMessage, trueValue);

        // Check "answer question" statement (the 3d question)

        logMessage = statements.get(1);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizAnswerQuestion, verbDisplayQuizAnswered);
        // Key 'object'
        checkObjectAnswerQuestionValues(logMessage, question3NodeUrl, question3, quizInteractionType, interactionType);
        // Check alternatives values 'description'
        checkAlternatives(logMessage, alternativesCountry);
        // Key 'context'
        checkParentValues(logMessage, quizNodeUrl, objectType); // parent values
        checkExtensionsOrder(logMessage, 3); // extensions
        // Key 'result'
        checkResultValue(logMessage, falseValue);

        // Check "finish quiz" statement

        logMessage = statements.get(0);
        // Key 'actor'
        checkActorValues(logMessage, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(logMessage, verbIdQuizFinished, verbDisplayQuizFinished);
        // Key 'object'
        checkObjectQuizValues(logMessage, quizNodeUrl, quizTitle);
        // Key 'result'
        int passRateActual = getPassRateActual(quizUrl); // get pass rate from 'My results' page
        boolean success = quizPassed(passRateActual,passRateExpected); // check if the quiz passed or failed

        Double score = (double)passRateActual/100; // get decimal score from actual result
        checkResultQuizValuesPassRate(logMessage, score, trueValue, success);
        // Key 'context'
        int duration = getDuration(quizUrl); // actual duration
        checkContextQuizValue(logMessage, duration);
    }



}



