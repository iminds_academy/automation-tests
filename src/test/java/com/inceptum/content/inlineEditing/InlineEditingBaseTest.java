package com.inceptum.content.inlineEditing;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.AddContentPage;
import com.inceptum.pages.BasePage;
import com.inceptum.pages.DownloadsPopupInlineEditingPage;
import com.inceptum.pages.InlineEditLiveSessionPage;
import com.inceptum.pages.InlineEditMediaPage;
import com.inceptum.pages.InlineEditTaskPage;
import com.inceptum.pages.InlineEditTheoryPage;
import com.inceptum.pages.LinksPopupInlineEditingPage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;

/**
 * Created by Olga on 28.04.2015.
 */
public class InlineEditingBaseTest extends CourseBaseTest {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final String imageType = "images";
    protected final String image = "image";
    protected final String videoType = "videos";
    protected final String video = "video";
    protected final String presentationType = "presentations";
    protected final String presentation = "presentation";
    protected final String linkType = "links";
    protected final String downloadsType = "downloads";
    protected final int hours = 4;
    protected final int minutes = 45;


     /* ----- METHODS ----- */

    public void createLiveSessionTemplate() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewLiveSessionPage viewLiveSessionPage = addContentPage.clickLinkLiveSession();
        assertTextOfMessage(viewLiveSessionPage.getMessage(), ".*Live session live_session temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewLiveSessionPage.getLinkEdit().isDisplayed());
    }

    public void createTaskTemplate() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewTaskPage viewTaskPage = addContentPage.clickLinkTask();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task task temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTaskPage.getLinkEdit().isDisplayed());
    }

    public void createTheoryTemplate() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewTheoryPage viewTheoryPage = addContentPage.clickLinkTheory();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory theory temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewTheoryPage.getLinkEdit().isDisplayed());
    }

    public void createMediaPageTemplate() throws Exception {
        webDriver.get(websiteUrl.concat("/node/add"));
        waitForPageToLoad();
        AddContentPage addContentPage = new AddContentPage(webDriver, pageLocation);
        ViewMediaPage viewMediaPage = addContentPage.clickLinkMediaPage();
        assertTextOfMessage(viewMediaPage.getMessage(), ".*Media page rich_media_page temp content has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewMediaPage.getLinkEdit().isDisplayed());
    }

    public void clickEditPage() throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Edit page")));
        WebElement linkEditPage = webDriver.findElement(By.partialLinkText("Edit page"));
        waitUntilElementIsClickable(linkEditPage);
        String script = "arguments[0].click();";
        ((JavascriptExecutor) webDriver).executeScript(script, linkEditPage);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.partialLinkText("Advanced edit")));
    }

    public void selectLiveSessionType(String type) throws Exception {
        // Click 'edit' icon for LiveSessionType
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickFieldLiveSessionType();
        // Select needed type
        selectItemFromListByOptionName(inlineEditLiveSessionPage.getFieldLiveSessionTypePopup(), type);
        closePopup();
    }
    public void selectLengthTheory(int hours, int minutes) throws Exception {
        // Click 'edit' icon for length field
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickFieldLength();
        // Select needed hours
        String hoursString = Integer.toString(hours);
        selectItemFromListByOptionName(inlineEditTheoryPage.getFieldLengthHoursPopup(), hoursString);
        String minutesString = Integer.toString(minutes);
        selectItemFromListByOptionName(inlineEditTheoryPage.getFieldLengthMinutesPopup(), minutesString);
        closePopup();
    }
    public void selectLengthTask(int hours, int minutes) throws Exception {
        // Click 'edit' icon for length field
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickFieldLength();
        // Select needed hours
        String hoursString = Integer.toString(hours);
        selectItemFromListByOptionName(inlineEditTaskPage.getFieldLengthHoursPopup(), hoursString);
        String minutesString = Integer.toString(minutes);
        selectItemFromListByOptionName(inlineEditTaskPage.getFieldLengthMinutesPopup(), minutesString);
        closePopup();
    }
    public void selectLengthMediaPage(int hours, int minutes) throws Exception {
        // Click 'edit' icon for length field
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickFieldLength();
        // Select needed hours
        String hoursString = Integer.toString(hours);
        selectItemFromListByOptionName(inlineEditMediaPage.getFieldLengthHoursPopup(), hoursString);
        String minutesString = Integer.toString(minutes);
        selectItemFromListByOptionName(inlineEditMediaPage.getFieldLengthMinutesPopup(), minutesString);
    }
    public void closePopup() {
        // Click outside the popup to close it
        WebElement background = webDriver.findElement(By.cssSelector(".quickedit-active"));
        background.click();
        // Wait until the popup becomes invisible
        waitUntilPopupIsClosed();
    }
    public void waitUntilPopupIsClosed() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("quickedit-field-form")));
    }
    public void selectMaterialType(String type) throws Exception {
        WebElement material = webDriver.findElement(By.cssSelector("a[type='" + type + "']"));
        waitUntilElementIsClickable(material);
        Thread.sleep(500);
        clickItem(material, "The material link could not be clicked on the popup.");
    }
    public void waitUntilMaterialIsAdded() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#add-material-backdrop")));
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("#add-material-backdrop")));
    }
    public int checkMaterialTemplateIsAdded(String content) throws Exception {
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getFieldMaterial());
        List<WebElement> materials = webDriver.findElements(By.cssSelector("a[href*='/course_material/nojs/" + content + "']"));
        Assert.assertTrue("No material with type '" + content + "' is displayed in 'Course material.'", materials.size() >= 1);
        String nodeId = webDriver.findElement(By.cssSelector("a[href*='/course_material/nojs/" + content + "'] div")).getAttribute("data-quickedit-entity-id");
        String[] parts = nodeId.split("node/"); // Separate nodeId
        String node = parts[1]; // get node
        return Integer.parseInt(node);
    }
    public void openMaterialEditPage(String content, int node) throws Exception {
        // Click material link with the node
        String cssMaterial = "a[href*='/course_material/nojs/" + content + "/temp_course_content_" + node + "']";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssMaterial)));
        WebElement materialLink = webDriver.findElement(By.cssSelector(cssMaterial));
        clickItem(materialLink, "The material link could not be clicked on the View page.");
        // Open its Edit page
        BasePage basePage = new BasePage(webDriver, pageLocation);
        waitUntilElementIsVisible(basePage.getPopupModalContent());
        String cssEditLink = "[class*='editPage'] [href*='" + node + "']";
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector(cssEditLink)));
        Thread.sleep(500);
        WebElement editLink = webDriver.findElement(By.cssSelector(cssEditLink));
        clickItem(editLink, "The 'Edit page' link could not be clicked from material popup.");
        wait.until(ExpectedConditions.elementToBeClickable(By.partialLinkText("Advanced edit")));
        webDriver.findElement(By.partialLinkText("Advanced edit")).click();
    }
    public void removeMaterial(int node) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[href*='" + node + "'] .removeButton")));
        WebElement crossSign = webDriver.findElement(By.cssSelector("a[href*='" + node + "'] .removeButton"));
        clickItem(crossSign, "The cross sign for the material could not be clicked.");
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("a[href*='" + node + "'] .removeButton")));
    }
    public void checkMaterialIsRemoved(int node) throws Exception {
        Thread.sleep(1000); // wait until elements on the page load
        String nodeString = Integer.toString(node);
        List<WebElement> materials = webDriver.findElements(By.cssSelector("a[href*='" + nodeString + "']"));
        Assert.assertTrue("The material with node " + nodeString + " is displayed in 'Course material.'", materials.size() == 0);
    }
    public void addLinkMaterial(String title, String url) throws Exception {
        // Wait until popup opens
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.waitUntilLoading();
        waitUntilElementIsVisible(basePage.getPopupQuickEdit());
        // Fill field Title
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = new LinksPopupInlineEditingPage(webDriver, pageLocation);
        linksPopupInlineEditingPage.fillFieldTitle(title);
        // Fill field URL
        linksPopupInlineEditingPage.fillFieldUrl(url);
        // Select 'Open URL in a New Window' option to open the link at the same tab
        linksPopupInlineEditingPage.selectCheckboxOpenInNewWindow();
    }
    public void add2ndLinkMaterial(String title, String url) throws Exception {
        // Wait until popup opens
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.waitUntilLoading();
        waitUntilElementIsVisible(basePage.getPopupQuickEdit());
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = new LinksPopupInlineEditingPage(webDriver, pageLocation);
        linksPopupInlineEditingPage.clickButtonAddAnotherItem();

        // Fill field Title for other Link
        linksPopupInlineEditingPage.fillFieldTitle3(title);
        // Fill field URL for other Link
        linksPopupInlineEditingPage.fillFieldUrl3(url);
    }
    public LinksPopupInlineEditingPage openLinksPopup() throws Exception {
        // Wait until icon 'edit' becomes visible, click
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".field--name-field-links-course")));
        WebElement iconEdit = webDriver.findElement(By.cssSelector(".field--name-field-links-course"));
        iconEdit.click();
        // Wait until popup opens
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.waitUntilLoading();
        waitUntilElementIsVisible(basePage.getPopupQuickEdit());
        return new LinksPopupInlineEditingPage(webDriver, pageLocation);
    }
    public DownloadsPopupInlineEditingPage openDownloadsPopup() throws Exception {
        // Wait until icon 'edit' becomes visible, click
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".field--name-field-download-title-course")));
        WebElement iconEdit = webDriver.findElement(By.cssSelector(".field--name-field-download-title-course"));
        iconEdit.click();
        // Wait until popup opens
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.waitUntilLoading();
        waitUntilElementIsVisible(basePage.getPopupQuickEdit());
        return new DownloadsPopupInlineEditingPage(webDriver, pageLocation);
    }
    public String getMaterialXpath(int node) {
        String nodeString = Integer.toString(node);
        String xpathMaterial = "//*[@id='mobileMenu']/div[1]/div[1]//div[contains(text(), '" + nodeString + "')]";
        return xpathMaterial;
    }
    public String getLinkXpath(String linkTitle) {
        String xpathLink = "//*[@id='mobileMenu']/div[1]/div[1]//a[text()='" + linkTitle + "']";
        return xpathLink;
    }
    public String getFileXpath(String fileTitle) {
        String xpathFile = "//*[@id='mobileMenu']/div[1]/div[1]//div[text()='" + fileTitle + "']";
        return xpathFile;
    }
    public void addDownloadedFileMaterial(String title, String filePath) throws Exception {
        // Wait until popup opens
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.waitUntilLoading();
        waitUntilElementIsVisible(basePage.getPopupQuickEdit());
        // Fill field Title
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = new DownloadsPopupInlineEditingPage(webDriver, pageLocation);
        downloadsPopupInlineEditingPage.fillFieldTitle(title);
        // Upload file
        uploadFileToDownloadsPopup(filePath);
    }
    public void uploadFileToDownloadsPopup(String filePath) throws Exception {
        // Get absolute file path
        String path = new File(filePath).getAbsolutePath();
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = new DownloadsPopupInlineEditingPage(webDriver, pageLocation);
        downloadsPopupInlineEditingPage.getFieldUploadFile().sendKeys(path);
        downloadsPopupInlineEditingPage.clickButtonUpload();
        waitUntilElementIsVisible(downloadsPopupInlineEditingPage.getButtonRemove());
    }
    public void add2ndFileMaterial(String title, String filePath) throws Exception {
        // Wait until popup opens
        BasePage basePage = new BasePage(webDriver, pageLocation);
        basePage.waitUntilLoading();
        waitUntilElementIsVisible(basePage.getPopupQuickEdit());
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = new DownloadsPopupInlineEditingPage(webDriver, pageLocation);
        downloadsPopupInlineEditingPage.clickButtonAddAnotherItem();
        // Fill field Title for other File
        downloadsPopupInlineEditingPage.fillFieldTitle2(title);
        // Upload a file
        String path = new File(filePath).getAbsolutePath(); // get absolute file path
        downloadsPopupInlineEditingPage.getFieldUploadFile2().sendKeys(path);
        downloadsPopupInlineEditingPage.clickButtonUpload2();
        waitUntilElementIsVisible(downloadsPopupInlineEditingPage.getButtonRemove2());
    }
    public void checkMaterialIsDisplayed(int node) {
        String xpath = getMaterialXpath(node);
        List<WebElement> materials = webDriver.findElements(By.xpath(xpath));
        String nodeString = Integer.toString(node);
        Assert.assertTrue("The material with node '" + nodeString + "' is not displayed on the View page.", materials.size() == 1);
    }
    public void checkLinkIsDisplayed(String title) {
        String xpath = getLinkXpath(title);
        List<WebElement> links = webDriver.findElements(By.xpath(xpath));
        Assert.assertTrue("The link with title '" + title + "' is not displayed on the View page.", links.size() == 1);
    }
    public void checkFileIsDisplayed(String title) {
        String xpath = getFileXpath(title);
        List<WebElement> files = webDriver.findElements(By.xpath(xpath));
        Assert.assertTrue("The file with title '" + title + "' is not displayed on the View page.", files.size() == 1);
    }
    public void checkLengthFieldValue(WebElement field) {
        String lengthActual = field.findElement(By.cssSelector(".field__item")).getText();
        if (hours == 0) {
            Assert.assertTrue("Wrong length is displayed: " + lengthActual,
                    lengthActual.equals(minutes + " min"));
            return;
        }
        if (minutes == 0 && (hours!=1)) {
            Assert.assertTrue("Wrong length is displayed: " + lengthActual,
                    lengthActual.equals(hours + " hours"));
            return;
        }
        if (hours!=0 && minutes!=0) {
            Assert.assertTrue("Wrong length is displayed: " + lengthActual,
                    lengthActual.matches(hours + " hour.*" + minutes + " min"));
            return;
        }
        if (hours==1 && minutes==0) {
            Assert.assertTrue("Wrong length is displayed: " + lengthActual,
                    lengthActual.equals(hours + " hour"));
            return;
        }
        Assert.assertTrue("Wrong Length field value is displayed on View page", false);
    }

    public void selectStartDateInline(int days) throws Exception {
        Thread.sleep(1000);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("edit-field-date-start-end-und-0-value-datepicker-popup-0")));
        // Select start date
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, days); // int day - is quantity of days
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String otherDate = format.format(currentDate.getTime());
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-value-datepicker-popup-0')" +
                        ".value = '" + otherDate + "';");
    }
    public void selectEndDateInline(int days) throws Exception {
        Thread.sleep(1000);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("edit-field-date-start-end-und-0-value2-datepicker-popup-0")));
        // Select start date
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, days); // int day - is quantity of days
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        String otherDate = format.format(currentDate.getTime());
        ((JavascriptExecutor) webDriver)
                .executeScript("document.getElementById('edit-field-date-start-end-und-0-value2-datepicker-popup-0')" +
                        ".value = '" + otherDate + "';");
    }









}
