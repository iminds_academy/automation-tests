package com.inceptum.content.inlineEditing;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.inceptum.pages.DownloadsPopupInlineEditingPage;
import com.inceptum.pages.EditImagePage;
import com.inceptum.pages.EditPresentationPage;
import com.inceptum.pages.EditVideoPage;
import com.inceptum.pages.InlineEditLiveSessionPage;
import com.inceptum.pages.InlineEditMediaPage;
import com.inceptum.pages.InlineEditTaskPage;
import com.inceptum.pages.InlineEditTheoryPage;
import com.inceptum.pages.LinksPopupInlineEditingPage;
import com.inceptum.pages.SelectImagePage;
import com.inceptum.pages.SelectPresentationPage;
import com.inceptum.pages.SelectVideoPage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;

/**
 * Created by Olga on 28.04.2015.
 */
public class InlineEditingTest extends InlineEditingBaseTest {


    /*---------CONSTANTS--------*/

    // protected final String actor = "admin";


     /* ----- Tests ----- */

// 'Live session' inline editing

    @Test
    public void testEditLiveSessionWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();
        // Edit Title
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.fillFieldTitleInline(liveSessionTitle);
        // Edit LiveSession type
        selectLiveSessionType(skype);
        // Edit WhatYouNeedToDo text
        inlineEditLiveSessionPage.fillFieldWhatYouNeedToDoInline(whatDoYouNeedToDoText);
        // Edit Planning text
        inlineEditLiveSessionPage.fillFieldPlanningInline(planningText);
        // Save changes
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check the Live session is updated
        String titleActual = viewLiveSessionPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewLiveSession page: " + titleActual,
                titleActual.contains(liveSessionTitle)); // LS title
        String typeActual = viewLiveSessionPage.getFieldLiveSessionType().findElement(By.cssSelector(".field__item")).getText();
        Assert.assertTrue("Wrong LiveSession type is displayed: " + typeActual, typeActual.equals(skype)); // LS type
        String whatYouNeedToDoActual = viewLiveSessionPage.getFieldWhatYouNeedToDo().getText();
        Assert.assertTrue("Wrong WhatDoYouNeedToDo text is displayed on ViewLiveSession page: " + whatYouNeedToDoActual,
                whatYouNeedToDoActual.equals(whatDoYouNeedToDoText)); // WhatYouNeedToDo field
        String planningActual = viewLiveSessionPage.getFieldPlanning().getText();
        Assert.assertTrue("Wrong Planning text is displayed on ViewLiveSession page: " + planningActual,
                planningActual.equals(planningText)); // Planning field
    }

    @Test
    public void testChangeDateForLiveSessionWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup, select an end date
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickFieldDateInline();
        String startDate = inlineEditLiveSessionPage.getStartDate(); // get start date
        inlineEditLiveSessionPage.selectCheckboxUseStartAndEndDate(); // use 'start and end date'
        selectDateDiffFormat(inlineEditLiveSessionPage.getFieldEndDate(), 1);
        String endDate = inlineEditLiveSessionPage.getEndDate(); // get end date
        closePopup();

        // Check that correct date is displayed on ViewLiveSession page
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        String dateActual = viewLiveSessionPage.getFieldDate().getText();
        String dateExpected = "From \n" + startDate + " to " + endDate;
        Assert.assertTrue("Wrong date is displayed: " + dateActual, dateActual.equals(dateExpected));
    }

    @Test // PILOT-1266 (also add getPrefix method if it's needed - as for Theory)
    public void testAddTimeToLiveSessionWithInlineEditing() throws Exception { // problem with unexpectable collapsing of 'Due date' popup
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickFieldDateInline();
        String startDate = inlineEditLiveSessionPage.getStartDate(); // get start date
        // Enter start time
        inlineEditLiveSessionPage.selectCheckboxShowTime();
        String startTime = enterStartTime(inlineEditLiveSessionPage.getFieldStartTime(), 0);
        // Submit inline editing, check date and time are shown correctly on ViewLiveSession page
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        String dateAndTimeStartExpected = startDate + ".*" + startTime;
        String dateAndTimeStartActual = viewLiveSessionPage.getFieldDate().getText();
        Assert.assertTrue("Wrong date and time is shown: " + dateAndTimeStartActual, dateAndTimeStartActual.matches(dateAndTimeStartExpected));

        // Click 'Edit page' link for inline editing again, add an end date and time
        clickEditPage();
        inlineEditLiveSessionPage.clickFieldDateInline();
        inlineEditLiveSessionPage.selectCheckboxUseStartAndEndDate();
        selectEndDateInline(1); // select end date > start date
        String endDate = inlineEditLiveSessionPage.getEndDate(); // get end date
        String endTime = enterEndTime(inlineEditLiveSessionPage.getFieldEndTime(), 1); // enter end time
        // Submit changes, check start/end date and time are shown correctly on ViewLiveSession page
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        String dateAndTimeExpected = "From " + startDate + ".*" + startTime +
                " to " + endDate + ".*" + endTime;
        String dateAndTimeActual = viewLiveSessionPage.getFieldDate().getText();  // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong start/end date and time are shown: " + dateAndTimeActual, dateAndTimeActual.matches(dateAndTimeExpected));
    }

    @Test
    public void testAddImageToLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Course material' with the proper button
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Image Edit page from the LiveSession and update it
        String urlViewLiveSessionPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewLiveSessionPage.getFieldMaterial());
        openMaterialEditPage(image, node);
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle); // new title
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image7Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        editImagePage.clickButtonSave();

        // Check that the new Image title is displayed on ViewLiveSession page
        webDriver.get(urlViewLiveSessionPage);
        String titleActual = viewLiveSessionPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(imageTitle));
    }

    @Test // PILOT-1326
    public void testRemoveImageFromLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Course material' with the proper button
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewLiveSession page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test
    public void testAddVideoToLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Course material' with the proper button
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Video Edit page from the LiveSession and update it
        String urlViewLiveSessionPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewLiveSessionPage.getFieldMaterial());
        openMaterialEditPage(video, node);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle); // new title
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlYouTube);
        waitUntilElementIsVisible(editVideoPage.getLinkRemoveMedia());
        editVideoPage.clickButtonSave();

        // Check that the new Video title is displayed on ViewLiveSession page
        webDriver.get(urlViewLiveSessionPage);
        String titleActual = viewLiveSessionPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(videoTitle));
    }

    @Test // PILOT-1326
    public void testRemoveVideoFromLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Course material' with the proper button
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewLiveSession page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test
    public void testAddPresentationToLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Course material' with the proper button
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Presentation Edit page from the LiveSession and update it
        String urlViewLiveSessionPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewLiveSessionPage.getFieldMaterial());
        openMaterialEditPage(presentation, node);
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        editPresentationPage.clickButtonSave();

        // Check that the new Presentation title is displayed on ViewLiveSession page
        webDriver.get(urlViewLiveSessionPage);
        String titleActual = viewLiveSessionPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(presentationTitle));
    }

    @Test // PILOT-1326
    public void testRemovePresentationFromLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Course material' with the proper button
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewLiveSession page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test // PILOT-1274
    public void testAddExternalLinkToLiveSessionCourseMaterial() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that correct title (url) of link is displayed on View page after saving
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        String titleActual = viewLiveSessionPage.getLinkExternalCourseMaterial().getText(); // bug !!!!!!!!!
        String urlActual = viewLiveSessionPage.getLinkExternalCourseMaterial().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.equals(linkUrl));
    }

    @Test // PILOT-1274
    public void testAdd2ndLinkToLiveSessionCourseMaterial() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();

        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Add the 2nd link with inline editing
        clickEditPage();
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        add2ndLinkMaterial(linkTitle2, linkUrl2); // 'Open URL in a New Window' option is selected here by default

        // Save changes, check that both titles of the links are displayed on the View page
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkExternalCourseMaterial());  // bug !!!!!!!!!
        WebElement link1 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle + "')]")); // the 1st link
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on View page.",link1.isDisplayed());
        WebElement link2 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle2 + "')]")); // the 2nd link
        Assert.assertTrue("The link with title '" + linkTitle2 + "' is not displayed on View page.", link2.isDisplayed());
        // Click the 2nd added Link, check that it opens a window in a new browser tab
        viewMaterialInAnotherBrowserTab(link2);
    }

    @Test // PILOT-1274
    public void testEditLinkInLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Edit the Link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        linksPopupInlineEditingPage.fillFieldUrl(linkUrl2); // url

        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkExternalCourseMaterial());

        // Check that the Link has correct title and url
        String titleActual = viewLiveSessionPage.getLinkExternalCourseMaterial().getText(); // bug !!!!!!!!!
        String urlActual = viewLiveSessionPage.getLinkExternalCourseMaterial().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle2));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.contains(linkUrl2));
    }

    @Test
    public void testRemoveLinkFromLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that the Link with defined title is displayed on View page after saving
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        String xpathLink = getLinkXpath(linkTitle);
        List<WebElement> links = webDriver.findElements(By.xpath(xpathLink));
        webDriver.navigate().refresh();
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on the View page.", links.size() == 1);

        // Open Links popup and remove the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.getFieldTitle().clear();
        linksPopupInlineEditingPage.getFieldUrl().clear();
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that no link with defined title is displayed on the View page
        links = webDriver.findElements(By.xpath(xpathLink));
        Assert.assertTrue("The link with title '" + linkTitle + "' is displayed on the View page.", links.size() == 0);
    }

    @Test // PILOT-1274
    public void testAddFileToLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);

        // Check that correct title is displayed on View page after saving
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // !!!!!!!! Remove the line after PILOT-1274 is resolved !!!!!!
        webDriver.navigate().refresh();

        WebElement fieldFile = viewLiveSessionPage.getLinkDownloadedCourseMaterial(); // PILOT-1274 !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle));
        // Check that the file can be downloaded
        viewLiveSessionPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1274
    public void testAdd2ndFileToLiveSessionCourseMaterial() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, xlsxFilePath);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the file can be downloaded
        viewLiveSessionPage.clickLinkDownloadedCourseMaterial(); // bug !!!!!!!!
        waitUntilFileDownloaded();

        // Add the 2nd file with inline editing to CourseMaterial
        clickEditPage();
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        add2ndFileMaterial(fileTitle2, pptxFilePath);

        // Save changes, check that both titles of the files are displayed on the View page
        inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // The 1st file
        String xpathFile1 = getFileXpath(fileTitle);
        String titleActual1 = webDriver.findElement(By.xpath(xpathFile1)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle + "' is not displayed on View page.", titleActual1.equals(fileTitle));
        // The 2nd file
        String xpathFile2 = getFileXpath(fileTitle2);
        String titleActual2 = webDriver.findElement(By.xpath(xpathFile2)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle2 + "' is not displayed on View page.", titleActual2.equals(fileTitle2));
    }

    @Test // PILOT-1274
    public void testEditFileInLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup and change data
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.fillFieldTitle(fileTitle2); // title
        downloadsPopupInlineEditingPage.clickButtonRemove();
        uploadFileToDownloadsPopup(xlsxFilePath); // file
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the new title is displayed on View page
        WebElement fieldFile = viewLiveSessionPage.getLinkDownloadedCourseMaterial(); // bug !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle2));
        // Check that the new file can be downloaded
        viewLiveSessionPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1276
    public void testRemoveFileFromLiveSessionCourseMaterialWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup, remove fieldset
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.clickButtonRemoveFieldSet();
        viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that no file with defined title is displayed on the View page
        String xpathFile = getFileXpath(fileTitle);
        List<WebElement> files = webDriver.findElements(By.xpath(xpathFile)); // bug !!!!!!
        Assert.assertTrue("The file with title '" + fileTitle + "' is displayed on the View page.", files.size() == 0);
    }

    @Test
    public void testAddAllMaterialsToLiveSessionWithInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add an image
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int nodeImage = checkMaterialTemplateIsAdded(image); // get image node
        // Add a video
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int nodeVideo = checkMaterialTemplateIsAdded(video); // get video node
        // Add a presentation
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int nodePresentation = checkMaterialTemplateIsAdded(presentation); // get presentation node
        // Add a link
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditLiveSessionPage.clickButtonSaveInline();
        // Add a file
        clickEditPage();
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, xlsxFilePath);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewLiveSessionPage.getFieldMaterial());

        // Check all materials are displayed on ViewLiveSession page
        checkMaterialIsDisplayed(nodeImage);
        checkMaterialIsDisplayed(nodeVideo);
        checkMaterialIsDisplayed(nodePresentation);
        checkLinkIsDisplayed(linkTitle);
        checkFileIsDisplayed(fileTitle);
    }

    @Test // PILOT-1274
    public void testCancelInlineEditingOfLiveSessionWithCloseButton() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditLiveSessionPage.clickButtonCloseInline(); // click Close button for inline editing
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveChanges(); // click Save on popup
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkExternalCourseMaterial()); // bug !!!!!!!!!
        // Check the link is displayed on View page
        checkLinkIsDisplayed(linkTitle);

        // Edit the title of the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        // Click Close button for inline editing
        inlineEditLiveSessionPage.clickButtonCloseInline();
        inlineEditLiveSessionPage.clickButtonDiscardChanges(); // discard changes
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkExternalCourseMaterial());

        // Check that non-updated title is displayed on View page
        checkLinkIsDisplayed(linkTitle);
    }

    @Test
    public void testCheckValidationForLiveSessionInlineEditing() throws Exception {
        // Create LiveSession template
        createLiveSessionTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Clear the fields: Title, WhatYouNeedToDo, Planning
        InlineEditLiveSessionPage inlineEditLiveSessionPage = new InlineEditLiveSessionPage(webDriver, pageLocation);
        inlineEditLiveSessionPage.clearFieldTitleInline();
        inlineEditLiveSessionPage.clearFieldWhatYouNeedToDoInline();
        inlineEditLiveSessionPage.clearFieldPlanningInline();
        inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // Check that inline editing is still active, error messages for the fields are displayed
        Assert.assertTrue("The button Save is not displayed for inline editing.",
                inlineEditLiveSessionPage.getButtonSaveInline().isDisplayed());
        Assert.assertTrue("Error message for Title field is not displayed.",
                inlineEditLiveSessionPage.getErrorMessageFieldTitle().isDisplayed());
        Assert.assertTrue("Error message for 'What you need to do' field is not displayed.",
                inlineEditLiveSessionPage.getErrorMessageFieldWhatYouNeedToDo().isDisplayed());
        Assert.assertTrue("Error message for 'Planning' field is not displayed.",
                inlineEditLiveSessionPage.getErrorMessageFieldPlanning().isDisplayed());

        // Fill the fields, submit changes
        inlineEditLiveSessionPage.fillFieldTitleInline(liveSessionTitle);
        inlineEditLiveSessionPage.fillFieldWhatYouNeedToDoWithSource(whatDoYouNeedToDoText);
        inlineEditLiveSessionPage.fillFieldPlanningWithSource(planningText);
        ViewLiveSessionPage viewLiveSessionPage = inlineEditLiveSessionPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewLiveSessionPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check that the Live session is updated
        String titleActual = viewLiveSessionPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewLiveSession page: " + titleActual,
                titleActual.contains(liveSessionTitle)); // LS title
        String whatYouNeedToDoActual = viewLiveSessionPage.getFieldWhatYouNeedToDo().getText();
        Assert.assertTrue("Wrong WhatDoYouNeedToDo text is displayed on ViewLiveSession page: " + whatYouNeedToDoActual,
                whatYouNeedToDoActual.equals(whatDoYouNeedToDoText)); // WhatYouNeedToDo field
        String planningActual = viewLiveSessionPage.getFieldPlanning().getText();
        Assert.assertTrue("Wrong Planning text is displayed on ViewLiveSession page: " + planningActual,
                planningActual.equals(planningText)); // Planning field
    }



// 'Theory' inline editing

    @Test
    public void testEditTheoryWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();
        // Edit Title
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.fillFieldTitleInline(theoryTitle);
        // Edit length field
        selectLengthTheory(hours, minutes);
        // Edit WhatYouNeedToDo text
        inlineEditTheoryPage.fillFieldWhatYouNeedToDoInline(whatDoYouNeedToDoText);
        // Edit WhatsThisFor text
        inlineEditTheoryPage.fillFieldWhatsThisForInline(whatIsThisForText);
        // Edit LearningObjectives text
        inlineEditTheoryPage.fillFieldLearningObjectivesInline(learningObjectivesText);
        // Save changes
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check the Theory is updated
        String titleActual = viewTheoryPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewTheory page: " + titleActual,
                titleActual.contains(theoryTitle)); // Theory title
        checkLengthFieldValue(viewTheoryPage.getFieldLength()); // Length field
        String whatYouNeedToDoActual = viewTheoryPage.getFieldWhatYouNeedToDo().getText();
        Assert.assertTrue("Wrong WhatDoYouNeedToDo text is displayed on ViewTheory page: " + whatYouNeedToDoActual,
                whatYouNeedToDoActual.equals(whatDoYouNeedToDoText)); // WhatYouNeedToDo field
        String whatsThisForActual = viewTheoryPage.getFieldWhatThisFor().getText();
        Assert.assertTrue("Wrong WhatsThisFor text is displayed on ViewTheory page: " + whatsThisForActual,
                whatsThisForActual.equals(whatIsThisForText)); // WhatsThisFor field
        String learningObjectivesActual = viewTheoryPage.getFieldLearningObjectives().getText();
        Assert.assertTrue("Wrong LearningObjectives text is displayed on ViewTheory page: " + learningObjectivesActual,
                learningObjectivesActual.equals(learningObjectivesText)); // LearningObjectives field
    }

    @Test // PILOT-1266
    public void testChangeDateForTheoryWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup, select an end date
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickFieldDateInline();
        String startDate = inlineEditTheoryPage.getStartDate(); // get start date
        inlineEditTheoryPage.selectCheckboxUseStartAndEndDate(); // use 'start and end date'
        selectDateDiffFormat(inlineEditTheoryPage.getFieldEndDate(), 1);
        String endDate = inlineEditTheoryPage.getEndDate(); // get end date
        closePopup();

        // Check that correct date is displayed on ViewTheory page
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        String prefix = viewTheoryPage.getPrefix(); // get date prefix
        String dateActual = prefix + viewTheoryPage.getFieldDate().getText();
        String dateExpected = "From " + startDate + " to " + endDate; // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong date is displayed: " + dateActual, dateActual.equals(dateExpected));
    }

    @Test // PILOT-1266
    public void testAddTimeToTheoryWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickFieldDateInline();
        String startDate = inlineEditTheoryPage.getStartDate(); // get start date
        // Enter start time
        inlineEditTheoryPage.selectCheckboxShowTime();
        String startTime = enterStartTime(inlineEditTheoryPage.getFieldStartTime(), 0);
        // Submit inline editing, check date and time are shown correctly on ViewTheory page
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        String prefix = viewTheoryPage.getPrefix(); // get date prefix
        String dateAndTimeStartExpected = "Read by " + startDate + ".*" + startTime;
        String dateAndTimeStartActual = prefix + viewTheoryPage.getFieldDate().getText();
        Assert.assertTrue("Wrong date and time is shown: " + dateAndTimeStartActual, dateAndTimeStartActual.matches(dateAndTimeStartExpected));

        // Click 'Edit page' link for inline editing again, add an end date and time
        clickEditPage();
        inlineEditTheoryPage.clickFieldDateInline();
        inlineEditTheoryPage.selectCheckboxUseStartAndEndDate();
        selectEndDateInline(1); // select end date > start date
        String endDate = inlineEditTheoryPage.getEndDate(); // get end date
        String endTime = enterEndTime(inlineEditTheoryPage.getFieldEndTime(), 1); // enter end time
        // Submit changes, check start/end date and time are shown correctly on ViewTheory page
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        prefix = viewTheoryPage.getPrefix(); // get date prefix
        String dateAndTimeExpected = "From " + startDate + ".*" + startTime +
                " to " + endDate + ".*" + endTime;
        String dateAndTimeActual = prefix + viewTheoryPage.getFieldDate().getText();  // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong start/end date and time are shown: " + dateAndTimeActual, dateAndTimeActual.matches(dateAndTimeExpected));
    }

    @Test
    public void testAddImageToTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Course material' with the proper button
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Image Edit page from the Theory and update it
        String urlViewTheoryPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewTheoryPage.getFieldMaterial());
        openMaterialEditPage(image, node);
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle); // new title
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image2Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        editImagePage.clickButtonSave();

        // Check that the new Image title is displayed on ViewTheory page
        webDriver.get(urlViewTheoryPage);
        String titleActual = viewTheoryPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(imageTitle));
    }

    @Test // PILOT-1326
    public void testRemoveImageFromTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Course material' with the proper button
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewTheory page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test
    public void testAddVideoToTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Course material' with the proper button
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Video Edit page from the Theory and update it
        String urlViewTheoryPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewTheoryPage.getFieldMaterial());
        openMaterialEditPage(video, node);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle); // new title
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlYouTube);
        waitUntilElementIsVisible(editVideoPage.getLinkRemoveMedia());
        editVideoPage.clickButtonSave();

        // Check that the new Video title is displayed on ViewTheory page
        webDriver.get(urlViewTheoryPage);
        String titleActual = viewTheoryPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(videoTitle));
    }

    @Test // PILOT-1326
    public void testRemoveVideoFromTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Course material' with the proper button
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewTheory page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test
    public void testAddPresentationToTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Course material' with the proper button
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Presentation Edit page from the Theory and update it
        String urlViewTheoryPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewTheoryPage.getFieldMaterial());
        openMaterialEditPage(presentation, node);
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        editPresentationPage.clickButtonSave();

        // Check that the new Presentation title is displayed on ViewTheory page
        webDriver.get(urlViewTheoryPage);
        String titleActual = viewTheoryPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(presentationTitle));
    }

    @Test // PILOT-1326
    public void testRemovePresentationFromTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Course material' with the proper button
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewTheory page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test // PILOT-1274
    public void testAddExternalLinkToTheoryCourseMaterial() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that correct title (url) of link is displayed on View page after saving
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        String titleActual = viewTheoryPage.getLinkExternalCourseMaterial().getText(); // bug !!!!!!!!!
        String urlActual = viewTheoryPage.getLinkExternalCourseMaterial().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.equals(linkUrl));
    }

    @Test // PILOT-1274
    public void testAddThe2ndLinkToTheoryCourseMaterial() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();

        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Add the 2nd link with inline editing
        clickEditPage();
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        add2ndLinkMaterial(linkTitle2, linkUrl2); // select 'Open URL in a New Window' option here

        // Save changes, check that both titles of the links are displayed on the View page
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewTheoryPage.getLinkExternalCourseMaterial());  // bug !!!!!!!!!
        WebElement link1 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle + "')]")); // the 1st link
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on View page.",link1.isDisplayed());
        WebElement link2 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle2 + "')]")); // the 2nd link
        Assert.assertTrue("The link with title '" + linkTitle2 + "' is not displayed on View page.", link2.isDisplayed());
        // Click the 2nd added Link, check that it opens a window in a new browser tab
        viewMaterialInAnotherBrowserTab(link2);
    }

    @Test // PILOT-1274
    public void testEditLinkInTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Edit the Link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        linksPopupInlineEditingPage.fillFieldUrl(linkUrl2); // url

        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewTheoryPage.getLinkExternalCourseMaterial());

        // Check that the Link has correct title and url
        String titleActual = viewTheoryPage.getLinkExternalCourseMaterial().getText(); // bug !!!!!!!!!
        String urlActual = viewTheoryPage.getLinkExternalCourseMaterial().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle2));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.contains(linkUrl2));
    }

    @Test
    public void testRemoveLinkFromTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that the Link with defined title is displayed on View page after saving
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        String xpathLink = getLinkXpath(linkTitle);
        List<WebElement> links = webDriver.findElements(By.xpath(xpathLink));
        webDriver.navigate().refresh();
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on the View page.", links.size() == 1);

        // Open Links popup and remove the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.getFieldTitle().clear();
        linksPopupInlineEditingPage.getFieldUrl().clear();
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that no link with defined title is displayed on the View page
        links = webDriver.findElements(By.xpath(xpathLink));
        Assert.assertTrue("The link with title '" + linkTitle + "' is displayed on the View page.", links.size() == 0);
    }

    @Test // PILOT-1274
    public void testAddFileToTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);

        // Check that correct title is displayed on View page after saving
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        WebElement fieldFile = viewTheoryPage.getLinkDownloadedCourseMaterial(); // bug !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle));
        // Check that the file can be downloaded
        viewTheoryPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1274
    public void testAdd2ndFileToTheoryCourseMaterial() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the file can be downloaded
        viewTheoryPage.clickLinkDownloadedCourseMaterial(); // bug !!!!!!!!
        waitUntilFileDownloaded();

        // Add the 2nd file with inline editing to CourseMaterial
        clickEditPage();
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        add2ndFileMaterial(fileTitle2, pptxFilePath);

        // Save changes, check that both titles of the files are displayed on the View page
        inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // The 1st file
        String xpathFile1 = getFileXpath(fileTitle);
        String titleActual1 = webDriver.findElement(By.xpath(xpathFile1)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle + "' is not displayed on View page.", titleActual1.equals(fileTitle));
        // The 2nd file
        String xpathFile2 = getFileXpath(fileTitle2);
        String titleActual2 = webDriver.findElement(By.xpath(xpathFile2)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle2 + "' is not displayed on View page.", titleActual2.equals(fileTitle2));
    }

    @Test // PILOT-1274
    public void testEditFileInTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup and change data
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.fillFieldTitle(fileTitle2); // title
        downloadsPopupInlineEditingPage.clickButtonRemove();
        uploadFileToDownloadsPopup(xlsxFilePath); // file
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the new title is displayed on View page
        WebElement fieldFile = viewTheoryPage.getLinkDownloadedCourseMaterial(); // bug !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle2));
        // Check that the new file can be downloaded
        viewTheoryPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1276
    public void testRemoveFileFromTheoryCourseMaterialWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup, remove fieldset
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.clickButtonRemoveFieldSet();
        viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that no file with defined title is displayed on the View page
        String xpathFile = getFileXpath(fileTitle);
        List<WebElement> files = webDriver.findElements(By.xpath(xpathFile)); // bug !!!!!!
        Assert.assertTrue("The file with title '" + fileTitle + "' is displayed on the View page.", files.size() == 0);
    }

    @Test
    public void testAddAllMaterialsToTheoryWithInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add an image
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int nodeImage = checkMaterialTemplateIsAdded(image); // get image node
        // Add a video
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int nodeVideo = checkMaterialTemplateIsAdded(video); // get video node
        // Add a presentation
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int nodePresentation = checkMaterialTemplateIsAdded(presentation); // get presentation node
        // Add a link
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditTheoryPage.clickButtonSaveInline();
        // Add a file
        clickEditPage();
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewTheoryPage.getFieldMaterial());

        // Check all materials are displayed on ViewTheory page
        checkMaterialIsDisplayed(nodeImage);
        checkMaterialIsDisplayed(nodeVideo);
        checkMaterialIsDisplayed(nodePresentation);
        checkLinkIsDisplayed(linkTitle);
        checkFileIsDisplayed(fileTitle);
    }

    @Test // PILOT-1274
    public void testCancelInlineEditingOfTheoryWithCloseButton() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditTheoryPage.clickButtonCloseInline(); // click Close button for inline editing
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveChanges(); // click Save on popup
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewTheoryPage.getLinkExternalCourseMaterial()); // bug !!!!!!!!!
        // Check the link is displayed on View page
        checkLinkIsDisplayed(linkTitle);

        // Edit the title of the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        // Click Close button for inline editing
        inlineEditTheoryPage.clickButtonCloseInline();
        inlineEditTheoryPage.clickButtonDiscardChanges(); // discard changes
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewTheoryPage.getLinkExternalCourseMaterial());

        // Check that non-updated title is displayed on View page
        checkLinkIsDisplayed(linkTitle);
    }

    @Test
    public void testCheckValidationForTheoryInlineEditing() throws Exception {
        // Create Theory template
        createTheoryTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Clear the fields: Title, WhatYouNeedToDo, WhatsThisFor, LearningObjectives
        InlineEditTheoryPage inlineEditTheoryPage = new InlineEditTheoryPage(webDriver, pageLocation);
        inlineEditTheoryPage.clearFieldTitleInline();
        inlineEditTheoryPage.clearFieldWhatYouNeedToDoInline();
        inlineEditTheoryPage.clearFieldWhatsThisForInline();
        inlineEditTheoryPage.clearFieldLearningObjectivesInline();
        inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // Check that inline editing is still active, error messages for the fields are displayed
        Assert.assertTrue("The button Save is not displayed for inline editing.",
                inlineEditTheoryPage.getButtonSaveInline().isDisplayed());
        Assert.assertTrue("Error message for Title field is not displayed.",
                inlineEditTheoryPage.getErrorMessageFieldTitle().isDisplayed());
        Assert.assertTrue("Error message for 'What you need to do' field is not displayed.",
                inlineEditTheoryPage.getErrorMessageFieldWhatYouNeedToDo().isDisplayed());
        Assert.assertTrue("Error message for 'Whats this for' field is not displayed.",
                inlineEditTheoryPage.getErrorMessageFieldWhatsThisFor().isDisplayed());
        Assert.assertTrue("Error message for 'Learning objectives' field is not displayed.",
                inlineEditTheoryPage.getErrorMessageFieldLearningObjectives().isDisplayed());

        // Fill the fields, submit changes
        inlineEditTheoryPage.fillFieldTitleInline(theoryTitle);
        inlineEditTheoryPage.fillFieldWhatYouNeedToDoWithSource(whatDoYouNeedToDoText);
        inlineEditTheoryPage.fillFieldWhatsThisForWithSource(whatIsThisForText);
        inlineEditTheoryPage.fillFieldLearningObjectivesWithSource(learningObjectivesText);
        ViewTheoryPage viewTheoryPage = inlineEditTheoryPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTheoryPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check that the Theory is updated
        String titleActual = viewTheoryPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewTheory page: " + titleActual,
                titleActual.contains(theoryTitle)); // Theory title
        String whatYouNeedToDoActual = viewTheoryPage.getFieldWhatYouNeedToDo().getText();
        Assert.assertTrue("Wrong WhatDoYouNeedToDo text is displayed on ViewTheory page: " + whatYouNeedToDoActual,
                whatYouNeedToDoActual.equals(whatDoYouNeedToDoText)); // WhatYouNeedToDo field
        String whatsThisForActual = viewTheoryPage.getFieldWhatThisFor().getText();
        Assert.assertTrue("Wrong WhatsThisFor text is displayed on ViewTheory page: " + whatsThisForActual,
                whatsThisForActual.equals(whatIsThisForText)); // WhatsThisFor field
        String learningObjectivesActual = viewTheoryPage.getFieldLearningObjectives().getText();
        Assert.assertTrue("Wrong LearningObjectives text is displayed on ViewTheory page: " + learningObjectivesActual,
                learningObjectivesActual.equals(learningObjectivesText)); // LearningObjectives field
    }



// 'Task' inline editing

    @Test
    public void testEditTaskWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();
        // Edit Title
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.fillFieldTitleInline(taskTitle);
        // Edit length field
        selectLengthTask(hours, minutes);
        // Edit WhatYouNeedToDo text
        inlineEditTaskPage.fillFieldWhatYouNeedToDoInline(whatDoYouNeedToDoText);
        // Edit WhatsThisFor text
        inlineEditTaskPage.fillFieldWhatsThisForInline(whatIsThisForText);
        // Edit LearningObjectives text
        inlineEditTaskPage.fillFieldLearningObjectivesInline(learningObjectivesText);
        // Save changes
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check the Task is updated
        String titleActual = viewTaskPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewTask page: " + titleActual,
                titleActual.contains(taskTitle)); // Task title
        checkLengthFieldValue(viewTaskPage.getFieldLength()); // Length field
        String whatYouNeedToDoActual = viewTaskPage.getFieldWhatYouNeedToDo().getText();
        Assert.assertTrue("Wrong WhatDoYouNeedToDo text is displayed on ViewTask page: " + whatYouNeedToDoActual,
                whatYouNeedToDoActual.equals(whatDoYouNeedToDoText)); // WhatYouNeedToDo field
        String whatsThisForActual = viewTaskPage.getFieldWhatThisFor().getText();
        Assert.assertTrue("Wrong WhatsThisFor text is displayed on ViewTask page: " + whatsThisForActual,
                whatsThisForActual.equals(whatIsThisForText)); // WhatsThisFor field
        String learningObjectivesActual = viewTaskPage.getFieldLearningObjectives().getText();
        Assert.assertTrue("Wrong LearningObjectives text is displayed on ViewTask page: " + learningObjectivesActual,
                learningObjectivesActual.equals(learningObjectivesText)); // LearningObjectives field
    }

    @Test // PILOT-1266
    public void testChangeDateForTaskWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup, select an end date
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickFieldDateInline();
        String startDate = inlineEditTaskPage.getStartDate(); // get start date
        inlineEditTaskPage.selectCheckboxUseStartAndEndDate(); // use 'start and end date'
        selectDateDiffFormat(inlineEditTaskPage.getFieldEndDate(), 1);
        String endDate = inlineEditTaskPage.getEndDate(); // get end date
        closePopup();

        // Check that correct date is displayed on ViewTask page
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        String prefix = viewTaskPage.getPrefix(); // get date prefix
        String dateActual = prefix + viewTaskPage.getFieldDate().getText();
        String dateExpected = "From " + startDate + " to " + endDate; // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong date is displayed: " + dateActual, dateActual.equals(dateExpected));
    }

    @Test // PILOT-1266
    public void testAddTimeToTaskWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickFieldDateInline();
        String startDate = inlineEditTaskPage.getStartDate(); // get start date
        // Enter start time
        inlineEditTaskPage.selectCheckboxShowTime();
        String startTime = enterStartTime(inlineEditTaskPage.getFieldStartTime(), 0);
        // Submit inline editing, check date and time are shown correctly on ViewTask page
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        String prefix = viewTaskPage.getPrefix(); // get date prefix
        String dateAndTimeStartExpected = "Complete by " + startDate + ".*" + startTime;
        String dateAndTimeStartActual = prefix + viewTaskPage.getFieldDate().getText();
        Assert.assertTrue("Wrong date and time is shown: " + dateAndTimeStartActual, dateAndTimeStartActual.matches(dateAndTimeStartExpected));

        // Click 'Edit page' link for inline editing again, add an end date and time
        clickEditPage();
        inlineEditTaskPage.clickFieldDateInline();
        inlineEditTaskPage.selectCheckboxUseStartAndEndDate();
        selectEndDateInline(1); // select end date > start date
        String endDate = inlineEditTaskPage.getEndDate(); // get end date
        String endTime = enterEndTime(inlineEditTaskPage.getFieldEndTime(), 1); // enter end time
        // Submit changes, check start/end date and time are shown correctly on ViewTask page
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        prefix = viewTaskPage.getPrefix(); // get date prefix
        String dateAndTimeExpected = "From " + startDate + ".*" + startTime +
                " to " + endDate + ".*" + endTime;
        String dateAndTimeActual = prefix + viewTaskPage.getFieldDate().getText();  // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong start/end date and time are shown: " + dateAndTimeActual, dateAndTimeActual.matches(dateAndTimeExpected));
    }

    @Test
    public void testAddImageToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Task resources' with the proper button
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Image Edit page from the Task and update it
        String urlViewTaskPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewTaskPage.getFieldMaterial());
        openMaterialEditPage(image, node);
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle); // new title
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image3Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        editImagePage.clickButtonSave();

        // Check that the new Image title is displayed on ViewTask page
        webDriver.get(urlViewTaskPage);
        String titleActual = viewTaskPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(imageTitle));
    }

    @Test // PILOT-1326
    public void testRemoveImageFromTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Task resources' with the proper button
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewTask page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test
    public void testAddVideoToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Task resources' with the proper button
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Video Edit page from the Task and update it
        String urlViewTaskPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewTaskPage.getFieldMaterial());
        openMaterialEditPage(video, node);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle); // new title
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlYouTube);
        waitUntilElementIsVisible(editVideoPage.getLinkRemoveMedia());
        editVideoPage.clickButtonSave();

        // Check that the new Video title is displayed on ViewTask page
        webDriver.get(urlViewTaskPage);
        String titleActual = viewTaskPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(videoTitle));
    }

    @Test // PILOT-1326
    public void testRemoveVideoFromTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Task resources' with the proper button
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewTask page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test
    public void testAddPresentationToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Task resources' with the proper button
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Presentation Edit page from the Task and update it
        String urlViewTaskPage = webDriver.getCurrentUrl();
        waitUntilElementIsVisible(viewTaskPage.getFieldMaterial());
        openMaterialEditPage(presentation, node);
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        editPresentationPage.clickButtonSave();

        // Check that the new Presentation title is displayed on ViewTask page
        webDriver.get(urlViewTaskPage);
        String titleActual = viewTaskPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(presentationTitle));
    }

    @Test // PILOT-1326
    public void testRemovePresentationFromTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Task resources' with the proper button
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewTask page after saving
        clickEditPage();
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        checkMaterialIsRemoved(node);
    }

    @Test // PILOT-1274
    public void testAddExternalLinkToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that correct title (url) of link is displayed on View page after saving
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        String titleActual = viewTaskPage.getLinkExternalTaskResources().getText(); // bug !!!!!!!!!
        String urlActual = viewTaskPage.getLinkExternalTaskResources().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.equals(linkUrl));
    }

    @Test // PILOT-1274
    public void testAdd2ndLinkToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();

        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Add the 2nd link with inline editing
        clickEditPage();
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        add2ndLinkMaterial(linkTitle2, linkUrl2); // select 'Open URL in a New Window' option here

        // Save changes, check that both titles of the links are displayed on the View page
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewTaskPage.getLinkExternalTaskResources());  // bug !!!!!!!!!
        WebElement link1 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle + "')]")); // the 1st link
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on View page.",link1.isDisplayed());
        WebElement link2 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle2 + "')]")); // the 2nd link
        Assert.assertTrue("The link with title '" + linkTitle2 + "' is not displayed on View page.", link2.isDisplayed());
        // Click the 2nd added Link, check that it opens a window in a new browser tab
        viewMaterialInAnotherBrowserTab(link2);
    }

    @Test // PILOT-1274
    public void testEditLinkInTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Edit the Link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        linksPopupInlineEditingPage.fillFieldUrl(linkUrl2); // url

        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewTaskPage.getLinkExternalTaskResources());

        // Check that the Link has correct title and url
        String titleActual = viewTaskPage.getLinkExternalTaskResources().getText(); // bug !!!!!!!!!
        String urlActual = viewTaskPage.getLinkExternalTaskResources().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle2));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.contains(linkUrl2));
    }

    @Test
    public void testRemoveLinkFromTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that the Link with defined title is displayed on View page after saving
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        String xpathLink = getLinkXpath(linkTitle);
        List<WebElement> links = webDriver.findElements(By.xpath(xpathLink));
        webDriver.navigate().refresh();
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on the View page.", links.size() == 1);

        // Open Links popup and remove the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.getFieldTitle().clear();
        linksPopupInlineEditingPage.getFieldUrl().clear();
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that no link with defined title is displayed on the View page
        links = webDriver.findElements(By.xpath(xpathLink));
        Assert.assertTrue("The link with title '" + linkTitle + "' is displayed on the View page.", links.size() == 0);
    }

    @Test // PILOT-1274
    public void testAddFileToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);

        // Check that correct title is displayed on View page after saving
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        WebElement fieldFile = viewTaskPage.getLinkDownloadedTaskResources(); // bug !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle));
        // Check that the file can be downloaded
        viewTaskPage.clickLinkDownloadedTaskResources();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1274
    public void testAdd2ndFileToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the file can be downloaded
        viewTaskPage.clickLinkDownloadedTaskResources(); // bug !!!!!!!!
        waitUntilFileDownloaded();

        // Add the 2nd file with inline editing to TaskResources
        clickEditPage();
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        add2ndFileMaterial(fileTitle2, pptxFilePath);

        // Save changes, check that both titles of the files are displayed on the View page
        inlineEditTaskPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // The 1st file
        String xpathFile1 = getFileXpath(fileTitle);
        String titleActual1 = webDriver.findElement(By.xpath(xpathFile1)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle + "' is not displayed on View page.", titleActual1.equals(fileTitle));
        // The 2nd file
        String xpathFile2 = getFileXpath(fileTitle2);
        String titleActual2 = webDriver.findElement(By.xpath(xpathFile2)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle2 + "' is not displayed on View page.", titleActual2.equals(fileTitle2));
    }

    @Test // PILOT-1274
    public void testEditFileInTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup and change data
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.fillFieldTitle(fileTitle2); // title
        downloadsPopupInlineEditingPage.clickButtonRemove();
        uploadFileToDownloadsPopup(xlsxFilePath); // file
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the new title is displayed on View page
        WebElement fieldFile = viewTaskPage.getLinkDownloadedTaskResources(); // bug !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle2));
        // Check that the new file can be downloaded
        viewTaskPage.clickLinkDownloadedTaskResources();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1276
    public void testRemoveFileFromTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup, remove fieldset
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.clickButtonRemoveFieldSet();
        viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that no file with defined title is displayed on the View page
        String xpathFile = getFileXpath(fileTitle);
        List<WebElement> files = webDriver.findElements(By.xpath(xpathFile)); // bug !!!!!!
        Assert.assertTrue("The file with title '" + fileTitle + "' is displayed on the View page.", files.size() == 0);
    }

    @Test
    public void testAddAllMaterialsToTaskResourcesWithInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add an image
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int nodeImage = checkMaterialTemplateIsAdded(image); // get image node
        // Add a video
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int nodeVideo = checkMaterialTemplateIsAdded(video); // get video node
        // Add a presentation
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int nodePresentation = checkMaterialTemplateIsAdded(presentation); // get presentation node
        // Add a link
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditTaskPage.clickButtonSaveInline();
        // Add a file
        clickEditPage();
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewTaskPage.getFieldMaterial());

        // Check all materials are displayed on ViewTask page
        checkMaterialIsDisplayed(nodeImage);
        checkMaterialIsDisplayed(nodeVideo);
        checkMaterialIsDisplayed(nodePresentation);
        checkLinkIsDisplayed(linkTitle);
        checkFileIsDisplayed(fileTitle);
    }

    @Test // PILOT-1274
    public void testCancelInlineEditingOfTaskWithCloseButton() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to TaskResources with inline editing
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditTaskPage.clickButtonCloseInline(); // click Close button for inline editing
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveChanges(); // click Save on popup
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewTaskPage.getLinkExternalTaskResources()); // bug !!!!!!!!!
        // Check the link is displayed on View page
        checkLinkIsDisplayed(linkTitle);

        // Edit the title of the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        // Click Close button for inline editing
        inlineEditTaskPage.clickButtonCloseInline();
        inlineEditTaskPage.clickButtonDiscardChanges(); // discard changes
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed
        waitUntilElementIsVisible(viewTaskPage.getLinkExternalTaskResources());

        // Check that non-updated title is displayed on View page
        checkLinkIsDisplayed(linkTitle);
    }

    @Test
    public void testCheckValidationForTaskInlineEditing() throws Exception {
        // Create Task template
        createTaskTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Clear the fields: Title, WhatYouNeedToDo, WhatsThisFor, LearningObjectives
        InlineEditTaskPage inlineEditTaskPage = new InlineEditTaskPage(webDriver, pageLocation);
        inlineEditTaskPage.clearFieldTitleInline();
        inlineEditTaskPage.clearFieldWhatYouNeedToDoInline();
        inlineEditTaskPage.clearFieldWhatsThisForInline();
        inlineEditTaskPage.clearFieldLearningObjectivesInline();
        inlineEditTaskPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // Check that inline editing is still active, error messages for the fields are displayed
        Assert.assertTrue("The button Save is not displayed for inline editing.",
                inlineEditTaskPage.getButtonSaveInline().isDisplayed());
        Assert.assertTrue("Error message for Title field is not displayed.",
                inlineEditTaskPage.getErrorMessageFieldTitle().isDisplayed());
        Assert.assertTrue("Error message for 'What you need to do' field is not displayed.",
                inlineEditTaskPage.getErrorMessageFieldWhatYouNeedToDo().isDisplayed());
        Assert.assertTrue("Error message for 'Whats this for' field is not displayed.",
                inlineEditTaskPage.getErrorMessageFieldWhatsThisFor().isDisplayed());
        Assert.assertTrue("Error message for 'Learning objectives' field is not displayed.",
                inlineEditTaskPage.getErrorMessageFieldLearningObjectives().isDisplayed());

        // Fill the fields, submit changes
        inlineEditTaskPage.fillFieldTitleInline(taskTitle);
        inlineEditTaskPage.fillFieldWhatYouNeedToDoWithSource(whatDoYouNeedToDoText);
        inlineEditTaskPage.fillFieldWhatsThisForWithSource(whatIsThisForText);
        inlineEditTaskPage.fillFieldLearningObjectivesWithSource(learningObjectivesText);
        ViewTaskPage viewTaskPage = inlineEditTaskPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewTaskPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check that the Task is updated
        String titleActual = viewTaskPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewTask page: " + titleActual,
                titleActual.contains(taskTitle)); // Theory title
        String whatYouNeedToDoActual = viewTaskPage.getFieldWhatYouNeedToDo().getText();
        Assert.assertTrue("Wrong WhatDoYouNeedToDo text is displayed on ViewTheory page: " + whatYouNeedToDoActual,
                whatYouNeedToDoActual.equals(whatDoYouNeedToDoText)); // WhatYouNeedToDo field
        String whatsThisForActual = viewTaskPage.getFieldWhatThisFor().getText();
        Assert.assertTrue("Wrong WhatsThisFor text is displayed on ViewTheory page: " + whatsThisForActual,
                whatsThisForActual.equals(whatIsThisForText)); // WhatsThisFor field
        String learningObjectivesActual = viewTaskPage.getFieldLearningObjectives().getText();
        Assert.assertTrue("Wrong LearningObjectives text is displayed on ViewTheory page: " + learningObjectivesActual,
                learningObjectivesActual.equals(learningObjectivesText)); // LearningObjectives field
    }


// 'Media page' inline editing

    @Test
    public void testEditMediaPageWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();
        // Edit Title
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.fillFieldTitleInline(mediaPageTitle);
        // Edit length field
        selectLengthMediaPage(hours, minutes);
        // Edit VideoSummary text
        inlineEditMediaPage.fillFieldVideoSummaryInline(videoSummaryText);
        // Edit ContextualInformation text
        inlineEditMediaPage.fillFieldContextualInformationInline(contextualInformationText);
        // Save changes
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check the MediaPage is updated
        String titleActual = viewMediaPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewMedia page: " + titleActual,
                titleActual.contains(mediaPageTitle)); // MediaPage title
        checkLengthFieldValue(viewMediaPage.getFieldLength()); // Length field
        String videoSummaryActual = viewMediaPage.getFieldVideoSummary().getText();
        Assert.assertTrue("Wrong VideoSummary text is displayed on ViewMedia page: " + videoSummaryActual,
                videoSummaryActual.equals(videoSummaryText)); // VideoSummary field
        viewMediaPage.clickLinkInfo(); // open 'Contextual information' popup
        String contextualInformationActual = viewMediaPage.getFieldContextualInformation().getText();
        Assert.assertTrue("Wrong ContextualInformation text is displayed on ViewMedia page: " + contextualInformationText,
                contextualInformationActual.equals(contextualInformationText)); // ContextualInformation field
    }

    @Test // PILOT-1266
    public void testChangeDateForMediaPageWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup, select an end date
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickFieldDateInline();
        String startDate = inlineEditMediaPage.getStartDate(); // get start date
        inlineEditMediaPage.selectCheckboxUseStartAndEndDate(); // use 'start and end date'
        selectDateDiffFormat(inlineEditMediaPage.getFieldEndDate(), 1);
        String endDate = inlineEditMediaPage.getEndDate(); // get end date

        // Check that correct date is displayed on ViewMedia page
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        String prefix = viewMediaPage.getPrefix(); // get date prefix
        String dateActual = prefix + viewMediaPage.getFieldDate().getText();
        String dateExpected = "From " + startDate + " to " + endDate; // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong date is displayed: " + dateActual, dateActual.equals(dateExpected));
    }

    @Test // PILOT-1266
    public void testAddTimeToMediaPageWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Open 'Due date' popup
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickFieldDateInline();
        String startDate = inlineEditMediaPage.getStartDate(); // get start date
        // Enter start time
        inlineEditMediaPage.selectCheckboxShowTime();
        String startTime = enterStartTime(inlineEditMediaPage.getFieldStartTime(), 0);
        // Submit inline editing, check date and time are shown correctly on ViewMedia page
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        String prefix = viewMediaPage.getPrefix(); // get date prefix
        String dateAndTimeStartExpected = "Viewed by " + startDate + ".*" + startTime;
        String dateAndTimeStartActual = prefix + viewMediaPage.getFieldDate().getText();
        Assert.assertTrue("Wrong date and time is shown: " + dateAndTimeStartActual, dateAndTimeStartActual.matches(dateAndTimeStartExpected));

        // Click 'Edit page' link for inline editing again, add an end date and time
        clickEditPage();
        inlineEditMediaPage.clickFieldDateInline();
        inlineEditMediaPage.selectCheckboxUseStartAndEndDate();
        selectEndDateInline(1); // select end date > start date
        String endDate = inlineEditMediaPage.getEndDate(); // get end date
        String endTime = enterEndTime(inlineEditMediaPage.getFieldEndTime(), 1); // enter end time
        // Submit changes, check start/end date and time are shown correctly on ViewMedia page
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        prefix = viewMediaPage.getPrefix(); // get date prefix
        String dateAndTimeExpected = "From " + startDate + ".*" + startTime +
                " to " + endDate + ".*" + endTime;
        String dateAndTimeActual = prefix + viewMediaPage.getFieldDate().getText();  // bug !!!!!!!!!!!!!
        Assert.assertTrue("Wrong start/end date and time are shown: " + dateAndTimeActual, dateAndTimeActual.matches(dateAndTimeExpected));
    }

    @Test // PILOT-1274
    public void testAddImageToMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Course material' with the proper button
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Image Edit page from the MediaPage and update it
        String urlViewMediaPage = webDriver.getCurrentUrl();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup       bug !!!!!!!!!!!!!!
        waitUntilElementIsVisible(viewMediaPage.getFieldMaterial());
        openMaterialEditPage(image, node);
        EditImagePage editImagePage = new EditImagePage(webDriver, pageLocation);
        editImagePage.fillFieldTitle(imageTitle); // new title
        SelectImagePage selectImagePage = editImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image4Path);
        Assert.assertTrue(editImagePage.getLinkRemoveMedia().isDisplayed());
        editImagePage.clickButtonSave();

        // Check that the new Image title is displayed on ViewMedia page
        webDriver.get(urlViewMediaPage);
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        String titleActual = viewMediaPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(imageTitle));
    }

    @Test // PILOT-1326
    public void testRemoveImageFromMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Image template to 'Course material' with the proper button
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(image); // get material node
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewMedia page after saving
        clickEditPage();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        checkMaterialIsRemoved(node);
    }

    @Test // PILOT-1274
    public void testAddVideoToMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Course material' with the proper button
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Video Edit page from the MediaPage and update it
        String urlViewMediaPage = webDriver.getCurrentUrl();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup       bug !!!!!!!!!!!!!!
        waitUntilElementIsVisible(viewMediaPage.getFieldMaterial());
        openMaterialEditPage(video, node);
        EditVideoPage editVideoPage = new EditVideoPage(webDriver, pageLocation);
        editVideoPage.fillFieldTitle(videoTitle); // new title
        SelectVideoPage selectVideoPage = editVideoPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(urlVimeo);
        waitUntilElementIsVisible(editVideoPage.getLinkRemoveMedia());
        editVideoPage.clickButtonSave();

        // Check that the new Video title is displayed on ViewMedia page
        webDriver.get(urlViewMediaPage);
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        String titleActual = viewMediaPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(videoTitle));
    }

    @Test // PILOT-1326
    public void testRemoveVideoFromMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Video template to 'Course material' with the proper button
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(video); // get material node
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewMedia page after saving
        clickEditPage();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        checkMaterialIsRemoved(node);
    }

    @Test // PILOT-1274
    public void testAddPresentationToMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Course material' with the proper button
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open the Presentation Edit page from the MediaPage and update it
        String urlViewMediaPage = webDriver.getCurrentUrl();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup       bug !!!!!!!!!!!!!!
        waitUntilElementIsVisible(viewMediaPage.getFieldMaterial());
        openMaterialEditPage(presentation, node);
        EditPresentationPage editPresentationPage = new EditPresentationPage(webDriver, pageLocation);
        editPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = editPresentationPage.clickLinkSelectMedia();
        selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(editPresentationPage.getLinkRemoveMedia().isDisplayed());
        editPresentationPage.clickButtonSave();

        // Check that the new Presentation title is displayed on ViewMedia page
        webDriver.get(urlViewMediaPage);
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        String titleActual = viewMediaPage.getFieldMaterial().getText();
        Assert.assertTrue("Wrong title of material is displayed on View page.", titleActual.equals(presentationTitle));
    }

    @Test // PILOT-1326
    public void testRemovePresentationFromMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add Presentation template to 'Course material' with the proper button
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int node = checkMaterialTemplateIsAdded(presentation); // get material node
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Remove the material, check that it's not displayed on ViewMedia page after saving
        clickEditPage();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        removeMaterial(node); // bug !!!!!!!!!!!!!
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        checkMaterialIsRemoved(node);
    }

    @Test // PILOT-1274
    public void testAddExternalLinkToMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that correct title (url) of link is displayed on View page after saving
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup          bug !!!!!!!!!
        String titleActual = viewMediaPage.getLinkExternalCourseMaterial().getText();
        String urlActual = viewMediaPage.getLinkExternalCourseMaterial().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.equals(linkUrl));
    }

    @Test // PILOT-1274
    public void testAdd2ndLinkToMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();

        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Add the 2nd link with inline editing
        clickEditPage();
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        add2ndLinkMaterial(linkTitle2, linkUrl2); // select 'Open URL in a New Window' option here

        // Save changes, check that both titles of the links are displayed on the View page
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup            bug !!!!!!!!!
        waitUntilElementIsVisible(viewMediaPage.getLinkExternalCourseMaterial());
        WebElement link1 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle + "')]")); // the 1st link
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on View page.",link1.isDisplayed());
        WebElement link2 = webDriver.findElement(By.xpath("//a[contains(text(),'" + linkTitle2 + "')]")); // the 2nd link
        Assert.assertTrue("The link with title '" + linkTitle2 + "' is not displayed on View page.", link2.isDisplayed());
        // Click the 2nd added Link, check that it opens a window in a new browser tab
        viewMaterialInAnotherBrowserTab(link2);
    }

    @Test  // PILOT-1274
    public void testEditLinkInMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Edit the Link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        linksPopupInlineEditingPage.fillFieldUrl(linkUrl2); // url

        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup        bug !!!!!!!!!
        waitUntilElementIsVisible(viewMediaPage.getLinkExternalCourseMaterial());

        // Check that the Link has correct title and url
        String titleActual = viewMediaPage.getLinkExternalCourseMaterial().getText();
        String urlActual = viewMediaPage.getLinkExternalCourseMaterial().getAttribute("href");
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(linkTitle2));
        Assert.assertTrue("The Link material has wrong URL on View page: " + urlActual, urlActual.contains(linkUrl2));
    }

    @Test // PILOT-1274
    public void testRemoveLinkFromMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);

        // Check that the Link with defined title is displayed on View page after saving
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup         bug !!!!!!!!
        String xpathLink = getLinkXpath(linkTitle);
        List<WebElement> links = webDriver.findElements(By.xpath(xpathLink));
        Assert.assertTrue("The link with title '" + linkTitle + "' is not displayed on the View page.", links.size() == 1);

        // Open Links popup and remove the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.getFieldTitle().clear();
        linksPopupInlineEditingPage.getFieldUrl().clear();
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check that the Links button is not displayed on the View page
        List<WebElement> buttons = webDriver.findElements(By.linkText("Links"));
        Assert.assertTrue("The button Links is displayed on the View page.", buttons.size() == 0); // bug !!!!!!!!
    }

    @Test // PILOT-1274
    public void testAddFileToMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);

        // Check that correct title is displayed on View page after saving
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        WebElement fieldFile = viewMediaPage.getLinkDownloadedCourseMaterial(); // bug !!!!!!!!!
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle));
        // Check that the file can be downloaded
        viewMediaPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
    }

    @Test // PILOT-1274
    public void testAdd2ndFileToMediaPageCourseMaterial() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the file can be downloaded
        inlineEditMediaPage.clickLinkLinks(); // open Links popup      bug !!!!!!!!
        viewMediaPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();

        // Add the 2nd file with inline editing to CourseMaterial
        clickEditPage();
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        add2ndFileMaterial(fileTitle2, pptxFilePath);

        // Save changes, check that both titles of the files are displayed on the View page
        inlineEditMediaPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        // The 1st file
        String xpathFile1 = getFileXpath(fileTitle);
        String titleActual1 = webDriver.findElement(By.xpath(xpathFile1)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle + "' is not displayed on View page.", titleActual1.equals(fileTitle));
        // The 2nd file
        String xpathFile2 = getFileXpath(fileTitle2);
        String titleActual2 = webDriver.findElement(By.xpath(xpathFile2)).getText();
        Assert.assertTrue("The file link with title '" + fileTitle2 + "' is not displayed on View page.", titleActual2.equals(fileTitle2));
    }

    @Test // PILOT-1274
    public void testEditFileInMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, pdfFilePath);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup and change data
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.fillFieldTitle(fileTitle2); // title
        downloadsPopupInlineEditingPage.clickButtonRemove();
        uploadFileToDownloadsPopup(xlsxFilePath); // file
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Check that the new title is displayed on View page
        inlineEditMediaPage.clickLinkLinks(); // open Links popup      bug !!!!
        WebElement fieldFile = viewMediaPage.getLinkDownloadedCourseMaterial();
        String titleActual = fieldFile.getText();
        Assert.assertTrue("Wrong title of material is displayed on View page: " + titleActual, titleActual.equals(fileTitle2));
        // Check that the new file can be downloaded
        viewMediaPage.clickLinkDownloadedCourseMaterial();
        waitUntilFileDownloaded();
    }

    @Test  // PILOT-1274
    public void testRemoveFileFromMediaPageCourseMaterialWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a DownloadableFile to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        // Open Downloads popup, remove fieldset
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        DownloadsPopupInlineEditingPage downloadsPopupInlineEditingPage = openDownloadsPopup();
        downloadsPopupInlineEditingPage.clickButtonRemoveFieldSet();
        viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        webDriver.navigate().refresh();

        // Check that the Links button is not displayed on the View page
        List<WebElement> buttons = webDriver.findElements(By.linkText("Links"));
        Assert.assertTrue("The button Links is displayed on the View page.", buttons.size() == 0); // bug !!!!!!!!
    }

    @Test // PILOT-1274
    public void testAddAllMaterialsToMediaPageWithInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add an image
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(imageType);
        waitUntilMaterialIsAdded();
        int nodeImage = checkMaterialTemplateIsAdded(image); // get image node
        // Add a video
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(videoType);
        waitUntilMaterialIsAdded();
        int nodeVideo = checkMaterialTemplateIsAdded(video); // get video node
        // Add a presentation
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(presentationType);
        waitUntilMaterialIsAdded();
        int nodePresentation = checkMaterialTemplateIsAdded(presentation); // get presentation node
        // Add a link
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditMediaPage.clickButtonSaveInline();
        // Add a file
        clickEditPage();
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(downloadsType);
        addDownloadedFileMaterial(fileTitle, docxFilePath);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        waitUntilElementIsVisible(viewMediaPage.getFieldMaterial());

        // Check all materials are displayed on ViewTheory page
        checkMaterialIsDisplayed(nodeImage);
        checkMaterialIsDisplayed(nodeVideo);
        checkMaterialIsDisplayed(nodePresentation);
        checkLinkIsDisplayed(linkTitle);
        checkFileIsDisplayed(fileTitle);
    }

    @Test // PILOT-1274
    public void testCancelInlineEditingOfMediaPageWithCloseButton() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Add a Link to CourseMaterial with inline editing
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clickButtonAddCourseMaterial();
        selectMaterialType(linkType);
        addLinkMaterial(linkTitle, linkUrl);
        inlineEditMediaPage.clickButtonCloseInline(); // click Close button for inline editing
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveChanges(); // click Save on popup
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup        bug !!!!!!!!!
        waitUntilElementIsVisible(viewMediaPage.getLinkExternalCourseMaterial());
        // Check the link is displayed on View page
        checkLinkIsDisplayed(linkTitle);

        // Edit the title of the link
        clickEditPage();
        Thread.sleep(1000); // test fails without the timeout
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        LinksPopupInlineEditingPage linksPopupInlineEditingPage = openLinksPopup();
        linksPopupInlineEditingPage.fillFieldTitle(linkTitle2); // title
        // Click Close button for inline editing
        inlineEditMediaPage.clickButtonCloseInline();
        waitUntilPopupIsClosed(); // wait until 'Course material' popup is closed
        inlineEditMediaPage.clickButtonDiscardChanges(); // discard changes
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed
        inlineEditMediaPage.clickLinkLinks(); // open Links popup
        waitUntilElementIsVisible(viewMediaPage.getLinkExternalCourseMaterial());

        // Check that non-updated title is displayed on View page
        checkLinkIsDisplayed(linkTitle);
    }

    @Test
    public void testCheckValidationForMediaPageInlineEditing() throws Exception {
        // Create MediaPage template
        createMediaPageTemplate();
        // Click 'Edit page' link for inline editing
        clickEditPage();

        // Clear the fields: Title, VideoSummary, ContextualInformation
        InlineEditMediaPage inlineEditMediaPage = new InlineEditMediaPage(webDriver, pageLocation);
        inlineEditMediaPage.clearFieldTitleInline();
        inlineEditMediaPage.clearFieldVideoSummaryInline();
        inlineEditMediaPage.clearFieldContextualInformationInline();
        inlineEditMediaPage.clickButtonSaveInline();
        waitUntilMaterialIsAdded();
        // Check that inline editing is still active, error messages for the fields are displayed
        Assert.assertTrue("The button Save is not displayed for inline editing.",
                inlineEditMediaPage.getButtonSaveInline().isDisplayed());
        Assert.assertTrue("Error message for Title field is not displayed.",
                inlineEditMediaPage.getErrorMessageFieldTitle().isDisplayed());
        Assert.assertTrue("Error message for 'Video summary' field is not displayed.",
                inlineEditMediaPage.getErrorMessageFieldVideoSummary().isDisplayed());
        Assert.assertTrue("Error message for 'Contextual information' field is not displayed.",
                inlineEditMediaPage.getErrorMessageFieldContextualInformation().isDisplayed());

        // Fill the fields, submit changes
        inlineEditMediaPage.fillFieldTitleInline(mediaPageTitle);
        inlineEditMediaPage.fillFieldVideoSummaryWithSource(videoSummaryText);
        inlineEditMediaPage.fillFieldContextualInformationWithSource(contextualInformationText);
        ViewMediaPage viewMediaPage = inlineEditMediaPage.clickButtonSaveInline();
        waitUntilElementIsVisible(viewMediaPage.getLinkEdit()); // link 'Edit page' is displayed

        // Check that the MediaPage is updated
        String titleActual = viewMediaPage.getFieldTitle().getText();
        Assert.assertTrue("Wrong title is displayed on ViewMedia page: " + titleActual,
                titleActual.contains(mediaPageTitle)); // MediaPage title
        String videoSummaryActual = viewMediaPage.getFieldVideoSummary().getText();
        Assert.assertTrue("Wrong VideoSummary text is displayed on ViewMedia page: " + videoSummaryActual,
                videoSummaryActual.equals(videoSummaryText)); // VideoSummary field
        inlineEditMediaPage.clickLinkInfo(); // open ContextualInformation popup
        String contextualInformationActual = viewMediaPage.getFieldContextualInformation().getText();
        Assert.assertTrue("Wrong ContextualInformation text is displayed on ViewMedia page: " + contextualInformationActual,
                contextualInformationActual.equals(contextualInformationText)); // ContextualInformation field
    }






























}
