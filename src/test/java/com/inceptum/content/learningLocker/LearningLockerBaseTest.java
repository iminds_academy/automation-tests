package com.inceptum.content.learningLocker;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.content.TinCanContentBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.CreateImagePage;
import com.inceptum.pages.CreatePresentationPage;
import com.inceptum.pages.CreateVideoPage;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.SelectImagePage;
import com.inceptum.pages.SelectPresentationPage;
import com.inceptum.pages.SelectVideoPage;
import com.inceptum.pages.TinCanApiPage;
import com.inceptum.pages.ViewLiveSessionPage;
import com.inceptum.pages.ViewTaskPage;
import com.inceptum.pages.ViewTheoryPage;
import com.inceptum.pages.learningLocker.AddFiltersLLPage;
import com.inceptum.pages.learningLocker.AdminDashboardLLPage;
import com.inceptum.pages.learningLocker.AllUsersLLPage;
import com.inceptum.pages.learningLocker.ClassesLLPage;
import com.inceptum.pages.learningLocker.ClientsLLPage;
import com.inceptum.pages.learningLocker.CourseContentLLPage;
import com.inceptum.pages.learningLocker.CourseDetailedLLPage;
import com.inceptum.pages.learningLocker.CreateLrsPage;
import com.inceptum.pages.learningLocker.LoginFormLLPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.pages.learningLocker.MovieDetailedLLPage;
import com.inceptum.pages.learningLocker.MoviesLLPage;
import com.inceptum.pages.learningLocker.QuizzesLLPage;
import com.inceptum.pages.learningLocker.UserDetailedLLPage;
import com.inceptum.pages.learningLocker.UsersLLPage;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerBaseTest extends TinCanContentBaseTest {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected static final String urlLL = "http://ocean4.uwsoftware.be";
    protected static final String email = "nikosverschore@gmail.com";
    protected static final String password = "1234";
    protected static final String nameCreateLRS = "Create an LRS";
    protected static final String titleLRS = "titleLRS";
    protected static final String descriptionLRS = "descriptionLRS";
    protected static String urlLRS = "";
    protected static String chapterLiveSession = "";
    protected static String nodeLiveSession = "";
    protected static String chapterTask = "";
    protected static String nodeTask = "";
    protected static String chapterTheory = "";
    protected static String nodeTheory = "";
    protected static String chapterInfoPage = "";
    protected static String nodeInfoPage = "";
    protected static String chapterMediaPage = "";
    protected static String nodeMediaPage = "";
    protected String chapterCourse = "Course";
    protected static String nodeImage = "";
    protected static String nodeImageDuplicate = "";
    protected static String nodeVideoYoutube = "";
    protected static String nodeVideoVimeo = "";
    protected static String nodeVideoSeparated = "";
    protected static String nodePresentation = "";
    protected static String nodePresentationSeparated = "";
    protected static String nodeQuiz = "";
    protected final String best = "BEST";
    protected final String worst = "WORST";
    protected final String filterUser = "user";
    protected final String filterDate = "date";
    protected final String filterPage = "page";
    protected static String vimeoURL = "https://vimeo.com/79676216"; // 20 sec


     /* ----- METHODS ----- */

    public List<String> getClientApiInfo() throws Exception {
        ClientsLLPage clientsLLPage = new ClientsLLPage(webDriver, pageLocation);
        String endpoint = clientsLLPage.getFieldEndpoint().getText().trim();
        String username = clientsLLPage.getFieldUsername().getText().trim();
        String password = clientsLLPage.getFieldPassword().getText().trim();
        return Arrays.asList(endpoint, username, password);
    }
    public void checkTinCanApiPageLL(List<String> apiInfo) throws Exception { // PILOT-1586 !!!
        // Open TinCanApi configuration page
        webDriver.get(websiteUrl.concat("/admin/config/services/tincanapi"));
        waitForPageToLoad();
        TinCanApiPage tinCanApiPage = new TinCanApiPage(webDriver, pageLocation);

        // Fill in TinCan API fields with current LRS info
        tinCanApiPage.fillFieldEndpoint(apiInfo.get(0));
        tinCanApiPage.fillFieldUser(apiInfo.get(1));
        tinCanApiPage.fillFieldPassword(apiInfo.get(2));

        // Check that Watchdog is enabled
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxLogServerResponse()); // is not enabled on production (should be selected)
        // 'Track anonymous users', 'Simplify statements IDs' are enabled on production (should be selected)
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxTrackAnonymousUsers());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxSimplifyStatementsIDs());
        // Check that correct content is enabled
        tinCanApiPage.clickLinkContentTypes();
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxClass());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxImage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxPeerAssignment()); // need to be selected in iMinds // PILOT-1586 !!!
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxPresentation());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxVideo());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxFullContent());

        tinCanApiPage.clickLinkIminds();
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxInfoPage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxLiveSession());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxMediaPage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxTaskPage());
        assertCheckboxIsSelected(tinCanApiPage.getCheckboxTheoryPage());
        // Save changes
        tinCanApiPage.clickButtonSaveConfiguration();
        assertMessage(tinCanApiPage.getMessage(), "messages status", ".*The configuration options have been saved.");
    }
    public String checkLrsIsCreated(String titleLRS) throws Exception {
        // Log in to Learning Locker
        AdminDashboardLLPage adminDashboardLLPage = loginToLL();
        // Check whether the LRS has already existed
        waitUntilElementIsVisible(adminDashboardLLPage.getFieldLRS());
        String xpathLRS = "//*[@class='dropdown hidden-xs']//a[text()=' " + titleLRS + "']";
        String script = "arguments[0].click();";
        List<WebElement> lrs = webDriver.findElements(By.xpath(xpathLRS));
        // Create a new LRS with the title if the LRS isn't existed yet
        if (lrs.size() == 0) {
            String xpathCreateLRS = "//*[@class='dropdown hidden-xs']//a[contains(text(), '" + nameCreateLRS + "')]";
            WebElement linkCreateLRS = webDriver.findElement(By.xpath(xpathCreateLRS));
            ((JavascriptExecutor) webDriver).executeScript(script, linkCreateLRS);
            waitForPageToLoad();
            CreateLrsPage createLrsPage = new CreateLrsPage(webDriver, pageLocation);
            createLrsPage.fillFieldTitle(titleLRS);
            createLrsPage.fillFieldDescription(descriptionLRS);
            createLrsPage.clickButtonSubmit();
        }
        // Open 'Main dashboard' page of the LRS
        WebElement linkLrs = webDriver.findElement(By.xpath(xpathLRS));
        ((JavascriptExecutor) webDriver).executeScript(script, linkLrs);
        waitForPageToLoad();
        return webDriver.getCurrentUrl();
    }
    public AdminDashboardLLPage loginToLL() throws Exception {
        // Open LL Login form
        webDriver.get(urlLL);
        LoginFormLLPage loginFormLLPage = new LoginFormLLPage(webDriver, pageLocation);
        // Log in
        loginFormLLPage.fillFieldEmail(email);
        loginFormLLPage.fillFieldPassword(password);
        loginFormLLPage.clickButtonSubmit();
        return new AdminDashboardLLPage(webDriver, pageLocation);
    }
    public void cleanLRS(String urlLRS) throws Exception {
        // Open LRS Dashboard page
        webDriver.get(urlLRS);
        waitForPageToLoad();
        // Click link 'Clean LRS'
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        ClassesLLPage classesLLPage = mainDashboardLLPage.clickLinkManageClasses();
        classesLLPage.clickLinkCleanLRS();
        // Check that 'Without class' is selected on MainDashboard page
        webDriver.get(urlLRS);
        waitForPageToLoad();
        List<WebElement> classes = mainDashboardLLPage.getFieldClass().findElements(By.xpath("./..//li/a"));
        Assert.assertTrue("Not only one option is in Classes list.", classes.size() == 1);
        WebElement option = mainDashboardLLPage.getFieldClass().findElement(By.xpath("./..//li/a"));
        Assert.assertTrue("Wrong option is shown in Classes field.",
                option.getAttribute("innerText").trim().equals("- Without class -")); // getText() method returns empty string here
        // Check that stats shows 0%
        checkGeneralStatsFromDashboardPage(0, 0, 0);
    }
    public void checkGeneralStatsFromCourseDetailedPage(int totalPageViews, int nOfUsersVisited, int percentOfUsersVisited) throws Exception{
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_pageviews div")));
        // Get 'Total page views'
        String totalPageViewsString = webDriver.findElement(By.cssSelector("#stat_pageviews div")).getText();
        int totalPageViewsActual = Integer.parseInt(totalPageViewsString);
        // Get '№ of users visited'
        String nOfUsersVisitedString = webDriver.findElement(By.cssSelector("#stat_users div")).getText();
        int nOfUsersVisitedActual = Integer.parseInt(nOfUsersVisitedString);
        // Get 'Users visited' %
        String percentOfUsersVisitedString = webDriver.findElement(By.cssSelector("#stat_users_percent div")).getText().replace("%", "");
        int percentOfUsersVisitedActual = Integer.parseInt(percentOfUsersVisitedString);
        Assert.assertTrue("General statistics is shown incorrectly on Course detailed page.",
                (totalPageViewsActual == totalPageViews) && (nOfUsersVisitedActual == nOfUsersVisited) &&
                        (percentOfUsersVisitedActual == percentOfUsersVisited));
    }
    public void checkGeneralStatsFromMovieDetailedPage(int timesFullyWatched, int nOfActiveUsers,
                                                       int percentOfUsersFullyWatched) throws Exception{
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_times_watched div")));
        // Wait until stats value is changed from '0' to actual value
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector("#stat_times_watched div"), "0"));

        // Get 'Times fully watched'
        String timesFullyWatchedString = webDriver.findElement(By.cssSelector("#stat_times_watched div")).getText();
        int timesFullyWatchedActual = Integer.parseInt(timesFullyWatchedString);
        // Get '№ of active users'
        String nOfActiveUsersString = webDriver.findElement(By.cssSelector("#stat_active_users div")).getText();
        int nOfActiveUsersActual = Integer.parseInt(nOfActiveUsersString);
        // Get 'Users fully watched' %
        String percentOfUsersFullyWatchedString = webDriver.findElement(By.cssSelector("#stat_users_watched_percent div"))
                .getText().replace("%", "");
        int percentOfUsersFullyWatchedActual = Integer.parseInt(percentOfUsersFullyWatchedString);
        Assert.assertTrue("General statistics is shown incorrectly on Movie detailed page.",
                (timesFullyWatchedActual == timesFullyWatched) && (nOfActiveUsersActual == nOfActiveUsers) &&
                        (percentOfUsersFullyWatchedActual == percentOfUsersFullyWatched));
    }
    public void checkGeneralStatsFromDashboardPage(int percentCourseProgress, int percentActiveUsers, int percentQuizzesCompleted) throws Exception{
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_pages_visited div")));
        // Get 'Course progress'
        String percentCourseProgressString = webDriver.findElement(By.cssSelector("#stat_pages_visited div")).getText().replace("%", "");
        int percentCourseProgressActual = Integer.parseInt(percentCourseProgressString);
        // Get 'Active users last week'
        String percentActiveUsersString = webDriver.findElement(By.cssSelector("#stat_active_users div")).getText().replace("%", "");
        int percentActiveUsersActual = Integer.parseInt(percentActiveUsersString);
        // Get 'Quizzes completed'
        String percentQuizzesCompletedString = webDriver.findElement(By.cssSelector("#stat_quizzes_completed div")).getText().replace("%", "");
        int percentQuizzesCompletedActual = Integer.parseInt(percentQuizzesCompletedString);
        Assert.assertTrue("General statistics is shown incorrectly on Dashboard page.",
                (percentCourseProgressActual == percentCourseProgress) && (percentActiveUsersActual == percentActiveUsers) &&
                        (percentQuizzesCompletedActual == percentQuizzesCompleted));
    }
    public void checkGeneralStatsFromCourseContentPage(int pageviews, int activeUsers, int activities) throws Exception{
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_pageviews div")));
        // Wait until stats value is changed from '0' to actual value
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector("#stat_pageviews div"), "0"));
        // Get 'Pageviews'
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_pageviews div")));
        String pageviewsString = webDriver.findElement(By.cssSelector("#stat_pageviews div")).getText();
        int pageviewsActual = Integer.parseInt(pageviewsString);
        // Get 'Active users'
        String activeUsersString = webDriver.findElement(By.cssSelector("#stat_active_users div")).getText();
        int activeUsersActual = Integer.parseInt(activeUsersString);
        // Get 'Activities'
        String activitiesString = webDriver.findElement(By.cssSelector("#stat_activities div")).getText();
        int activitiesActual = Integer.parseInt(activitiesString);
        Assert.assertTrue("General statistics is shown incorrectly on 'Course content' page.",
                (pageviewsActual == pageviews) && (activeUsersActual == activeUsers) && (activitiesActual == activities));
    }
    public void checkGeneralStatsFromUsersPage(int pageviews, int activeUsers, int activities) throws Exception{
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_pageviews div")));
        // Wait until stats value is changed from '0' to actual value
        wait.until(ExpectedConditions.invisibilityOfElementWithText(By.cssSelector("#stat_pageviews div"), "0"));
        // Get 'Pageviews'
        String pageviewsString = webDriver.findElement(By.cssSelector("#stat_pageviews div")).getText();
        int pageviewsActual = Integer.parseInt(pageviewsString);
        // Get 'Active users'
        String activeUsersString = webDriver.findElement(By.cssSelector("#stat_active_users div")).getText();
        int activeUsersActual = Integer.parseInt(activeUsersString);
        // Get 'Activities'
        String activitiesString = webDriver.findElement(By.cssSelector("#stat_activities div")).getText();
        int activitiesActual = Integer.parseInt(activitiesString);
        Assert.assertTrue("General statistics is shown incorrectly on 'Users' page.",
                (pageviewsActual == pageviews) && (activeUsersActual == activeUsers) && (activitiesActual == activities));
    }
    public void checkGeneralStatsFromUserDetailedPage(int percentOfCourseCompleted, int percentAverageQuizScore, int activities) throws Exception{
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_pages_completed div")));
        // Get 'Percent of course completed'
        String percentOfCourseCompletedString = webDriver.findElement(By.cssSelector("#stat_pages_completed div")).getText().replace("%", "");
        int percentOfCourseCompletedActual = Integer.parseInt(percentOfCourseCompletedString);
        // Get 'Average quiz score'
        String percentAverageQuizScoreString = webDriver.findElement(By.cssSelector("#stat_quizzes_completed div")).getText().replace("%", "");
        int percentAverageQuizScoreActual = Integer.parseInt(percentAverageQuizScoreString);
        // Get 'Activities'
        String activitiesString = webDriver.findElement(By.cssSelector("#stat_activities div")).getText();
        int activitiesActual = Integer.parseInt(activitiesString);
        Assert.assertTrue("General statistics is shown incorrectly on User Detailed page.",
                (percentOfCourseCompletedActual == percentOfCourseCompleted) && (percentAverageQuizScoreActual == percentAverageQuizScore)
                        && (activitiesActual == activities));
    }
    public void checkGeneralStatsFromMoviesPage(int timesFullyWatched, int activeUsers,
                                                int percentFullyWatched) throws Exception {
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_times_watched div")));
        // Get 'Times fully watched'
        String timesWatchedString = webDriver.findElement(By.cssSelector("#stat_times_watched div")).getText();
        int timesFullyWatchedActual = Integer.parseInt(timesWatchedString);
        // Get 'Active users'
        String activeUsersString = webDriver.findElement(By.cssSelector("#stat_active_users div")).getText();
        int activeUsersActual = Integer.parseInt(activeUsersString);
        // Get 'Percent of fully watched'
        String percentFullyWatchedString = webDriver.findElement(By.cssSelector("#stat_percent_watched div")).getText().replace("%", "");
        int percentFullyWatchedActual = Integer.parseInt(percentFullyWatchedString);
        Assert.assertTrue("General statistics is shown incorrectly on Movies page.",
                (timesFullyWatchedActual == timesFullyWatched) && (activeUsersActual == activeUsers)
                        && (percentFullyWatchedActual == percentFullyWatched));
    }
    public void checkGeneralStatsFromQuizzesPage(int timesQuizzesTaken, int activeUsers,
                                                int percentAvgQuizScore) throws Exception {
        // Wait until General Statistics is shown
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("#stat_quizzes_taken div")));
        // Get 'Quizzes taken' value
        String timesQuizzesTakenValue = webDriver.findElement(By.cssSelector("#stat_quizzes_taken div")).getText();
        int timesQuizzesTakenActual = Integer.parseInt(timesQuizzesTakenValue);
        // Get 'Active users'
        String activeUsersValue = webDriver.findElement(By.cssSelector("#stat_active_users div")).getText();
        int activeUsersActual = Integer.parseInt(activeUsersValue);
        // Get percent of 'Average quiz score'
        String percentAvgQuizScoreValue = webDriver.findElement(By.cssSelector("#stat_average_score div")).getText().replace("%", "");
        int percentAvgQuizScoreActual = Integer.parseInt(percentAvgQuizScoreValue);
        Assert.assertTrue("General statistics is shown incorrectly on Movies page.",
                (timesQuizzesTakenActual == timesQuizzesTaken) && (activeUsersActual == activeUsers)
                        && (percentAvgQuizScoreActual == percentAvgQuizScore));
    }
    public void clearCache(String urlLRS) throws Exception {
        // Open Users page
        webDriver.get(urlLRS.concat("/dashboard/user"));
        waitForPageToLoad();
        // Click icon 'clear cache'
        UsersLLPage usersLLPage = new UsersLLPage(webDriver, pageLocation);
        usersLLPage.clickIconClearCache();
    }
    public void checkUserIsShownOnDetailedPage(List<String> user) throws Exception {
        waitForPageToLoad();
        CourseDetailedLLPage courseDetailedLLPage = new CourseDetailedLLPage(webDriver, pageLocation);
        List<WebElement> users = courseDetailedLLPage.getTableMostActiveUsers().findElements(By.xpath(".//a/div/div[1][text()='"
                + user.get(0) + "']"));
        if(users.size() == 1) {
            return;
        }
        Assert.assertTrue("The user isn't displayed in 'Most active users' list or more than 1 user with such name is shown.",
                false);
    }
    public void checkUserIsShownOnMovieDetailedPageInTopList(List<String> user) throws Exception {
        waitForPageToLoad();
        MovieDetailedLLPage movieDetailedLLPage = new MovieDetailedLLPage(webDriver, pageLocation);
        List<WebElement> users = movieDetailedLLPage.getTableTopUsers().findElements(By.xpath(".//a/div/div[1]" +
                "[contains(text(), '" + user.get(0) + "')]"));
        if(users.size() == 1) {
            return;
        }
        Assert.assertTrue("The user isn't displayed in 'Top users' list or more than 1 user with such name is shown.",
                false);
    }
    public void checkUserIsShownInTopListWatchedMovies(List<String> user, int nOfWatchedMovies) throws Exception {
        waitForPageToLoad();
        // Find list of users with defined name
        MoviesLLPage moviesLLPage = new MoviesLLPage(webDriver, pageLocation);
        List<WebElement> users = moviesLLPage.getTableTopUsersWatched().findElements(By.xpath(".//a/div/div[1]" +
                "[contains(text(), '" + user.get(0) + "')]"));
        if(users.size() == 1) { // if the user is displayed in the list
            WebElement userLink = moviesLLPage.getTableTopUsersWatched().findElement(By.xpath(".//a/div/div[1]" +
                    "[contains(text(), '" + user.get(0) + "')]"));
            // Check that correct number of watched movies is displayed for the user
            String statsText = userLink.findElement(By.xpath("./../div[2]")).getText();
            String[] parts = statsText.split("Fully watched ");
            String nOfWatchedMoviesString = parts[1].split(" movies")[0];
            int nOfWatchedMoviesActual = Integer.parseInt(nOfWatchedMoviesString); // get int of 'number of movies watched'
            Assert.assertTrue("Incorrect number of fully watched movies is shown for the user.",
                    nOfWatchedMoviesActual == nOfWatchedMovies);
            return;
        }
        Assert.assertTrue("The user isn't displayed in 'Top users' list or more than 1 user with such name is shown.",
                false);
    }
    public void checkUserIsNotShownOnDetailedPage(List<String> user) throws Exception {
        waitForPageToLoad();
        CourseDetailedLLPage courseDetailedLLPage = new CourseDetailedLLPage(webDriver, pageLocation);
        List<WebElement> users = courseDetailedLLPage.getTableMostActiveUsers().findElements(By.xpath(".//a/div/div[1][text()='"
                + user.get(0) + "']"));
        if(users.size() == 0) {
            return;
        }
        Assert.assertTrue("The user is displayed in 'Most active users' list.", false);
    }
    public void checkNoRowInTable(WebElement table) throws Exception {
        waitForPageToLoad();
        List<WebElement> rows = table.findElements(By.cssSelector("tbody tr"));
        if(rows.size() == 0) {
            return;
        }
        Assert.assertTrue("Rows are displayed in the table.", false);
    }
    public WebElement checkUserIsDisplayedInAllUsersTable(List<String> user) throws Exception {
        waitForPageToLoad();
        AllUsersLLPage allUsersLLPage = new AllUsersLLPage(webDriver, pageLocation);
        List<WebElement> users = allUsersLLPage.getTableUsers().findElements(By.xpath("./tbody/tr//div[@class='content']" +
                "[contains(text(), '" + user.get(0) + "')]"));
        if(users.size() == 1) {
            WebElement userLink = allUsersLLPage.getTableUsers().findElement(By.xpath("./tbody/tr//div[@class='content']" +
                    "[contains(text(), '" + user.get(0) + "')]/ancestor::a"));
            return userLink;
        }
        Assert.assertTrue("The user isn't displayed in 'Most active users' list or more than 1 user with such name is shown.",
                false);
        return null;
    }
    public UserDetailedLLPage clickUserLinkFromMostActive(List<String> user) throws Exception {
        waitForPageToLoad();
        WebElement userLink = webDriver.findElement(By.xpath("//*[@id='list_active_users']//a/div/div[1][text()='" +
                user.get(0) + "']"));
        waitUntilElementIsClickable(userLink);
        clickItem(userLink, "The userLink could not be clicked from 'Most active users' list.");
        waitForPageToLoad();
        return new UserDetailedLLPage(webDriver, pageLocation);
    }
    public void checkPageIsShownInMostViewedList(String fullPageTitle, int nOfVisits, int nOfUsers) throws Exception {
        waitForPageToLoad();
        // Get list of elements with defined title
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        List<WebElement> pages = mainDashboardLLPage.getTableMostViewedPages().findElements(By.xpath(".//td[2]/a[text()='"
                + fullPageTitle + "']"));
        // If the page is displayed on MainDashboard - check its stats values
        if (pages.size() == 1) {
            WebElement page = mainDashboardLLPage.getTableMostViewedPages().findElement(By.xpath(".//td[2]/a[text()='"
                    + fullPageTitle + "']")); // the defined page link
            // Check 'nOfVisits'
            String nOfVisitsActual = page.findElement(By.xpath("./../../td[3]/div")).getText().trim();
            int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
            Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
            // Check 'nOfUsers'
            String nOfUsersActual = page.findElement(By.xpath("./../../td[4]/div")).getText().trim();
            int nOfUsersInt = Integer.parseInt(nOfUsersActual);
            Assert.assertTrue("'№ of users' has incorrect value for the page.", nOfUsersInt == nOfUsers);
            return;
        }
        Assert.assertTrue("The page isn't displayed on MainDashboard or № of pages with such title is more than 1.",
                false);
    }
    public void checkPageIsNotShownInMostViewedList(String pageTitle) throws Exception {
        waitForPageToLoad();
        // Get list of elements with defined title
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        List<WebElement> pages = mainDashboardLLPage.getTableMostViewedPages().findElements(By.xpath(".//td[2]/a[contains(text(), " +
                "'" + pageTitle + "')]"));
        Assert.assertTrue("The course page is displayed on MainDashboard.", pages.size() == 0);
    }
    public WebElement checkPageIsShownInAllPagesCourseContentList(String fullPageTitle, String chapter, int nOfVisits,
                                                            int nOfUsers, int percentOfUsers) throws Exception {
        waitForPageToLoad();
        // Get list of elements with defined title
        CourseContentLLPage courseContentLLPage = new CourseContentLLPage(webDriver, pageLocation);
        List<WebElement> pages = courseContentLLPage.getTableCourseContent().findElements(By.xpath(".//td[1]/a[text()='"
                + fullPageTitle + "']"));
        // If the page is displayed on CourseContent - check its stats values
        if (pages.size() == 1) {
            WebElement page = courseContentLLPage.getTableCourseContent().findElement(By.xpath(".//td[1]/a[text()='"
                    + fullPageTitle + "']"));
            // Check chapter
            String chapterActual = page.findElement(By.xpath("./../../preceding-sibling::tr[not(@style)][1]//b")).getText();
            Assert.assertTrue("Incorrect chapter is shown for the page.", chapterActual.equals(chapter));
            // Check 'nOfVisits'
            String nOfVisitsActual = page.findElement(By.xpath("./../../td[2]")).getText().trim();
            int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
            Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
            // Check 'nOfUsers'
            String nOfUsersActual = page.findElement(By.xpath("./../../td[3]")).getText().trim();
            int nOfUsersInt = Integer.parseInt(nOfUsersActual);
            Assert.assertTrue("'№ of users' has incorrect value for the page.", nOfUsersInt == nOfUsers);
            // Check '%users'
            String percentOfUsersActual = page.findElement(By.xpath("./../../td[4]")).getText().trim();
            String percentOfUsersValue = percentOfUsersActual.replace("%", "");
            int percentOfUsersInt = Integer.parseInt(percentOfUsersValue);
            Assert.assertTrue("'%users' has incorrect value for the page.", percentOfUsersInt == percentOfUsers);
            return page;
        }
        Assert.assertTrue("The page isn't displayed on CourseContent or № of pages with such title is more than 1.",
                false);
        return null;
    }
    public void checkPageIsGreyedOut(WebElement page) throws Exception { // the page should be a link
        waitForPageToLoad();
        String classValue = page.findElement(By.xpath("./ancestor::tr")).getAttribute("class"); // get class value
        Assert.assertTrue("The page isn't inactive.", classValue.contains("inactive")); // check if the page inactive
    }
    public WebElement checkDuplicateIsShownInAllPagesCourseContentList(String fullPageTitle, int nOfVisits, int nOfUsers, int percentOfUsers) throws Exception {
        waitForPageToLoad();
        List<WebElement> pages = webDriver.findElements(By.xpath("//a[text()='" +
                fullPageTitle + " ']/b[text()='(duplicate)']"));
        // If the page is displayed - check its stats values
        if (pages.size() == 1) {
            WebElement page = webDriver.findElement(By.xpath("//a[text()='" + fullPageTitle + " ']/b[text()='(duplicate)']/.."));
            // Check 'nOfVisits'
            String nOfVisitsActual = page.findElement(By.xpath("./../../td[2]")).getText().trim();
            int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
            Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
            // Check 'nOfUsers'
            String nOfUsersActual = page.findElement(By.xpath("./../../td[3]")).getText().trim();
            int nOfUsersInt = Integer.parseInt(nOfUsersActual);
            Assert.assertTrue("'№ of users' has incorrect value for the page.", nOfUsersInt == nOfUsers);
            // Check '%users'
            String percentOfUsersActual = page.findElement(By.xpath("./../../td[4]")).getText().trim();
            String percentOfUsersValue = percentOfUsersActual.replace("%", "");
            int percentOfUsersInt = Integer.parseInt(percentOfUsersValue);
            Assert.assertTrue("'%users' has incorrect value for the page.", percentOfUsersInt == percentOfUsers);
            return page;
        }
        Assert.assertTrue("The page isn't displayed on CourseContent or № of pages with such title is more than 1.",
                false);
        return null;
    }
    public void checkMaterialIsDuplicate(WebElement material) {
        waitUntilElementIsVisible(material);
        List<WebElement> duplicateMarks = material.findElements(By.xpath("./b[text()='(duplicate)']"));
        // Check that 'duplicate' mark is displayed for the material
        Assert.assertTrue("Material isn't displayed as a duplicate.", duplicateMarks.size() == 1);
    }
    public WebElement getDuplicate (String fullPageTitle, String chapter) throws Exception { // on User detailed page
        waitForPageToLoad();
        // Find duplicate page (only for depth=1)
        WebElement duplicate = webDriver.findElement(By.xpath("//tr[@depth='1']//a[contains(text(), '" + fullPageTitle
                + "')]/b[text()='(duplicate)']/.."));
        // Get chapter link
        UserDetailedLLPage userDetailedLLPage = new UserDetailedLLPage(webDriver, pageLocation);
        WebElement chapterLink = userDetailedLLPage.getTableDetailedCourseProgress().findElement(By.xpath(".//b[text()='"
                + chapter + "']")); // get the chapter xpath
        if(!(duplicate.isDisplayed())) { // if duplicate page isn't displayed
            // Click icon 'expander' to expand course tree for the chapter
            WebElement iconExpander = chapterLink.findElement(By.xpath("./ancestor::tr/td/*[@class='expander']"));
            waitUntilElementIsClickable(iconExpander);
            clickItem(iconExpander, "The icon 'expander' for the chapter could not be clicked.");
            // Wait until the page is displayed in the tree (depth = 1)
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[@depth='1']//a[contains(text(), '"
                    + fullPageTitle + "')]/b[text()='(duplicate)']/..")));
        }
        return duplicate;
    }
    public void checkPageIsNotShownInAllPagesCourseContentList(String fullPageTitle) throws Exception {
        waitForPageToLoad();
        // Get list of elements with defined title
        CourseContentLLPage courseContentLLPage = new CourseContentLLPage(webDriver, pageLocation);
        List<WebElement> pages = courseContentLLPage.getTableCourseContent().findElements(By.xpath(".//td[1]/a[text()='"
                + fullPageTitle + "']"));
        Assert.assertTrue("The course page is displayed on Course Content page.", pages.size() == 0);
    }
    public WebElement checkPageIsDisplayedOnUserDetailedPage(String fullPageTitle, String chapter) throws Exception {
        waitForPageToLoad();
        // Check if chapter with defined name is displayed on the page
        UserDetailedLLPage userDetailedLLPage = new UserDetailedLLPage(webDriver, pageLocation);
        List<WebElement> chapters = userDetailedLLPage.getTableDetailedCourseProgress().findElements(By.xpath(".//b[text()='"
                + chapter + "']"));
        WebElement chapterLink;
        WebElement page = null;
        if(chapters.size() == 1) { // if the chapter is displayed
            chapterLink = userDetailedLLPage.getTableDetailedCourseProgress().findElement(By.xpath(".//b[text()='"
                    + chapter + "']")); // get the chapter xpath
            // List elements with Course page title (depth = 1)
            List<WebElement> pages = userDetailedLLPage.getTableDetailedCourseProgress().findElements(By.xpath(".//tr[@depth='1']/td/a[text()='"
                    + fullPageTitle + "']"));
            if(pages.size() == 1) { // if the element has been found
                // Check if the page is displayed in course tree
                page = chapterLink.findElement(By.xpath("./ancestor::tr/following-sibling::tr[@depth='1']//a[text()='"
                        + fullPageTitle + "']")); // find the page by xpath
                if(!(page.isDisplayed())) { // if it isn't displayed
                    // Click icon 'expander' to expand course tree for the chapter
                    WebElement iconExpander = chapterLink.findElement(By.xpath("./ancestor::tr/td[4]/*[@class='expander']"));
                    waitUntilElementIsClickable(iconExpander);
                    clickItem(iconExpander, "The icon 'expander' for the chapter could not be clicked.");
                    // Wait until the course page is displayed in the tree
                    WebDriverWait wait = new WebDriverWait(webDriver, 10);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list_pages']" +
                            "//tr[@depth='1']/td/a[text()='" + fullPageTitle + "']")));
                }
                return page;
            }
            // If 0 or more than 1 such title have been found in course tree
            Assert.assertTrue("'0' or more than '1' course page with such title is displayed " +
                    "in the 'Detailed course progress' list.", false);
        }
        // If the chapter isn't displayed
        Assert.assertTrue("The chapter isn't displayed on UserDetailed page or " +
                        "№ of chapters with such title is more than 1.", false);
        return page;
    }
    public WebElement checkPageIsDisplayedAsChildOf(String fullTitleParentPage, String fullTitleChildPage) throws Exception {
        waitForPageToLoad();
        // Check if parent page with defined name is displayed on the page
        UserDetailedLLPage userDetailedLLPage = new UserDetailedLLPage(webDriver, pageLocation);
        List<WebElement> parentLinks = userDetailedLLPage.getTableDetailedCourseProgress().findElements(By.xpath(".//a[text()='"
                + fullTitleParentPage + "']"));
        WebElement parentLink;
        WebElement childPage = null;
        if(parentLinks.size() == 1) { // if the parent page is displayed
            parentLink = userDetailedLLPage.getTableDetailedCourseProgress().findElement(By.xpath(".//a[text()='"
                    + fullTitleParentPage + "']")); // get the parent page xpath
            // List child elements (with defined title) of this parent page (depth = 2)
            List<WebElement> pages = userDetailedLLPage.getTableDetailedCourseProgress().findElements(By.xpath(".//tr[@depth='2']" +
                    "/td/a[contains(text(), '" + fullTitleChildPage + "')]"));
            if(pages.size() == 1) { // if the child element has been found
                // Check if the page is displayed in course tree
                childPage = parentLink.findElement(By.xpath("./ancestor::tr/following-sibling::tr[@depth='2']" +
                        "//a[contains(text(), '" + fullTitleChildPage + "')]")); // find the page by xpath
                if(!(childPage.isDisplayed())) { // if it isn't displayed
                    // Click icon 'expander' to expand course tree for the parent page
                    WebElement iconExpander = parentLink.findElement(By.xpath("./ancestor::tr/td/*[@class='expander']"));
                    waitUntilElementIsClickable(iconExpander);
                    clickItem(iconExpander, "The icon 'expander' for the parent page could not be clicked.");
                    // Wait until the child page is displayed in the tree (depth = 2)
                    WebDriverWait wait = new WebDriverWait(webDriver, 10);
                    wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list_pages']" +
                            "//tr[@depth='2']/td/a[contains(text(), '" + fullTitleChildPage + "')]")));
                }
                return childPage;
            }
            // If 0 or more than 1 such title have been found in course tree
            Assert.assertTrue("'0' or more than '1' course child page with such title is displayed " +
                    "in the 'Detailed course progress' list.", false);
        }
        // If the parent page isn't displayed
        Assert.assertTrue("The parent page isn't displayed on UserDetailed page or " +
                "№ of pages with such title is more than 1.", false);
        return childPage;
    }
    public void checkStatsForPageOnUserDetailed(WebElement page, int nOfVisits, boolean completed) {
        // Check 'nOfVisits'
        String nOfVisitsActual = page.findElement(By.xpath("./ancestor::tr/td[2]")).getText().trim();
        int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
        Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
        // Check 'completed' value
        String completedString = page.findElement(By.xpath("./ancestor::tr/td[3]/div")).getAttribute("data-completed");
        boolean completedActual = Boolean.valueOf(completedString);
        Assert.assertTrue("Incorrect boolean value for 'completed'.", completedActual == completed);
    }
    public void checkStatsForPageOnCourseContent(WebElement page, int nOfVisits, int nOfUsers, int percentOfUsers) {
        // Check 'nOfVisits'
        String nOfVisitsActual = page.findElement(By.xpath("./../../td[2]")).getText().trim();
        int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
        Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
        // Check 'nOfUsers'
        String nOfUsersActual = page.findElement(By.xpath("./../../td[3]")).getText().trim();
        int nOfUsersInt = Integer.parseInt(nOfUsersActual);
        Assert.assertTrue("'№ of users' has incorrect value for the page.", nOfUsersInt == nOfUsers);
        // Check '%users'
        String percentOfUsersActual = page.findElement(By.xpath("./../../td[4]")).getText().trim();
        String percentOfUsersValue = percentOfUsersActual.replace("%", "");
        int percentOfUsersInt = Integer.parseInt(percentOfUsersValue);
        Assert.assertTrue("'%users' has incorrect value for the page.", percentOfUsersInt == percentOfUsers);
    }
    public CourseDetailedLLPage openCourseDetailedPageFromCourseContentTable(String fullPageTitle) throws Exception {
        waitForPageToLoad();
        CourseContentLLPage courseContentLLPage = new CourseContentLLPage(webDriver, pageLocation);
        WebElement page = courseContentLLPage.getTableCourseContent().findElement(By.xpath(".//td[1]/a[text()='"
                + fullPageTitle + "']"));
        waitUntilElementIsClickable(page);
        clickItem(page, "The page link could not be clicked from CourseContent table.");
        return new CourseDetailedLLPage(webDriver, pageLocation);
    }
    public CourseDetailedLLPage openCourseMaterialFromComponentDetailedPage(String fullPageTitle) throws Exception {
        waitForPageToLoad();
        CourseDetailedLLPage courseDetailedLLPage = new CourseDetailedLLPage(webDriver, pageLocation);
        WebElement page = courseDetailedLLPage.getTableCourseMaterial().findElement(By.xpath(".//td[1]/a[text()='"
                + fullPageTitle + "']"));
        waitUntilElementIsClickable(page);
        clickItem(page, "The page link could not be clicked from CourseMaterial table.");
        return new CourseDetailedLLPage(webDriver, pageLocation);
    }
    public CourseDetailedLLPage openAdditionalMaterialFromComponentDetailedPage(String fullPageTitle) throws Exception {
        waitForPageToLoad();
        CourseDetailedLLPage courseDetailedLLPage = new CourseDetailedLLPage(webDriver, pageLocation);
        WebElement page = courseDetailedLLPage.getTableAdditionalMaterial().findElement(By.xpath(".//td[1]/a[text()='"
                + fullPageTitle + "']"));
        waitUntilElementIsClickable(page);
        clickItem(page, "The page link could not be clicked from AdditionalMaterial table.");
        return new CourseDetailedLLPage(webDriver, pageLocation);
    }
    public CourseDetailedLLPage openCourseDetailedPageFromUserDetailedTable(String fullPageTitle) throws Exception {
        waitForPageToLoad();
        UserDetailedLLPage userDetailedLLPage = new UserDetailedLLPage(webDriver, pageLocation);
        WebElement page = userDetailedLLPage.getTableDetailedCourseProgress().findElement(By.xpath(".//td[1]/a[text()='"
                + fullPageTitle + "']"));
        waitUntilElementIsClickable(page);
        clickItem(page, "The page link could not be clicked from user's detailed page.");
        return new CourseDetailedLLPage(webDriver, pageLocation);
    }
    public void checkCourseProgressForUserOnUsersPage(List<String> user, int percentCourseProgress) throws Exception {
        waitForPageToLoad();
        String xpathCourseProgress = "//*[@id='list_users']//a[contains(text(), '" + user.get(0)
                + "')]/ancestor::tr/td[2]";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathCourseProgress)));
        String courseProgressActual = webDriver.findElement(By.xpath(xpathCourseProgress)).getText().trim().replace("%", "");
        int courseProgressInt = Integer.parseInt(courseProgressActual);
        Assert.assertTrue("Wrong percent of user's course progress is displayed on Users page.",
                courseProgressInt == percentCourseProgress);
    }
    public void addCourseMaterialsToLiveSession(String titlePresentation, String titleVideo, String titleImage) throws Exception {
        // Open 'Course material' tab
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        // Add a Presentation with 'add' icon
        CreatePresentationPage createPresentationPage = editLiveSessionPage.clickIconAddPresentation();
        createPresentationPage.fillFieldTitle(titlePresentation);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        assertTextOfMessage(editLiveSessionPage.getMessage(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editLiveSessionPage.getFieldPresentationsCourseMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation, titleOfCreatedPresentation.matches(titlePresentation + ".*"));

        // Add a Video with 'add' icon
        CreateVideoPage createVideoPage = editLiveSessionPage.clickIconAddVideo();
        createVideoPage.fillFieldTitle(titleVideo);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(urlYouTube);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        assertTextOfMessage(editLiveSessionPage.getMessage(), ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEO'S field
        String titleOfCreatedVideo = editLiveSessionPage.getFieldVideosCourseMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEO'S field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(titleVideo + ".*"));

        // Add an Image with 'add' icon
        CreateImagePage createImagePage = editLiveSessionPage.clickIconAddImage();
        createImagePage.fillFieldTitle(titleImage);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertTextOfMessage(editLiveSessionPage.getMessage(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editLiveSessionPage.getFieldImagesCourseMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(titleImage + ".*"));

        // Save the Live session, check the materials are shown on View page
        ViewLiveSessionPage viewLiveSessionPage = editLiveSessionPage.clickButtonSave();
        assertTextOfMessage(viewLiveSessionPage.getMessage(), ".*Live session .+ has been updated.");
        List<WebElement> materials = webDriver.findElements(By.cssSelector(".field__items .title")); // get list of materials
        List<String> materialsTitles = new ArrayList<String>(); // get list of materials titles
        for (WebElement material : materials) {
            String materialTitle = material.getText();
            materialsTitles.add(materialTitle);
        }
        Assert.assertTrue("Wrong materials titles are displayed on View Live Session page.",
                materialsTitles.contains(titlePresentation) && materialsTitles.contains(titleVideo) && materialsTitles.contains(titleImage));
    }
    public String addPresentationToTaskAdditionalResources(String titlePresentation) throws Exception { // additional resources
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        // Add a Presentation with 'add' icon
        CreatePresentationPage createPresentationPage = editTaskPage.clickIconAddPresentationAdditional();
        createPresentationPage.fillFieldTitle(titlePresentation);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        assertTextOfMessage(editTaskPage.getMessage(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editTaskPage.getFieldPresentationsAdditionalResourses().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation,
                titleOfCreatedPresentation.matches(titlePresentation + ".*"));
        String node = titleOfCreatedPresentation.split("\\(")[1].split("\\)")[0]; // get video node
        return node;
    }
    public String addVideoVimeoToTaskResources(String titleVideo) throws Exception {
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        CreateVideoPage createVideoPage = editTaskPage.clickIconAddVideo();
        createVideoPage.fillFieldTitle(titleVideo);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(vimeoURL);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        assertTextOfMessage(editTaskPage.getMessage(), ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEO'S field
        String titleOfCreatedVideo = editTaskPage.getFieldVideosTaskResorses().getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEO'S field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(titleVideo + ".*"));
        String node = titleOfCreatedVideo.split("\\(")[1].split("\\)")[0]; // get video node
        return node;
    }
    public void addPresentationAndAdditionalImageToTask(String titlePresentation, String titleImage) throws Exception {
        // Open 'Task resources' tab
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        // Add a Presentation with 'add' icon
        CreatePresentationPage createPresentationPage = editTaskPage.clickIconAddPresentation();
        createPresentationPage.fillFieldTitle(titlePresentation);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        assertTextOfMessage(editTaskPage.getMessage(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editTaskPage.getFieldPresentationsTaskResorses().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation,
                titleOfCreatedPresentation.matches(titlePresentation + ".*"));

        // Open 'Additional resources' tab
        editTaskPage.switchToTabAdditionalResources();
        // Add an Image with 'add' icon
        CreateImagePage createImagePage = editTaskPage.clickIconAddImageAdditional();
        createImagePage.fillFieldTitle(titleImage);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertTextOfMessage(editTaskPage.getMessage(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editTaskPage.getFieldImagesAdditionalResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(titleImage + ".*"));

        // Save the Task, check resources are shown on View page
        ViewTaskPage viewTaskPage = editTaskPage.clickButtonSave();
        assertTextOfMessage(viewTaskPage.getMessage(), ".*Task .+ has been updated.");
        List<WebElement> resources = webDriver.findElements(By.cssSelector(".field__items .title")); // get list of resources
        List<String> resourcesTitles = new ArrayList<String>(); // get list of resources titles
        for (WebElement resource : resources) {
            String resourceTitle = resource.getText();
            resourcesTitles.add(resourceTitle);
        }
        Assert.assertTrue("Wrong resources titles are displayed on View Task page.", resourcesTitles.contains(titlePresentation)
                && resourcesTitles.contains(titleImage));
    }
    public String addImageToTheoryCourseMaterials(String titleImage) throws Exception { // course material
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        // Add an Image with 'add' icon
        CreateImagePage createImagePage = editTheoryPage.clickIconAddImage();
        createImagePage.fillFieldTitle(titleImage); // image title
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path); // image path
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertTextOfMessage(editTheoryPage.getMessage(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editTheoryPage.getFieldImagesCourseMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(titleImage + ".*"));
        String node = titleOfCreatedImage.split("\\(")[1].split("\\)")[0]; // get image node
        return node;
    }
    public void addImageToTheoryAdditionalMaterials(String titleImage) throws Exception { // additional material
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        // Add an Image with 'add' icon
        CreateImagePage createImagePage = editTheoryPage.clickIconAddImageAdditional();
        createImagePage.fillFieldTitle(titleImage); // image title
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        selectImagePage.selectImage(image1Path); // image path
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        assertTextOfMessage(editTheoryPage.getMessage(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editTheoryPage.getFieldImagesAdditionalMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(titleImage + ".*"));
    }
    public String addVideoYoutubeToTheoryAdditionalMaterial(String titleVideo) throws Exception {
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        CreateVideoPage createVideoPage = editTheoryPage.clickIconAddVideoAdditional();
        createVideoPage.fillFieldTitle(titleVideo);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(urlYouTube);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        assertTextOfMessage(editTheoryPage.getMessage(), ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEO'S field
        String titleOfCreatedVideo = editTheoryPage.getFieldVideosAdditionalMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEO'S field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(titleVideo + ".*"));
        String node = titleOfCreatedVideo.split("\\(")[1].split("\\)")[0]; // get video node
        return node;
    }
    public void addAdditionalMaterialsToTheory(String titlePresentation, String titleVideo, String titleImage) throws Exception {
        // Open 'Additional material' tab
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabAdditionalMaterial();
        // Add a Presentation with 'add' icon
        CreatePresentationPage createPresentationPage = editTheoryPage.clickIconAddPresentationAdditional();
        createPresentationPage.fillFieldTitle(titlePresentation);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        assertTextOfMessage(editTheoryPage.getMessage(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editTheoryPage.getFieldPresentationsAdditionalMaterial().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation, titleOfCreatedPresentation.matches(titlePresentation + ".*"));

        // Add a Video with 'add' icon
        addVideoYoutubeToTheoryAdditionalMaterial(titleVideo);

        // Add an Image with 'add' icon
        addImageToTheoryAdditionalMaterials(titleImage);

        // Save the Theory, check the materials are shown on View page
        ViewTheoryPage viewTheoryPage = editTheoryPage.clickButtonSave();
        assertTextOfMessage(viewTheoryPage.getMessage(), ".*Theory .+ has been updated.");
        List<WebElement> materials = webDriver.findElements(By.cssSelector(".field__items .title")); // get list of materials
        List<String> materialsTitles = new ArrayList<String>(); // get list of materials titles
        for (WebElement material : materials) {
            String materialTitle = material.getText();
            materialsTitles.add(materialTitle);
        }
        Assert.assertTrue("Wrong materials titles are displayed on View Theory page.",
                materialsTitles.contains(titlePresentation) && materialsTitles.contains(titleVideo) && materialsTitles.contains(titleImage));
    }
    public void checkMaterialIsShownInCourseMaterialBlock(String fullMaterialTitle, int nOfVisits, int nOfUsers, int percentOfUsers) throws Exception {
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until table is displayed
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list_course_material']//table")));
        // Get list of materials with defined title
        String xpathLinkMaterial = "//*[@id='list_course_material']//a[text()='" + fullMaterialTitle + "']";
        List<WebElement> materials = webDriver.findElements(By.xpath(xpathLinkMaterial));
        // Check whether the material is displayed in 'Course material' block
        if(materials.size() == 1) {
            WebElement material = webDriver.findElement(By.xpath(xpathLinkMaterial));
            Assert.assertTrue("The material is not displayed in 'Course material' block.", material.isDisplayed());
            // Check 'nOfVisits'
            String nOfVisitsActual = material.findElement(By.xpath("./../../td[2]")).getText().trim();
            int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
            Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
            // Check 'nOfUsers'
            String nOfUsersActual = material.findElement(By.xpath("./../../td[3]")).getText().trim();
            int nOfUsersInt = Integer.parseInt(nOfUsersActual);
            Assert.assertTrue("'№ of users' has incorrect value for the page.", nOfUsersInt == nOfUsers);
            // Check '%users'
            String percentOfUsersActual = material.findElement(By.xpath("./../../td[4]")).getText().trim();
            String percentOfUsersValue = percentOfUsersActual.replace("%", "");
            int percentOfUsersInt = Integer.parseInt(percentOfUsersValue);
            Assert.assertTrue("'%users' has incorrect value for the page.", percentOfUsersInt == percentOfUsers);
            return;
        }
        Assert.assertTrue("The material is not displayed in 'Course material' " +
                "or more than 1 material with such title are shown.", false);
    }
    public void checkUserIsShownInWatchedMovieTable(List<String> user, boolean fullyWatched) throws Exception {
        waitForPageToLoad();
        // Wait until the table 'Users who watched this movie' is displayed
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list_users_watched']//table")));
        // Get list of users with defined username
        String xpathLinkUser = "//*[@id='list_users_watched']//a[contains(text(), '" + user.get(0) + "')]";
        List<WebElement> users = webDriver.findElements(By.xpath(xpathLinkUser));
        // Check whether the user is displayed in 'Users who watched this movie' table
        if(users.size() == 1) {
            WebElement userWatchedMovie = webDriver.findElement(By.xpath(xpathLinkUser));
            Assert.assertTrue("The user is not displayed in 'Users who watched this movie' table.",
                    userWatchedMovie.isDisplayed());
            // Check if the video completed (fully watched)
            String percentWatchedText = userWatchedMovie.findElement(By.xpath("./../../td[2]")).getText();
            String percentWatched = percentWatchedText.replace("%", "");
            int percentWatchedActual = Integer.parseInt(percentWatched); // get percent of video watched
            // Check that if the percent < 90% - the video is partially watched
            boolean fullyWatchedActual = true;
            if (percentWatchedActual < 90) {
                fullyWatchedActual = false; // partially watched
            }
            Assert.assertTrue("Completeness of the video isn't as expected.", fullyWatchedActual == fullyWatched);
            return;
        }
        Assert.assertTrue("The user is not displayed in 'Users who watched this movie' table " +
                "or more than 1 user with such username are shown.", false);
    }
    public void checkMaterialIsShownInAdditionalMaterialBlock(String fullMaterialTitle, int nOfVisits, int nOfUsers, int percentOfUsers) throws Exception {
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until table is displayed
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id='list_additional_material']//table")));
        // Get list of materials with defined title
        String xpathLinkMaterial = "//*[@id='list_additional_material']//a[text()='" + fullMaterialTitle + "']";
        List<WebElement> materials = webDriver.findElements(By.xpath(xpathLinkMaterial));
        // Check whether the material is displayed in 'Additional material' block
        if(materials.size() == 1) {
            WebElement material = webDriver.findElement(By.xpath(xpathLinkMaterial));
            Assert.assertTrue("The material is not displayed in 'Additional material' block.", material.isDisplayed());
            // Check 'nOfVisits'
            String nOfVisitsActual = material.findElement(By.xpath("./../../td[2]")).getText().trim();
            int nOfVisitsInt = Integer.parseInt(nOfVisitsActual);
            Assert.assertTrue("'№ of visits' has incorrect value for the page.", nOfVisitsInt == nOfVisits);
            // Check 'nOfUsers'
            String nOfUsersActual = material.findElement(By.xpath("./../../td[3]")).getText().trim();
            int nOfUsersInt = Integer.parseInt(nOfUsersActual);
            Assert.assertTrue("'№ of users' has incorrect value for the page.", nOfUsersInt == nOfUsers);
            // Check '%users'
            String percentOfUsersActual = material.findElement(By.xpath("./../../td[4]")).getText().trim();
            String percentOfUsersValue = percentOfUsersActual.replace("%", "");
            int percentOfUsersInt = Integer.parseInt(percentOfUsersValue);
            Assert.assertTrue("'%users' has incorrect value for the page.", percentOfUsersInt == percentOfUsers);
            return;
        }
        Assert.assertTrue("The material is not displayed in 'Additional material' " +
                "or more than 1 material with such title are shown.", false);
    }
    public void checkNoContentOnDetailedPage() throws Exception {
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("list_course_material")));
        WebElement titleNoContent = webDriver.findElement(By.xpath("//h3[text()='No content here']"));
        Assert.assertTrue("Text 'No content here' is not displayed on Detailed Course page.",
                titleNoContent.isDisplayed());
    }
    public void checkGroupIsNotSelectedInTinCanApi() throws Exception {
        // Open TinCanApi configuration page
        webDriver.get(websiteUrl.concat("/admin/config/services/tincanapi"));
        waitForPageToLoad();
        // Check that Group content type is not selected in 'Content types' block
        TinCanApiPage tinCanApiPage = new TinCanApiPage(webDriver, pageLocation);
        tinCanApiPage.clickLinkContentTypes();
        assertCheckboxIsNotSelected(tinCanApiPage.getCheckboxGroupContentTypes());
        // Check that Group content type is not selected in 'iMinds' block
        tinCanApiPage.clickLinkIminds();
        assertCheckboxIsNotSelected(tinCanApiPage.getCheckboxGroupIMinds());
        // Save changes
        tinCanApiPage.clickButtonSaveConfiguration();
        assertTextOfMessage(tinCanApiPage.getMessage(), ".*The configuration options have been saved.");
    }
    public void selectClassInLRS(String className) throws Exception {
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mainDashboardLLPage.getFieldClass());
        List<WebElement> classes = mainDashboardLLPage.getFieldClass().findElements(By.xpath("./..//li/a"));
        for (WebElement classToSelect : classes) {
            if (classToSelect.getAttribute("innerText").trim().equals("Class: " + className)) { // getText() returns empty string here
                String script = "arguments[0].click();";
                ((JavascriptExecutor) webDriver).executeScript(script, classToSelect);
                waitForPageToLoad();
                return;
            }
        }
        Assert.assertTrue("No such class in the list.", false);
    }
    public WebElement checkMovieIsShownWithStats(String fullParentInnerVideoTitle, List<Integer> fullyWatchedStats,
                                           List<Integer> partialWatchedStats) throws Exception {
        waitForPageToLoad();
        // Wait until the table 'All movies' is displayed
        MoviesLLPage moviesLLPage = new MoviesLLPage(webDriver, pageLocation);
        waitUntilElementIsVisible(moviesLLPage.getTableMovies());
        // Check if the Movie with defined title is displayed in the table
        List<WebElement> movies = moviesLLPage.getTableMovies().findElements(By.xpath(".//*[@class='parent']" +
                "[text()='" + fullParentInnerVideoTitle + "']/.."));


        if (movies.size() == 1) { // if the Movie is displayed
            WebElement movie = moviesLLPage.getTableMovies().findElement(By.xpath(".//*[@class='parent']" +
                    "[text()='" + fullParentInnerVideoTitle + "']/.."));
            Assert.assertTrue("The Movie is not displayed in 'All movies' table.", movie.isDisplayed());

            // Check 'Fully watched' stats
            String text = movie.findElement(By.xpath("./../../td[2]")).getText(); // get full stats text
            String[] parts = text.split("\\n\\("); // separate the text into parts
            String numberOfUsers = parts[0]; // get number of users
            int numberOfUsersInt = Integer.parseInt(numberOfUsers); // get int of 'number of users'
            parts = parts[1].split("%"); // split the part to get percent of users
            String percentOfUsers = parts[0];
            int percentOfUsersInt = Integer.parseInt(percentOfUsers); // get int of 'percent of users'
            List<Integer> fullyWatchedStatsActual = Arrays.asList(numberOfUsersInt, percentOfUsersInt);
            Assert.assertTrue("Stats for 'Fully watched' is incorrect for the Movie.",
                    fullyWatchedStatsActual.equals(fullyWatchedStats));

            // Check 'Partial watched' stats
            text = movie.findElement(By.xpath("./../../td[3]")).getText(); // get full stats text
            parts = text.split("\\n\\("); // separate the text into parts
            numberOfUsers = parts[0]; // get number of users
            numberOfUsersInt = Integer.parseInt(numberOfUsers); // get int of 'number of users'
            parts = parts[1].split("%"); // split the part to get percent of users
            percentOfUsers = parts[0];
            percentOfUsersInt = Integer.parseInt(percentOfUsers); // get int of 'percent of users'
            List<Integer> partialWatchedStatsActual = Arrays.asList(numberOfUsersInt, percentOfUsersInt);
            Assert.assertTrue("Stats for 'Partial watched' is incorrect for the Movie.",
                    partialWatchedStatsActual.equals(partialWatchedStats));
            return movie;
        }
        Assert.assertTrue("The Movie is not displayed in 'All movies' table " +
                "or more than 1 movie with such title are shown.", false);
        return null;
    }
    public WebElement checkQuizIsShownWithStats(String titleQuiz, int timesTaken,
                                          int nOfUsers, int percentAvgScore)  throws Exception { // Quizzes page
        // Get list of quizzes with defined title
        QuizzesLLPage quizzesLLPage = new QuizzesLLPage(webDriver, pageLocation);
        List<WebElement> quizzes = quizzesLLPage.getTableQuizzes().
                findElements(By.xpath(".//td[1]/a[contains(text(), '" + titleQuiz + "')]"));
        if (quizzes.size() == 1) { // if the quiz has been found
            // Check that 'times taken' value is displayed correctly
            WebElement quiz = quizzesLLPage.getTableQuizzes().
                    findElement(By.xpath(".//td[1]/a[contains(text(), '" + titleQuiz + "')]/ancestor::tr"));
            String timesTakenValue = quiz.findElement(By.xpath("./td[2]")).getText();
            int timesTakenActual = Integer.parseInt(timesTakenValue);
            Assert.assertTrue("Wrong 'times taken' value is displayed in 'All quizzes' table for the Quiz.",
                    timesTakenActual == timesTaken);
            // Check that 'n of users' value is displayed correctly
            String nOfUsersValue = quiz.findElement(By.xpath("./td[3]")).getText();
            int nOfUsersActual = Integer.parseInt(nOfUsersValue);
            Assert.assertTrue("Wrong 'n of users' value is displayed in 'All quizzes' table for the Quiz.",
                    nOfUsersActual == nOfUsers);
            // Check that 'avg. score' value is displayed correctly
            String percentAvgScoreValue = quiz.findElement(By.xpath("./td[4]")).getText();
            String percent = percentAvgScoreValue.split("%")[0];
            int percentAvgScoreActual = Integer.parseInt(percent);
            Assert.assertTrue("Wrong percent of 'avg. score' value is displayed in 'All quizzes' table for the Quiz.",
                    percentAvgScoreActual == percentAvgScore);
            // Return the link 'Score per user' for the Quiz
            WebElement linkScorePerUser = quiz.findElement(By.cssSelector(".score-per-user a"));
            return linkScorePerUser;
        }
        Assert.assertTrue("The Quiz is not displayed in 'All quizzes' list " +
                "or more than 1 Quiz with such title are shown.", false);
        return null;
    }
    public void checkScorePerUser(List<String> user, int triedValue, int percentScore) throws Exception { // from Quizzes page
        // Get list of users with defined username
        QuizzesLLPage quizzesLLPage = new QuizzesLLPage(webDriver, pageLocation);
        List<WebElement> users = quizzesLLPage.getPopupListScorePerUser().
                findElements(By.xpath(".//a[text()='" + user.get(0) + "']"));
        if (users.size() == 1) { // if the user has been found
            // Check that 'tried' value is displayed correctly
            WebElement userLine = quizzesLLPage.getPopupListScorePerUser().
                    findElement(By.xpath(".//a[text()='" + user.get(0) + "']/ancestor::tr"));
            String triedText = userLine.findElement(By.xpath("./td[2]")).getText();
            int triedActual = Integer.parseInt(triedText);
            Assert.assertTrue("Wrong 'tried' value is displayed in 'Score per user' table for the Quiz.",
                    triedActual == triedValue);
            // Check that percent 'score' is displayed correctly
            String percentScoreValue = userLine.findElement(By.xpath("./td[3]")).getText();
            String percent = percentScoreValue.split("%")[0];
            int percentScoreActual = Integer.parseInt(percent);
            Assert.assertTrue("Wrong percent of 'score' value is displayed in 'Score per user' table for the Quiz.",
                    percentScoreActual == percentScore);
            return;
        }
        Assert.assertTrue("The Quiz is not displayed in 'All quizzes' list " +
                "or more than 1 Quiz with such title are shown.", false);
    }
    public WebElement checkQuizResults(String quizTitle, int averageScore, int timesCompleted) throws Exception {
        waitForPageToLoad();
        // Get list of quizzes with defined title (user course tree page)
        UserDetailedLLPage userDetailedLLPage = new UserDetailedLLPage(webDriver, pageLocation);
        List<WebElement> quizzes = userDetailedLLPage.getListQuizzes().
                findElements(By.xpath(".//*[@class='title'][text()='" + quizTitle + "']"));
        if (quizzes.size() == 1) { // if the quiz has been found
            // Check that 'average score' value is displayed correctly
            WebElement quiz = userDetailedLLPage.getListQuizzes().
                    findElement(By.xpath(".//*[@class='title'][text()='" + quizTitle + "']/.."));
            String scoreValue = quiz.findElement(By.cssSelector(".stat_average_user_score")).getText();
            String score = scoreValue.split("%")[0];
            int scoreActual = Integer.parseInt(score);
            Assert.assertTrue("Wrong average score percent is displayed in 'Quiz results' for the Quiz.",
                    scoreActual == averageScore);
            // Check that 'times completed' value is displayed correctly
            String timesCompletedValue = quiz.findElement(By.cssSelector(".completed")).getText();
            String times = timesCompletedValue.split(" times")[0];
            int timesCompletedActual = Integer.parseInt(times);
            Assert.assertTrue("Wrong 'times completed' value is displayed in 'Quiz results' for the Quiz.",
                    timesCompletedActual == timesCompleted);
            // Return the link 'View details' for the Quiz
            WebElement linkViewDetails = quiz.findElement(By.cssSelector("[href*='quizzes/details']"));
            return linkViewDetails;
        }
        Assert.assertTrue("The Quiz is not displayed in 'Quiz results' list " +
                "or more than 1 Quiz with such title are shown.", false);
        return null;
    }
    public WebElement checkTopQuizResultForUser(List<String> user, String nameOfList, int percentResult,
                                                int numberCompleted, int numberOfAllQuizzes) throws Exception {
        waitForPageToLoad();
        // Get list of top users with defined username (Quizzes page)
        QuizzesLLPage quizzesLLPage = new QuizzesLLPage(webDriver, pageLocation);
        // Verify which list of users is active
        WebElement listUsers;
        if (nameOfList.equals(best)) { // list 'BEST'
            listUsers = quizzesLLPage.getListBestUsers();
        } else { listUsers = quizzesLLPage.getListWorstUsers(); } // list 'WORST'
        // Find list of users with defined username
        List<WebElement> userResults = listUsers.findElements(By.xpath(".//div[contains(text(), '"
                + user.get(0) + "')]"));
        if (userResults.size() == 1) { // if the user has been found in the list
            // Check that percent value is displayed correctly
            WebElement userResult = listUsers.findElement(By.xpath(".//div[contains(text(), '"
                    + user.get(0) + "')]"));
            String percentResultValue = userResult.findElement(By.xpath("./span")).getText();
            String percent = percentResultValue.split("%")[0];
            int percentResultActual = Integer.parseInt(percent);
            Assert.assertTrue("Wrong result percent is displayed in 'Top users quiz result' on Quizzes page.",
                    percentResultActual == percentResult);
            // Check that 'quizzes completed' value is displayed correctly
            String quizzesCompletedActual = userResult.findElement(By.xpath("./../div[2]")).getText();
            String quizzesCompletedExpected = numberCompleted + " of " + numberOfAllQuizzes + " quizzes completed";
            Assert.assertTrue("Wrong 'quizzes completed' value is displayed in 'Top users quiz result' on Quizzes page.",
                    quizzesCompletedActual.equals(quizzesCompletedExpected));
            // Get user link
            WebElement userLink = userResult.findElement(By.xpath("./ancestor::a"));
            return userLink;
        }
        Assert.assertTrue("The user is not displayed in 'Top users quiz result' list " +
                "or more than 1 user with such name is shown.", false);
        return null;
    }
    public void checkListOfTopQuizResultsIsShown(String nameOfList) throws Exception {
        waitForPageToLoad();
        // Get name of active list in 'Top users quiz result'
        String nameOfListActive = webDriver.findElement(By.cssSelector(".tab-handles .active a")).getText();
        // Switch to another list if active list is not as expected
        if (!(nameOfListActive.equals(nameOfList))) {
            WebElement listNotActive = webDriver.findElement(By.xpath("//*[@class='tab-handles']" +
                    "/li[not(@class='active')]/a")); // not active list
            final String dataToggleValue = listNotActive.getAttribute("data-toggle");
            clickItem(listNotActive, "The inactive list in 'Top users quiz result' could not be clicked.");
            waitCondition(new ExpectedCondition<Boolean>() { // wait until the list becomes active
                public Boolean apply(WebDriver webDriver) {
                    return webDriver.findElement(By.xpath("//a[@data-toggle='" + dataToggleValue + "']/.."))
                            .getAttribute("class").equals("active");
                }
            });
            nameOfListActive = webDriver.findElement(By.cssSelector(".tab-handles .active a")).getText();
        }
        Assert.assertTrue("Wrong list of quiz top results is opened.", nameOfListActive.equals(nameOfList));
    }
    public void checkFilterByTabIsSelected(final WebElement tabFilterBy) throws Exception {
        List<WebElement> elements = tabFilterBy.findElements(By.xpath("./self::*[@class='active']"));
        if (elements.size() == 0) { // if the tab is not active
            String script = "arguments[0].click();"; // click the tab
            ((JavascriptExecutor) webDriver).executeScript(script, tabFilterBy);
            waitCondition(new ExpectedCondition<Boolean>() { // wait until the list becomes active
                public Boolean apply(WebDriver webDriver) { // wait until the tab is active
                    return tabFilterBy.findElement(By.xpath("./self::*[contains(@class, 'active')]")).isDisplayed();
                }
            });
        }
    }
    public void selectPageOnAddFiltersPopup(final String fullTitle) throws Exception {
        // Type page title into 'Select page' field
        AddFiltersLLPage addFiltersLLPage = new AddFiltersLLPage(webDriver, pageLocation);
        addFiltersLLPage.getFieldSelectPage().sendKeys(fullTitle);
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) { // wait until a dropdown list is displayed
                return webDriver.findElement(By.cssSelector("form#page_search_form .selectize-dropdown")).isDisplayed();
            }
        });
        // Click the 1st option with such name
        WebElement dropdownList = webDriver.findElement(By.cssSelector("form#page_search_form .selectize-dropdown"));
        final WebElement page = dropdownList.findElement(By.xpath("./div/div[@class='option active']"));
        waitCondition(new ExpectedCondition<Boolean>() { // wait until defined page is active
            public Boolean apply(WebDriver webDriver) {
                return page.getText().equals(fullTitle);
            }
        });
        String script = "arguments[0].click();"; // click the page from dropdown list
        ((JavascriptExecutor) webDriver).executeScript(script, page);
        // Click tab to close the dropdown list
        clickItem(addFiltersLLPage.getTabByPage(), "Tab 'By page' could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("form#page_search_form .selectize-dropdown")));
        // Check if the page is selected and displayed under the field
        List<WebElement> pages = webDriver.findElements(By.xpath("//*[@class='item']/*[@class='name']" +
                "[text()='" + fullTitle + "']"));
        if (pages.size() == 1) { // if 1 page with such title has been found, check whether it's displayed
            WebElement pageSelected = webDriver.findElement(By.xpath("//*[@class='item']/*[@class='name']" +
                    "[text()='" + fullTitle + "']"));
            Assert.assertTrue("The page is not displayed in 'selected pages' list.", pageSelected.isDisplayed());
            return;
        }
        Assert.assertTrue("No page is selected or more than 1 page with such title has been found.", false);
    }
    public void removeItemFromList(String fullTitle) throws Exception { // on 'Add filters' page (for pages, users)
        WebElement item = webDriver.findElement(By.xpath("//*[@class='item']/*[@class='name']" +
                "[text()='" + fullTitle + "']/..")); // find item
        WebElement iconDelete = item.findElement(By.cssSelector("a.delete"));
        waitUntilElementIsClickable(iconDelete); // find 'delete' icon for the item
        // Click 'delete' icon for the item
        clickItem(iconDelete, "Icon delete for current item could not be clicked.");
        // Wait until the item is invisible
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath("//*[@class='item']/*[@class='name']" +
                "[text()='" + fullTitle + "']")));
    }
    public void selectUserOnAddFiltersPopup(final List<String> user) throws Exception {
        // Type username into 'Select a user' field
        AddFiltersLLPage addFiltersLLPage = new AddFiltersLLPage(webDriver, pageLocation);
        final String username = user.get(0);
        addFiltersLLPage.getFieldSelectUser().sendKeys(username);
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) { // wait until a dropdown list is displayed
                return webDriver.findElement(By.cssSelector("form#user_search_form .selectize-dropdown")).isDisplayed();
            }
        });
        // Click the 1st option with such name
        WebElement dropdownList = webDriver.findElement(By.cssSelector("form#user_search_form .selectize-dropdown"));
        final WebElement userOption = dropdownList.findElement(By.xpath("./div/div[@class='option active']"));
        waitCondition(new ExpectedCondition<Boolean>() { // wait until defined page is active
            public Boolean apply(WebDriver webDriver) {
                return userOption.getText().equals(username);
            }
        });
        String script = "arguments[0].click();"; // click the page from dropdown list
        ((JavascriptExecutor) webDriver).executeScript(script, userOption);
        // Click tab to close the dropdown list
        clickItem(addFiltersLLPage.getTabByUser(), "Tab 'By user' could not be clicked.");
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("form#user_search_form .selectize-dropdown")));
        // Check if the user is selected and displayed under the field
        List<WebElement> usernames = webDriver.findElements(By.xpath("//*[@class='item']/*[@class='name']" +
                "[text()='" + username + "']"));
        if (usernames.size() == 1) { // if 1 user with such username has been found, check whether it's displayed
            WebElement userSelected = webDriver.findElement(By.xpath("//*[@class='item']/*[@class='name']" +
                    "[text()='" + username + "']"));
            Assert.assertTrue("The user is not displayed in 'selected users' list.", userSelected.isDisplayed());
            return;
        }
        Assert.assertTrue("No user is selected or more than 1 user with such name has been found.", false);
    }
    public void applyFilter(String filter) throws Exception { // filter can be: user, date, page
        // Click button 'Apply filters' on 'Add filters' popup
        AddFiltersLLPage addFiltersLLPage = new AddFiltersLLPage(webDriver, pageLocation);
        addFiltersLLPage.clickButtonApplyFilters();
        // Get text from 'current filter' field
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        String currentFilter = mainDashboardLLPage.getFieldCurrentFilter().getText();
        if (filter.equals(filterUser)) { // if 'user' field is applied
            Assert.assertTrue("Wrong filter has been applied.", currentFilter.contains("users"));
            return;
        }
        if (filter.equals(filterDate)) { // if 'date' field is applied
            Assert.assertTrue("Wrong filter has been applied.", currentFilter.contains("date range"));
            return;
        }
        if (filter.equals(filterPage)) { // if 'page' field is applied
            Assert.assertTrue("Wrong filter has been applied.", currentFilter.contains("pages"));
            return;
        }
        Assert.assertTrue("Filter name is incorrect or wrong filter has been applied.", false);
    }
    public void saveFilter(String filter) throws Exception { // filter can be: user, date, page
        // Click button 'Save' on 'Add filters' popup
        AddFiltersLLPage addFiltersLLPage = new AddFiltersLLPage(webDriver, pageLocation);
        addFiltersLLPage.clickButtonSave();
        // Get text from 'current filter' field
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        String currentFilter = mainDashboardLLPage.getFieldCurrentFilter().getText();
        if (filter.equals(filterUser)) { // if 'user' field is applied
            Assert.assertTrue("Wrong filter has been applied.", currentFilter.contains("users"));
            return;
        }
        if (filter.equals(filterDate)) { // if 'date' field is applied
            Assert.assertTrue("Wrong filter has been applied.", currentFilter.contains("date range"));
            return;
        }
        if (filter.equals(filterPage)) { // if 'page' field is applied
            Assert.assertTrue("Wrong filter has been applied.", currentFilter.contains("pages"));
            return;
        }
        Assert.assertTrue("Filter name is incorrect or wrong filter has been applied.", false);
    }
    public List<WebElement> checkFilterByUserIsSaved(int fields, int statements) throws Exception { // only for cases
                                                                             // when one filter of such kind is saved
        waitForPageToLoad();
        // Get list of all filters on SavedFilters page
        List<WebElement> filters  = webDriver.findElements(By.cssSelector(".container-fluid tbody tr"));
        // For each filter check if it has 'users' field
        for (WebElement filter : filters) {
            List<WebElement> users = filter.findElements(By.xpath("./td[contains(text(), 'users')]"));
            if (users.size() == 1) { // if the filter is 'By user'
                // Check that correct number of fields is displayed for the filter
                String fieldsNumber = filter.findElement(By.xpath("./td[2]")).getText().split("\\(")[1].split("\\)")[0];
                int fieldsActual = Integer.parseInt(fieldsNumber);
                Assert.assertTrue("Another number of fields is shown for the filter on SavedFilters page.",
                        fieldsActual == fields);
                // Check that correct number of statements is displayed for the filter
                String statementsNumber = filter.findElement(By.xpath("./td[3]")).getText();
                int statementsActual = Integer.parseInt(statementsNumber);
                Assert.assertTrue("Another number of statements is shown for the filter on SavedFilters page.",
                        statementsActual == statements);
                // Find buttons 'Use' and 'Delete' for the filter
                WebElement buttonUse = filter.findElement(By.xpath("./td/a[text()='Use']"));
                WebElement buttonDelete = filter.findElement(By.xpath("./td/form[contains(@action, 'delete')]"));
                return Arrays.asList(buttonUse, buttonDelete); // return the list of buttons
            }
        }
        // No filter 'By user' has been found
        Assert.assertTrue("Filter by user hasn't been found.", false);
        return null;
    }
    public List<WebElement> checkFilterByPageIsSaved(int fields, int statements) throws Exception { // only for cases
                                                                             // when one filter of such kind is saved
        waitForPageToLoad();
        // Get list of all filters on SavedFilters page
        List<WebElement> filters  = webDriver.findElements(By.cssSelector(".container-fluid tbody tr"));
        // For each filter check if it has 'pages' field
        for (WebElement filter : filters) {
            List<WebElement> pages = filter.findElements(By.xpath("./td[contains(text(), 'pages')]"));
            if (pages.size() == 1) { // if the filter is 'By page'
                // Check that correct number of fields is displayed for the filter
                String fieldsNumber = filter.findElement(By.xpath("./td[2]")).getText().split("\\(")[1].split("\\)")[0];
                int fieldsActual = Integer.parseInt(fieldsNumber);
                Assert.assertTrue("Another number of fields is shown for the filter on SavedFilters page.",
                        fieldsActual == fields);
                // Check that correct number of statements is displayed for the filter
                String statementsNumber = filter.findElement(By.xpath("./td[3]")).getText();
                int statementsActual = Integer.parseInt(statementsNumber);
                Assert.assertTrue("Another number of statements is shown for the filter on SavedFilters page.",
                        statementsActual == statements);
                // Find buttons 'Use' and 'Delete' for the filter
                WebElement buttonUse = filter.findElement(By.xpath("./td/a[text()='Use']"));
                WebElement buttonDelete = filter.findElement(By.xpath("./td/form[contains(@action, 'delete')]"));
                return Arrays.asList(buttonUse, buttonDelete); // return the list of buttons
            }
        }
        // No filter 'By page' has been found
        Assert.assertTrue("Filter by page hasn't been found.", false);
        return null;
    }
    public List<WebElement> checkFilterByDateIsSaved(int statements) throws Exception { // only for cases
                                                                 // when one filter of such kind is saved
        waitForPageToLoad();
        // Get list of all filters on SavedFilters page
        List<WebElement> filters  = webDriver.findElements(By.cssSelector(".container-fluid tbody tr"));
        // For each filter check if it has 'date' field
        for (WebElement filter : filters) {
            List<WebElement> fields = filter.findElements(By.xpath("./td[contains(text(), 'date')]"));
            if (fields.size() == 1) { // if the filter is 'By date'
                // Check that correct number of statements is displayed for the filter
                String statementsNumber = filter.findElement(By.xpath("./td[3]")).getText();
                int statementsActual = Integer.parseInt(statementsNumber);
                Assert.assertTrue("Another number of statements is shown for the filter on SavedFilters page.",
                        statementsActual == statements);
                // Find buttons 'Use' and 'Delete' for the filter
                WebElement buttonUse = filter.findElement(By.xpath("./td/a[text()='Use']"));
                WebElement buttonDelete = filter.findElement(By.xpath("./td/form[contains(@action, 'delete')]"));
                return Arrays.asList(buttonUse, buttonDelete); // return the list of buttons
            }
        }
        // No filter 'By date' has been found
        Assert.assertTrue("Filter by date hasn't been found.", false);
        return null;
    }
    public void checkLinkFiltersIsNotShown() throws Exception { // on sidebar
        waitForPageToLoad();
        List<WebElement> links = webDriver.findElements(By.cssSelector(".nav-sidebar a[href*='dashboard/filters']"));
        Assert.assertTrue("'Filters' link is shown on sidebar.", links.size() == 0);
    }

    public void deleteAllFilters() throws Exception {
        waitForPageToLoad();
        // Click the link Filters if it's displayed on side-bar
        List<WebElement> links = webDriver.findElements(By.cssSelector(".nav-sidebar a[href*='dashboard/filters']"));
        if (links.size() != 0) {
            MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
            mainDashboardLLPage.clickLinkFilters();
            // Count all filters on 'Saved filters' page
            List<WebElement> filters = webDriver.findElements(By.cssSelector(".container-fluid tbody tr"));
            // If at least 1 filter has been found delete all filters
            while (filters.size() > 1) {
                WebElement filter = filters.get(0);
                WebElement iconDelete = filter.findElement(By.cssSelector("td [action*='delete'] button")); // find 'delete' icon
                final int j = filters.size();
                clickItem(iconDelete, "Icon 'delete' for current filter could not be clicked."); // click the icon
                // Wait until number of filters reduces by 1
                waitCondition(new ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver webDriver) {
                        int i = webDriver.findElements(By.cssSelector(".container-fluid tbody tr")).size();
                        return i == j - 1;
                    }
                });
                filters = webDriver.findElements(By.cssSelector(".container-fluid tbody tr"));
            }
            // Delete the last filter
            WebElement iconDeleteLastFilter = webDriver.findElement(By.cssSelector("td [action*='delete'] button"));
            clickItem(iconDeleteLastFilter, "Icon 'delete' for the last filter could not be clicked.");
            waitForPageToLoad(); // redirecting to dashboard page
            links = webDriver.findElements(By.cssSelector(".nav-sidebar a[href*='dashboard/filters']"));
            Assert.assertTrue("Link 'Filters' is still visible.", links.size() == 0);
        }
    }
    public void checkStatementsAreShownOnlyFor(List<String> nodes) throws Exception { // the list of 2 nodes only!
        waitForPageToLoad();
        // Get list of statements
        List<WebElement> statements = webDriver.findElements(By.cssSelector(".statement-row a"));
        // For each statement find its node
        for (WebElement statement : statements) {
            String href = statement.getAttribute("href");
            String nodeActual = href.split("node/")[1];
            if (!(nodeActual.equals(nodes.get(0)))) { // if the node doesn't match the 1st element from list,
                                                      // check if it's equal to the 2nd element
                Assert.assertTrue("Extra statement is displayed on Statements page.",
                        nodeActual.equals(nodes.get(1)));
            }
        }
    }
    public void checkStatementsAreShownForNode(String node) throws Exception {
        waitForPageToLoad();
        // Get list of statements
        List<WebElement> statements = webDriver.findElements(By.cssSelector(".statement-row a"));
        // For each statement find its node
        for (WebElement statement : statements) {
            String href = statement.getAttribute("href");
            String nodeActual = href.split("node/")[1];
            // Check that the node equals to expected one
            Assert.assertTrue("Not only statements for current node are displayed on Statements page.",
                    nodeActual.equals(node));
        }
    }
    public void checkStatementsAreShownForUser(List<String> user) throws Exception {
        waitForPageToLoad();
        // Get list of statements
        List<WebElement> statements = webDriver.findElements(By.cssSelector(".statement-row"));
        // For each statement find its actor
        for (WebElement statement : statements) {
            String actor = statement.getText();
            String actorActual = actor.split(" ")[0]; // split by space
            // Check that the actor equals to expected one
            Assert.assertTrue("Not only statements for current user are displayed on Statements page.",
                    actorActual.equals(user.get(0)));
        }
    }
    public void checkAtLeastOneStatementIsShownForUser(List<String> user) throws Exception {
        waitForPageToLoad();
        // Get list of statements
        List<WebElement> statements = webDriver.findElements(By.cssSelector(".statement-row"));
        // For each statement find its actor
        for (WebElement statement : statements) {
            String actor = statement.getText();
            String actorActual = actor.split(" ")[0]; // split by space
            // Check whether the actor equals to expected one
            if (actorActual.equals(user.get(0))) {
                return; // if at least one statement with such actor has been found
            }
        }
        Assert.assertTrue("Statement with such actor hasn't been found.", false);
    }
    public void checkNoStatementIsShownForUser(List<String> user) throws Exception {
        waitForPageToLoad();
        // Get list of statements
        List<WebElement> statements = webDriver.findElements(By.cssSelector(".statement-row"));
        // For each statement find its actor
        for (WebElement statement : statements) {
            String actor = statement.getText();
            String actorActual = actor.split(" ")[0]; // split by space
            // Check whether the actor equals to expected one
            if (actorActual.equals(user.get(0))) {
                Assert.assertTrue("Statement with such actor has been found.", false);
            }
        }
    }
    public void checkOnlyOnePageIsShownOnDashboard() throws Exception {
        waitForPageToLoad();
        List<WebElement> pages = webDriver.findElements(By.cssSelector("#list_most_viewed_pages tr a"));
        Assert.assertTrue("0 or more than 1 pages are shown on Dashboard page.", pages.size() == 1);
    }
    public void checkOnlyOneUserIsShownInMostActiveList() throws Exception {
        waitForPageToLoad();
        List<WebElement> users = webDriver.findElements(By.cssSelector("#list_active_users a"));
        Assert.assertTrue("0 or more than 1 users are shown in 'Most active users'.", users.size() == 1);
    }
    public void checkOnlyOneUserIsShownInAllUsersTable() throws Exception {
        waitForPageToLoad();
        List<WebElement> users = webDriver.findElements(By.cssSelector(".vau-table a"));
        Assert.assertTrue("0 or more than 1 users are shown in 'All users' table.", users.size() == 1);
    }
    public void checkNumberOfPagesWhichShownInCourseTree(int n) throws Exception { // pages with 'depth=1 and 2' are counted
        waitForPageToLoad();
        List<WebElement> pages = webDriver.findElements(By.cssSelector("#list_pages tr[depth='1'] , tr[depth='2']"));
        int nActual = pages.size();
        Assert.assertTrue("Another amount of pages is shown in course tree: " + nActual, pages.size() == n);
    }
    public void openCourseTreeFromAllUsersPage(WebElement userLink) throws Exception {
        waitUntilElementIsClickable(userLink);
        clickItem(userLink, "The user link couldn't be clicked on 'All users' page.");
        waitForPageToLoad();
    }
    public void checkShowEverythingIsSelected() throws Exception {
        AddFiltersLLPage addFiltersLLPage = new AddFiltersLLPage(webDriver, pageLocation);
        waitUntilElementIsClickable(addFiltersLLPage.getRadiobuttonShowEverything());
        WebElement radiobutton = addFiltersLLPage.getRadiobuttonShowEverything();
        if (!(radiobutton.isSelected())) {
            clickItem(radiobutton, "The radiobutton 'Show everything' on the AddFilters page could not be clicked.");
        }
        Assert.assertTrue("Radiobutton 'Show everything' on the AddFilters page is not selected.", radiobutton.isSelected());
    }
    public void selectFilteringDate(WebElement field, int days) throws Exception {
        waitUntilElementIsClickable(field);
        field.click();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".datepicker-days"))); // datepicker is shown

        // Select other date
        Calendar currentDate = Calendar.getInstance();
        currentDate.add(Calendar.DATE, days); // int day - is quantity of days
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String otherDate = format.format(currentDate.getTime());
        field.clear();
        field.sendKeys(otherDate);
    }
    public void resetCurrentFilter() throws Exception {
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickIconResetFilter();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOf(mainDashboardLLPage.getButtonAddFiltering()));
        waitForPageToLoad();
    }
















}
