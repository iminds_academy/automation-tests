package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.learningLocker.AllUsersLLPage;
import com.inceptum.pages.learningLocker.CourseContentLLPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerCommonTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected String urlClassOverviewPage = "";


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerCommonClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();


        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);

        quitWebDriver();
    }

    @Before
    public void beforeTest2() throws Exception {
        // Delete all classes with such name
        deleteClass(className);
        // Clean LRS
        loginToLL();
        cleanLRS(urlLRS);
        // Create a new Class
        urlClassOverviewPage = addClass(className); // get url of Overview Class page
    }


    @Test // LL-158 ("Group page is reflected in LL")
    public void testStatsForGroup() throws Exception {
        // Check that Group content type is not selected on Tin Can API page
        checkGroupIsNotSelectedInTinCanApi();
        // Add a group to the Class
        createGroup(groupName, className);

        // Run cron
        runCron();
        // Clear cache
        webDriver.get(urlLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        selectClassInLRS(className);
        clearCache(urlLRS);

        // Check that the Group page isn't displayed on MainDashboard, general stats is 0
        mainDashboardLLPage.clickLinkMainDashboard();
        checkGeneralStatsFromDashboardPage(0, 0, 0); // !!!!
        checkPageIsNotShownInMostViewedList(groupName);  // !!!!
    }

    @Test
    public void testViewAllUsers() throws Exception {
        // Open AllUsers page with 'View all users' link from MainDashboard page
        webDriver.get(urlLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AllUsersLLPage allUsersLLPage = mainDashboardLLPage.clickLinkViewAllUsers();
        // Check that no user is displayed in the table yet
        String actualStats = allUsersLLPage.getPaginationStatsField().getText().trim();
        Assert.assertTrue("Wrong viewing stats is displayed on AllUsers page.", actualStats.equals("Viewing 0 - 0 of 0 users"));
        checkNoRowInTable(allUsersLLPage.getTableUsers());

        // Add a new user to the Class
        addAuthenticatedUser(student);
        addUserToClass(student, urlClassOverviewPage);
        // Add Task to the Class
        webDriver.get(urlClassOverviewPage);
        addTaskToClass(); // here admin is viewing the node
        // Masquerade as user, view the node
        masqueradeAsUser(student);
        switchBackFromMasqueradingAs(student, admin);

        // Run cron
        runCron();
        // Clear cache
        webDriver.get(urlLRS);
        selectClassInLRS(className);
        clearCache(urlLRS);

        // Click 'View all users' link from CourseContent page
        CourseContentLLPage courseContentLLPage = mainDashboardLLPage.clickLinkCourseContent();
        courseContentLLPage.clickLinkViewAllUsers();
        // Check both users are displayed in 'All users' table
        checkUserIsDisplayedInAllUsersTable(admin);
        checkUserIsDisplayedInAllUsersTable(student);
        actualStats = allUsersLLPage.getPaginationStatsField().getText().trim();
        Assert.assertTrue("Wrong viewing stats is displayed on AllUsers page.", actualStats.equals("Viewing 0 - 2 of 2 users"));
    }

    @Test // LL-276, PILOT-1621 !!!
    public void testCheckParentChildForDuplicatedMaterial() throws Exception {
        // Create Presentation in the Class
        String presentationTitle = addPresentationToClass();
        String fullPresentationTitle = "Presentation: " + presentationTitle;

        // Create Task in the Class
        webDriver.get(urlClassOverviewPage);
        String titleTask = addTaskToClass(); // add Task
        String fullTaskTitle = "Task: " + titleTask;
        // Add the Presentation to the Task
        openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        String chapterTask = editTaskPage.getChapterName(); // get chapter of the Task
        editTaskPage.switchToTabTaskResources();
        autoSelectItemFromList(editTaskPage.getFieldPresentationsTaskResorses(), presentationTitle);
        editTaskPage.clickButtonSave();
        // View the Presentation from Task
        clickMaterialByName(presentationTitle);

        // Create Theory in the Class
        webDriver.get(urlClassOverviewPage);
        String titleTheory = addTheoryToClass();
        String fullTheoryTitle = "Theory: " + titleTheory;
        // Add the Presentation to the Theory
        openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        String chapterTheory = editTheoryPage.getChapterName(); // get chapter of the Theory
        editTheoryPage.switchToTabCourseMaterial();
        autoSelectItemFromList(editTheoryPage.getFieldPresentationsCourseMaterial(), presentationTitle);
        editTheoryPage.clickButtonSave();
        // View the Presentation from Theory
        clickMaterialByName(presentationTitle);

        // Run cron
        runCron();
        // Clear cache
        webDriver.get(urlLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        selectClassInLRS(className);
        clearCache(urlLRS);

        // Check general stats on admin detailed page
        mainDashboardLLPage.clickLinkUsers();
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(100, 0, 4); // ??? check after LL-276 is resolved
        // Task is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask); // PILOT-1621 !!!
        // Presentation is displayed as a child of the Task
        WebElement presentationPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullPresentationTitle);
        checkStatsForPageOnUserDetailed(presentationPage, 3, true);

        // Theory is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // PILOT-1621 !!!
        // Presentation is displayed as a child of the Theory
        presentationPage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullPresentationTitle);
        checkStatsForPageOnUserDetailed(presentationPage, 3, true);
        checkMaterialIsDuplicate(presentationPage); // duplicate

        // Presentation is shown in Course chapter as a duplicate
        presentationPage = getDuplicate(fullPresentationTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(presentationPage, 3, true);
        checkPageIsGreyedOut(presentationPage); // greyed out
        checkNumberOfPagesWhichShownInCourseTree(5);
    }



















}
