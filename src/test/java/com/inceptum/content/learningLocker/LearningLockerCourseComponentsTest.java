package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerCourseComponentsTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/




     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerCourseComponentsClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        courseBaseTest.addClass(className);
        // Create each type of Course Components, get their chapters
        chapterLiveSession = courseBaseTest.createLiveSession();
        chapterTask = courseBaseTest.createTask();
        chapterTheory = courseBaseTest.createTheory();
        chapterInfoPage = courseBaseTest.createInfoPage();
        chapterMediaPage = courseBaseTest.createMediaPage();
        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);


        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test
    public void testLiveSessionStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Live session is displayed on Dashboard page
        String fullLiveSessionTitle = "Live session: " + liveSessionTitle; // get full Live session title
        checkPageIsShownInMostViewedList(fullLiveSessionTitle, 2, 1); // its stats
        // Check the Live session is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullLiveSessionTitle, chapterLiveSession, 2, 1, 100); // its stats
        // Check stats of the Live session on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullLiveSessionTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        // Check user is shown on the page, open the user's course tree
        checkUserIsShownOnDetailedPage(admin);
        clickUserLinkFromMostActive(admin);
        // Check the Live session is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullLiveSessionTitle, chapterLiveSession);
        checkStatsForPageOnUserDetailed(page, 2, true);
    }

    @Test
    public void testTaskStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Task is displayed on Dashboard page
        String fullTaskTitle = "Task: " + taskTitle; // get full Task title
        checkPageIsShownInMostViewedList(fullTaskTitle, 2, 1); // its stats
        // Check the Task is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullTaskTitle, chapterTask, 2, 1, 100); // its stats
        // Check stats of the Task on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullTaskTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        // Check user is shown on the page, open the user's course tree
        checkUserIsShownOnDetailedPage(admin);
        clickUserLinkFromMostActive(admin);
        // Check the Task is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask);
        checkStatsForPageOnUserDetailed(page, 2, true);
    }

    @Test
    public void testTheoryStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Theory is displayed on Dashboard page
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title
        checkPageIsShownInMostViewedList(fullTheoryTitle, 2, 1); // its stats
        // Check the Theory is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 2, 1, 100); // its stats
        // Check stats of the Theory on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        // Check user is shown on the page, open the user's course tree
        checkUserIsShownOnDetailedPage(admin);
        clickUserLinkFromMostActive(admin);
        // Check the Theory is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory);
        checkStatsForPageOnUserDetailed(page, 2, true);
    }

    @Test
    public void testMediaPageStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Media page is displayed on Dashboard page
        String fullMediaPageTitle = "Media page: " + mediaPageTitle; // get full Media page title
        checkPageIsShownInMostViewedList(fullMediaPageTitle, 2, 1); // its stats
        // Check the Media page is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullMediaPageTitle, chapterMediaPage, 2, 1, 100); // its stats
        // Check stats of the Media page on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullMediaPageTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        // Check user is shown on the page, open the user's course tree
        checkUserIsShownOnDetailedPage(admin);
        clickUserLinkFromMostActive(admin);
        // Check the Media page is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage);
        checkStatsForPageOnUserDetailed(page, 2, true);
    }

    @Test
    public void testInfoPageStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Info page is displayed on Dashboard page
        String fullInfoPageTitle = "Info page: " + infoPageTitle; // get full Info page title
        checkPageIsShownInMostViewedList(fullInfoPageTitle, 2, 1); // its stats
        // Check the Info page is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullInfoPageTitle, chapterInfoPage, 2, 1, 100); // its stats
        // Check stats of the Info page on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullInfoPageTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        // Check user is shown on the page, open the user's course tree
        checkUserIsShownOnDetailedPage(admin);
        clickUserLinkFromMostActive(admin);
        // Check the Info page is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullInfoPageTitle, chapterInfoPage);
        checkStatsForPageOnUserDetailed(page, 2, true);
    }

    @Test
    public void testCourseComponentsGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check General statistics from Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0);
        // Check General statistics from 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(10, 1, 5);
        // Check General statistics from Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(10, 1, 5);
        checkCourseProgressForUserOnUsersPage(admin, 100);
        // Check General statistics from user's detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(100, 0, 5);
    }













}
