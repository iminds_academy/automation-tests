package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerCourseComponentsWithMaterialsTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected static String imageTitle4 = "";
    protected static String titleMediaPage = "";


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerCourseComponentsWithMaterialsClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        String urlClassOverviewPage = courseBaseTest.addClass(className);
        // Create LiveSession, add 3 different Course materials to the LS
        chapterLiveSession = courseBaseTest.createLiveSession();
        courseBaseTest.openEditPage();
        learningLockerBaseTest.addCourseMaterialsToLiveSession(presentationTitle, videoTitle, imageTitle);
        courseBaseTest.clickMaterialByName(presentationTitle); // view presentation
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(videoTitle); // view video
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(imageTitle); // view image

        // Create Theory, add 3 different Additional materials to the Theory
        chapterTheory = courseBaseTest.createTheory();
        courseBaseTest.openEditPage();
        learningLockerBaseTest.addAdditionalMaterialsToTheory(presentationTitle2, videoTitle2, imageTitle2);
        courseBaseTest.clickMaterialByName(presentationTitle2); // view presentation
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(videoTitle2); // view video
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(imageTitle2); // view image

        // Create Task, add Presentation to Task resources, add Image to Additional resources
        chapterTask = courseBaseTest.createTask();
        courseBaseTest.openEditPage();
        learningLockerBaseTest.addPresentationAndAdditionalImageToTask(presentationTitle3, imageTitle3);
        courseBaseTest.clickMaterialByName(imageTitle3); // view image
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(presentationTitle3); // view presentation

        // Create Image, create Media page; add the Image to the Media page with 'autocomplete'
        webDriver.get(urlClassOverviewPage);
        imageTitle4 = courseBaseTest.addImageToClass(); // add Image
        webDriver.get(urlClassOverviewPage);
        titleMediaPage = courseBaseTest.addMediaPageToClass(); // add Media page
        courseBaseTest.openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        chapterMediaPage = courseBaseTest.getNameOfSelectedOption(editMediaPage.getFieldChapter()); // get chapter name
        editMediaPage.switchToTabCourseMaterial();
        courseBaseTest.autoSelectItemFromList(editMediaPage.getFieldImagesCourseMaterial(), imageTitle4);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        // View the Image from Media page
        viewMediaPage.clickLinkLinks(); // open Links popup
        courseBaseTest.clickMaterialByName(imageTitle4); // view image

        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test
    public void testLiveSessionWithCourseMaterials() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Live session is displayed on Dashboard page
        String fullLiveSessionTitle = "Live session: " + liveSessionTitle; // get full Live session title
        checkPageIsShownInMostViewedList(fullLiveSessionTitle, 3, 1); // its stats

        // Check the Live session is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullLiveSessionTitle, chapterLiveSession, 3, 1, 100); // its stats
        // Check that course materials pages aren't shown on Course content page
        String fullPresentationTitle = "Presentation: " + presentationTitle; // get full Presentation title
        String fullVideoTitle = "Video: " + videoTitle; // get full Video title
        String fullImageTitle = "Image: " + imageTitle; // get full Image title
        checkPageIsNotShownInAllPagesCourseContentList(fullPresentationTitle);
        checkPageIsNotShownInAllPagesCourseContentList(fullVideoTitle);
        checkPageIsNotShownInAllPagesCourseContentList(fullImageTitle);

        // Open LiveSession detailed page, check stats for each material from 'Course material' block
        openCourseDetailedPageFromCourseContentTable(fullLiveSessionTitle);
        checkMaterialIsShownInCourseMaterialBlock(fullPresentationTitle, 1, 1, 100); // Presentation stats
        checkMaterialIsShownInCourseMaterialBlock(fullVideoTitle, 1, 1, 100); // Video stats
        checkMaterialIsShownInCourseMaterialBlock(fullImageTitle, 1, 1, 100); // Image stats

        // Open Presentation detailed page, check stats
        openCourseMaterialFromComponentDetailedPage(fullPresentationTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Open admin's course tree
        clickUserLinkFromMostActive(admin);
        checkPageIsDisplayedOnUserDetailedPage(fullLiveSessionTitle, chapterLiveSession); // the LiveSession is displayed in proper chapter
        WebElement presentationPage = checkPageIsDisplayedAsChildOf(fullLiveSessionTitle, fullPresentationTitle);
        checkStatsForPageOnUserDetailed(presentationPage, 1, true); // Presentation stats
        WebElement videoPage = checkPageIsDisplayedAsChildOf(fullLiveSessionTitle, fullVideoTitle);
        checkStatsForPageOnUserDetailed(videoPage, 1, true); // Video stats
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullLiveSessionTitle, fullImageTitle);
        checkStatsForPageOnUserDetailed(imagePage, 1, true); // Image stats

        // Open Video detailed page, check stats
        openCourseDetailedPageFromUserDetailedTable(fullVideoTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page
    }

    @Test // LL-189, LL-203 !!!!!
    public void testTheoryWithAdditionalMaterials() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Theory is displayed on Dashboard page
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title
        checkPageIsShownInMostViewedList(fullTheoryTitle, 3, 1); // its stats

        // Check the Theory is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 3, 1, 100); // its stats
        // Check that course materials pages aren't shown on Course content page
        String fullPresentationTitle = "Presentation: " + presentationTitle2; // get full Presentation title
        String fullVideoTitle = "Video: " + videoTitle2; // get full Video title
        String fullImageTitle = "Image: " + imageTitle2; // get full Image title
        checkPageIsNotShownInAllPagesCourseContentList(fullPresentationTitle);
        checkPageIsNotShownInAllPagesCourseContentList(fullVideoTitle);
        checkPageIsNotShownInAllPagesCourseContentList(fullImageTitle);

        // Open Theory detailed page, check stats for each material from 'Additional material' block
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle);
        checkMaterialIsShownInAdditionalMaterialBlock(fullPresentationTitle, 1, 1, 100); // Presentation stats
        checkMaterialIsShownInAdditionalMaterialBlock(fullVideoTitle, 1, 1, 100); // Video stats
        checkMaterialIsShownInAdditionalMaterialBlock(fullImageTitle, 1, 1, 100); // Image stats

        // Open Video detailed page, check stats
        openAdditionalMaterialFromComponentDetailedPage(fullVideoTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Open admin's course tree
        clickUserLinkFromMostActive(admin); // !!!!!
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // the Theory is displayed in proper chapter
        WebElement presentationPage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullPresentationTitle); // !!!!
        checkStatsForPageOnUserDetailed(presentationPage, 1, true); // Presentation stats
        WebElement videoPage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullVideoTitle);
        checkStatsForPageOnUserDetailed(videoPage, 1, true); // Video stats
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullImageTitle);
        checkStatsForPageOnUserDetailed(imagePage, 1, true); // Image stats

        // Open Image detailed page, check stats
        openCourseDetailedPageFromUserDetailedTable(fullImageTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page
    }

    @Test
    public void testTaskWithResources() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Task is displayed on Dashboard page
        String fullTaskTitle = "Task: " + taskTitle; // get full Task title
        checkPageIsShownInMostViewedList(fullTaskTitle, 3, 1); // its stats

        // Check the Task is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullTaskTitle, chapterTask, 3, 1, 100); // its stats
        // Check that resources pages aren't shown on Course content page
        String fullPresentationTitle = "Presentation: " + presentationTitle3; // get full Presentation title
        String fullImageTitle = "Image: " + imageTitle3; // get full Image title
        checkPageIsNotShownInAllPagesCourseContentList(fullPresentationTitle);
        checkPageIsNotShownInAllPagesCourseContentList(fullImageTitle);

        // Open Task detailed page, check stats for each material
        openCourseDetailedPageFromCourseContentTable(fullTaskTitle);
        checkMaterialIsShownInCourseMaterialBlock(fullPresentationTitle, 1, 1, 100); // Presentation stats
        checkMaterialIsShownInAdditionalMaterialBlock(fullImageTitle, 1, 1, 100); // Image stats

        // Open Presentation detailed page, check stats
        openCourseMaterialFromComponentDetailedPage(fullPresentationTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Open admin's course tree
        clickUserLinkFromMostActive(admin);
        checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask); // the Task is displayed in proper chapter
        WebElement presentationPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullPresentationTitle);
        checkStatsForPageOnUserDetailed(presentationPage, 1, true); // Presentation stats
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullImageTitle);  // !!!!
        checkStatsForPageOnUserDetailed(imagePage, 1, true); // Image stats

        // Open Image detailed page, check stats
        openCourseDetailedPageFromUserDetailedTable(fullImageTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page
    }

    @Test // LL-39 ("Two viewed statements"), LL-143 (""Repeated materials)
    public void testMediaPageWithMaterialAutocompleted() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check that Media and Image pages are displayed on Dashboard page
        String fullMediaPageTitle = "Media page: " + titleMediaPage; // get full Media page title
        String fullImageTitle = "Image: " + imageTitle4; // get full Image title
        checkPageIsShownInMostViewedList(fullMediaPageTitle, 2, 1); // Media page stats
        checkPageIsShownInMostViewedList(fullImageTitle, 2, 1); // Image stats  // !!!!!!!

        // Check that Media page is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullMediaPageTitle, chapterMediaPage, 2, 1, 100); // its stats
        // Check that the Image isn't shown on Course content page
        checkPageIsNotShownInAllPagesCourseContentList(fullImageTitle);

        // Open detailed Media page, check stats for Image
        openCourseDetailedPageFromCourseContentTable(fullMediaPageTitle);
        checkMaterialIsShownInCourseMaterialBlock(fullImageTitle, 1, 1, 100); // Image stats // ???

        // Open Image detailed page, check stats
        openCourseMaterialFromComponentDetailedPage(fullImageTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100); // !!!
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Open admin's course tree
        clickUserLinkFromMostActive(admin);
        checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage); // the Media page is displayed in proper chapter
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullMediaPageTitle, fullImageTitle);
        checkStatsForPageOnUserDetailed(imagePage, 2, true); // Image stats // ???
        // Check the Image is also shown separately in 'Course' chapter
        WebElement imagePageSeparated = checkPageIsDisplayedOnUserDetailedPage(fullImageTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(imagePageSeparated, 2, true); // Image stats // ???
    }

    @Test // !!!!
    public void testCourseComponentsWithMaterialsGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check General statistics from Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0);
        // Check General statistics from 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(21, 1, 13); // ???
        // Check General statistics from Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(21, 1, 13); // ???
        checkCourseProgressForUserOnUsersPage(admin, 100);
        // Check General statistics from user's detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(100, 0, 13); // ???
    }















}
