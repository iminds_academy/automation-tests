package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerCourseMaterialsTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected static String chapterCourse = "Course";


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerCourseMaterialsClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        courseBaseTest.addClass(className);
        // Create each type of Course Components, get their chapters
        courseBaseTest.createImageFromContentPage(image1Path);
        courseBaseTest.createVideoFromContentPage();
        courseBaseTest.createPresentationFromContentPage();
        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test // LL-39 ("Two 'viewed' statements are generated"); LL-196 ("Content names are shown arbitrarily") !!!!!
    public void testImageStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Image is displayed on Dashboard page
        String fullImageTitle = "Image: " + imageTitle; // get full Image title
        checkPageIsShownInMostViewedList(fullImageTitle, 2, 1); // its stats        !!!!!!!!!!!!!
        // Check the Image is not displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsNotShownInAllPagesCourseContentList(fullImageTitle);
        // Click user's link from 'Most active users'
        clickUserLinkFromMostActive(admin);
        // Check the Image is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullImageTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(page, 2, true); // !!!!!!!!!
        // Check Image stats from its detailed page
        openCourseDetailedPageFromUserDetailedTable(fullImageTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
    }

    @Test // LL-39 ("Two 'viewed' statements are generated"); LL-196 ("Content names are shown arbitrarily") !!!!!
    public void testPresentationStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Presentation is displayed on Dashboard page
        String fullPresentationTitle = "Presentation: " + presentationTitle; // get full Presentation title
        checkPageIsShownInMostViewedList(fullPresentationTitle, 2, 1); // its stats        !!!!!!!!!!!!!
        // Check the Presentation is not displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsNotShownInAllPagesCourseContentList(fullPresentationTitle);
        // Click user's link from 'Most active users'
        clickUserLinkFromMostActive(admin);
        // Check the Presentation is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullPresentationTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(page, 2, true); // !!!!!!!!!
        // Check Presentation stats from its detailed page
        openCourseDetailedPageFromUserDetailedTable(fullPresentationTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
    }

    @Test // LL-39 ("Two 'viewed' statements are generated"); LL-196 ("Content names are shown arbitrarily") !!!!!
    public void testVideoStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Video is displayed on Dashboard page
        String fullVideoTitle = "Video: " + videoTitle; // get full Video title
        checkPageIsShownInMostViewedList(fullVideoTitle, 2, 1); // its stats        !!!!!!!!!!!!!
        // Check the Video is not displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsNotShownInAllPagesCourseContentList(fullVideoTitle);
        // Click user's link from 'Most active users'
        clickUserLinkFromMostActive(admin);
        // Check the Video is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullVideoTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(page, 2, true); // !!!!!!!!!
        // Check Video stats from its detailed page
        openCourseDetailedPageFromUserDetailedTable(fullVideoTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        checkNoContentOnDetailedPage(); // check that inner video isn't displayed on the page
    }

    @Test // LL-39 ("Two 'viewed' statements are generated"); LL-199 ("Inner video is still visible") !!!!!!
    public void testCourseMaterialsGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check General statistics from Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0);
        // Check General statistics from 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(6, 1, 3); // !!!!!!
        // Check General statistics from Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(6, 1, 3);
        checkCourseProgressForUserOnUsersPage(admin, 100);
        // Check General statistics from user's detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(100, 0, 3);
    }
















}
