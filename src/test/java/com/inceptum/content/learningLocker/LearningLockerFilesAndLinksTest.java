package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerFilesAndLinksTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected static String titleMediaPage = "";
    protected static String titleLiveSession = "";


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerFilesAndLinksClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        String urlClassOverviewPage = courseBaseTest.addClass(className);
        // Create Media page, add links to Course and Additional materials
        titleMediaPage = courseBaseTest.addMediaPageToClass(); // add Media page
        courseBaseTest.openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        chapterMediaPage = courseBaseTest.getNameOfSelectedOption(editMediaPage.getFieldChapter()); // get chapter name
        editMediaPage.switchToTabCourseMaterial(); // Course materials
        editMediaPage.fillFieldLinksTitle(linkTitle); // Link1
        editMediaPage.fillFieldLinksUrl(linkUrl);
        editMediaPage.switchToTabAdditionalMaterial(); // Additional materials
        editMediaPage.fillFieldLinksTitleAdditional(linkTitle2); // Link2
        editMediaPage.fillFieldLinksUrlAdditional(linkUrl2);
        editMediaPage.clickCheckboxOpenInNewWindow(); // remove selection 'Open in a new window' by clicking the checkbox
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        // View the Links from Media page
        viewMediaPage.clickLinkLinks(); // open Links popup
        WebElement link = learningLockerBaseTest.getLinkByName(linkTitle);
        learningLockerBaseTest.viewMaterialInAnotherBrowserTab(link); // visit Link1 in another browser tab
        String urlActual = webDriver.getCurrentUrl();
        viewMediaPage.clickLinkAdditionalMaterialByName(linkTitle2); // visit Link2 at the same window
        webDriver.get(urlActual);
        waitForPageToLoad();

        // Create Live Session, add files to Course and Additional materials
        webDriver.get(urlClassOverviewPage);
        titleLiveSession = courseBaseTest.addLiveSessionToClass();
        courseBaseTest.openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        chapterLiveSession = courseBaseTest.getNameOfSelectedOption(editLiveSessionPage.getFieldChapter()); // get chapter name
        editLiveSessionPage.switchToTabCourseMaterial(); // Course material
        editLiveSessionPage.fillFieldDownloadsTitle(fileTitle); // File1
        editLiveSessionPage.uploadFileToDownloads(docxFilePath);
        editLiveSessionPage.switchToTabAdditionalMaterial(); // Additional material
        editLiveSessionPage.fillFieldDownloadsTitleAdditional(fileTitle2); // File2
        editLiveSessionPage.uploadFileToDownloadsAdditional(xlsxFilePath);
        editLiveSessionPage.clickButtonSave();
        learningLockerBaseTest.clickLinkDownloadedCourseMaterialByName(fileTitle); // download File1
        learningLockerBaseTest.waitUntilFileDownloaded();
        learningLockerBaseTest.clickLinkDownloadedCourseMaterialByName(fileTitle2); // download File2
        learningLockerBaseTest.waitUntilFileDownloaded();

        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test // LL-166 ("Link is not shown on Dashboard"); LL-106 ("...in Course block"); LL-193 (0 pageviews); LL-103 (title)
    public void testLinksStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check that Media page and two links are displayed on Dashboard page
        String fullMediaPageTitle = "Media page: " + titleMediaPage; // get full Media page title
        checkPageIsShownInMostViewedList(fullMediaPageTitle, 3, 1); // Media page stats
        String fullLinkTitle1 = "Link: " + linkTitle; // get full Link1 title     // LL-103 !!!!
        checkPageIsShownInMostViewedList(fullLinkTitle1, 1, 1); // Link1 stats    // LL-166 !!!!
        String fullLinkTitle2 = "Link: " + linkTitle2; // get full Link2 title    // LL-103 !!!!
        checkPageIsShownInMostViewedList(fullLinkTitle2, 1, 1); // Link2 stats    // LL-166 !!!!
        // Check that the links are not shown on Course content page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsNotShownInAllPagesCourseContentList(fullLinkTitle1);
        checkPageIsNotShownInAllPagesCourseContentList(fullLinkTitle2);
        // Check the links are displayed in proper blocks of materials on Detailed Media page
        openCourseDetailedPageFromCourseContentTable(fullMediaPageTitle);
        checkMaterialIsShownInCourseMaterialBlock(fullLinkTitle1, 1, 1, 100); // Link1 stats - Course materials
        checkMaterialIsShownInAdditionalMaterialBlock(fullLinkTitle2, 1, 1, 100); // Link2 stats - Additional materials // LL-106 !!!

        // Open admin's course tree
        clickUserLinkFromMostActive(admin);
        checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage); // the Media page is displayed in proper chapter
        WebElement link1 = checkPageIsDisplayedAsChildOf(fullMediaPageTitle, fullLinkTitle1);
        checkStatsForPageOnUserDetailed(link1, 1, true); // Link1 stats
        WebElement link2 = checkPageIsDisplayedAsChildOf(fullMediaPageTitle, fullLinkTitle2);
        checkStatsForPageOnUserDetailed(link2, 1, true); // Link2 stats
        // Check stats from link detailed page
        openCourseDetailedPageFromUserDetailedTable(fullLinkTitle1);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100); // LL-193 !!!!
        checkNoContentOnDetailedPage(); // no inner content is shown on the page
    }

    @Test // LL-166 ("Link is not shown on Dashboard"); LL-106 ("...in Course block"); LL-193 (0 pageviews); LL-103 (title)
    public void testFilesStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check that Live session and two files are displayed on Dashboard page
        String fullLiveSessionTitle = "Live session: " + titleLiveSession; // get full Live session title
        checkPageIsShownInMostViewedList(fullLiveSessionTitle, 2, 1); // Live session stats
        String fullFileTitle1 = "File: " + fileTitle; // get full File1 title     // LL-103 !!!!
        checkPageIsShownInMostViewedList(fullFileTitle1, 1, 1); // File1 stats    // LL-166 !!!!
        String fullFileTitle2 = "File: " + fileTitle2; // get full File2 title    // LL-103 !!!!
        checkPageIsShownInMostViewedList(fullFileTitle2, 1, 1); // File2 stats    // LL-166 !!!!
        // Check that the files are not shown on Course content page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsNotShownInAllPagesCourseContentList(fullFileTitle1);
        checkPageIsNotShownInAllPagesCourseContentList(fullFileTitle2);
        // Check the files are displayed in proper blocks of materials on Detailed Live session page
        openCourseDetailedPageFromCourseContentTable(fullLiveSessionTitle);
        checkMaterialIsShownInCourseMaterialBlock(fullFileTitle1, 1, 1, 100); // File1 stats - Course materials
        checkMaterialIsShownInAdditionalMaterialBlock(fullFileTitle2, 1, 1, 100); // File2 stats - Additional materials // LL-106 !!!

        // Open admin's course tree
        clickUserLinkFromMostActive(admin);
        checkPageIsDisplayedOnUserDetailedPage(fullLiveSessionTitle, chapterLiveSession); // the Live session is displayed in proper chapter
        WebElement file1 = checkPageIsDisplayedAsChildOf(fullLiveSessionTitle, fullFileTitle1);
        checkStatsForPageOnUserDetailed(file1, 1, true); // File1 stats
        WebElement file2 = checkPageIsDisplayedAsChildOf(fullLiveSessionTitle, fullFileTitle2);
        checkStatsForPageOnUserDetailed(file2, 1, true); // File2 stats
        // Check stats from file detailed page
        openCourseDetailedPageFromUserDetailedTable(fullFileTitle2);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100); // LL-193 !!!!
        checkNoContentOnDetailedPage(); // no inner content is shown on the page
    }

    @Test // !!!
    public void testFilesAndLinksGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check General statistics from Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0);
        // Check General statistics from 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(9, 1, 6); // ???
        // Check General statistics from Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(9, 1, 6); // ???
        checkCourseProgressForUserOnUsersPage(admin, 100);
        // Check General statistics from user's detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(100, 0, 6); // ???
    }


















}
