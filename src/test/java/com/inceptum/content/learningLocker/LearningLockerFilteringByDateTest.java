package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.learningLocker.AddFiltersLLPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerFilteringByDateTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/



     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerFilteringByDateClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.addClass(className);
        // Create Live session
        chapterLiveSession = courseBaseTest.createLiveSession();
        nodeLiveSession = courseBaseTest.getNodeFromViewComponentPage();

        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test
    public void testFilterByDate() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by date (tab 'By date')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        selectFilteringDate(addFiltersLLPage.getFieldEndDate(), -1); // select yesterday's day
        applyFilter(filterDate);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(0, 0, 0);

        // Add filtering where 'startDay = today'
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        selectFilteringDate(addFiltersLLPage.getFieldStartDate(), 0); // select today for startDate
        applyFilter(filterDate);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0);
        // The LS is displayed
        String fullLiveSessionTitle = "Live session: " + liveSessionTitle;
        checkPageIsShownInMostViewedList(fullLiveSessionTitle, 2, 1); // PILOT-1621 !!!
        // Check General stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(2, 1, 1);

        // Clear the dates with proper link (from popup)
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        addFiltersLLPage.clickLinkClearDates();
        addFiltersLLPage.clickButtonApplyFilters();

        // Check that the button 'Add filtering' is displayed
        Assert.assertTrue("The button 'Add filtering' is not displayed.",
                mainDashboardLLPage.getButtonAddFiltering().isDisplayed());
        checkCourseProgressForUserOnUsersPage(admin, 100);
        mainDashboardLLPage.clickLinkMainDashboard();
        checkGeneralStatsFromDashboardPage(100, 100, 0); // stats is shown
    }

    @Test
    public void testResetFilteringByDate() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by date (tab 'By date')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        selectFilteringDate(addFiltersLLPage.getFieldEndDate(), -1); // select yesterday's day
        applyFilter(filterDate);

        // Check General statistics on Dashboard page after filtering
        checkGeneralStatsFromDashboardPage(0, 0, 0);

        // Reset the filter
        resetCurrentFilter();

        // Check General statistics on Dashboard page after reset filtering
        checkGeneralStatsFromDashboardPage(100, 100, 0);
    }

    @Test
    public void testCancelFilteringByDate() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Select date on 'Add filters'(tab 'By date')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        selectFilteringDate(addFiltersLLPage.getFieldEndDate(), -1); // select yesterday's day
        addFiltersLLPage.getTabByDate().click();
        // Click link Cancel
        addFiltersLLPage.clickLinkCancel();

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0); // - filtering hasn't been applied

        // Add filtering by date
        addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        selectFilteringDate(addFiltersLLPage.getFieldEndDate(), -1); // select yesterday's day
        applyFilter(filterDate);

        // Check stats after filtering
        checkGeneralStatsFromDashboardPage(0, 0, 0);

        // Click 'Clear dates' link on 'Add filters' popup
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        addFiltersLLPage.clickLinkClearDates();
        // Click link Cancel
        addFiltersLLPage.clickLinkCancel();

        // Check that the filter hasn't been removed
        checkGeneralStatsFromDashboardPage(0, 0, 0);
    }















}
