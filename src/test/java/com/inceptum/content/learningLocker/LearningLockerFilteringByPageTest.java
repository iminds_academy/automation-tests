package com.inceptum.content.learningLocker;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.ManageQuestionsQuizPage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.TakeQuizPage;
import com.inceptum.pages.ViewMediaPage;
import com.inceptum.pages.ViewQuizPage;
import com.inceptum.pages.learningLocker.AddFiltersLLPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.pages.learningLocker.MoviesLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerFilteringByPageTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected static String imageTitle2 = "";
    protected static String titleMediaPage = "";


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerFilteringByPageClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);
        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        courseBaseTest.deleteContentTypeFromTable("Multiple choice question"); // delete all MCQ
        String urlClassOverviewPage = courseBaseTest.addClass(className);
        // Create Live session
        chapterLiveSession = courseBaseTest.createLiveSession();
        nodeLiveSession = courseBaseTest.getNodeFromViewComponentPage();

        // Create Theory, add Image to Course materials, Video youtube - to Additional material
        chapterTheory = courseBaseTest.createTheory();
        nodeTheory = courseBaseTest.getNodeFromViewComponentPage();
        courseBaseTest.openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabCourseMaterial();
        nodeImage = learningLockerBaseTest.addImageToTheoryCourseMaterials(imageTitle);
        editTheoryPage.switchToTabAdditionalMaterial();
        nodeVideoYoutube = learningLockerBaseTest.addVideoYoutubeToTheoryAdditionalMaterial(videoTitle3);
        editTheoryPage.clickButtonSave();
        courseBaseTest.clickMaterialByName(imageTitle); // view image
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(videoTitle3); // view video

        // Create Task, add Video vimeo to Task resources, Presentation - to Additional resources
        chapterTask = courseBaseTest.createTask();
        nodeTask = courseBaseTest.getNodeFromViewComponentPage();
        courseBaseTest.openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        editTaskPage.switchToTabTaskResources();
        nodeVideoVimeo = learningLockerBaseTest.addVideoVimeoToTaskResources(videoTitle2);
        editTaskPage.switchToTabAdditionalResources();
        nodePresentation = learningLockerBaseTest.addPresentationToTaskAdditionalResources(presentationTitle2);
        editTaskPage.clickButtonSave();
        courseBaseTest.clickMaterialByName(videoTitle2); // view video
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        courseBaseTest.waitUntilVimeoVideoIsCompleted(); // watch till the end
        webDriver.switchTo().defaultContent();
        Thread.sleep(2000); // timeout for correct generating of video statements
        courseBaseTest.closePopup();
        courseBaseTest.clickMaterialByName(presentationTitle2); // view presentation

        // Create Info page
        chapterInfoPage = courseBaseTest.createInfoPage();
        nodeInfoPage = courseBaseTest.getNodeFromViewComponentPage();

        // Create Image, create Media page; add the Image to the Media page with 'autocomplete'
        webDriver.get(urlClassOverviewPage);
        imageTitle2 = courseBaseTest.addImageToClass(); // add Image
        webDriver.get(urlClassOverviewPage);
        titleMediaPage = courseBaseTest.addMediaPageToClass(); // add Media page
        nodeMediaPage = courseBaseTest.getNodeFromViewComponentPage();
        courseBaseTest.openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        chapterMediaPage = courseBaseTest.getNameOfSelectedOption(editMediaPage.getFieldChapter()); // get chapter name
        editMediaPage.switchToTabCourseMaterial();
        nodeImageDuplicate = courseBaseTest.autoSelectItemFromListAndGetNode(editMediaPage.getFieldImagesCourseMaterial(), imageTitle2);
        ViewMediaPage viewMediaPage = editMediaPage.clickButtonSave();
        // View the Image from Media page
        viewMediaPage.clickLinkLinks(); // open Links popup
        courseBaseTest.clickMaterialByName(imageTitle2); // view image

        // Create Video
        courseBaseTest.createVideoFromContentPage(); // 'videoTitle'
        nodeVideoSeparated = courseBaseTest.getNodeFromViewCoursePage();
        // Create Presentation
        courseBaseTest.createPresentationFromContentPage(); // 'presentationTitle'
        nodePresentationSeparated = courseBaseTest.getNodeFromViewCoursePage();

        // Create Quiz
        courseBaseTest.createQuiz();
        // Add a MCQ to the Quiz
        courseBaseTest.createMCQuestionFromManageQuestionTab(question1, alternativesName, 1);
        // Take the Quiz
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        ViewQuizPage viewQuizPage = manageQuestionsQuizPage.clickTabView();
        nodeQuiz = courseBaseTest.getNodeFromViewCoursePage();
        TakeQuizPage takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        takeQuizPage.selectVariant(1); // correct
        courseBaseTest.finishQuiz();


        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByLiveSessionPage() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Live session page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullLiveSessionTitle = "Live session: " + liveSessionTitle; // get full Live session title
        selectPageOnAddFiltersPopup(fullLiveSessionTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        // Course progress (100/10) - where 10 = CourseComponents + CourseMaterials (not greyed out) + AddMaterials
        checkGeneralStatsFromDashboardPage(10, 100, 0);
        // Check the Live session is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullLiveSessionTitle, 2, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats for Live session on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(2, 1, 1);
        checkPageIsShownInAllPagesCourseContentList(fullLiveSessionTitle, chapterLiveSession, 2, 1, 100); // its stats // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check stats on detailed Live session page
        openCourseDetailedPageFromCourseContentTable(fullLiveSessionTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(2, 1, 1);
        // Course progress (100/8) - where 8 = CourseComponents + CourseMaterials (not greyed out)
        checkCourseProgressForUserOnUsersPage(admin, 10); // 13 - correct value? // LL-270 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        // Course completed (100/8) - where 8 = CourseComponents + CourseMaterials (not greyed out)
        checkGeneralStatsFromUserDetailedPage(13, 0, 1);
        checkPageIsDisplayedOnUserDetailedPage(fullLiveSessionTitle, chapterLiveSession); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check that only statements related to the Live session are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeLiveSession); // LL-265 !!!
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByTheoryPage() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Theory page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title
        selectPageOnAddFiltersPopup(fullTheoryTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(10, 100, 0);
        // Check the Theory is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullTheoryTitle, 3, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats for Theory on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(3, 1, 1);
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 3, 1, 100); // its stats // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check stats on detailed Theory page
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(3, 1, 1);
        checkCourseProgressForUserOnUsersPage(admin, 10); // 13 - correct value? // LL-270 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(13, 0, 1);
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check that only statements related to the Theory are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeTheory);
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByImagePageCourseMaterial() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Image page (the Image is Course material of Theory)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullImageTitle = "Image: " + imageTitle; // get full Image title
        selectPageOnAddFiltersPopup(fullImageTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(20, 100, 0); // 10, 100, 0 - correct values !!!
        // Check the Image is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullImageTitle, 1, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(4, 1, 2);
        // Check the Image is displayed with Theory (parent content)
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title (parent)
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullImageTitle);
        // Theory stats
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 3, 1, 100); // PILOT-1621 !!!
        // Image stats
        checkStatsForPageOnCourseContent(imagePage, 1, 1, 100);

        // Check stats on detailed Image page
        String courseContentPageUrl = webDriver.getCurrentUrl();
        openCourseDetailedPageFromCourseContentTable(fullImageTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        // Check stats on detailed Theory page
        webDriver.get(courseContentPageUrl);
        waitForPageToLoad();
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkMaterialIsShownInCourseMaterialBlock(fullImageTitle, 1, 1, 100); // Image stats

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(4, 1, 2); // 1, 1 ,1 - correct values (counted without parent)
        checkCourseProgressForUserOnUsersPage(admin, 20); // 13 - correct value // LL-244 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(25, 0, 2); // (100/8)*2 - stats for Image + Theory
        // Theory is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // PILOT-1621 !!!
        // Image is displayed as a child of the Theory
        imagePage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullImageTitle);
        checkStatsForPageOnUserDetailed(imagePage, 1, true); // Image stats
        // Check that only two pages are displayed in the course tree
        checkNumberOfPagesWhichShownInCourseTree(2);

        // Check that only statements related to the Image are shown on Statements page (without parent)
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeImage); // LL-265 !!!!
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByVideoPageAdditionalMaterial() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Video page (the Video is Additional material of Theory)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullVideoTitle = "Video: " + videoTitle3; // get full Video title
        selectPageOnAddFiltersPopup(fullVideoTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(20, 100, 0); // 10, 100, 0 - correct values !!!
        // Check the Video is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullVideoTitle, 1, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(3, 1, 1);
        // Check that only Theory is displayed on the page
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title (parent)
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 3, 1, 100); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1);

        // Check stats on detailed Theory page
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkMaterialIsShownInAdditionalMaterialBlock(fullVideoTitle, 1, 1, 100); // Video stats

        // Check stats on detailed Video page
        openAdditionalMaterialFromComponentDetailedPage(fullVideoTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers(); // LL-270 !!!
        checkGeneralStatsFromUsersPage(3, 1, 1); // 0, 0 ,0 - !!! correct values (counted without parent,
                                                                       // without Additional material)
        checkCourseProgressForUserOnUsersPage(admin, 20); // 0 - correct value !!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(13, 0, 1); // additional material isn't shown in course tree
        // Theory is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1);

        // Check that only statements related to the Video are shown on Statements page (without parent)
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeVideoYoutube); // LL-265 !!!!
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByTaskPage() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Task page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullTaskTitle = "Task: " + taskTitle; // get full Task title
        selectPageOnAddFiltersPopup(fullTaskTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(10, 100, 0);
        // Check the Task is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullTaskTitle, 3, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats for Task on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(3, 1, 1);
        checkPageIsShownInAllPagesCourseContentList(fullTaskTitle, chapterTask, 3, 1, 100); // its stats // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check stats on detailed Task page
        openCourseDetailedPageFromCourseContentTable(fullTaskTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(3, 1, 1);
        checkCourseProgressForUserOnUsersPage(admin, 10); // 13 - correct value? // LL-270 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(13, 0, 1);
        checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check that only statements related to the Task are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeTask);
    }

    @Test // LL-244, PILOT-1621, LL-272 !!!!!!!!!!!!!!
    public void testApplyFilteringByVideoPageCourseMaterialViewedCompletely() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Video page (the Video is Task resource)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullVideoTitle = "Video: " + videoTitle2; // get full Video title
        selectPageOnAddFiltersPopup(fullVideoTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(20, 100, 0); // 10, 100, 0 - correct values !!!
        // Check the Video is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullVideoTitle, 1, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(4, 1, 2);
        // Check the Video is displayed with Task (parent content)
        String fullTaskTitle = "Task: " + taskTitle; // get full Task title (parent)
        WebElement videoPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullVideoTitle);
        // Task stats
        checkPageIsShownInAllPagesCourseContentList(taskTitle, chapterTask, 3, 1, 100); // PILOT-1621 !!!
        // Video stats
        checkStatsForPageOnCourseContent(videoPage, 1, 1, 100);

        // Check stats on detailed Video page
        String courseContentPageUrl = webDriver.getCurrentUrl();
        openCourseDetailedPageFromCourseContentTable(fullVideoTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);
        // Check stats on detailed Theory page
        webDriver.get(courseContentPageUrl);
        waitForPageToLoad();
        openCourseDetailedPageFromCourseContentTable(fullTaskTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkMaterialIsShownInCourseMaterialBlock(fullVideoTitle, 1, 1, 100); // Video stats

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(4, 1, 2); // 1, 1 ,1 - correct values (counted without parent)
        checkCourseProgressForUserOnUsersPage(admin, 20); // 13 - correct value // LL-244 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(25, 0, 2); // (100/8)*2 - stats for Video + Task
        // Task is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask); // PILOT-1621 !!!
        // Video is displayed as a child of the Task
        videoPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullVideoTitle);
        checkStatsForPageOnUserDetailed(videoPage, 1, true); // Video stats
        // Check that only two pages are displayed in the course tree
        checkNumberOfPagesWhichShownInCourseTree(2);

        // Check stats is displayed on Movies page
        MoviesLLPage moviesLLPage = mainDashboardLLPage.clickLinkMovies();
        List<Integer> fullyWatchedStats = Arrays.asList(1, 100);
        List<Integer> partialWatchedStats = Arrays.asList(1, 100);
        moviesLLPage.clickIconCogwheel(); // refresh stats by clicking on 'cogwheel' icon
        checkMovieIsShownWithStats(fullVideoTitle, fullyWatchedStats, partialWatchedStats); // LL-272 !!!

        // Check that only statements related to the Video are shown on Statements page (without parent)
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeVideoVimeo); // LL-265 !!!!
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByPresentationPageAdditionalMaterial() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Presentation page (the Presentation is Additional material of Task)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullPresentationTitle = "Presentation: " + presentationTitle2; // get full Presentation title
        selectPageOnAddFiltersPopup(fullPresentationTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(20, 100, 0); // 10, 100, 0 - correct values !!!
        // Check the Presentation is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullPresentationTitle, 1, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(3, 1, 1);
        // Check that only Task is displayed on the page
        String fullTaskTitle = "Task: " + taskTitle; // get full Task title (parent)
        checkPageIsShownInAllPagesCourseContentList(fullTaskTitle, chapterTask, 3, 1, 100); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1);

        // Check stats on detailed Task page
        openCourseDetailedPageFromCourseContentTable(fullTaskTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkMaterialIsShownInAdditionalMaterialBlock(fullPresentationTitle, 1, 1, 100); // Presentation stats

        // Check stats on detailed Presentation page
        openAdditionalMaterialFromComponentDetailedPage(fullPresentationTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers(); // LL-270 !!!
        checkGeneralStatsFromUsersPage(3, 1, 1); // 0, 0 ,0 - !!! correct values (counted without parent,
                                                                          // without Additional material)
        checkCourseProgressForUserOnUsersPage(admin, 20); // 0 - correct value !!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(13, 0, 1); // additional material isn't shown in course tree
        // Task is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1);

        // Check that only statements related to the Presentation are shown on Statements page (without parent)
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodePresentation); // LL-265 !!!!
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByInfoPage() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Info page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullInfoPageTitle = "Info page: " + infoPageTitle; // get full Info page title
        selectPageOnAddFiltersPopup(fullInfoPageTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(10, 100, 0);
        // Check the Info page is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullInfoPageTitle, 2, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats for Info page on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(2, 1, 1);
        checkPageIsShownInAllPagesCourseContentList(fullInfoPageTitle, chapterInfoPage, 2, 1, 100); // its stats // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check stats on detailed Info page
        openCourseDetailedPageFromCourseContentTable(fullInfoPageTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(2, 1, 1);
        checkCourseProgressForUserOnUsersPage(admin, 10); // 13 - correct value? // LL-270 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(13, 0, 1);
        checkPageIsDisplayedOnUserDetailedPage(fullInfoPageTitle, chapterInfoPage); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check that only statements related to the Info page are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeInfoPage);
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByMediaPage() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Media page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullMediaPageTitle = "Media page: " + titleMediaPage; // get full Media page title
        selectPageOnAddFiltersPopup(fullMediaPageTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(10, 100, 0);
        // Check the Media page is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullMediaPageTitle, 2, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats for Media page on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(2, 1, 1);
        checkPageIsShownInAllPagesCourseContentList(fullMediaPageTitle, chapterMediaPage, 2, 1, 100); // its stats // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check stats on detailed Media page
        openCourseDetailedPageFromCourseContentTable(fullMediaPageTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        checkNoContentOnDetailedPage(); // no inner content is shown on the page

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(2, 1, 1);
        checkCourseProgressForUserOnUsersPage(admin, 10); // 13 - correct value? // LL-270 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(13, 0, 1);
        checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // only one page is displayed

        // Check that only statements related to the Media page are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeMediaPage);
    }

    @Test // LL-244, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByImagePageDuplicate() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Image page (the Image is Course material of Media page, selected with autocomplete)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullImageTitle = "Image: " + imageTitle2; // get full Image title
        selectPageOnAddFiltersPopup(fullImageTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(20, 100, 0); // 10, 100, 0 - correct values !!!
        // Check the Image is displayed on Dashboard page //PILOT-1621, LL-39 !!!
        checkPageIsShownInMostViewedList(fullImageTitle, 3, 1); // 2, 1 - correct stats!!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(5, 1, 2); // 4, 1, 2 - correct stats !!!
        // Check the Image is displayed with Media page (parent content)
        String fullMediaPageTitle = "Media page: " + titleMediaPage; // get full Media page title (parent)
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullMediaPageTitle, fullImageTitle);
        // Media page stats
        checkPageIsShownInAllPagesCourseContentList(fullMediaPageTitle, chapterMediaPage, 2, 1, 100); // PILOT-1621 !!!
        // Image stats
        checkStatsForPageOnCourseContent(imagePage, 3, 1, 100); // 2, 1, 100 - correct stats!!!
        // Image duplicate stats
        WebElement imageDuplicate = checkDuplicateIsShownInAllPagesCourseContentList(fullImageTitle, 3, 1, 100); // 2, 1, 100 - correct values!!!
        checkPageIsGreyedOut(imageDuplicate); // duplicate is greyed out

        // Check stats on detailed Image page
        String courseContentPageUrl = webDriver.getCurrentUrl();
        openCourseDetailedPageFromCourseContentTable(fullImageTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100); // 2, 1, 100 - correct value!!!
        // Check stats on detailed Media page
        webDriver.get(courseContentPageUrl);
        waitForPageToLoad();
        openCourseDetailedPageFromCourseContentTable(fullMediaPageTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);
        checkMaterialIsShownInCourseMaterialBlock(fullImageTitle, 3, 1, 100); // Image stats // 2, 1, 100 - correct value!!!

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(5, 1, 2); // 2, 1 ,1 - correct values (counted without parent)
        checkCourseProgressForUserOnUsersPage(admin, 20); // 13 - correct value // LL-244 !!!!!!!!!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(25, 0, 2); // (100/8)*2 - stats for Image + Media page
        // Media page is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage); // PILOT-1621 !!!
        // Image is displayed as a child of the Media page
        imagePage = checkPageIsDisplayedAsChildOf(fullMediaPageTitle, fullImageTitle);
        checkStatsForPageOnUserDetailed(imagePage, 3, true); // 2 - correct value!!! // Image stats
        // Image duplicate is displayed with stats
        imageDuplicate = getDuplicate(fullImageTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(imageDuplicate, 3, true); // 2 - correct value!!! // duplicate stats
        checkPageIsGreyedOut(imageDuplicate); // duplicate is greyed out
        // Check that three pages are displayed in the course tree
        checkNumberOfPagesWhichShownInCourseTree(3);

        // Check that only statements related to the Image are shown on Statements page (without parent)
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeImageDuplicate); // LL-265 !!!!
    }

    @Test // LL-244, LL-39, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByVideoPageCreatedSeparately() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Video page (the Video has been created separately from Course component)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullVideoTitle = "Video: " + videoTitle; // get full Video title
        selectPageOnAddFiltersPopup(fullVideoTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(0, 100, 0); // ???

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(0, 1, 0);
        // Check the Video is shown with stats
        WebElement page = checkPageIsShownInAllPagesCourseContentList(fullVideoTitle, chapterCourse, 2, 1, 100); // PILOT-1621, LL-39 !!!
        checkPageIsGreyedOut(page); // the video page is greyed out

        // Check stats on detailed Video page
        openCourseDetailedPageFromCourseContentTable(fullVideoTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(0, 1, 0);
        checkCourseProgressForUserOnUsersPage(admin, 0); // ???

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(0, 0, 0);
        // Video page is displayed in proper chapter
        page = checkPageIsDisplayedOnUserDetailedPage(fullVideoTitle, chapterCourse); // PILOT-1621 !!!
        checkStatsForPageOnUserDetailed(page, 2, true); // Video stats
        checkPageIsGreyedOut(page); // the Video is greyed out
        // Check that one page is displayed in the course tree
        checkNumberOfPagesWhichShownInCourseTree(1);

        // Check that only statements related to the Video are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeVideoSeparated); // LL-265 !!!!
    }

    @Test // LL-244, LL-39, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByPresentationPageCreatedSeparately() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Presentation page (the Presentation has been created separately from Course component)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullPresentationTitle = "Presentation: " + presentationTitle; // get full Presentation title
        selectPageOnAddFiltersPopup(fullPresentationTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(0, 100, 0); // ???

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(0, 1, 0);
        // Check the Presentation is shown with stats
        WebElement page = checkPageIsShownInAllPagesCourseContentList(fullPresentationTitle, chapterCourse, 2, 1, 100); // PILOT-1621, LL-39 !!!
        checkPageIsGreyedOut(page); // the presentation page is greyed out

        // Check stats on detailed Presentation page
        openCourseDetailedPageFromCourseContentTable(fullPresentationTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(2, 1, 100);

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(0, 1, 0);
        checkCourseProgressForUserOnUsersPage(admin, 0); // ???

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(0, 0, 0);
        // Video page is displayed in proper chapter
        page = checkPageIsDisplayedOnUserDetailedPage(fullPresentationTitle, chapterCourse); // PILOT-1621 !!!
        checkStatsForPageOnUserDetailed(page, 2, true); // Presentation stats
        checkPageIsGreyedOut(page); // the Presentation is greyed out
        // Check that one page is displayed in the course tree
        checkNumberOfPagesWhichShownInCourseTree(1);

        // Check that only statements related to the Presentation are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodePresentationSeparated); // LL-265 !!!!
    }

    @Ignore // Quiz is greyed out ATM (the test-case is for non-greyed out Quiz) LL-275 !!!
    @Test // LL-244, LL-39, PILOT-1621 !!!!!!!!!!!!!!
    public void testApplyFilteringByQuiz() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Quiz page (the Quiz has been taken by admin)
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullQuizTitle = "Quiz: " + quizTitle; // get full Quiz title
        selectPageOnAddFiltersPopup(fullQuizTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(9, 100, 100); // ??? 9 = 100/11
        // Check the Quiz is displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullQuizTitle, 1, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(1, 1, 1);
        // Check the Quiz is shown with stats
        checkPageIsShownInAllPagesCourseContentList(fullQuizTitle, chapterCourse, 1, 1, 100); // PILOT-1621 !!!

        // Check stats on detailed Quiz page
        openCourseDetailedPageFromCourseContentTable(fullQuizTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100);

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(1, 1, 1);
        checkCourseProgressForUserOnUsersPage(admin, 11); // 100/9

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(11, 100, 1);
        // Quiz page is displayed in proper chapter
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullQuizTitle, chapterCourse); // PILOT-1621 !!!
        checkStatsForPageOnUserDetailed(page, 1, true); // Quiz stats
        // Check that one page is displayed in the course tree
        checkNumberOfPagesWhichShownInCourseTree(1);
        checkQuizResults(quizTitle, 100, 1); // Quiz results

        // Check that only statements related to the Quiz are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForNode(nodeQuiz); // LL-265 !!!!

        // Check stats on Quizzes page
        mainDashboardLLPage.clickLinkQuizzes();
        checkGeneralStatsFromQuizzesPage(1, 1, 100);
        WebElement linkScorePerUser = checkQuizIsShownWithStats(quizTitle, 1, 1, 100);
        checkListOfTopQuizResultsIsShown(best);
        checkTopQuizResultForUser(admin, best, 100, 1, 1);
        // Open 'Score per user' popup
        clickItem(linkScorePerUser, "The link 'Score per user' on Quizzes page could not be clicked.");
        checkScorePerUser(admin, 1, 100);
    }

    @Test // LL-270, PILOT-1621 !!!!
    public void testFilteringByTwoPages() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Info page and vimeo Video
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullInfoPageTitle = "Info page: " + infoPageTitle; // get full Info page title
        selectPageOnAddFiltersPopup(fullInfoPageTitle); // !!! PILOT-1621
        String fullVideoTitle = "Video: " + videoTitle2; // get full Video title
        selectPageOnAddFiltersPopup(fullVideoTitle);
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(20, 100, 0); // (100/10)*2
        // Check the Info page and Video are displayed on Dashboard page
        checkPageIsShownInMostViewedList(fullInfoPageTitle, 2, 1); // stats for Info page  // PILOT-1621 !!!
        checkPageIsShownInMostViewedList(fullVideoTitle, 1, 1); // stats for Video

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(6, 1, 3);
        checkPageIsShownInAllPagesCourseContentList(fullInfoPageTitle, chapterInfoPage, 2, 1, 100); // its stats // PILOT-1621 !!!
        String fullTaskTitle = "Task: " + taskTitle; // get full Task title
        checkPageIsShownInAllPagesCourseContentList(fullTaskTitle, chapterTask, 3, 1, 100);
        checkPageIsDisplayedAsChildOf(fullTaskTitle, fullVideoTitle);
        checkNumberOfPagesWhichShownInCourseTree(3); // Info page, Video and its parent Task are shown

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(3, 1, 2); // LL-270 !!!
        checkCourseProgressForUserOnUsersPage(admin, 25); // (100/8)*2 // LL-270 !!!

        // Check stats on admin detailed page
        clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(38, 0, 3);
        checkPageIsDisplayedOnUserDetailedPage(fullInfoPageTitle, chapterInfoPage); // PILOT-1621 !!!
        checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask); // PILOT-1621 !!!
        // Video is displayed as a child of the Task
        WebElement videoPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullVideoTitle);
        checkStatsForPageOnUserDetailed(videoPage, 1, true); // Video stats
        checkNumberOfPagesWhichShownInCourseTree(3); // Info page, Video and its parent Task are shown
    }

    @Test // !!! PILOT-1621
    public void testClearFilteringByPages() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Info page and vimeo Video
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullInfoPageTitle = "Info page: " + infoPageTitle; // get full Info page title
        selectPageOnAddFiltersPopup(fullInfoPageTitle); // !!! PILOT-1621
        String fullVideoTitle = "Video: " + videoTitle2; // get full Video title
        selectPageOnAddFiltersPopup(fullVideoTitle);
        applyFilter(filterPage);

        // Check General statistics on Dashboard page - for two pages
        checkGeneralStatsFromDashboardPage(20, 100, 0); // (100/10)*2

        // Remove one item (Info page) with cross sign from 'Add filters' page
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        removeItemFromList(fullInfoPageTitle);
        applyFilter(filterPage);

        // Check General statistics on Dashboard page - for one page
        checkGeneralStatsFromDashboardPage(10, 100, 0); // (100/10)*1   // !!!

        // Click the link "Clear all selected pages" to remove filtering
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        addFiltersLLPage.clickLinkClearAllSelectedPages();
        // Cancel clearing the list of pages
        addFiltersLLPage.clickLinkCancel();

        // Check General statistics on Dashboard page - for one page
        checkGeneralStatsFromDashboardPage(10, 100, 0); // (100/10)*1   // !!!

        // Clear all selected pages
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        addFiltersLLPage.clickLinkClearAllSelectedPages();
        addFiltersLLPage.clickButtonApplyFilters();

        // Check that the button 'Add filtering' is displayed
        Assert.assertTrue("The button 'Add filtering' is not displayed.",
                mainDashboardLLPage.getButtonAddFiltering().isDisplayed());

        // Check General statistics on Dashboard page - for all pages (no filter is applied)
        checkGeneralStatsFromDashboardPage(100, 100, 100);
    }

    @Test // !!! PILOT-1621
    public void testResetFilteringByPage() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by Media page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullMediaPageTitle = "Media page: " + titleMediaPage; // get full Media page title
        selectPageOnAddFiltersPopup(fullMediaPageTitle); // !!! PILOT-1621
        applyFilter(filterPage);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(10, 100, 0);

        // Reset the filter
        resetCurrentFilter();

        // Check General statistics on Dashboard page after reset filtering
        checkGeneralStatsFromDashboardPage(100, 100, 100);
    }














}
