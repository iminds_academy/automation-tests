package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.EditTheoryPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.learningLocker.AddFiltersLLPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.pages.learningLocker.UsersLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerFilteringByUserTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/



     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerFilteringByUserClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Add student to Example class
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.addAuthenticatedUser(student); // user is added to Example class

        // Open Example class overview page
        String urlExampleClass = websiteUrl.concat("/overview/nojs/1"); // Example class is the 1st node (by default)
        webDriver.get(urlExampleClass);
        // Create Theory, add Image to Course materials
        chapterTheory = courseBaseTest.createTheory();
        nodeTheory = courseBaseTest.getNodeFromViewComponentPage();
        String urlTheory = webDriver.getCurrentUrl();
        courseBaseTest.openEditPage();
        EditTheoryPage editTheoryPage = new EditTheoryPage(webDriver, pageLocation);
        editTheoryPage.switchToTabCourseMaterial();
        nodeImage = learningLockerBaseTest.addImageToTheoryCourseMaterials(imageTitle);
        editTheoryPage.clickButtonSave();
        webDriver.get(urlExampleClass);

        // View Theory and Image as student
        courseBaseTest.masqueradeAsUser(student);
        webDriver.get(urlTheory);
        waitForPageToLoad();
        courseBaseTest.clickMaterialByName(imageTitle); // view image
        courseBaseTest.closePopup();
        webDriver.get(urlExampleClass);
        courseBaseTest.switchBackFromMasqueradingAs(student, admin);

        // Log out, view Theory as Anonymous user
        courseBaseTest.logout(admin);
        webDriver.get(urlTheory);
        waitForPageToLoad();

        // Run cron
        courseBaseTest.login(admin);
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(exampleClass);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test // PILOT-1621, LL-287 !!!
    public void testApplyFilteringByAdmin() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by admin (tab 'By user')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        selectUserOnAddFiltersPopup(admin);
        applyFilter(filterUser);


        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(50, 100, 0);
        // Check the Theory is displayed on Dashboard page
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title
        checkPageIsShownInMostViewedList(fullTheoryTitle, 3, 1); // its stats  // PILOT-1621 !!!
        checkOnlyOnePageIsShownOnDashboard();
        checkOnlyOneUserIsShownInMostActiveList();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(3, 1, 1);
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 3, 1, 100); // its stats // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // LL-287 ??? // only one page is displayed

        // Check stats on detailed Theory page
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(3, 1, 100);
        checkOnlyOneUserIsShownInMostActiveList();

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(3, 1, 1);
        checkCourseProgressForUserOnUsersPage(admin, 50);
        checkOnlyOneUserIsShownInMostActiveList();


        // Click 'View all users' link from Users page
        UsersLLPage usersLLPage = new UsersLLPage(webDriver, pageLocation);
        usersLLPage.clickLinkViewAllUsers();
        // Check only admin is displayed in 'All users' table
        checkOnlyOneUserIsShownInAllUsersTable();
        WebElement userLink = checkUserIsDisplayedInAllUsersTable(admin);
        openCourseTreeFromAllUsersPage(userLink);

        // Check stats on admin detailed page
        checkGeneralStatsFromUserDetailedPage(50, 0, 1);
        // Theory is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // PILOT-1621 !!!
        checkNumberOfPagesWhichShownInCourseTree(1); // LL-287 ??? // Image is not shown in course tree

        // Check that only statements related to admin are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForUser(admin);
    }

    @Test // PILOT-1621, LL-286 !!!
    public void testApplyFilteringByStudent() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by student (tab 'By user')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        selectUserOnAddFiltersPopup(student);
        applyFilter(filterUser);


        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 0);
        // Check that Theory and Image are displayed on Dashboard page
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title
        checkPageIsShownInMostViewedList(fullTheoryTitle, 1, 1); // its stats  // PILOT-1621 !!!
        String fullImageTitle = "Image: " + imageTitle; // get full Image title
        checkPageIsShownInMostViewedList(fullImageTitle, 1, 1);
        checkOnlyOneUserIsShownInMostActiveList();

        // Check stats on Course content page
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(2, 1, 2);
        WebElement imagePage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullImageTitle); // PILOT-1621 !!!
        // Theory stats
        checkPageIsShownInAllPagesCourseContentList(fullTheoryTitle, chapterTheory, 1, 1, 100); // PILOT-1621 !!!
        // Image stats
        checkStatsForPageOnCourseContent(imagePage, 1, 1, 100);

        // Check stats on detailed Theory page
        openCourseDetailedPageFromCourseContentTable(fullTheoryTitle); // PILOT-1621 !!!
        checkGeneralStatsFromCourseDetailedPage(1, 1, 100); // Theory
        checkMaterialIsShownInCourseMaterialBlock(fullImageTitle, 1, 1, 100); // LL-286 !!! // Image
        checkOnlyOneUserIsShownInMostActiveList();

        // Check stats on Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(2, 1, 2);
        checkCourseProgressForUserOnUsersPage(student, 100);
        checkOnlyOneUserIsShownInMostActiveList();


        // Click 'View all users' link from Users page
        UsersLLPage usersLLPage = new UsersLLPage(webDriver, pageLocation);
        usersLLPage.clickLinkViewAllUsers();
        // Check only student is displayed in 'All users' table
        checkOnlyOneUserIsShownInAllUsersTable();
        WebElement userLink = checkUserIsDisplayedInAllUsersTable(student);
        openCourseTreeFromAllUsersPage(userLink);

        // Check stats on student detailed page
        checkGeneralStatsFromUserDetailedPage(100, 0, 2);
        // Theory is displayed in proper chapter
        checkPageIsDisplayedOnUserDetailedPage(fullTheoryTitle, chapterTheory); // PILOT-1621 !!!
        imagePage = checkPageIsDisplayedAsChildOf(fullTheoryTitle, fullImageTitle); // PILOT-1621 !!!
        checkStatsForPageOnUserDetailed(imagePage, 1, true); // Image stats

        // Check that only statements related to student are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForUser(student);
    }

    @Test // LL-59, LL-284 !!!
    public void testShowOnlyAnonymousData() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering: 'Show only anonymous data' (tab 'By user')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        addFiltersLLPage.selectRadiobuttonShowAnonymousData(); // select 'Show only anonymous data'
        applyFilter(filterUser); // LL-284 !!!

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(0, 0, 0);

        // Check that only statements related to Anonymous user are shown on Statements page
        mainDashboardLLPage.clickLinkStatements();
        checkStatementsAreShownForUser(anonymous); // LL-59 !!!
        checkStatementsAreShownForNode(nodeTheory);
    }

    @Test // LL-59, LL-284 !!!
    public void testDoNotShowAnonymous() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering: 'Show everything' (tab 'By user')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        applyFilter(filterUser); // LL-284 !!!

        // Check that statement with actor 'Anonymous' exists
        mainDashboardLLPage.clickLinkStatements();
        checkAtLeastOneStatementIsShownForUser(anonymous); // LL-59 !!!

        // Add filtering: 'Don't show anonymous' (tab 'By user')
        addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        addFiltersLLPage.selectRadiobuttonDoNotShowAnonymous(); // select 'Don't show anonymous'
        applyFilter(filterUser); // LL-284 !!!

        // Check that no statement with actor 'Anonymous' is shown
        mainDashboardLLPage.clickLinkStatements();
        checkNoStatementIsShownForUser(anonymous); // LL-59 !!!
    }

    @Test
    public void testCancelFilteringByUser() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by admin and student (tab 'By user')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        selectUserOnAddFiltersPopup(admin);
        selectUserOnAddFiltersPopup(student);
        applyFilter(filterUser);

        // Check General statistics on Dashboard page (for both users)
        checkGeneralStatsFromDashboardPage(75, 100, 0);

        // Remove student from 'selected users' list
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        removeItemFromList(student.get(0));
        applyFilter(filterUser);

        // Check General statistics on Dashboard page (for admin)
        checkGeneralStatsFromDashboardPage(50, 100, 0);

        // Cancel clearing the list of users
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        addFiltersLLPage.clickLinkClearAllSelectedUsers(); // click 'Clear all selected users'
        addFiltersLLPage.clickLinkCancel(); // cancel the action

        // Check General statistics on Dashboard page (for admin)
        checkGeneralStatsFromDashboardPage(50, 100, 0);
        checkOnlyOneUserIsShownInMostActiveList();

        // Clear all selected users
        addFiltersLLPage = mainDashboardLLPage.clickButtonChangeFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        addFiltersLLPage.clickLinkClearAllSelectedUsers();
        addFiltersLLPage.clickButtonApplyFilters();

        // Check that the button 'Add filtering' is displayed
        Assert.assertTrue("The button 'Add filtering' is not displayed.",
                mainDashboardLLPage.getButtonAddFiltering().isDisplayed());

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(75, 100, 0);
    }

    @Test
    public void testResetFilteringByUser() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Add filtering by admin (tab 'By user')
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        selectUserOnAddFiltersPopup(admin);
        applyFilter(filterUser);

        // Check General statistics on Dashboard page
        checkGeneralStatsFromDashboardPage(50, 100, 0);

        // Reset the filter
        resetCurrentFilter();

        // Check General statistics on Dashboard page after reset filtering
        checkGeneralStatsFromDashboardPage(75, 100, 0);
    }

    @Test
    public void testSaveDeleteFilterByUser() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);
        deleteAllFilters(); // delete all filters if they exist

        // Add filtering by student and save the filter
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        selectUserOnAddFiltersPopup(student);
        saveFilter(filterUser); // save the filter

        // Check that filter has been applied automatically after saving
        checkGeneralStatsFromDashboardPage(100, 100, 0); // stats for student

        // Open 'Saved filters' page with Filters link from sidebar
        mainDashboardLLPage.clickLinkFilters();
        // Check the filter is saved with correct values (fields, statements)
        List<WebElement> filterButtons = checkFilterByUserIsSaved(1, 3);
        // Delete the filter with proper icon
        clickItem(filterButtons.get(1), "'Delete' button couldn't be clicked for the filter.");
        waitForPageToLoad();

        // Check the filter has still applied
        checkGeneralStatsFromDashboardPage(100, 100, 0); // stats for student is only shown
        checkLinkFiltersIsNotShown(); // link 'Filters' is not shown on sidebar

        // Reset the filter
        resetCurrentFilter();
        checkGeneralStatsFromDashboardPage(75, 100, 0); // stats is shown without filtering
    }

    @Test // LL-265, LL-269 !!!
    public void testApplySeveralFilters() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);
        deleteAllFilters(); // delete all filters if they exist

        // Add filtering by date, save the filter
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByDate());
        selectFilteringDate(addFiltersLLPage.getFieldEndDate(), -1); // select yesterday's day
        saveFilter(filterDate); // save the filter

        // Check that filter has been applied
        checkGeneralStatsFromDashboardPage(0, 0, 0); // stats after 'by date' filtering
        // Open SavedFilters page, check that the date filter is shown
        mainDashboardLLPage.clickLinkFilters();
        checkFilterByDateIsSaved(0);
        // Reset the filter
        resetCurrentFilter();

        // Add filtering by Image page, save the filter
        addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullImageTitle = "Image: " + imageTitle; // get full Image title
        selectPageOnAddFiltersPopup(fullImageTitle);
        saveFilter(filterPage); // save the filter

        // Check that 'by page' filter is shown on SavedFilters page with correct data
        checkFilterByPageIsSaved(1, 1); // LL-265 !!!
        // Check that stats after filtering by Image is shown correctly on dashboard
        mainDashboardLLPage.clickLinkMainDashboard();
        checkGeneralStatsFromDashboardPage(50, 100, 0); // ???  LL-269 !!! (Image is a child of Theory)

        // Click 'Use' button for 'by date' filter
        mainDashboardLLPage.clickLinkFilters();
        List<WebElement> filterButtons = checkFilterByDateIsSaved(0);
        clickItem(filterButtons.get(0), "'Use' button couldn't be clicked for the filter.");
        waitForPageToLoad();

        // Check stats is shown correctly after using the filter
        checkGeneralStatsFromDashboardPage(0, 0, 0);
        resetCurrentFilter(); //reset the filter
        // No filter is applied, check stats
        checkGeneralStatsFromDashboardPage(75, 100, 0);
        // Verify that the link 'Filters' is still displayed on sidebar
        Assert.assertTrue("The link 'Filters' isn't displayed on sidebar.",
                mainDashboardLLPage.getLinkFilters().isDisplayed());
    }

    @Test // LL-296 !!!
    public void testApplyFilterBySeveralFields() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);
        deleteAllFilters(); // delete all filters if they exist

        // Open 'Add filters' popup, select 'admin'
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        AddFiltersLLPage addFiltersLLPage = mainDashboardLLPage.clickButtonAddFiltering();
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByUser());
        checkShowEverythingIsSelected(); // check that radiobutton 'Show everything' is selected
        selectUserOnAddFiltersPopup(admin);
        // Select Theory page in the proper tab
        checkFilterByTabIsSelected(addFiltersLLPage.getTabByPage());
        String fullTheoryTitle = "Theory: " + theoryTitle; // get full Theory title
        selectPageOnAddFiltersPopup(fullTheoryTitle);
        // Save the filter
        addFiltersLLPage.clickButtonSave();

        // Check stats for 'admin', 'Theory page'
        checkGeneralStatsFromDashboardPage(100, 100, 0); // LL-296 !!!
        // Check that correct filter has been applied
        String currentFilter = mainDashboardLLPage.getFieldCurrentFilter().getText();
        Assert.assertTrue("Actual filter doesn't match expected one.",
                currentFilter.equals("Filtered on users, pages"));

        // Check that correct data is shown for saved filter
        mainDashboardLLPage.clickLinkFilters();
        // Fields:
        String fieldsActual = webDriver.findElement(By.xpath("//*[@class='container-fluid']//tbody/tr/td[2]")).getText();
        Assert.assertTrue("Actual fields of the filter don't match expected fields.",
                fieldsActual.equals("users (1), pages (1)"));
        // Number of statements:
        String statements = webDriver.findElement(By.xpath("//*[@class='container-fluid']//tbody/tr/td[3]")).getText();
        int statementsActual = Integer.parseInt(statements);
        Assert.assertTrue("Actual number of statements isn't equal to expected one.",
                statementsActual == 3); // admin viewed Theory three times
        resetCurrentFilter(); // reset the filter

        // Filtering is removed, initial stats is displayed
        mainDashboardLLPage.clickLinkMainDashboard();
        checkGeneralStatsFromDashboardPage(75, 100, 0);
    }






















}
