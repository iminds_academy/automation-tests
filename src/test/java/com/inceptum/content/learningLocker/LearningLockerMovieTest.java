package com.inceptum.content.learningLocker;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.CreateVideoPage;
import com.inceptum.pages.EditLiveSessionPage;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.SelectVideoPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.pages.learningLocker.MoviesLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerMovieTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected static String titleLiveSession = "";
    protected static String titleMediaPage = "";
    protected static String youtubeURL = "https://www.youtube.com/watch?v=ji3bFv5QJGg"; // 15 sec


     /* ----- Tests ----- */

    @BeforeClass // LL-205, 206, 219
    public static void beforeLearningLockerMovieClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        String urlClassOverviewPage = courseBaseTest.addClass(className);
        // Add youtube video to the Class
        courseBaseTest.createVideoWithUrl(youtubeURL, videoTitle);
        // Watch youtube video till the end (by admin)
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        courseBaseTest.waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        Thread.sleep(2000); // timeout for correct generating of video statements

        // Add a Media page to the Class, select youtube video in Educational content, watch till the end
        webDriver.get(urlClassOverviewPage);
        titleMediaPage = courseBaseTest.addMediaPageToClass(); // add Media page
        courseBaseTest.openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        editMediaPage.switchToTabEducationalContent();
        SelectVideoPage selectVideoPage = editMediaPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(youtubeURL);
        Assert.assertTrue(editMediaPage.getLinkRemoveMedia().isDisplayed());
        editMediaPage.clickButtonSave();
        // Wait until video is completed
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        courseBaseTest.clickYoutubeLargePlayButton(); // click large 'play' button
        courseBaseTest.waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        Thread.sleep(2000); // timeout for correct generating of video statements

        // Add a Live session with vimeo video to the Class
        webDriver.get(urlClassOverviewPage);
        titleLiveSession = courseBaseTest.addLiveSessionToClass(); // add LS
        courseBaseTest.openEditPage();
        EditLiveSessionPage editLiveSessionPage = new EditLiveSessionPage(webDriver, pageLocation);
        editLiveSessionPage.switchToTabCourseMaterial();
        CreateVideoPage createVideoPage = editLiveSessionPage.clickIconAddVideo(); // add vimeo video
        createVideoPage.fillFieldTitle(videoTitle2);
        selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(vimeoURL);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSaveTop();
        editLiveSessionPage.clickButtonSave();

        // Watch vimeo video till the end (by admin)
        courseBaseTest.clickMaterialByName(videoTitle2);
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        courseBaseTest.waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();
        Thread.sleep(2000); // timeout for correct generating of video statements

        // Add a new user to the Class
        learningLockerBaseTest.addAuthenticatedUser(student);
        learningLockerBaseTest.addUserToClass(student, urlClassOverviewPage);
        // Watch vimeo video partially (by user)
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.masqueradeAsUser(student); // masquerade as the user
        learningLockerBaseTest.openViewNodePageFromClassOverview(titleLiveSession); // open View Live session page
        courseBaseTest.clickMaterialByName(videoTitle2); // click video link
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        courseBaseTest.waitUntilVimeoVideoPlaysSec(4); // wait 4 sec
        courseBaseTest.clickButtonPauseVimeo(); // pause
        webDriver.switchTo().defaultContent();

        // Switch back from masquerading
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.switchBackFromMasqueradingAs(student, admin);

        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test // LL-196 ('Content names are shown arbitrarily in LL')!!!
    public void testStatsForYoutubeVideoFullyWatched() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Open Movies page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkMovies();
        // Check the Movie stats
        List<Integer> fullyWatchedStats = Arrays.asList(1, 50);
        List<Integer> partialWatchedStats = Arrays.asList(1, 50);
        String fullVideoTitle = "Video: " + videoTitle; // LL-196 !!!
        WebElement movie = checkMovieIsShownWithStats(fullVideoTitle, fullyWatchedStats, partialWatchedStats);
        // Open the Movie Detailed page
        movie.click();
        waitForPageToLoad();
        checkGeneralStatsFromMovieDetailedPage(1, 1, 50); // check general stats for the video
        checkUserIsShownOnMovieDetailedPageInTopList(admin); // user is shown in 'Top 5 users' list
        // Check that admin is shown in 'Users who watched this movie' table
        checkUserIsShownInWatchedMovieTable(admin, true); // check the video is completed (>= 90% watched)
    }

    @Test
    public void testStatsForVideoFromMediaPageEducationalContent() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Open Movies page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkMovies();
        // Check the Movie stats
        List<Integer> fullyWatchedStats = Arrays.asList(1, 50);
        List<Integer> partialWatchedStats = Arrays.asList(1, 50);
        String fullParentOfInnerVideoTitle = "Media page: " + titleMediaPage;
        WebElement movie = checkMovieIsShownWithStats(fullParentOfInnerVideoTitle, fullyWatchedStats, partialWatchedStats);
        // Open the Movie Detailed page
        movie.click();
        checkGeneralStatsFromMovieDetailedPage(1, 1, 50); // check general stats for the video
        checkUserIsShownOnMovieDetailedPageInTopList(admin); // user is shown in 'Top 5 users' list
        // Check that admin is shown in 'Users who watched this movie' table
        checkUserIsShownInWatchedMovieTable(admin, true); // check the video is completed (>= 90% watched)
    }

    @Test
    public void testStatsMixedWatchedVimeoVideo() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Open Movies page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        MoviesLLPage moviesLLPage = mainDashboardLLPage.clickLinkMovies();
        // Check the Movie stats
        List<Integer> fullyWatchedStats = Arrays.asList(1, 50);
        List<Integer> partialWatchedStats = Arrays.asList(2, 100);
        String fullParentOfInnerVideoTitle = "Video: " + videoTitle2;
        moviesLLPage.clickIconCogwheel(); // refresh stats by clicking on 'cogwheel' icon
        WebElement movie = checkMovieIsShownWithStats(fullParentOfInnerVideoTitle, fullyWatchedStats, partialWatchedStats);
        // Open the Movie Detailed page
        movie.click();
        checkGeneralStatsFromMovieDetailedPage(1, 2, 50); // check general stats for the video
        checkUserIsShownOnMovieDetailedPageInTopList(admin); // admin is shown in 'Top 5 users' list
        checkUserIsShownOnMovieDetailedPageInTopList(student); // user is shown in 'Top 5 users' list
        // Check that users are shown in 'Users who watched this movie' table
        checkUserIsShownInWatchedMovieTable(admin, true); // check the video is completed (>= 90% watched)
        checkUserIsShownInWatchedMovieTable(student, false); // check the video is partially watched
    }

    @Test
    public void testMoviesGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Open Movies page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        MoviesLLPage moviesLLPage = mainDashboardLLPage.clickLinkMovies();
        moviesLLPage.clickIconCogwheel(); // refresh stats by clicking on 'cogwheel' icon
        // Check general stats
        checkGeneralStatsFromMoviesPage(3, 2, 50);
        // Check that correct number of fully watched movies is displayed for each user
        checkUserIsShownInTopListWatchedMovies(admin, 3);
        checkUserIsShownInTopListWatchedMovies(student, 0);
    }




















}
