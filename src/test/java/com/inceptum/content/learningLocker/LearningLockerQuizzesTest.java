package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.ManageQuestionsQuizPage;
import com.inceptum.pages.TakeQuizPage;
import com.inceptum.pages.ViewQuizPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerQuizzesTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerQuizzesClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        courseBaseTest.deleteContentTypeFromTable("Multiple choice question");
        String urlClassOverviewPage = courseBaseTest.addClass(className);
        // Add a new user to the Class
        learningLockerBaseTest.addAuthenticatedUser(student);
        learningLockerBaseTest.addUserToClass(student, urlClassOverviewPage);

        // Create Quiz1
        courseBaseTest.createQuiz();
        // Add two MCQs to the Quiz
        courseBaseTest.createMCQuestionFromManageQuestionTab(question1, alternativesName, 1);
        courseBaseTest.createMCQuestionFromManageQuestionTab(question2, alternativesAge, 2);

        // Take the Quiz (by admin)
        ManageQuestionsQuizPage manageQuestionsQuizPage = new ManageQuestionsQuizPage(webDriver, pageLocation);
        ViewQuizPage viewQuizPage = manageQuestionsQuizPage.clickTabView();
        String urlViewQuiz1Page = webDriver.getCurrentUrl();
        TakeQuizPage takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        // Check that correct question is shown, select the 2nd answer (wrong)
        takeQuizPage.checkQuestionAndItsOrderNumber(question1, 1);
        takeQuizPage.selectVariant(2);
        // Switch to the next question, select the 3d answer (wrong)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question2, 2);
        takeQuizPage.selectVariant(3);
        // Finish the Quiz
        courseBaseTest.finishQuiz();

        // Take the Quiz by admin one more time (another results)
        viewQuizPage = manageQuestionsQuizPage.clickTabView();
        takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        // Check that correct question is shown, select the 2nd answer (wrong)
        takeQuizPage.checkQuestionAndItsOrderNumber(question1, 1);
        takeQuizPage.selectVariant(2);
        // Switch to the next question, select the 2nd answer (correct)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question2, 2);
        takeQuizPage.selectVariant(2);
        // Finish the Quiz
        courseBaseTest.finishQuiz();

        // Masquerade as the user, take the Quiz1
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.masqueradeAsUser(student);
        webDriver.get(urlViewQuiz1Page);
        takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        // Check that correct question is shown, select the 1st answer (correct)
        takeQuizPage.checkQuestionAndItsOrderNumber(question1, 1);
        takeQuizPage.selectVariant(1);
        // Switch to the next question, select the 2nd answer (correct)
        takeQuizPage.clickButtonNext();
        takeQuizPage.checkQuestionAndItsOrderNumber(question2, 2);
        takeQuizPage.selectVariant(2);
        // Finish the Quiz
        courseBaseTest.finishQuiz();

        // Switch back from masquerading
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.switchBackFromMasqueradingAs(student, admin);

        // Create Quiz2 (from MCQ)
        courseBaseTest.createMCQuestionWithQuiz(question3, alternativesCountry, 3, quizTitle2);
        // Take the Quiz2 (as admin)
        courseBaseTest.openContentPage();
        courseBaseTest.openViewMaterialPageFromContentTable(quizTitle2);
        takeQuizPage = viewQuizPage.clickButtonStartQuiz();
        // Check that correct question is shown, select the 3d answer (correct)
        takeQuizPage.checkQuestionAndItsOrderNumber(question3, 1);
        takeQuizPage.selectVariant(3);
        // Finish the Quiz
        courseBaseTest.finishQuiz();

        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test
    public void testQuizCreatedWithAddContentLink() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Open Quizzes page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkQuizzes();
        // Check stats for the Quiz
        WebElement linkScorePerUser = checkQuizIsShownWithStats(quizTitle, 3, 2, 50);
        // Open 'Score per user' popup
        clickItem(linkScorePerUser, "The link 'Score per user' on Quizzes page could not be clicked.");
        checkScorePerUser(admin, 2, 25); // admin stats for the Quiz
        checkScorePerUser(student, 1, 100); // student's stats for the Quiz
    }

    @Test
    public void testQuizCreatedFromMCQ() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Open Quizzes page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkQuizzes();
        // Check stats for the Quiz
        WebElement linkScorePerUser = checkQuizIsShownWithStats(quizTitle2, 1, 1, 100); // ???
        // Open 'Score per user' popup
        clickItem(linkScorePerUser, "The link 'Score per user' on Quizzes page could not be clicked.");
        checkScorePerUser(admin, 1, 100); // admin stats for the Quiz
    }

    @Test // change the stats after LL-142 has been resolved !!!
    public void testQuizzesGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check General statistics from Dashboard page
        checkGeneralStatsFromDashboardPage(100, 100, 50); // ????
        // Open Quizzes page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkQuizzes();
        // Check General statistics from Quizzes page
        checkGeneralStatsFromQuizzesPage(4, 2, 75); // ????
        // Check student's best result
        checkListOfTopQuizResultsIsShown(best);
        checkTopQuizResultForUser(student, best, 50, 1, 2); // ????
        // Check admin's worst result
        checkListOfTopQuizResultsIsShown(worst);
        WebElement linkUser = checkTopQuizResultForUser(admin, worst, 50, 2, 2); // ????

        // Open admin course tree, check the quizzes stats
        linkUser.click();
        checkQuizResults(quizTitle, 25, 2); // ????  stats for Quiz1
        checkQuizResults(quizTitle2, 100, 1); // ????   stats for Quiz2
        // Open student's course tree, check the quizzes stats
        mainDashboardLLPage.clickLinkUsers();
        clickUserLinkFromMostActive(student);
        checkQuizResults(quizTitle, 100, 1); // ????    stats for Quiz1
    }


















}
