package com.inceptum.content.learningLocker;

import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebElement;

import com.inceptum.content.CourseBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.EditInfoPage;
import com.inceptum.pages.EditMediaPage;
import com.inceptum.pages.EditTaskPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.learningLocker.MainDashboardLLPage;
import com.inceptum.pages.learningLocker.UserDetailedLLPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 12.10.2015.
 */
public class LearningLockerStatsForTwoUsersTest extends LearningLockerBaseTest {


    /*---------CONSTANTS--------*/

    protected static String titleInfoPage = "";
    protected static String titleMediaPage = "";
    protected static String titlePresentation = "";
    protected static String titleTask = "";


     /* ----- Tests ----- */

    @BeforeClass
    public static void beforeLearningLockerStatsForTwoUsersClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin to academy site
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
        // Delete all classes with such name
        CourseBaseTest courseBaseTest = new CourseBaseTest();
        courseBaseTest.deleteClass(className);

        // Check if LRS has already been created (Learning Locker site)
        LearningLockerBaseTest learningLockerBaseTest = new LearningLockerBaseTest();
        urlLRS = learningLockerBaseTest.checkLrsIsCreated(titleLRS);
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        // Add correct LL client data to TinCanApi
        mainDashboardLLPage.clickLinkManageClients();
        List<String> apiInfo = learningLockerBaseTest.getClientApiInfo();
        learningLockerBaseTest.checkTinCanApiPageLL(apiInfo);
        // Clean LRS
        learningLockerBaseTest.cleanLRS(urlLRS);

        // Create a new Class
        String urlClassOverviewPage = courseBaseTest.addClass(className);
        // Add Info page to the Class
        titleInfoPage = courseBaseTest.addInfoPageToClass();
        courseBaseTest.openEditPage();
        EditInfoPage editInfoPage = new EditInfoPage(webDriver, pageLocation);
        chapterInfoPage = courseBaseTest.getNameOfSelectedOption(editInfoPage.getFieldChapter()); // get chapter name
        // Add Media page to the Class
        webDriver.get(urlClassOverviewPage);
        titleMediaPage = courseBaseTest.addMediaPageToClass();
        courseBaseTest.openEditPage();
        EditMediaPage editMediaPage = new EditMediaPage(webDriver, pageLocation);
        chapterMediaPage = courseBaseTest.getNameOfSelectedOption(editMediaPage.getFieldChapter()); // get chapter name
        // Add Presentation to the Class
        webDriver.get(urlClassOverviewPage);
        titlePresentation = courseBaseTest.addPresentationToClass();
        // Add Task to the Class
        webDriver.get(urlClassOverviewPage);
        titleTask = courseBaseTest.addTaskToClass();
        // Add the Presentation to the Task
        courseBaseTest.openEditPage();
        EditTaskPage editTaskPage = new EditTaskPage(webDriver, pageLocation);
        chapterTask = courseBaseTest.getNameOfSelectedOption(editTaskPage.getFieldChapter()); // get chapter name
        editTaskPage.switchToTabTaskResources();
        courseBaseTest.autoSelectItemFromList(editTaskPage.getFieldPresentationsTaskResorses(), titlePresentation);
        editTaskPage.clickButtonSave();

        // Add a new user to the Class
        learningLockerBaseTest.addAuthenticatedUser(student);
        learningLockerBaseTest.addUserToClass(student, urlClassOverviewPage);
        // Masquerade as the user
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.masqueradeAsUser(student);
        learningLockerBaseTest.openViewNodePageFromClassOverview(titleInfoPage); // view Info page
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.openViewNodePageFromClassOverview(titleTask); // view Task
        courseBaseTest.clickMaterialByName(titlePresentation); // view Presentation from View Task page
        // Switch masquerading back
        webDriver.get(urlClassOverviewPage);
        learningLockerBaseTest.switchBackFromMasqueradingAs(student, admin);

        // Run cron
        courseBaseTest.runCron();
        // Clear cache
        webDriver.get(urlLRS);
        learningLockerBaseTest.selectClassInLRS(className);
        learningLockerBaseTest.clearCache(urlLRS);

        quitWebDriver();
    }


    @Test
    public void testViewInfoPageByTwoUsers() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Info page is displayed on Dashboard page
        String fullInfoPageTitle = "Info page: " + titleInfoPage; // get full Info page title
        checkPageIsShownInMostViewedList(fullInfoPageTitle, 2, 2); // Info page stats
        // Check the Info page is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullInfoPageTitle, chapterInfoPage, 2, 2, 100); // Info page stats
        // Check stats of the Info page on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullInfoPageTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 2, 100);
        // Check admin is shown on the page, open the admin's course tree
        String urlDetailedInfoPage = webDriver.getCurrentUrl();
        checkUserIsShownOnDetailedPage(admin);
        clickUserLinkFromMostActive(admin);
        // Check the Info page is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullInfoPageTitle, chapterInfoPage);
        checkStatsForPageOnUserDetailed(page, 1, true);

        // Check the user is shown on Detailed Info page, open the user's course tree
        webDriver.get(urlDetailedInfoPage);
        checkUserIsShownOnDetailedPage(student);
        clickUserLinkFromMostActive(student);
        // Check the Info page is displayed in the course tree with proper stats
        page = checkPageIsDisplayedOnUserDetailedPage(fullInfoPageTitle, chapterInfoPage);
        checkStatsForPageOnUserDetailed(page, 1, true);
    }

    @Test
    public void testViewMediaPageOnlyByOneUser() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Media page is displayed on Dashboard page
        String fullMediaPageTitle = "Media page: " + titleMediaPage; // get full Media page title
        checkPageIsShownInMostViewedList(fullMediaPageTitle, 1, 1); // Media page stats
        // Check the Media page is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullMediaPageTitle, chapterMediaPage, 1, 1, 50); // Media page stats
        // Check stats of the Media page on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullMediaPageTitle);
        checkGeneralStatsFromCourseDetailedPage(1, 1, 50);
        // Check that the user is not shown in 'Most active users'
        checkUserIsNotShownOnDetailedPage(student);
        // Check admin is shown on the page, open the admin's course tree
        checkUserIsShownOnDetailedPage(admin);
        UserDetailedLLPage userDetailedLLPage = clickUserLinkFromMostActive(admin);
        // Check the Media page is displayed in the course tree with proper stats
        WebElement page = checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage);
        checkStatsForPageOnUserDetailed(page, 1, true);

        // Open user's course tree, check Media page stats
        userDetailedLLPage.clickLinkUsers();
        checkUserIsShownOnDetailedPage(student);
        clickUserLinkFromMostActive(student);
        // Check the Media page is displayed in the course tree with proper stats
        page = checkPageIsDisplayedOnUserDetailedPage(fullMediaPageTitle, chapterMediaPage);
        checkStatsForPageOnUserDetailed(page, 0, false);
    }

    @Test // LL-39 ("Two statements")
    public void testStatsForUncompletedMaterialInCourseComponent() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check the Task and Presentation are displayed on Dashboard page
        String fullTaskTitle = "Task: " + titleTask; // get full Task title
        checkPageIsShownInMostViewedList(fullTaskTitle, 3, 2); // Task stats
        String fullPresentationTitle = "Presentation: " + titlePresentation; // get full Presentation title
        checkPageIsShownInMostViewedList(fullPresentationTitle, 2, 2); // Presentation stats // LL-39 ???
        // Check the Task is displayed on 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkPageIsShownInAllPagesCourseContentList(fullTaskTitle, chapterTask, 3, 2, 100); // Task stats
        // Check stats of the Task on its detailed page
        openCourseDetailedPageFromCourseContentTable(fullTaskTitle);
        checkGeneralStatsFromCourseDetailedPage(3, 2, 100);
        checkMaterialIsShownInCourseMaterialBlock(fullPresentationTitle, 1, 1, 50); // Presentation stats
        // Check that admin and user are shown in 'Most active users'
        checkUserIsShownOnDetailedPage(admin);
        checkUserIsShownOnDetailedPage(student);
        openCourseMaterialFromComponentDetailedPage(fullPresentationTitle);
        checkGeneralStatsFromCourseDetailedPage(2, 2, 100); // ???

        // Open admin's course tree, check that the Task is displayed in proper chapter
        UserDetailedLLPage userDetailedLLPage = clickUserLinkFromMostActive(admin);
        WebElement taskPage = checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask);
        checkStatsForPageOnUserDetailed(taskPage, 2, true); // Task stats
        // Check that Presentation is shown as child of Task
        WebElement presentationPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullPresentationTitle);
        checkStatsForPageOnUserDetailed(presentationPage, 0, false); // Presentation stats // ?????
        // Check that Presentation is also shown in 'Course' chapter
        WebElement presentationCoursePage = checkPageIsDisplayedOnUserDetailedPage(fullPresentationTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(presentationCoursePage, 1, true); // Presentation stats in 'Course' // ?????

        // Open user's course tree
        userDetailedLLPage.clickLinkUsers();
        checkUserIsShownOnDetailedPage(student);
        clickUserLinkFromMostActive(student);
        // Check that the Task is displayed in proper chapter
        taskPage = checkPageIsDisplayedOnUserDetailedPage(fullTaskTitle, chapterTask);
        checkStatsForPageOnUserDetailed(taskPage, 1, true); // Task stats
        // Check that Presentation is shown as child of Task
        presentationPage = checkPageIsDisplayedAsChildOf(fullTaskTitle, fullPresentationTitle);
        checkStatsForPageOnUserDetailed(presentationPage, 1, true); // Presentation stats // ?????
        // Check that Presentation is also shown in 'Course' chapter
        presentationCoursePage = checkPageIsDisplayedOnUserDetailedPage(fullPresentationTitle, chapterCourse);
        checkStatsForPageOnUserDetailed(presentationCoursePage, 0, false); // Presentation stats in 'Course' // ?????
    }

    @Test // ???
    public void testTwoUsersGeneralStats() throws Exception {
        // Log in to Learning Locker, open the LRS Dashboard page
        loginToLL();
        webDriver.get(urlLRS);

        // Check General statistics from Dashboard page
        checkGeneralStatsFromDashboardPage(88, 100, 0); // ???
        // Check General statistics from 'Course content' page
        MainDashboardLLPage mainDashboardLLPage = new MainDashboardLLPage(webDriver, pageLocation);
        mainDashboardLLPage.clickLinkCourseContent();
        checkGeneralStatsFromCourseContentPage(8, 2, 4); // ???
        // Check General statistics from Users page
        mainDashboardLLPage.clickLinkUsers();
        checkGeneralStatsFromUsersPage(8, 2, 4);
        checkCourseProgressForUserOnUsersPage(admin, 100); // course progress for admin
        checkCourseProgressForUserOnUsersPage(student, 75); // course progress for user
        // Check General statistics from admin's detailed page
        UserDetailedLLPage userDetailedLLPage = clickUserLinkFromMostActive(admin);
        checkGeneralStatsFromUserDetailedPage(100, 0, 4);
        // Check General statistics from user's detailed page
        userDetailedLLPage.clickLinkUsers();
        clickUserLinkFromMostActive(student);
        checkGeneralStatsFromUserDetailedPage(75, 0, 3);
    }


















}
