package com.inceptum.feedbackSessionTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TestBase;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.CreateFeedbackPage;
import com.inceptum.pages.FeedbackEvaluationPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.OverviewFeedbackPage;
import com.inceptum.pages.ViewFeedbackEvaluationPage;

/**
 * Created by Olga on 09.02.2015.
 */
public class FeedbackBaseTest extends TestBase {



    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final List<String> admin = Arrays.asList("admin", "admin");
    protected final String feedbackSessionName = "FeedbackSession1";
    protected final String feedbackSessionName2 = "FeedbackSession2";
    protected final String templateName = "ORW Jury feedback form";
    protected final String commentNeed = "CommentNeed";
    protected final String commentApproach = "CommentApproach";
    protected final String commentBenefit = "CommentBenefit";
    protected final String commentCompetition = "CommentCompetition";
    protected final String commentCommunicate = "CommentCommunicate";
    protected final String commentAchieved = "CommentAchieved";
    protected final String commentComplementary = "CommentComplementary";
    protected final String commentQuality = "CommentQuality";
    protected final String commentExitEvent = "CommentExitEvent";
    protected final String commentIdea = "CommentIdea";
    protected final String commentFeasibility = "CommentFeasibility";
    protected final String commentPresentation = "CommentPresentation";
    protected final String commentQA = "CommentQA";


    /* ----- Tests ----- */

    @Before
    public void beforeTest() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
        // Login as admin and clear Recent log messages
        loginAs(admin);
        clearLogMessages();
        webDriver.get(websiteUrl);
        waitForPageToLoad();
    }

    @After
    public void afterTest() throws Exception { // admin should be logged in
        // Open Home page and check admin is logged in
        HomePage homePage = openHomePage();
        String xpathLinkUserAccount = "//nav[@id='block-system-user-menu']/ul/li/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLinkUserAccount)));
        WebElement linkUserAccount = webDriver.findElement(By.xpath(xpathLinkUserAccount));
        if (!(linkUserAccount.getText().equals(admin.get(0)))) {
            if (linkUserAccount.getText().equals("Sign in")) { // 'Sign in' button instead of user link is displayed
                login(admin);
            } else { // log out if user link doesn't match with 'admin' (another user has been logged in)
                linkUserAccount.click();
                wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".expanded.open"))); // dropdown list is open
                homePage.clickLinkSignOut();
                login(admin);
            }
        }
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> errors = new ArrayList<String>();
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available.
                break;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {  // Find php messages starting with "Notice:" or "Warning:"
                    errors.add(message.getAttribute("href"));
                }
            }
        }

        // Get all php messages
        for (String url : errors) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            System.out.println(fullMessage);
        }

        if (errors.size() > 0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }


    public void selectClassForFeedback(String className) throws Exception {
        CreateFeedbackPage createFeedbackPage = new CreateFeedbackPage(webDriver, pageLocation);
        List<WebElement> options = createFeedbackPage.getFieldClass().findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                if (!(option.isSelected())) {
                    option.click();
                    waitUntilActionWithThrobberDone();
                }
                option = webDriver.findElement(By.xpath("//option[text()='" + className + "']"));
                Assert.assertTrue("The option is not selected.", option.isSelected());
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }

    public void selectFeedbackContents(String templateName) throws Exception {
        CreateFeedbackPage createFeedbackPage = new CreateFeedbackPage(webDriver, pageLocation);
        List<WebElement> options = createFeedbackPage.getFieldFeedbackContents().findElements((By.tagName("option")));
        for (WebElement option : options) {
            if (option.getText().equals(templateName)) {
                option.click();
                Thread.sleep(1000);
                Assert.assertTrue("The option is not selected.", option.isSelected());
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public int getNodeOfLastCreatedFeedbackSession() {
        String xpathLastFeedbackSession = "//div[@class='l-content-inner']/div[2]/div[2]/a[last()]";
        int lastSession;
        if (!(webDriver.findElements(By.xpath(xpathLastFeedbackSession)).size() == 0)) {
            String lastSessionUrl = webDriver.findElement(By.xpath(xpathLastFeedbackSession)).getAttribute("href");
            String[] parts = lastSessionUrl.split("create/"); // Separate url
            String lastSessionId = parts[1]; // get node
            lastSession = Integer.parseInt(lastSessionId);
            return lastSession;
        } else {
            lastSession = 1; // if no feedback session is created, the 1st node == 2 (and value '1' is needed for the next step)
            return lastSession;
        }
    }
    public String createFeedbackSessionByAdmin(String feedbackSessionName, List<String> juryMember, String className, String groupName) throws Exception {
        // Add the user with role 'jury member' to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Create a Group and add it to the Class
        createGroup(groupName, className);
        String urlViewGroupPage = webDriver.getCurrentUrl();
        // Add jury member to the group
        addUserToGroup(juryMember, groupName);
        // Open Overview Feedback session page and find node of last created session
        HomePage homePage = openHomePage();
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        int lastSession = getNodeOfLastCreatedFeedbackSession();
        // Create new feedback session
        CreateFeedbackPage createFeedbackPage = overviewFeedbackPage.clickButtonNewFeedbackSession();
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        // Check that the new feedback session is created and displayed on Overview page
        int newSession = getNodeOfLastCreatedFeedbackSession();
        Assert.assertTrue("New session has not been created.", newSession == lastSession + 1);
        return urlViewGroupPage;
    }
    public void createFeedbackSessionForTwoJuries(List<String> jury1, List<String> jury2) throws Exception {
        HomePage homePage = openHomePage();
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        CreateFeedbackPage createFeedbackPage = overviewFeedbackPage.clickButtonNewFeedbackSession();
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), jury1.get(0));
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers2(), jury2.get(0));
        createFeedbackPage.clickButtonSave();
    }
    public void doEvaluation() throws Exception { // fill in all fields and select one star-icon for each point
        FeedbackEvaluationPage feedbackEvaluationPage = new FeedbackEvaluationPage(webDriver, pageLocation);

        // Tab NABC
        feedbackEvaluationPage.fillFieldNeed(commentNeed);
        feedbackEvaluationPage.clickIconOneStarNeed();

        feedbackEvaluationPage.fillFieldApproach(commentApproach);
        feedbackEvaluationPage.clickIconTwoStarsApproach();

        feedbackEvaluationPage.fillFieldBenefit(commentBenefit);
        feedbackEvaluationPage.clickIconThreeStarsBenefit();

        feedbackEvaluationPage.fillFieldCompetition(commentCompetition);
        feedbackEvaluationPage.clickIconFourStarsCompetition();

        feedbackEvaluationPage.clickButtonSave();
        feedbackEvaluationPage.switchToTabTeam();

        // Tab Team
        feedbackEvaluationPage.fillFieldCommunicate(commentCommunicate);
        feedbackEvaluationPage.clickIconFourStarsCommunicate();

        feedbackEvaluationPage.fillFieldAchieved(commentAchieved);
        feedbackEvaluationPage.clickIconThreeStarsAchieved();

        feedbackEvaluationPage.fillFieldComplementary(commentComplementary);
        feedbackEvaluationPage.clickIconTwoStarsComplementary();

        feedbackEvaluationPage.clickButtonSave();
        feedbackEvaluationPage.switchToTabPresentation();

        // Tab Presentation
        feedbackEvaluationPage.fillFieldQuality(commentQuality);
        feedbackEvaluationPage.clickIconOneStarQuality();

        feedbackEvaluationPage.switchToTabBusinessPotential();

        // Tab 'Business potential'
        feedbackEvaluationPage.fillFieldExitEvent(commentExitEvent);
        feedbackEvaluationPage.clickIconOneStarExitEvent();

        feedbackEvaluationPage.fillFieldIdea(commentIdea);
        feedbackEvaluationPage.clickIconFourStarsIdea();

        feedbackEvaluationPage.fillFieldFeasibility(commentFeasibility);
        feedbackEvaluationPage.clickIconFourStarsFeasibility();

        feedbackEvaluationPage.switchToTabOverallAdvice();

        // Tab 'Overall advice'
        feedbackEvaluationPage.fillFieldPresentation(commentPresentation);
        feedbackEvaluationPage.fillFieldQA(commentQA);

        // Submit changes and check that submission has been saved
        feedbackEvaluationPage.clickButtonSubmit();
        checkAlert();
        assertTextOfMessage(feedbackEvaluationPage.getMessage(), ".*Your submission has been saved.");
    }
    public void openFeedbackEvaluationPage(String feedbackSessionName, String groupName) throws Exception {
        HomePage homePage = openHomePage();
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until jury block appears
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".jury")));
        String xpathCreatedSession = "//h3[contains(text(), 'Jury duty')]/../div/div/h4[text()='" + feedbackSessionName +
                "']/../a[contains(text(), 'Evaluate " + groupName + "')]";
        webDriver.findElement(By.xpath(xpathCreatedSession)).click(); // click link 'Evaluate group'
        waitForPageToLoad();
    }
    public void checkCorrectGroupIsSelectedOnEvaluationPage(String groupName) throws Exception {
        FeedbackEvaluationPage feedbackEvaluationPage = new FeedbackEvaluationPage(webDriver, pageLocation);
        WebElement selectedGroup = feedbackEvaluationPage.getFieldGroup().findElement(By.cssSelector("[selected]")); // selected option in Group field
        Assert.assertTrue("Selected group is not equal to the proper group in 'Change feedback for' field on FeedbackEvaluation page.",
                selectedGroup.getText().contains(groupName));
    }
    public void checkCorrectJuryIsSelectedOnEvaluationPage(List<String> jury) throws Exception {
        ViewFeedbackEvaluationPage viewFeedbackEvaluationPage = new ViewFeedbackEvaluationPage(webDriver, pageLocation);
        waitForPageToLoad();
        WebElement selectedJury = viewFeedbackEvaluationPage.getFieldJury().findElement(By.cssSelector("[selected]")); // selected option in Jury field
        Assert.assertTrue("Selected jury is not equal to the proper jury member in 'Feedback by' field on ViewFeedbackEvaluation page.",
                selectedJury.getText().equals(jury.get(0)));
    }
    public void checkFeedbackSessionIsSubmitted(String feedbackSessionName, String groupName) throws Exception {
        String xpathLinkFeedback = "//h3[contains(text(), 'Jury duty')]/a[text()='(Show submitted feedback)']";
        webDriver.findElement(By.xpath(xpathLinkFeedback)).click(); // link in jury duty
        waitForPageToLoad();
        String xpathSubmittedSession = "//h3[contains(text(), 'Jury history')]/../div/div/h4[text()='" + feedbackSessionName +
                "']/../a[contains(text(), 'Evaluate " + groupName + "')]";
        WebElement linkEvaluateGroup = webDriver.findElement(By.xpath(xpathSubmittedSession)); // link in jury history
        Assert.assertTrue("The group is not submitted.", linkEvaluateGroup.findElement(By.xpath("./div")).getText().equals("Submitted"));
    }
    public void checkEvaluationInRow(WebElement row, int valueIconStarExpected, String comment) throws Exception {
        // Check star evaluation
        String valueActualString = iconStar(row).getAttribute("value");
        int valueActual = Integer.parseInt(valueActualString);
        Assert.assertTrue("Evaluation (star icon) is shown incorrectly.", valueActual == valueIconStarExpected);

        // Check comment
        String commentActual = comment(row).getText();
        Assert.assertTrue("Evaluation comment is incorrect.", commentActual.equals(comment));
    }
    public WebElement iconStar(WebElement rowEvaluation) {
        WebElement iconStar = rowEvaluation.findElement(By.cssSelector(".star-button"));
        return iconStar;
    }
    public WebElement comment(WebElement rowEvaluation) {
        WebElement comment = rowEvaluation.findElement(By.cssSelector(".field--type-text-long"));
        return comment;
    }
    public void checkPresentationComment(String comment) throws Exception {
        ViewFeedbackEvaluationPage viewFeedbackEvaluationPage = new ViewFeedbackEvaluationPage(webDriver, pageLocation);
        String commentActual = viewFeedbackEvaluationPage.getFieldPresentationComment().getText();
        Assert.assertTrue("'Presentation' comment is incorrect.", commentActual.equals(comment));
    }
    public void checkQAComment(String comment) throws Exception {
        ViewFeedbackEvaluationPage viewFeedbackEvaluationPage = new ViewFeedbackEvaluationPage(webDriver, pageLocation);
        String commentActual = viewFeedbackEvaluationPage.getFieldQAComment().getText();
        Assert.assertTrue("'QA' comment is incorrect.", commentActual.equals(comment));
    }
    public void checkEvaluationOfJury() throws Exception { // this method has exact data from doEvaluation() method
        ViewFeedbackEvaluationPage viewFeedbackEvaluationPage = new ViewFeedbackEvaluationPage(webDriver, pageLocation);
        // Tab NABS
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowNeed(), 0, commentNeed);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowApproach(), 1, commentApproach);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowBenefit(), 2, commentBenefit);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowCompetition(), 3, commentCompetition);

        // Tab Team
        viewFeedbackEvaluationPage.switchToTabTeam();
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowCommunicate(), 3, commentCommunicate);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowAchieved(), 2, commentAchieved);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowComplementary(), 1, commentComplementary);

        // Tab Presentation
        viewFeedbackEvaluationPage.switchToTabPresentation();
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowQuality(), 0, commentQuality);

        // Tab 'Business potential'
        viewFeedbackEvaluationPage.switchToTabBusinessPotential();
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowExitEvent(), 0, commentExitEvent);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowIdea(), 3, commentIdea);
        checkEvaluationInRow(viewFeedbackEvaluationPage.getRowFeasibility(), 3, commentFeasibility);

        // Tab 'Overall advice'
        viewFeedbackEvaluationPage.switchToTabOverallAdvice();
        checkPresentationComment(commentPresentation);
        checkQAComment(commentQA);
    }
    public boolean assertGroupSubmitted(String group) {
        String linkGroupJuryDuty = "//h3[contains(text(), 'Jury duty')]/../div/div/a[contains(text(), '" + group + "')]/div";
        String state = webDriver.findElement(By.xpath(linkGroupJuryDuty)).getText();
        Assert.assertTrue(state.equals("Submitted"));
        return true;
    }
    public boolean assertGroupSubmittedJuryHistory(String group) {
        String linkGroup = "//h3[contains(text(), 'Jury history')]/../div/div/a[contains(text(), '" + group + "')]/div";
        String state = webDriver.findElement(By.xpath(linkGroup)).getText();
        Assert.assertTrue(state.equals("Submitted"));
        return true;
    }
    public void openJuryHistory() throws Exception {
        String xpathLinkFeedback = "//h3[contains(text(), 'Jury duty')]/a[text()='(Show submitted feedback)']";
        webDriver.findElement(By.xpath(xpathLinkFeedback)).click(); // link in jury duty
        waitForPageToLoad();
    }

    public CreateFeedbackPage clickLastCreatedFeedbackSession() throws Exception {
        // Open Feedback overview page, select class
        webDriver.get(websiteUrl.concat("/jury/form/overview"));
        waitForPageToLoad();
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, pageLocation);
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
        // 'Cookie_notification' message can be shown on the page, it blocks clicking the link (Chrome)
        closeCookiesPopup();
        // Find the last feedback session link, click the link
        String xpathLastFeedbackSession = "//div[@class='l-content-inner']/div[2]/div[2]/a[last()]";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathLastFeedbackSession)));
        WebElement linkLastSession = webDriver.findElement(By.xpath(xpathLastFeedbackSession));
        scrollToElement(linkLastSession);
        clickItem(linkLastSession, "Link of the last created feedback session could not be clicked on OverviewFeedbackSession page.");
        return new CreateFeedbackPage(webDriver, pageLocation);
    }

    public WebElement getFeedbackSession(String feedbackSessionName, String groupName) {
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until jury block appears
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".jury")));
        String xpathSession = "//h3[contains(text(), 'Jury duty')]/../div/div/h4[text()='" + feedbackSessionName +
                "']/../a[contains(text(), 'Evaluate " + groupName + "')]";
        WebElement feedbackSession;
        if (webDriver.findElements(By.xpath(xpathSession)).size() == 1) {
            feedbackSession = webDriver.findElement(By.xpath(xpathSession));
            return feedbackSession;
        }
        Assert.assertTrue("Feedback session '" + feedbackSessionName + "' has not been found.", false);
        return null;
    }





}
