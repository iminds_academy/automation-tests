package com.inceptum.feedbackSessionTest;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.inceptum.pages.AddUserPage;
import com.inceptum.pages.CreateFeedbackPage;
import com.inceptum.pages.CreateGroupPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.OverviewFeedbackPage;
import com.inceptum.pages.ViewGroupPage;

/**
 * Created by Olga on 09.02.2015.
 */
public class FeedbackTest extends FeedbackBaseTest {

    /* ----- CONSTANTS ----- */


    protected final String errorMessageFull = ".*Feedback session name field is required.\nTemplate cannot be empty\nJury member field cannot be empty\nGroup field cannot be empty";


    /* ----- Tests ----- */

    @Test
    public void testCreateFeedbackSessionByAdmin() throws Exception {
        createFeedbackSessionByAdmin(feedbackSessionName, juryMember, className, groupName);
    }

    @Test // PILOT-1531 (can't create a new feedback session) !!!!!
    public void testCreateFeedbackSessionByContentEditor() throws Exception {
        // Add two users with roles 'jury member' and 'content editor' to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        addUserToClassWithRole(contentEditor, className, roleContentEditor);
        // Create a Group and add it to the Class
        createGroup(groupName, className);
        String viewGroupPage = webDriver.getCurrentUrl();
        // Add jury member, content editor to the group
        addUserToGroup(juryMember, groupName);
        webDriver.get(viewGroupPage); // get url of Overview Group page
        addUserToGroup(contentEditor, groupName);
        logout(admin);

        // Log in as content editor
        login(contentEditor);
        // Open Overview Feedback session page and find node of last created session
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        int lastSession = getNodeOfLastCreatedFeedbackSession();
        // Create new feedback session
        CreateFeedbackPage createFeedbackPage = overviewFeedbackPage.clickButtonNewFeedbackSession();
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        // Check that the new feedback session is created and displayed on Overview page
        int newSession = getNodeOfLastCreatedFeedbackSession();
        Assert.assertTrue("New session has not been created.", newSession == lastSession + 1);
    }
    @Test // PILOT-1532 (can't create a new feedback session) !!!!!
    public void testCreateFeedbackSessionByMasterEditor() throws Exception {
        // Add user with role 'jury member' to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Create user account with role 'master editor'
        checkUserIsAlreadyRegistered(masterEditor);
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(masterEditor);
        addUserPage.addUserWithRole(roleMasterEditor, masterEditor.get(0), email, masterEditor.get(1));

        // Create a Group and add it to the Class
        createGroup(groupName, className);
        // Add jury member to the group
        addUserToGroup(juryMember, groupName);
        logout(admin);

        // Log in as master editor
        login(masterEditor);
        // Open Overview Feedback session page and find node of last created session
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        int lastSession = getNodeOfLastCreatedFeedbackSession();
        // Create new feedback session
        CreateFeedbackPage createFeedbackPage = overviewFeedbackPage.clickButtonNewFeedbackSession();
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        // Check that the new feedback session is created and displayed on Overview page
        int newSession = getNodeOfLastCreatedFeedbackSession();
        Assert.assertTrue("New session has not been created.", newSession == lastSession + 1);
    }

    @Test // PILOT-1533 (Class can be selected only from the second attempt) !!!!!
    public void testCreateFeedbackSessionByClassManager() throws Exception {
        // Add user with role 'jury member' to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Create user account with role 'class manager'
        checkUserIsAlreadyRegistered(classManager);
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(classManager);
        addUserPage.addUserWithRole(roleClassManager, classManager.get(0), email, classManager.get(1));

        // Create a Group and add it to the Class
        createGroup(groupName, className);
        // Add jury member to the group
        addUserToGroup(juryMember, groupName);
        logout(admin);

        // Log in as master editor
        login(classManager);
        // Open Overview Feedback session page and find node of last created session
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        int lastSession = getNodeOfLastCreatedFeedbackSession();
        // Create new feedback session
        CreateFeedbackPage createFeedbackPage = overviewFeedbackPage.clickButtonNewFeedbackSession();
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);  // PILOT-1533 (only from the second attempt) !!!!!
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        // Check that the new feedback session is created and displayed on Overview page
        int newSession = getNodeOfLastCreatedFeedbackSession();
        Assert.assertTrue("New session has not been created.", newSession == lastSession + 1);
    }

    @Test // PILOT-1534 (Wrong class overview page opens for jury after evaluation) !!!!!
    public void testJuryDoEvaluation() throws Exception {
        // Create new Feedback session
        createFeedbackSessionByAdmin(feedbackSessionName, juryMember, className, groupName);
        logout(admin);
        // Log in as the jury member, open FeedbackEvaluation page
        login(juryMember);
        openFeedbackEvaluationPage(feedbackSessionName, groupName);

        // Check that correct group is selected on FeedbackEvaluation page
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName);
        // Do evaluation as the jury member
        doEvaluation();



        // !!!!!! Remove these two lines after PILOT-1534 is solved !!!!!!
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, pageLocation);
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!


        // Check that the feedback session has been submitted
        checkFeedbackSessionIsSubmitted(feedbackSessionName, groupName); // !!!!!!!
    }

    @Test // PILOT-1536 !!!!!!!!!!!!!! - RESOLVED
    public void testViewFeedbackAsStudent() throws Exception {
        // Create Feedback session, add student to Class and Group
        String urlViewGroupPage = createFeedbackSessionByAdmin(feedbackSessionName, juryMember, className, groupName);
        addStudentToClass(student, className);
        webDriver.get(urlViewGroupPage);
        addUserToGroup(student, groupName);
        logout(admin);

        // Log in as the jury member, open FeedbackEvaluation page
        login(juryMember);
        openFeedbackEvaluationPage(feedbackSessionName, groupName);
        // Check that correct group is selected on FeedbackEvaluation page
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName);
        // Do evaluation as the jury member
        doEvaluation();
        logout(juryMember);

        // Log in as the student, open jury's evaluation
        login(student);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        homePage.clickLinkFeedback();


        // !!!!!! Remove these two lines after PILOT-1536 is solved !!!!!!
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, pageLocation);
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!


        WebElement linkJuryEvaluation = webDriver.findElement(By.xpath("//a[text()='" + juryMember.get(0) + "']"));
        linkJuryEvaluation.click();

        // Check that the jury name is displayed on the page
        checkCorrectJuryIsSelectedOnEvaluationPage(juryMember);
        // Check whole evaluation of jury
        checkEvaluationOfJury();
    }

    @Test // PILOT-1536 (User sees default class after login) !!!!!! - RESOLVED
    public void testTwoJuriesDoEvaluation() throws Exception {
        // Add the 1st jury to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Add the 2nd jury to the class
        addUserToClassWithRole(juryMember2, className, roleJuryMember);

        // Create a Group and add it to the Class
        createGroup(groupName, className);
        String urlViewGroupPage = webDriver.getCurrentUrl();
        // Add two juries to the group
        addUserToGroup(juryMember, groupName);
        webDriver.get(urlViewGroupPage);
        addUserToGroup(juryMember2, groupName);

        // Create new feedback session (include the two juries)
        createFeedbackSessionForTwoJuries(juryMember, juryMember2);

        // Add student to Class and Group
        addStudentToClass(student, className);
        webDriver.get(urlViewGroupPage);
        addUserToGroup(student, groupName);
        logout(admin);

        // Log in as the 1st jury, check that correct group is selected on FeedbackEvaluation page
        login(juryMember);
        openFeedbackEvaluationPage(feedbackSessionName, groupName);
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName);
        // Do evaluation
        doEvaluation();
        logout(juryMember);

        // Log in as the 2nd jury, check that correct group is selected on FeedbackEvaluation page
        login(juryMember2);
        openFeedbackEvaluationPage(feedbackSessionName, groupName);
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName);
        // Do evaluation
        doEvaluation();
        logout(juryMember2);

        // Log in as the student, open OverviewFeedback page
        login(student);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        homePage.clickLinkFeedback();


        // !!!!!! Remove these two lines after PILOT-1536 is solved !!!!!!
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, pageLocation);
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!


        String urlOverviewFeedbackPage = webDriver.getCurrentUrl();
        // Check evaluation of the 1st jury
        WebElement linkJuryEvaluation = webDriver.findElement(By.xpath("//a[text()='" + juryMember.get(0) + "']"));
        linkJuryEvaluation.click();
        checkCorrectJuryIsSelectedOnEvaluationPage(juryMember);
        checkEvaluationOfJury();
        // Check evaluation of the 2nd jury
        webDriver.get(urlOverviewFeedbackPage);
        linkJuryEvaluation = webDriver.findElement(By.xpath("//a[text()='" + juryMember2.get(0) + "']"));
        linkJuryEvaluation.click();
        checkCorrectJuryIsSelectedOnEvaluationPage(juryMember2);
        checkEvaluationOfJury();
    }

    @Test
    public void testCheckValidationFeedbackSession() throws Exception {
        // Add the user with role 'jury member' to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Create a Group and add it to the Class
        createGroup(groupName, className);
        // Add jury member to the group
        addUserToGroup(juryMember, groupName);
        // Open Overview Feedback session page and find node of last created session
        HomePage homePage = openHomePage();
        OverviewFeedbackPage overviewFeedbackPage = homePage.clickLinkFeedback();
        int lastSession = getNodeOfLastCreatedFeedbackSession();

        // Check validation works (click button Save without filling fields)
        CreateFeedbackPage createFeedbackPage = overviewFeedbackPage.clickButtonNewFeedbackSession();
        createFeedbackPage.getButtonSave().click();
        // Check error message
        assertTextOfMessage(createFeedbackPage.getMessageError(), errorMessageFull);

        // Create Feedback session after the error
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        // Check that the new feedback session is created and displayed on Overview page
        int newSession = getNodeOfLastCreatedFeedbackSession();
        Assert.assertTrue("New session has not been created.", newSession == lastSession + 1);
    }

    @Test // PILOT-1534 (Wrong class overview page) !!!!!!!
    public void testEvaluationOfTwoGroupsInFeedbackSession() throws Exception {
        // Add new user with the role 'jury member' to the class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Create the 1st group and add the jury to it
        createGroup(groupName, className);
        addUserToGroup(juryMember, groupName);

        // Create the 2nd group and add the jury to it
        webDriver.get(websiteUrl.concat("/node/add/group"));
        waitForPageToLoad();
        CreateGroupPage createGroupPage = new CreateGroupPage(webDriver, pageLocation);
        createGroupPage.fillFieldTitle(groupName2);
        selectProperClassInGroupAudience(className);
        ViewGroupPage viewGroupPage = createGroupPage.clickButtonSave();
        assertTextOfMessage(viewGroupPage.getMessage(), ".*Group " + groupName2 + " has been created.");
        addUserToGroup(juryMember, groupName2);

        // Create a Feedback session (add two created groups to the session)
        webDriver.get(websiteUrl.concat("/jury/form/create"));
        waitForPageToLoad();
        CreateFeedbackPage createFeedbackPage = new CreateFeedbackPage(webDriver, pageLocation);
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName); // the 1st group
        autoSelectItemFromList(createFeedbackPage.getFieldGroups2(), groupName2); // the 2nd group
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        logout(admin);

        // Log in as the jury, do evaluation of the 1st group
        login(juryMember);
        openFeedbackEvaluationPage(feedbackSessionName, groupName);
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName);
        doEvaluation();


        // !!!!!! Remove these two lines after PILOT-1534 is solved !!!!!!
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, pageLocation);
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!


        assertGroupSubmitted(groupName); // group has status 'Submitted'

        // Do evaluation of the 2nd group
        openFeedbackEvaluationPage(feedbackSessionName, groupName2);
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName2);
        doEvaluation();


        // !!!!!! Remove the line after PILOT-1534 is solved !!!!!!
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!


        openJuryHistory();
        assertGroupSubmittedJuryHistory(groupName2); // group has status 'Submitted'
    }

    @Test
    public void testEditFeedbackSession() throws Exception {
        // Create feedback session
        createFeedbackSessionByAdmin(feedbackSessionName, juryMember, className, groupName);
        // Create new jury's account, add it to the Class
        addUserToClassWithRole(juryMember2, className, roleJuryMember);
        // Create new group and add the jury to it
        webDriver.get(websiteUrl.concat("/node/add/group"));
        waitForPageToLoad();
        CreateGroupPage createGroupPage = new CreateGroupPage(webDriver, pageLocation);
        createGroupPage.fillFieldTitle(groupName2);
        selectProperClassInGroupAudience(className);
        ViewGroupPage viewGroupPage = createGroupPage.clickButtonSave();
        assertTextOfMessage(viewGroupPage.getMessage(), ".*Group " + groupName2 + " has been created.");
        addUserToGroup(juryMember2, groupName2);

        // Edit Feedback session
        CreateFeedbackPage createFeedbackPage = clickLastCreatedFeedbackSession();
        createFeedbackPage.clearFieldGroups(); // edit group
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName2);
        createFeedbackPage.clearFieldJuryMembers(); // edit jury
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember2.get(0));
        // Save and Confirm these changes
        createFeedbackPage.clickButtonSave();

        logout(admin);

        // Log in as new jury member and check edited feedback session with new group is available for evaluation
        login(juryMember2);
        openFeedbackEvaluationPage(feedbackSessionName, groupName2);
        checkCorrectGroupIsSelectedOnEvaluationPage(groupName2);
    }

    @Test // PILOT-1534 !!!!
    public void testAddJuryMemberToTwoFeedbackSessions() throws Exception {
        // Create feedback session
        createFeedbackSessionByAdmin(feedbackSessionName, juryMember, className, groupName);
        // Create the 2nd Feedback session (select here the same juryMember)
        webDriver.get(websiteUrl.concat("/jury/form/create"));
        waitForPageToLoad();
        CreateFeedbackPage createFeedbackPage = new CreateFeedbackPage(webDriver, pageLocation);
        createFeedbackPage.fillFieldFeedbackSessionName(feedbackSessionName2);
        selectClassForFeedback(className);
        selectFeedbackContents(templateName);
        autoSelectItemFromList(createFeedbackPage.getFieldGroups(), groupName);
        autoSelectItemFromList(createFeedbackPage.getFieldJuryMembers(), juryMember.get(0));
        createFeedbackPage.clickButtonSave();

        logout(admin);

        // Log in as the jury, open OverviewFeedbackSession page
        login(juryMember);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        homePage.clickLinkFeedback();


        // !!!!!! Remove these two lines after PILOT-1534 is solved !!!!!!
        OverviewFeedbackPage overviewFeedbackPage = new OverviewFeedbackPage(webDriver, pageLocation);
        overviewFeedbackPage.selectClassOnFeedbackOverviewPage(className);
// !!!!!!!!!!!!!!!!!!!!!!!!!!!



        // Check that two Feedback sessions are displayed in Jury duty
        WebElement feedbackSession1 = getFeedbackSession(feedbackSessionName, groupName);
        WebElement feedbackSession2 = getFeedbackSession(feedbackSessionName2, groupName);
        Assert.assertTrue("The 1st feedback session is not displayed in Jury duty.", feedbackSession1.isDisplayed());
        Assert.assertTrue("The 2nd feedback session is not displayed in Jury duty.", feedbackSession2.isDisplayed());
    }




}
