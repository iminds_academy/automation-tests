package com.inceptum.frontpage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TestBase;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.AddFeedbackBlockPage;
import com.inceptum.pages.AddLeadBlockPage;
import com.inceptum.pages.AddMeetExpertsBlockPage;
import com.inceptum.pages.AddTextBlockPage;
import com.inceptum.pages.AddVideoBlockPage;
import com.inceptum.pages.AddWhatYouNeedToKnowBlockPage;
import com.inceptum.pages.ChangeFrontpageSettingsPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.SelectVideoPage;

/**
 * Created by Olga on 11.03.2015.
 */
public class FrontpageBaseTest extends TestBase {



    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final String nameTextBlock = "Text block";
    protected final String nameTwoColumns = "Two columns";
    protected final String nameLeadBlock = "Lead block";
    protected final String nameFeedbackBlock = "Feedback block";
    protected final String nameMeetExpertsBlock = "Meet the experts block";
    protected final String nameVideoBlock = "Video block for the frontpage";
    protected final String nameWhatYouNeedToKnowBlock = "What you need to know block for the frontpage";
    protected final String labelVideoBlock = "LabelVideoBlock";
    protected final String labelWhatYouNeedToKnowBlock = "LabelWhatYouNeedToKnowBlock";
    protected final String titleVideoBlock = "TitleVideoBlock";
    protected final String titleWhatYouNeedToKnowBlock = "TitleWhatYouNeedToKnowBlock";
    protected final String titleTextBlock = "TitleTextBlock";
    protected final String textForTextBlock = "TextForTextBlock";
    protected final String titleTextBlock2 = "TitleTextBlock2";
    protected final String textForTextBlock2 = "TextForTextBlock2";
    protected final String titleLeadBlock = "TitleLeadBlock";
    protected final String textForLeadBlock = "TextForLeadBlock";
    protected final String titleOfButton = "TitleOfButton";
    protected final String titleOfButton2 = "TitleOfButton2";
    protected final String urlSite = "https://www.wikipedia.org/";
    protected final String urlSite2 = "https://www.google.com/";
    protected final String titleFeedbackBlock = "TitleFeedbackBlock";
    protected final String nameOfPerson = "NameOfPerson";
    protected final String titleOfPerson = "TitleOfPerson";
    protected final String textFeedback = "TextFeedback";
    protected final String nameOfPerson2 = "NameOfPerson2";
    protected final String titleOfPerson2 = "TitleOfPerson2";
    protected final String textFeedback2 = "TextFeedback2";
    protected final String titleMeetExpertBlock = "TitleMeetExpertBlock";
    protected final String descriptionMeetExperts = "DescriptionMeetExperts";
    protected final String descriptionMeetExperts2 = "DescriptionMeetExperts2";
    protected final String titleMoreDetails = "TitleMoreDetails";
    protected final String titleInfoSection = "TitleInfoSection";
    protected final String titleInfoSection2 = "TitleInfoSection2";
    protected final String iconCalendar = "icon_calendar";
    protected final String iconLuggage = "icon_lugage";
    protected final String descriptionText = "DescriptionText";
    protected final String descriptionText2 = "DescriptionText2";
    protected final String titleInfoMoreDetails = "TitleInfoMoreDetails";
    protected final String titleInfoMoreDetails2 = "TitleInfoMoreDetails2";
    protected final String youtube = "youtube";
    protected final String vimeo = "vimeo";
    protected final String errorMessageWYNTKBlock = ".*Label field is required.\nURL field is required." +
            "\nYou cannot enter a title without a link url.\nTitle field is required.\nDescription field is required.";


    /* ----- Tests ----- */

    @Before
    public void beforeTest() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
        // Login as admin and clear Recent log messages
        loginAs(admin);
        clearLogMessages();
        webDriver.get(websiteUrl);
        waitForPageToLoad();
    }

    @After
    public void afterTest() throws Exception {
        // Open Home page and check admin is logged in
        HomePage homePage = openHomePage();
        String xpathLinkUserAccount = "//nav[@id='block-system-user-menu']/ul/li/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLinkUserAccount)));
        WebElement linkUserAccount = webDriver.findElement(By.xpath(xpathLinkUserAccount));
        if (!(linkUserAccount.getText().equals(admin.get(0)))) {
            if (linkUserAccount.getText().equals("Sign in")) { // 'Sign in' button instead of user link is displayed
                login(admin);
            } else { // log out if user link doesn't match with 'admin' (another user has been logged in)
                linkUserAccount.click();
                homePage.clickLinkSignOut();
                login(admin);
            }
        }
        webDriver.get(websiteUrl.concat("/admin/reports/dblog")); // open Recent log messages and check php messages
        waitForPageToLoad();
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> errors = new ArrayList<String>();
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available
                return;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {  // Find php messages starting with "Notice:"/"Warning:"/"Strict warning:"
                    errors.add(message.getAttribute("href"));
                }
            }
        }

        // Get all php messages
        for (String url : errors) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            System.out.println(fullMessage);
        }

        if (errors.size() > 0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }


    public void checkDefaultBlocksAreShown() throws Exception {
        waitUntilPageIsOpenedWithTitle("Change frontpage settings");
        Assert.assertTrue("No such block.", webDriver.findElement(By.xpath("//h3[text()='Lead block']")).isDisplayed()); // Lead block
        Assert.assertTrue("No such block.", webDriver.findElement(By.xpath("//h3[text()='Two columns']")).isDisplayed()); // Two columns
        int i = webDriver.findElements(By.xpath("//h3[text()='Text block']")).size(); // get number of Text blocks
        Assert.assertTrue("Number of Text blocks is less than two.", i >= 2);
        Assert.assertTrue("No such block.", webDriver.findElement(By.xpath("//h3[text()='Upcoming classes']")).isDisplayed()); // Upcoming classes
    }

    public WebElement addBlock(WebElement buttonAddBlock, String nameBlock) throws Exception {
        final String xpathBlocks = "//*[@id='frontpage-table']/tbody/tr/td/h3"; // xpath of blocks in the list
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlocks));
        final int numberOfBlocks = blocks.size(); // quantity of blocks in the list
        // Click AddBlock button and wait until a proper block appears
        waitUntilElementIsClickable(buttonAddBlock);
        buttonAddBlock.click();
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlocks));
                return blocks.size() == numberOfBlocks + 1; // new block is added
            }
        });
        // Check that the last block in the list is an added block and it has a proper name
        blocks = webDriver.findElements(By.xpath(xpathBlocks));
        WebElement newBlock = blocks.get(blocks.size() - 1);
        String nameBlockActual = newBlock.getText();
        Assert.assertTrue("Name of the added block is not equal to " + nameBlock, nameBlockActual.equals(nameBlock));
        return newBlock;
    }

    public void deleteBlock(WebElement block) throws Exception { // not a container!
    // (If a container is deleted the included blocks to it are also deleted. Result: another number of blocks)

        final String xpathBlocks = "//*[@id='frontpage-table']/tbody/tr/td/h3"; // xpath of blocks in the list
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlocks));
        final int numberOfBlocks = blocks.size(); // quantity of blocks in the list
        // Click Delete button for a block and wait until it's removed
        clickButtonDelete(block);
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlocks));
                return blocks.size() == numberOfBlocks - 1; // new block is removed
            }
        });
    }

    public void clickButtonConfigure(WebElement block) throws Exception {
        String xpathButtonConfigure = "./../../td[3]/div/input[@value='Configure']";
        WebElement buttonConfigure = block.findElement(By.xpath(xpathButtonConfigure));
        waitUntilElementIsClickable(buttonConfigure);
        buttonConfigure.click();
        waitForPageToLoad();
    }
    public void clickButtonDelete(WebElement block) throws Exception {
        String xpathButtonDelete = "./../../td[3]/div/input[@value='Delete']";
        WebElement buttonDelete = block.findElement(By.xpath(xpathButtonDelete));
        waitUntilElementIsClickable(buttonDelete);
        buttonDelete.click();
        waitForPageToLoad();
    }
    public void checkTextBlockOneColumnIsDisplayedOnFrontpage(String titleTextBlock, String textForTextBlock) {
        String xpathNewBlock = "//*[contains(@id, 'block-text')]/div/h2[text()='" + titleTextBlock + "']/../p[text()='" + textForTextBlock + "']";
        Assert.assertTrue("The Text block is not displayed on the Frontpage.", webDriver.findElement(By.xpath(xpathNewBlock)).isDisplayed());
    }
    public void checkTextBlocksTwoColumnsAreDisplayedOnFrontpage(String titleTextBlock1, String textForTextBlock1, String titleTextBlock2, String textForTextBlock2) {
        // Find a new (last) TwoColumns container on frontpage
        String xpathBlockTwoColumns = "//div[contains(@id, 'block-two-columns')][last()]";
        WebElement containerTwoColumns = webDriver.findElement(By.xpath(xpathBlockTwoColumns));
        // Find two Text blocks on frontpage
        String xpathTextBlock1 = "./div/div[@class='column_one']/div/div/h2[text()='" + titleTextBlock1 + "']/../p[text()='" + textForTextBlock1 + "']";
        String xpathTextBlock2 = "./div/div[@class='column_two']/div/div/h2[text()='" + titleTextBlock2 + "']/../p[text()='" + textForTextBlock2 + "']";
        WebElement textBlock1 = containerTwoColumns.findElement(By.xpath(xpathTextBlock1));
        WebElement textBlock2 = containerTwoColumns.findElement(By.xpath(xpathTextBlock2));
        // Check that the two Text blocks are displayed on the page
        Assert.assertTrue("The Text block " + titleTextBlock1 + " is not displayed on the frontpage.",
                textBlock1.isDisplayed());
        Assert.assertTrue("The Text block " + titleTextBlock2 + " is not displayed on the frontpage.",
                textBlock2.isDisplayed());
    }
    public void checkLeadBlockIsDisplayedOnFrontpage(String titleLeadBlock, String textForLeadBlock) {
        String xpathNewBlock = "//*[contains(@id, 'block-lead')]/div/h2[text()='" + titleLeadBlock + "']/../p[text()='" + textForLeadBlock + "']";
        Assert.assertTrue("The Lead block is not displayed on the Frontpage.", webDriver.findElement(By.xpath(xpathNewBlock)).isDisplayed());
    }
    public void checkButtonInLeadBlockHasCorrectData(String titleLeadBlock, String titleOfButton, String url) {
        String xpathButton = "//*[contains(@id, 'block-lead')]/div/h2[text()='" + titleLeadBlock + "']/../p/a[text()='" + titleOfButton + "']";
        WebElement button = webDriver.findElement(By.xpath(xpathButton));
        Assert.assertTrue("The button is not displayed on the Frontpage.", button.isDisplayed());
        String linkActual = button.getAttribute("href");
        Assert.assertTrue("The link of button in Lead block is incorrect: " + linkActual, linkActual.equals(url));
    }
    public void checkTextBlockIsNotDisplayedOnFrontpage(String titleTextBlock) {
        String xpathTextBlock = "//*[contains(@id, 'block-text')]/div/h2[text()='" + titleTextBlock + "']";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathTextBlock));
        int numberOfBlocks = blocks.size();
        Assert.assertTrue("The Text block '" + titleTextBlock + "' is displayed on frontpage.", numberOfBlocks == 0);
    }
    public void checkLeadBlockIsNotDisplayedOnFrontpage(String titleLeadBlock) {
        String xpathLeadBlock = "//*[contains(@id, 'block-lead')]/div/h2[text()='" + titleLeadBlock + "']";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathLeadBlock));
        int numberOfBlocks = blocks.size();
        Assert.assertTrue("The Lead block '" + titleLeadBlock + "' is displayed on frontpage.", numberOfBlocks == 0);
    }
    public void checkFeedbackBlockIsNotDisplayedOnFrontpage(String titleFeedbackBlock) {
        String xpathFeedbackBlock = "//*[contains(@id, 'block-feedback')]/div/h2[text()='" + titleFeedbackBlock + "']";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathFeedbackBlock));
        int numberOfBlocks = blocks.size();
        Assert.assertTrue("The Feedback block '" + titleFeedbackBlock + "' is displayed on frontpage.", numberOfBlocks == 0);
    }
    public void checkMeetExpertsBlockIsNotDisplayedOnFrontpage(String titleMeetExpertsBlock) {
        String xpathMeetExpertsBlock = "//*[contains(@id, 'block-experts')]/div/h2[text()='" + titleMeetExpertsBlock + "']";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathMeetExpertsBlock));
        int numberOfBlocks = blocks.size();
        Assert.assertTrue("The MeetExperts block '" + titleMeetExpertsBlock + "' is displayed on frontpage.", numberOfBlocks == 0);
    }
    public void checkFeedbackBlockIsDisplayedOnFrontpage(String titleFeedbackBlock, String textFeedback, String nameOfPerson, String titleOfPerson) {
        // Check if Feedback block is displayed on frontpage
        String xpathNewBlock = "//*[contains(@id, 'block-feedback')]/div/h2[text()='" + titleFeedbackBlock + "']/../div" +
                "/p[text()='" + textFeedback + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        Assert.assertTrue("The Feedback block is not displayed on the Frontpage.", block.isDisplayed());
        // Check if the avatar is displayed on frontpage
        String xpathAvatar = xpathNewBlock + "/../../div/div/img[contains(@src, '/image')]"; // 'image' = part of imagePath value
        WebElement avatar = webDriver.findElement(By.xpath(xpathAvatar));
        Assert.assertTrue("The avatar is not displayed in Feedback block on the Frontpage.", avatar.isDisplayed());
        // Check if person info is displayed correctly on frontpage
        String xpathPersonInfo = xpathNewBlock + "/../../div/div[@class='name']";
        WebElement personInfo = webDriver.findElement(By.xpath(xpathPersonInfo));
        String infoActual = personInfo.getText();
        String infoExpected = nameOfPerson + "\n" + titleOfPerson;
        Assert.assertTrue("Person info is not displayed in Feedback block on the Frontpage.", personInfo.isDisplayed());
        Assert.assertTrue("Person info is incorrect: " + infoActual, infoActual.equals(infoExpected));
    }
    public void checkTwoFeedbacksAreShownOnFrontpage(String titleFeedbackBlock, final List<String> personInfo1, final List<String> personInfo2) {
        // Find Feedback block
        final String xpathBlock = "//*[contains(@id, 'block-feedback')]/div/h2[text()='" + titleFeedbackBlock + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathBlock));
        // Find feedbacks on frontpage
        List<WebElement> info1 = block.findElements(By.xpath("./../div/p[text()='" + personInfo1.get(0) + "']")); // personInfo1.get(0) == textFeedback1
        List<WebElement> info2 = block.findElements(By.xpath("./../div/p[text()='" + personInfo2.get(0) + "']")); // personInfo2.get(0) == textFeedback2

        // Check two feedbacks are displayed randomly on frontpage after refreshing
        if (info1.size() == 1) { // if textFeedback1 has been found on frontpage
            checkFeedbackBlockIsDisplayedOnFrontpage(titleFeedbackBlock, personInfo1.get(0), personInfo1.get(1), personInfo1.get(2)); // check all data is correct for current feedback
            waitCondition(new ExpectedCondition<Boolean>() { // wait until another feedback is displayed on frontpage after refreshing the page
                public Boolean apply(WebDriver webDriver) {
                    webDriver.navigate().refresh();
                    WebElement blockRefreshed = webDriver.findElement(By.xpath(xpathBlock));
                    String feedback = blockRefreshed.findElement(By.xpath("./../div/p")).getText();
                    return feedback.equals(personInfo2.get(0)); // textFeedback2 is finally displayed, return
                }
            });
            return;
        } else {
            if (info2.size() == 1) { // if textFeedback2 has been found on frontpage
                checkFeedbackBlockIsDisplayedOnFrontpage(titleFeedbackBlock, personInfo2.get(0), personInfo2.get(1), personInfo2.get(2)); // check all data is correct for current feedback
                waitCondition(new ExpectedCondition<Boolean>() { // wait until another feedback is displayed on frontpage after refreshing the page
                    public Boolean apply(WebDriver webDriver) {
                        webDriver.navigate().refresh();
                        WebElement blockRefreshed = webDriver.findElement(By.xpath(xpathBlock));
                        String feedback = blockRefreshed.findElement(By.xpath("./../div/p")).getText();
                        return feedback.equals(personInfo1.get(0)); // textFeedback1 is finally displayed, return
                    }
                });
                return;
            }
        }
        Assert.assertTrue("No feedback info is displayed on frontpage.", false); // failed, if no feedback is displayed on frontpage
    }
    public void checkAvatarIsNotDisplayedOnFrontpage(String titleFeedbackBlock, String textFeedback) {
        String xpathBlock = "//*[contains(@id, 'block-feedback')]/div/h2[text()='" + titleFeedbackBlock + "']/../div" +
                "/p[text()='" + textFeedback + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathBlock));
        Assert.assertTrue("The Feedback block is not displayed on the Frontpage.", block.isDisplayed());
        // Check that the avatar is not displayed on frontpage
        String xpathAvatar = xpathBlock + "/../../div/div/img";
        int avatars = webDriver.findElements(By.xpath(xpathAvatar)).size();
        Assert.assertTrue("Avatar is displayed in Feedback block on frontpage.", avatars == 0);
    }
    public void checkMeetExpertBlockIsDisplayedOnFrontpage(String titleMeetExpertBlock, List<String> expert, String description) {
        String xpathNewBlock = "//*[contains(@id, 'block-experts')]/div/h2[text()='" + titleMeetExpertBlock + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        WebElement expertInfo = block.findElement(By.xpath("./../div/div/h3[text()='" + expert.get(0) + "']/../p[text()='" + description + "']"));
        Assert.assertTrue("Expert info is not displayed in MeetExperts block on frontpage.", expertInfo.isDisplayed());
    }
    public void checkExpertInfoIsNotShownOnFrontpage(String titleMeetExpertBlock, List<String> expert) {
        String xpathNewBlock = "//*[contains(@id, 'block-experts')]/div/h2[text()='" + titleMeetExpertBlock + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        List<WebElement> infoSections = block.findElements(By.xpath("./../div/div/h3[text()='" + expert.get(0) + "']"));
        Assert.assertTrue("Expert's info is displayed on frontpage.", infoSections.size() == 0);
    }
    public void checkProfilePictureIsShownOnFrontpage(String titleMeetExpertBlock) {
        String xpathNewBlock = "//*[contains(@id, 'block-experts')]/div/h2[text()='" + titleMeetExpertBlock + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        WebElement picture = block.findElement(By.xpath("./../div/div/img[contains(@src, '/image')]")); // 'image' = part of imagePath value
        Assert.assertTrue("The profile picture is not displayed in MeetExperts block on the Frontpage.", picture.isDisplayed());
    }
    public void checkProfilePictureIsNotShownOnFrontpage(String titleMeetExpertBlock) {
        String xpathNewBlock = "//*[contains(@id, 'block-experts')]/div/h2[text()='" + titleMeetExpertBlock + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        List<WebElement> numberPictures = block.findElements(By.xpath("./../div/div/img[contains(@src, '/image')]")); // 'image' = part of imagePath value
        Assert.assertTrue("The profile picture is displayed in MeetExperts block on the Frontpage.", numberPictures.size() == 0);
    }
    public void checkVideoBlockIsDisplayedOnFrontpage(String titleVideoBlock, String provider) {
        String xpathNewBlock = "//*[contains(@id, 'block-bean-video')]/div/h2[text()='" + titleVideoBlock + "']";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        String xpathIframe = "./../div/div/div/div/div/div/iframe[@class='media-" + provider + "-player']";
        List<WebElement> blocks = block.findElements(By.xpath(xpathIframe));
        int numberOfBlocks = blocks.size();
        Assert.assertTrue("The video block with the title is not displayed on frontpage.",
                numberOfBlocks == 1);
    }
    public void checkVideoBlockIsNotDisplayedOnFrontpage(String titleVideoBlock, String provider) {
        String xpathNewBlock = "//*[contains(@id, 'block-bean-video')]/div/h2[text()='" + titleVideoBlock + "']";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathNewBlock));
        Assert.assertTrue("The Video block is displayed on frontpage.", blocks.size() == 0);
        String xpathIframe = "//iframe[@class='media-" + provider + "-player']";
        List<WebElement> iframes = webDriver.findElements(By.xpath(xpathIframe));
        Assert.assertTrue("The Video iframe is displayed on frontpage.",
                iframes.size() == 0);
    }
    public void checkWYNTKBlockOnFrontpage(String titleBlock, String titleMoreDetails, String urlDetails, String titleInfoSection, String iconValue, String descriptionText, String titleInfoMoreDetails, String urlInfoDetails) {
        // Find 'What you need to know' block
        String xpathNewBlock = "//*[contains(@id, 'block-bean-wyntk')]/div/h2[text()='" + titleBlock + "']/..";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        // Find elements of this block
        WebElement icon = block.findElement(By.xpath("//div[contains(@class, '" + iconValue + "')]"));
        WebElement fieldTitleInfoSection = block.findElement(By.xpath("//div[text()='" + titleInfoSection + "']"));
        WebElement fieldDescription = block.findElement(By.xpath("//div/p[text()='" + descriptionText + "']"));
        WebElement linkInfoMoreDetails = block.findElement(By.xpath("//a[@href='" + urlInfoDetails + "'][text()='" + titleInfoMoreDetails + "']"));
        WebElement linkMoreDetails = block.findElement(By.xpath("//a[@href='" + urlDetails + "'][text()='" + titleMoreDetails + "']"));
        // Check that all these elements are displayed on frontpage
        List<WebElement> elements = Arrays.asList(icon, fieldTitleInfoSection, fieldDescription, linkInfoMoreDetails, linkMoreDetails);
        for (WebElement element : elements) {
            Assert.assertTrue("The element of WhatYouNeedToKnow block is not displayed on frontpage.", element.isDisplayed());
        }
    }
    public void checkWYNTKInfoIsNotShownOnFrontpage(String titleBlock, int numberOfInfoSections) {
        String xpathNewBlock = "//*[contains(@id, 'block-bean-wyntk')]/div/h2[text()='" + titleBlock + "']/..";
        WebElement block = webDriver.findElement(By.xpath(xpathNewBlock));
        // Find number of info sections in the block
        List<WebElement> infoSections = block.findElements(By.xpath("//div[contains(@class, 'field--name-field-wyntk-info')]/div/div[contains(@class, 'field__item')]"));
        Assert.assertTrue("Number of info sections in the block is not equal '" + numberOfInfoSections + "'.",
                infoSections.size() == numberOfInfoSections);
    }
    public void checkWhatYouNeedToKnowBlockIsNotShownOnFrontpage(String titleBlock) {
        String xpathNewBlock = "//*[contains(@id, 'block-bean-wyntk')]/div/h2[text()='" + titleBlock + "']/..";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathNewBlock));
        int numberOfBlocks = blocks.size();
        Assert.assertTrue("The 'What you need to know' block is displayed on frontpage.", numberOfBlocks == 0);
    }
    public void addTextBlock(String nameTextBlock, String titleTextBlock, String textForTextBlock) throws Exception {
        // Open EditFrontpage, add a new Text block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement newBlock = addBlock(changeFrontpageSettingsPage.getButtonAddTextBlock(), nameTextBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(newBlock);
        AddTextBlockPage addTextBlockPage = new AddTextBlockPage(webDriver, pageLocation);
        addTextBlockPage.fillFieldTitle(titleTextBlock);
        fillFrame(addTextBlockPage.getFrameText(), textForTextBlock);
        addTextBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new Text block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkTextBlockOneColumnIsDisplayedOnFrontpage(titleTextBlock, textForTextBlock);
    }
    public void addLeadBlock(String nameLeadBlock, String titleLeadBlock, String textForLeadBlock, String imgPath, String titleOfButton, String url) throws Exception {
        // Open EditFrontpage, add a new Lead block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement leadBlock = addBlock(changeFrontpageSettingsPage.getButtonAddLeadBlock(), nameLeadBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(leadBlock);
        AddLeadBlockPage addLeadBlockPage = new AddLeadBlockPage(webDriver, pageLocation);
        addLeadBlockPage.fillFieldTitle(titleLeadBlock); // title
        addLeadBlockPage.fillFieldText(textForLeadBlock); // text
        uploadImage(addLeadBlockPage.getFieldUpload(), imgPath, addLeadBlockPage.getButtonUpload()); // upload image
        waitUntilElementIsVisible(addLeadBlockPage.getButtonRemove());
        // Add a button to the Lead block
        addLeadBlockPage.clickButtonAddButton();
        addLeadBlockPage.fillFieldTitleOfButton(titleOfButton); // title of button
        addLeadBlockPage.fillFieldUrlOfButton(url); // url of button
        addLeadBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new Lead block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkLeadBlockIsDisplayedOnFrontpage(titleLeadBlock, textForLeadBlock);
        // Check that added to the Lead block button has defined title and url
        checkButtonInLeadBlockHasCorrectData(titleLeadBlock, titleOfButton, url);
    }
    public void addFeedbackBlock(String nameFeedbackBlock, String titleFeedbackBlock, String nameOfPerson, String titleOfPerson, String imgPath, String textFeedback) throws Exception {
        // Open EditFrontpage, add a new Feedback block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement feedbackBlock = addBlock(changeFrontpageSettingsPage.getButtonAddFeedbackBlock(), nameFeedbackBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(feedbackBlock);
        AddFeedbackBlockPage addFeedbackBlockPage = new AddFeedbackBlockPage(webDriver, pageLocation);
        addFeedbackBlockPage.fillFieldTitle(titleFeedbackBlock);
        addFeedbackBlockPage.clickButtonAddFeedback();
        addFeedbackBlockPage.fillFieldNameOfPerson(nameOfPerson);
        addFeedbackBlockPage.fillFieldHisTitle(titleOfPerson);
        uploadImage(addFeedbackBlockPage.getFieldAvatar(), imgPath, addFeedbackBlockPage.getButtonUpload()); // add avatar
        waitUntilElementIsVisible(addFeedbackBlockPage.getButtonRemove());
        fillFrame(addFeedbackBlockPage.getIframeFeedback(), textFeedback);
        addFeedbackBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new Feedback block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkFeedbackBlockIsDisplayedOnFrontpage(titleFeedbackBlock, textFeedback, nameOfPerson, titleOfPerson);
    }
    public void addMeetExpertsBlock(String nameMeetExpertsBlock, String titleMeetExpertBlock, final List<String> expert, String description) throws Exception {
        // Open EditFrontpage, add a new MeetExperts block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement meetExpertsBlock = addBlock(changeFrontpageSettingsPage.getButtonAddMeetExpertsBlock(), nameMeetExpertsBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(meetExpertsBlock);
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        addMeetExpertsBlockPage.fillFieldTitle(titleMeetExpertBlock); // title
        addMeetExpertsBlockPage.clickButtonAddExpert();
        autoSelectItemFromList(addMeetExpertsBlockPage.getFieldUser(), expert.get(0)); // expert
        final WebElement fieldName = addMeetExpertsBlockPage.getFieldName();
        waitUntilNameFieldIsAutomaticallyFilled(fieldName, expert); // wait until Name field is filled in
        // Check that Override Name checkbox is not selected
        Assert.assertTrue("OverrideName checkbox is selected.", !(addMeetExpertsBlockPage.getCheckboxOverrideName().isSelected()));
        // Check that Name field is disabled
        Assert.assertTrue("The field 'Name' is enabled.", !(fieldName.isEnabled())); // disabled
        fillFrame(addMeetExpertsBlockPage.getIframeDescription(), description); // description
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new MeetExperts block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        waitForPageToLoad();

        // // PILOT-1545 !!!!!!!!!

        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, description);
    }
    public String addVideoBlock(String provider, String urlVideo) throws Exception {
        // Open EditFrontpage, add a new Video block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement videoBlock = addBlock(changeFrontpageSettingsPage.getButtonAddVideoBlock(), nameVideoBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(videoBlock);
        AddVideoBlockPage addVideoBlockPage = new AddVideoBlockPage(webDriver, pageLocation);
        String titleActual = addVideoBlockPage.getFieldTitle().getAttribute("value"); // get title
        SelectVideoPage selectVideoPage = addVideoBlockPage.clickLinkSelectMedia(); // select video
        selectVideoPage.selectVideo(urlVideo);
        waitUntilElementIsVisible(addVideoBlockPage.getLinkRemoveMedia());
        addVideoBlockPage.clickButtonSave();

        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new Video block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkVideoBlockIsDisplayedOnFrontpage(titleActual, provider);
        return titleActual;
    }
    public String addWhatYouNeedToKnowBlock() throws Exception {
        // Open EditFrontpage, add a new 'What you need to know' block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement whatYouNeedToKnowBlock = addBlock(changeFrontpageSettingsPage.getButtonAddWhatYouNeedToKnowBlock(),
                nameWhatYouNeedToKnowBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(whatYouNeedToKnowBlock);
        AddWhatYouNeedToKnowBlockPage addWhatYouNeedToKnowBlockPage = new AddWhatYouNeedToKnowBlockPage(webDriver, pageLocation);
        String titleBlock = addWhatYouNeedToKnowBlockPage.getFieldTitle().getAttribute("value");
        // Fill in 'More details' fieldset
        addWhatYouNeedToKnowBlockPage.fillFieldTitleMoreDetails(titleMoreDetails);
        addWhatYouNeedToKnowBlockPage.fillFieldUrlMoreDetails(urlSite);
        // Fill in info section
        addWhatYouNeedToKnowBlockPage.fillFieldTitleInfoSection(titleInfoSection);
        selectItemFromListByOptionValue(addWhatYouNeedToKnowBlockPage.getFieldIcon(), iconCalendar);
        fillFrame(addWhatYouNeedToKnowBlockPage.getIframeDescription(), descriptionText);
        addWhatYouNeedToKnowBlockPage.fillFieldTitleInfoMoreDetails(titleInfoMoreDetails);
        addWhatYouNeedToKnowBlockPage.fillFieldUrlInfoMoreDetails(urlSite2);
        addWhatYouNeedToKnowBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkWYNTKBlockOnFrontpage(titleBlock, titleMoreDetails, urlSite, titleInfoSection, iconCalendar, descriptionText,
                titleInfoMoreDetails, urlSite2);
        return titleBlock;
    }
    public void waitUntilNameFieldIsAutomaticallyFilled(final WebElement fieldName, final List<String> expert) throws Exception {
        waitCondition(new ExpectedCondition<Boolean>() {  // wait until value in the Name field equals selected expert title
            public Boolean apply(WebDriver webDriver) {
                String value = fieldName.getAttribute("value");
                return value.equals(expert.get(0));
            }
        });
    }
    public void waitUntilFieldNameIsEnabled(final WebElement fieldName) throws Exception {
        waitCondition(new ExpectedCondition<Boolean>() {  // wait until the Name field is enabled
            public Boolean apply(WebDriver webDriver) {
                return fieldName.isEnabled();
            }
        });
    }

    public WebElement getLastBlock(String nameBlock) {
        String xpathBlocks = "//*[@id='frontpage-table']/tbody/tr/td/h3"; // xpath of blocks in the list
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlocks));
        WebElement lastBlock = blocks.get(blocks.size() - 1); // the last block
        String nameBlockActual = lastBlock.getText();
        Assert.assertTrue("Name of the added block is not equal to " + nameBlock, nameBlockActual.equals(nameBlock));
        return lastBlock;
    }
    public void deleteNotDefaultBlocks() throws Exception {
        // Open 'Change frontpage settings'
        HomePage homePage = openHomePage();
        openEditPage();
        // Find not default elements from the list (number of default blocks = 5; exclude inline blocks) and Delete them
        final String xpathBlocks = "//*[@id='frontpage-table']/tbody/tr[position()>5]/td[1][not(div[@class='indentation'])]";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlocks));
        Iterator<WebElement> iterator = blocks.iterator();
        while (iterator.hasNext()) {
            WebElement block = iterator.next();
            final int numberOfBlocks = webDriver.findElements(By.xpath(xpathBlocks)).size();
            WebElement buttonDelete = block.findElement(By.xpath("./../td[3]/div/input[@value='Delete']"));
            buttonDelete.click(); // click button Delete
            waitCondition(new ExpectedCondition<Boolean>() { // wait until number of blocks is decreased in one
                public Boolean apply(WebDriver webDriver) {
                    List<WebElement> blocksUpdated = webDriver.findElements(By.xpath(xpathBlocks));
                    return blocksUpdated.size() == numberOfBlocks - 1; // a block is removed
                }
            });
            blocks = webDriver.findElements(By.xpath(xpathBlocks));
            iterator = blocks.iterator();
        }
        // Submit changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();
        waitUntilElementIsVisible(homePage.getTabView());
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
    }
    public void checkNewBlockIsNotPresentedInFrontpageTable() throws Exception {
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        waitUntilElementIsVisible(changeFrontpageSettingsPage.getTable());
        String xpathBlock = "//*[@id='frontpage-table']/tbody/tr";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlock)); // get list of all blocks
        int numberOfBlocks = blocks.size(); // get number of blocks
        // Check that the number and names of default blocks are correct
        Assert.assertTrue("Actual number of blocks is: '" + numberOfBlocks + "'", numberOfBlocks == 5); // 5 default blocks
        checkDefaultBlocksAreShown();
    }
    public void checkNewBlockIsDisplayedInFrontpageTable(String nameBlock) throws Exception {
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        waitUntilElementIsVisible(changeFrontpageSettingsPage.getTable());
        String xpathBlock = "//*[@id='frontpage-table']/tbody/tr";
        List<WebElement> blocks = webDriver.findElements(By.xpath(xpathBlock)); // get list of all blocks
        int numberOfBlocks = blocks.size(); // get number of blocks
        // Check that the number of blocks more than the number of default blocks
        Assert.assertTrue("Actual number of blocks is: '" + numberOfBlocks + "'", numberOfBlocks > 5); // 5 default blocks
        getLastBlock(nameBlock); // check here if the last block has the expected name
    }
    public void checkBlockIsShownWithIndentation(WebElement block) {
        String classActual = block.findElement(By.xpath("./../div")).getAttribute("class");
        Assert.assertTrue(classActual.equals("indentation"));
    }
    public void checkImageLeadBlockIsShown(String titleLeadBlock) {
        String xpathImage = "//*[contains(@id, 'block-lead')]/div/h2[text()='" + titleLeadBlock + "']/../div";
        WebElement image = webDriver.findElement(By.xpath(xpathImage));
        String attributeActual = image.getAttribute("style"); // 'image' in this attribute = partial name of image file
        Assert.assertTrue("The image in lead block is not displayed.", attributeActual.contains("block_lead/image"));
    }
    public void checkImageLeadBlockIsNotShown(String titleLeadBlock) {
        String xpathImage = "//*[contains(@id, 'block-lead')]/div/h2[text()='" + titleLeadBlock + "']/../div";
        WebElement image = webDriver.findElement(By.xpath(xpathImage));
        String attributeActual = image.getAttribute("style"); // 'image' in this attribute = partial name of image file
        Assert.assertTrue("The image is displayed in lead block.", (!(attributeActual.contains("block_lead/image"))));
    }
    public void checkProfilePictureBlockIsDisabled() throws Exception {
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        WebElement block = addMeetExpertsBlockPage.getBlockProfilePicture();
        String stateActual = block.getAttribute("disabled");
        Assert.assertTrue("The block 'Profile picture' is enabled.'", stateActual.equals("true"));
    }
    public void checkViewProfileLink(List<String> user) throws Exception {
        // Click 'View profile' link on frontpage
        String xpathLink = "//h3[text()='" + user.get(0) + "']/../div/a";
        WebElement link = webDriver.findElement(By.xpath(xpathLink));
        closeCookiesPopup();
        clickItem(link, "The link 'View profile' on frontpage could not be clicked.");
        // Wait until information on profile page is displayed
        String cssName = ".field--name-field-profile-full-name .field__item";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(cssName)));
        WebElement fieldName = webDriver.findElement(By.cssSelector(cssName));
        // Compare actual name with expected name
        String nameActual = fieldName.getText();
        Assert.assertTrue("Actual username '" + nameActual + "' is not equal to expected one: " + user.get(0),
                nameActual.equals(user.get(0)));
    }
    public void waitUntilLabelFieldHasError() throws Exception {
        AddWhatYouNeedToKnowBlockPage addWhatYouNeedToKnowBlockPage = new AddWhatYouNeedToKnowBlockPage(webDriver, pageLocation);
        final WebElement labelField = addWhatYouNeedToKnowBlockPage.getFieldLabel();
        waitCondition(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webDriver) {
                String error = labelField.getAttribute("class");
                return error.equals("form-text required error"); // the field has error
            }
        });
    }
    public void checkOrderOfTextBlocksOnFrontpage(String titleTextBlockFirst, String titleTextBlockSecond) {
        // Get order number of the 1st text block
        String xpathId1 = "//h2[text()='" + titleTextBlockFirst + "']/../..";
        String id1 = webDriver.findElement(By.xpath(xpathId1)).getAttribute("id");
        String[] parts1 = id1.split("block-text--");
        id1 = parts1[1];
        int number1 = Integer.parseInt(id1);

        // Get order number of the 2nd text block
        String xpathId2 = "//h2[text()='" + titleTextBlockSecond + "']/../..";
        String id2 = webDriver.findElement(By.xpath(xpathId2)).getAttribute("id");
        String[] parts2 = id2.split("block-text--");
        id2 = parts2[1];
        int number2 = Integer.parseInt(id2);

        // Check that the 1st block has lower weight than the 2nd one
        Assert.assertTrue("The order of blocks is not correct on frontpage.", number1 < number2);
    }


}
