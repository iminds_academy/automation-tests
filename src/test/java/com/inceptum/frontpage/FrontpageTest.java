package com.inceptum.frontpage;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.pages.AddFeedbackBlockPage;
import com.inceptum.pages.AddLeadBlockPage;
import com.inceptum.pages.AddMeetExpertsBlockPage;
import com.inceptum.pages.AddTextBlockPage;
import com.inceptum.pages.AddVideoBlockPage;
import com.inceptum.pages.AddWhatYouNeedToKnowBlockPage;
import com.inceptum.pages.ChangeFrontpageSettingsPage;
import com.inceptum.pages.EditClassPage;
import com.inceptum.pages.EditUserAccountPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.PeoplePage;
import com.inceptum.pages.ProfileSettingsPage;
import com.inceptum.pages.RegisterForCoursePage;
import com.inceptum.pages.SelectVideoPage;

/**
 * Created by Olga on 11.03.2015.
 */
public class FrontpageTest extends FrontpageBaseTest {

    /* ----- CONSTANTS ----- */

    // protected final List<String> administrator = Arrays.asList("administrator", "administrator");


    /* ----- Tests ----- */


// 'Upcoming classes' block

    @Test
    public void testRegistrationWithRegisterLinkAutomaticState() throws Exception {
        // Create an authenticated user account
        addAuthenticatedUser(student);
        // Delete class with defined name and add a new one with 'Automatic' state
        deleteClass(className);
        addClassAutomaticState(className, 1, 1, 2); // select start/end date/deadlineDate = actual date + 1 day; capacity = 2
        // Check that 'Registered' and 'View' links are shown on the HomePage
        openHomePage();
        checkClassLinkIsShown(className, linkRegistered); // registered
        checkClassLinkIsShown(className, linkViewClass); // view

        logout(admin);

        // Check that only 'Registered' link is shown for anonymous user
        checkClassLinkIsShown(className, linkRegister);
        checkClassLinkIsNotPresented(className, linkViewClass);

        // Click 'Registered' link, log in as the authenticated user
        clickClassLink(className, linkRegister);
        LoginFormPage loginFormPage = new LoginFormPage(webDriver, pageLocation);
        waitUntilElementIsVisible(loginFormPage.getFormLogin());
        fillFieldsOnLoginFormAndSubmit(student);
        // Check Register form is opened and the Class is already selected
        RegisterForCoursePage registerForCoursePage = new RegisterForCoursePage(webDriver, pageLocation);
        waitUntilElementIsVisible(registerForCoursePage.getLabelRegisterForACourse());
        checkClassIsSelectedOnRegisterPage(className);
        // Check that the Class overview page is opened after registration
        registerForCoursePage.clickButtonConfirmRegistration();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        waitUntilElementIsVisible(overviewPage.getFieldClass());
        String selectedClass = webDriver.findElement(By.cssSelector(".class-select [selected]")).getText();
        Assert.assertTrue("The class " + className + " is not selected on the Overview page.", selectedClass.equals(className));

        // Check that 'Registered' and 'View' links are shown on the HomePage
        openHomePage();
        checkClassLinkIsShown(className, linkRegistered);
        checkClassLinkIsShown(className, linkViewClass);

        // Check that link 'Complete' is shown for anonymous user
        logout(student);
        checkClassLinkIsShown(className, linkCompleteClass);
    }

    @Test
    public void testFirstRegistrationForCourseAutomaticState() throws Exception {
        // Create an authenticated user account
        addAuthenticatedUser(student);
        // Delete class with defined name and add a new one with 'Automatic' state
        deleteClass(className);
        addClassAutomaticState(className, 1, 1, 2); // select start/end date/deadlineDate = actual date + 1 day; capacity = 2

        logout(admin);

        // Check that Registration page is opened after login
        submitLoginForm(student);
        RegisterForCoursePage registerForCoursePage = new RegisterForCoursePage(webDriver, pageLocation);
        waitUntilElementIsVisible(registerForCoursePage.getLabelRegisterForACourse());
        // Check that no class is selected on Register form
        WebElement radiobuttonClass = checkNoClassIsSelectedOnRegisterPage(className); // get the Class
        // Register for the Class and check the Class overview page is opened
        radiobuttonClass.sendKeys(Keys.SPACE);
        registerForCoursePage.clickButtonConfirmRegistration();
        assertClassIsSelected(className);

        // Links 'Registered' and 'View' are shown on the HomePage
        openHomePage();
        checkClassLinkIsShown(className, linkRegistered);
        checkClassLinkIsShown(className, linkViewClass);

        // Check that link 'Complete' is shown for anonymous user
        logout(student);
        checkClassLinkIsShown(className, linkCompleteClass);
    }

    @Test // change to testAutomaticStateDeadlineDateIsReached() !!!!!!!!!!!!!!!!!
    public void testAutomaticStateStartDateIsReached() throws Exception {
        // Delete class with defined name and add a new one with 'Automatic' state
        deleteClass(className);
        addClassAutomaticState(className, 1, 1, 2); // select start/end date = actual date + 1 day; capacity = 2

        logout(admin);

        // Check the link 'Register' is shown on the HomePage
        checkClassLinkIsShown(className, linkRegister);

        // Open the ClassEdit page and edit Start date < Actual date
        login(admin);
        clickClassLink(className, linkViewClass);
        assertClassIsSelected(className);
        openEditPage();
        EditClassPage editClassPage = new EditClassPage(webDriver, pageLocation);
        selectDateDiffFormat(editClassPage.getFieldStartDate(), -1); // start date = actual date - 1 day
        editClassPage.clickButtonSave();
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        assertTextOfMessage(overviewPage.getInfoMessage(), ".*Class " + className + " has been updated.");

        // Check that the Class is not displayed on HomePage anymore (deadline - start date - has been reached)
        openHomePage();
        checkClassIsNotShownOnFrontpage(className);
        // Check the Class is not visible for anonymous user too
        logout(admin);
        checkClassIsNotShownOnFrontpage(className);
    }

    @Test
    public void testOpenClosedForRegistrationState() throws Exception {
        // Create an authenticated user account
        addAuthenticatedUser(student);
        // Delete class with defined name and add a new one with 'Open for registration' state
        deleteClass(className);
        addClassOpenForRegistrationState(className, -1, 1); // select start(end) date = actual date - 1(+1) day

        // Check that 'Registered' and 'View' links are shown on the HomePage
        openHomePage();
        checkClassLinkIsShown(className, linkRegistered); // registered
        checkClassLinkIsShown(className, linkViewClass); // view

        // Check that 'Register' link is displayed for anonymous user
        logout(admin);
        checkClassLinkIsShown(className, linkRegister); // register

        // Open EditClass page and select the option 'Closed for registration'
        login(admin);
        clickClassLink(className, linkViewClass);
        assertClassIsSelected(className);
        openEditPage();
        EditClassPage editClassPage = new EditClassPage(webDriver, pageLocation);
        checkRegistrationSettingsIsOpen();
        editClassPage.selectOptionClosedForRegistration();
        editClassPage.clickButtonSave();

        // Check that the Class with 'closed' status is not shown on Frontpage
        openHomePage();
        checkClassIsNotShownOnFrontpage(className);

        logout(admin);
        // Check that the Class with 'closed' status is not shown for anonymous user
        checkClassIsNotShownOnFrontpage(className);

        login(student);
        // Check that the Class with 'closed' status is not shown for registered user
        checkClassIsNotShownOnFrontpage(className);
    }

    @Test
    public void testPrivateRegistrationState() throws Exception {
        // Delete class with defined name and add a new one with 'Private registration' state
        deleteClass(className);
        addClassPrivateRegistrationState(className, -1, 1); // select start(end) date = actual date - 1(+1) day

        // Check that the Class is not shown on the Home page (Frontpage)
        checkClassIsNotShownOnFrontpage(className);
    }

    @Test
    public void testRegistrationApprovalBeforeJoiningClass() throws Exception {
        // Create an authenticated user account
        addAuthenticatedUser(student);
        // Delete class with defined name, add a new one with 'Open for registration' state, 'User need to get approval...' option
        deleteClass(className);
        addClassOpenForRegistrationNeedApproval(className, -1, 1); // select start(end) date = actual date - 1(+1) day

        // Check the link 'Register' is displayed on Home page
        logout(admin);
        checkClassLinkIsShown(className, linkRegister);
        // Log in with the link
        clickClassLink(className, linkRegister);
        fillFieldsOnLoginFormAndSubmit(student);
        // Check 'Registration contact' is shown near the Class on Registration form, register to the Class
        RegisterForCoursePage registerForCoursePage = new RegisterForCoursePage(webDriver, pageLocation);
        waitUntilElementIsVisible(registerForCoursePage.getLabelRegisterForACourse());
        checkClassIsSelectedOnRegisterPage(className);
        registerForCoursePage.clickButtonConfirmRegistration();

        // Check the proper link is displayed on Home page, info message is shown
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(),
                ".*You have joined " + className + " and an administrator will approve your registration soon.");
        checkClassLinkIsShown(className, linkRegistrationPending); // 'Registration pending' link is shown
        checkClassLinkIsNotPresented(className, linkRegistered); // 'Registered' link is not shown
    }

    @Test
    public void testDefaultBlocks() throws Exception {
        // Open "Change frontpage settings" page and check that default blocks are added
        deleteNotDefaultBlocks();
        openEditPage();
        checkDefaultBlocksAreShown();

        // Add authenticated user to Class
        addStudentToClass(student, className);

        logout(admin);
        login(student);

        // Check that the user is redirected to Overview Class page by clicking 'See course overview' button
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewPage overviewPage = homePage.clickButtonSeeCourseOverview();
        WebElement selectedClass = overviewPage.getFieldClass().findElement(By.cssSelector("option[selected]"));
        String actualClassName = selectedClass.getText();
        Assert.assertTrue(className + " is not selected on Overview page.", actualClassName.equals(className));
        // Check that the user is redirected to Overview Class page by clicking 'Go to full program' button
        openHomePage();
        overviewPage = clickButtonGoToFullProgram();
        selectedClass = overviewPage.getFieldClass().findElement(By.cssSelector("option[selected]"));
        actualClassName = selectedClass.getText();
        Assert.assertTrue(className + " is not selected on Overview page.", actualClassName.equals(className));
    }



// Text block

    @Test
    public void testAddTextBlock() throws Exception {
        deleteNotDefaultBlocks();
        addTextBlock(nameTextBlock, titleTextBlock, textForTextBlock);
    }

    @Test
    public void testEditTextBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Text block
        addTextBlock(nameTextBlock, titleTextBlock, textForTextBlock);
        // Open Edit Text block page with Configure button, edit title and text
        openEditPage();
        WebElement newBlock = getLastBlock(nameTextBlock);
        clickButtonConfigure(newBlock);
        AddTextBlockPage addTextBlockPage = new AddTextBlockPage(webDriver, pageLocation);
        addTextBlockPage.getFieldTitle().clear();
        addTextBlockPage.fillFieldTitle(titleTextBlock2);
        fillFrame(addTextBlockPage.getFrameText(), textForTextBlock2);
        addTextBlockPage.clickButtonSave();
        // Submit the changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the edited Text block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkTextBlockOneColumnIsDisplayedOnFrontpage(titleTextBlock2, textForTextBlock2);
    }

    @Test
    public void testDeleteTextBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Text block
        addTextBlock(nameTextBlock, titleTextBlock, textForTextBlock);
        // Delete the block with a proper button
        openEditPage();
        WebElement newBlock = getLastBlock(nameTextBlock);
        deleteBlock(newBlock);
        // Submit the changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the block is not displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkTextBlockIsNotDisplayedOnFrontpage(titleTextBlock);
    }

    @Test
    public void testAddTextBlockWithoutSubmitting() throws Exception {
        deleteNotDefaultBlocks();
        // Open EditFrontpage, add a new Text block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement newBlock = addBlock(changeFrontpageSettingsPage.getButtonAddTextBlock(), nameTextBlock);
        // Add information to the block with Configure button
        clickButtonConfigure(newBlock);
        AddTextBlockPage addTextBlockPage = new AddTextBlockPage(webDriver, pageLocation);
        addTextBlockPage.fillFieldTitle(titleTextBlock);
        fillFrame(addTextBlockPage.getFrameText(), textForTextBlock);
        addTextBlockPage.clickButtonSave();
        waitUntilElementIsVisible(changeFrontpageSettingsPage.getTable());
        // Check the block is displayed in the frontpage table
        checkNewBlockIsDisplayedInFrontpageTable(nameTextBlock);

        // The frontpage table is not submitted; open frontpage and check the Text block is not shown there
        openHomePage();
        checkTextBlockIsNotDisplayedOnFrontpage(titleTextBlock);
        // Open fronpage table and check the block is not shown there
        openEditPage();
        checkNewBlockIsNotPresentedInFrontpageTable();
    }

    @Test
    public void testDeleteTextBlockWithoutSubmitting() throws Exception {
        deleteNotDefaultBlocks();
        // Open EditFrontpage, add a new Text block
        addTextBlock(nameTextBlock,titleTextBlock, textForTextBlock);
        // Open frontpage table and click Delete button near the block
        openEditPage();
        WebElement newBlock = getLastBlock(nameTextBlock);
        deleteBlock(newBlock); // here decreased number of blocks is checked

        // The frontpage table is not submitted; open frontpage and check the Text block is still displayed there
        HomePage homePage = openHomePage();
        waitUntilElementIsVisible(homePage.getMenuClass());
        checkTextBlockOneColumnIsDisplayedOnFrontpage(titleTextBlock, textForTextBlock);
        // Check the block is still displayed in the frontpage table
        openEditPage();
        checkNewBlockIsDisplayedInFrontpageTable(nameTextBlock);
    }

    @Test
    public void testAddTwoTextColumns() throws Exception {
        deleteNotDefaultBlocks();
        // Add a container TwoBlocks to frontpage
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        addBlock(changeFrontpageSettingsPage.getButtonAddTwoColumns(), nameTwoColumns);
        // Add the 1st Text block to the container
        WebElement textBlock1 = addBlock(changeFrontpageSettingsPage.getButtonAddTextBlock(), nameTextBlock);
        checkBlockIsShownWithIndentation(textBlock1);
        // Save information in the block
        clickButtonConfigure(textBlock1);
        AddTextBlockPage addTextBlockPage = new AddTextBlockPage(webDriver, pageLocation);
        addTextBlockPage.fillFieldTitle(titleTextBlock);
        fillFrame(addTextBlockPage.getFrameText(), textForTextBlock);
        addTextBlockPage.clickButtonSave();

        // Add the 2nd Text block to the container
        WebElement textBlock2 = addBlock(changeFrontpageSettingsPage.getButtonAddTextBlock(), nameTextBlock);
        checkBlockIsShownWithIndentation(textBlock2);
        // Save information in the block
        clickButtonConfigure(textBlock2);
        addTextBlockPage.fillFieldTitle(titleTextBlock2);
        fillFrame(addTextBlockPage.getFrameText(), textForTextBlock2);
        addTextBlockPage.clickButtonSave();

        // Submit changes, check that two Text blocks are displayed in the container on the frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkTextBlocksTwoColumnsAreDisplayedOnFrontpage(titleTextBlock, textForTextBlock, titleTextBlock2, textForTextBlock2);
    }


// Lead block

    @Test
    public void testAddLeadBlock() throws Exception {
        deleteNotDefaultBlocks();
        addLeadBlock(nameLeadBlock, titleLeadBlock, textForLeadBlock, image3Path, titleOfButton, urlSite);
    }

    @Test
    public void testAddDeleteAnotherButtonToLeadBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new lead block with a button
        addLeadBlock(nameLeadBlock, titleLeadBlock, textForLeadBlock, image3Path, titleOfButton, urlSite);
        // Add the second button to the lead block
        openEditPage();
        WebElement leadBlock = getLastBlock(nameLeadBlock);
        clickButtonConfigure(leadBlock);
        AddLeadBlockPage addLeadBlockPage = new AddLeadBlockPage(webDriver, pageLocation);
        addLeadBlockPage.clickButtonAddButton();
        addLeadBlockPage.fillFieldTitleOfButton2(titleOfButton2);
        addLeadBlockPage.fillFieldUrlOfButton2(urlSite2);
        addLeadBlockPage.clickButtonSave();
        // Submit changes and check if two buttons are displayed in the lead block on frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkButtonInLeadBlockHasCorrectData(titleLeadBlock, titleOfButton, urlSite); // the 1st button
        checkButtonInLeadBlockHasCorrectData(titleLeadBlock, titleOfButton2, urlSite2); // the 2nd button

        // Delete the 1st button in the lead block
        openEditPage();
        leadBlock = getLastBlock(nameLeadBlock);
        clickButtonConfigure(leadBlock);
        addLeadBlockPage.clickButtonDeleteButton();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // Wait until fieldset for the deleted button disappears
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-buttons-buttons-2")));
        addLeadBlockPage.clickButtonSave();
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the 2nd button is still shown on frontpage
        checkButtonInLeadBlockHasCorrectData(titleLeadBlock, titleOfButton2, urlSite2);
        // Check that the 1st button is not shown on the page
        int numberOfButtons = webDriver.findElements(By.linkText(titleOfButton)).size(); // with defined title
        Assert.assertTrue("The button with title '" + titleOfButton + "' is displayed on frontpage.", numberOfButtons == 0);
    }

    @Test
    public void testRemoveImageFromLeadBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new lead block with an image
        addLeadBlock(nameLeadBlock, titleLeadBlock, textForLeadBlock, image3Path, titleOfButton, urlSite);
        // Check that the image is displayed on frontpage
        checkImageLeadBlockIsShown(titleLeadBlock);

        // Delete the image
        openEditPage();
        WebElement leadBlock = getLastBlock(nameLeadBlock);
        clickButtonConfigure(leadBlock);
        AddLeadBlockPage addLeadBlockPage = new AddLeadBlockPage(webDriver, pageLocation);
        addLeadBlockPage.clickButtonRemove();
        addLeadBlockPage.clickButtonSave();
        // Submit changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the image is not shown in the Lead block
        checkImageLeadBlockIsNotShown(titleLeadBlock);
    }

    @Test
    public void testDeleteLeadBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new lead block
        addLeadBlock(nameLeadBlock, titleLeadBlock, textForLeadBlock, image3Path, titleOfButton, urlSite);

        // Delete the block with a proper button
        openEditPage();
        WebElement leadBlock = getLastBlock(nameLeadBlock);
        deleteBlock(leadBlock);
        // Submit the changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Check that the block is not displayed on frontpage
        checkLeadBlockIsNotDisplayedOnFrontpage(titleLeadBlock);
    }


// Feedback block

    @Test
    public void testAddFeedbackBlock() throws Exception {
        deleteNotDefaultBlocks();
        addFeedbackBlock(nameFeedbackBlock, titleFeedbackBlock, nameOfPerson, titleOfPerson, image3Path, textFeedback);
    }

    @Test
    public void testRemoveAvatarFromFeedbackBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add Feedback block with avatar
        addFeedbackBlock(nameFeedbackBlock, titleFeedbackBlock, nameOfPerson, titleOfPerson, image3Path, textFeedback);

        // Delete avatar from the Feedback block
        openEditPage();
        WebElement feedbackBlock = getLastBlock(nameFeedbackBlock);
        clickButtonConfigure(feedbackBlock);
        AddFeedbackBlockPage addFeedbackBlockPage = new AddFeedbackBlockPage(webDriver, pageLocation);
        addFeedbackBlockPage.clickButtonRemove();
        addFeedbackBlockPage.clickButtonSave();
        // Submit the changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Check that the avatar is not displayed in Feedback block on frontpage
        checkAvatarIsNotDisplayedOnFrontpage(titleFeedbackBlock, textFeedback);
    }

    @Test
    public void testDeleteFeedbackBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Feedback block
        addFeedbackBlock(nameFeedbackBlock, titleFeedbackBlock, nameOfPerson, titleOfPerson, image3Path, textFeedback);

        // Delete the block with a proper button
        openEditPage();
        WebElement feedbackBlock = getLastBlock(nameFeedbackBlock);
        deleteBlock(feedbackBlock);
        // Submit the changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Check that the block is not displayed on frontpage
        checkFeedbackBlockIsNotDisplayedOnFrontpage(titleFeedbackBlock);
    }

    @Test
    public void testAddDeleteFeedbackFromFeedbackBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Feedback block
        addFeedbackBlock(nameFeedbackBlock, titleFeedbackBlock, nameOfPerson, titleOfPerson, image3Path, textFeedback);

        // Add the 2nd feedback for this block
        openEditPage();
        WebElement feedbackBlock = getLastBlock(nameFeedbackBlock);
        clickButtonConfigure(feedbackBlock);
        AddFeedbackBlockPage addFeedbackBlockPage = new AddFeedbackBlockPage(webDriver, pageLocation);
        addFeedbackBlockPage.clickButtonAddFeedback();
        addFeedbackBlockPage.fillFieldNameOfPerson2(nameOfPerson2);
        addFeedbackBlockPage.fillFieldHisTitle2(titleOfPerson2);
        uploadImage(addFeedbackBlockPage.getFieldAvatar2(), image3Path, addFeedbackBlockPage.getButtonUpload2()); // add avatar
        waitUntilElementIsVisible(addFeedbackBlockPage.getButtonRemove2());
        fillFrame(addFeedbackBlockPage.getIframeFeedback2(), textFeedback2);
        addFeedbackBlockPage.clickButtonSave();
        // Submit changes for frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that two feedbacks are shown randomly on frontpage after refreshing
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        List<String> personInfo1 = Arrays.asList(textFeedback, nameOfPerson, titleOfPerson); // info for the 1st feedback
        List<String> personInfo2 = Arrays.asList(textFeedback2, nameOfPerson2, titleOfPerson2); // info for the 2nd feedback
        checkTwoFeedbacksAreShownOnFrontpage(titleFeedbackBlock, personInfo1, personInfo2);

        // Delete the 1st feedback from the block
        openEditPage();
        feedbackBlock = getLastBlock(nameFeedbackBlock);
        clickButtonConfigure(feedbackBlock);
        addFeedbackBlockPage.clickButtonDeleteFeedback();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // Wait until fieldset for the deleted feedback disappears
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-feedback-feedback-1-personal-delete-button-button")));
        addFeedbackBlockPage.clickButtonSave();
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the 2nd feedback is displayed on frontpage
        checkFeedbackBlockIsDisplayedOnFrontpage(titleFeedbackBlock, textFeedback2, nameOfPerson2, titleOfPerson2);
    }



// Meet experts block

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testAddMeetExpertsBlock() throws Exception {
        // Create an authenticated user account
        addAuthenticatedUser(expert);
        // Delete extra blocks
        deleteNotDefaultBlocks();

        // !!!!!!!!!!!!!!!!

        // Add MeetExperts block
        addMeetExpertsBlock(nameMeetExpertsBlock, titleMeetExpertBlock, expert, descriptionMeetExperts);
    }

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testAddDeleteExpertFromExpertsBlock() throws Exception {
        // Add two authenticated users (experts)
        addAuthenticatedUser(expert); // the 1st expert
        addAuthenticatedUser(student); // the 2nd expert

        deleteNotDefaultBlocks();
        // Add MeetExperts block with the 1st expert to frontpage
        addMeetExpertsBlock(nameMeetExpertsBlock, titleMeetExpertBlock, expert, descriptionMeetExperts);

        // Add the 2nd expert to the block
        openEditPage();
        WebElement meetExpertsBlock = getLastBlock(nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        addMeetExpertsBlockPage.clickButtonAddExpert();
        // Fill the fields for the expert
        autoSelectItemFromList(addMeetExpertsBlockPage.getFieldUser2(), student.get(0));
        WebElement fieldName2 = addMeetExpertsBlockPage.getFieldName2();
        waitUntilNameFieldIsAutomaticallyFilled(fieldName2, student); // wait until the name is shown in Name field
        // Check that Override Name checkbox is not selected
        Assert.assertTrue("OverrideName checkbox is selected.", !(addMeetExpertsBlockPage.getCheckboxOverrideName2().isSelected()));
        // Check that Name field is disabled
        Assert.assertTrue("The field 'Name' is enabled.", !(fieldName2.isEnabled())); // disabled
        fillFrame(addMeetExpertsBlockPage.getIframeDescription2(), descriptionMeetExperts2); // add description
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that two experts' info sections are displayed in the block on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, descriptionMeetExperts); // the 1st expert
        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, student, descriptionMeetExperts2); // the 2nd expert

        // Delete the 2nd expert from the block
        openEditPage();
        meetExpertsBlock = getLastBlock(nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        addMeetExpertsBlockPage.clickButtonDeleteExpert();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // Wait until fieldset for the deleted expert disappears
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-experts-elements-1-form-personal-delete-button")));
        addMeetExpertsBlockPage.clickButtonSave();
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that only the 2nd expert is displayed in the block on frontpage
        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, student, descriptionMeetExperts2); // the 2nd expert's info section is shown
        checkExpertInfoIsNotShownOnFrontpage(titleMeetExpertBlock, expert); // the 1st expert info section is not shown
    }

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testUploadRemoveProfilePictureForExpert() throws Exception {
        // Add an authenticated user (expert)
        addAuthenticatedUser(expert);

        deleteNotDefaultBlocks();
        // Add a new MeetExperts block with profile image of the expert
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement meetExpertsBlock = addBlock(changeFrontpageSettingsPage.getButtonAddMeetExpertsBlock(), nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        addMeetExpertsBlockPage.fillFieldTitle(titleMeetExpertBlock); // title
        addMeetExpertsBlockPage.clickButtonAddExpert();
        autoSelectItemFromList(addMeetExpertsBlockPage.getFieldUser(), expert.get(0)); // expert
        final WebElement fieldName = addMeetExpertsBlockPage.getFieldName();
        waitUntilNameFieldIsAutomaticallyFilled(fieldName, expert); // wait until Name field is filled in
        // Check profile picture block is disabled
        checkProfilePictureBlockIsDisabled();
        // Select Override to enable profile picture editing
        addMeetExpertsBlockPage.selectCheckboxOverridePicture();
        // Upload the picture
        uploadImage(addMeetExpertsBlockPage.getFieldProfilePicture(), image3Path, addMeetExpertsBlockPage.getButtonUpload());
        waitUntilElementIsVisible(addMeetExpertsBlockPage.getButtonRemove());
        fillFrame(addMeetExpertsBlockPage.getIframeDescription(), descriptionMeetExperts); // description
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the MeetExperts block with the profile picture is displayed on frontpage


        // !!!!!!!!!!!!

        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, descriptionMeetExperts);

        // !!!!!!!!!!!

        checkProfilePictureIsShownOnFrontpage(titleMeetExpertBlock);

        // Remove profile picture from the block
        openEditPage();
        meetExpertsBlock = getLastBlock(nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        addMeetExpertsBlockPage.clickButtonRemove();
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check the block with expert's info is displayed
        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, descriptionMeetExperts);
        // Check that the profile picture is not shown in the block
        checkProfilePictureIsNotShownOnFrontpage(titleMeetExpertBlock);
    }

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testOverrideProfilePictureInExpertsBlock() throws Exception {
        // Add an authenticated user (expert)
        addAuthenticatedUser(expert);
        // Add a profile picture to this user from People table
        PeoplePage peoplePage = openPeoplePage();
        EditUserAccountPage editUserAccountPage = clickEditLinkInPeopleTable(expert);
        ProfileSettingsPage profileSettingsPage = editUserAccountPage.switchToTabProfileSettings();
        profileSettingsPage.uploadImage(image3Path);
        profileSettingsPage.clickButtonSave();
        assertTextOfMessage(peoplePage.getMessage(), ".*The changes have been saved.*");

        deleteNotDefaultBlocks();
        // Add a new MeetExperts block with profile image of the expert
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement meetExpertsBlock = addBlock(changeFrontpageSettingsPage.getButtonAddMeetExpertsBlock(), nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        addMeetExpertsBlockPage.fillFieldTitle(titleMeetExpertBlock); // title
        addMeetExpertsBlockPage.clickButtonAddExpert();
        autoSelectItemFromList(addMeetExpertsBlockPage.getFieldUser(), expert.get(0)); // expert
        // Check that the Remove button appears, so the user's profile picture has been automatically uploaded
        waitUntilElementIsVisible(addMeetExpertsBlockPage.getButtonRemove());
        // Check profile picture block is disabled
        checkProfilePictureBlockIsDisabled();

        // Select Override to enable profile picture editing
        addMeetExpertsBlockPage.selectCheckboxOverridePicture();
        // Remove the picture and upload a new one
        addMeetExpertsBlockPage.clickButtonRemove();
        uploadImage(addMeetExpertsBlockPage.getFieldProfilePicture(), image4Path, addMeetExpertsBlockPage.getButtonUpload());
        waitUntilElementIsVisible(addMeetExpertsBlockPage.getButtonRemove());
        // Check that image icon is displayed
        Assert.assertTrue("The image icon is not displayed on AddMeetExpertsBlock page.", webDriver.findElement(By.xpath("//a[contains(text(), 'image')]")).isDisplayed());

        // Add description and Save
        fillFrame(addMeetExpertsBlockPage.getIframeDescription(), descriptionMeetExperts); // description
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the MeetExperts block with the profile picture is displayed on frontpage

        // !!!!!!!!!

        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, descriptionMeetExperts);

        // !!!!!!!!!

        checkProfilePictureIsShownOnFrontpage(titleMeetExpertBlock);
    }

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testOverrideUserNameInExpertsBlock() throws Exception {
        // Add an authenticated user
        addAuthenticatedUser(student);

        deleteNotDefaultBlocks();
        // Add a new MeetExperts block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement meetExpertsBlock = addBlock(changeFrontpageSettingsPage.getButtonAddMeetExpertsBlock(), nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        addMeetExpertsBlockPage.fillFieldTitle(titleMeetExpertBlock); // title
        addMeetExpertsBlockPage.clickButtonAddExpert();
        // Select the user in 'User' field
        autoSelectItemFromList(addMeetExpertsBlockPage.getFieldUser(), student.get(0));
        // Wait until the field 'Name' is automatically filled in
        WebElement fieldName = addMeetExpertsBlockPage.getFieldName();
        waitUntilNameFieldIsAutomaticallyFilled(fieldName, student);
        // Select checkbox Override near the Name field and wait until it is enabled
        addMeetExpertsBlockPage.selectCheckboxOverrideName();
        waitUntilFieldNameIsEnabled(fieldName);
        // Enter new username into the field
        fieldName.clear();
        fieldName.sendKeys(expert.get(0));

        fillFrame(addMeetExpertsBlockPage.getIframeDescription(), descriptionMeetExperts); // description
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the MeetExperts block with a new expert's name is displayed on frontpage

        // !!!!!!!!!!!!!!!

        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, descriptionMeetExperts);
    }

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testAddInternalProfileLinkToExpert() throws Exception {
        // Add an authenticated user
        addAuthenticatedUser(expert);

        deleteNotDefaultBlocks();
        // Add a new MeetExperts block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement meetExpertsBlock = addBlock(changeFrontpageSettingsPage.getButtonAddMeetExpertsBlock(), nameMeetExpertsBlock);
        clickButtonConfigure(meetExpertsBlock);
        AddMeetExpertsBlockPage addMeetExpertsBlockPage = new AddMeetExpertsBlockPage(webDriver, pageLocation);
        addMeetExpertsBlockPage.fillFieldTitle(titleMeetExpertBlock); // title
        addMeetExpertsBlockPage.clickButtonAddExpert();
        // Select an expert
        autoSelectItemFromList(addMeetExpertsBlockPage.getFieldUser(), expert.get(0));
        // Select 'Internal profile' option
        addMeetExpertsBlockPage.selectRadiobuttonInternalProfile();
        fillFrame(addMeetExpertsBlockPage.getIframeDescription(), descriptionMeetExperts); // description
        addMeetExpertsBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the MeetExperts block is displayed on frontpage

        // !!!!!!!!!!!!!!! Add the line back after the bug is resolved !!!!!!!
//        checkMeetExpertBlockIsDisplayedOnFrontpage(titleMeetExpertBlock, expert, descriptionMeetExperts);
        // !!!!!!!!!!!!


        // Check that correct page is opened with 'View profile' link
        checkViewProfileLink(expert);
    }


    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testDeleteExpertsBlock() throws Exception {
        // Create an authenticated user account
        addAuthenticatedUser(expert);

        deleteNotDefaultBlocks();
        // Add MeetExperts block
        addMeetExpertsBlock(nameMeetExpertsBlock, titleMeetExpertBlock, expert, descriptionMeetExperts);

        // Delete the block with a proper button
        openEditPage();
        WebElement meetExpertsBlock = getLastBlock(nameMeetExpertsBlock);
        deleteBlock(meetExpertsBlock);
        // Submit the changes
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Check that the block is not displayed on frontpage

        // !!!!!!!!!!!
        checkMeetExpertsBlockIsNotDisplayedOnFrontpage(titleMeetExpertBlock);
    }



// Video block


    @Test // PILOT-1547 (Video block can't be added) !!!!!!!!!!!!!
    public void testAddVideoBlock() throws Exception {
        // Delete not default blocks
        deleteNotDefaultBlocks();

        // Add a new Video block
        addVideoBlock(youtube, urlYouTube);
    }

    @Test // PILOT-1547 (Video block can't be added) !!!!!!!!!!!!!
    public void testEditVideoTitleAndLabelOfVideoBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Video block
        addVideoBlock(youtube, urlYouTube);

        // Edit label and title of the block
        openEditPage();
        WebElement videoBlock = getLastBlock(nameVideoBlock);
        clickButtonConfigure(videoBlock);
        AddVideoBlockPage addVideoBlockPage = new AddVideoBlockPage(webDriver, pageLocation);
        addVideoBlockPage.getFieldLabel().clear();
        addVideoBlockPage.fillFieldLabel(labelVideoBlock); // label
        addVideoBlockPage.getFieldTitle().clear();
        addVideoBlockPage.fillFieldTitle(titleVideoBlock); // title
        // Save and check that info message contains the new title
        addVideoBlockPage.clickButtonSave();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        assertTextOfMessage(changeFrontpageSettingsPage.getMessage(), ".*Video block " + titleVideoBlock + " has been updated.");

        // Submit changes and check that the video block is displayed with the new title
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkVideoBlockIsDisplayedOnFrontpage(titleVideoBlock, youtube);

        // Open Edit page and check that Video block label is changed
        openEditPage();
        getLastBlock(labelVideoBlock);
    }

    @Test // PILOT-1547 (Video block can't be added) !!!!!!!!!!!!!
    public void testEditMediaInVideoBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Video block (youtube)
        String titleVideoBlock = addVideoBlock(youtube, urlYouTube);

        // Remove youtube video
        openEditPage();
        WebElement videoBlock = getLastBlock(nameVideoBlock);
        clickButtonConfigure(videoBlock);
        AddVideoBlockPage addVideoBlockPage = new AddVideoBlockPage(webDriver, pageLocation);
        addVideoBlockPage.clickLinkRemoveMedia();
        // Select vimeo video
        SelectVideoPage selectVideoPage = addVideoBlockPage.clickLinkSelectMedia(); // select video
        selectVideoPage.selectVideo(urlVimeo);
        waitUntilElementIsVisible(addVideoBlockPage.getLinkRemoveMedia());
        addVideoBlockPage.clickButtonSave();

        // Submit changes for frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the Video block with vimeo video is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkVideoBlockIsDisplayedOnFrontpage(titleVideoBlock, vimeo);
    }

    @Test // PILOT-1547 (Video block can't be added) !!!!!!!!!!!!!
    public void testCheckValidationForVideoBlock() throws Exception {
        deleteNotDefaultBlocks();

        // Add a new Video block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement videoBlock = addBlock(changeFrontpageSettingsPage.getButtonAddVideoBlock(), nameVideoBlock);
        // Check validation on AddVideoBlock page
        clickButtonConfigure(videoBlock);
        AddVideoBlockPage addVideoBlockPage = new AddVideoBlockPage(webDriver, pageLocation);
        addVideoBlockPage.getFieldLabel().clear(); // clear Label field
        addVideoBlockPage.clickButtonSave(); // save
        // Check error message
        assertTextOfMessage(addVideoBlockPage.getMessage(), ".*Label field is required.\nVideo is required.");

        // Fill in all required fields and Save
        addVideoBlockPage.fillFieldLabel(labelVideoBlock); // fill Label field
        String titleActual = addVideoBlockPage.getFieldTitle().getAttribute("value"); // get title
        SelectVideoPage selectVideoPage = addVideoBlockPage.clickLinkSelectMedia(); // select video
        selectVideoPage.selectVideo(urlVimeo);
        waitUntilElementIsVisible(addVideoBlockPage.getLinkRemoveMedia());
        addVideoBlockPage.clickButtonSave();

        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new Video block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkVideoBlockIsDisplayedOnFrontpage(titleActual, vimeo);
    }

    @Test // PILOT-1547 (Video block can't be added) !!!!!!!!!!!!!
    public void testDeleteVideoBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new Video block
        String titleVideoBlock = addVideoBlock(youtube, urlYouTube); // get title of the video block

        // Delete the Video block with a proper button
        openEditPage();
        WebElement videoBlock = getLastBlock(nameVideoBlock);
        deleteBlock(videoBlock);
        // Submit changes for frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the Video block is not displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkVideoBlockIsNotDisplayedOnFrontpage(titleVideoBlock, youtube);
    }



// 'What do you need to know' block

    @Test
    public void testAddWhatYouNeedToKnowBlock() throws Exception {
        // Delete Not default blocks
        deleteNotDefaultBlocks();
        // Add a new 'What you need to know' block
        addWhatYouNeedToKnowBlock();
    }

    @Test
    public void testEditLabelAndTitleOfWhatYouNeedToKnowBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new 'What you need to know' block
        addWhatYouNeedToKnowBlock();

        // Edit label and title of the block
        openEditPage();
        WebElement block = getLastBlock(nameWhatYouNeedToKnowBlock);
        clickButtonConfigure(block);
        AddWhatYouNeedToKnowBlockPage addWhatYouNeedToKnowBlockPage = new AddWhatYouNeedToKnowBlockPage(webDriver, pageLocation);
        addWhatYouNeedToKnowBlockPage.getFieldLabel().clear();
        addWhatYouNeedToKnowBlockPage.fillFieldLabel(labelWhatYouNeedToKnowBlock); // label
        addWhatYouNeedToKnowBlockPage.getFieldTitle().clear();
        addWhatYouNeedToKnowBlockPage.fillFieldTitle(titleWhatYouNeedToKnowBlock); // title
        // Save and check that info message contains the new title
        addWhatYouNeedToKnowBlockPage.clickButtonSave();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        assertTextOfMessage(changeFrontpageSettingsPage.getMessage(),
                ".*What you need to know block " + titleWhatYouNeedToKnowBlock + " has been updated.");

        // Submit changes and check that the video block is displayed with the new title
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkWYNTKBlockOnFrontpage(titleWhatYouNeedToKnowBlock, titleMoreDetails, urlSite, titleInfoSection,
                iconCalendar, descriptionText, titleInfoMoreDetails, urlSite2);

        // Open Edit page and check that Video block label is changed
        openEditPage();
        getLastBlock(labelWhatYouNeedToKnowBlock);
    }

    @Test
    public void testAddRemoveAnotherInfoSectionFromWhatYouNeedToKnowBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new 'What you need to know' block
        String titleBlock = addWhatYouNeedToKnowBlock(); // get title of the block

        // Add another info section to the block
        openEditPage();
        WebElement block = getLastBlock(nameWhatYouNeedToKnowBlock);
        clickButtonConfigure(block);
        AddWhatYouNeedToKnowBlockPage addWhatYouNeedToKnowBlockPage = new AddWhatYouNeedToKnowBlockPage(webDriver, pageLocation);
        addWhatYouNeedToKnowBlockPage.clickButtonAddAnotherItem();
        // Enter data to the 2nd info section
        addWhatYouNeedToKnowBlockPage.fillFieldTitleInfoSection2(titleInfoSection2);
        selectItemFromListByOptionValue(addWhatYouNeedToKnowBlockPage.getFieldIcon2(), iconLuggage);
        fillFrame(addWhatYouNeedToKnowBlockPage.getIframeDescription2(), descriptionText2);
        addWhatYouNeedToKnowBlockPage.fillFieldTitleInfoMoreDetails2(titleInfoMoreDetails2);
        addWhatYouNeedToKnowBlockPage.fillFieldUrlInfoMoreDetails2(urlSite2);
        addWhatYouNeedToKnowBlockPage.clickButtonSave();

        // Submit changes for frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the block with two info sections are displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkWYNTKBlockOnFrontpage(titleBlock, titleMoreDetails, urlSite, titleInfoSection, iconCalendar, descriptionText,
                titleInfoMoreDetails, urlSite2); // the 1st info section
        checkWYNTKBlockOnFrontpage(titleBlock, titleMoreDetails, urlSite, titleInfoSection2, iconLuggage, descriptionText2,
                titleInfoMoreDetails2, urlSite2); // the 2nd info section

        // Remove the 1st info section from the block
        openEditPage();
        block = getLastBlock(nameWhatYouNeedToKnowBlock);
        clickButtonConfigure(block);
        addWhatYouNeedToKnowBlockPage.clickButtonRemove();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until the fieldset of deleted item is removed
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-field-wyntk-info-und-1-remove-button")));
        addWhatYouNeedToKnowBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the block with only one info section (the 2nd) is displayed on frontpage
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkWYNTKBlockOnFrontpage(titleBlock, titleMoreDetails, urlSite, titleInfoSection2, iconLuggage, descriptionText2,
                titleInfoMoreDetails2, urlSite2); // the 2nd info section is shown
        checkWYNTKInfoIsNotShownOnFrontpage(titleBlock, 1); // only one info section is shown
    }

    @Test
    public void testCheckValidationForWhatYouNeedToKnowBlock() throws Exception {
        deleteNotDefaultBlocks();

        // Add a new 'What you need to know' block
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        WebElement block = addBlock(changeFrontpageSettingsPage.getButtonAddWhatYouNeedToKnowBlock(), nameWhatYouNeedToKnowBlock);
        // Check validation on AddWhatYouNeedToKnowBlock page
        clickButtonConfigure(block);
        AddWhatYouNeedToKnowBlockPage addWhatYouNeedToKnowBlockPage = new AddWhatYouNeedToKnowBlockPage(webDriver, pageLocation);
        addWhatYouNeedToKnowBlockPage.getFieldLabel().clear(); // clear Label field
        addWhatYouNeedToKnowBlockPage.fillFieldTitleInfoMoreDetails(titleInfoMoreDetails); // enter value into Title field (info section, More details)
        addWhatYouNeedToKnowBlockPage.clickButtonSave();

        // Check error message
        waitUntilLabelFieldHasError();
        assertTextOfMessage(addWhatYouNeedToKnowBlockPage.getMessage(), errorMessageWYNTKBlock);

        // Fill in the fields on the page
        String titleBlock = addWhatYouNeedToKnowBlockPage.getFieldTitle().getAttribute("value");
        addWhatYouNeedToKnowBlockPage.fillFieldLabel(labelWhatYouNeedToKnowBlock);
        addWhatYouNeedToKnowBlockPage.fillFieldTitleMoreDetails(titleMoreDetails); // 'More details' fieldset
        addWhatYouNeedToKnowBlockPage.fillFieldUrlMoreDetails(urlSite);
        addWhatYouNeedToKnowBlockPage.fillFieldTitleInfoSection(titleInfoSection); // info section
        selectItemFromListByOptionValue(addWhatYouNeedToKnowBlockPage.getFieldIcon(), iconCalendar);
        fillFrame(addWhatYouNeedToKnowBlockPage.getIframeDescription(), descriptionText);
        addWhatYouNeedToKnowBlockPage.fillFieldUrlInfoMoreDetails(urlSite2);
        addWhatYouNeedToKnowBlockPage.clickButtonSave();
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the new block is displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkWYNTKBlockOnFrontpage(titleBlock, titleMoreDetails, urlSite, titleInfoSection, iconCalendar, descriptionText,
                titleInfoMoreDetails, urlSite2);
    }

    @Test
    public void testDeleteWhatYouNeedToKnowBlock() throws Exception {
        deleteNotDefaultBlocks();
        // Add a new 'What you need to know' block
        String titleBlock = addWhatYouNeedToKnowBlock(); // get title of the block

        // Delete the block with a proper button
        openEditPage();
        WebElement block = getLastBlock(nameWhatYouNeedToKnowBlock);
        deleteBlock(block);
        // Submit changes for frontpage
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        changeFrontpageSettingsPage.clickButtonSubmit();

        // Check that the block is not displayed on frontpage
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");
        checkWhatYouNeedToKnowBlockIsNotShownOnFrontpage(titleBlock);
    }



// Order of blocks

    @Test
    public void testChangeOrderOfBlocksOnFrontpage() throws Exception {
        deleteNotDefaultBlocks();
        // Add two text blocks to frontpage
        addTextBlock(nameTextBlock, titleTextBlock, textForTextBlock); // the 1st added block
        addTextBlock(nameTextBlock, titleTextBlock2, textForTextBlock2); // the 2nd added block
        // Check that the first added block is shown first on frontpage
        checkOrderOfTextBlocksOnFrontpage(titleTextBlock, titleTextBlock2);

        // Change an order of the text blocks with 'drag' elements on ChangeFrontpageSettings page
        openEditPage();
        // The first block (source)
        WebElement textBlock1 = webDriver.findElement(By.xpath("//tr[last()-1]//h3[text()='" + nameTextBlock + "']"));
        WebElement dragElement = textBlock1.findElement(By.xpath("./../a"));
        // The second block (target)
        WebElement textBlock2 = getLastBlock(nameTextBlock);
        WebElement target = textBlock2.findElement(By.xpath("./../a"));
        // Perform 'drag-and-drop', check the info message appeared
        new Actions(webDriver).dragAndDrop(dragElement, target).perform();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        waitUntilElementIsVisible(changeFrontpageSettingsPage.getMessageTabledrag());

        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Check that the first added block is shown the second, so the order of blocks has been changed
        checkOrderOfTextBlocksOnFrontpage(titleTextBlock2, titleTextBlock);
    }

    @Test
    public void testChangeOrderOfBlocksInOneContainer() throws Exception {
        deleteNotDefaultBlocks();
        // Add a block 'Two columns'
        openEditPage();
        ChangeFrontpageSettingsPage changeFrontpageSettingsPage = new ChangeFrontpageSettingsPage(webDriver, pageLocation);
        addBlock(changeFrontpageSettingsPage.getButtonAddTwoColumns(), nameTwoColumns);
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Add two text blocks to frontpage
        addTextBlock(nameTextBlock, titleTextBlock, textForTextBlock); // the 1st text block
        addTextBlock(nameTextBlock, titleTextBlock2, textForTextBlock2); // the 2nd text block

        // Check that the 1st text block is shown in the 1st column, the 2nd one - in the second column
        checkTextBlocksTwoColumnsAreDisplayedOnFrontpage(titleTextBlock, textForTextBlock, titleTextBlock2, textForTextBlock2);

        // Change order of the text blocks in 'Two column' container
        openEditPage();
        // The first block (source)
        WebElement textBlock1 = webDriver.findElement(By.xpath("//tr[last()-1]//h3[text()='" + nameTextBlock + "']"));
        WebElement dragElement = textBlock1.findElement(By.xpath("./../a"));
        // The second block (target)
        WebElement textBlock2 = getLastBlock(nameTextBlock);
        WebElement target = textBlock2.findElement(By.xpath("./../a"));
        // Perform 'drag-and-drop', check the info message appeared
        new Actions(webDriver).dragAndDrop(dragElement, target).perform();
        waitUntilElementIsVisible(changeFrontpageSettingsPage.getMessageTabledrag());
        // Submit changes for frontpage
        changeFrontpageSettingsPage.clickButtonSubmit();
        assertTextOfMessage(homePage.getMessage(), ".*The frontpage has been adapted.");

        // Check that the 1st text block is shown in the 2nd column, the 2nd one - in the 1st column; so order has been changed
        checkTextBlocksTwoColumnsAreDisplayedOnFrontpage(titleTextBlock2, textForTextBlock2, titleTextBlock, textForTextBlock);
    }

    @Test // PILOT-1545 (Description for MeetExpertsBlock can't be added) !!!!!!!!!!
    public void testClassManagerIsAbleToEditFrontpage() throws Exception {
        // Add an authenticated user
        addAuthenticatedUser(expert);
        // Add a user with 'class manager' site role
        addUserWithSiteRole(classManager, roleClassManager);

        // Delete not default blocks
        deleteNotDefaultBlocks();
        logout(admin);

        // Log in as the class manager, check that the Edit link is displayed on frontpage
        login(classManager);
        List<WebElement> linkEdit = webDriver.findElements(By.linkText("Edit"));
        Assert.assertTrue("The 'Edit' link is not displayed on the frontpage.", linkEdit.size() == 1);
        // Check if default blocks are shown for the user
        openEditPage();
        checkDefaultBlocksAreShown();
        openHomePage();


        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // Check that the user is able to add a block, e.g. 'Meet experts' block
        addMeetExpertsBlock(nameMeetExpertsBlock, titleMeetExpertBlock, expert, descriptionMeetExperts);
    }

    @Test
    public void testAuthenticatedUserIsNotAbleToEditFrontpage() throws Exception {
        // Add an authenticated user
        addAuthenticatedUser(student);
        logout(admin);

        // Login as the user, check that there is no Edit link on frontpage
        login(student);
        List<WebElement> linkEdit = webDriver.findElements(By.linkText("Edit"));
        Assert.assertTrue("The 'Edit' link is not displayed on the frontpage.", linkEdit.size() == 0);

        // Redirect to ChangeFrontpageSettings page and check that access is denied
        webDriver.get(websiteUrl.concat("/admin/config/user-interface/frontpage"));
        waitForPageToLoad();
        String xpathMessage = "//p[text()='You are not authorized to access this page.']";
        Assert.assertTrue("The message is not displayed.", webDriver.findElement(By.xpath(xpathMessage)).isDisplayed());
    }






}
