package com.inceptum.peasToolTest;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TinCanBaseTest;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.BlocksPage;
import com.inceptum.pages.CreateImagePage;
import com.inceptum.pages.CreatePeerAssignmentPage;
import com.inceptum.pages.CreatePresentationPage;
import com.inceptum.pages.CreateVideoPage;
import com.inceptum.pages.EditClassPage;
import com.inceptum.pages.EditImagePage;
import com.inceptum.pages.EditPeerAssignmentPage;
import com.inceptum.pages.EditPresentationPage;
import com.inceptum.pages.EditVideoPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.OAuthProvidersPage;
import com.inceptum.pages.OverviewAssignmentsPage;
import com.inceptum.pages.SelectImagePage;
import com.inceptum.pages.SelectPresentationPage;
import com.inceptum.pages.SelectVideoPage;
import com.inceptum.pages.ViewPeerAssignmentPage;
import com.inceptum.util.PropertyLoader;
import com.inceptum.vo.BrowserVo;
import com.inceptum.webdriver.WebDriverFactory;

/**
 * Created by Olga on 11.12.2014.
 */
public class PeasBaseTest extends TinCanBaseTest {

    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final String docxFilePath = "src/test/resources/files/hello.docx";
    protected final String pdfFilePath = "src/test/resources/files/htmlText.pdf";
    protected final String pptxFilePath = "src/test/resources/files/title.pptx";
    protected static final List<String> admin = Arrays.asList("admin", "admin");
    protected final List<String> teacher = Arrays.asList("teacher", "teacher");
    protected final List<String> contentEditor = Arrays.asList("contentEditor", "contentEditor");
    protected final List<String> user1 = Arrays.asList("user1", "user1");
    protected final List<String> user2 = Arrays.asList("user2", "user2");
    protected final List<String> user3 = Arrays.asList("user3", "user3");
    protected final String assignmentCase2 = "AssignmentCase2.";
    protected final String assignmentCase3 = "AssignmentCase3.";
    protected final String nameEvaluationCriteria = "EvaluationCriteria1";
    protected final String nameEvaluationCriteria2 = "EvaluationCriteria2";
    protected final String descriptionEvaluationCriteria = "DescriptionForEvaluationCriteria1";
    protected final String descriptionEvaluationCriteria2 = "DescriptionForEvaluationCriteria2";
    protected final String step1 = "1";
    protected final String commentForUserStep2 = ".*Your review assignments will be published after the draft submission deadline.*";
    protected final String commentDraft = ".*This file can no longer be edited.*";
    protected final String commentReview = ".*Comments can no longer be submitted.*";
    protected final String commentFinal = ".*This file can no longer be edited.*";
    protected final String commentUsefulness = ".*The author did not yet mark the usefulness of this comment.*";
    protected final String commentIsUseful = ".*This comment was marked useful by the author.*";
    protected final String commentFeedback = "MyComment.";
    protected final String commentFeedbackUpdated = "MyCommentNew.";
    protected final String presentationTitle = "Presentation1";
    protected final String presentationTitleNew = "NewPresentation";
    protected final String urlSlideshare = "http://www.slideshare.net/tutorialsruby/csstutorialboxmodel";
    protected final String materialCSS = "div.title";
    protected final String videoTitle = "Video1";
    protected final String videoTitleNew = "NewVideo";
    protected final String urlYouTube = "http://www.youtube.com/watch?v=Wz2klMXDqF4";
    protected final String imageTitle = "Image1";
    protected final String imagePath = "src/test/resources/files/image1.jpeg";
    protected final String imageTitleNew = "NewImage";
    protected final String titleUploadedFile = "File1";
    protected final String titleUploadedFile2 = "File2";
    protected final String titleLink = "Link1";
    protected final String titleLink2 = "Link2";
    protected final String urlLink = "https://www.google.com/";
    protected final String urlLink2 = "https://www.gmail.com/";
    protected final String draft = "draft";
    protected final String review = "review";
    protected final String finalD ="final";
    protected final String yes ="yes";
    protected final String no ="no";



    /* ----- Tests ----- */

    @BeforeClass
    public static void beforeClass() throws Exception {
        // Set websiteUrl, webDriver, pageLocation
        websiteUrl = System.getProperty("site.url");
        if (websiteUrl == null || websiteUrl.isEmpty()) {
            throw new RuntimeException("Please specify site.url value in maven command line (e.g. -Dsite.url=http://staging5.inceptum.eu)");
        }
        gridHubUrl = PropertyLoader.loadProperty("grid2.hub");
        browser = new BrowserVo(PropertyLoader.loadProperty("browser.name"),
                PropertyLoader.loadProperty("browser.version"),
                PropertyLoader.loadProperty("browser.platform"));

        String username = PropertyLoader.loadProperty("user.username");
        String password = PropertyLoader.loadProperty("user.password");

        webDriver = WebDriverFactory.getInstance(gridHubUrl, browser, username, password, currentTest);
        webDriver.manage().window().maximize();

        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);

        // Login as admin
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();

        // Enable 'iMindsX Google Service Account OAuth' module
        webDriver.get(websiteUrl.concat("/admin/modules"));
        enableModuleImindsXGoogleServiceAccountQAuth();
        enableModulesPeerAssignmentCoreAndNavigation();

        // Add a provider if it doesn't exist yet
        webDriver.get(websiteUrl.concat("/admin/config/services/oauth_provider"));
        OAuthProvidersPage oAuthProvidersPage = new OAuthProvidersPage(webDriver, pageLocation);
        waitUntilElementIsClickable(oAuthProvidersPage.getLinkAddProvider());
        List<WebElement> providers = webDriver.findElements(By.xpath("//tr/td[text()='" + accountName + "']"));
        if (providers.size() == 0) {
            addProvider();
        }

        // Add 'Peer Assignment' button on the Frontpage
        webDriver.get(websiteUrl.concat("/admin/structure/block"));
        BlocksPage blocksPage = new BlocksPage(webDriver, pageLocation);
        waitUntilElementIsVisible(blocksPage.getTableBlocks());
        WebElement option = webDriver.findElement(By.xpath("//td[text()='PEAS navigation link']/../td[2]/div/select/option[text()='Header right']"));
        option.click();
        blocksPage.clickButtonSaveBlocks();
        assertTextOfMessage(blocksPage.getMessage(), ".*The block settings have been updated.");
        openHomePage();
        Assert.assertTrue("The button 'Peer assignment' is not displayed on Home page",
                homePage.getButtonPeerAssignments().isDisplayed());

        // Quit webDriver
        quitWebDriver();
    }



    @Before
    public void beforeTest() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage.openThisPage(webDriver, pageLocation);
        // Login as admin and clear Recent log messages
        loginAs(admin);
        clearLogMessages();
        webDriver.get(websiteUrl);
        waitForPageToLoad();
    }

    @After
    public void afterTest() throws Exception {
        // Open Home page and check admin is logged in
        HomePage homePage = openHomePage();
        String xpathLinkUserAccount = "//nav[@id='block-system-user-menu']/ul/li/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLinkUserAccount)));
        WebElement linkUserAccount = webDriver.findElement(By.xpath(xpathLinkUserAccount));
        if (!(linkUserAccount.getText().equals(admin.get(0)))) {
            if (linkUserAccount.getText().equals("Sign in")) { // 'Sign in' button instead of user link is displayed
                login(admin);
            } else { // log out if user link doesn't match with 'admin' (another user has been logged in)
                linkUserAccount.click();
                homePage.clickLinkSignOut();
                login(admin);
            }
        }
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        waitUntilPageIsOpenedWithTitle("Recent log messages");
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        int i=0;
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available
                break;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {  // Find php messages starting with "Notice:" or "Warning:"
                    i++;
                    String defaultWindow = webDriver.getWindowHandle();
                    String selectAll = Keys.chord(Keys.SHIFT, Keys.RETURN); // press the keys simultaneously
                    message.sendKeys(selectAll);                            // to open the link in a new window
                    // Wait until two windows are opened
                    waitCondition(new ExpectedCondition<Boolean>() {
                        public Boolean apply(WebDriver webDriver) {
                            Set<String> handles = webDriver.getWindowHandles();
                            int windows = handles.size();
                            return windows == 2;
                        }
                    });
                    ArrayList<String> newTab = new ArrayList<String>(webDriver.getWindowHandles());
                    newTab.remove(defaultWindow);
                    // Change focus on the new tab
                    webDriver.switchTo().window(newTab.get(0));
                    wait.until(ExpectedConditions.titleContains("Details")); // "Details" page is opened
                    String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
                    wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpathOfMessage)));
                    waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
                    String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
                    System.out.println(fullMessage);
                    webDriver.close();
                    // Change focus on the default tab
                    webDriver.switchTo().window(defaultWindow);
                }
            }
        }
        if (i>0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }

    public void createPeerAssignment(boolean GDrive, String templatePath) throws Exception {
        // Open 'Create peer assignment' page from the Class page
        webDriver.get(websiteUrl);
        waitForPageToLoad();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        CreatePeerAssignmentPage createPeerAssignmentPage = overviewAssignmentsPage.clickButtonAddPeerAssignment();
        // Check that proper Class is in 'Your groups' field
        createPeerAssignmentPage.checkProperClassInGroupAudience(className);

        // Enter data into "General settings" page
        createPeerAssignmentPage.fillFieldTitle(titlePA);
        createPeerAssignmentPage.fillFieldSummary(summaryPA);
        // Select 'Coaching' option in the Chapter field
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldChapter(), coaching);
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldLengthHours(), "3");
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldLengthMinutes(), "45");
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldNumberOfReviews(), "1");

        // Select Deadlines
        createPeerAssignmentPage.switchToTabDeadlines();
        // Draft submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldDraftSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldDraftSubmissionDeadlineTime(), 1);
        // Review submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldReviewSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldReviewSubmissionDeadlineTime(), 1);
        // Final submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldFinalSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldFinalSubmissionDeadlineTime(), 1);

        // Fill in 'What do you need to do' iframe in Assignment context
        createPeerAssignmentPage.switchToTabAssignmentContext();
        fillFrame(createPeerAssignmentPage.getIframeWhatDoYouNeedToDo(), whatDoYouNeedToDoText);

        // Select Template on "Assignment resources"
        createPeerAssignmentPage.switchToTabTaskResources();
        if (GDrive) { // condition, that checks if we'll use storage account or not
            createPeerAssignmentPage.selectStorageAccount(accountName);
        }
        createPeerAssignmentPage.fillFieldTitleTemplateSolution(titleTemplateSolution);

        createPeerAssignmentPage.selectTemplate(templatePath);

        // Enter data into 'Assignment cases'
        createPeerAssignmentPage.switchToTabAssignmentCases();
        fillFrame(createPeerAssignmentPage.getIframeAssignmentCase(), assignmentCase1);

        ViewPeerAssignmentPage viewPeerAssignmentPage = createPeerAssignmentPage.clickButtonSaveTop();

        // Check that Peer Assignment is created
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been created.");
        Assert.assertTrue("Edit link is not shown on the page.", viewPeerAssignmentPage.getLinkEdit().isDisplayed());
    }
    public void openPeerAssignmentWithNode(String node) throws Exception {
        // 'Cookie_notification' message can be shown on the page, it blocks clicking Peer Assignment link (Chrome)
        closeCookiesPopup();
        String peerAssignmentXpath = "//div[text()='" + titlePA + "'][@data-quickedit-field-id='node/" + node + "/title/und/teaser']/../..";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(peerAssignmentXpath)));
        WebElement peerAssignment = webDriver.findElement(By.xpath(peerAssignmentXpath));
        scrollToElement(peerAssignment);
        peerAssignment.click();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewPeerAssignmentPage.getTabStep1());
        //waitUntilElementIsVisible(viewPeerAssignmentPage.getButtonStartYourAssignment()); // the button is not used in v3.4.0
    }
    public void checkSubmissionIsAdded(String user, String titlePA) throws Exception {
        webDriver.get(websiteUrl.concat("/admin/content"));
        waitForPageToLoad();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains("Content"));
        String xpathSubmission = "//a[text()='" + user + "']/../../td[2]/a[text()='Submission (" + titlePA + ")']";
        Assert.assertTrue("Submission is not added.", webDriver.findElement(By.xpath(xpathSubmission)).isDisplayed());
    }
    public void submitDraftVersion(List<String> user, String nodePA) throws Exception { // with GDrive
        // Log in as user (student) and open added Peer Assignment
        loginAs(user);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);



        openPeerAssignmentWithNode(nodePA); // PILOT-1529 !!!!!!!




        // Submit a draft version
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);

        //viewPeerAssignmentPage.clickButtonStartYourAssignment(); // the button is not used in v3.4.0

        viewPeerAssignmentPage.checkActiveTab(step1);
        viewPeerAssignmentPage.checkAssignmentCase(assignmentCase1);
        viewPeerAssignmentPage.clickButtonFillOutTheSolutionTemplate();

        // Switch to google window, close tab
        String defaultWindow = webDriver.getWindowHandle();
        switchFromFirstPageToSecond(defaultWindow);
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.titleContains("DRAFT-" + user.get(0))); // Google doc is opened
        webDriver.close();
        // Change focus on the default tab
        webDriver.switchTo().window(defaultWindow);

        // Check comment on Step2
        viewPeerAssignmentPage.switchToStep2();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForUserStep2);
    }
    public void createFinalSubmission(List<String> user) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.clickButtonCreateFinalSubmission();
        // Switch to google window, close tab
        String defaultWindow = webDriver.getWindowHandle();
        switchFromFirstPageToSecond(defaultWindow);
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.titleContains("FINAL-" + user.get(0))); // Google doc is opened
        webDriver.close();
        // Change focus on the default tab
        webDriver.switchTo().window(defaultWindow);
    }
    public void viewPeerDraft(List<String> user) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.clickLinkViewSubmission();
        // Switch to google window, close tab
        String defaultWindow = webDriver.getWindowHandle();
        switchFromFirstPageToSecond(defaultWindow);
        WebDriverWait wait = new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.titleContains("DRAFT-" + user.get(0))); // Google doc is opened
        webDriver.close();
        // Change focus on the default tab
        webDriver.switchTo().window(defaultWindow);
    }
    public void commentPeerDraft() throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.fillFieldComments(commentFeedback);
        viewPeerAssignmentPage.submitComment();
    }
    public String getNodePA() throws Exception {
        // openEditPage();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        openEditPage();

        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentPageUrl = webDriver.getCurrentUrl();
        String[] parts = editPeerAssignmentPageUrl.split("/node/"); // Separate url into parts
        parts = parts[1].split("/edit");
        String node = parts[0]; // Peer Assignment node
        return node;
    }
    public String getAssignmentCase() throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        String caseText = viewPeerAssignmentPage.getCaseText();
        return caseText;
    }
    public void viewSubmission(String username) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.clickButtonViewSubmission();
        // Switch to google window, close tab
        String defaultWindow = webDriver.getWindowHandle();
        switchFromFirstPageToSecond(defaultWindow);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains("DRAFT-" + username)); // Google doc is opened
        webDriver.close();
        // Change focus on the default tab
        webDriver.switchTo().window(defaultWindow);
    }
    public void viewFinalSolution(List<String> username) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.clickButtonEditYourSolution();
        // Switch to google window, close tab
        String defaultWindow = webDriver.getWindowHandle();
        switchFromFirstPageToSecond(defaultWindow);
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains("FINAL-" + username.get(0))); // Google doc is opened
        webDriver.close();
        // Change focus on the default tab
        webDriver.switchTo().window(defaultWindow);
    }
    public void checkClassIsAdded(String className) throws Exception {
        // Open PeerAssignment overview page
        webDriver.get(websiteUrl);
        waitForPageToLoad();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        homePage.clickButtonPeerAssignments();
        List<WebElement> options = webDriver.findElements((By.tagName("option")));
        // Check if the Class has already been added
        for (WebElement option : options) {
            if (option.getText().equals(className)) {
                option.click();
                System.out.println("The class " + className + " has already been added.");
                // Make the Class 'Open for registration', change Capacity
                openEditPage();
                EditClassPage editClassPage = new EditClassPage(webDriver, pageLocation);
                editClassPage.selectOptionOpenForRegistration();
                editClassPage.clickButtonSave();
                OverviewAssignmentsPage overviewAssignmentsPage = new OverviewAssignmentsPage(webDriver, pageLocation);
                assertTextOfMessage(overviewAssignmentsPage.getMessage(), ".*Class " + className + " has been updated.");
                return;
            }
        }
        // Add Class if no class with such name has been found
        addClass(className);
    }
    public void addRoleForUser(List<String> user, String role, String className) throws Exception {
        OverviewAssignmentsPage overviewAssignmentsPage = new OverviewAssignmentsPage(webDriver, pageLocation);
        overviewAssignmentsPage.clickLinkGroup();
        waitUntilPageIsOpenedWithTitle(className);
        checkUserIsSubscribedWithRole(role, className, user);
    }

    public void addEvaluationCriteria(String name, String description) throws Exception { // start from EditPA page
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabEvaluationCriteria();
        editPeerAssignmentPage.addEvaluationCriteria(name, description);
        editPeerAssignmentPage.clickButtonSave();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
    }
    public String reviewPeersWork(String nodePA) throws Exception {
        // Add three users' accounts
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);
        logout(admin);

        // Each user submit his draft version
        submitDraftVersion(user1, nodePA); // PILOT-1529 !!!!!
        logout(user1);
        submitDraftVersion(user2, nodePA);
        logout(user2);
        submitDraftVersion(user3, nodePA);
        logout(user3);

        // Log in as admin and check that Submissions are added
        loginAs(admin);
        checkSubmissionIsAdded(user1.get(0), titlePA);
        checkSubmissionIsAdded(user2.get(0), titlePA);
        checkSubmissionIsAdded(user3.get(0), titlePA);

        // Change draft submission deadline, run Cron
        String editPeerAssignmentPageUrl = websiteUrl.concat("/node/" + nodePA + "/edit?destination=peas/overview/nojs/" + nodePA);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(draft);
        logout(admin);

        // Check that the draft version can no longer be edited
        loginAs(user1);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(nodePA);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        Assert.assertTrue(viewPeerAssignmentPage.getFieldDraftComment().getText().matches(commentDraft));

        // Review peer's work
        viewPeerAssignmentPage.switchToStep2();
        String username = viewPeerAssignmentPage.getUsernameForReviewing(); // this method return name of user which work is reviewing
        viewSubmission(username);
        viewPeerAssignmentPage.checkEvaluationCriteria(nameEvaluationCriteria, descriptionEvaluationCriteria);
        viewPeerAssignmentPage.fillFieldComments(commentFeedback);
        viewPeerAssignmentPage.submitComment();
        // Check that user is able to edit his comment
        viewPeerAssignmentPage.editComment(commentFeedbackUpdated);

        // Check message on Step3
        viewPeerAssignmentPage.switchToStep3();
        viewPeerAssignmentPage.checkCommentsOnStep3();
        return username;
    }
    public void changeSubmissionDeadlineDate(String deadline) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabDeadlines();
        WebElement field = null;
        if (deadline.equals("draft")) {
            field = editPeerAssignmentPage.getFieldDraftSubmissionDeadlineDate();
        }
        if (deadline.equals("review")) {
            field = editPeerAssignmentPage.getFieldReviewSubmissionDeadlineDate();
        }
        if (deadline.equals("final")) {
            field = editPeerAssignmentPage.getFieldFinalSubmissionDeadlineDate();
        }
        selectDate(field, -1); // get yesterday's day
        editPeerAssignmentPage.clickButtonSaveTop();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        waitForPageToLoad();
        Thread.sleep(1000); // additional time for generating correct list of statements
    }
    public void checkCommentOfPeer(List<String> userWhoCommented) throws Exception {
        String xpathCommentSubmittedBy = "//*[@class='comment-label']/span/div/div/div[text()='" + userWhoCommented.get(0) + "']";
        WebDriverWait wait= new WebDriverWait(webDriver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathCommentSubmittedBy)));
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        Assert.assertTrue("Feedback comment is wrong.", viewPeerAssignmentPage.getFieldPeersComment().getText().equals(commentFeedbackUpdated));
    }
    public void checkUsefulnessOfComment(String nodePA) throws Exception {
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(nodePA);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep2();
        Assert.assertTrue("Usefulness of the comment is shown incorrectly.",
                viewPeerAssignmentPage.getFieldUsefulnessOfComment().getText().matches(commentIsUseful));
    }
    public void checkFinalSubmission(String nodePA) throws Exception {
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(nodePA);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep3();
        Assert.assertTrue(viewPeerAssignmentPage.getFieldFinalComment().getText().matches(commentFinal));
        Assert.assertTrue(viewPeerAssignmentPage.getButtonViewSubmission().isDisplayed());
    }
    public void addMainContent(String title, String url) throws Exception {
        // Add main content to 'Assignment context' tab
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabAssignmentContext();
        editPeerAssignmentPage.fillFieldTitleMainContent(title);
        SelectVideoPage selectVideoPage = editPeerAssignmentPage.clickLinkSelectMedia();
        selectVideoPage.selectVideo(url);
        Assert.assertTrue(editPeerAssignmentPage.getLinkRemoveMedia().isDisplayed());
    }
    public void addAnotherItemMainContent(String title, String url, String description) throws Exception {
        // Add another item to main content ('Assignment context' tab)
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonAddAnotherItemMainContent();
        editPeerAssignmentPage.fillFieldTitleMainContent2(title);
        SelectVideoPage selectVideoPage = editPeerAssignmentPage.clickLinkSelectMedia2();
        selectVideoPage.selectVideo(url);
        Assert.assertTrue(editPeerAssignmentPage.getLinkRemoveMedia2().isDisplayed());
        fillFrame(editPeerAssignmentPage.getFrameDescriptionAnotherItemMainContent(), description);
    }
    public void checkAnotherItemMainContent(String title, String player) throws Exception {
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__item field-item even'][text()='"
                + title + "']")).isDisplayed()); // title of main content is displayed
        Assert.assertTrue(webDriver.findElement(By.cssSelector("iframe.media-" + player + "-player")).isDisplayed()); // media player is displayed
    }
    public void removeMediaFromAnotherItemMainContent() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabAssignmentContext();
        editPeerAssignmentPage.clickLinkRemoveMedia2();
    }
    public void checkMediaIsRemoved(String title, String description) throws Exception {
        // Check that media is displayed only for main content (not another item).
        int iframes = webDriver.findElements(By.tagName("iframe")).size();
        Assert.assertTrue("Quantity of frames on the View peer assignment page is not equal 1.", iframes==1);
        // Title of another item main content is displayed
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__item field-item even'][text()='"
                + title + "']")).isDisplayed());
        // Description of another item main content is displayed
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__item field-item even']/p[text()='"
                + description + "']")).isDisplayed());
    }
    public void removeAnotherItemMainContent() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabAssignmentContext();
        editPeerAssignmentPage.clickButtonRemoveAnotherItemMainContent();
    }
    public void checkMediaContentIsRemoved(String title) throws Exception {
        // Check that mediaPlayer for the main content is displayed
        int iframes = webDriver.findElements(By.tagName("iframe")).size();
        Assert.assertTrue("Quantity of frames on the View peer assignment page is not equal 1.", iframes==1);
        // Check that the title of another item has been also removed
        int titleField = webDriver.findElements(By.xpath("//*[@class='field__item field-item even'][text()='"
                + title + "']")).size();
        Assert.assertTrue("Title " + title + " of another item is still shown on View peer assignment page." ,titleField==0);
    }
    public void addPresentationToTaskResources() throws Exception {
        // Add presentation using icon 'add'
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        CreatePresentationPage createPresentationPage = editPeerAssignmentPage.clickIconAddPresentation();
        createPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        // Check that the Presentation is created
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editPeerAssignmentPage.getFieldPresentationsTaskResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation, titleOfCreatedPresentation.matches(presentationTitle + ".*"));
    }
    public void addPresentationToAdditionalResources() throws Exception {
        // Add presentation using icon 'add'
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        CreatePresentationPage createPresentationPage = editPeerAssignmentPage.clickIconAddPresentationAdditional();
        createPresentationPage.fillFieldTitle(presentationTitle);
        SelectPresentationPage selectPresentationPage = createPresentationPage.clickLinkSelectMedia();
        createPresentationPage = selectPresentationPage.selectSlidesharePresentation(urlSlideshare);
        Assert.assertTrue(createPresentationPage.getLinkRemoveMedia().isDisplayed());
        createPresentationPage.clickButtonSave();
        // Check that the Presentation is created
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Presentation .+ has been created.");
        // Check that title of created presentation is in the PRESENTATIONS field
        String titleOfCreatedPresentation = editPeerAssignmentPage.getFieldPresentationsAdditionalResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in PRESENTATIONS field: " + titleOfCreatedPresentation, titleOfCreatedPresentation.matches(presentationTitle + ".*"));
    }
    public void addVideoToTaskResources(String url) throws Exception {
        // Add video using icon 'add'
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        CreateVideoPage createVideoPage = editPeerAssignmentPage.clickIconAddVideo();
        createVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(url);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        // Check that the Video is created
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEOS field
        String titleOfCreatedVideo = editPeerAssignmentPage.getFieldVideosTaskResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEOS field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(videoTitle + ".*"));
    }
    public void addVideoToAdditionalResources(String url) throws Exception {
        // Add video using icon 'add'
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        CreateVideoPage createVideoPage = editPeerAssignmentPage.clickIconAddVideoAdditional();
        createVideoPage.fillFieldTitle(videoTitle);
        SelectVideoPage selectVideoPage = createVideoPage.clickLinkSelectMedia();
        createVideoPage = selectVideoPage.selectVideo(url);
        Assert.assertTrue(createVideoPage.getLinkRemoveMedia().isDisplayed());
        createVideoPage.clickButtonSave();
        // Check that the Video is created
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Video .+ has been created.");
        // Check that title of created video is in the VIDEOS field
        String titleOfCreatedVideo = editPeerAssignmentPage.getFieldVideosAdditionalResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in VIDEOS field: " + titleOfCreatedVideo, titleOfCreatedVideo.matches(videoTitle + ".*"));
    }
    public void addImageToTaskResources() throws Exception {
        // Add image using icon 'add'
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        CreateImagePage createImagePage = editPeerAssignmentPage.clickIconAddImage();
        createImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        createImagePage = selectImagePage.selectImage(imagePath);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        // Check that the Image is created
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editPeerAssignmentPage.getFieldImagesTaskResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(imageTitle + ".*"));
    }
    public void addImageToAdditionalResources() throws Exception {
        // Add image using icon 'add'
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        CreateImagePage createImagePage = editPeerAssignmentPage.clickIconAddImageAdditional();
        createImagePage.fillFieldTitle(imageTitle);
        SelectImagePage selectImagePage = createImagePage.clickLinkSelectMedia();
        createImagePage = selectImagePage.selectImage(imagePath);
        Assert.assertTrue(createImagePage.getLinkRemoveMedia().isDisplayed());
        createImagePage.clickButtonSave();
        // Check that the Image is created
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Image .+ has been created.");
        // Check that title of created image is in the IMAGES field
        String titleOfCreatedImage = editPeerAssignmentPage.getFieldImagesAdditionalResources().getAttribute("value");
        Assert.assertTrue("Detected wrong data in IMAGES field: " + titleOfCreatedImage, titleOfCreatedImage.matches(imageTitle + ".*"));
    }
    public void editPresentationFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        EditPresentationPage editPresentationPage = editPeerAssignmentPage.clickIconEditPresentation();
        editPresentationPage.getFieldTitle().clear();
        editPresentationPage.fillFieldTitle(presentationTitleNew);
        editPresentationPage.clickButtonSave();
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Presentation .+ has been updated.");
    }
    public void editVideoFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        EditVideoPage editVideoPage = editPeerAssignmentPage.clickIconEditVideo();
        editVideoPage.getFieldTitle().clear();
        editVideoPage.fillFieldTitle(videoTitleNew);
        editVideoPage.clickButtonSave();
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Video .+ has been updated.");
    }
    public void editImageFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        EditImagePage editImagePage = editPeerAssignmentPage.clickIconEditImage();
        editImagePage.getFieldTitle().clear();
        editImagePage.fillFieldTitle(imageTitleNew);
        editImagePage.clickButtonSave();
        assertTextOfMessage(editPeerAssignmentPage.getMessage(), ".*Image .+ has been updated.");
    }
    public void removePresentationFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldPresentationsTaskResources().clear();
        editPeerAssignmentPage.clickButtonSave();
    }
    public void removePresentationFromTaskResources2() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldPresentationsTaskResources2().clear();
    }
    public void removeVideoFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldVideosTaskResources().clear();
        editPeerAssignmentPage.clickButtonSave();
    }
    public void removeVideoFromTaskResources2() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldVideosTaskResources2().clear();
    }
    public void removeImageFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldImagesTaskResources().clear();
        editPeerAssignmentPage.clickButtonSave();
    }
    public void removeImageFromTaskResources2() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldImagesTaskResources2().clear();
    }
    public void uploadFileToDownloads(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldDownloadsFile().sendKeys(path);
        editPeerAssignmentPage.clickButtonUploadFileToDownloads();
        waitUntilElementIsVisible(editPeerAssignmentPage.getButtonRemoveFileFromDownloads());
    }
    public void uploadFileToDownloads2(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldDownloadsFile2().sendKeys(path);
        editPeerAssignmentPage.clickButtonUploadFileToDownloads2();
        waitUntilElementIsVisible(editPeerAssignmentPage.getButtonRemoveFieldsetDownloads2());
    }
    public void uploadFileToDownloadsAdditional(String filePath) throws Exception {
        String path = new File(filePath).getAbsolutePath();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.getFieldDownloadsFileAdditional().sendKeys(path);
        editPeerAssignmentPage.clickButtonUploadFileToDownloadsAdditional();
        waitUntilElementIsVisible(editPeerAssignmentPage.getButtonRemoveFileFromDownloadsAdditional());
    }
    public void addFileToDownloads(String title, String filePath) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.fillFieldTitleDownloads(title);
        uploadFileToDownloads(filePath);
    }
    public void addFileToDownloadsAdditional(String title, String filePath) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.fillFieldTitleDownloadsAdditional(title);
        uploadFileToDownloadsAdditional(filePath);
    }
    public void addFileToDownloads2(String title, String filePath) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.fillFieldTitleDownloads2(title);
        uploadFileToDownloads2(filePath);
    }
    public void removeFileFromDownloads() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonRemoveFieldsetDownloads();
    }
    public void removeFileFromDownloads2() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonRemoveFieldsetDownloads2();
    }
    public void addLinkToTaskResources(String title, String url) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.fillFieldTitleLink(title);
        editPeerAssignmentPage.fillFieldUrlLink(url);
    }
    public void addLinkToTaskResources2(String title, String url) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.fillFieldTitleLink2(title);
        editPeerAssignmentPage.fillFieldUrlLink2(url);
    }
    public void addLinkToAdditionalResources(String title, String url) throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.fillFieldTitleLinkAdditional(title);
        editPeerAssignmentPage.fillFieldUrlLinkAdditional(url);
    }
    public void removeLinkFromTaskResources() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clearFieldTitleLink();
        editPeerAssignmentPage.clearFieldUrlLink();
    }
    public void removeLinkFromTaskResources2() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clearFieldTitleLink2();
        editPeerAssignmentPage.clearFieldUrlLink2();
    }
    public void addPresentationAnotherItem() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonAddAnotherItemPresentation();
        autoSelectItemFromList(editPeerAssignmentPage.getFieldPresentationsTaskResources2(), presentationTitle);
    }
    public void addVideoAnotherItem() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonAddAnotherItemVideo();
        autoSelectItemFromList(editPeerAssignmentPage.getFieldVideosTaskResources2(), videoTitle);
    }
    public void addImageAnotherItem() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonAddAnotherItemImage();
        autoSelectItemFromList(editPeerAssignmentPage.getFieldImagesTaskResources2(), imageTitle);
    }
    public void addFileToDownloadsAnotherItem() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonAddAnotherItemFileDownloads();
        addFileToDownloads2(titleUploadedFile2, docxFilePath);
    }
    public void addLinkAnotherItem() throws Exception {
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonAddAnotherItemLink();
        addLinkToTaskResources2(titleLink2, urlLink2);
    }
    public void checkPresentations(int quantity) throws Exception {
        String css = ".node--presentation .title";
        Assert.assertTrue(webDriver.findElements(By.cssSelector(css)).size()==quantity);
    }
    public void checkVideos(int quantity) throws Exception {
        String css = ".node--video .title";
        Assert.assertTrue(webDriver.findElements(By.cssSelector(css)).size()==quantity);
    }
    public void checkImages(int quantity) throws Exception {
        String css = ".node--image .title";
        Assert.assertTrue(webDriver.findElements(By.cssSelector(css)).size()==quantity);
    }
    public void checkFilesUploaded(int quantity) throws Exception {
        String css = ".field--name-field-download-title-course .field-item";
        Assert.assertTrue(webDriver.findElements(By.cssSelector(css)).size()==quantity);
    }
    public void checkLinks(int quantity) throws Exception {
        String css = ".field--name-field-links-course .field__item a";
        Assert.assertTrue(webDriver.findElements(By.cssSelector(css)).size()==quantity);
    }
    public String getAssignmentCaseForUser(List<String> user, String nodePA) throws Exception {
        loginAs(user);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(nodePA);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String assignmentCaseUser = getAssignmentCase();
        logout(user);
        return assignmentCaseUser;
    }
    public void assertListsAreEqual(List<String> list1, List<String> list2) throws Exception {
        Set<String> set1 = new HashSet<String>();
        set1.addAll(list1);
        Set<String> set2 = new HashSet<String>();
        set2.addAll(list2);
        Assert.assertTrue("Assignment cases are displayed incorrectly.", set1.equals(set2));
    }
    public void scorePeerComment(String score) throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        if (score.equals(yes)) {
            viewPeerAssignmentPage.clickCommentUsefulYes();
        }
        if (score.equals(no)) {
            viewPeerAssignmentPage.clickCommentUsefulNo();
        }
        Thread.sleep(1000); // to make sure the tincan statement is received
    }










}
