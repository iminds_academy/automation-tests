package com.inceptum.peasToolTest;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.By;

import com.inceptum.pages.ContentPage;
import com.inceptum.pages.CreatePeerAssignmentPage;
import com.inceptum.pages.DeleteItemConfirmationPage;
import com.inceptum.pages.EditPeerAssignmentPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.OverviewAssignmentsPage;
import com.inceptum.pages.ViewPeerAssignmentPage;

/**
 * Created by Olga on 11.12.2014.
 */
public class PeasTest extends PeasBaseTest {

    /*---------CONSTANTS--------*/

    protected final String commentForStaffMemberStep1 = ".*As a staff member you are not able to submit a draft version of this assignment.*";
    protected final String commentForStaffMemberStep2 = ".*As a staff member you have not been assigned any peer reviews.*";
    protected final String commentForStaffMemberStep3 = ".*As a staff member you are not able to submit a final version of this assignment.*";
    protected final String titleMainContent = "TitleMainContent";
    protected final String titleMainContent2 = "TitleMainContent2";
    protected final String descriptionAnotherItemMainContent = "Description";
    protected final String urlYouTube = "http://www.youtube.com/watch?v=Wz2klMXDqF4";
    protected final String urlVimeo = "http://vimeo.com/43714499";
    protected final String levelName = "LevelName1";
    protected final String levelName2 = "LevelName2";
    protected final String rationaleText = "RationaleText";
    protected final String rationaleText2 = "RationaleText2";
    protected final String remediationText = "RemediationText";
    protected final String remediationText2 = "RemediationText2";


    /* ----- Tests ----- */

    // Peer assignment = PA

    @Test
    public void testCreatePeerAssignmentUsingProperButton() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);
        // Create PA
        createPeerAssignment(GDrive = true, xlsxFilePath); // use storage account in this test
    }

    @Test
    public void testCreatePeerAssignmentFromClassOverview() throws Exception {
        // Check Class is added on Class overview page
        checkClassIsAddedOnOverview(className);
        // Create PA from the page
        createPeerAssignmentFromClassOverview(GDrive = true, pptxFilePath); // use storage account in this test
    }

    @Test // PILOT-1528 (comments have been changed) !!!!!!!!!!
    public void testStartPeerAssignmentAsAdmin() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, xlsxFilePath);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        // Start assignment and check comments

        //viewPeerAssignmentPage.clickButtonStartYourAssignment(); // the button is not used in v3.4.0

        viewPeerAssignmentPage.checkActiveTab(step1);
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep1);
        viewPeerAssignmentPage.switchToStep2();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep2);
        viewPeerAssignmentPage.switchToStep3();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep3);
    }

    @Test // !!!! PILOT-1007 (teacher can't add PA)
    public void testStartPeerAssignmentAsTeacher() throws Exception {
        // Add user with 'teacher member' role to the Class
        checkUserIsCreated(teacher.get(0));
        checkClassIsAdded(className);
        OverviewAssignmentsPage overviewAssignmentsPage = new OverviewAssignmentsPage(webDriver, pageLocation);
        overviewAssignmentsPage.selectClass(className);
        addRoleForUser(teacher, "teacher member", className);

        // Log in as teacher
        logout(admin);
        loginAs(teacher);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, xlsxFilePath);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        // Start assignment and check comments

        //viewPeerAssignmentPage.clickButtonStartYourAssignment(); // the button is not used in v3.4.0

        viewPeerAssignmentPage.checkActiveTab(step1);
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep1);
        viewPeerAssignmentPage.switchToStep2();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep2);
        viewPeerAssignmentPage.switchToStep3();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep3);
    }

    @Test // !!!! PILOT-1007 (content editor can't add PA)
    public void testStartPeerAssignmentAsContentEditor() throws Exception {
        // Add user with 'editor member' role to the Class
        checkUserIsCreated(contentEditor.get(0));
        checkClassIsAdded(className);
        OverviewAssignmentsPage overviewAssignmentsPage = new OverviewAssignmentsPage(webDriver, pageLocation);
        overviewAssignmentsPage.selectClass(className);
        addRoleForUser(contentEditor, "content editor", className);

        // Log in as content editor
        logout(admin);
        loginAs(contentEditor);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, xlsxFilePath);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        // Start assignment and check comments

        //viewPeerAssignmentPage.clickButtonStartYourAssignment(); // the button is not used in v3.4.0

        viewPeerAssignmentPage.checkActiveTab(step1);
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep1);
        viewPeerAssignmentPage.switchToStep2();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep2);
        viewPeerAssignmentPage.switchToStep3();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForStaffMemberStep3);
    }

    @Test
    public void testSubmitDraftVersionUsingGDriveXLSX() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, xlsxFilePath);
        // Get the Peer Assignment node
        String node = getNodePA();

        // Add user account
        addAuthenticatedUser(user1);
        logout(admin);

        // Log in as the user (student) and submit his draft version
        submitDraftVersion(user1, node);

        // Log in as admin and check that Submission is added
        logout(user1);
        loginAs(admin);
        checkSubmissionIsAdded(user1.get(0), titlePA);
    }

    @Test
    public void testSubmitDraftVersionUsingGDrivePPTX() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment, check the User is added
        createPeerAssignment(GDrive = true, pptxFilePath);
        // Get the Peer Assignment node
        String node = getNodePA();

        // Add user account
        addAuthenticatedUser(user1);
        logout(admin);

        // Log in as user (student) and submit his draft version
        submitDraftVersion(user1, node);

        // Log in as admin and check that Submission is added
        logout(user1);
        loginAs(admin);
        checkSubmissionIsAdded(user1.get(0), titlePA);
    }

    @Ignore // PILOT-1170 ?
    @Test
    public void testSubmitDraftVersionUsingFileUploading() throws Exception { // test should be divided: 1) check storage account is selected PILOT-1049;
                                                                              //                         2) testSubmitDraftVersionUsingFileUploading for unsupported formats by GDrive (future versions)
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment, check the User is added
        createPeerAssignment(GDrive = false, docxFilePath); // don't select the storage account

        // !!!!!! after clicking Save button an error message should be shown PILOT-1049 !!!!!!!
        // !!!!!! check what file formats are not supported by google drive -
        // - only for these files student will be able to upload his draft manually (using Upload button) !!!!!!!!
        // !!!!!! all current formats: pdf doc docx ppt pptx xls xlsx - are supported, so all files will be opened with google drive for student

        // Get the Peer Assignment node
        String node = getNodePA();

        // Add user account
        addAuthenticatedUser(user1);
        logout(admin);

        // Log in as the user (student) and open added Peer Assignment
        loginAs(user1);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(node);

        // Submit a draft version
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);

        //viewPeerAssignmentPage.clickButtonStartYourAssignment(); this button is not used in v3.4.0

        viewPeerAssignmentPage.checkActiveTab(step1);
        viewPeerAssignmentPage.checkAssignmentCase(assignmentCase1);
        // Download Template; upload your solution
        viewPeerAssignmentPage.downloadTemplate(titleTemplateSolution);
        waitUntilFileDownloaded();
        viewPeerAssignmentPage.uploadYourDraft(docxFilePath);
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Draft submission uploaded");

        // Check comment on Step2
        viewPeerAssignmentPage.switchToStep2();
        assertTextOfMessage(viewPeerAssignmentPage.getComment(), commentForUserStep2);

        // Log in as admin and check that Submission is added
        logout(user1);
        loginAs(admin);
        checkSubmissionIsAdded(user1.get(0), titlePA);
    }

    @Test
    public void testReviewPeersWorkDOCX() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account
        // Get the Peer Assignment node, add 'Evaluation criteria'
        String node = getNodePA();
        addEvaluationCriteria(nameEvaluationCriteria, descriptionEvaluationCriteria);

        // Review peer's work
        reviewPeersWork(node);
    }

    @Test // PILOT-1529 !!!!!!!!
    public void testImproveOwnWorkGdrivePDF() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, pdfFilePath); // select the storage account
        // Get the Peer Assignment node, add 'Evaluation criteria'
        String node = getNodePA();
        addEvaluationCriteria(nameEvaluationCriteria, descriptionEvaluationCriteria);

        // Review peer's work
        String userWhichWorkIsReviewed = reviewPeersWork(node); // !!!!!!!!!!!!!!!!!!
        List<String> user = Arrays.asList(userWhichWorkIsReviewed, userWhichWorkIsReviewed); // the user credentials

        logout(user1);
        loginAs(admin);

        // Change review submission deadline, run Cron
        String editPeerAssignmentPageUrl = websiteUrl.concat("/node/" + node + "/edit?destination=peas/overview/nojs/" + node);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(review);
        logout(admin);

        // Login as user, which work is already reviewed; check that comments can no longer be submitted (Step2)
        loginAs(user);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(node);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep2();
        Assert.assertTrue(viewPeerAssignmentPage.getFieldReviewComment().getText().matches(commentReview));
        Assert.assertTrue(viewPeerAssignmentPage.getFieldUsefulnessOfComment().getText().matches(commentUsefulness));

        // Switch to Step3
        viewPeerAssignmentPage.switchToStep3();
        checkCommentOfPeer(user1); // check comment which other user have left
        viewPeerAssignmentPage.selectUsefulYes(); // assert: comment is useful
        viewFinalSolution(user);

        // Change final submission deadline and run Cron
        logout(user);
        loginAs(admin);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(finalD);
        logout(admin);

        // Check usefulness of the comment
        loginAs(user1);
        checkUsefulnessOfComment(node);

        // Check final submission
        logout(user1);
        loginAs(user);
        checkFinalSubmission(node);
    }

    @Test  // PILOT-1529 !!!!!!!!
    public void testImproveOwnWorkGdrivePPTX() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, pptxFilePath); // select the storage account
        // Get the Peer Assignment node, add 'Evaluation criteria'
        String node = getNodePA();
        addEvaluationCriteria(nameEvaluationCriteria, descriptionEvaluationCriteria);

        // Review peer's work
        String userWhichWorkIsReviewed = reviewPeersWork(node); // !!!!!!!!
        List<String> user = Arrays.asList(userWhichWorkIsReviewed, userWhichWorkIsReviewed); // the user credentials

        logout(user1);
        loginAs(admin);

        // Change review submission deadline, run Cron
        String editPeerAssignmentPageUrl = websiteUrl.concat("/node/" + node + "/edit?destination=peas/overview/nojs/" + node);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(review);
        logout(admin);

        // Login as user, which work is already reviewed; check that comments can no longer be submitted (Step2)
        loginAs(user);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(node);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep2();
        Assert.assertTrue(viewPeerAssignmentPage.getFieldReviewComment().getText().matches(commentReview));
        Assert.assertTrue(viewPeerAssignmentPage.getFieldUsefulnessOfComment().getText().matches(commentUsefulness));

        // Switch to Step3
        viewPeerAssignmentPage.switchToStep3();
        checkCommentOfPeer(user1); // check comment which other user have left
        viewPeerAssignmentPage.selectUsefulYes(); // assert: comment is useful
        viewFinalSolution(user);

        // Change final submission deadline and run Cron
        logout(user);
        loginAs(admin);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(finalD);
        logout(admin);

        // Check usefulness of the comment
        loginAs(user1);
        checkUsefulnessOfComment(node);

        // Check final submission
        logout(user1);
        loginAs(user);
        checkFinalSubmission(node);
    }

    @Ignore
    @Test
    public void testImproveOwnWorkFileUploading() throws Exception { // change this test for uploading files with unsupported formats by GDrive (future version)
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = false, docxFilePath); // select the storage account
        // Get the Peer Assignment node, add 'Evaluation criteria'
        String node = getNodePA();
        addEvaluationCriteria(nameEvaluationCriteria, descriptionEvaluationCriteria);

        // Review peer's work
        String userWhichWorkIsReviewed = reviewPeersWork(node);
        List<String> user = Arrays.asList(userWhichWorkIsReviewed, userWhichWorkIsReviewed); // the user credentials

        logout(user1);
        loginAs(admin);

        // Change review submission deadline, run Cron
        String editPeerAssignmentPageUrl = websiteUrl.concat("/node/" + node + "/edit?destination=peas/overview/nojs/" + node);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(review);
        logout(admin);

        // Login as user, which work is already reviewed; check that comments can no longer be submitted (Step2)
        loginAs(user);
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        openPeerAssignmentWithNode(node);
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep2();
        Assert.assertTrue(viewPeerAssignmentPage.getFieldReviewComment().getText().matches(commentReview));
        Assert.assertTrue(viewPeerAssignmentPage.getFieldUsefulnessOfComment().getText().matches(commentUsefulness));

        // Switch to Step3
        viewPeerAssignmentPage.switchToStep3();
        checkCommentOfPeer(user1); // check comment which other user have left
        viewPeerAssignmentPage.selectUsefulNo(); // assert: comment is not useful
        viewFinalSolution(user);

        // Change final submission deadline and run Cron
        logout(user);
        loginAs(admin);
        webDriver.get(editPeerAssignmentPageUrl);
        changeSubmissionDeadlineDate(finalD);
        logout(admin);

        // Check usefulness of the comment
        loginAs(user1);
        checkUsefulnessOfComment(node);

        // Check final submission
        logout(user1);
        loginAs(user);
        checkFinalSubmission(node);
    }

    @Test
    public void testAddYoutubeVideoToAssignmentMainContent() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account
        // Add main content to 'Assignment context' tab
        ContentPage contentPage = openContentPage();
        contentPage.clickIconEditFor1stElement();
        addMainContent(titleMainContent, urlYouTube);

        // Save changes in the Peer Assignment
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(contentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        // Check that the video is added to the PA
        contentPage.clickLinkTitleOf1stElement();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewPeerAssignmentPage.getLinkEdit());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__item field-item even'][text()='"
                + titleMainContent + "']")).isDisplayed()); // title of main content is displayed
        Assert.assertTrue(webDriver.findElement(By.cssSelector("iframe.media-youtube-player")).isDisplayed()); // media player is shown
    }

    @Test
    public void testAddVimeoVideoToAssignmentMainContent() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, xlsxFilePath);
        // Add main content to 'Assignment context' tab
        ContentPage contentPage = openContentPage();
        contentPage.clickIconEditFor1stElement();
        addMainContent(titleMainContent, urlVimeo);

        // Save changes in the Peer Assignment
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(contentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        // Check that the video is added to the PA
        contentPage.clickLinkTitleOf1stElement();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(viewPeerAssignmentPage.getLinkEdit());
        Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__item field-item even'][text()='"
                + titleMainContent + "']")).isDisplayed()); // title of main content is displayed
        Assert.assertTrue(webDriver.findElement(By.cssSelector("iframe.media-vimeo-player")).isDisplayed()); // media player is shown
    }

    @Test
    public void testAddAndRemoveAnotherItemInAssignmentMainContent() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account
        // Add main content to 'Assignment context' tab
        openEditPage();
        addMainContent(titleMainContent, urlYouTube);
        // Add another item to main content
        addAnotherItemMainContent(titleMainContent2, urlVimeo, descriptionAnotherItemMainContent);
        // Save changes in the Peer Assignment
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        waitUntilElementIsVisible(viewPeerAssignmentPage.getLinkEdit());

        // The 1st video is displayed
        checkAnotherItemMainContent(titleMainContent, "youtube");
        // The 2nd video is displayed
        checkAnotherItemMainContent(titleMainContent2, "vimeo");

        // Remove media from another item
        openEditPage();
        removeMediaFromAnotherItemMainContent();
        editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        // Check that only one mediaFrame is shown on the page (not another item frame)
        checkMediaIsRemoved(titleMainContent2, descriptionAnotherItemMainContent);

        // Remove another item from main content
        openEditPage();
        removeAnotherItemMainContent();
        editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        // Check that the 2nd media content is removed
        checkMediaContentIsRemoved(titleMainContent2);
    }

    @Test
    public void testAddEditRemovePresentationInAssignmentResources() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add Presentation to Assignment resources
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabTaskResources();
        addPresentationToTaskResources();
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment .+ has been updated.");
        // Added Presentation to the Peer Assignment is displayed on a View page
        Assert.assertEquals(presentationTitle, viewPeerAssignmentPage.getFieldPresentationResource().getText());

        // Edit the Presentation
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        editPresentationFromTaskResources();
        viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        // The updated Presentation is displayed on a View page (with new title)
        Assert.assertEquals(presentationTitleNew, viewPeerAssignmentPage.getFieldPresentationResource().getText());

        // Remove the Presentation
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        removePresentationFromTaskResources();
        // Check that the presentation is removed (presentation title is absent on ViewPeerAssignment page)
        checkElementIsAbsent(materialCSS);
    }

    @Test
    public void testAddEditRemoveVideoInAssignmentResources() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add Video to Assignment resources
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabTaskResources();
        addVideoToTaskResources(urlYouTube);
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment .+ has been updated.");
        // Added Video to the Peer Assignment is displayed on a View page
        Assert.assertEquals(videoTitle, viewPeerAssignmentPage.getFieldVideoResource().getText());

        // Edit the Video
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        editVideoFromTaskResources();
        viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        // The updated Video is displayed on a View page (with new title)
        Assert.assertEquals(videoTitleNew, viewPeerAssignmentPage.getFieldVideoResource().getText());

        // Remove the Video
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        removeVideoFromTaskResources();
        // Check that the video is removed (video title is absent on ViewPeerAssignment page)
        checkElementIsAbsent(materialCSS);
    }

    @Test
    public void testAddEditRemoveImageInAssignmentResources() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add Image to Assignment resources
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabTaskResources();
        addImageToTaskResources();
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment .+ has been updated.");
        // Added Image to the Peer Assignment is displayed on a View page
        Assert.assertEquals(imageTitle, viewPeerAssignmentPage.getFieldImageResource().getText());

        // Edit the Image
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        editImageFromTaskResources();
        viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        // The updated Image is displayed on a View page (with new title)
        Assert.assertEquals(imageTitleNew, viewPeerAssignmentPage.getFieldImageResource().getText());

        // Remove the Image
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        removeImageFromTaskResources();
        // Check that the image is removed (image title is absent on ViewPeerAssignment page)
        checkElementIsAbsent(materialCSS);
    }

    @Test
    public void testAddRemoveFileInAssignmentResources() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add file (Downloads) to Assignment resources
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabTaskResources();
        addFileToDownloads(titleUploadedFile, xlsxFilePath);
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment .+ has been updated.");
        // Uploaded file is displayed on a View page
        Assert.assertEquals(titleUploadedFile, viewPeerAssignmentPage.getFieldUploadedFileResource().getText());

        // Remove the file
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        removeFileFromDownloads();
        editPeerAssignmentPage.clickButtonSave();
        // Check that the file is removed (file title is absent on ViewPeerAssignment page)
        String fileFieldCss = ".field--name-field-download-title-course .field-item";
        int uploadedFiles = webDriver.findElements(By.cssSelector(fileFieldCss)).size();
        Assert.assertTrue("Uploaded file is not deleted from Peer Assignment (Additional material...).", uploadedFiles==0);
    }

    @Test
    public void testAddRemoveLinkInAssignmentResources() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add link to Assignment resources
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabTaskResources();
        addLinkToTaskResources(titleLink, urlLink);
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment .+ has been updated.");
        // Link is displayed on a View page and it's correct
        Assert.assertEquals(titleLink, viewPeerAssignmentPage.getFieldLinkResource().getText());
        String url = viewPeerAssignmentPage.getFieldLinkResource().getAttribute("href");
        Assert.assertTrue("The link is incorrect.", url.equals(urlLink));

        // Remove the link
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        removeLinkFromTaskResources();
        editPeerAssignmentPage.clickButtonSave();
        // Check that the link is removed
        String linkFieldCss = ".field--name-field-links-course .field__item a";
        int links = webDriver.findElements(By.cssSelector(linkFieldCss)).size();
        Assert.assertTrue("The link is not deleted from Peer Assignment (Additional material...).", links == 0);
    }

    @Test
    public void testAddRemoveAnotherItemInAssignmentResources() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add each type of content to Assignment resources
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabTaskResources();
        addPresentationToTaskResources(); // presentation
        addVideoToTaskResources(urlYouTube); // video
        addImageToTaskResources(); // image
        addFileToDownloads(titleUploadedFile, xlsxFilePath); // uploaded file
        addLinkToTaskResources(titleLink, urlLink); // link

        // Add another item to each type of content
        addPresentationAnotherItem(); // presentation2
        addVideoAnotherItem(); // video2
        addImageAnotherItem(); // image2
        addFileToDownloadsAnotherItem(); // uploaded file2
        addLinkAnotherItem(); // link2
        // Save changes
        editPeerAssignmentPage.clickButtonSave();

        // Check that there are two links for each content type on ViewPeerAssignment page
        checkPresentations(2);
        checkVideos(2);
        checkImages(2);
        checkFilesUploaded(2);
        checkLinks(2);


        // Remove the 2nd items from 'Task resources'
        openEditPage();
        editPeerAssignmentPage.switchToTabTaskResources();
        removePresentationFromTaskResources2(); // presentation2
        removeVideoFromTaskResources2(); // video2
        removeImageFromTaskResources2(); // image2
        removeFileFromDownloads2(); // uploaded file2
        removeLinkFromTaskResources2(); // link2
        // Save changes
        editPeerAssignmentPage.clickButtonSave();

        // Check that there is one link for each content type on ViewPeerAssignment page
        checkPresentations(1);
        checkVideos(1);
        checkImages(1);
        checkFilesUploaded(1);
        checkLinks(1);
    }

    @Test
    public void testAddRemoveEvaluationCriteria() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Add the 1st evaluation criteria
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabEvaluationCriteria();
        editPeerAssignmentPage.addEvaluationCriteria(nameEvaluationCriteria, descriptionEvaluationCriteria);
        // Add two levels with additional information (rationale, remediation)
        editPeerAssignmentPage.fillFieldLevelName(levelName); // the 1st level
        fillFrame(editPeerAssignmentPage.getFrameRationale(), rationaleText);
        fillFrame(editPeerAssignmentPage.getFrameSuggestedRemediation(), remediationText);
        editPeerAssignmentPage.clickButtonAddAnotherItemLevel();
        editPeerAssignmentPage.fillFieldLevelName2(levelName2); // the 2nd level
        fillFrame(editPeerAssignmentPage.getFrameRationale2(), rationaleText2);
        fillFrame(editPeerAssignmentPage.getFrameSuggestedRemediation2(), remediationText2);

        // Add the 2nd evaluation criteria (only name and description)
        editPeerAssignmentPage.clickButtonAddAnotherItemCriteria();
        editPeerAssignmentPage.addEvaluationCriteria2(nameEvaluationCriteria2, descriptionEvaluationCriteria2);

        // Save changes and open Step1 on ViewPeerAssignment page
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSave();
        viewPeerAssignmentPage.switchToStep1();

        // Check that all criteria elements are added (displayed) to Peer assignment
        viewPeerAssignmentPage.clickLinkEvaluationCiteria();
        List<String> elements = Arrays.asList(nameEvaluationCriteria, descriptionEvaluationCriteria, levelName, levelName2, nameEvaluationCriteria2, descriptionEvaluationCriteria2);
        List<String> elements2 = Arrays.asList(rationaleText, remediationText, rationaleText2, remediationText2);
        for (String element : elements) {
            Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__items']/div[text()='" + element + "']")).isDisplayed());
        }
        for (String element : elements2) {
            Assert.assertTrue(webDriver.findElement(By.xpath("//*[@class='field__items']/div/p[text()='" + element + "']")).isDisplayed());
        }

        // Remove Level2 from Criteria1, remove Criteria2
        viewPeerAssignmentPage.clickLinkClosePopup();
        openEditPage();
        editPeerAssignmentPage.switchToTabEvaluationCriteria();
        editPeerAssignmentPage.clickButtonRemoveLevel2();
        editPeerAssignmentPage.clickButtonRemoveCriteria2();
        // Save the changes
        editPeerAssignmentPage.clickButtonSave();

        // Check that deleted elements are absent on criteria popup
        viewPeerAssignmentPage.clickLinkEvaluationCiteria();
        List<String> elementsUpdated = Arrays.asList(levelName2, nameEvaluationCriteria2, descriptionEvaluationCriteria2);
        List<String> elementsUpdated2 = Arrays.asList(rationaleText2, remediationText2);
        for (String element : elementsUpdated) {
            Assert.assertTrue(webDriver.findElements(By.xpath("//*[@class='field__items']/div[text()='" + element + "']")).size()==0);
        }
        for (String element : elementsUpdated2) {
            Assert.assertTrue(webDriver.findElements(By.xpath("//*[@class='field__items']/div/p[text()='" + element + "']")).size()==0);
        }
    }

    @Test // PILOT-1529 !!!!!
    public void testAddAnotherItemAssignmentCase() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment and get its node
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account
        String node = getNodePA();

        // Enter three cases for the Peer assignment (the 1st case was added with PA creation)
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        editPeerAssignmentPage.switchToTabAssignmentCases();
        fillFrame(editPeerAssignmentPage.getIframeAssignmentCase2(), assignmentCase2);
        editPeerAssignmentPage.clickButtonAddAnotherButtonCase();
        fillFrame(editPeerAssignmentPage.getIframeAssignmentCase3(), assignmentCase3);
        // Save changes
        editPeerAssignmentPage.clickButtonSave();

        // Check that three user' accounts are already created
        checkUserIsCreated(user1.get(0));
        checkUserIsCreated(user2.get(0));
        checkUserIsCreated(user3.get(0));
        logout(admin);

        // Get assignment cases for three users
        String assignmentCaseUser1 = getAssignmentCaseForUser(user1, node);
        String assignmentCaseUser2 = getAssignmentCaseForUser(user2, node);
        String assignmentCaseUser3 = getAssignmentCaseForUser(user3, node);

        // Check that three different cases are used for the users
        List<String> list1 = Arrays.asList(assignmentCase1, assignmentCase2, assignmentCase3);
        List<String> list2 = Arrays.asList(assignmentCaseUser1, assignmentCaseUser2, assignmentCaseUser3);
        assertListsAreEqual(list1, list2); // by its meaning, not the order
    }

    @Test
    public void testDeletePeerAssignmentFromEditPage() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Delete Peer assignment from Edit page
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        DeleteItemConfirmationPage deleteItemConfirmationPage = editPeerAssignmentPage.clickButtonDeleteTop();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + titlePA + ".*");
        HomePage homePage = new HomePage(webDriver, pageLocation);
        deleteItemConfirmationPage.clickButtonDelete();
        // Check that Peer assignment is deleted
        assertMessage(homePage.getMessage(), "messages messages--status", ".*Peer Assignment .+ has been deleted.");
    }

    @Test
    public void testDeletePeerAssignmentFromContentTable() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Create Peer Assignment
        createPeerAssignment(GDrive = true, docxFilePath); // select the storage account

        // Delete Peer assignment from Content page
        ContentPage contentPage = openContentPage();
        DeleteItemConfirmationPage deleteItemConfirmationPage = contentPage.clickIconDeleteFor1stElement();
        assertTextOfMessage(deleteItemConfirmationPage.getMessage(), ".*Are you sure you want to delete " + titlePA + ".*");
        deleteItemConfirmationPage.clickButtonDelete();
        // Check that Peer assignment is deleted
        assertMessage(contentPage.getMessage(), "messages status", ".*Peer Assignment .+ has been deleted.");
        // Check that Content page is opened
        assertElementActiveStatus(contentPage.getLinkContent());
    }

    @Test
    public void testCreatePeerAssignmentWithoutSelectedStorageAccount() throws Exception {
        // Check Class is added on PA overview page
        checkClassIsAdded(className);

        // Open 'Create peer assignment' page from the Class page
        webDriver.get(websiteUrl);
        waitForPageToLoad();
        HomePage homePage = new HomePage(webDriver, pageLocation);
        OverviewAssignmentsPage overviewAssignmentsPage = homePage.clickButtonPeerAssignments();
        overviewAssignmentsPage.selectClass(className);
        CreatePeerAssignmentPage createPeerAssignmentPage = overviewAssignmentsPage.clickButtonAddPeerAssignment();
        // Check that proper Class is in 'Your groups' field
        createPeerAssignmentPage.checkProperClassInGroupAudience(className);

        // Enter data into "General settings" page
        createPeerAssignmentPage.fillFieldTitle(titlePA);
        createPeerAssignmentPage.fillFieldSummary(summaryPA);
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldChapter(), coaching);
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldLengthHours(), "3");
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldLengthMinutes(), "45");
        selectItemFromListByOptionName(createPeerAssignmentPage.getFieldNumberOfReviews(), "1");

        // Select Deadlines
        createPeerAssignmentPage.switchToTabDeadlines();
        // Draft submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldDraftSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldDraftSubmissionDeadlineTime(), 1);
        // Review submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldReviewSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldReviewSubmissionDeadlineTime(), 1);
        // Final submission deadline
        selectCurrentDate(createPeerAssignmentPage.getFieldFinalSubmissionDeadlineDate());
        createPeerAssignmentPage.fillFieldDeadlineTimePlus(
                createPeerAssignmentPage.getFieldFinalSubmissionDeadlineTime(), 1);

        // Enter data into 'Assignment cases'
        createPeerAssignmentPage.switchToTabAssignmentCases();
        fillFrame(createPeerAssignmentPage.getIframeAssignmentCase(), assignmentCase1);
        createPeerAssignmentPage.clickButtonSaveTop();

        // Check error message
        assertTextOfMessage(createPeerAssignmentPage.getErrorMessage(), ".*The desired storage account should be chosen.");

        // Select the storage account and submit the form, check PA is saved
        createPeerAssignmentPage.switchToTabTaskResources();
        createPeerAssignmentPage.selectStorageAccount(accountName);
        createPeerAssignmentPage.fillFieldTitleTemplateSolution(titleTemplateSolution);
        createPeerAssignmentPage.selectTemplate(pdfFilePath);
        ViewPeerAssignmentPage viewPeerAssignmentPage = createPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been created."); // created
        Assert.assertTrue("Edit link is not shown on the page.", viewPeerAssignmentPage.getLinkEdit().isDisplayed());
    }





}
