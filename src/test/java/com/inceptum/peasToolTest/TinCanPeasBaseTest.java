package com.inceptum.peasToolTest;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.GroupTabClassPage;
import com.inceptum.pages.ModulesPage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.ViewPeerAssignmentPage;

/**
 * Created by Olga on 25.05.2015.
 */
public class TinCanPeasBaseTest extends PeasTest {


    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final String peerTaskType = "http://orw.iminds.be/tincan/content/type/peertask";
    protected final String criteriaType = "http://orw.iminds.be/tincan/content/type/peerTaskCriteria";
    protected final String submitDraftVersionType = "http://orw.iminds.be/tincan/content/type/peertaskDraft";
    protected final String viewPeerReviewPageType = "http://orw.iminds.be/tincan/content/type/peertaskReview";
    protected final String viewFinalPageType = "http://orw.iminds.be/tincan/content/type/peertaskFinal";
    protected final String viewPeerDraftType = "http://orw.iminds.be/tincan/content/type/peerSubmission";


     /* ----- METHODS ----- */

    public void checkTinCanConfiguration() throws Exception {
        checkTinCanApiPeasModuleIsEnabled();
        checkTinCanApiPage();
    }
    public void checkTinCanApiPeasModuleIsEnabled() throws Exception {
        // Open Modules page
        webDriver.get(websiteUrl.concat("/admin/modules"));
        waitForPageToLoad();
        ModulesPage modulesPage = new ModulesPage(webDriver, pageLocation);
        // If switch has OFF state switch it ON
        waitUntilElementIsVisible(modulesPage.getSwitchTinCanApiPeas());
        if (modulesPage.getSwitchTinCanApiPeas().getAttribute("class").contains("off")) {
            modulesPage.getSwitchTinCanApiPeas().click();
            final WebElement checkbox = modulesPage.getSwitchTinCanApiPeas();
            waitCondition(new ExpectedCondition<Boolean>() {
                public Boolean apply(WebDriver webDriver) {
                    return checkbox.findElement(By.xpath("./../..")).getAttribute("class").contains("enabling");
                }
            });
            // Submit changes
            modulesPage.clickButtonSaveConfiguration();
            assertTextOfMessage(modulesPage.getMessage(), ".*The configuration options have been saved.");
        }
    }
    public void checkViewPeerTaskStatement(String statement, String actorName, String actorMbox, String peerTaskNodeUrl,
                                           String peerTaskName, String classNodeUrl, String chapterID,
                                           String chapterName) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(statement, peerTaskNodeUrl, peerTaskName, peerTaskType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'extensions'
        String classNode = classNodeUrl.split("/node/")[1];
        String peerTaskNodeId = peerTaskNodeUrl.split("/node/")[1];
        String chapterOrder = getChapterOrder(classNode, titlePA, peerTaskNodeId); // Redirecting to Overview page
        checkExtensionsValues(statement, chapterID, chapterName, chapterOrder);
    }
    public void checkViewSubmitDraftVersionStatement(String statement, String actorName, String actorMbox,
                                                     String viewSubmitDraftVersionUrl, String peerTaskName,
                                                     String classNodeUrl, String chapterID, String chapterName,
                                                     String nodeId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String submitDraftVersionName = peerTaskName.concat(" - Draft");
        checkObjectViewValues(statement, viewSubmitDraftVersionUrl, submitDraftVersionName, submitDraftVersionType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        // Key 'extensions'
        String classNode = classNodeUrl.split("/node/")[1];
        String chapterOrder = getChapterOrder(classNode, titlePA, nodeId); // Redirecting to Overview page
        checkExtensionsValues(statement, chapterID, chapterName, chapterOrder);
    }
    public void checkViewEvaluationCriteriaStatement(String statement, String actorName, String actorMbox, String criteriaId,
                                           String criteriaName, String classNodeUrl, String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        checkObjectViewValues(statement, criteriaId, criteriaName, criteriaType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkFailToUploadDraftStatement(String statement, String actorName, String actorMbox, String draftId,
                                                     String draftName, String classNodeUrl) throws Exception { // + , String parentId
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdFails, verbDisplayFails);
        // Key 'object'
        checkObjectViewValues(statement, draftId, draftName, submitDraftVersionType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
//        checkParentValues(statement, parentId, objectType); !!!!!!!!!!!!!!!!!!!!!!!!!
    }
    public void checkProgressToUploadDraftStatement(String statement, String actorName, String actorMbox, String draftId,
                                                String draftName, String classNodeUrl, String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdPassed, verbDisplayProgressed);
        // Key 'object'
        checkObjectViewValues(statement, draftId, draftName, submitDraftVersionType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkViewPeerReviewPageStatement(String statement, String actorName, String actorMbox, String reviewId,
                                                    String reviewName, String authorMbox, String classNodeUrl,
                                                    String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdViewed, verbDisplayViewed);

        // !!!!!! reviewId !!!!!!!
        // Key 'object'
        checkObjectViewValues(statement, reviewId, reviewName, viewPeerReviewPageType); // !!!!!!!!!!!!!

        // Check author
        checkAuthorValue(statement, authorMbox);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkViewFinalPageStatement(String statement, String actorName, String actorMbox, String viewFinalPageUrl,
                                                 String peerTaskName, String classNodeUrl,
                                                 String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdViewed, verbDisplayViewed);
        // Key 'object'
        String finalName = peerTaskName.concat(" - Final");
        checkObjectViewValues(statement, viewFinalPageUrl, finalName, viewFinalPageType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkViewOwnFinalStatement(String statement, String actorName, String actorMbox, String finalId,
                                            String finalName, String authorMbox, String classNodeUrl,
                                            String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdDownloaded, verbDisplayDownloaded);
        // Key 'object'
        checkObjectViewValues(statement, finalId, finalName, viewPeerDraftType);
        // Check 'extensions'
        checkAuthorValue(statement, authorMbox);
        checkFinalValue(statement, "1");
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkViewPeerDraftStatement(String statement, String actorName, String actorMbox, String submissionId,
                                                 String submissionName, String authorMbox, String classNodeUrl,
                                                 String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdDownloaded, verbDisplayDownloaded);

        // !!!!! peerSubmissionId !!!!!!!
        // Key 'object'
        checkObjectViewValues(statement, submissionId, submissionName, viewPeerDraftType); // !!!!!!!!!!!!!

        // Check author
        checkAuthorValue(statement, authorMbox);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkCommentPeerDraftStatement(String statement, String actorName, String actorMbox, String submissionId,
                                            String submissionName, String authorMbox, String classNodeUrl,
                                            String parentId) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdReviewed, verbDisplayReviewed);

        // !!!!! peerSubmissionId !!!!!!!
        // Key 'object'
        checkObjectViewValues(statement, submissionId, submissionName, submitDraftVersionType); // !!!!!!!!!!!!!

        // Check author
        checkAuthorValue(statement, authorMbox);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);
        checkParentValues(statement, parentId, objectType);
    }
    public void checkThreeFailsToCommentStatements(List<String> statements, List<String> users, List<String> mboxs,
                                                   String viewPeerReviewPageUrl, String classNodeUrl, String parentId) throws Exception {
        int usersInt = users.size(); // get number of users
        // Check the 1st statement
        String user1 = checkFailsToCommentStatement(statements.get(0),users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
        usersInt = usersInt - 1; // one statement is checked

        // Check the 2nd statement
        String user2 = checkFailsToCommentStatement(statements.get(1),users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as the previous statement.", (!(user2.equals(user1))));
        usersInt = usersInt - 1; // two statements are checked

        // Check the 3d statement
        String user3 = checkFailsToCommentStatement(statements.get(2),users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as one of two previous statements.",
                (!(user3.equals(user1))) && (!(user3.equals(user2))));
        usersInt = usersInt - 1; // three statements are checked
        // Check that all users have been included in the statements as actors
        Assert.assertTrue("Number of users is not equal 0.", usersInt == 0);
    }
    public String checkFailsToCommentStatement(String statement, List<String> users, List<String> mboxs,
                                             String viewPeerReviewPageUrl, String classNodeUrl, String parentId) throws Exception {
        // Key 'actor'
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'actor' value
        Map<String, Object> actor = (Map)post.get("actor");

        // Check 'name' value
        String user = (String)actor.get("name");
        Assert.assertTrue("The user is not presented in defined list of users.", users.contains(user));
        // Check 'mbox' value
        String actorMbox = (String)actor.get("mbox");
        String mbox = ""; // get expected mbox value
        if (user.equals(users.get(0))) {
            mbox = mboxs.get(0);
        }
        if (user.equals(users.get(1))) {
            mbox = mboxs.get(1);
        }
        if (user.equals(users.get(2))) {
            mbox = mboxs.get(2);
        }
        Assert.assertTrue("Mbox value '" + actorMbox + "' of key 'actor' is not equal to '" + mbox + "'",
                actorMbox.equals(mbox));
        // Key 'verb'
        checkVerbValues(statement, verbIdFails, verbDisplayFails);
        // Key 'object'
        String reviewName = titlePA + " - Review";
        checkObjectViewValues(statement, viewPeerReviewPageUrl, reviewName, viewPeerReviewPageType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);

        // !!! PILOT-1526 !!!!!
//        checkParentValues(statement, parentId, objectType);

        return user;
    }
    public void checkThreeProgressedToFinalStageStatements(List<String> statements, List<String> users, List<String> mboxs,
                                                           String viewPeerReviewPageUrl, String classNodeUrl, String parentId) throws Exception {
        int usersInt = users.size(); // get number of users
        // Check the 1st statement
        String user1 = checkProgressedToFinalStageStatement(statements.get(0), users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
        usersInt = usersInt - 1; // one statement is checked

        // Check the 2nd statement
        String user2 = checkProgressedToFinalStageStatement(statements.get(1), users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as the previous statement.", (!(user2.equals(user1))));
        usersInt = usersInt - 1; // two statements are checked

        // Check the 3d statement
        String user3 = checkProgressedToFinalStageStatement(statements.get(2), users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as one of two previous statements.",
                (!(user3.equals(user1))) && (!(user3.equals(user2))));
        usersInt = usersInt - 1; // three statements are checked
        // Check that all users have been included in the statements as actors
        Assert.assertTrue("Number of users is not equal 0.", usersInt == 0);
    }
    public String checkProgressedToFinalStageStatement(String statement, List<String> users, List<String> mboxs,
                                               String viewPeerReviewPageUrl, String classNodeUrl, String parentId) throws Exception {
        // Key 'actor'
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'actor' value
        Map<String, Object> actor = (Map)post.get("actor");

        // Check 'name' value
        String user = (String)actor.get("name");
        Assert.assertTrue("The user is not presented in defined list of users.", users.contains(user));
        // Check 'mbox' value
        String actorMbox = (String)actor.get("mbox");
        String mbox = ""; // get expected mbox value
        if (user.equals(users.get(0))) {
            mbox = mboxs.get(0);
        }
        if (user.equals(users.get(1))) {
            mbox = mboxs.get(1);
        }
        if (user.equals(users.get(2))) {
            mbox = mboxs.get(2);
        }
        Assert.assertTrue("Mbox value '" + actorMbox + "' of key 'actor' is not equal to '" + mbox + "'",
                actorMbox.equals(mbox));


        // !!!!!!!!!!!! PILOT-1488 ('passed') !!!!!!!!!!!!

        // Key 'verb'
//        checkVerbValues(statement, verbIdPassed, verbDisplayProgressed);



        // Key 'object'
        String reviewName = titlePA + " - Review";
        checkObjectViewValues(statement, viewPeerReviewPageUrl, reviewName, viewPeerReviewPageType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);


        // !!! PILOT-1526 !!!!!
//        checkParentValues(statement, parentId, objectType);

        return user;
    }
    public void checkThreeFailToSubmitFinalStatements(List<String> statements, List<String> users, List<String> mboxs,
                                                   String viewFinalPageUrl, String classNodeUrl, String parentId) throws Exception {
        int usersInt = users.size(); // get number of users
        // Check the 1st statement
        String user1 = checkFailToSubmitFinalStatement(statements.get(0),users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
        usersInt = usersInt - 1; // one statement is checked

        // Check the 2nd statement
        String user2 = checkFailToSubmitFinalStatement(statements.get(1),users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as the previous statement.", (!(user2.equals(user1))));
        usersInt = usersInt - 1; // two statements are checked

        // Check the 3d statement
        String user3 = checkFailToSubmitFinalStatement(statements.get(2),users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as one of two previous statements.",
                (!(user3.equals(user1))) && (!(user3.equals(user2))));
        usersInt = usersInt - 1; // three statements are checked
        // Check that all users have been included in the statements as actors
        Assert.assertTrue("Number of users is not equal 0.", usersInt == 0);
    }
    public String checkFailToSubmitFinalStatement(String statement, List<String> users, List<String> mboxs, String viewFinalPageUrl,
                                                  String classNodeUrl, String parentId) throws Exception {
        // Key 'actor'
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'actor' value
        Map<String, Object> actor = (Map)post.get("actor");

        // Check 'name' value
        String user = (String)actor.get("name");
        Assert.assertTrue("The user is not presented in defined list of users.", users.contains(user));
        // Check 'mbox' value
        String actorMbox = (String)actor.get("mbox");
        String mbox = ""; // get expected mbox value
        if (user.equals(users.get(0))) {
            mbox = mboxs.get(0);
        }
        if (user.equals(users.get(1))) {
            mbox = mboxs.get(1);
        }
        if (user.equals(users.get(2))) {
            mbox = mboxs.get(2);
        }
        Assert.assertTrue("Mbox value '" + actorMbox + "' of key 'actor' is not equal to '" + mbox + "'",
                actorMbox.equals(mbox));
        // Key 'verb'
        checkVerbValues(statement, verbIdFails, verbDisplayFails);
        // Key 'object'
        String finalName = titlePA + " - Final";
        checkObjectViewValues(statement, viewFinalPageUrl, finalName, viewFinalPageType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);

        // !!! PILOT-1526 !!!!!
//        checkParentValues(statement, parentId, objectType);

        return user;
    }
    public void checkThreeSucceedToSubmitFinalStatements(List<String> statements, List<String> users, List<String> mboxs,
                                                      String viewFinalPageUrl, String classNodeUrl, String parentId) throws Exception {
        int usersInt = users.size(); // get number of users
        // Check the 1st statement
        String user1 = checkSucceedToSubmitFinalStatement(statements.get(0),users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
        usersInt = usersInt - 1; // one statement is checked

        // Check the 2nd statement
        String user2 = checkSucceedToSubmitFinalStatement(statements.get(1),users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as the previous statement.", (!(user2.equals(user1))));
        usersInt = usersInt - 1; // two statements are checked

        // Check the 3d statement
        String user3 = checkSucceedToSubmitFinalStatement(statements.get(2),users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
        Assert.assertTrue("The statement has the same actor as one of two previous statements.",
                (!(user3.equals(user1))) && (!(user3.equals(user2))));
        usersInt = usersInt - 1; // three statements are checked
        // Check that all users have been included in the statements as actors
        Assert.assertTrue("Number of users is not equal 0.", usersInt == 0);
    }
    public String checkSucceedToSubmitFinalStatement(String statement, List<String> users, List<String> mboxs, String viewFinalPageUrl,
                                                  String classNodeUrl, String parentId) throws Exception {
        // Key 'actor'
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(statement);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'actor' value
        Map<String, Object> actor = (Map)post.get("actor");

        // Check 'name' value
        String user = (String)actor.get("name");
        Assert.assertTrue("The user is not presented in defined list of users.", users.contains(user));
        // Check 'mbox' value
        String actorMbox = (String)actor.get("mbox");
        String mbox = ""; // get expected mbox value
        if (user.equals(users.get(0))) {
            mbox = mboxs.get(0);
        }
        if (user.equals(users.get(1))) {
            mbox = mboxs.get(1);
        }
        if (user.equals(users.get(2))) {
            mbox = mboxs.get(2);
        }
        Assert.assertTrue("Mbox value '" + actorMbox + "' of key 'actor' is not equal to '" + mbox + "'",
                actorMbox.equals(mbox));
        // Key 'verb'
        checkVerbValues(statement, verbIdCompleted, verbDisplayCompleted);
        // Key 'object'
        String finalName = titlePA + " - Final";
        checkObjectViewValues(statement, viewFinalPageUrl, finalName, viewFinalPageType);
        // Key 'context'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);

        // !!! PILOT-1526 !!!!!
//        checkParentValues(statement, parentId, objectType);

        return user;
    }
    public void viewEvaluationCriteria() throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.clickLinkEvaluationCiteria();
        Thread.sleep(1000);
        viewPeerAssignmentPage.clickLinkClosePopup();
    }
    public void checkOnlyUserInClassGroup(List<String> user, String viewClassPageUrl) throws Exception {
        // Open Class overview page
        webDriver.get(viewClassPageUrl);
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        GroupTabClassPage groupTabClassPage = overviewPage.clickLinkGroup(); // click link Group
        groupTabClassPage.clickLinkPeople(); // click link People
        // Get list of usernames from a table
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        Iterator<WebElement> iterator = elements.iterator();
        // Remove all users from the group except of current user and admin
        while (iterator.hasNext()) {
            WebElement element = iterator.next();
            if (!(element.getText().contains(user.get(0)) || element.getText().contains(admin.get(0)))) {
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                removeSelectedUseraccountFromClass();
                elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
                iterator = elements.iterator();
            }
        }
    }
    public void checkThreeUsersInClassGroup(List<String> user1, List<String> user2, List<String> user3,
                                            String viewClassPageUrl) throws Exception{
        // Open Class overview page
        webDriver.get(viewClassPageUrl);
        OverviewPage overviewPage = new OverviewPage(webDriver, pageLocation);
        GroupTabClassPage groupTabClassPage = overviewPage.clickLinkGroup(); // click link Group
        groupTabClassPage.clickLinkPeople(); // click link People
        // Get list of usernames from a table
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        Iterator<WebElement> iterator = elements.iterator();
        // Remove all users from the group except of current user and admin
        while (iterator.hasNext()) {
            WebElement element = iterator.next();
            if (!(element.getText().equals(user1.get(0)) || element.getText().equals(user2.get(0)) ||
                    element.getText().equals(user3.get(0)) || element.getText().equals(admin.get(0)))) {
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                removeSelectedUseraccountFromClass();
                elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
                iterator = elements.iterator();
            }
        }
    }
    public void submitDraftVersionsOfUsers(List<String> user1, List<String> user2, List<String> user3,
                                           String viewSubmitDraftVersionUrl, String editPeerAssignmentUrl) throws Exception {
        // Open 'Submit draft version' page
        webDriver.get(viewSubmitDraftVersionUrl);
        // Submit users' draft versions
        masqueradeAsUser(user1); // user1
        createDraftSubmission(user1);
        switchBackFromMasqueradingAs(user1, admin);

        masqueradeAsUser(user2); // user2
        createDraftSubmission(user2);
        switchBackFromMasqueradingAs(user2, admin);

        masqueradeAsUser(user3); // user3
        createDraftSubmission(user3);
        switchBackFromMasqueradingAs(user3, admin);

        // Change draft submission deadline date < actual date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(draft);
    }
    public void checkScorePeerCommentStatement(String statement, String actorName, String actorMbox, String draftSolutionId,
                                               String classNodeUrl, String parentId, String authorMbox, int score) throws Exception {
        // Key 'actor'
        checkActorValues(statement, actorName, actorMbox);
        // Key 'verb'
        checkVerbValues(statement, verbIdRated, verbDisplayRated);
        // Key 'object'
        String draftSubmissionName = titlePA + " - Draft submission of " + actorName;

        // !!!!! draftSolutionId - ??????????????
//        checkObjectViewValues(statement, draftSolutionId, draftSubmissionName, submitDraftVersionType);


        // Key 'extensions'
        checkGroupingViewValues(statement, classNodeUrl, className, objectType);

        // !!!!!!!!!!! PILOT-1526
//        checkParentValues(statement, parentId, objectType);


        checkAuthorValue(statement, authorMbox);
        // Key 'result'
        checkScoreValue(statement, score);
    }
    public List<String> getPeerForReviewInfo(List<String> user, String peerTaskName,
                                             List<String> peer1, List<String> peer2) throws Exception {
        // View review page by the user (admin's review page is opened - Step2)
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        masqueradeAsUser(user);
        String peerToReview = viewPeerAssignmentPage.getFieldPeerToReview().getText(); // get peer's name
        List<String> userCredentials = Arrays.asList(peerToReview, peerToReview);
        String authorMbox = getMboxValue(userCredentials); // get author value
        String reviewName = getReviewName(userCredentials, peerTaskName); // get review name
        String reviewId = viewPeerAssignmentPage.getReviewId(); // get review id                 !!!!!!!!!!!!!!!!! change node !!!!!!!!!!
        // Check that the peer's name equal to one of possible values
        Assert.assertTrue("Wrong peer's username is displayed on ViewPeerReviewPage.", peerToReview.contains(peer1.get(0))
                || peerToReview.contains(peer2.get(0)));
        return Arrays.asList(reviewId, reviewName, authorMbox);
    }
    public String getPeerToReviewName() throws Exception {
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        String peerToReview = viewPeerAssignmentPage.getFieldPeerToReview().getText(); // get peer's name
        return peerToReview;
    }
    public void checkScoreValue(String logMessage, int scaled) throws Exception {
        JSONParser parser = new JSONParser();
        JSONObject obj = (JSONObject)parser.parse(logMessage);
        JSONObject post = (JSONObject)((JSONObject)obj.get("request")).get("post");
        // Get 'result' value
        Map<String, Object> result = (Map)post.get("result");
        // Get 'score' value
        Map<String, Object> score = (Map)result.get("score");
        // Check 'scaled' value
        Object scaledValue = score.get("scaled");
        String scaledActual = (scaledValue == null) ? null : scaledValue.toString();
        String scaledExpected = Integer.toString(scaled);
        Assert.assertTrue("Scaled value '" + scaledActual + "' of key 'result' is not equal to '" + scaledExpected + "'",
                scaledActual.equals(scaledExpected));
    }
    public List<String> getListOfAuthorsForScoringComment(List<String> actorNames, List<String> peerToReviewNames) {
        String authorMbox1 = "";
        String authorMbox2 = "";
        String authorMbox3 = "";

        // Find authorMboxs from pairs: actorName / peerToReviewName
        for (String peer : peerToReviewNames) {
            if (peer.equals(actorNames.get(0))) {
                String authorName1 = "";
                if (peer.equals(peerToReviewNames.get(1))) {
                    authorName1 = actorNames.get(1); // user2
                }
                if (peer.equals(peerToReviewNames.get(2))) {
                    authorName1 = actorNames.get(2); // user3
                }
                List<String> author1 = Arrays.asList(authorName1, authorName1);
                authorMbox1 = getMboxValue(author1);
            }
            if (peer.equals(actorNames.get(1))) {
                String authorName2 = "";
                if (peer.equals(peerToReviewNames.get(0))) {
                    authorName2 = actorNames.get(0); // user1
                }
                if (peer.equals(peerToReviewNames.get(2))) {
                    authorName2 = actorNames.get(2); // user3
                }
                List<String> author2 = Arrays.asList(authorName2, authorName2);
                authorMbox2 = getMboxValue(author2);
            }
            if (peer.equals(actorNames.get(2))) {
                String authorName3 = "";
                if (peer.equals(peerToReviewNames.get(0))) {
                    authorName3 = actorNames.get(0); // user1
                }
                if (peer.equals(peerToReviewNames.get(1))) {
                    authorName3 = actorNames.get(1); // user2
                }
                List<String> author3 = Arrays.asList(authorName3, authorName3);
                authorMbox3 = getMboxValue(author3);
            }
        }
        List<String> authorMboxs = Arrays.asList(authorMbox1, authorMbox2, authorMbox3);
        return authorMboxs;
    }




}
