package com.inceptum.peasToolTest;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.inceptum.pages.EditPeerAssignmentPage;
import com.inceptum.pages.MediaPopupPage;
import com.inceptum.pages.ViewPeerAssignmentPage;

/**
 * Created by Olga on 25.05.2015.
 */
public class TinCanPeasTest extends TinCanPeasBaseTest {

    /*---------CONSTANTS--------*/



     /* ----- Tests ----- */

// View PeerTask (the 1st tab)

    @Test
    public void testViewPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on Class overview page
        checkClassIsAddedOnOverview(className);
        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath);

        // Get the peer task name
        String peerTaskName = "Peer Assignment: " + titlePA;
        String viewPeerTaskDescriptionUrl = webDriver.getCurrentUrl(); // get current url
        // Get the peer task node url
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerTaskUrl = webDriver.getCurrentUrl();
        String peerTaskNodeUrl = editPeerTaskUrl.split("/edit")[0]; // Separate needed node URL
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID
        String chapterName = editPeerAssignmentPage.getChapterName();
        String chapterID =editPeerAssignmentPage.getChapterID();
        // Get actor name and email
        String actorName = getName();
        String actorMbox = "mailto:" + getEmailAddress();

        clearLogMessages();

        // View Task description
        webDriver.get(viewPeerTaskDescriptionUrl);
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement

        // Get list of tincan statements for viewing PA
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(0);

        // Check tincan statement
        checkViewPeerTaskStatement(statement, actorName, actorMbox, peerTaskNodeUrl, peerTaskName, classNodeUrl,
                chapterID, chapterName);
    }

    @Test
    public void testViewPeerTaskAsUser() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on Class overview page
        checkClassIsAddedOnOverview(className);
        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath);

        // Get the peer task name
        String peerTaskName = "Peer Assignment: " + titlePA;
        String viewPeerTaskDescriptionUrl = webDriver.getCurrentUrl(); // get current url
        // Get the peer task node url
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerTaskUrl = webDriver.getCurrentUrl();
        String peerTaskNodeUrl = editPeerTaskUrl.split("/edit")[0]; // Separate needed node URL
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID
        String chapterName = editPeerAssignmentPage.getChapterName();
        String chapterID =editPeerAssignmentPage.getChapterID();
        // Get actor name and email
        addAuthenticatedUser(user1);
        String actorName = user1.get(0);
        String actorMbox = "mailto:" + getEmail(user1);

        clearLogMessages();

        // View PeerTask description as the user
        logout(admin);
        loginAs(user1); // here the user will register to the defined Class
        webDriver.get(viewPeerTaskDescriptionUrl); // open ViewPeerTask page
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement
        logout(user1);
        login(admin); // log in as admin

        // Get list of tincan statements for viewing PA
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(0);

        // Check tincan statement
        checkViewPeerTaskStatement(statement, actorName, actorMbox, peerTaskNodeUrl, peerTaskName, classNodeUrl,
                chapterID, chapterName);
    }


// Watch the main content video from View PeerTask page

    @Test
    public void testViewYoutubeVideoAssignmentContentWithPause() throws Exception { // youtube video paused
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account
        // Add main content to 'Assignment context' tab
        openEditPage();
        addMainContent(titleMainContent, youtubeURL); // youtube video

        // Get parent node
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String parentNode = editTaskUrl.split("/edit")[0].split("node/")[1];

        // Get Class node url and name
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Save changes in the Peer Assignment
        editPeerAssignmentPage.clickButtonSaveTop();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // Get main content video name
        String videoName = getVideoYoutubeName();
        String viewVideoAssignmentContentUrl = webDriver.getCurrentUrl(); // get current url

        clearLogMessages();

        // Open ViewPeerTask page
        webDriver.get(viewVideoAssignmentContentUrl);
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement

        // Get video duration, define start, pause, end points
        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set pause point array
        List<Integer> pausePointArray = Arrays.asList((duration/2) - 1, duration/2, (duration/2) + 1);
        // Set end point array
        List<Integer> endPointArray = durationArray;
        // Pause in the middle of the video
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        closeCookiesPopup();
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        clickButtonPlayYoutube(); // play
        Thread.sleep(duration*1000/2);
        clickButtonPauseYoutube(); // pause
        clickButtonPlayYoutube(); // play
        // Wait until video is completed
        waitUntilYoutubeVideoIsCompleted(); // completed
        webDriver.switchTo().defaultContent();
        Thread.sleep(2000); // timeout for correct generating of video statements

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();

        // Check tincan statements
        checkVideoPausedStatementsWithoutNode(statements, youtubeURL, videoName, classNodeUrl, className, parentNode,
                durationArray, startPointArray, pausePointArray, endPointArray);
    }

    @Test
    public void testViewVimeoVideoAssignmentContentWithSkippingWithoutPause() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pptxFilePath); // select the storage account
        // Add main content to 'Assignment context' tab
        openEditPage();
        addMainContent(titleMainContent, vimeoURL); // vimeo video

        // Get parent node
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String parentNode = editTaskUrl.split("/edit")[0].split("node/")[1];

        // Get Class node url and name
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Save changes in the Peer Assignment
        editPeerAssignmentPage.clickButtonSaveTop();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // Get main content video name
        String videoName = getVideoVimeoName();
        String viewVideoAssignmentContentUrl = webDriver.getCurrentUrl(); // get current url

        clearLogMessages();

        // Open ViewPeerTask page
        webDriver.get(viewVideoAssignmentContentUrl);
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement

        // Get video duration, define start, skipping, end points
        int duration = getDurationVimeoVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(2, 3);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(6, 7); // these values depends on duration (70%)
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Watch the video with skipping
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        closeCookiesPopup();
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        clickButtonPlayVimeo(); // play
        waitUntilVimeoVideoPlaysSec(3); // wait 3 sec
        // Skip a part of video (end point = 70% of video width) without pausing
        skipPartOfVimeoVideo(70);
        // Wait until video is completed
        waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();

        Thread.sleep(2000); // timeout for correct generating of video statements

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();

        // Check tincan statements
        checkVideoSkippedStatementsWithoutNode(statements, vimeoURL, videoName, classNodeUrl, className, parentNode,
                durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }

    @Test
    public void testViewYoutubeVideoAssignmentContentWithSkippingWithoutPause() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pptxFilePath); // select the storage account
        // Add main content to 'Assignment context' tab
        openEditPage();
        addMainContent(titleMainContent, youtubeURL2); // youtube video

        // Get parent node
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editTaskUrl = webDriver.getCurrentUrl();
        String parentNode = editTaskUrl.split("/edit")[0].split("node/")[1];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Save changes in the Peer Assignment
        editPeerAssignmentPage.clickButtonSaveTop();
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        String viewVideoAssignmentContentUrl = webDriver.getCurrentUrl(); // get current url

        clearLogMessages();

        // Open ViewPeerTask page
        webDriver.get(viewVideoAssignmentContentUrl);
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement

        // Get video duration, define start, skipping, end points
        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(3, 4, 5);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(10, 11); // = 2/3 of whole video width
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Watch the video with skipping
        MediaPopupPage mediaPopupPage = new MediaPopupPage(webDriver, pageLocation);
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        closeCookiesPopup();
        String videoName = getVideoYoutubeName(); // get main content video name
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        clickButtonPlayYoutube(); // play
        waitUntilYoutubeVideoPlaysSec(4); // wait 4 sec
        // Skip a part of video without pausing
        skipPartOfYoutubeVideo(); // skipping end point == 2/3 of whole video width
        // Wait until video is completed
        waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();

        Thread.sleep(2000); // timeout for correct generating of video statements

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();

        // Check tincan statements
        checkVideoSkippedStatementsWithoutNode(statements, youtubeURL2, videoName, classNodeUrl, className, parentNode,
                durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }


// View Task resources from View PeerTask page

    @Test
    public void testViewImageTaskResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pptxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add an Image to 'Task resources'
        editPeerAssignmentPage.switchToTabTaskResources();
        addImageToTaskResources();

        // Get the image node ID
        String imageNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldImagesTaskResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // View the Image from PeerTask
        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the image name
        String imageName = mediaPopupPage.getPopupName();
        // Get the image node url
        String imageNodeUrl = websiteUrl +"/node/" + imageNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewImageStatement(false, logMessage, imageNodeUrl, imageName, classNodeUrl, className, parentId);
    }

    @Test
    public void testViewVideoTaskResourcesFromPeerTask() throws Exception { // youtube
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Video to 'Task resources'
        editPeerAssignmentPage.switchToTabTaskResources();
        addVideoToTaskResources(urlYouTube);

        // Get the video node ID
        String videoNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldVideosTaskResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // View the Video from PeerTask
        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the video name
        String videoName = mediaPopupPage.getPopupName();
        // Get the video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewVideoStatement(false, logMessage, videoNodeUrl, videoName, classNodeUrl, className, parentId);
    }

    @Test
    public void testWatchVideoWithSkippingFromTaskResourcesPeerTask() throws Exception { // youtube
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String[] parts = editPeerAssignmentUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Video to 'Task resources'
        editPeerAssignmentPage.switchToTabTaskResources();
        addVideoToTaskResources(youtubeURL2);

        // Get the video node ID
        String videoNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldVideosTaskResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        String viewPeerTaskUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // View the Video from PeerTask
        webDriver.get(viewPeerTaskUrl);
        Thread.sleep(1000); // wait for generating 'view PeerTask' statement first
        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();
        // Get the video name
        String videoName = getVideoYoutubeName();
        // Get the video title
        String videoTitle = mediaPopupPage.getPopupName();
        // Get the video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Get video duration and set start/skip/end points
        int duration = getDurationYoutubeVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(4, 5);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(10, 11); // = 2/3 of whole video width
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Watch the video 4 sec
        waitUntilElementIsVisible(mediaPopupPage.getIframeYoutubePlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeYoutubePlayer());
        waitUntilYoutubeVideoPlaysSec(4); // wait 4 sec
        // Skip a part of video without pausing
        skipPartOfYoutubeVideo(); // skipping end point == 2/3 of whole video width
        // Wait until video is completed
        waitUntilYoutubeVideoIsCompleted();
        webDriver.switchTo().defaultContent();

        Thread.sleep(2000); // timeout for correct generating of video statements

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoSkippedStatements(statements, videoNodeUrl, youtubeURL2, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }

    @Test
    public void testViewPresentationTaskResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Presentation to 'Task resources'
        editPeerAssignmentPage.switchToTabTaskResources();
        addPresentationToTaskResources();

        // Get the Presentation node ID
        String presentationNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldPresentationsTaskResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // View the Presentation from PeerTask
        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the presentation name
        String presentationName = mediaPopupPage.getPopupName();
        // Get the video node url
        String presentationNodeUrl = websiteUrl +"/node/" + presentationNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewPresentationStatement(false, logMessage, presentationNodeUrl, presentationName, classNodeUrl,
                className, parentId);
    }

    @Test
    public void testViewDownloadedMaterialTaskResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a File to 'Task resources'
        editPeerAssignmentPage.switchToTabTaskResources();
        addFileToDownloads(titleUploadedFile, xlsxFilePath);

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // Get ID of downloaded file
        String downloadsId = viewPeerAssignmentPage.getDownloadsIdTaskResources();

        // View downloaded file
        viewPeerAssignmentPage.clickFileDownloads();
        waitUntilFileDownloaded();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewFileDownloadsStatement(false, logMessage, downloadsId, titleUploadedFile, classNodeUrl,
                className, parentId);
    }

    @Test
    public void testVisitLinkTaskResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Link to 'Task resources'
        editPeerAssignmentPage.switchToTabTaskResources();
        addLinkToTaskResources(titleLink, linkUrl);
        editPeerAssignmentPage.selectCheckboxOpenInNewWindow();

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // Visit the link
        viewMaterialInAnotherBrowserTab(viewPeerAssignmentPage.getFieldLinkResource());

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkVisitLinkStatement(false, logMessage, linkUrl, titleLink, classNodeUrl, className, parentId);
    }



// View Additional resources from View PeerTask    (PILOT-978 for video, image, presentation ????????????)

    @Test // PILOT-1517 !!!!!!!!
    public void testViewImageAdditionalResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pptxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add an Image to 'Additional resources'
        editPeerAssignmentPage.switchToTabAdditionalResources();
        addImageToAdditionalResources();

        // Get the image node ID
        String imageNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldImagesAdditionalResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // View the Image from PeerTask

        // PILOT-1517 (Additional resources are displayed incorrectly in PA) !!!!!!!!

        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the image name
        String imageName = mediaPopupPage.getPopupName();
        // Get the image node url
        String imageNodeUrl = websiteUrl +"/node/" + imageNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewImageStatement(additional, logMessage, imageNodeUrl, imageName, classNodeUrl, className, parentId);
    }

    @Test // PILOT-1517 !!!!!!!!
    public void testViewVideoAdditionalResourcesFromPeerTask() throws Exception { // vimeo
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pptxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Video to 'Additional resources'
        editPeerAssignmentPage.switchToTabAdditionalResources();
        addVideoToAdditionalResources(urlVimeo);

        // Get the video node ID
        String videoNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldVideosAdditionalResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // View the Video from PeerTask

        // PILOT-1517 (Additional resources are displayed incorrectly in PA) !!!!!!!!

        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the video name
        String videoName = mediaPopupPage.getPopupName();
        // Get the video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewVideoStatement(additional, logMessage, videoNodeUrl, videoName, classNodeUrl, className, parentId);
    }

    @Test // PILOT-1517 !!!!!!!!
    public void testWatchVideoWithSkippingFromAdditionalResourcesPeerTask() throws Exception { // vimeo
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String[] parts = editPeerAssignmentUrl.split("/edit"); // Separate needed node URL
        String parentId = parts[0]; // Node url
        parts = parentId.split("node/");
        String parentNode = parts[1]; // get parent node

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Video to 'Additional resources'
        editPeerAssignmentPage.switchToTabAdditionalResources();
        addVideoToAdditionalResources(vimeoURL);

        // Get the video node ID
        String videoNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldVideosAdditionalResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");
        String viewPeerTaskUrl = webDriver.getCurrentUrl();

        clearLogMessages();

        // View the Video from PeerTask
        webDriver.get(viewPeerTaskUrl);
        Thread.sleep(1000); // wait for generating 'view PeerTask' statement first

        // PILOT-1517 (Additional resources are displayed incorrectly in PA) !!!!!!!!

        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();
        // Get the video name
        String videoName = getVideoVimeoName();
        // Get the video title
        String videoTitle = mediaPopupPage.getPopupName();
        // Get the video node url
        String videoNodeUrl = websiteUrl +"/node/" + videoNodeId;

        // Get video duration and set start/skip/end points
        int duration = getDurationVimeoVideo();
        List<Integer> durationArray = Arrays.asList(duration - 1, duration, duration + 1); // the values are possible
        // Set start point array
        List<Integer> startPointArray = Arrays.asList(0, 1); // two values are possible
        // Set skip start point array
        List<Integer> startSkipPointArray = Arrays.asList(2, 3);
        // Set skip end point array
        List<Integer> endSkipPointArray = Arrays.asList(6, 7); // these values depends on duration (70%)
        // Set end point array
        List<Integer> endPointArray = durationArray;

        // Watch the video 2 sec
        waitUntilElementIsVisible(mediaPopupPage.getIframeVimeoPlayer());
        webDriver.switchTo().frame(mediaPopupPage.getIframeVimeoPlayer());
        waitUntilVimeoVideoPlaysSec(3); // wait 3 sec
        // Skip a part of video (end point = 70% of video width) without pausing
        skipPartOfVimeoVideo(70);
        // Wait until video is completed
        waitUntilVimeoVideoIsCompleted();
        webDriver.switchTo().defaultContent();

        Thread.sleep(2000); // timeout for correct generating of video statements

        // Get list of tincan statements for Video interaction
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();


        // Check tincan statements
        checkVideoSkippedStatements(statements, videoNodeUrl, vimeoURL, videoTitle, videoName, classNodeUrl, className,
                parentId, parentNode, durationArray, startPointArray, startSkipPointArray, endSkipPointArray, endPointArray);
    }

    @Test // PILOT-1517 !!!!!!!!
    public void testViewPresentationAdditionalResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Presentation to 'Additional resources'
        editPeerAssignmentPage.switchToTabAdditionalResources();
        addPresentationToAdditionalResources();

        // Get the presentation node ID
        String presentationNodeId = editPeerAssignmentPage.getItemNodeId(editPeerAssignmentPage.getFieldPresentationsAdditionalResources());

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // View the Presentation from PeerTask

        // PILOT-1517 (Additional resources are displayed incorrectly in PA) !!!!!!!!

        MediaPopupPage mediaPopupPage = viewPeerAssignmentPage.clickLinkMaterial();
        mediaPopupPage.assertPopupIsOpened();

        // Get the presentation name
        String presentationName = mediaPopupPage.getPopupName();
        // Get the presentation node url
        String presentationNodeUrl = websiteUrl +"/node/" + presentationNodeId;

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewPresentationStatement(additional, logMessage, presentationNodeUrl, presentationName, classNodeUrl,
                className, parentId);
    }

    @Test // PILOT-1517, 978 !!!!!!!!
    public void testViewDownloadedMaterialAdditionalResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a File to 'Additional resources'
        editPeerAssignmentPage.switchToTabAdditionalResources();
        addFileToDownloadsAdditional(titleUploadedFile, xlsxFilePath);

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // Get ID of downloaded file
        String downloadsId = viewPeerAssignmentPage.getDownloadsIdAdditionalResources();

        // View downloaded file
        viewPeerAssignmentPage.clickFileDownloadsAdditional(); // PILOT-1517 (icons) !!!!!!!!
        waitUntilFileDownloaded();

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement

        // PILOT-978 (additional) !!!!!!!!!!!

        checkViewFileDownloadsStatement(additional, logMessage, downloadsId, titleUploadedFile, classNodeUrl,
                className, parentId);
    }

    @Test // PILOT-1517, 978 !!!!!!!!
    public void testVisitLinkAdditionalResourcesFromPeerTask() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account

        // Get parent node
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Add a Link to 'Additional resources'
        editPeerAssignmentPage.switchToTabAdditionalResources();
        addLinkToAdditionalResources(titleLink, linkUrl);
        editPeerAssignmentPage.selectCheckboxOpenInNewWindowAdditional();

        // Save changes in the Peer Assignment
        ViewPeerAssignmentPage viewPeerAssignmentPage = editPeerAssignmentPage.clickButtonSaveTop();
        assertTextOfMessage(viewPeerAssignmentPage.getMessage(), ".*Peer Assignment " + titlePA + " has been updated.");

        // Visit the link
        viewMaterialInAnotherBrowserTab(viewPeerAssignmentPage.getFieldLinkAdditional()); // PILOT-1517 (icons) !!!

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement

        // PILOT-978 (additional) !!!!!!!!!!!

        checkVisitLinkStatement(additional, logMessage, linkUrl, titleLink, classNodeUrl, className, parentId);
    }



// Viewing 'Submit a draft version' tab

    @Test
    public void testViewSubmitDraftVersionPage() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on PA overview page
        checkClassIsAddedOnOverview(className);
        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, docxFilePath); // select the storage account
        // Get the peer task name
        String peerTaskName = "Peer Assignment: " + titlePA;
        // Get url of the 'Submit draft version' page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get current url
        // Get Class node url
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;
        // Get Chapter name, ID
        String chapterName = editPeerAssignmentPage.getChapterName();
        String chapterID =editPeerAssignmentPage.getChapterID();
        // Get node ID
        String nodeId = webDriver.getCurrentUrl().split("/edit")[0].split("node/")[1];
        // Get actor name and email
        addAuthenticatedUser(user1);
        String actorName = user1.get(0);
        String actorMbox = "mailto:" + getEmail(user1);

        clearLogMessages();

        // View SubmitDraftVersion page as the user
        logout(admin);
        loginAs(user1);
        webDriver.get(viewSubmitDraftVersionUrl); // open View SubmitDraftVersion page
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement
        logout(user1);
        login(admin); // log in as admin

        // Get text of tincan log message
        String logMessage = getTextOfTinCanLogMessage();

        // Check tincan statement
        checkViewSubmitDraftVersionStatement(logMessage, actorName, actorMbox, viewSubmitDraftVersionUrl, peerTaskName,
                classNodeUrl, chapterID, chapterName, nodeId);
    }


// Viewing Template (2 cases: 1) with downloading a file, 2) GDrive doc)

    // PILOT-1356 !!!!!!!!!!!!!!



// Viewing Own draft (2.3)

    // with downloading a draft
    // !!!!!!!!!!!!!!!



// Viewing Evaluation criteria

    @Test // PILOT-1520 (extra slash) !!!!
    public void testViewEvaluationCriteriaByUser() throws Exception {
        checkTinCanConfiguration();

        // Check Class is added on Class overview page
        checkClassIsAddedOnOverview(className);
        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath);

        // Get criteria name, id
        String criteriaName = titlePA + " - Evaluation Criteria";
        String viewPeerTaskDescriptionUrl = webDriver.getCurrentUrl(); // get current url
        String criteriaId = viewPeerTaskDescriptionUrl.concat("/rubric/"); // "/rubric" !!!!!!

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email
        addAuthenticatedUser(user1);
        String actorName = user1.get(0);
        String actorMbox = "mailto:" + getEmail(user1);

        clearLogMessages();

        // View Evaluation criteria as the user
        logout(admin);
        loginAs(user1); // here the user will register to the defined Class
        webDriver.get(viewPeerTaskDescriptionUrl); // open ViewPeerTask page
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement
        viewEvaluationCriteria(); // view evaluation criteria
        logout(user1);
        login(admin); // log in as admin

        // Get list of tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(0);

        // Check tincan statement
        checkViewEvaluationCriteriaStatement(statement, actorName, actorMbox, criteriaId, criteriaName, classNodeUrl, parentId);
    }


// Fails to upload draft before deadline


    @Test
    public void testFailToUploadDraftBeforeDeadline() throws Exception {
        checkTinCanConfiguration();

        // Add a new Class, add a student to the Class
        String viewClassPageUrl = addStudentToClass(user1, className);
        checkOnlyUserInClassGroup(user1, viewClassPageUrl); // check that only the student and admin are members in the Class

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, xlsxFilePath);

        // Get draft name, id
        String draftName = titlePA + " - Draft";
        String viewPeerTaskDescriptionUrl = webDriver.getCurrentUrl(); // get current url
        String draftId = viewPeerTaskDescriptionUrl.concat("/draft");

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
//        String parentId = editPeerAssignmentUrl.split("/edit")[0]; // !!!!!!!!! bug???

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email
        String actorName = user1.get(0);
        String actorMbox = "mailto:" + getEmail(user1);

        clearLogMessages();

        // Change draft submission deadline date < actual date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(draft);

        // Get list of tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(2); // 'fail' statement

        // Check tincan statement
        checkFailToUploadDraftStatement(statement, actorName, actorMbox, draftId, draftName, classNodeUrl); // , parentId !!!!
    }


// Progressed to review stage

    @Test // PILOT-1522, parent key !!!!!!
    public void testProgressToReviewStage() throws Exception {
        checkTinCanConfiguration();

        // Add a new Class, add a student to the Class
        String viewClassPageUrl = addStudentToClass(user1, className);
        checkOnlyUserInClassGroup(user1, viewClassPageUrl); // check that only the student and admin are members in the Class

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get draft name, id
        String draftName = titlePA + " - Draft";
        String viewPeerTaskDescriptionUrl = webDriver.getCurrentUrl(); // get current url
        String draftId = viewPeerTaskDescriptionUrl.concat("/draft");

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];

        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email
        String actorName = user1.get(0);
        String actorMbox = "mailto:" + getEmail(user1);

        clearLogMessages();

        // Submit draft version as the user
        logout(admin);
        login(user1);
        webDriver.get(viewPeerTaskDescriptionUrl); // open ViewPeerTask page
        createDraftSubmission(user1);
        logout(user1);
        login(admin); // log in as admin

        // Change draft submission deadline date < actual date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(draft);

        // Get list of tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(2); // 'progressed' statement   !!!!!!!!!!!!!!!!!!!!!!!!

        // Check tincan statement
        checkProgressToUploadDraftStatement(statement, actorName, actorMbox, draftId, draftName, classNodeUrl, parentId);
    }


// View PEER review page

    @Test // PILOT-1525 !!!!!! reviewId ??? (the person id instead of the object id)
    public void testViewPeerReviewPage() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        String peerTaskName = "Peer Assignment: " + titlePA; // for review name

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email for three users
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);
        String actorName2 = user2.get(0);
        String actorMbox2 = getMboxValue(user2);
        String actorName3 = user3.get(0);
        String actorMbox3 = getMboxValue(user3);

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);
        clearLogMessages();

        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);
        // Masquerade as user1 and get his peer for review info
        List<String> infoPeer1 = getPeerForReviewInfo(user1, peerTaskName, user2, user3); // !!!!!! review id !!!!!
        switchBackFromMasqueradingAs(user1, admin);                    // (the person id instead of the object id)

        // Masquerade as user2 and get his peer for review info
        List<String> infoPeer2 = getPeerForReviewInfo(user2, peerTaskName, user1, user3); // !!!!!! review id !!!!!
        switchBackFromMasqueradingAs(user2, admin);

        // Masquerade as user3 and get his peer for review info
        List<String> infoPeer3 = getPeerForReviewInfo(user3, peerTaskName, user1, user2); // !!!!!! review id !!!!!
        switchBackFromMasqueradingAs(user3, admin);

        // Get list of tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();

        // PILOT-1525 !!!!!!!!!!!!!!!!!!!!!!!!!!
        String statement1 = statements.get(7); // 5
        String statement2 = statements.get(4); // 3
        String statement3 = statements.get(1);

        // Check tincan statements for each user
        checkViewPeerReviewPageStatement(statement1, actorName1, actorMbox1, infoPeer1.get(0), infoPeer1.get(1),
                infoPeer1.get(2), classNodeUrl, parentId);
        checkViewPeerReviewPageStatement(statement2, actorName2, actorMbox2, infoPeer2.get(0), infoPeer2.get(1),
                infoPeer2.get(2), classNodeUrl, parentId);
        checkViewPeerReviewPageStatement(statement3, actorName3, actorMbox3, infoPeer3.get(0), infoPeer3.get(1),
                infoPeer3.get(2), classNodeUrl, parentId);
    }


// View peer's draft

    @Test // submissionId ???? (the person id instead of the object id)
    public void testViewPeerDraft() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        String peerTaskName = "Peer Assignment: " + titlePA; // for submission name

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email for one of users
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);
        clearLogMessages();

        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);
        // Masquerade as user1, get values
        masqueradeAsUser(user1);
        String peerToReview = viewPeerAssignmentPage.getFieldPeerToReview().getText(); // get peer's name
        List<String> userCredentials = Arrays.asList(peerToReview, peerToReview);
        String submissionId = viewPeerAssignmentPage.getPeerSubmissionId(); // get peer submission id  !!!!!!!!!!!!!!!!! change node !!!!!!!!!!
        String submissionName = getPeerSubmissionName(userCredentials, peerTaskName); // get submission name
        String authorMbox = getMboxValue(userCredentials); // get author value
        // Check that the peer's name equal to one of possible values
        Assert.assertTrue("Wrong peer's username is displayed on ViewPeerReviewPage.", peerToReview.contains(user2.get(0))
                || peerToReview.contains(user3.get(0)));
        // View peer's draft
        viewPeerDraft(userCredentials);
        switchBackFromMasqueradingAs(user1, admin);

        // Get tincan statement
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(1);

        // Check tincan statement
        checkViewPeerDraftStatement(statement, actorName1, actorMbox1, submissionId, submissionName,
                authorMbox, classNodeUrl, parentId);
    }



// Comment on peer draft

    @Test // submissionId ???? (the person id instead of the object id)
    public void testCommentOnPeerDraft() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        String peerTaskName = "Peer Assignment: " + titlePA; // for submission name

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email for one of users
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);
        clearLogMessages();

        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);
        // Masquerade as user1, get values
        masqueradeAsUser(user1);
        String peerToReview = viewPeerAssignmentPage.getFieldPeerToReview().getText(); // get peer's name
        List<String> userCredentials = Arrays.asList(peerToReview, peerToReview);
        String submissionId = viewPeerAssignmentPage.getPeerSubmissionId(); // get peer submission id  !!!!!!!!!!!!!!!!! change node !!!!!!!!!!
        String submissionName = getPeerSubmissionName(userCredentials, peerTaskName); // get submission name
        String authorMbox = getMboxValue(userCredentials); // get author value
        // Check that the peer's name equal to one of possible values
        Assert.assertTrue("Wrong peer's username is displayed on ViewPeerReviewPage.", peerToReview.contains(user2.get(0))
                || peerToReview.contains(user3.get(0)));
        // Comment peer's draft
        commentPeerDraft();
        switchBackFromMasqueradingAs(user1, admin);

        // Get tincan statement
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(2);

        // Check tincan statement
        checkCommentPeerDraftStatement(statement, actorName1, actorMbox1, submissionId, submissionName,
                authorMbox, classNodeUrl, parentId);
    }


// Fail to comment before deadline

    @Test // !!! PILOT-1526 (parentId) !!!!!
    public void testFailToCommentBeforeDeadline() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actors' names and emails
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);
        String actorName2 = user2.get(0);
        String actorMbox2 = getMboxValue(user2);
        String actorName3 = user3.get(0);
        String actorMbox3 = getMboxValue(user3);

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);
        clearLogMessages();

        // Open 'Review peer work' page
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(review);

        // Get tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement1 = statements.get(2);
        String statement2 = statements.get(3);
        String statement3 = statements.get(4);
        statements = Arrays.asList(statement1, statement2, statement3); // get list of statements
        List<String> users = Arrays.asList(actorName1, actorName2, actorName3); // get list of actors
        List<String> mboxs = Arrays.asList(actorMbox1, actorMbox2, actorMbox3); // get list of mboxs

        // Check tincan statements
        checkThreeFailsToCommentStatements(statements, users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
    }


// Progressed to final stage

    @Test // !!!! PILOT-1488 (verbId) !!!!
    public void testProgressedToFinalStage() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actors' names and emails
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);
        String actorName2 = user2.get(0);
        String actorMbox2 = getMboxValue(user2);
        String actorName3 = user3.get(0);
        String actorMbox3 = getMboxValue(user3);

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);

        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);
        // Masquerade as user1, comment peer's draft
        masqueradeAsUser(user1);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user1, admin);

        // Masquerade as user2, comment peer's draft
        masqueradeAsUser(user2);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user2, admin);

        // Masquerade as user3, comment peer's draft
        masqueradeAsUser(user3);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user3, admin);

        // Change review deadline date
        clearLogMessages();
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(review);

        // Get tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement1 = statements.get(2);
        String statement2 = statements.get(3);
        String statement3 = statements.get(4);
        statements = Arrays.asList(statement1, statement2, statement3); // get list of statements
        List<String> users = Arrays.asList(actorName1, actorName2, actorName3); // get list of actors
        List<String> mboxs = Arrays.asList(actorMbox1, actorMbox2, actorMbox3); // get list of mboxs

        // Check tincan statements
        checkThreeProgressedToFinalStageStatements(statements, users, mboxs, viewPeerReviewPageUrl, classNodeUrl, parentId);
    }



// View final page

    @Test
    public void testViewFinalPage() throws Exception {
        checkTinCanConfiguration();

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        // Add a user
        addAuthenticatedUser(user1);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get final page url
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        String peerTaskName = "Peer Assignment: " + titlePA; // get peertask name
        viewPeerAssignmentPage.switchToStep3();
        String viewFinalPageUrl = webDriver.getCurrentUrl();

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email for user1
        String actorName = user1.get(0);
        String actorMbox = getMboxValue(user1);

        clearLogMessages();

        // Open final page as the user
        logout(admin);
        loginAs(user1);
        webDriver.get(viewFinalPageUrl); // open ViewFinal page
        waitForPageToLoad();
        Thread.sleep(1000); // timeout for generating correct 'viewed' statement
        logout(user1);
        login(admin);

        // Get list of tincan statements
        openRecentLogMessagesPage();
        String statement = getTextOfTinCanLogMessage();

        // Check tincan statements for each user
        checkViewFinalPageStatement(statement, actorName, actorMbox, viewFinalPageUrl, peerTaskName,
                classNodeUrl, parentId);
    }


// View own final

    @Test
    public void testViewOwnFinal() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        viewPeerAssignmentPage.switchToStep3();
        String viewFinalPageUrl = webDriver.getCurrentUrl();


        // ?????????????????
        String finalId ="";



        String finalName = titlePA + " - Final submission of " + user1.get(0);


        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actor name and email for user1
        String actorName = user1.get(0);
        String actorMbox = getMboxValue(user1);

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);

        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);
        // Masquerade as user1, comment peer's draft
        masqueradeAsUser(user1);
        String peerToReview = viewPeerAssignmentPage.getFieldPeerToReview().getText(); // get peer's name
        List<String> userCredentials = Arrays.asList(peerToReview, peerToReview);
        String authorMbox = getMboxValue(userCredentials); // get author value
        commentPeerDraft();
        switchBackFromMasqueradingAs(user1, admin);

        // Change review deadline date < actual date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(review);
        clearLogMessages();

        // Open final page as the user
        webDriver.get(viewFinalPageUrl);
        masqueradeAsUser(user1);
        createFinalSubmission(user1);
        switchBackFromMasqueradingAs(user1, admin);

        // Get list of tincan statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement = statements.get(1);

        // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // Check tincan statements for each user
        checkViewOwnFinalStatement(statement, actorName, actorMbox, finalId, finalName, authorMbox, classNodeUrl, parentId);
    }



// 4.3 Upload final ????????????????




// Score some-ones comment

    @Test // PILOT-1525 (two messages after masquerading ), PILOT-1488 (score), PILOT-1526 (parent) !!!!!!!!!
    public void testScorePeerComment() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        viewPeerAssignmentPage.switchToStep3();
        String viewFinalPageUrl = webDriver.getCurrentUrl(); // get url of 'Submit final version' page

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actors' names and emails
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);
        String actorName2 = user2.get(0);
        String actorMbox2 = getMboxValue(user2);
        String actorName3 = user3.get(0); // mbox for this user isn't needed because he won't score a comment

        List<String> actorNames = Arrays.asList(actorName1, actorName2, actorName3); // list of actors' names

        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);

        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);
        // Get draft solution id
        String draftSolutionId = ""; // !!!!!!!!!!!!!!!!!!!!!!!!!!! add id

        // Masquerade as user1, comment peer's draft
        masqueradeAsUser(user1);
        String peerToReview1 = getPeerToReviewName(); // get peer name
        commentPeerDraft();
        switchBackFromMasqueradingAs(user1, admin);

        // Masquerade as user2, comment peer's draft
        masqueradeAsUser(user2);
        String peerToReview2 = getPeerToReviewName(); // get peer name
        commentPeerDraft();
        switchBackFromMasqueradingAs(user2, admin);

        // Masquerade as user3, comment peer's draft
        masqueradeAsUser(user3);
        String peerToReview3 = getPeerToReviewName(); // get peer name
        commentPeerDraft();
        switchBackFromMasqueradingAs(user3, admin);

        List<String> peerToReviewNames = Arrays.asList(peerToReview1, peerToReview2, peerToReview3); // list of 'peers to review'
        List<String> authorMboxs = getListOfAuthorsForScoringComment(actorNames, peerToReviewNames); // list of authors' mboxs

        // Change review deadline date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(review);

        clearLogMessages();

        // Masquerade as user1, score comment as useful
        webDriver.get(viewFinalPageUrl);
        masqueradeAsUser(user1);
        scorePeerComment(yes);
        switchBackFromMasqueradingAs(user1, admin);

        // Masquerade as user2, score comment as not useful
        masqueradeAsUser(user2);
        scorePeerComment(no);
        switchBackFromMasqueradingAs(user2, admin);

        // Get 'score' statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statementYes = statements.get(5); // (4) !!!!!!!!!! PILOT-1525
        String statementNo = statements.get(1);

        // Check tincan statements
        checkScorePeerCommentStatement(statementYes, actorName1, actorMbox1, draftSolutionId,
                classNodeUrl, parentId, authorMboxs.get(0), 1); // comment is useful

        // !!!!!! (scaled == -1) if No (PILOT-1488)
        checkScorePeerCommentStatement(statementNo, actorName2, actorMbox2, draftSolutionId,
                classNodeUrl, parentId, authorMboxs.get(1), 0); // comment isn't useful
    }



// Fail to submit final before deadline

    @Test // !!! PILOT-1512 !!!
    public void testFailToSubmitFinalBeforeDeadline() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        viewPeerAssignmentPage.switchToStep3();
        String viewFinalPageUrl = webDriver.getCurrentUrl(); // get url of 'Submit final version' page

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actors' names and emails
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);
        String actorName2 = user2.get(0);
        String actorMbox2 = getMboxValue(user2);
        String actorName3 = user3.get(0);
        String actorMbox3 = getMboxValue(user3);

        List<String> users = Arrays.asList(actorName1, actorName2, actorName3); // names
        List<String> mboxs = Arrays.asList(actorMbox1, actorMbox2, actorMbox3); // emails
// Draft
        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);
// Review
        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);

        // Masquerade as user1, comment peer's draft
        masqueradeAsUser(user1);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user1, admin);

        // Masquerade as user2, comment peer's draft
        masqueradeAsUser(user2);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user2, admin);

        // Masquerade as user3, comment peer's draft
        masqueradeAsUser(user3);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user3, admin);

        // Change review deadline date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(review);

        clearLogMessages();
        // Change final deadline date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(finalD);

        // Get 'fail' statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement1 = statements.get(2);
        String statement2 = statements.get(3);
        String statement3 = statements.get(4);

        List<String> statementsToCheck = Arrays.asList(statement1, statement2, statement3); // 'fail' statements

        // Check tincan statements
        checkThreeFailToSubmitFinalStatements(statementsToCheck, users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
    }



// Succeed to submit final before deadline

    @Test // !!! PILOT-1511 !!!
    public void testSucceedToSubmitFinal() throws Exception {
        checkTinCanConfiguration();

        // Add three new users
        addAuthenticatedUser(user1);
        addAuthenticatedUser(user2);
        addAuthenticatedUser(user3);

        // Check if a Class has already been added
        checkClassIsAddedOnOverview(className);
        String viewClassPageUrl = webDriver.getCurrentUrl(); // get url of overview Class page
        // Add the three users to the Class
        addUserToClass(user1, viewClassPageUrl);
        addUserToClass(user2, viewClassPageUrl);
        addUserToClass(user3, viewClassPageUrl);
        // Check that only these three users + admin are members of the Class
        checkThreeUsersInClassGroup(user1, user2, user3, viewClassPageUrl);

        // Create Peer Assignment
        createPeerAssignmentFromClassOverview(GDrive = true, pdfFilePath);

        // Get urls for further redirecting to proper page
        ViewPeerAssignmentPage viewPeerAssignmentPage = new ViewPeerAssignmentPage(webDriver, pageLocation);
        viewPeerAssignmentPage.switchToStep1();
        String viewSubmitDraftVersionUrl = webDriver.getCurrentUrl(); // get url of 'Submit draft version' page
        viewPeerAssignmentPage.switchToStep2();
        String viewPeerReviewPageUrl = webDriver.getCurrentUrl(); // get url of 'Review peer work' page
        viewPeerAssignmentPage.switchToStep3();
        String viewFinalPageUrl = webDriver.getCurrentUrl(); // get url of 'Submit final version' page

        // Get parent id
        openEditPage();
        EditPeerAssignmentPage editPeerAssignmentPage = new EditPeerAssignmentPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editPeerAssignmentPage.getFieldTitle());
        String editPeerAssignmentUrl = webDriver.getCurrentUrl();
        String parentId = editPeerAssignmentUrl.split("/edit")[0];
        // Get Class node url
        editPeerAssignmentPage.clickLinkGroupsAudience();
        String classNode = editPeerAssignmentPage.getClassNode();
        String classNodeUrl = websiteUrl.concat("/node/") + classNode;

        // Get actors' names and emails
        String actorName1 = user1.get(0);
        String actorMbox1 = getMboxValue(user1);
        String actorName2 = user2.get(0);
        String actorMbox2 = getMboxValue(user2);
        String actorName3 = user3.get(0);
        String actorMbox3 = getMboxValue(user3);

        List<String> users = Arrays.asList(actorName1, actorName2, actorName3); // names
        List<String> mboxs = Arrays.asList(actorMbox1, actorMbox2, actorMbox3); // emails
// Draft
        // Submit draft versions
        submitDraftVersionsOfUsers(user1, user2, user3, viewSubmitDraftVersionUrl, editPeerAssignmentUrl);
// Review
        // Open 'Review peer work' page
        webDriver.get(viewPeerReviewPageUrl);

        // Masquerade as user1, comment peer's draft
        masqueradeAsUser(user1);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user1, admin);

        // Masquerade as user2, comment peer's draft
        masqueradeAsUser(user2);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user2, admin);

        // Masquerade as user3, comment peer's draft
        masqueradeAsUser(user3);
        commentPeerDraft();
        switchBackFromMasqueradingAs(user3, admin);

        // Change review deadline date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(review);
// Final
        // Open final page
        webDriver.get(viewFinalPageUrl);

        // Masquerade as user1, submit final
        masqueradeAsUser(user1);
        createFinalSubmission(user1);
        switchBackFromMasqueradingAs(user1, admin);

        // Masquerade as user2, submit final
        masqueradeAsUser(user2);
        createFinalSubmission(user2);
        switchBackFromMasqueradingAs(user2, admin);

        // Masquerade as user3, submit final
        masqueradeAsUser(user3);
        createFinalSubmission(user3);
        switchBackFromMasqueradingAs(user3, admin);

        clearLogMessages();

        // Change final deadline date
        webDriver.get(editPeerAssignmentUrl);
        changeSubmissionDeadlineDate(finalD);

        // Get 'completed' statements
        openRecentLogMessagesPage();
        List<String> statements = getListOfStatements();
        String statement1 = statements.get(2);
        String statement2 = statements.get(3);
        String statement3 = statements.get(4);

        List<String> statementsToCheck = Arrays.asList(statement1, statement2, statement3); // 'completed' statements

        // Check tincan statements
        checkThreeSucceedToSubmitFinalStatements(statementsToCheck, users, mboxs, viewFinalPageUrl, classNodeUrl, parentId);
    }








}
