package com.inceptum.useraccount;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.inceptum.pages.*;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TestBase;
import com.inceptum.enumeration.PageLocation;

/**
 * Created by Olga on 19.01.2015.
 */
public class AccountBaseTest extends TestBase {


    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final List<String> admin = Arrays.asList("admin", "admin");
    protected final String messageText = ".*Thank you for applying for an account. Your account is currently pending approval by the site administrator.\n" +
            "In the meantime, a welcome message with further instructions has been sent to your e-mail address.";


    /* ----- Tests ----- */


    @Before
    public void beforeTest() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
        // Login as admin and clear Recent log messages
        loginAs(admin);
        clearLogMessages();
    }

    @After
    public void afterTest() throws Exception { // admin should be logged in
        // Open Home page and check admin is logged in
        HomePage homePage = openHomePage();
        String xpathLinkUserAccount = "//nav[@id='block-system-user-menu']/ul/li/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLinkUserAccount)));
        WebElement linkUserAccount = webDriver.findElement(By.xpath(xpathLinkUserAccount));
        if (!(linkUserAccount.getText().equals(admin.get(0)))) {
            if (linkUserAccount.getText().equals("Sign in")) { // 'Sign in' button instead of user link is displayed
                login(admin);
            } else { // log out if user link doesn't match with 'admin' (another user has been logged in)
                linkUserAccount.click();
                homePage.clickLinkSignOut();
                login(admin);
            }
        }
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> errors = new ArrayList<String>();
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available.
                break;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {  // Find php messages starting with "Notice:" or "Warning:"
                    errors.add(message.getAttribute("href"));
                }
            }
        }

        // Get all php messages
        for (String url : errors) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            System.out.println(fullMessage);
        }

        if (errors.size() > 0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }


    public void removeRole(String role, List<String> user) throws Exception {
        // Open People page and select the user account (its checkbox)
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        for (WebElement element : elements) {
            if (element.getText().equals(user.get(0))) {
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                // Find option = role in 'Update options' -> 'Remove a role from the selected users' and submit
                PeoplePage peoplePage = new PeoplePage(webDriver, pageLocation);
                List<WebElement> options = peoplePage.getFieldUpdateOptions().findElements(By.tagName("option"));
                for (WebElement option : options) {
                    if (option.getText().equals(role)) {
                        if (option.findElement(By.xpath("./..")).getAttribute("label").equals("Remove a role from the selected users")) {
                            option.click();
                            wait.until(ExpectedConditions.elementToBeSelected(option));
                            peoplePage.clickButtonUpdate();
                            assertTextOfMessage(peoplePage.getMessage(), ".*The update has been performed.");
                            // Check that the selected role is not displayed in People table
                            String roleXpath = "//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']/../../td[4]/div";
                            String actualRole = webDriver.findElement(By.xpath(roleXpath)).getText(); // the string should be empty
                            Assert.assertTrue("The value is still shown in People table: " + actualRole + ".", actualRole.equals(""));
                            return;
                        }
                    }
                }
            }
        }
    }


    public void openEditAccountPageFromPeopleTable(List<String> user) throws Exception {
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        for (WebElement element : elements) {
            if (element.getText().equals(user.get(0))) { // find element with needed name
                WebElement linkEdit = element.findElement(By.xpath("./../../td[7]/a"));
                linkEdit.click(); // click 'edit' link
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.titleContains(user.get(0)));
                break;
            }
        }
    }

    public void unblockSelectedUserAccount(List<String> user) throws Exception {
        PeoplePage peoplePage = new PeoplePage(webDriver, pageLocation);
        List<WebElement> options = peoplePage.getFieldUpdateOptions().findElements(By.tagName("option"));
        for (WebElement option : options) {
            if (option.getText().equals("Unblock the selected users")) {
                option.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(option));
                peoplePage.clickButtonUpdate();
                assertTextOfMessage(peoplePage.getMessage(), ".*The update has been performed.");
                break;
            }
        }
    }

    public void changeAccountStatusToActive(List<String> user) throws Exception {
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        // Find all user names in the People table
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        for (WebElement element : elements) {
            if (element.getText().equals(user.get(0))) {
                // Select needed username (its checkbox)
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                // Unblock user account using Update options
                unblockSelectedUserAccount(user);
                // Check that the user account status is changed to 'active'
                String statusFieldXpath = "//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']/../../td[3]";
                String status = webDriver.findElement(By.xpath(statusFieldXpath)).getText();
                Assert.assertTrue(status.equals("active"));
                break;
            }
        }
    }

    public void createNewAccountWithCorrectCredentials(List<String> user) throws Exception {
        // Open CreateNewAccount page
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        CreateNewAccountPage createNewAccountPage = loginFormPage.switchToCreateNewAccountPage();
        // Fill required fields on the page
        fillFieldsCreateNewAccount(user);

        // Save the data and check message
        createNewAccountPage.clickButtonCreateNewAccount();
        ModalClassWindowPage modalClassWindowPage=new ModalClassWindowPage(webDriver, pageLocation);
        homePage=modalClassWindowPage.clickButtonCancel();

        homePage.waitUntilElementIsVisible(homePage.getUserName());
        String userName=homePage.getUserName().getText();
        //Verify that user Loged in successfully;
        Assert.assertTrue("User is not Log in successfully, current userName is "+userName+" Expected name is "+user.get(0),userName.equals(user.get(0)) );


    }

    public void fillFieldsCreateNewAccount(List<String> user) throws Exception {
        CreateNewAccountPage createNewAccountPage = new CreateNewAccountPage(webDriver, pageLocation);
        // Fill in "Full name" field
        createNewAccountPage.fillFieldFullName(user.get(0));
        // Fill in "E-mail" field
        String email = getEmail(user);
        createNewAccountPage.fillFieldEmail(email);
        // Fill in "Password" field
        createNewAccountPage.fillFieldPassword(user.get(1));
        // Fill in "Confirm password" field
        createNewAccountPage.fillFieldConfirmPassword(user.get(1));
    }

    public void fillFieldsLogIn(List<String> user) throws Exception {
        LoginFormPage loginFormPage = new LoginFormPage(webDriver, pageLocation);
        // Fill in "E-mail" field
        loginFormPage.fillFieldEmail(user.get(0));
        // Fill in "Password" field
        loginFormPage.fillFieldPassword(user.get(1));
    }
    public void waitUntilMessageIsRefreshed() throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.id("edit-submit--2"))); // button 'E-mail new password'
    }
    public void checkEmailFieldHasFullNameData(String fullName) throws Exception {
        RequestNewPasswordPage requestNewPasswordPage = new RequestNewPasswordPage(webDriver, pageLocation);
        waitUntilElementIsVisible(requestNewPasswordPage.getFieldUsernameOrEmailAddress());
        String value = requestNewPasswordPage.getFieldUsernameOrEmailAddress().getAttribute("value");
        Assert.assertTrue(value.equals(fullName));
    }
    public EditUserAccountPage openEditUserAccountPage(List<String> user) throws Exception {
        // Open People page and select the user account (its checkbox)
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        for (WebElement element : elements) {
            if (element.getText().equals(user.get(0))) {
                WebElement linkEdit = element.findElement(By.xpath("./../../td[7]/a"));
                linkEdit.click();
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.titleContains(user.get(0)));
                break;
            }
        }
        return new EditUserAccountPage(webDriver, pageLocation);
    }
    public void checkEmailAddressIsChanged(String emailEdited, List<String> userEdited) throws Exception {
        // Open Profile page
        HomePage homePage = new HomePage(webDriver, pageLocation);
        homePage.clickLinkProfile(userEdited);
        // Wait until the page is opened
        String[] parts = websiteUrl.split("http://"); // Separate website url
        String websiteUrlTrimmed = parts[1];
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains(websiteUrlTrimmed));

        // Check new email address is displayed in the proper field
        openEditPage();
        EditYourProfilePage editYourProfilePage = new EditYourProfilePage(webDriver, pageLocation);
        editYourProfilePage.checkNewEmailAddressIsDisplayed(emailEdited);
    }





}
