package com.inceptum.useraccount;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.pages.AddUserPage;
import com.inceptum.pages.CreateNewAccountPage;
import com.inceptum.pages.DeleteUserAccountConfirmationPage;
import com.inceptum.pages.EditUserAccountPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.PeoplePage;
import com.inceptum.pages.RequestNewPasswordPage;


public class AccountTest extends AccountBaseTest {

    /* ----- CONSTANTS ----- */

    protected final List<String> user = Arrays.asList("JohnBrown", "JohnBrown");
    protected final List<String> userEdited = Arrays.asList("JohnBrownEdited", "JohnBrownEdited");
    protected final List<String> user2 = Arrays.asList("Bob Green", "BobGreen");
    protected final String unregisteredEmail = "someadress@mail.ru";
    protected final String unregisteredUsername = "Some User";
    protected final String wrongPassword = "WrongPassword";
    protected final String masterEditor = "master editor";


    /* ----- Tests ----- */

    @Test
    public void testUserAccountWithCorrectCredentials() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);
        logout(admin);
        // Create new user account
        createNewAccountWithCorrectCredentials(user); // user account is pending approval by admin

    }
  // Name should be filled, duplicate error message.
    @Test
    public void testCreateNewAccountEmptyFields() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);
        logout(admin);
        // Open CreateNewAccount page
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        CreateNewAccountPage createNewAccountPage = loginFormPage.switchToCreateNewAccountPage();

        // Don't fill in any fields, click Save, check message
        createNewAccountPage.clickButtonCreateNewAccount();
        assertTextOfMessage(createNewAccountPage.getMessageError(), ".*E-mail field is required.\nPassword field is required.\nFull name field is required.");

        // Create new user account after validation has worked
        fillFieldsCreateNewAccount(user);
        // Save the data and check message
        createNewAccountPage.clickButtonCreateNewAccount();
        assertTextOfMessage(homePage.getMessage(), messageText); // pending approval
        // Check that the user hasn't been logged in
        Assert.assertTrue("Button 'Sign in' is not displayed.", homePage.getButtonSignIn().isDisplayed());
    }

    @Test
    public void testUserAccountCreatedByAdmin() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));
        logout(admin);

        // Log in as the new user (here you check that the user can log in after registration by admin)
        login(user);
    }

    @Test // PILOT-1646 !!!
    public void testUserAccountCreatedByAdminBlockedStatus() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin (status = blocked)
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.fillInFields(user.get(0), email, user.get(1));
        addUserPage.selectRadiobuttonBlocked(); // select 'Blocked' status
        addUserPage.clickButtonCreateNewAccount();
        assertTextOfMessage(addUserPage.getMessage(), ".*Created a new user account for " + user.get(0) + ". No e-mail has been sent.");

        logout(admin);

        // Log in as the new user (here you check that the user account is blocked)
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(user.get(0));
        loginFormPage.fillFieldPassword(user.get(1));
        // Click 'Log in' button
        loginFormPage.clickButtonLogIn();
        assertTextOfMessage(loginFormPage.getMessage(), ".*The username " + user.get(0) + " has not been activated or is blocked.");

        // Make the user account active
        login(admin);
        changeAccountStatusToActive(user);
        logout(admin);
        // Check that the user can be already logged in
        login(user);
    }

    @Test
    public void testLogInEmptyFields() throws Exception {
        logout(admin);
        // On Login page click 'Log in' button without entering any data into the fields
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.clickButtonLogIn();
        // Check error message
        assertTextOfMessage(loginFormPage.getMessage(), ".*E-mail or username field is required.\nPassword field is required.");

        // Check that user can still log in after the error (let it be admin)
        fillFieldsLogIn(admin);
        loginFormPage.clickButtonLogIn();
        waitUntilProfileLinkIsDisplayed(admin);
    }

    @Test
    public void testLogInWithIncorrectCredentials() throws Exception {
        logout(admin);
        // Open Login page and try to log in with incorrect credentials
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail("SomeUsername"); // incorrect username (the user has not been registered)
        loginFormPage.fillFieldPassword("SomePassword"); // some password
        loginFormPage.clickButtonLogIn();
        // Check error message
        assertTextOfMessage(loginFormPage.getMessage(), ".*Sorry, unrecognized username or password.*");

        // Check that user (admin) can log in after the error
        loginFormPage.getFieldEmail().clear(); // remove wrong data from the field
        fillFieldsLogIn(admin);
        loginFormPage.clickButtonLogIn();
        waitUntilProfileLinkIsDisplayed(admin);
    }

    @Test
    public void testLogInWithEmailData() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));
        logout(admin);

        // Log in as the new user, enter user's email in E-mail field, password; log in
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(email);
        loginFormPage.fillFieldPassword(user.get(1));
        loginFormPage.clickButtonLogIn();
        checkIfRegistrationPopupIsOpenedAndCancel();
        waitUntilProfileLinkIsDisplayed(user);
    }

    @Test
    public void testCreateAccountByAdminIfEmailIsAlreadyUsed() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));
        // Create one more user account but with the same email address
        addUserPage.fillInFields(user2.get(0), email, user2.get(1));
        addUserPage.clickButtonCreateNewAccount();

        // Check the error message
        assertTextOfMessage(addUserPage.getMessageError(), ".*The e-mail address " + email + " is already taken.");
    }

    @Test
    public void testCreateAccountByUserIfEmailIsAlreadyUsed() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));
        logout(admin);

        // Open CreateNewAccount page and fill in all fields, enter the email of the 1st user to Email field
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        CreateNewAccountPage createNewAccountPage = loginFormPage.switchToCreateNewAccountPage();
        createNewAccountPage.fillFieldFullName(user2.get(0)); // fill in "Full name" field
        createNewAccountPage.fillFieldEmail(email); // fill in "E-mail" field (already registered email address)
        createNewAccountPage.fillFieldPassword(user2.get(1)); // fill in "Password" field
        createNewAccountPage.fillFieldConfirmPassword(user2.get(1)); // fill in "Confirm password" field

        // Save the data and check error message
        createNewAccountPage.clickButtonCreateNewAccount();
        assertTextOfMessage(createNewAccountPage.getMessageError(), ".*The e-mail address " + email + " is already registered.*");
    }

    @Test
    public void testRequestNewPasswordWithCorrectUsernameAndEmail() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(user);
        addUserPage.addUser(user.get(0), email, user.get(1));
        logout(admin);

        // Check that instructions will be sent if enter correct Username on RequestNewPassword page
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        RequestNewPasswordPage requestNewPasswordPage = loginFormPage.switchToRequestNewPasswordPage();
        requestNewPasswordPage.fillFieldUsernameOrEmailAddress(user.get(0)); // enter correct username
        requestNewPasswordPage.clickButtonEmailNewPassword();
        assertTextOfMessage(homePage.getMessage(), ".*Further instructions have been sent to your e-mail address.");

        // Check that instructions will be sent if enter correct Email address on RequestNewPassword page
        loginFormPage = homePage.clickButtonSignIn();
        requestNewPasswordPage = loginFormPage.switchToRequestNewPasswordPage();
        requestNewPasswordPage.fillFieldUsernameOrEmailAddress(email); // enter correct email address
        requestNewPasswordPage.clickButtonEmailNewPassword();
        assertTextOfMessage(homePage.getMessage(), ".*Further instructions have been sent to your e-mail address.");
        waitUntilLoginPopupIsClosed();
    }

    @Test
    public void testRequestNewPasswordFromErrorMessageLink() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String fullName = user.get(0);
        String email = getEmail(user);
        addUserPage.addUser(fullName, email, user.get(1));
        logout(admin);

        // Open Login page, enter correct (registered) email and wrong password
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        loginFormPage.fillFieldEmail(email);
        loginFormPage.fillFieldPassword(wrongPassword);
        // Click 'Log in' and check error message
        loginFormPage.clickButtonLogIn();
        waitUntilElementIsVisible(loginFormPage.getMessage());
        WebElement link = loginFormPage.getMessage().findElement(By.xpath("./a"));
        assertTextOfMessage(link, "Have you forgotten your password.");
        // Click link "Have you forgotten your password?"
        link.click();
        waitForPageToLoad();

        // Submit mailing a new password
        checkEmailFieldHasFullNameData(fullName); // the E-mail field has FullName value
        RequestNewPasswordPage requestNewPasswordPage = new RequestNewPasswordPage(webDriver, pageLocation);
        requestNewPasswordPage.clickButtonEmailNewPassword();
        // Check the message
        assertTextOfMessage(requestNewPasswordPage.getMessage(), ".*Further instructions have been sent to your e-mail address.");

        // Log in as admin to check php messages
        loginFormPage.fillFieldEmail(admin.get(0));
        loginFormPage.fillFieldPassword(admin.get(1));
        loginFormPage.clickButtonLogIn();
    }

    @Test
    public void testRequestNewPasswordWithWrongData() throws Exception {
        logout(admin);

        // Open RequestNewPassword page and enter unregistered email address
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        RequestNewPasswordPage requestNewPasswordPage = loginFormPage.switchToRequestNewPasswordPage();
        requestNewPasswordPage.fillFieldUsernameOrEmailAddress(unregisteredEmail); // enter wrong (unregistered) email
        // Submit the form and check error message
        requestNewPasswordPage.clickButtonEmailNewPassword();
        assertTextOfMessage(requestNewPasswordPage.getMessageError(), ".*Sorry, " + unregisteredEmail + " is not recognized as a user name or an e-mail address.");

        // Remove previous data from the field and enter unregistered username
        requestNewPasswordPage.getFieldUsernameOrEmailAddress().clear();
        requestNewPasswordPage.fillFieldUsernameOrEmailAddress(unregisteredUsername); // enter wrong (unregistered) username
        // Submit the form and check error message
        requestNewPasswordPage.clickButtonEmailNewPassword();
        waitUntilMessageIsRefreshed();
        assertTextOfMessage(requestNewPasswordPage.getMessageError(), ".*Sorry, " + unregisteredUsername + " is not recognized as a user name or an e-mail address.");

        // Request new password for 'admin'
        requestNewPasswordPage.getFieldUsernameOrEmailAddress().clear();
        requestNewPasswordPage.fillFieldUsernameOrEmailAddress(admin.get(0)); // enter 'admin' username
        requestNewPasswordPage.clickButtonEmailNewPassword();
        assertTextOfMessage(homePage.getMessage(), ".*Further instructions have been sent to your e-mail address.");
        waitUntilLoginPopupIsClosed();
    }

    @Test
    public void testRequestNewPasswordWithEmptyField() throws Exception {
        logout(admin);

        // Open RequestNewPassword page, submit the form without entering any data into the field
        HomePage homePage = new HomePage(webDriver, pageLocation);
        LoginFormPage loginFormPage = homePage.clickButtonSignIn();
        RequestNewPasswordPage requestNewPasswordPage = loginFormPage.switchToRequestNewPasswordPage();
        requestNewPasswordPage.clickButtonEmailNewPassword();

        // Check error message
        assertTextOfMessage(requestNewPasswordPage.getMessageError(), ".*E-mail field is required.");

        // Request new password for user (admin) after the error
        requestNewPasswordPage.fillFieldUsernameOrEmailAddress(admin.get(0));
        requestNewPasswordPage.clickButtonEmailNewPassword();
        waitUntilLoginPopupIsClosed();
        // Check message
        assertTextOfMessage(homePage.getMessage(), ".*Further instructions have been sent to your e-mail address.");
    }

    @Test
    public void testAddRemoveRoleForSelectedUser() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String fullName = user.get(0);
        String email = getEmail(user);
        addUserPage.addUser(fullName, email, user.get(1));

        // Check that admin is able to add/remove a role for user account with 'Update options'
        addRole(masterEditor, user); // add role
        removeRole(masterEditor, user); // remove role
    }

    @Test
    public void testChangeUsernameEmailPassword() throws Exception {
        // Check if the users are already registered (delete the users' accounts)
        checkUserIsAlreadyRegistered(user);
        checkUserIsAlreadyRegistered(userEdited);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String fullName = user.get(0);
        String email = getEmail(user);
        addUserPage.addUser(fullName, email, user.get(1));

        // Open EditUserAccount page and edit FullName, Email and Password
        EditUserAccountPage editUserAccountPage = openEditUserAccountPage(user);
        String emailEdited = getEmail(userEdited);
        editUserAccountPage.clearAndFillInFields(userEdited.get(0), emailEdited, userEdited.get(1));
        editUserAccountPage.clickButtonSave();
        // Check that changes have been saved
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.titleContains("People"));
        PeoplePage peoplePage = new PeoplePage(webDriver, pageLocation);
        assertTextOfMessage(peoplePage.getMessage(), ".*The changes have been saved.");
        logout(admin);

        // Try to log in with new (edited) user credentials
        login(userEdited);
        // Check that email address is also changed (open EditYourProfilePage)
        checkEmailAddressIsChanged(emailEdited, userEdited);
    }

    @Test
    public void testDeleteAccountFromPeopleTable() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String fullName = user.get(0);
        String email = getEmail(user);
        addUserPage.addUser(fullName, email, user.get(1));

        // Delete account using 'Update options'
        webDriver.get(websiteUrl.concat("/admin/people"));
        waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a"));
        for (WebElement element : elements) {
            if (element.getText().equals(user.get(0))) { // find element with needed name
                WebElement checkbox = element.findElement(By.xpath("./../../td[1]/div/input"));
                checkbox.click(); // select the element
                WebDriverWait wait = new WebDriverWait(webDriver, 10);
                wait.until(ExpectedConditions.elementToBeSelected(checkbox));
                cancelSelectedUserAccount(user); // cancel user account and check info message
                waitForPageToLoad();
                System.out.println("The account '" + user.get(0) + "' has been deleted.");
                break;
            }
        }

        // Check that the account has been deleted from People table
        elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']"));
        Assert.assertTrue("The account '" + user.get(0) + "' is present in People table.", elements.size()==0);
    }

    @Test
    public void testDeleteAccountFromEditAccountPage() throws Exception {
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(user);

        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String fullName = user.get(0);
        String email = getEmail(user);
        addUserPage.addUser(fullName, email, user.get(1));

        // Open Edit account page and cancel the account
        openEditAccountPageFromPeopleTable(user);
        EditUserAccountPage editUserAccountPage = new EditUserAccountPage(webDriver, pageLocation);
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = editUserAccountPage.clickButtonCancelAccount(user);
        deleteUserAccountConfirmationPage.selectCheckboxDeleteAccountAndItsContent();
        deleteUserAccountConfirmationPage.clickButtonCancelAccounts();

        // Check info message
        PeoplePage peoplePage = new PeoplePage(webDriver, pageLocation);
        assertTextOfMessage(peoplePage.getMessage(), ".*" + user.get(0) + " has been deleted.");
        System.out.println("The account '" + user.get(0) + "' has been deleted.");

        // Check that the account has been deleted from People table
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a[text()='" + user.get(0) + "']"));
        Assert.assertTrue("The account '" + user.get(0) + "' is present in People table.", elements.size()==0);
    }





}
