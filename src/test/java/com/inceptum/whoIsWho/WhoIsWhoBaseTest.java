package com.inceptum.whoIsWho;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchContextException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.inceptum.common.TestBase;
import com.inceptum.enumeration.PageLocation;
import com.inceptum.pages.DeleteUserAccountConfirmationPage;
import com.inceptum.pages.EditMembershipInGroupPage;
import com.inceptum.pages.EditUserAccountPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.ModifyRolePage;
import com.inceptum.pages.PeopleInGroupForClassPage;
import com.inceptum.pages.RemoveMembershipInGroupPage;
import com.inceptum.pages.WhoIsWhoPage;

/**
 * Created by Olga on 23.02.2015.
 */
public class WhoIsWhoBaseTest extends TestBase {



    protected static PageLocation pageLocation;


    /*---------CONSTANTS--------*/

    protected final List<String> admin = Arrays.asList("admin", "admin");
    protected final String linkEditUser = "Edit user";
    protected final String linkEditMembership = "Edit membership";
    protected final String allUsers = "All users";
    protected final String statusAny = "Any";
    protected final String statusPending = "Pending";
    protected final String statusBlocked = "Blocked";
    protected final String statusActive = "Active";
    protected final String cancelUserAccount = "Cancel user account";
    protected final String modifyMembershipStatus = "Modify membership status";
    protected final String removeFromGroup = "Remove from group";


    /* ----- Tests ----- */

    @Before
    public void beforeTest() throws Exception {
        pageLocation = new PageLocation(websiteUrl);
        HomePage homePage = HomePage.openThisPage(webDriver, pageLocation);
        // Login as admin and clear Recent log messages
        loginAs(admin);
        clearLogMessages();
        webDriver.get(websiteUrl);
        waitForPageToLoad();
    }

    @After
    public void afterTest() throws Exception {
        HomePage homePage = openHomePage();
        String xpathLinkUserAccount = "//nav[@id='block-system-user-menu']/ul/li/a";
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathLinkUserAccount)));
        WebElement linkUserAccount = webDriver.findElement(By.xpath(xpathLinkUserAccount));
        if (!(linkUserAccount.getText().equals(admin.get(0)))) {
            if (linkUserAccount.getText().equals("Sign in")) {
                login(admin);
            } else {
                linkUserAccount.click();
                homePage.clickLinkSignOut();
                login(admin);
            }
        }
        webDriver.get(websiteUrl.concat("/admin/reports/dblog"));
        waitForPageToLoad();
        String xpath = "//*/table/tbody/tr";
        List<WebElement> elements = webDriver.findElements(By.xpath(xpath));
        ArrayList<String> errors = new ArrayList<String>();
        for (WebElement element : elements) {
            if (element.findElements(By.xpath("./td")).size() == 1) {   // No log messages available
                break;
            }

            if (element.findElement(By.xpath(".//td[2]")).getText().equals("php")) {   // Log messages exist
                WebElement message = element.findElement(By.xpath(".//td[4]/a"));
                if (message.getText().matches("Notice:.*") || message.getText().matches("Warning:.*")
                        || message.getText().matches("Strict warning:.*")) {  // Find php messages starting with "Notice:" or "Warning:"
                    errors.add(message.getAttribute("href"));
                }
            }
        }

        // Get all php messages
        for (String url : errors) {
            webDriver.get(url);
            String xpathOfMessage = "//*[@id=\"content\"]/table/tbody/tr[6]/td";
            waitUntilElementIsVisible(webDriver.findElement(By.xpath(xpathOfMessage)));
            String fullMessage = webDriver.findElement(By.xpath(xpathOfMessage)).getText();
            System.out.println(fullMessage);
        }

        if (errors.size() > 0)
            Assert.fail("Test failed: log messages with 'php' type were found.");
    }

    public WhoIsWhoPage waitUntilWhoIsWhoPageOpens() throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until WhoIsWho page opens
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//option[text()='All users']")));
        return new WhoIsWhoPage(webDriver, pageLocation);
    }

    public String getSelectedOption() throws Exception {
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        String cssSelectedOption = ".view-header option[selected]";
        String nameOption = webDriver.findElement(By.cssSelector(cssSelectedOption)).getText();
        return nameOption;
    }

    public void checkSelectedOption(String option) throws Exception {
        String nameSelectedOption = getSelectedOption();
        Assert.assertTrue("Selected option on WhoIsWho page is incorrect.", nameSelectedOption.equals(option));
    }

    public void checkUserIsPresentInWhoIsWhoTable(List<String> user) {
        List<WebElement> linksUsers = webDriver.findElements(By.cssSelector(".views-field-field-profile-full-name a"));
        for (WebElement link : linksUsers) {
            if (!(linksUsers.size()==0)) {
                if (link.getText().equals(user.get(0))) {
                    Assert.assertTrue(link.isDisplayed());
                    return;
                }
            }
        }
        Assert.assertTrue("The user is not displayed in the table on WhoIsWho page.", false);
    }

    public void checkUserIsAbsentInWhoIsWhoTable(List<String> user) {
        List<WebElement> linksUsers = webDriver.findElements(By.cssSelector(".views-field-field-profile-full-name a"));
        for (WebElement link : linksUsers) {
            if (!(linksUsers.size()==0)) {
                if (link.getText().equals(user.get(0))) {
                    Assert.assertTrue(link.isDisplayed());
                    Assert.assertTrue("The user is displayed in the table on WhoIsWho page.", false);
                }
            }
        }
    }

    public void linkIsDisplayedInListView(String expectedLink, List<String> userInTheRow) {
        WebElement linkUser = webDriver.findElement(By.xpath("//a[text()='" + userInTheRow.get(0) + "']"));
        List<WebElement> links = linkUser.findElements(By.xpath("./../..//a[contains(text(), '" + expectedLink + "')]"));
        if (links.size() == 1) {
            return;
        }
        Assert.assertTrue("No such link is displayed: " + expectedLink, false);
    }

    public void linkIsNotShownInListView(String expectedLink, List<String> userInTheRow) {
        WebElement linkUser = webDriver.findElement(By.xpath("//a[text()='" + userInTheRow.get(0) + "']"));
        List<WebElement> links = linkUser.findElements(By.xpath("./../..//a[contains(text(), '" + expectedLink + "')]"));
        if (links.size() == 0) {
            return;
        }
        Assert.assertTrue("The link is displayed: " + expectedLink, false);
    }

    public void checkEditLinksAreAbsent() { // grid view
        List<WebElement> links1 = webDriver.findElements(By.xpath("//a[contains(text(), 'Edit membership')]"));
        Assert.assertTrue("The link 'Edit membership' is displayed on WhoIsWho page.", links1.size() == 0);
        List<WebElement> links2 = webDriver.findElements(By.xpath("//a[contains(text(), 'Edit user')]"));
        Assert.assertTrue("The link 'Edit user' is displayed on WhoIsWho page.", links2.size() == 0);
    }

    public void waitUntilGridViewIsOpened() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("table.views-view-grid")));
    }

    public void waitUntilTableViewIsOpened() {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("table.views-table")));
    }

    public void checkTabAuthenticatedUserIsSelected() throws Exception {
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        WebElement tab = whoIsWhoPage.getTabAuthenticatedUser().findElement(By.xpath("./../input"));
        Assert.assertTrue("The tab 'Authenticated user' is not selected.", tab.isSelected());
    }
    public void checkTabStudentsIsSelected() throws Exception {
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        WebElement tab = whoIsWhoPage.getTabStudents().findElement(By.xpath("./../input"));
        Assert.assertTrue("The tab 'Students' is not selected.", tab.isSelected());
    }

    public void checkViewProfilePageCanBeOpened(List<String> user) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        String xpathUsername = "//table//a[text()='" + user.get(0) + "']";
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathUsername)));
        WebElement link = webDriver.findElement(By.xpath(xpathUsername));
        link.click(); // click username
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='field__items']/div[text()='" + user.get(0) + "']")));
    }

    public void checkViewProfilePageCannotBeOpened(List<String> user) throws Exception {
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        String xpathUsername = "//table//a[text()='" + user.get(0) + "']";
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpathUsername)));
        WebElement link = webDriver.findElement(By.xpath(xpathUsername));
        link.click(); // click username
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//p[text()='You are not authorized to access this page.']")));
    }

    public void checkOneOptionIsPresentInClassList() throws Exception {
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        List<WebElement> options = whoIsWhoPage.getFieldClass().findElements(By.tagName("option"));
        Assert.assertTrue("Not only one option is displayed in the list.", options.size() == 1);
    }
    public void checkStatusIsSelected(WebElement fieldStatus, String status) throws Exception {
        List<WebElement> options = fieldStatus.findElements(By.tagName("option"));
        String statusActual;
        for (WebElement option : options) {
            if (option.isSelected()) {
                statusActual = option.getText();
                Assert.assertTrue("Actual status doesn't equal the expected status.", statusActual.contains(status));
                return;
            }
        }
        Assert.assertTrue("No such option in the list.", false);
    }
    public void selectCheckbox(List<String> username) {
        WebElement linkUsername = webDriver.findElement(By.xpath("//a[text()='" + username.get(0) + "']"));
        WebElement checkbox = linkUsername.findElement(By.xpath("./../../td[1]/div/input"));
        //checkbox.sendKeys(Keys.SPACE);
        checkbox.click();
    }
    public void executeCancelUserAccountToBlockOperation(List<String> username) throws Exception {
        // WhoIsWho page is opened
        selectCheckbox(username); // select needed user
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(whoIsWhoPage.getFieldOperation());
        selectItemFromListByOptionName(whoIsWhoPage.getFieldOperation(), "Cancel user account"); // select Cancel option
        // Execute 'Cancel user account' operation with 'Disable the account and keep its content.' option
        whoIsWhoPage.clickButtonExecute();
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = new DeleteUserAccountConfirmationPage(webDriver, pageLocation);
        deleteUserAccountConfirmationPage.selectCheckboxDisableAccountAndKeepContent();
        deleteUserAccountConfirmationPage.clickButtonNext();
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until new confirmation page opens
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='You selected the following ']/.." +
                "/ul/li[text()='" + username.get(0) + "']")));
        whoIsWhoPage = deleteUserAccountConfirmationPage.clickButtonConfirm(); // confirm disabling
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Performed Cancel user account on 1 item.");
    }
    public void executeCancelUserAccountOperation(List<String> user) throws Exception {
        // Execute 'Cancel user account' operation
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = openConfirmationPageWithOperation(user, cancelUserAccount);
        deleteUserAccountConfirmationPage.selectCheckboxDeleteAccountAndItsContent();
        deleteUserAccountConfirmationPage.clickButtonNext();
        waitUntilConfirmationPageOpens(user);
        WhoIsWhoPage whoIsWhoPage = deleteUserAccountConfirmationPage.clickButtonConfirm(); // confirm cancelling
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Performed Cancel user account on 1 item.");
    }
    public void deleteUserAccountWithEditUserLink(List<String> user) throws Exception {
        // Click the link 'Edit user'
        EditUserAccountPage editUserAccountPage = clickLinkEditUser(user);
        // Delete the user
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = editUserAccountPage.clickButtonCancelAccount(user);
        deleteUserAccountConfirmationPage.selectCheckboxDeleteAccountAndItsContent();
        deleteUserAccountConfirmationPage.clickButtonCancelAccount();
        waitUntilWhoIsWhoPageOpens();
    }
    public void executeModifyAddUserRoleOperation(List<String> user, String role) throws Exception {
        // Execute 'Modify user roles' operation
        ModifyRolePage modifyRolePage = openModifyRolePageWithProperOperation(user);
        selectItemFromListByOptionName(modifyRolePage.getFieldAddRoles(), role);
        modifyRolePage.clickButtonNext();
        waitUntilConfirmationPageOpens(user);
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = new DeleteUserAccountConfirmationPage(webDriver, pageLocation);
        WhoIsWhoPage whoIsWhoPage = deleteUserAccountConfirmationPage.clickButtonConfirm();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Performed Modify user roles on 1 item.");
    }
    public void executeModifyRemoveUserRoleOperation(List<String> user, String role) throws Exception {
        // Execute 'Modify user roles' operation
        ModifyRolePage modifyRolePage = openModifyRolePageWithProperOperation(user);
        selectItemFromListByOptionName(modifyRolePage.getFieldRemoveRoles(), role);
        modifyRolePage.clickButtonNext();
        waitUntilConfirmationPageOpens(user);
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = new DeleteUserAccountConfirmationPage(webDriver, pageLocation);
        WhoIsWhoPage whoIsWhoPage = deleteUserAccountConfirmationPage.clickButtonConfirm();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Performed Modify user roles on 1 item.");
    }
    public void executeModifyMembershipStatusOperation(List<String> user, String status) throws Exception {
        // Execute 'Modify membership status' operation
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = openConfirmationPageWithOperation(user, modifyMembershipStatus);
        selectItemFromListByOptionName(deleteUserAccountConfirmationPage.getFieldState(), status);
        deleteUserAccountConfirmationPage.clickButtonNext();
        waitUntilConfirmationPageOpens(user);
        WhoIsWhoPage whoIsWhoPage = deleteUserAccountConfirmationPage.clickButtonConfirm(); // confirm current operation
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Performed Modify membership status on 1 item.");
    }
    public void executeRemoveFromGroupOperation(List<String> user) throws Exception {
        // Execute 'Remove from group' operation
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = openConfirmationPageWithOperation(user, removeFromGroup);
        waitUntilConfirmationPageOpens(user);
        WhoIsWhoPage whoIsWhoPage = deleteUserAccountConfirmationPage.clickButtonConfirm(); // confirm current operation
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Performed Remove from group on 1 item.");
    }
    public void removeUserFromGroupWithEditMembershipLink(List<String> user, String className) throws Exception {
        RemoveMembershipInGroupPage removeMembershipInGroupPage = openRemoveMembershipInGroupPage(user, className);
        removeMembershipInGroupPage.clickButtonRemove();
        PeopleInGroupForClassPage peopleInGroupForClassPage = new PeopleInGroupForClassPage(webDriver, pageLocation);
        assertTextOfMessage(peopleInGroupForClassPage.getMessage(), ".*The membership was removed.");
        List<WebElement> links = webDriver.findElements(By.linkText(user.get(0)));
        Assert.assertTrue("The user account is displayed in the group (Class) table.", links.size() == 0);
    }
    public RemoveMembershipInGroupPage openRemoveMembershipInGroupPage(List<String> user, String className) throws Exception {
        EditMembershipInGroupPage editMembershipInGroupPage = clickLinkEditMembership(user);
        editMembershipInGroupPage.clickLinkRemove();
        waitUntilPageIsOpenedWithTitle("Remove membership in group " + className);
        return new RemoveMembershipInGroupPage(webDriver, pageLocation);
    }
    public void waitUntilConfirmationPageOpens(List<String> username) {
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until new confirmation page opens
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[text()='You selected the following ']/.." +
                "/ul/li[contains(text(), '" + username.get(0) + "')]")));
    }
    public DeleteUserAccountConfirmationPage openConfirmationPageWithOperation(List<String> username, String operation) throws Exception {
        // WhoIsWho page is opened
        selectCheckbox(username); // select needed user
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(whoIsWhoPage.getFieldOperation());
        selectItemFromListByOptionName(whoIsWhoPage.getFieldOperation(), operation); // select option
        // Click Execute button
        whoIsWhoPage.clickButtonExecute();
        return new DeleteUserAccountConfirmationPage(webDriver, pageLocation);
    }
    public ModifyRolePage openModifyRolePageWithProperOperation(List<String> user) throws Exception {
        // WhoIsWho page is opened
        selectCheckbox(user); // select needed user
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        waitUntilElementIsVisible(whoIsWhoPage.getFieldOperation());
        selectItemFromListByOptionName(whoIsWhoPage.getFieldOperation(), "Modify user roles"); // select 'Modify user roles'
        // Execute 'Modify user roles' operation
        whoIsWhoPage.clickButtonExecute();
        return new ModifyRolePage(webDriver, pageLocation);
    }
    public void blockUserAccountWithLinkEditUser(List<String> username) throws Exception {
        EditUserAccountPage editUserAccountPage = clickLinkEditUser(username);
        editUserAccountPage.selectRadiobuttonBlocked();
        editUserAccountPage.clickButtonSave();
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*The changes have been saved.");
    }
    public void checkLinkActivateUserIsDisplayed(List<String> username) {
        WebElement linkActivateUser = webDriver.findElement(By.xpath("//a[text()='" + username.get(0) + "']" +
                "/../../td/a[text()='Activate user']"));
        Assert.assertTrue("The link 'Activate user' is not displayed.", linkActivateUser.isDisplayed());
    }
    public void checkUserIsActive(List<String> username, String status) throws Exception {
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        selectItemFromListByOptionName(whoIsWhoPage.getFieldStatus(), status); // select option 'No'/'Yes' in the field 'Active'
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until
        int i;
        if (status.equals("No")) {
            i = 0; // status 'No'
        } else { i = 1; } // status 'Yes'
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@class='vbo-views-form']" +
                "/form[contains(@action, 'status=" + i + "')]")));
        checkUserIsPresentInWhoIsWhoTable(username); // the user is displayed in the table
    }
    public EditUserAccountPage clickLinkEditUser(List<String> username) throws Exception {
        WebElement linkEditUser = webDriver.findElement(By.xpath("//a[text()='" + username.get(0) + "']" +
                "/../../td/a[text()='Edit user']"));
        linkEditUser.click();
        EditUserAccountPage editUserAccountPage = new EditUserAccountPage(webDriver, pageLocation);
        waitUntilElementIsVisible(editUserAccountPage.getFieldFullName());
        return new EditUserAccountPage(webDriver, pageLocation);
    }
    public EditMembershipInGroupPage clickLinkEditMembership(List<String> username) throws Exception {
        WebElement linkEditMembership = webDriver.findElement(By.xpath("//a[text()='" + username.get(0) + "']" +
                "/../../td/a[text()='Edit membership']"));
        linkEditMembership.click();
        waitUntilPageIsOpenedWithTitle("Edit membership in group");
        return new EditMembershipInGroupPage(webDriver, pageLocation);
    }

    public void clickLinkActivateUser(List<String> username) throws Exception {
        String xpathLinkActivateUser = "//a[text()='" + username.get(0) + "']/../../td/a[text()='Activate user']";
        WebElement linkActivateUser = webDriver.findElement(By.xpath(xpathLinkActivateUser));
        linkActivateUser.click(); // click the link 'Activate user'
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until the link disappears
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpathLinkActivateUser)));
    }
    public void clickLinkApprove(List<String> username) {
        String xpathLinkApprove = "//a[text()='" + username.get(0) + "']/../../td/a[text()='Approve']";
        WebElement linkApprove = webDriver.findElement(By.xpath(xpathLinkApprove));
        linkApprove.click(); // click the link 'Approve'
        WebDriverWait wait = new WebDriverWait(webDriver, 10); // wait until the link disappears
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.xpath(xpathLinkApprove)));
    }

    public void verifyUserGroupMemberRoleInClass(String username, List<String> UsersInTab)throws Exception{
        List <String> user=new ArrayList<String>();
       // int element;
        HomePage homePage = new HomePage(webDriver, pageLocation);
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();
        waitForPageToLoad();
        Assert.assertTrue("Students tab is not selected by default on 'Who is who page'",whoIsWhoPage.getTabStudents().isDisplayed());
        // Verify that Roles tabs are shown correct for particular user
        rolesTabsShownCorrect(UsersInTab, username, whoIsWhoPage);
        user.add(username);
        if (username.equals(student.get(0))){
            checkUserIsPresentInWhoIsWhoTable(user);
        }
        else{checkUserIsAbsentInWhoIsWhoTable(user);

        verifyThatUserPresentedInCurrentTab(username, whoIsWhoPage);}


    }

    public void rolesTabsShownCorrect(List<String> Tabs, String username,WhoIsWhoPage whoIsWhoPage)throws Exception{
               if(!(whoIsWhoPage.getTabsName().size()==Tabs.size())||(!whoIsWhoPage.getTabsName().containsAll(Tabs))){throw new NoSuchContextException("Tabs are not as expected for["+username+"] \n"+"Expected: ["+Tabs+"] \n"
                +"Actual ["+whoIsWhoPage.getTabsName()+"]");}
    }

    public void verifyThatUserPresentedInCurrentTab(String username,WhoIsWhoPage whoIsWhoPage) throws Exception{
        List <String> user=new ArrayList<String>();
        if(username.equals(administratorMember.get(0))){whoIsWhoPage.switchToTabAdminisInCertainClass();}
        else if(username.equals(juryMember.get(0))){whoIsWhoPage.switchToTabJuries();}
        else if(username.equals(contentEditor.get(0))){whoIsWhoPage.switchToTabClassManager();}
        else if(username.equals(teacherMember.get(0))){whoIsWhoPage.switchToTabTeachers();}
        else{throw new NoSuchContextException("No such ClassRole ["+username+"]");}
        user.add(username);
        checkUserIsPresentInWhoIsWhoTable(user);
    }




}
