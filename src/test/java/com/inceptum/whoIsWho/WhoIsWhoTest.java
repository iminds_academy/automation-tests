package com.inceptum.whoIsWho;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.inceptum.pages.AddUserPage;
import com.inceptum.pages.DeleteUserAccountConfirmationPage;
import com.inceptum.pages.EditMembershipInGroupPage;
import com.inceptum.pages.EditUserAccountPage;
import com.inceptum.pages.GroupTabClassPage;
import com.inceptum.pages.HomePage;
import com.inceptum.pages.LoginFormPage;
import com.inceptum.pages.ModifyRolePage;
import com.inceptum.pages.OverviewPage;
import com.inceptum.pages.PeopleInGroupForClassPage;
import com.inceptum.pages.RemoveMembershipInGroupPage;
import com.inceptum.pages.WhoIsWhoPage;

/**
 * Created by Olga on 23.02.2015.
 */
public class WhoIsWhoTest extends WhoIsWhoBaseTest {

    /* ----- CONSTANTS ----- */

    protected final List<String> administrator = Arrays.asList("administrator", "administrator");
    protected final List<String> classManager = Arrays.asList("classManager", "classManager");
    protected final List<String> masterEditor = Arrays.asList("masterEditor", "masterEditor");
    protected final String roleAdministrator = "administrator";
    protected final String roleClassManager = "class manager";
    protected final String roleMasterEditor = "master editor";


    /* ----- Tests ----- */

    @Test
    public void testAdministratorRoleWholeSite() throws Exception {
        // Delete 'administrator' account if it has already been registered
        checkUserIsAlreadyRegistered(administrator);
        // Create 'student' account
        String viewClassPageUrl = addStudentToClass(student, className);

        // Create 'administrator' account
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(administrator);
        addUserPage.addUserWithRole(roleAdministrator, administrator.get(0), email, administrator.get(1));
        // Check 'administrator' role is displayed in the table
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleAdministrator, administrator);

        // Open WhoIsWho page with Users link, check "All users" option is selected
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkSelectedOption(allUsers);

        // Check that new user is displayed in "Administrator" table on WhoIsWho page
        whoIsWhoPage.switchToTabAdministrator();
        checkUserIsPresentInWhoIsWhoTable(administrator);

        // Add administrator user to the Class
        webDriver.get(viewClassPageUrl);
        homePage.clickLinkWhoIsWho();
        PeopleInGroupForClassPage peopleInGroupForClassPage = whoIsWhoPage.clickButtonAddUser();
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), administrator.get(0));
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + administrator.get(0) + ".*has been added to the group " + className + ".");
        logout(admin);

        // Log in as administrator and check that "All users" option is selected on WhoIsWho page
        login(administrator);
        homePage.clickLinkUsers(administrator); // go to wide site view
        checkSelectedOption(allUsers);
        // Check the 'administrator' user is in the correct table (Administrator tab)
        whoIsWhoPage.switchToTabAdministrator();
        checkUserIsPresentInWhoIsWhoTable(administrator);
        linkIsNotShownInListView(linkEditMembership, administrator); // 'Edit membership' link is not shown in 'All users' table

        // Open WhoIsWho page in Class view and check 'edit' links are available
        homePage.clickLinkWhoIsWho(); // go to class view
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        linkIsDisplayedInListView(linkEditUser, student); // 'Edit user' link
        linkIsDisplayedInListView(linkEditMembership, student); // 'Edit membership' link
        // Check 'administrator' is shown in Students table
        checkUserIsPresentInWhoIsWhoTable(administrator);
    }

    @Test
    public void testClassManagerRoleWholeSite() throws Exception {
        // Delete 'class manager' account if it has already been registered
        checkUserIsAlreadyRegistered(classManager);
        // Create 'student' account
        String viewClassPageUrl = addStudentToClass(student, className);

        // Create 'class manager' account
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(classManager);
        addUserPage.addUserWithRole(roleClassManager, classManager.get(0), email, classManager.get(1));
        // Check 'class manager' role is displayed in the table
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleClassManager, classManager);

        // Open WhoIsWho wide site view, check "All users" option is selected
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkSelectedOption(allUsers);

        // Check that new user is displayed in "Class manager" table on WhoIsWho page
        whoIsWhoPage.switchToTabClassManager();
        checkUserIsPresentInWhoIsWhoTable(classManager);

        // Add class manager to the Class
        webDriver.get(viewClassPageUrl);
        homePage.clickLinkWhoIsWho();
        PeopleInGroupForClassPage peopleInGroupForClassPage = whoIsWhoPage.clickButtonAddUser();
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), classManager.get(0));
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + classManager.get(0) + ".*has been added to the group " + className + ".");


        logout(admin);

        // Log in as class manager, check that "All users" option is selected on WhoIsWho page in wide site view
        login(classManager);
        homePage.clickLinkUsers(classManager); // go to wide site view
        checkSelectedOption(allUsers);
        // Check the 'class manager' user is in the correct table ('Class manager' tab)
        whoIsWhoPage.switchToTabClassManager();
        checkUserIsPresentInWhoIsWhoTable(classManager);
        linkIsNotShownInListView(linkEditMembership, classManager); // 'Edit membership' link is not shown in 'All users' table

        // Open WhoIsWho page in Class view and check 'edit' links are available
        homePage.clickLinkWhoIsWho(); // go to class view
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        linkIsDisplayedInListView(linkEditUser, student); // 'Edit user' link
        linkIsDisplayedInListView(linkEditMembership, student); // 'Edit membership' link
        // Check 'class manager' is shown in Students table
        checkUserIsPresentInWhoIsWhoTable(classManager);
    }

    @Test // PILOT-1216 !!!!!
    public void testMasterEditorRoleWholeSite() throws Exception {
        // Delete 'master editor' account if it has already been registered
        checkUserIsAlreadyRegistered(masterEditor);
        // Create 'student' account
        String viewClassPageUrl = addStudentToClass(student, className);

        // Create 'master editor' account
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(masterEditor);
        addUserPage.addUserWithRole(roleMasterEditor, masterEditor.get(0), email, masterEditor.get(1));
        // Check 'master editor' role is displayed in the table
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleMasterEditor, masterEditor);

        // Open WhoIsWho page in wide site view, check that the new user is displayed in "Master editor" table
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        whoIsWhoPage.switchToTabMasterEditor();
        checkUserIsPresentInWhoIsWhoTable(masterEditor);

        // Add master editor to the Class
        webDriver.get(viewClassPageUrl);
        homePage.clickLinkWhoIsWho();
        PeopleInGroupForClassPage peopleInGroupForClassPage = whoIsWhoPage.clickButtonAddUser();
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), masterEditor.get(0));
        peopleInGroupForClassPage.clickButtonAddUsers();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + masterEditor.get(0) + ".*has been added to the group " + className + ".");


        logout(admin);

        // Log in as master editor, check that 'Who's who', 'Users' links aren't displayed
        login(masterEditor);
        homePage.clickLinkWhoIsWho(); // go to class view
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
       // waitUntilTableViewIsOpened(); // table view
        checkUserIsPresentInWhoIsWhoTable(masterEditor);
        //homePage.checkLinkWhoIsWhoIsNotDisplayed(); // bug !!!
    }

    @Test
    public void testAuthenticatedUserRoleWholeSite() throws Exception {
        // Add authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho wide site view, check that new user is displayed in "Authenticated user" table
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(student);

        // Open WhoIsWho class view, check the user is displayed in Students table
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        checkUserIsPresentInWhoIsWhoTable(student);

        logout(admin);

        // Log in as student, check that 'Who's who', 'Users' links aren't displayed
        login(student);
        homePage.checkLinkUsersIsNotDisplayedForUser(student);
        webDriver.navigate().refresh(); // refresh the page to close the popup
       //  homePage.checkLinkWhoIsWhoIsNotDisplayed();
        verifyUserGroupMemberRoleInClass(student.get(0), WhoIsWhoThreeTabs);
    }


    @Test
    public void testAuthenticatedUserRoleTemplateClass() throws Exception {

        // Add authenticated user
        addAuthenticatedUser(student);

        // Open WhoIsWho wide site view, check that new user is displayed in "Authenticated user" table
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(student);

        // Open WhoIsWho class view, check the user is displayed in Students table
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), exampleClass);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        checkUserIsPresentInWhoIsWhoTable(student);

        logout(admin);

        // Log in as student, check that 'Who's who', 'Users' links aren't displayed
        login(student);
        homePage.checkLinkUsersIsNotDisplayedForUser(student);
        webDriver.navigate().refresh(); // refresh the page to close the popup
        homePage.checkLinkWhoIsWhoIsNotDisplayed();
    }

    @Test
    public void testAdministratorMemberGroupRole() throws Exception {
        // Add 'administrator member' to Class
        addUserToClassWithRole(administratorMember, className, roleAdministratorMember);
        // Check the role of the user is shown in group table
/*        PeopleInGroupForClassPage peopleInGroupForClassPage = new PeopleInGroupForClassPage(webDriver, pageLocation);
        peopleInGroupForClassPage.clickLinkGroup();
        assertGroupRoleIsShown(administratorMember, roleAdministratorMember, className);*/
        // Check roles for whole site are automatically added to the user
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleClassEditor + "\n" + roleClassJury, administratorMember); // 'class editor', 'class jury'

        // Check that new user is displayed in "Authenticated user" table on WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(administratorMember);

        // Open WhoIsWho class view, check the user isn't displayed in Students table
       /* homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        checkUserIsAbsentInWhoIsWhoTable(administratorMember);*/

        logout(admin);

        // Log in as 'administrator member', check that 'Who's who', 'Users' links aren't displayed
        login(administratorMember);
        homePage.checkLinkUsersIsNotDisplayedForUser(administratorMember);
        webDriver.navigate().refresh(); // refresh the page to close the popup
        // homePage.checkLinkWhoIsWhoIsNotDisplayed();
        verifyUserGroupMemberRoleInClass(administratorMember.get(0), WhoIsWhoFiveTabs);
    }

    @Test
    public void testJuryMemberGroupRole() throws Exception {
        // Add 'jury member' to Class
        addUserToClassWithRole(juryMember, className, roleJuryMember);
        // Check the role of the user is shown in group table
      /*  PeopleInGroupForClassPage peopleInGroupForClassPage = new PeopleInGroupForClassPage(webDriver, pageLocation);
        peopleInGroupForClassPage.clickLinkGroup();
        assertGroupRoleIsShown(juryMember, roleJuryMember, className);*/
        // Check role for whole site is automatically added to the user
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleClassJury, juryMember); // 'class jury'

        // Check that 'jury member' is displayed in "Authenticated user" table on WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(juryMember);

        // Open WhoIsWho class view, check the user is displayed in Juries table
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        whoIsWhoPage.switchToTabJuries();
        checkUserIsPresentInWhoIsWhoTable(juryMember);

        logout(admin);

        // Log in as 'jury member', check that 'Who's who', 'Users' links aren't displayed
        login(juryMember);
        homePage.checkLinkUsersIsNotDisplayedForUser(juryMember);
        webDriver.navigate().refresh(); // refresh the page to close the popup
        // homePage.checkLinkWhoIsWhoIsNotDisplayed();
        verifyUserGroupMemberRoleInClass(juryMember.get(0), WhoIsWhoThreeTabs);
    }
    //User with group role "Content manager" could not identify himself on "Who's who" page
    @Test
    public void testContentEditorGroupRole() throws Exception {
        // Add 'content editor' to Class
        addUserToClassWithRole(contentEditor, className, roleContentEditor);
        /*
        // Check the role of the user is shown in group table
        PeopleInGroupForClassPage peopleInGroupForClassPage = new PeopleInGroupForClassPage(webDriver, pageLocation);
        peopleInGroupForClassPage.clickLinkGroup();
        assertGroupRoleIsShown(contentEditor, roleContentEditor, className);
        */
        // Check role for whole site is automatically added to the user
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleClassEditor, contentEditor); // 'class editor'

        // Check that 'content editor' is displayed in "Authenticated user" table on WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(contentEditor);

        // Open WhoIsWho class view, check the user isn't displayed in Students table
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        checkUserIsAbsentInWhoIsWhoTable(contentEditor);

        logout(admin);

        // Log in as 'content editor', check that 'Who's who', 'Users' links aren't displayed
        login(contentEditor);
        homePage.checkLinkUsersIsNotDisplayedForUser(contentEditor);
        webDriver.navigate().refresh(); // refresh the page to close the popup
      //  homePage.checkLinkWhoIsWhoIsNotDisplayed();
        verifyUserGroupMemberRoleInClass(contentEditor.get(0), WhoIsWhoThreeTabs);
    }

    @Test
    public void testTeacherMemberGroupRole() throws Exception {
        // Add 'teacher member' to Class
        addUserToClassWithRole(teacherMember, className, roleTeacherMember);
        /*
        // Check the role of the user is shown in group table
        PeopleInGroupForClassPage peopleInGroupForClassPage = new PeopleInGroupForClassPage(webDriver, pageLocation);
        peopleInGroupForClassPage.clickLinkGroup();
        assertGroupRoleIsShown(teacherMember, roleTeacherMember, className);
        // Check that no role for whole site is displayed for the user
       */
        openPeoplePage();
        roleIsAbsentInPeopleTable(teacherMember); // no role

        // Check that 'teacher member' is displayed in "Authenticated user" table on WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(teacherMember);

        // Open WhoIsWho class view, check the user is displayed in Teachers table
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        whoIsWhoPage.switchToTabTeachers();
        checkUserIsPresentInWhoIsWhoTable(teacherMember);

        logout(admin);

        // Log in as 'teacher member', check that 'Who's who', 'Users' links aren't displayed
        login(teacherMember);
        homePage.checkLinkUsersIsNotDisplayedForUser(teacherMember);
        webDriver.navigate().refresh(); // refresh the page to close the popup
       //  homePage.checkLinkWhoIsWhoIsNotDisplayed();
        verifyUserGroupMemberRoleInClass(teacherMember.get(0), WhoIsWhoThreeTabs);
    }

    @Test
    public void testBlockUseraccountWithProperOperation() throws Exception {
        // Add authenticated user
        addAuthenticatedUser(student);
        // Open WhoIsWho page and check that 'All users' option, 'Any' status are selected
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);
        checkSelectedOption(allUsers);
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        checkStatusIsSelected(whoIsWhoPage.getFieldStatus(), statusAny);

        // Block user account with 'Cancel user account' option
        executeCancelUserAccountToBlockOperation(student);

        // The link 'Activate user' is displayed near the user
        checkLinkActivateUserIsDisplayed(student);
        checkUserIsActive(student, "No"); // user is blocked

        logout(admin);

        // Check the user is not able to log in with 'blocked' status, error message is shown
        submitLoginForm(student);
        LoginFormPage loginFormPage = new LoginFormPage(webDriver, pageLocation);
        assertTextOfMessage(loginFormPage.getMessage(), ".*The username " + student.get(0) + " has not been activated or is blocked.");
    }

    @Test
    public void testChangeUserStatusWithLinksEditActivateUser() throws Exception {
        // Add authenticated user
        addAuthenticatedUser(student);
        // Open WhoIsWho page and check that 'All users' option, 'Any' status are selected
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);
        checkSelectedOption(allUsers);
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        checkStatusIsSelected(whoIsWhoPage.getFieldStatus(), statusAny);

        // Block user account with 'Edit user' link
        blockUserAccountWithLinkEditUser(student);
        checkLinkActivateUserIsDisplayed(student); // the link 'Activate user' appeared

        logout(admin);

        // Check the user is not able to log in with 'blocked' status, error message is shown
        submitLoginForm(student);
        LoginFormPage loginFormPage = new LoginFormPage(webDriver, pageLocation);
        assertTextOfMessage(loginFormPage.getMessage(), ".*The username " + student.get(0) + " has not been activated or is blocked.");

        // Change status to 'active' with 'Activate user' link, check the user is able to log in
        login(admin);
        homePage.clickLinkUsers(admin);
        clickLinkActivateUser(student);
        checkUserIsActive(student, "Yes");

        logout(admin);

        login(student);
    }

    @Test
    public void testCancelUserAccountWithProperOperation() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);

        // Cancel user account with the proper operation
        executeCancelUserAccountOperation(student);
        checkTabAuthenticatedUserIsSelected(); // the tab 'Authenticated user' is opened

        // Check that the user account is not displayed in the table
        checkUserIsAbsentInWhoIsWhoTable(student);
    }

    @Test
    public void testCancelDeletingUserAccountWithBulkOperation() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);

        // Select the user in the table, click Execute button for 'Cancel user account' operation
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = openConfirmationPageWithOperation(student, cancelUserAccount);
        deleteUserAccountConfirmationPage.clickLinkCancel(); // cancel deleting user account
        waitUntilTableViewIsOpened();
        checkUserIsPresentInWhoIsWhoTable(student);

        // Click Execute the operation one more time, Next button
        deleteUserAccountConfirmationPage = openConfirmationPageWithOperation(student, cancelUserAccount);
        deleteUserAccountConfirmationPage.clickButtonNext();
        waitUntilConfirmationPageOpens(student);
        deleteUserAccountConfirmationPage.clickLinkCancel(); // cancel deleting user account
        waitUntilTableViewIsOpened();
        checkUserIsPresentInWhoIsWhoTable(student); // the user account hasn't been deleted
    }

    @Test
    public void testCancelUserAccountWithEditUserLink() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);

        // Delete user account with 'Edit user' link
        deleteUserAccountWithEditUserLink(student);

        // Check info message
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + student.get(0) + " has been deleted.");
        waitUntilTableViewIsOpened();
        checkUserIsAbsentInWhoIsWhoTable(student);
        // Check that the account has been deleted from People table
        openPeoplePage();
        List<WebElement> elements = webDriver.findElements(By.xpath("//table/tbody/tr/td[2]/a[text()='" + student.get(0) + "']"));
        Assert.assertTrue("The account '" + student.get(0) + "' is present in People table.", elements.size() == 0);
    }

    @Test
    public void testCancelDeletingUserAccountWithEditUserLink() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);

        // Click link 'Edit user'
        EditUserAccountPage editUserAccountPage = clickLinkEditUser(student);
        // Click 'Cancel account' button
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = editUserAccountPage.clickButtonCancelAccount(student);
        // Cancel deleting the account by clicking the proper link
        deleteUserAccountConfirmationPage.clickLinkCancel();

        // Check that the user account is still present in WhoIsWho table
        waitUntilWhoIsWhoPageOpens();
        waitUntilTableViewIsOpened();
        checkUserIsPresentInWhoIsWhoTable(student);
    }

    @Test
    public void testChangeUserRoleForWholeSiteWithProperOperation() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);

        // Add new role for the user using the proper operation
        executeModifyAddUserRoleOperation(student, roleMasterEditor);

        // Check that the user isn't already displayed in 'Authenticated user' table
        checkTabAuthenticatedUserIsSelected();
        checkUserIsAbsentInWhoIsWhoTable(student);
        // Check that the user is present in 'Master editor' table
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        whoIsWhoPage.switchToTabMasterEditor();
        checkUserIsPresentInWhoIsWhoTable(student);
        // Check that the new role is displayed in People table for the user
        openPeoplePage();
        roleIsDisplayedInPeopleTable(roleMasterEditor, student);
    }

    @Test
    public void testCancelChangingRoleWithBulkOperation() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);

        // Open 'Modify user role' page
        ModifyRolePage modifyRolePage = openModifyRolePageWithProperOperation(student);
        modifyRolePage.clickLinkCancel(); // cancel changing role
        waitUntilWhoIsWhoPageOpens();

        // Click Execute the operation one more time, Next button
        modifyRolePage = openModifyRolePageWithProperOperation(student);
        selectItemFromListByOptionName(modifyRolePage.getFieldAddRoles(), roleClassManager);
        modifyRolePage.clickButtonNext();
        waitUntilConfirmationPageOpens(student);
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = new DeleteUserAccountConfirmationPage(webDriver, pageLocation);
        deleteUserAccountConfirmationPage.clickLinkCancel(); // cancel adding the role

        // Check that the user is still present in 'Authenticated user' table (no role has been added)
        waitUntilWhoIsWhoPageOpens();
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(student);
    }

    @Test
    public void testRemoveUserRoleForWholeSiteWithProperOperation() throws Exception {
        // Add a user with whole site role 'administrator'
        addUserWithSiteRole(administrator, roleAdministrator);
        openPeoplePage(); // check 'administrator' role is displayed in People table for the user
        roleIsDisplayedInPeopleTable(roleAdministrator, administrator);
        // Open WhoIsWho/Administrator page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        whoIsWhoPage.switchToTabAdministrator();

        // Execute 'Modify user roles' operation to remove the role
        executeModifyRemoveUserRoleOperation(administrator, roleAdministrator);

        // Check that the user account is already displayed in 'Authenticated user' table
        whoIsWhoPage.switchToTabAuthenticatedUser();
        checkUserIsPresentInWhoIsWhoTable(administrator);
        // Check that the user has no role in People table
        openPeoplePage();
        roleIsAbsentInPeopleTable(administrator);
    }

    @Test
    public void testChangeUserRoleForWholeSiteWithEditUserLink() throws Exception {
        // Add authenticated user, open WhoIsWho page
        addAuthenticatedUser(student);
        HomePage homePage = openHomePage();
        homePage.clickLinkUsers(admin);

        // Add 'class manager' role to the user with 'Edit user' link
        EditUserAccountPage editUserAccountPage = clickLinkEditUser(student);
        editUserAccountPage.selectRole(roleClassManager);
        editUserAccountPage.clickButtonSave();
        WhoIsWhoPage whoIsWhoPage = new WhoIsWhoPage(webDriver, pageLocation);
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*The changes have been saved.");

        checkUserIsAbsentInWhoIsWhoTable(student); // user is absent in 'Authenticated user' table
        whoIsWhoPage.switchToTabClassManager(); // switch to 'Class manager' tab
        checkUserIsPresentInWhoIsWhoTable(student); // user account is displayed in 'Class manager' table

        // Remove 'class manager' role from the user
        editUserAccountPage = clickLinkEditUser(student);
        editUserAccountPage.selectRole(roleClassManager); // remove selection of 'class manager' checkbox
        editUserAccountPage.clickButtonSave();

        // Check the user account is displayed in 'Authenticated user' page
        whoIsWhoPage.switchToTabAuthenticatedUser();
        checkUserIsPresentInWhoIsWhoTable(student);
    }

    @Test
    public void testChangeUserRoleWithEditMembershipLink() throws Exception {
        // Add authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho and check that new user is displayed in 'Authenticated user' table
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
        checkUserIsPresentInWhoIsWhoTable(student);

        // Add 'teacher member' role to the user with 'Edit membership' link
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        checkTabStudentsIsSelected();
   //     whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        checkUserIsPresentInWhoIsWhoTable(student);
        EditMembershipInGroupPage editMembershipInGroupPage = clickLinkEditMembership(student);
        editMembershipInGroupPage.selectRole(roleTeacherMember);
        editMembershipInGroupPage.clickButtonUpdateMembership();
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*The membership has been updated.");
        // Check that the user account is displayed in 'Teachers' table
        whoIsWhoPage.switchToTabTeachersListView();
        checkUserIsPresentInWhoIsWhoTable(student);

        // Remove 'teacher member' role from the user
        editMembershipInGroupPage = clickLinkEditMembership(student);
        editMembershipInGroupPage.selectRole(roleTeacherMember); // remove selection of 'teacher member' checkbox
        editMembershipInGroupPage.clickButtonUpdateMembership();
        // Check that the user account is displayed in 'Students' table
        whoIsWhoPage.switchToTabStudentsListView();
        checkUserIsPresentInWhoIsWhoTable(student);
    }

    @Test
    public void testModifyMembershipStatusPendingInGroupWithProperOperation() throws Exception {
        // Add authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();

        // Switch to the Class page/Students tab and change membership status to 'Pending'
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        checkTabStudentsIsSelected();
     // whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        executeModifyMembershipStatusOperation(student, statusPending); // change status to 'Pending'

        logout(admin);
        login(student);

        // Check that the user is not able to view the Class
        OverviewPage overviewPage = clickButtonGoToFullProgram();
        assertOptionIsAbsentInTheList(overviewPage.getFieldClass(), className);

        logout(student);
        login(admin);

        // Approve membership 'Active' status
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
    //    whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        clickLinkApprove(student); // click link 'Approve'

        logout(admin);
        login(student);

        // The user can view the Class page
        overviewPage = clickButtonGoToFullProgram();
        assertItemIsInTheList(overviewPage.getFieldClass(), className);
    }

    @Test
    public void testModifyMembershipStatusBlockedInGroupWithProperOperation() throws Exception {
        // Add authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();

        // Switch to the Class page/Students tab and change membership status to 'Blocked'
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        checkTabStudentsIsSelected();
     //   whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        executeModifyMembershipStatusOperation(student, statusBlocked); // change status to 'Blocked'
        // Check the status is displayed on EditMembership page
        EditMembershipInGroupPage editMembershipInGroupPage = clickLinkEditMembership(student);
        checkStatusIsSelected(editMembershipInGroupPage.getFieldStatus(), statusBlocked);

        logout(admin);
        login(student);

        // Check that the user is not able to view the Class
        OverviewPage overviewPage = clickButtonGoToFullProgram();
        assertOptionIsAbsentInTheList(overviewPage.getFieldClass(), className);

        logout(student);
        login(admin);

        // Change membership status to 'Active' with proper operation
        homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
     //   whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        executeModifyMembershipStatusOperation(student, statusActive); // change status to 'Active'

        logout(admin);
        login(student);

        // The user can view the Class page
        overviewPage = clickButtonGoToFullProgram();
        assertItemIsInTheList(overviewPage.getFieldClass(), className);
    }

    @Test
    public void testRemoveUserFromGroupWithProperOperation() throws Exception {
        // Add an authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho page
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();

        // Switch to the Class page/Students tab
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        checkTabStudentsIsSelected();
    //    whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        checkUserIsPresentInWhoIsWhoTable(student); // check the user account is displayed in 'Students' table

        // Remove the user from the Class with a proper operation
        executeRemoveFromGroupOperation(student);
        checkUserIsAbsentInWhoIsWhoTable(student); // user account is absent in 'Students' table
        // The user account is still displayed in 'Authenticated user' table
        homePage.clickLinkUsers(admin);
        checkTabAuthenticatedUserIsSelected();
     //   whoIsWhoPage.waitUntilFirstTabTableOpens();// wait until 'Students' table opens
        checkUserIsPresentInWhoIsWhoTable(student);

        logout(admin);
        login(student);

        // Check that the user is not able to view the Class
        OverviewPage overviewPage = clickButtonGoToFullProgram();
        assertOptionIsAbsentInTheList(overviewPage.getFieldClass(), className);
    }

    @Test
    public void testRemoveUserFromGroupWithEditMembershipLink() throws Exception {
        // Add an authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho page, the Class view
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);

        // Check that the user account is displayed in 'Students' table
        checkTabStudentsIsSelected(); // 'Students' tab is selected
    //    whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        checkUserIsPresentInWhoIsWhoTable(student);

        // Remove the user from the Class
        removeUserFromGroupWithEditMembershipLink(student, className);

        // Check that the user account is not displayed in the WhoIsWho/Class table
        homePage = openHomePage();
        whoIsWhoPage = homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
    //    whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        checkUserIsAbsentInWhoIsWhoTable(student); // the user is absent in the table
    }

    @Test
    public void testCancelRemovingUserFromGroup() throws Exception {
        // Add an authenticated user to Class
        addStudentToClass(student, className);
        // Open WhoIsWho page, the Class view
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);

        // Check that the user account is displayed in 'Students' table
        checkTabStudentsIsSelected(); // 'Students' tab is selected
     //   whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        checkUserIsPresentInWhoIsWhoTable(student);

        // Click Cancel link on Remove membership page
        RemoveMembershipInGroupPage removeMembershipInGroupPage = openRemoveMembershipInGroupPage(student, className);
        removeMembershipInGroupPage.clickLinkCancel();
        // Check the user is displayed on PeopleInGroup page
        checkUserIsPresentInWhoIsWhoTable(student);

        // Open WhoIsWho page, the Class view
        homePage = openHomePage();
        whoIsWhoPage = homePage.clickLinkWhoIsWho();
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        // Click Cancel link on DeleteUserAccountConfirmation page
        DeleteUserAccountConfirmationPage deleteUserAccountConfirmationPage = openConfirmationPageWithOperation(student, removeFromGroup);
        deleteUserAccountConfirmationPage.clickLinkCancel();

        // Check that the user is displayed in the Class table
     //   whoIsWhoPage.waitUntilFirstTabTableOpens(); // wait until 'Students' table opens
        checkUserIsPresentInWhoIsWhoTable(student);
    }

    @Test
    public void testAddUserToClassGroupFromWhoIsWhoPage() throws Exception {
        checkClassIsAddedOnOverview(className);
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(student);
        // Create user account by admin
        webDriver.get(websiteUrl.concat("/admin/people/create"));
        waitForPageToLoad();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(student);
        addUserPage.addUser(student.get(0), email, student.get(1));

        // Check the user is not displayed on WhoIsWho page (Teachers table)
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkWhoIsWho(); // go to class view
        selectItemFromListByOptionName(whoIsWhoPage.getFieldClass(), className);
        waitUntilElementIsVisible(whoIsWhoPage.getTabStudents());
        waitUntilTableViewIsOpened(); // table view
        whoIsWhoPage.switchToTabTeachers();
        checkUserIsAbsentInWhoIsWhoTable(student);

        // Add the user to the Class group with 'Add users' button
        PeopleInGroupForClassPage peopleInGroupForClassPage = whoIsWhoPage.clickButtonAddUser();
        autoSelectItemFromList(peopleInGroupForClassPage.getFieldUsername(), student.get(0));
        peopleInGroupForClassPage.selectRole(roleTeacherMember); // select 'teacher member' role
        peopleInGroupForClassPage.clickButtonAddUsers();
        // Check the user is displayed in 'Teachers' table
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*" + student.get(0) + ".*has been added to the group "
                + className + ".");
        waitUntilTableViewIsOpened(); // table view
        whoIsWhoPage.switchToTabTeachers();
        checkUserIsPresentInWhoIsWhoTable(student);
    }

    @Test
    public void testCreateUserFromWhoIsWhoTable() throws Exception {
        checkClassIsAddedOnOverview(className);
        // Check if the user is already registered (delete the user account)
        checkUserIsAlreadyRegistered(student);

        // Open WhoIsWho page (wide site view)
        HomePage homePage = openHomePage();
        WhoIsWhoPage whoIsWhoPage = homePage.clickLinkUsers(admin);
        // Check that the user with such name isn't displayed in 'Administrator' table
        checkTabAuthenticatedUserIsSelected();
        whoIsWhoPage.switchToTabAdministrator();
        checkUserIsAbsentInWhoIsWhoTable(student);

        // Create a new user with 'administrator' role
        whoIsWhoPage.clickButtonCreateUser();
        AddUserPage addUserPage = new AddUserPage(webDriver, pageLocation);
        String email = getEmail(student);
        addUserPage.fillInFields(student.get(0), email, student.get(1));
        selectRole(roleAdministrator);
        addUserPage.clickButtonCreateNewAccount();
        // Check the user is displayed in 'Administrator' table
        assertTextOfMessage(whoIsWhoPage.getMessage(), ".*Created a new user account for " + student.get(0) +
                ". No e-mail has been sent.");
        waitUntilTableViewIsOpened(); // table view
        whoIsWhoPage.switchToTabAdministrator();
        // Check the user is displayed in 'Administrator' table
        checkUserIsPresentInWhoIsWhoTable(student);
    }








}
